var path = require('path');
var webpack = require('webpack');
var combineLoaders = require('webpack-combine-loaders')

module.exports = {
    entry: [
        "babel-polyfill", path.resolve(__dirname, 'lib', 'client.js'),
    ],
    output: {
        filename: 'sportunity.js',
        path: path.resolve(__dirname, 'lib'),
    },
    devtool: 'cheap-module-source-map',
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('development')
            }
        }),
        new webpack.optimize.AggressiveMergingPlugin(),
    ],
    module: {
        loaders: [
            {
                test: /\.css$/,
                loader: combineLoaders([
                    {
                        loader: 'style-loader'
                    }, {
                        loader: 'css-loader',
                        query: {
                            modules: true,
                            localIdentName: '[name]__[local]___[hash:base64:5]'
                        }
                    }
                ])
            },
            {
                test: /\.js?$/,
                exclude: /node_modules/,
                loaders: ["react-hot-loader", "babel-loader"],
            }
        ]
    },
};
