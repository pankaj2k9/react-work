import Radium, {StyleRoot} from 'radium';
import React, {Component} from 'react';
import Helmet from 'react-helmet';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

class App extends Component {
    render () {
        const meta = {
            // title:"Test"
        }
        const muiTheme = getMuiTheme({
            stepper: {
                iconColor: '#5EA1D9' // or logic to change color
            }
        })
        return (
            <MuiThemeProvider muiTheme={muiTheme}>
            <div>
                <StyleRoot>
                    {this.props.children}
                </StyleRoot>
            </div>
            </MuiThemeProvider>
        )
    }
};

export default Radium(App);