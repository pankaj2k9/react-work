import https from 'https';
import http from 'http';
import express from 'express';
import path from 'path';
import renderOnServer from './renderOnServer'
import {schema} from '../schema.json';
import { backendUrl, backendUrlGraphql } from '../constants.json';

const APP_PORT = 8080;

var app = express();

// Serve CSS
app.use('/assets/', express.static(path.resolve(__dirname, '..', 'assets'), { maxAge: 86400000*7 }));

// Serve JavaScript
/*app.get('/sportunity.js', (req, res) => {
    res.setHeader('Content-Type', 'application/javascript');
    //if (backendUrlGraphql.search('https') >= 0) {
            req.url = req.url + '.gz';
            res.set('Content-Encoding', 'gzip');
            res.setHeader('Cache-Control', 'public, max-age=86400');
            res.sendFile('sportunity.js.gz', {root: __dirname});
    //}
    //else if (backendUrlGraphql.search('http') >= 0) {
    //  res.sendFile('sportunity.js', {root: __dirname});
    //}
});*/

app.get('/sportunity.js', (req, res) => {
    res.setHeader('Content-Type', 'application/javascript');
    res.sendFile('sportunity.js', { root: __dirname });
});

// Serve HTML
app.get('/*', (req, res, next) => {
    if (backendUrlGraphql.search('https') >= 0) {
        https.get(backendUrlGraphql, (resp) => {
            renderOnServer(req, res, next);
        }).on('error', e => {
            console.log(e);
            res.render(path.resolve(__dirname, '..', 'views', 'down.ejs'), {});
        })
    }
    else if (backendUrlGraphql.search('http') >= 0) {
        http.get(backendUrlGraphql, (resp) => {
            renderOnServer(req, res, next);
        }).on('error', e => {
            console.log(e);
            res.render(path.resolve(__dirname, '..', 'views', 'down.ejs'), {});
        })
    }
});

app.listen(APP_PORT, () => {
    console.log(`App is now running on http://localhost:${APP_PORT}`);
});
