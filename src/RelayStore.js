import RelaySubscriptions from 'relay-subscriptions';

// eslint-disable-next-line no-unused-vars
import RelayNetworkDebug from 'react-relay/lib/RelayNetworkDebug';
import NetworkLayer from './NetworkLayer'
import { backendUrlGraphql } from '../constants.json'

class RelayStore {
  constructor() {
    this._env = new RelaySubscriptions.Environment();
    this._networkLayer = null;
    this._taskScheduler = null;


    // RelayNetworkDebug.init(this._env);
  }

  reset(networkLayer) {

    if (networkLayer !== undefined) {
      this._networkLayer = networkLayer;
    }

    this._env = new RelaySubscriptions.Environment();
    if (this._networkLayer !== null) {
      this._env.injectNetworkLayer(this._networkLayer);
    }
    if (this._taskScheduler !== null) {
      this._env.injectTaskScheduler(this._taskScheduler);
    }

    let token = localStorage.getItem('token');
    let userToken = localStorage.getItem('userToken');
    let superToken = localStorage.getItem('superToken');

    if (token && superToken && userToken === 'undefined') {
      localStorage.setItem('userToken', token)
    }

    // Comment/Uncomment the line bellow to enable relay debug (dafult commented)
    // RelayNetworkDebug.init(this._env);
  }

  // Map existing RelayEnvironment methods
  getStoreData() {
    return this._env.getStoreData();
  }

  injectNetworkLayer(networkLayer) {
    this._networkLayer = networkLayer;
    this._env.injectNetworkLayer(networkLayer);
  }

  injectTaskScheduler(taskScheduler) {
    this._taskScheduler = taskScheduler;
    this._env.injectTaskScheduler(taskScheduler);
  }

  primeCache(...args) {
    return this._env.primeCache(...args);
  }

  forceFetch(...args) {
    return this._env.forceFetch(...args);
  }

  read(...args) {
    return this._env.read(...args);
  }

  readAll(...args) {
    return this._env.readAll(...args);
  }

  readQuery(...args) {
    return this._env.readQuery(...args);
  }

  observe(...args) {
    return this._env.observe(...args);
  }

  getFragmentResolver(...args) {
    return this._env.getFragmentResolver(...args);
  }

  applyUpdate(...args) {
    return this._env.applyUpdate(...args);
  }

  commitUpdate(...args) {
    return this._env.commitUpdate(...args);
  }

  /**
   * @deprecated
   *
   * Method renamed to commitUpdate
   */
  update(...args) {
    return this._env.update(...args);
  }

  // relay-subscriptions
  subscribe(subscription, observer) {

    return this._env.subscribe(subscription, observer);

  }
}

const relayStore = new RelayStore();

export const updateGlobalToken = (token) => {
  localStorage.setItem('token', token);

  if (token === '' || token === null) {
      const networkLayer = new NetworkLayer(backendUrlGraphql, {});
      relayStore.reset(networkLayer);
      //localStorage.clear();
      localStorage.removeItem('userToken');
      localStorage.removeItem('superToken');
      localStorage.removeItem('token');
      localStorage.removeItem('userId');

  } else {
      const networkLayer = new NetworkLayer(
          backendUrlGraphql, {
          headers: {
              token: token,
          },
      });
      console.log("1",networkLayer);
      networkLayer.setToken(token);
      console.log("2",networkLayer)
      relayStore.reset(networkLayer);
  }
}

export const updateSuperToken = (token) => {
  localStorage.setItem('superToken', token);
}

export const updateUserToken = (token) => {
  localStorage.setItem('userToken', token);
}

export default relayStore;
