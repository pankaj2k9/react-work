require('babel-polyfill');

import React from 'react';
import IsomorphicRelay from 'isomorphic-relay';
import IsomorphicRouter from 'isomorphic-relay-router';
import {render} from 'react-dom';
import {browserHistory, match} from 'react-router';
import {routes, AppRoutes} from './routes';
import NetworkLayer from './NetworkLayer';
import RelayStore from './RelayStore';
import { backendUrlGraphql } from '../constants.json';
import RelayNetworkDebug from 'react-relay/lib/RelayNetworkDebug';
import ReactPixel from 'react-facebook-pixel';

import store from './store/store';
// const environment = new Relay.Environment();

if (localStorage.getItem('token')) {

    const networkLayer = new NetworkLayer(
        backendUrlGraphql, {
            headers: {
            token: localStorage.getItem('token'),
            },
        } 
    );
    networkLayer.setToken(localStorage.getItem('token'));
    RelayStore.reset(networkLayer);
}
else {
    const networkLayer = new NetworkLayer(backendUrlGraphql, {});
    // console.log("networkLayer",networkLayer)
    RelayStore.reset(networkLayer);
}

// environment.injectNetworkLayer(new Relay.DefaultNetworkLayer(backendUrlGraphql));

// console.log(RelayStore);
// console.log(RelayStore._env)

const data = JSON.parse(document.getElementById('preloadedData').textContent);

IsomorphicRelay.injectPreparedData(RelayStore._env, data);

const rootElement = document.getElementById('root');

ReactPixel.init('1261089323989181', {}, {
    autoConfig: true, debug: false
});

match({ routes, history: browserHistory }, (error, redirectLocation, renderProps) => {
//   RelayNetworkDebug.init(RelayStore._env);
  IsomorphicRouter.prepareInitialRender(RelayStore._env, renderProps).then(props => {
    
    render(      
        <AppRoutes
          environment={RelayStore._env}
        />
    , rootElement);
  });
});