import IsomorphicRouter from 'isomorphic-relay-router';
import path from 'path';
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { match } from 'react-router';
import Relay from 'react-relay';
import store from './store/store';
import { Provider } from 'react-redux';

import App from './App';
import {routes} from './routes';
import { backendUrlGraphql } from '../constants.json';

import Helmet from 'react-helmet';
// import NetworkLayer from './NetworkLayer';

const networkLayer = new Relay.DefaultNetworkLayer(backendUrlGraphql);
// const networkLayer = new NetworkLayer(backendUrlGraphql, {});

export default (req, res, next) => {
  match({ routes, location: req.url }, (error, redirectLocation, renderProps) => {
    if (error) {
      next(error);
    } else if (redirectLocation) {
      res.redirect(302, redirectLocation.pathname + redirectLocation.search);
    } else if (renderProps) {
      IsomorphicRouter.prepareData(renderProps, networkLayer).then(render).catch(next);
    } else {
      res.status(404).send('Not Found');
    }

    function render({ data, props }) {
      const reactOutput = ReactDOMServer.renderToString(
        <Provider store={store}>
            <App radiumConfig={{userAgent: req.headers['user-agent']}}>
              {IsomorphicRouter.render(props)}
            </App>
        </Provider>
      );
      const meta = Helmet.rewind();
      
      res.render(path.resolve(__dirname, '..', 'views', 'index.ejs'), {
        preloadedData: data,
        reactOutput,
        metaTitle: meta.title.toString(),
        meta: meta.meta.toString()
      });
    }
  });
};
