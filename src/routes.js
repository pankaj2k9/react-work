import React, { Component } from 'react';
import { Route, IndexRoute, browserHistory, applyRouterMiddleware, Router } from 'react-router';
import useRelay from 'react-router-relay';
import { Provider } from 'react-redux';
import Radium, {StyleRoot} from 'radium';
import ReactGA from 'react-ga';

import store from './store/store';

import App from './App'
import Home from './components/Home/Home';
//import Clubs from './components/Clubs/Clubs';
import Register from './components/Register/Register.js';
import Login from './components/Login/Login.js';
import Logout from './components/Login/Logout.js';
import ChangePassword from './components/ChangePassword/ChangePassword.js';
import ResetPassword from './components/ResetPassword/ResetPassword.js';
import Profile from './components/Profile/Profile.js';
import ProfileView from './components/ProfileView/ProfileView.js';
import NewSportunity from './components/NewSportunity/NewSportunity.js';
import FindSportunity from './components/FindSportunity/FindSportunity.js';
import Venue from './components/Venue/Venue.js';
import VenueView from './components/VenueView/VenueView.js';
import Venues from './components/Venues/Venues.js';
import SportOrganization from './components/SportOrganization/SportOrganization';
import Facility from './components/Facility/Facility.js';
import EventView from './components/EventView/EventView'
import SportunityDetail from './components/SportunityDetail/SportunityDetail.js';
import SportsUpdate from './components/SportsUpdate/SportsUpdate.js';
import MyEvents from './components/MyEvents/MyEvents.js'
import Me from './components/Me/Me.js'
import ManageVenue from './components/ManageVenue/ManageVenue.js'
import LoggedIn from './components/LoggedIn/LoggedIn.js'
import MyCircles from './components/MyCircles/MyCircles.js'
import Circle from './components/Circle/Circle.js'
import MyInfo from './components/MyInfo/MyInfo.js'
import Privacy from './components/Static/Privacy'
import AboutUs from './components/AboutUs/AboutUs'
import Term from './components/Static/Term'
import Contact from './components/Static/Contact'
import FaqCalendarSync from './components/FAQ/CalendarSync/index';
import FaqGoogleCalendarSync from './components/FAQ/CalendarSync/GoogleCalendarSync';
import FaqOutlookCalendarSync from './components/FAQ/CalendarSync/OutlookCalendarSync';
import FaqAppleCalendarSync from './components/FAQ/CalendarSync/AppleCalendarSync';
import FaqHowtoFollowOrganiser from './components/FAQ/UserTutorial/FollowOrganiser';
import FaqHowtoModifyProfile from './components/FAQ/UserTutorial/ModifyProfile';
import TeamTutorial from './components/FAQ/UserTutorial/TeamTutorial';
import Tutorial, { TutorialContent } from './components/FAQ/Tutorial';
import ClubsTutorialShareWithTeammates from './components/FAQ/UserTutorial/Clubs/ShareWithTeammates';
import ManageAClubTutorial from './components/FAQ/UserTutorial/Clubs/ManageAClub'
import ClubsTutorialUseStatistics from './components/FAQ/UserTutorial/Clubs/UseStatistics';
import IndividualOrganizeSportActivities from './components/FAQ/UserTutorial/OrganizeSportActivities';
import CompaniesTutorial from './components/FAQ/UserTutorial/CompaniesTutorial';
import Blog from './components/Blog/Blog';
import VenuesHome from  './components/VenuesHome/VenuesHome';
import CitiesHome from './components/CitiesHome/CitiesHome';
import CompaniesHome from './components/CompaniesHome/CompaniesHome';
import ClubsHome from  './components/ClubsHome/ClubsHome'
import LoadUrl from './components/common/LoadUrl/LoadUrl';
import Loading from './components/common/Loading/Loading.js'
import Footer from './components/common/Footer/Footer'

import { backendUrlGraphql } from '../constants.json';
import NetworkLayer from './NetworkLayer';
import RelayStore, {
  updateGlobalToken,
  updateSuperToken,
  updateUserToken
} from './RelayStore';

const ViewerQueries = {
  viewer: () => Relay.QL`query { viewer }`,
};

ReactGA.initialize('UA-86793644-1');

// export const routes = [
//     {
//         path: '/',
//         component: App,
//         indexRoute: {
//             component: Home,
//             queries: ViewerQueries,
//         },
//         childRoutes: [
//             {
//                 path: 'register',
//                 component: Register,
//                 queries: ViewerQueries,
//             },
//             {
//                 path: 'login',
//                 component: Login,
//                 queries: ViewerQueries
//             },
//             {
//                 path:'logged-in',
//                 component: LoggedIn,
//                 queries: ViewerQueries
//             }
//         ],
//     },
// ];

export const routes = (
    <Route path="/" component={App} >
        <IndexRoute component={Home} queries={ViewerQueries}/>
        {/*<Route*/}
            {/*path="/clubs"*/}
            {/*component={Clubs}*/}
            {/*queries={ViewerQueries}*/}
        {/*/>*/}
        <Route 
            path="/s/:shortUrl"
            component={LoadUrl}
            queries={ViewerQueries}
        />
      <Route
        path="/clubs"
        component={ClubsHome}
        queries={ViewerQueries}
      />
      {/*<Route*/}
        {/*path="/cities"*/}
        {/*component={CitiesHome}*/}
        {/*queries={ViewerQueries}*/}
      {/*/>*/}
      <Route
        path="/venues"
        component={VenuesHome}
        queries={ViewerQueries}
      />
      <Route
        path="/companies"
        component={CompaniesHome}
        queries={ViewerQueries}
      />
        <Route
            path="/register"
            component={Register}
            queries={ViewerQueries}
            /*updateToken={this.updateToken}*/
        />
        <Route
            path="/login(/:token)"
            component={Login}
            queries={ViewerQueries}
            /*updateToken={updateToken}*/
        />
        <Route
            path="/login-superuser(/:token)"
            component={Login}
            queries={ViewerQueries}
        />
        <Route
            path="/login-switch(/:token)"
            component={Login}
            queries={ViewerQueries}
        />
        <Route
            path="/logged-in"
            component={LoggedIn}
            queries={ViewerQueries}
            /*updateToken={this.updateToken}*/
        />
        <Route
            path="/me"
            component={Me}
            queries={ViewerQueries}
            /*updateToken={this.updateToken}*/
        />
        <Route
            path="/manage-venue"
            component={ManageVenue}
            queries={ViewerQueries}
            /*updateToken={this.updateToken}*/
        />
        <Route
            path='/logout'
            component={Logout}
            /*updateToken={this.updateToken}*/
        />
        <Route
            path="/mailvalidation&token=:tokenId"
            component={Login}
            queries={ViewerQueries}
            /*updateToken={this.updateToken}*/
        />
        <Route
            path="/resetpassword"
            component={ResetPassword}
            queries={ViewerQueries}
        />
        <Route
            path="/changepassword&token=:tokenId"
            component={ChangePassword}
            queries={ViewerQueries}
        />
        <Route
            path="/profile"
            component={Profile}
            queries={ViewerQueries}
        />
        <Route
            path="/sports-update"
            component={SportsUpdate}
            queries={ViewerQueries}
        />

        <Route
            path="/new-sportunity"
            component={NewSportunity}
            queries={ViewerQueries}
        />
        <Route
            path="/find-sportunity"
            component={FindSportunity}
            queries={ViewerQueries}
        />
        <Route
            path="/find-sportunity(/lat=(:urlLat)&lng=(:urlLng)(/range=(:urlRange)))(/sport=(:urlSportId))(/fromDate=(:urlFromDate)&toDate=(:urlToDate))(/free=(:urlFree))"
            component={FindSportunity}
            queries={ViewerQueries}
        />
        <Route
            path="/venue"
            component={Venue}
            queries={ViewerQueries}
        />
        <Route
            path='/venue-view/:venueId'
            component={VenueView}
            queries={ViewerQueries}
        />
        {/*<Route*/}
            {/*path="/venues"*/}
            {/*component={Venues}*/}
            {/*queries={ViewerQueries}*/}
        {/*/>*/}
        <Route
            path="/sport-organization"
            component={SportOrganization}
            queries={ViewerQueries}
        />
        <Route
            path="/facility/:venueId"
            component={Facility}
            queries={ViewerQueries}
        />
        <Route
            path="/circle/:circleId"
            component={MyCircles}
            queries={ViewerQueries}
        />
      <Route
        path="/circle/:circleId/:activeTab"
        component={MyCircles}
        queries={ViewerQueries}
      />
        <Route
            path="/join-circle/:circleId"
            component={MyCircles}
            queries={ViewerQueries}
        />
        <Route 
            path="/subaccounts-circle-creation"
            component={MyCircles}
            queries={ViewerQueries}
        />
        <Route
            path="/profile-view/:userId/:activeTab"
            component={ProfileView}
            queries={ViewerQueries}
        />
        <Route
          path="/profile-view/:userId/:activeTab/:circleId"
          component={ProfileView}
          queries={ViewerQueries}
        />
        <Route
          path="/profile-view/:userId"
          component={ProfileView}
          queries={ViewerQueries}
        />
        <Route
            path="/event-view/:sportunityId"
            component={EventView}
            queries={ViewerQueries}
        />
        <Route
            path="/event-edit/:sportunityId"
            component={NewSportunity}
            queries={ViewerQueries}
        />
        <Route
            path="/serie-edit/:sportunityId"
            component={NewSportunity}
            queries={ViewerQueries}
        />
        <Route
            path="/event-reorganize/:sportunityId"
            component={NewSportunity}
            queries={ViewerQueries}
        />
        <Route
            path="/sportunity-detail"
            component={SportunityDetail}
        />
        <Route
            path="/my-events"
            component={MyEvents}
            queries={ViewerQueries}
        />
        <Route
            path="/my-info"
            component={MyInfo}
            queries={ViewerQueries}
        />
        <Route
            path="/my-shared-info"
            component={MyInfo}
            queries={ViewerQueries}
        />
        <Route
            path="/my-wallet"
            component={MyInfo}
            queries={ViewerQueries}
        />
        <Route
            path="/preferences"
            component={MyInfo}
            queries={ViewerQueries}
        />
        <Route
            path="/notification-preferences/:token"
            component={MyInfo}
            queries={ViewerQueries}
        />
        <Route
            path="/my-circles"
            component={MyCircles}
            queries={ViewerQueries}
        />
        <Route
            path="/my-circles/all-members"
            component={MyCircles}
            queries={ViewerQueries}
        />
        <Route
            path="/my-circles/members-info"
            component={MyCircles}
            queries={ViewerQueries}
        />
        <Route
            path="/my-circles/payment-models"
            component={MyCircles}
            queries={ViewerQueries}
        />
        <Route
            path="/my-circles/terms-of-uses"
            component={MyCircles}
            queries={ViewerQueries}
        />
        <Route
            path="/find-circles"
            component={MyCircles}
            queries={ViewerQueries}
        />
        <Route
            path="/privacy"
            component={Privacy}
            queries={ViewerQueries}
        />
        <Route
            path="/about-us"
            component={AboutUs}
            queries={ViewerQueries}
        />
        <Route
            path="/contact-us"
            component={AboutUs}
            queries={ViewerQueries}
        />
        <Route
            path="/term"
            component={Term}
            queries={ViewerQueries}
        />
        <Route
            path="/faq/calendar-sync"
            component={FaqCalendarSync}
            queries={ViewerQueries}
        />
        <Route
            path="/faq/calendar-sync/google-calendar"
            component={FaqGoogleCalendarSync}
            queries={ViewerQueries}
        />
        <Route
            path="/faq/calendar-sync/outlook-calendar"
            component={FaqOutlookCalendarSync}
            queries={ViewerQueries}
        />
        <Route
            path="/faq/calendar-sync/apple-calendar"
            component={FaqAppleCalendarSync}
            queries={ViewerQueries}
        />
        <Route
            path="/faq/how-to-follow-organizer"
            component={FaqHowtoFollowOrganiser}
            queries={ViewerQueries}
        />
        <Route
            path="/faq/how-to-modify-profile"
            component={FaqHowtoModifyProfile}
            queries={ViewerQueries}
        />
        <Route
            path="/faq/team-tutorial"
            component={TeamTutorial}
            queries={ViewerQueries}
        />
        <Route
            path="/faq/tutorial"
            component={Tutorial}
            queries={ViewerQueries}
        />
        <Route
            path="/faq-mobile/tutorial"
            component={Tutorial}
            queries={ViewerQueries}
        />
        <Route
            path="/faq/tutorial/:tutorial"
            component={TutorialContent}
            queries={ViewerQueries}
        />
        <Route
            path="/faq-mobile/tutorial/:tutorial"
            component={TutorialContent}
            queries={ViewerQueries}
        />
        <Route
            path="/faq/clubs/share-with-teammates"
            component={ClubsTutorialShareWithTeammates}
            queries={ViewerQueries}
        />
        <Route
            path="/faq/clubs/use-statistics"
            component={ClubsTutorialUseStatistics}
            queries={ViewerQueries}
        />
        <Route
            path="/faq/clubs/manage-a-club"
            component={ManageAClubTutorial}
            queries={ViewerQueries}
        />
        <Route
            path="/faq/individuals-organize-sport-activities"
            component={IndividualOrganizeSportActivities}
            queries={ViewerQueries}
        />
        <Route
            path="/faq/companies-tutorial"
            component={CompaniesTutorial}
            queries={ViewerQueries}
        />
        <Route
            path="/blog"
            component={Blog}
            queries={ViewerQueries}
        />
    </Route>
)


export class AppRoutes extends Component {
    state = {
        isLoading: true,
        token: '',
    };

    componentDidMount() {
        this.getToken();
        setTimeout(() => {
            this.checkIsLoggedIn();
        }, 100)
    }


    getToken = () => {
        let token;
        try {
            token = localStorage.getItem('token');
        } catch (err) {
            token = '';
        }

        if (token && token.length > 0) {
            const networkLayer = new NetworkLayer(
                backendUrlGraphql, {
                headers: {
                    token,
                },
            });
            networkLayer.setToken(token);
            RelayStore.reset(networkLayer);

        } else {
            const networkLayer = new NetworkLayer(backendUrlGraphql, {});
            RelayStore.reset(networkLayer);
        }

        this.setState({
            isLoading: false,
            token,
        });
        // console.log('token:', token)
    };

    checkIsLoggedIn = () => {
        if (this.state.token === '' || !this.state.token) {
            if (window && window.location && 
                (window.location.pathname === '/my-events' ||
                window.location.pathname === "/me" ||
                window.location.pathname === "/manage-venue" || 
                window.location.pathname === "/profile" ||
                window.location.pathname.indexOf("/event-edit") >= 0 ||
                window.location.pathname.indexOf("/serie-edit") >= 0 ||
                window.location.pathname.indexOf("/event-reorganize") >= 0 ||
                window.location.pathname === "/my-info" ||
                window.location.pathname === "/my-shared-info" ||
                window.location.pathname === "/my-wallet" ||
                window.location.pathname === "/preferences" )) {
                browserHistory.push(`/`);
            }
        }
    }

    updateToken = (token, login = false) => {
      if (login || !token) {
        updateSuperToken(token)
        updateUserToken(token)
      }
      updateGlobalToken(token)

      this.setState({
          token,
      });
      Router.refresh;
    };

    scenes = (
        <Route path="/" component={App} >
            <IndexRoute component={Home} queries={ViewerQueries}/>
            {/*<Route*/}
                {/*path="/clubs"*/}
                {/*component={Clubs}*/}
                {/*queries={ViewerQueries}*/}
            {/*/>*/}
            <Route 
                path="/s/:shortUrl"
                component={LoadUrl}
                queries={ViewerQueries}
            />
          <Route
            path="/clubs"
            component={ClubsHome}
            queries={ViewerQueries}
          />
          {/*<Route*/}
            {/*path="/cities"*/}
            {/*component={CitiesHome}*/}
            {/*queries={ViewerQueries}*/}
          {/*/>*/}
          <Route
            path="/venues"
            component={VenuesHome}
            queries={ViewerQueries}
          />
          <Route
            path="/companies"
            component={CompaniesHome}
            queries={ViewerQueries}
          />
            <Route
                path="/register"
                component={Register}
                queries={ViewerQueries}
                updateToken={this.updateToken}
            />
            <Route
                path="/login(/:token)"
                component={Login}
                queries={ViewerQueries}
                updateToken={this.updateToken}
            />
            <Route
                path="/login-superuser(/:token)"
                component={Login}
                queries={ViewerQueries}
                updateToken={this.updateToken}
            />
            <Route
                path="/login-switch(/:token)"
                component={Login}
                queries={ViewerQueries}
                updateToken={this.updateToken}
            />
            <Route
                path="/logged-in"
                component={LoggedIn}
                queries={ViewerQueries}
                updateToken={this.updateToken}
            />
            <Route
                path="/me"
                component={Me}
                queries={ViewerQueries}
                updateToken={this.updateToken}
            />
            <Route
                path="/manage-venue"
                component={ManageVenue}
                queries={ViewerQueries}
                updateToken={this.updateToken}
            />
            <Route
                path='/logout'
                component={Logout}
                updateToken={this.updateToken}
            />
            <Route
                path="/mailvalidation&token=:tokenId"
                component={Login}
                queries={ViewerQueries}
                updateToken={this.updateToken}
            />
            <Route
                path="/resetpassword"
                component={ResetPassword}
                queries={ViewerQueries}
            />
            <Route
                path="/changepassword&token=:tokenId"
                component={ChangePassword}
                queries={ViewerQueries}
            />
            <Route
                path="/profile"
                component={Profile}
                queries={ViewerQueries}
            />
            <Route
                path="/sports-update"
                component={SportsUpdate}
                queries={ViewerQueries}
            />

            <Route
                path="/new-sportunity"
                component={NewSportunity}
                queries={ViewerQueries}
            />
            <Route
                path="/find-sportunity"
                component={FindSportunity}
                queries={ViewerQueries}
            />
            <Route
                path="/find-sportunity(/lat=(:urlLat)&lng=(:urlLng)(/range=(:urlRange)))(/sport=(:urlSportId))(/fromDate=(:urlFromDate)&toDate=(:urlToDate))(/free=(:urlFree))"
                component={FindSportunity}
                queries={ViewerQueries}
            />
            <Route
                path="/venue"
                component={Venue}
                queries={ViewerQueries}
            />
            <Route
                path='/venue-view/:venueId'
                component={VenueView}
                queries={ViewerQueries}
            />
            {/*<Route*/}
                {/*path="/venues"*/}
                {/*component={Venues}*/}
                {/*queries={ViewerQueries}*/}
            {/*/>*/}
            <Route
                path="/sport-organization"
                component={SportOrganization}
                queries={ViewerQueries}
            />
            <Route
                path="/facility/:venueId"
                component={Facility}
                queries={ViewerQueries}
            />
          <Route
            path="/circle/:circleId"
            component={MyCircles}
            queries={ViewerQueries}
          />
          <Route
            path="/circle/:circleId/:activeTab"
            component={MyCircles}
            queries={ViewerQueries}
          />
            <Route
                path="/join-circle/:circleId"
                component={MyCircles}
                queries={ViewerQueries}
            />
            <Route 
                path="/subaccounts-circle-creation"
                component={MyCircles}
                queries={ViewerQueries}
            />
            <Route
                path="/profile-view/:userId/:activeTab"
                component={ProfileView}
                queries={ViewerQueries}
            />
            <Route
              path="/profile-view/:userId/:activeTab/:circleId"
              component={ProfileView}
              queries={ViewerQueries}
            />
            <Route
              path="/profile-view/:userId"
              component={ProfileView}
              queries={ViewerQueries}
            />
            <Route
                path="/event-view/:sportunityId"
                component={EventView}
                queries={ViewerQueries}
            />
            <Route
                path="/event-edit/:sportunityId"
                component={NewSportunity}
                queries={ViewerQueries}
            />
            <Route
                path="/serie-edit/:sportunityId"
                component={NewSportunity}
                queries={ViewerQueries}
            />
            <Route
                path="/event-reorganize/:sportunityId"
                component={NewSportunity}
                queries={ViewerQueries}
            />
            <Route
                path="/sportunity-detail"
                component={SportunityDetail}
            />
            <Route
                path="/my-events"
                component={MyEvents}
                queries={ViewerQueries}
            />
            <Route
                path="/my-info"
                component={MyInfo}
                queries={ViewerQueries}
            />
            <Route
                path="/my-shared-info"
                component={MyInfo}
                queries={ViewerQueries}
            />
            <Route
                path="/my-wallet"
                component={MyInfo}
                queries={ViewerQueries}
            />
            <Route
                path="/preferences"
                component={MyInfo}
                queries={ViewerQueries}
            />
            <Route
                path="/notification-preferences/:token"
                component={MyInfo}
                queries={ViewerQueries}
                updateToken={this.updateToken}
            />
          <Route
            path="/my-circles"
            component={MyCircles}
            queries={ViewerQueries}
          />
          <Route
            path="/my-circles/all-members"
            component={MyCircles}
            queries={ViewerQueries}
          />
          <Route
            path="/my-circles/members-info"
            component={MyCircles}
            queries={ViewerQueries}
          />
          <Route
            path="/my-circles/payment-models"
            component={MyCircles}
            queries={ViewerQueries}
          />
          <Route
            path="/my-circles/terms-of-uses"
            component={MyCircles}
            queries={ViewerQueries}
          />
          <Route
            path="/find-circles"
            component={MyCircles}
            queries={ViewerQueries}
          />
            <Route
                path="/privacy"
                component={Privacy}
                queries={ViewerQueries}
            />
            <Route
                path="/about-us"
                component={AboutUs}
                queries={ViewerQueries}
            />
            <Route
                path="/contact-us"
                component={AboutUs}
                queries={ViewerQueries}
            />
            <Route
                path="/term"
                component={Term}
                queries={ViewerQueries}
            />
            <Route
                path="/faq/calendar-sync"
                component={FaqCalendarSync}
                queries={ViewerQueries}
            />
            <Route
                path="/faq/calendar-sync/google-calendar"
                component={FaqGoogleCalendarSync}
                queries={ViewerQueries}
            />
            <Route
                path="/faq/calendar-sync/outlook-calendar"
                component={FaqOutlookCalendarSync}
                queries={ViewerQueries}
            />
            <Route
                path="/faq/calendar-sync/apple-calendar"
                component={FaqAppleCalendarSync}
                queries={ViewerQueries}
            />
            <Route
                path="/faq/how-to-follow-organizer"
                component={FaqHowtoFollowOrganiser}
                queries={ViewerQueries}
            />
            <Route
                path="/faq/how-to-modify-profile"
                component={FaqHowtoModifyProfile}
                queries={ViewerQueries}
            />
            <Route
                path="/faq/team-tutorial"
                component={TeamTutorial}
                queries={ViewerQueries}
            />
            <Route
                path="/faq/tutorial"
                component={Tutorial}
                queries={ViewerQueries}
            />
            <Route
                path="/faq-mobile/tutorial"
                component={Tutorial}
                queries={ViewerQueries}
            />
            <Route
                path="/faq/tutorial/:tutorial"
                component={TutorialContent}
                queries={ViewerQueries}
            />
            <Route
                path="/faq-mobile/tutorial/:tutorial"
                component={TutorialContent}
                queries={ViewerQueries}
            />
            <Route
                path="/faq/clubs/share-with-teammates"
                component={ClubsTutorialShareWithTeammates}
                queries={ViewerQueries}
            />
            <Route
                path="/faq/clubs/use-statistics"
                component={ClubsTutorialUseStatistics}
                queries={ViewerQueries}
            />
            <Route
                path="/faq/clubs/manage-a-club"
                component={ManageAClubTutorial}
                queries={ViewerQueries}
            />
            <Route
                path="/faq/individuals-organize-sport-activities"
                component={IndividualOrganizeSportActivities}
                queries={ViewerQueries}
            />
            <Route
                path="/faq/companies-tutorial"
                component={CompaniesTutorial}
                queries={ViewerQueries}
            />
            <Route
                path="/blog"
                component={Blog}
                queries={ViewerQueries}
            />
        </Route>
    )

    _handleUpdate = () => {
        ReactGA.set({ page: window.location.pathname });
        ReactGA.pageview(window.location.pathname);
        window.scrollTo(0, 0) ;
    }


    render() {
        const { environment } = this.props;
        return (
            <Provider store={store}>
                <StyleRoot>
                <Router
                    history={browserHistory}
                    render={applyRouterMiddleware(useRelay)}
                    environment={RelayStore._env}
                    routes={this.scenes}
                    onUpdate={this._handleUpdate}
                />
                </StyleRoot>
            </Provider>
        )
    }
};
