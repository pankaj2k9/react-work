import * as types from '../actions/actionTypes.js';

import { merge, cloneDeep } from 'lodash';

const defaultState = {
  filter: ['Organized', 'Booked', 'Invited', 'CoOrganizer', 'AskedCoOrganizer', 'Cancelled'],
  userFilter: [],
  sportunityTypeFilter: [],
  organizersFilter: [],
  opponentsFilter: [],
  selectedFilters: [],
  selectedClubs: [],
  hasFilterChanged: false,
};

export default function(state = defaultState, action) {
  switch (action.type) {
    case types.UPDATE_MY_EVENT_FILTER:
      return {
        ...state,
        filter: action.value
      }
    
    case types.UPDATE_MY_EVENT_RESET_FILTER:
      return {
        ...defaultState,
      };

    case types.UPDATE_MY_EVENT_USER_FILTER: 
      return {
        ...state,
        userFilter: action.value
      }
    
    case types.UPDATE_MY_EVENT_SPORTUNITY_TYPE_FILTER: 
      return {
        ...state,
        sportunityTypeFilter: action.value
      }

    case types.UPDATE_MY_EVENT_ORGANIZERS_FILTER:
      return {
        ...state,
	      organizersFilter: action.value
      }

    case types.UPDATE_MY_EVENT_OPPONENTS_FILTER:
      return {
        ...state, 
        opponentsFilter: action.value
      }

    case types.UPDATE_MY_EVENT_FILTER_HAS_CHANGED: 
      return {
        ...state,
        hasFilterChanged: action.value
      }

    case types.UPDATE_MY_EVENT_SELECTED_FILTERS:
      return {
        ...state,
        selectedFilters: action.value
      }

    case types.UPDATE_MY_EVENT_SELECTED_CLUBS_ADD: {
      return merge(
        {}, 
        state, 
        {
          selectedClubs: [
            ...state.selectedClubs, 
            action.selectedClub
          ] 
        }
      );
    }
    case types.UPDATE_MY_EVENT_SELECTED_CLUBS_REMOVE: {
      const { unselectedClub } = action;
      let newState = cloneDeep(state);
      let optionIndex = newState.selectedClubs && newState.selectedClubs.findIndex(selectedClub => unselectedClub.id === selectedClub.id) ;    
      newState.selectedClubs.splice(optionIndex, 1);
      return newState
    }
    case types.UPDATE_MY_EVENT_SELECTED_CLUBS_CLEAR: {
      let newState = cloneDeep(state);
      newState.selectedClubs = [];
      return newState;
    }

    default: return state;
  }
}


