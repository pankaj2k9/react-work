import React, { Component } from 'react';
import Relay from 'react-relay';
import debounce from 'lodash.debounce'
import Radium from 'radium'
import Helmet from 'react-helmet'
import { Link } from 'react-router'
import ReactPixel from 'react-facebook-pixel'

import Header from '../common/Header/Header.js'
import Footer from '../common/Footer/Footer.js'
import Loading from '../common/Loading/Loading.js'
import HeaderImage from './HeaderImage';
import TagBox from './TagBox';
//import VideoArea from './VideoArea';
import localizations from '../Localizations'
import constants from "../../../constants";
import colors from './../../theme/colors'
import FacebookProvider, { CustomChat } from 'react-facebook'
import Features from './Features'
import Ref from './Ref'

let styles;

const RLink = Radium(Link)

class ClubsHome extends Component {
	constructor(props){
		super(props)
		this.state = {
			sportFilter: '',
			locationFilter: '',
      loading: true,
      language: localizations.getLanguage(),
		}
		this._onDebounceSportFilterChange = debounce(this._onDebounceSportFilterChange, 400);
		this._onDebounceLocationFilterChange = debounce(this._onDebounceLocationFilterChange, 400);
	}

	_onDebounceSportFilterChange = (e) => {
		this.setState({ sportFilter: e.target.value })
	}

	_onDebounceLocationFilterChange = (e) => {
		this.setState({ locationFilter: e.target.value })
	}

	_onSportFilterChange = (e) => {
    e.persist();
    this._onDebounceSportFilterChange(e);
  }

	_onLocationFilterChange = (e) => {
    e.persist();
    this._onDebounceLocationFilterChange(e);
  }

  _resetState = (language) => {
    this.setState({ language:language })
  }

  componentDidMount = () => {
    setTimeout(() => this.setState({ loading: false }), 500)
    ReactPixel.pageView();
  }

  renderMetaTags = () => {
    return <Helmet>
              <title>{localizations.meta_title_clubs}</title>
              <meta name="description" content={localizations.meta_desc_clubs}/>
              <meta property="fb:app_id" content="1759806787601548"/>
              <meta property="og:type" content="website"/>
              <meta property="og:title" content={localizations.meta_title_clubs} />
              <meta property="og:description" content={localizations.meta_desc_clubs}/>
              <meta property="og:url" content={constants.appUrl}/>
              <meta property="og:image" content={constants.appUrl+"/assets/images/logo-blue@3x.png"} />
              <meta property="og:image:width" content="225"/>
              <meta property="og:image:height" content="270"/>
          </Helmet>
  }

  render() {
    const { viewer } = this.props

    return (
      <div style={styles.container}>
        {this.state.loading && <Loading />}
        {this.renderMetaTags()}

				<FacebookProvider
					appId='1759806787601548'
					language={localizations.getLanguage()}
				>
          <CustomChat
						pageId="1785262331755411"
						minimized={false}
					/>
        </FacebookProvider>

        { viewer && viewer.me ?
          <Header viewer={viewer} user={viewer.me} {...this.state} /> :
          <Header viewer={viewer ? viewer : null} user={null} {...this.state} /> }
        <HeaderImage viewer={viewer ? viewer : null}
                  onSportFilterChange={this._onSportFilterChange}
                  onLocationFilterChange={this._onLocationFilterChange}
                  {...this.state}/>
        <div style={styles.download_icons_footer}>
          <div style={{width:'50%'}}>
              <a target="_blank" href={constants.appLinkAppStore}>
                <img style={{width:"75%"}} src="/assets/images/icon_appstore.png"/>
              </a>
            </div>
            <div style={{width:'50%'}}>
              <a target="_blank" href={constants.appLinkPlayStore}>
                <img style={{width:"75%"}} src="/assets/images/icon_playstore.png"/>
              </a>
            </div>
        </div>
        <TagBox  {...this.state}/>
        <div style={styles.headerDiscovery}>
          {localizations.home_features_title}
        </div>
        <Features {...this.state}/>
        
        <div style={styles.text}>
          {localizations.home_understandAs}
          <RLink to='/' style={{color: '#5e9fdf', textDecoration: 'none'}}>
            {localizations.home_understandAsIndividual}
          </RLink>,
          <RLink to='/companies' style={{color: "#ed5816", textDecoration: 'none'}}>
            {localizations.home_understandAsCompany + ' '}
          </RLink>
          <RLink>
            {localizations.login_or}
          </RLink>
          <RLink to='/venues' style={{color: "#2aad6c", textDecoration: 'none'}}>
            {localizations.home_understandAsInfrastructure}
          </RLink>
          {/*<RLink style={{color: "#ce2e83", textDecoration: 'none'}}>
            {localizations.home_understandAsCity}
          </RLink>*/}
        </div>
        <Ref/>
        <div style={styles.youAreContainer}>
          <p style={styles.youAreTitle}>
            {localizations.home_youAre}
          </p>
          <p>
            <RLink key={'1'} to='/' style={{...styles.youAreLink, borderRight: '1px solid white'}}>
              {localizations.home_particuliers}
            </RLink>
            <RLink key={'2'} to='/clubs' style={{...styles.youAreLink, borderRight: '1px solid white'}}>
              {localizations.home_club}
            </RLink>
            <RLink key={'3'} to='/companies' style={{...styles.youAreLink, borderRight: '1px solid white'}}>
              {localizations.home_enterprise}
            </RLink>
            <RLink key={'4'} to='/venues' style={styles.youAreLink}>
              {localizations.home_facility}
            </RLink>
          </p>
        </div>
        {viewer && viewer.me ?
          <Footer onUpdateLanguage={this._resetState} viewer={viewer} user={viewer.me}/> :
          <Footer onUpdateLanguage={this._resetState} viewer={viewer ? viewer : null} user={null}/> }
      </div>

    );
  }
}

export default Relay.createContainer(Radium(ClubsHome), {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        me {
          ${Header.getFragment('user')}
          ${Footer.getFragment('user')}
          id
        }
        ${HeaderImage.getFragment('viewer')},
        ${Header.getFragment('viewer')},
        ${Footer.getFragment('viewer')}
      }
    `
  },
});

styles = {
  youAreContainer: {
    backgroundColor: '#504596',
    padding: 20,
    display: 'flex',
    flexDirection: 'column',
    textAlign: 'center',
  },
  youAreTitle: {
    color: colors.white,
    fontFamily: 'Lato',
    fontSize: '16px',
    fontWeight: 'bold',
    margin: '5px auto 15px auto',
    '@media (max-width: 600px)': {
      fontSize: '12px',
    },
  },
  youAreLink: {
    color: colors.white,
    textDecoration: 'none',
    fontFamily: 'Lato',
    fontSize: '16px',
    fontWeight: 'bold',
    cursor: 'pointer' ,
    padding: '0px 15px',
    ':hover': {
      color: colors.lightGray
    },
    '@media (max-width: 600px)': {
      fontSize: '14px',
    },
  },
  text: {
    // width: '160px',
    fontFamily: 'Lato',
    fontSize: '24px',
    fontWeight: '500',
    textAlign: 'center',
    color: 'rgba(0,0,0,0.65)',
    marginBottom: 30,
    '@media (max-width: 600px)': {
      fontSize: '22px',
    },
  },
  headerDiscovery: {
    textAlign: 'center',
    fontFamily: 'lato',
    margin: '20px 0px',
    padding: 10,
    fontSize: 26,
    fontWeight: 'bold',
    '@media (max-width: 1280px)': {
      fontSize: 24,
    },
    '@media (max-width: 978px)': {
      fontSize: 22,
    },
    '@media (max-width: 768px)': {
      fontSize: 20,
    },
  },
  container: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  download_icons:{
    display: 'none',
    '@media (max-width: 480px)': {
      display: 'block',
      textAlign: 'center',
      paddingTop: 7,
      paddingBottom: 7
    },
  },
  download_icons_text: {
    fontSize: 14,
    fontFamily: 'Lato',
  },
  download_icons_footer: {
    display: 'flex',
    justifyContent:'center',
    flexDirection:'row',
    maxWidth: 500,
    margin: '-75px auto 14px auto',
    textAlign: 'center',
    width: '100%',
    zIndex: 100,
  },
  button: {
    height: '55px',
    borderRadius: '100px',
    backgroundColor: colors.blue,
    margin: '40px auto',
    maxWidth: '360px',
    paddingRight: 40,
    paddingLeft: 40,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    textDecoration: 'none',
    fontSize: 25,
    fontFamily: 'Lato',
    color: '#fff',
    cursor: 'pointer',
    '@media (max-width: 850px)': {
      borderRadius: '100px 100px 100px 100px',
    },
  },
}
