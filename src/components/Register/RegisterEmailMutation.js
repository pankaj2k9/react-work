import Relay from 'react-relay';

export default class RegisterEmailMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      newUser
    }`;
  }

  getVariables() {
    return {
      superUserToken: this.props.superUserTokenVar,
      user: {
        avatar: this.props.avatarVar,
        pseudo: this.props.pseudoVar,
        email: this.props.emailVar,
        password: this.props.passwordVar,
        phoneNumber: this.props.phoneVar,
        description: this.props.descriptionVar,
        birthday: this.props.birthdayVar,
        sex: this.props.sexVar,
        profileType: this.props.profileTypeVar,
        appLanguage: this.props.appLanguageVar,
        appCurrency: this.props.appCurrencyVar,
        appCountry: this.props.appCountryVar,
	      subAccountsPseudoList: this.props.subAccountsPseudoListVar
      },
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on newUserPayload {
        clientMutationId,
        user,
        viewer
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'REQUIRED_CHILDREN',
      children: [
        Relay.QL`
          fragment on newUserPayload {
            clientMutationId,
            viewer,
            user
          }
        `,
      ],
    }];
  }
}
