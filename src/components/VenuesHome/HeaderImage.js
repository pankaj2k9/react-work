import React, { Component } from 'react';
import Radium from 'radium'
import Relay from 'react-relay'
import localizations from '../Localizations'
import ModalVideo from 'react-modal-video'

import { colors } from '../../theme';
import {browserHistory, Link} from "react-router";
import AlertContainer from 'react-alert';
import * as types from "../../actions/actionTypes";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
let styles;

class HeaderImage extends Component {
  constructor () {
    super();
    this.state = {
      isOpen: false
    }
  }

  handleClick = (canCreate) => {
    if (canCreate)
    {
      this.props._updateRegisterFromAction('profileType/ORGANIZATION');
      browserHistory.push({
        pathname : '/contact-us'
      })
    }
    else
      this.msg.show(localizations.home_alreadyConnect, {
        time: 2000,
        type: 'error',
      });
  };

  render() {
    const { viewer } = this.props
    return (
      <div style={styles.container}>
        <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
        <div style={styles.containerFilter}>
          <p style={styles.headingTextMaj}>
            {localizations.homeVenue_headerTextMaj}
          </p>
          <p style={styles.headingTextMin}>
            {localizations.homeVenue_headerTextMin}
          </p>
          <ModalVideo
            channel='youtube'
            isOpen={this.state.isOpen}
            videoId='oIXeyNbqx_w'
            onClose={() => this.setState({isOpen: false})}
            youtube={{fs:0, autoplay: 1, cc_load_policy: 1, controls: 1 }}
          />
          <span onClick={() => {this.setState({isOpen: true})}} style={styles.playButton} key={'playButton'}>
            <i style={styles.playIcon} className="fa fa-play fa-2x" />
          </span>
          <div style={styles.containerButton} key={'searchButton'}>
            { !viewer.me ?
              <Link style={styles.searchButton} onClick={() => this.handleClick(true)}>
                <p style={styles.inputSearch}>{localizations.homeVenue_createAccount}</p>
              </Link>
              :
              <Link style={styles.searchButton} onClick={() => this.handleClick(true)}>
                <p style={styles.inputSearch}>{localizations.homeVenue_createAccount}</p>
              </Link>
            }
          </div>
        </div>
        {/*<HeaderImageNavigator {...this.props} viewer={viewer} />*/}
      </div>
    );
  }
}


styles = {
  containerButton: {
    height: '60px',
    boxShadow: '0 0 6px 0 rgba(0,0,0,0.5)',
    borderRadius: '10px',
    // zIndex: '1',
    marginTop: 100,
    position: 'absolute',
    top: '270px',
    justifyContent: 'flex-end',
    '@media (max-width: 610px)': {
      display: 'block',
      top: 320
    },
    transition: 'all cubic-bezier(0.22,0.61,0.36,1) .3s',
    ':hover': {
      transform:'scale(1.05)',
      boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)'
    }
  },
  containerFilter: {
    height: '100%',
    backgroundSize: 'cover',
    backgroundColor: '#00000044',
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: '25px 80px 150px 80px',
    '@media only screen and (max-width : 850px)': {
      padding: '25px 25px 150px 25px',
    },
  },
  container: {
    width: '100%',
    height: '410px',
    // opacity: 0.9,
    backgroundImage: 'url("assets/images/background-venue.jpg")',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    '@media (max-width: 320px)': {
      height: '210px',
    },
    '@media only screen and (max-width : 850px)': {
      height: 480,
    },
    '@media only screen and (min-width : 1530px)': {
      height: '600px',
      backgroundPosition: 'center -250px',
    },
    '@media only screen and (max-width : 1980px)': {
      height: 600,
    },
  },
  headingTextMaj: {
    // width: '36%',
    fontFamily: 'Lato',
    fontSize: '36px',
    lineHeight: '40px',
    color: colors.white,
    fontWeight: 'bold',
    // position: 'absolute',
    // top: '15px',
    // left: '19%',
    textAlign: 'center',
    '@media (max-width: 1280px)': {
      fontSize: '34px',
    },
    '@media (max-width: 978px)': {
      fontSize: '32px',
      // width: '50%',
    },
    '@media (max-width: 768px)': {
      // width: '80%',
      fontSize: '26px',
      lineHeight: '36px',
      // left: '5%',
    },
    '@media (max-width: 425px)': {
      // width: '80%',
      fontSize: '22px',
      lineHeight: '30px',
      // left: '5%',
    },
  },
  headingTextMin: {
    // width: '36%',
    fontFamily: 'Lato',
    fontSize: '28px',
    lineHeight: '52px',
    color: colors.white,
    // position: 'absolute',
    // top: '15px',
    // left: '19%',
    textAlign: 'center',
    '@media (max-width: 1280px)': {
      fontSize: '24px',
    },
    '@media (max-width: 978px)': {
      fontSize: '22px',
      // width: '50%',
    },
    '@media (max-width: 768px)': {
      // width: '80%',
      fontSize: '18px',
      lineHeight: '44px',
      // left: '5%',
    },
    '@media (max-width: 425px)': {
      // width: '80%',
      fontSize: '14px',
      lineHeight: '30px',
      // left: '5%',
    },
  },
  inputSearch: {
    fontFamily: 'Lato',
    fontSize: '18px',
    display: 'inline',
    padding: '5px 20px',
    color: colors.white,
    '@media (max-width: 425px)': {
      fontSize: '16px',
    }
  },
  playButton: {
    cursor: 'pointer',
    fontSize: '15px',
    height: 70,
    width: 70,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
    backgroundColor: 'rgba(25, 25, 25, 0.95)',
    color: colors.white,
    transition: 'all cubic-bezier(0.22,0.61,0.36,1) .3s',
    opacity: 0.9,
    marginTop: 70,
    ':hover': {
      opacity: 1,
      transform:'scale(1.1)',
      boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)'
    },
    '@media (max-width: 850px)': {
      // marginTop: 50
    }
  },
  playIcon: {
    marginLeft: 4
  },
  searchButton: {
    height: '100%',
    backgroundColor: '#2fac67',
    boxSizing: 'border-box',
    paddingRight: 10,
    paddingLeft: 10,
    borderRadius: 10,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    textDecoration: 'none',
    cursor: 'pointer'
  },
};


const _updateRegisterFromAction = (text) => ({
  type: types.UPDATE_REGISTER_FROM,
  text
})

const stateToProps = (state) => ({
  createProfileFrom: state.registerReducer.createProfileFrom,
})

const dispatchToProps = (dispatch) => ({
  _updateRegisterFromAction: bindActionCreators(_updateRegisterFromAction, dispatch),
})

const ReduxContainer = connect(stateToProps,dispatchToProps)(Radium(HeaderImage));

export default Relay.createContainer(Radium(ReduxContainer), {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        me
      }
    `,
  },
});
