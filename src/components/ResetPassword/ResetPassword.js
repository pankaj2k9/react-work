import React, { Component } from 'react';
import Relay from 'react-relay';
import Logo from './Logo.js';
import Submit from './Submit.js';
import { colors } from '../../theme'
import Inputs from './Inputs'
import { Link } from 'react-router'
import AlertContainer from 'react-alert'

import localizations from '../Localizations'
import ChangePasswordMutation from './ChangePasswordMutation';

let styles;

class ResetPassword extends Component {
  constructor() {
    super();
    this.state = {
      email: '',
    }

    this.alertOptions = {
      offset: 14,
      position: 'top right',
      theme: 'light',
      transition: 'fade',
      time: 2000,
    };
  }

  _onChangeEmail(email) {
    this.setState({ email });
  }

  _onSubmit() {
    
    if (this.state.email) {
      this.props.relay.commitUpdate(
          new ChangePasswordMutation({
            email: this.state.email,
            viewer: this.props.viewer,
          }),
          {
            onSuccess: () => {
              this.msg.show(localizations.popup_resetPassword_success, {
                  time: 2000,
                  type: 'success',
                });
              
            },
            onFailure: (error) => {
              this.msg.show(error.getError().source.errors[0].message, {
                time: 2000,
                type: 'error',
              });
            },
          }
        );
    }
    else {
      this.msg.show(localizations.popup_resetPassword_required_fields, {
        time: 2000,
        type: 'error',
      });      
    }
    setTimeout(() => {
      this.msg.removeAll();
    }, 2000);
  }

  render() {
    return (
      <div style={styles.container}>
        
        <div style={styles.signup}>
          {localizations.login_dontHaveAccount} <Link to='/register' style={styles.link}>{localizations.login_joinUs}</Link>
        </div>
        <div style={styles.modal}>
          <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
          <Logo text={localizations.passwordReset}/>
          <Inputs 
            email={this.state.email}
            onChangeEmail={this._onChangeEmail.bind(this)} 
          />
          <Submit text={localizations.sendMeEmail} onSubmit={this._onSubmit.bind(this)} />
        </div>
      </div>
    );
  }
}

export default Relay.createContainer(ResetPassword, {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
        ${ChangePasswordMutation.getFragment('viewer')}
      }
    `,
  },
});

styles = {
  separator: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 16,
    marginTop: 10,
    marginBottom: 10,
  },
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    paddingTop: 63,
    paddingBottom: 63,


    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    width: '100vw',
    minHeight: '100vh',

    backgroundColor: colors.black,
    fontFamily: 'Lato',

    backgroundImage: 'url(assets/images/background-signup.jpg',

  },
  modal: {
    position: 'relative',
    margin: 'auto',
    width: 480,
    display: 'flex',
    flexDirection: 'column',
    padding: 30,
    borderRadius: 16,
    backgroundColor: 'rgba(255,255,255,0.8)',
  },
  signup: {
    position: 'absolute',
    right: 40,
    top: 30,
    color: colors.gray,
    fontFamily: 'Lato',
    fontSize: 18,
  },
  link: {
    color: colors.gray,
    fontFamily: 'Lato',
    fontSize: 18,
    textTransform: 'none',
  },
}
