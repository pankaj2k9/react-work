import React, { Component } from 'react';
import Relay from 'react-relay'
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as types from '../../actions/actionTypes.js';

class LoggedIn extends Component {
  constructor() {
    super();
  }

  componentDidMount = () => {
    const { me } = this.props.viewer;
    this.props._resetAction();
    if (this.props.loggedInFromPage) {
      this.props._loggedFromPage('');
      browserHistory.push(this.props.loggedInFromPage)
    }
    else {//if (me.sex && me.publicAddress && me.sports && me.sports.length > 0 ) {
      this.props._updateIsProfilFromLogin(false)
      
      if (me.homePagePreference === 'ORGANIZED')
        browserHistory.push(`/my-events`)
      else
        browserHistory.push(`/find-sportunity`)
    }
    //else {
    //  browserHistory.push(`/profile`)
    //}
   
  }
  render = () => {
    return (null)
  }
}

// REDUX
const _updateIsProfilFromLogin = (value) => ({
  type: types.UPDATE_IS_PROFILE_FROM_LOGIN,
  value,
})

const _loggedFromPage = (value) => ({
  type: types.LOGGED_IN_FROM_PAGE,
  value,
})

const _resetAction = () => ({
  type: types.UPDATE_MY_EVENT_RESET_FILTER,
})

const dispatchToProps = (dispatch) => ({
  _updateIsProfilFromLogin: bindActionCreators(_updateIsProfilFromLogin, dispatch),
  _loggedFromPage: bindActionCreators(_loggedFromPage, dispatch),
  _resetAction: bindActionCreators(_resetAction, dispatch)
})

const stateToProps = (state) => ({
  loggedInFromPage: state.loginReducer.loggedInFromPage,
})

let ReduxContainer = connect(
  stateToProps,
  dispatchToProps
)(LoggedIn);

export default Relay.createContainer(ReduxContainer, {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
        me {
          id
          homePagePreference
          sex
          publicAddress {
            city
            country
          }
          sports {
            sport {
              id
            }
          }
        }
      }
    `,
  },
});