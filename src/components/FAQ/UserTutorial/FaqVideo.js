import React, { Component } from 'react';
import styles from './styles';
import Youtube from 'react-youtube';
import Radium from 'radium';
var Style = Radium.Style;


const FaqVideo = (props) => {
  const options = {
    height: '230',
    width: '460',
    playerVars: {
      autoplay: props.autoplay || 0,
      cc_load_policy: 1,
      controls: 1,
      start: props.start || 0,
    }
  }

  return (
    <div id="video-container" style={styles.videoContainer}>
      <Style
        scopeSelector="#video-container iframe"
        rules={styles.iframe}
      />
      <Youtube
        videoId={props.videoId}
        opts={options}
      />
    </div>
  );
};

export default FaqVideo;
