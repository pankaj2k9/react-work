import React, { Component } from 'react';
import Relay from 'react-relay';

import Header from '../../common/Header/Header.js'
import Footer from '../../common/Footer/Footer'
import Loading from '../../common/Loading/Loading'
import localizations from '../../Localizations'

import Radium from 'radium';
import styles from './styles';


class CalendarSync extends Component {
  constructor(props) {
    super(props)
    this.state = {
      language: localizations.getLanguage(),
    }
  }

  _setLanguage = (language) => {
    this.setState({ language: language })
  }
  
  render() {
    const { viewer } = this.props

    return (
      <div style={styles.container}>
        {
          viewer.me ? <Header user={viewer.me} viewer={viewer} {...this.state} /> : <Header user={null} {...this.state} />
        }
        <div style={styles.content}>
            <div style={styles.title}>
                {localizations.faq_calendar_sync_title}
            </div>
            <div style={styles.body}>
                <div style={styles.sectionTitle}>
                    {localizations.faq_calendar_sync_subtitle}
                </div>
                <div style={styles.sectionContent}>
                    <div style={styles.paragraph}>
                        <div style={styles.sectionSubTitle}>
                            {localizations.faq_calendar_sync_ical_section_title}
                        </div>
                        <span>{localizations.faq_calendar_sync_ical_section_text}</span>
                        <img style={styles.image} src="/assets/images/faq/calendar-sync/ical-1.jpg"/>
                    </div>
                </div>
            </div>
        </div>
        
        {viewer.me 
            ? <Footer onUpdateLanguage={this._setLanguage} viewer={viewer} user={viewer.me}/> 
            : <Footer onUpdateLanguage={this._setLanguage} viewer={viewer} user={null}/> 
        }
      </div>
      
    );
  }
}

export default Relay.createContainer(Radium(CalendarSync), {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        ${Header.getFragment('viewer')}
        ${Footer.getFragment('viewer')}
        id
        me {
          id
          pseudo          
        }
      }
    `,
  },
});


