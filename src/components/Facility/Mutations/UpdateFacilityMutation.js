import Relay from 'react-relay';

export default class UpdateInfrastructureMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
         updateInfrastructure
      }`
  }
  
  getVariables() {
    return  {
        infrastructure: {
            id: this.props.facilityIdVar, 
            name: this.props.facilityNameVar,
            sport: this.props.sportsVar,
            authorized_managers: this.props.authorizedManagersVar
            //logo: this.props.photoVar,
        },
    }
  }

  getFiles() {
    return {
      avatars: this.props.photoVar,
    };
  }

  getFatQuery() {
    return Relay.QL`
        fragment on updateInfrastructurePayload {
          clientMutationId,
          viewer {
            id
            venue {
              infrastructures
            }
          }
        }
      `
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
        fieldIDs: {
          viewer: this.props.viewer.id,
        },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        me {
          id
        }
        venue
        
      }
    `,
  };

}
