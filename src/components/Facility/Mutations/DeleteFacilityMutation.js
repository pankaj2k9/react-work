import Relay from 'react-relay';

export default class DeleteInfrastructureMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
         deleteInfrastructure
      }`
  }
  
  getVariables() {
    return  {
        infrastructureId: this.props.facilityIdVar, 
    }
  }

  getFatQuery() {
    return Relay.QL`
        fragment on deleteInfrastructurePayload {
          clientMutationId,
          viewer {
            id,
            me
            venue {
              infrastructures
            }
          }
        }
      `
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
        fieldIDs: {
          viewer: this.props.viewer.id,
        },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        me {
          id
        }
        venue
        
      }
    `,
  };

}
