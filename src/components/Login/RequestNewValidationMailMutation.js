import Relay from 'react-relay';
/**
*  mail validation mutation
*/
export default class MailValidationMutation extends Relay.Mutation {
  /**
  *  Mutation
  */
  getMutation() {
    return Relay.QL`mutation Mutation {askNewValidationMail}`;
  }
  /**
  *  Variables
  */
  getVariables() {
    return {
      pseudo: this.props.pseudo,
      email: this.props.email,
    };
  }
  /**
  *  Fat query
  */
  getFatQuery() {
    return Relay.QL`
      fragment on askNewValidationMailPayload {
        viewer{
          id
        }
      }
    `;
  }

   /**
  *  Config
  */
  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        viewer: this.props.viewer.id,
      },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
      }
    `,
  };
}