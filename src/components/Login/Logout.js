import React, { Component } from 'react';
import Relay from 'react-relay';

class Logout extends Component {
    componentWillMount() {
        const { updateToken } = this.props.route;
        if (typeof(updateToken) === 'function')
        updateToken('')
        
        let path = '/';
        this.props.router.push({
            pathname : path,
        })
    }
    render() {        
        return (
            <div></div>
        );
    }
}

export default Relay.createContainer(Logout, {
  fragments: {
  },
});
