require('babel-polyfill');
let regeneratorRuntime =  require("regenerator-runtime");

import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { colors, fonts } from '../../theme'
import FacebookProvider, { Login } from 'react-facebook'
import { browserHistory } from 'react-router'
import AlertContainer from 'react-alert'
import ReactLoading from 'react-loading'
import localizations from '../Localizations'
import * as types from '../../actions/actionTypes.js';

import { backendUrl } from '../../../constants.json';

let styles;

class Facebook extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
    }
    this.alertOptions = {
      offset: 14,
      position: 'top right',
      theme: 'light',
      time: 100,
      transition: 'fade',
    };
  }

  _onFacebookResponse = (data) => {
    this._loginFacebook(data.tokenDetail.accessToken)
  }

  _changeLoadingStatus = (bool) => {
    this.setState({
      isLoading: bool,
    })
  }

  _loginFacebook = async (facebookToken) => {
    this._changeLoadingStatus(true);

    let response;

    try {
      response = await fetch(backendUrl+'/auth/facebook', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          token: facebookToken,
        }),
      })
      .then((response) => response.json())
    } catch (err) {
      this._changeLoadingStatus(false);
      this.msg.show(localizations.popup_login_failed, {
        time: 0,
        type: 'error',
      });
      response = null;
    }

    if (response && response.success) {
      let token = response.token;
      this.props.updateToken(token, true);
      this._changeLoadingStatus(false);
      this.props._updateIsProfilFromLogin(true)
      this.msg.show(localizations.popup_login_success, {
        time: 0,
        type: 'success',
      });
      setTimeout(() => {
        this.props.trackLogin('Login-Facebook')
        browserHistory.push(`/logged-in`)
      }, 1000);

    } else {

      let error = response.msg;
      this.msg.show(error, {
        time: 0,
        type: 'error',
      });
      console.log(error);
      this._changeLoadingStatus(false);
    }
  }

  render () {
    return(
      <div style={styles.container}>
        <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
        {
          this.state.isLoading  &&
            <ReactLoading type='cylon' color={colors.blue} />
        }
        <FacebookProvider appId='1759806787601548'>
          <Login scope="email" onResponse={this._onFacebookResponse}>
            <span style={styles.button}>{localizations.login_loginFacebook}</span>
          </Login>
        </FacebookProvider>
        <span style={styles.note}>
          <i className="fa fa-lock" aria-hidden="true"></i> {localizations.login_facebookNote}</span>
      </div>

    )
  }
}

const _updateIsProfilFromLogin = (value) => ({
  type: types.UPDATE_IS_PROFILE_FROM_LOGIN,
  value,
})

const dispatchToProps = (dispatch) => ({
  _updateIsProfilFromLogin: bindActionCreators(_updateIsProfilFromLogin, dispatch),
})

const stateToProps = (state) => ({
})

export default connect(
  stateToProps,
  dispatchToProps
)(Facebook);

// STYLES //

styles = {
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    backgroundColor: colors.blueFacebook,
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
    borderRadius: '100px',
		display: 'inline-block',
    fontFamily: 'Lato',
    fontWeight: fonts.weight.medium,
    fontSize: '16px',
    textAlign: 'center',
    color: colors.white,
    borderWidth: 0,
    marginTop: 5,
    marginBottom: 5,
    cursor: 'pointer',
		// lineHeight: '27px',
    backgroundImage: 'url(/assets/images/facebook.png)',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'left 27px center',
    paddingTop: 22,
    paddingBottom:22,
    paddingLeft: 60,
    paddingRight: 25
  },
  note: {
    fontSize: 12,
    marginTop: 6,
  },
}
