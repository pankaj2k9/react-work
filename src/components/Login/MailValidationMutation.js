import Relay from 'react-relay';
/**
*  mail validation mutation
*/
export default class MailValidationMutation extends Relay.Mutation {
  /**
  *  Mutation
  */
  getMutation() {
    return Relay.QL`mutation Mutation {mailValidation}`;
  }
  /**
  *  Variables
  */
  getVariables() {
    return {
      token: this.props.token,
    };
  }
  /**
  *  Fat query
  */
  getFatQuery() {
    return Relay.QL`
      fragment on mailValidationPayload {
        viewer{
          id
        }
      }
    `;
  }

   /**
  *  Config
  */
  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        viewer: this.props.viewer.id,
      },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
      }
    `,
  };
}