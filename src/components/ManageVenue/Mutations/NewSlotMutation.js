import Relay from 'react-relay';

export default class NewSlotMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
         newSlot
      }`
  }
  
  getVariables() {
    return  {
        venueId: this.props.venueIDVar,
        infrastructureId: this.props.infrastructureIDVar,
        slot: {
            from: this.props.fromVar,
            end: this.props.endVar,
            price: this.props.priceVar,
            usersSlotIsFor: this.props.usersVar,
            circlesSlotIsFor: this.props.circlesVar, 
            flexible: this.props.flexibleVar
        },
        repetitionNumber: this.props.repetitionNumberVar
    }
  }

  getFatQuery() {
    return Relay.QL`
        fragment on newSlotPayload {
          clientMutationId,
          viewer {
            id,
            venue {
              infrastructures
            }
          }
        }
      `
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
        fieldIDs: {
          viewer: this.props.viewer.id,
        },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        me {
          id
        }
        venue
        
      }
    `,
  };

}
