import React from 'react'
import Relay from 'react-relay'
import NewTimeSlot from './NewTimeSlot'


let styles 

class CalendarSidebar extends React.Component {
  render() {
    return(
      <section style={styles.container}>
        <NewTimeSlot 
          viewer={this.props.viewer} 
          {...this.props} 
          onRefresh={this._refresh} 
        />
      </section>
    )
  }
}

styles = {
  container: {
    width: 220,
    display: 'flex',
    flexDirection: 'column',
    marginLeft:20,
    marginRight: 20,
  },
}

export default Relay.createContainer(CalendarSidebar, {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        ${NewTimeSlot.getFragment('viewer')}
      }
    `,
  },
});