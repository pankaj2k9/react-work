import React, { Component } from 'react';
import Radium from 'radium'
import Relay from 'react-relay'
import localizations from '../Localizations'
import ModalVideo from 'react-modal-video'
import AlertContainer from 'react-alert'

import { colors } from '../../theme';
import {Link} from "react-router";
import metrics from "../../theme/metrics";
let styles;

class HeaderImage extends Component {
  constructor () {
    super();
    this.state = {
      isOpen: false
    }
    this.alertOptions = {
      offset: 60,
      position: 'top right',
      theme: 'light',
      transition: 'fade',
    };
  }

  handleClick = () => {
    this.msg.show(localizations.home_alreadyConnect, {
      time: 2000,
      type: 'error',
    });
  };

  render() {
    const { viewer } = this.props
    return (
      <div style={styles.container}>
        <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
        <div style={styles.containerFilter}>
          <p style={styles.headingText}>
            {localizations.aboutUs_header}
          </p>
          <div style={styles.row}>
            <Link style={styles.box} to='/faq/tutorial'>
              <img src='/assets/images/AboutUs/tutorial-01.png' style={styles.bgIcon}/>
              <h1 style={{fontSize: 26, fontWeight: metrics.border.large, color: '#fdd000'}}>{localizations.aboutUs_aboutUs_title}</h1>
              <p style={{fontSize: 20, margin: 10, textAlign: 'center', color: colors.white}}>{localizations.aboutUs_aboutUs_text}</p>
            </Link>
            <Link style={styles.box} href='mailto://info@sportunity.ch'>
	            <img src='/assets/images/AboutUs/contact-01.png' style={styles.bgIcon}/>
	            <h1 style={{fontSize: 26, fontWeight: metrics.border.large, color: '#fc5d5a'}}>{localizations.aboutUs_contactUs_title}</h1>
              <div style={styles.contactInfo}>
                <img src='/assets/images/AboutUs/swiss.png' style={styles.smIcon}/>
                {localizations.contactUs_phoneNumber}
              </div>
              <div style={styles.contactInfo}>
                <img src='/assets/images/AboutUs/france.png' style={styles.smIcon}/>
                {localizations.contactUs_phoneNumber_FR}
              </div>
              <div style={styles.contactInfo}>
                <img src='/assets/images/AboutUs/mail-01.png' style={styles.smIcon}/>
                {localizations.contactUs_email}
              </div>
            </Link>
          </div>
          <div style={styles.containerButton} key={'searchButton'}>
            { !viewer.me ?
              <Link to='/register' style={styles.searchButton}>
                <p style={styles.inputSearch}>{localizations.home_createAccount}</p>
              </Link>
              :
              <Link style={styles.searchButton} onClick={this.handleClick}>
                <p style={styles.inputSearch}>{localizations.home_createAccount}</p>
              </Link>
            }
          </div>
        </div>
        {/*<HeaderImageNavigator {...this.props} viewer={viewer} />*/}
      </div>
    );
  }
}


styles = {
  row: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
    margin: 20,
    fontFamily: 'lato',
  },
  bgIcon: {
    width: 100,
    margin: 10,
  },
  smIcon: {
    width: 30,
    margin: 10,
  },
  containerButton: {
    height: '60px',
    boxShadow: '0 0 6px 0 rgba(0,0,0,0.5)',
    borderRadius: '10px',
    // zIndex: '1',
    marginTop: 10,
    justifyContent: 'flex-end',
    '@media (max-width: 610px)': {
      display: 'block',
      top: 320
    },
    transition: 'all cubic-bezier(0.22,0.61,0.36,1) .3s',
    ':hover': {
      transform:'scale(1.05)',
      boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)'
    },
  },
  box: {
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: '#0005',
    borderRadius: 20,
    width: 400,
    height: 300,
    alignItems: 'center',
    textDecoration: 'none',
  },
  containerFilter: {
    height: '100%',
    backgroundSize: 'cover',
    backgroundColor: '#00000033',
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: '25px 80px 25px 80px',
    '@media only screen and (max-width : 850px)': {
      padding: '25px 25px 25px 25px',
    },
  },
  container: {
    width: '100%',
    // opacity: 0.9,
    backgroundImage: 'url("assets/images/AboutUs/background.png")',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
  },
  headingText: {
    // width: '36%',
    fontFamily: 'Lato',
    fontSize: '36px',
    lineHeight: '52px',
    color: colors.white,
    fontWeight: 'bold',
    // position: 'absolute',
    // top: '15px',
    // left: '19%',
    textAlign: 'center',
    '@media (max-width: 1280px)': {
      fontSize: '34px',
    },
    '@media (max-width: 978px)': {
      fontSize: '32px',
      // width: '50%',
    },
    '@media (max-width: 768px)': {
      // width: '80%',
      fontSize: '26px',
      lineHeight: '44px',
      // left: '5%',
    },
    '@media (max-width: 425px)': {
      // width: '80%',
      fontSize: '22px',
      lineHeight: '30px',
      // left: '5%',
    },
  },
  inputSearch: {
    fontFamily: 'Lato',
    fontSize: '18px',
    display: 'inline',
    padding: '5px 20px',
    color: colors.white,
    '@media (max-width: 425px)': {
      fontSize: '16px',
    }
  },
  playButton: {
    cursor: 'pointer',
    fontSize: '15px',
    height: 70,
    width: 70,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
    backgroundColor: 'rgba(25, 25, 25, 0.95)',
    color: colors.white,
    transition: 'all cubic-bezier(0.22,0.61,0.36,1) .3s',
    opacity: 0.9,
    marginTop: 70,
    ':hover': {
      opacity: 1,
      transform:'scale(1.1)',
      boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)'
    },
    '@media (max-width: 850px)': {
      // marginTop: 50
    }
  },
  playIcon: {
    marginLeft: 4
  },
  searchButton: {
    height: '100%',
    backgroundColor: colors.blue,
    boxSizing: 'border-box',
    paddingRight: 10,
    paddingLeft: 10,
    borderRadius: 10,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    textDecoration: 'none',
    cursor: 'pointer'
  },
  contactInfo: {
    fontSize: 20,
    color: colors.white,
    display: 'flex',
    alignItems: 'center'
  }
};

export default Relay.createContainer(Radium(HeaderImage), {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        me
      }
    `,
  },
});
