import React, { Component } from 'react';
import Relay from 'react-relay';
import debounce from 'lodash.debounce'
import Radium from 'radium'
import Helmet from 'react-helmet'

import Header from '../common/Header/Header.js'
import Footer from '../common/Footer/Footer.js'
import Loading from '../common/Loading/Loading.js'
import HeaderImage from './HeaderImage';
import localizations from '../Localizations'
import constants from "../../../constants";
import colors from './../../theme/colors'
import FacebookProvider, { CustomChat } from 'react-facebook'

let styles;

class AboutUs extends Component {
	constructor(props){
		super(props)
		this.state = {
			sportFilter: '',
			locationFilter: '',
      loading: true,
      language: localizations.getLanguage(),
		}
		this._onDebounceSportFilterChange = debounce(this._onDebounceSportFilterChange, 400);
		this._onDebounceLocationFilterChange = debounce(this._onDebounceLocationFilterChange, 400);
	}

	_onDebounceSportFilterChange = (e) => {
		this.setState({ sportFilter: e.target.value })
	}

	_onDebounceLocationFilterChange = (e) => {
		this.setState({ locationFilter: e.target.value })
	}

	_onSportFilterChange = (e) => {
    e.persist();
    this._onDebounceSportFilterChange(e);
  }

	_onLocationFilterChange = (e) => {
    e.persist();
    this._onDebounceLocationFilterChange(e);
  }

  _resetState = (language) => {
    this.setState({ language:language })
  }

  componentDidMount = () => {
    setTimeout(() => this.setState({ loading: false }), 1000)
  }

  renderMetaTags = () => {
    return <Helmet>
              <title>{localizations.meta_title}</title>
              <meta name="description" content={localizations.meta_description}/>
              <meta property="fb:app_id" content="1759806787601548"/>
              <meta property="og:type" content="website"/>
              <meta property="og:title" content={localizations.meta_title} />
              <meta property="og:description" content={localizations.meta_description}/>
              <meta property="og:url" content={constants.appUrl}/>
              <meta property="og:image" content={constants.appUrl+"/assets/images/logo-blue@3x.png"} />
              <meta property="og:image:width" content="225"/>
              <meta property="og:image:height" content="270"/>
          </Helmet>
  }

  render() {
    const { viewer } = this.props

    return (
      <div style={styles.container}>
        {this.state.loading && <Loading />}
        {this.renderMetaTags()}

				<FacebookProvider
					appId='1759806787601548'
					language={localizations.getLanguage()}
				>
          <CustomChat
						pageId="1785262331755411"
						minimized={true}
					/>
        </FacebookProvider>

        { viewer && viewer.me ?
          <Header viewer={viewer} user={viewer.me} {...this.state} /> :
          <Header viewer={viewer ? viewer : null} user={null} {...this.state} /> }
        <HeaderImage viewer={viewer ? viewer : null}
                  onSportFilterChange={this._onSportFilterChange}
                  onLocationFilterChange={this._onLocationFilterChange}
                  {...this.state}/>
        {viewer && viewer.me ?
          <Footer onUpdateLanguage={this._resetState} viewer={viewer} user={viewer.me}/> :
          <Footer onUpdateLanguage={this._resetState} viewer={viewer ? viewer : null} user={null}/> }
      </div>

    );
  }
}



styles = {
  youAreContainer: {
    backgroundColor: colors.blue,
    padding: 20,
    display: 'flex',
    flexDirection: 'column',
    textAlign: 'center',
  },
  youAreTitle: {
    color: colors.white,
    fontFamily: 'Lato',
    fontSize: '16px',
    fontWeight: 'bold',
    margin: '5px auto 15px auto',
    '@media (max-width: 600px)': {
      fontSize: '12px',
    },
  },
  youAreLink: {
    color: colors.white,
    textDecoration: 'none',
    fontFamily: 'Lato',
    fontSize: '16px',
    fontWeight: 'bold',
    cursor: 'pointer' ,
    padding: '0px 15px',
    ':hover': {
      color: colors.lightGray,
    },
    '@media (max-width: 600px)': {
      fontSize: '14px',
    },
  },
  text: {
    // width: '160px',
    fontFamily: 'Lato',
    fontSize: '24px',
    fontWeight: '500',
    textAlign: 'center',
    color: 'rgba(0,0,0,0.65)',
    marginBottom: 30,
    '@media (max-width: 600px)': {
      fontSize: '22px',
    },
  },
  headerDiscovery: {
    textAlign: 'center',
    fontFamily: 'lato',
    margin: '20px 0px',
    padding: 10,
    fontSize: 26,
    fontWeight: 'bold',
    color: colors.darkGray,
    '@media (max-width: 1280px)': {
      fontSize: 24,
    },
    '@media (max-width: 978px)': {
      fontSize: 22,
    },
    '@media (max-width: 768px)': {
      fontSize: 20,
    },
  },
  container: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  download_icons:{
    display: 'none',
    '@media (max-width: 480px)': {
      display: 'block',
      textAlign: 'center',
      paddingTop: 7,
      paddingBottom: 7
    },
  },
  download_icons_text: {
    fontSize: 14,
    fontFamily: 'Lato',
  },
  download_icons_footer: {
    display: 'flex',
    justifyContent:'center',
    flexDirection:'row',
    maxWidth: 500,
    margin: '-75px auto 20px auto',
    textAlign: 'center',
    width: '100%',
    zIndex: 100,
  },
  button: {
    height: '55px',
    borderRadius: '100px',
    backgroundColor: colors.blue,
    margin: '40px auto',
    maxWidth: '360px',
    paddingRight: 40,
    paddingLeft: 40,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    textDecoration: 'none',
    fontSize: 25,
    fontFamily: 'Lato',
    color: '#fff',
    cursor: 'pointer',
    '@media (max-width: 850px)': {
      borderRadius: '100px 100px 100px 100px',
    },
  },
}

export default Relay.createContainer(Radium(AboutUs), {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        me {
          ${Header.getFragment('user')}
          ${Footer.getFragment('user')}
          id
        }
        ${HeaderImage.getFragment('viewer')},
        ${Header.getFragment('viewer')},
        ${Footer.getFragment('viewer')}
      }
    `
  },
});