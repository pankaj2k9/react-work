import Relay from 'react-relay';

export default class SecondaryOrganizerRefuseRole extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation {secondaryOrganizerRefuseRole}`;
  }

  getVariables() {
    return {
      sportunityID: this.props.sportunity.id,
      pendingOrganizerID: this.props.pendingOrganizerIDVar, 
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on secondaryOrganizerRefuseRolePayload{
        viewer {
            sportunity {
                organizers
                pendingOrganizers
                status
            }
        }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        viewer: this.props.viewer.id,
      },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
      }
    `,
  };
}
