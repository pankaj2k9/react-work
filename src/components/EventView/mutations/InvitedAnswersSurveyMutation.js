import Relay from 'react-relay';

export default class InvitedAnswersSurveyMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation {invitedAnswersSurvey}`;
  }

  getVariables() {
    return {
      sportunityID: this.props.sportunity.id,
	    userId: this.props.userIdVar,
	    answers: this.props.answersVar
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on invitedAnswersSurveyPayload{
          sportunity {
            survey {
              isSurveyTransformed
              surveyDates {
                beginning_date
                ending_date
                answers {
                  user {
                    id
                  }
                  answer
                }
              }
            }
          }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
	      sportunity: this.props.sportunity.id,
      },
    }];
  }

  static fragments = {
    sportunity: () => Relay.QL`
      fragment on Sportunity {
        id
        survey {
          isSurveyTransformed
          surveyDates {
            beginning_date
            ending_date
            answers {
              user {
                id
              }
              answer
            }
          }
        }
      }
    `,
  };
}
