import Relay from 'react-relay';

export default class OrganizerAddInviteds extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation {organizerAddInviteds}`;
  }

  getVariables() {
    return {
      sportunityID: this.props.sportunity.id,
      inviteds: this.props.invitedsVar, 
      putInvitedsInCircle: this.props.putInvitedsInCircleVar
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on organizerAddInvitedsPayload{
        viewer {
            sportunity {
                invited
                participants
                status
            }
        }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        viewer: this.props.viewer.id,
      },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
      }
    `,
  };
}
