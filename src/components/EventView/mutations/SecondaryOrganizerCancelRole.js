import Relay from 'react-relay';

export default class SecondaryOrganizerCancelRole extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation {secondaryOrganizerCancelRole}`;
  }

  getVariables() {
    return {
      sportunityID: this.props.sportunity.id,
      cancelSerie: this.props.cancelSerieVar
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on secondaryOrganizerCancelRolePayload{
        viewer {
            sportunity {
                organizers
                pendingOrganizers
                status
            }
        }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        viewer: this.props.viewer.id,
      },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
      }
    `,
  };
}
