import Relay from 'react-relay';

export default class OrganizerPickDateMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation {organizerPickDate}`;
  }

  getVariables() {
    return {
      sportunityID: this.props.sportunity.id,
	    beginning_date: this.props.answerVar.beginning_date,
	    ending_date: this.props.answerVar.ending_date
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on organizerPickSurveyDatePayload{
          sportunity {
            beginning_date
            ending_date
            participants {
              id
              pseudo
              avatar
            }
            invited {
              user {
                id
                pseudo
                avatar
              }
            }
            survey {
              isSurveyTransformed
              surveyDates {
                beginning_date
                ending_date
                answers {
                  user {
                    id
                  }
                  answer
                }
              }
            }
          }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
	      sportunity: this.props.sportunity.id,
      },
    }];
  }

  static fragments = {
    sportunity: () => Relay.QL`
      fragment on Sportunity {
        id
        beginning_date
        ending_date
        participants {
          id
          pseudo
          avatar
        }
        invited {
          user {
            id
            pseudo
            avatar
          }
        }
        survey {
          isSurveyTransformed
          surveyDates {
            beginning_date
            ending_date
            answers {
              user {
                id
              }
              answer
            }
          }
        }
      }
    `,
  };
}
