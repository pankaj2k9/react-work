import Relay from 'react-relay';

export default class OrganizerAddParticipants extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation {organizerAddParticipants}`;
  }

  getVariables() {
    return {
      sportunityID: this.props.sportunity.id,
      participants: this.props.participantsVar, 
      putParticipantsInCircle: this.props.putParticipantsInCircleVar
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on organizerAddParticipantsPayload{
        viewer {
            sportunity {
                participants
                waiting
                invited
                status
            }
        }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        viewer: this.props.viewer.id,
      },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
      }
    `,
  };
}
