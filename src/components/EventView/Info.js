import React from 'react'
import Radium from 'radium'
import Relay from 'react-relay'
import { Link } from 'react-router'
import RelayStore from '../../RelayStore'

import ReactTooltip from 'react-tooltip'
import { colors } from '../../theme'
import GameInformationRow from './GameInformationRow';
import TimeAndLocation from './TimeAndLocation'
import Price from './Price'
import FAQLinks from './FAQLinks';
import AskedCoOrganization from './AskedCoOrganization';
import CoOrganizationCancelation from './CoOrganizationCancelation';
import localizations from '../Localizations'
import CarPooling from "./CarPooling";
import Comments from "./Comments";
import {formatDate} from "./formatDate";
import Checkbox from "../common/Inputs/InputCheckbox";
import Invited from "../NewSportunity/Invited";
import AlertContainer from "react-alert";
import InvitedAnswersSurveyMutation from "./mutations/InvitedAnswersSurveyMutation";
import OrganizerPickDateMutation from './mutations/OrganizerPickDateMutation'


const Title = ({ children }) => <h2 style={styles.title}>{children}</h2>;

class Info extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      isLoginError: false,
      userDates: [],
      organiserDates: [],
	    canModify: false,
    }
	  this.alertOptions = {
		  offset: 60,
		  position: 'top right',
		  theme: 'light',
		  transition: 'fade',
	  };
  }

  _setLoginError = () => {
    this.setState({
      isLoginError: true,
    })
  };

	componentWillReceiveProps = (nextProps) => {
    const {sportunity, viewer, userIsInvited, isAdmin} = nextProps;
    let canModify = false;
    if (sportunity.survey && (userIsInvited || sportunity.kind === 'PUBLIC' || isAdmin)) {
      let userDates = [];
      let organiserDates = [];
      sportunity.survey.surveyDates.forEach((date) => {
        userDates.push({
          beginning_date: date.beginning_date,
          ending_date: date.ending_date,
          answer: date.answers.findIndex(answer => answer.user.id === viewer.me.id && answer.answer === 'YES') >= 0
        })
	      organiserDates.push({
          beginning_date: date.beginning_date,
          ending_date: date.ending_date,
          answer: false
        })
	      canModify = canModify || date.answers.findIndex(answer => answer.user.id === viewer.me.id) >= 0
      });
      this.setState({userDates, organiserDates, canModify: !canModify})
    }
  };

  _capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase()
  };

  statusStyle = (status) => {
    switch(status) {
      case 'RED':
        return styles.red;
      case 'YELLOW':
        return styles.yellow;
      case 'GREY':
        return styles.lightGreen;
      case 'BLACK':
        return styles.darkGreen;
      case 'GREEN':
        return styles.darkGreen;
      default:
        return styles.lightGreen;
    }
  };

  _shouldShowRequirements = (sportunity) => {
    
    return (!sportunity.sport.allLevelSelected ||
      sportunity.sport.certificates.length > 0 ||
      sportunity.sport.positions.length > 0 ||
      sportunity.sexRestriction !== 'NONE' ||
      sportunity.ageRestriction.from !== 0 ||
      sportunity.ageRestriction.to !== 100)
  };

  _renderRequirements = (sport) => {
    if (sport.levels && sport.levels.length > 0)
      sport.levels = sport.levels.sort((a,b)=>{return a.EN.skillLevel - b.EN.skillLevel});

    return (
      <div style={{marginBottom: 10}}>
        { !sport.allLevelSelected && sport.levels.length > 0 &&
        <p>{localizations.event_levels + ': '} {this.displayLevel(sport)}</p>
        }
        {
          sport.positions.length > 0 &&
          <p>{localizations.event_positions + ': ' + sport.positions.map(position => ' '+position[localizations.getLanguage().toUpperCase()])}</p>
        }
        {
          sport.certificates.length > 0 &&
          <p>{localizations.event_certificates + ': ' + sport.certificates.map(certificate => ' '+certificate.name[localizations.getLanguage().toUpperCase()])}</p>
        }
      </div>
    )
  };

  _renderSexRestrictions = sexRestriction => {
    let translations = {
      "NONE": localizations.newSportunity_sex_restriction_none,
      "MALE": localizations.newSportunity_sex_restriction_male,
      "FEMALE": localizations.newSportunity_sex_restriction_female
    };
    if (sexRestriction === 'NONE')
      return null; 
    else 
      return (
        <div style={{marginBottom: 10}}>
          {localizations.event_sexRestriction + ' : '+ translations[sexRestriction]}
        </div>
      )
  };

  _renderAgeRestrictions = ageRestriction => {
    if (ageRestriction.from !== 0 || ageRestriction.to !== 100) {
      return (
        <div>
          { localizations.event_ageRestriction + ' : '+
          localizations.newSportunity_from + ' ' + ageRestriction.from + ' ' +
          localizations.newSportunity_to + ' ' + ageRestriction.to + ' ' +
          localizations.event_ageRestriction_years
          }
        </div>
      )
    }
    else return null;
  };

  displayLevel = (sport) => {
    let sports = sport.levels.map(level => (
      {
        name: level[localizations.getLanguage().toUpperCase()].name,
        description: level[localizations.getLanguage().toUpperCase()].description
      }
    ));

    if (sport.allLevelSelected) {
      return  <span>
                {localizations.event_allLevelSelected}
              </span>
    }
    else if (sports.length === 1) {
      return <span>
              {sports[0].name}
        { sports[0].description &&
        <i
          data-tip={sports[0].description}
          style={{marginLeft:10, cursor:'pointer'}}
          className="fa fa-question-circle"
          aria-hidden="true"
        />
        }
            </span>
    } else {
      return <span>
              {sports[0].name}
        {' ' + localizations.find_to + ' '}
        {sports[sports.length - 1].name}
        {
          sports[0].description && sports[sports.length - 1].description &&
          <i
            data-tip={localizations.event_level_from+': '+ sports[0].description+'<br/>'+localizations.event_level_to+': '+sports[sports.length - 1].description}
            style={{marginLeft:10, cursor:'pointer'}}
            className="fa fa-question-circle"
            aria-hidden="true"
          />
        }
            </span>
    }
  };

	_handleChangeSuggest = (checked, index) => {
    let {userDates} = this.state;
    userDates[index].answer = checked;
    this.setState({userDates});
  };

	_handleChangeDate = (checked, index) => {
    let {organiserDates} = this.state;
		organiserDates.forEach((organiserDate, localIndex) => {
			organiserDates[localIndex].answer = false
		});
		organiserDates[index].answer = checked;
    this.setState({organiserDates});
  };

	_handleSubmitSuggest = () => {
		let answersVar = [];
		this.state.userDates.forEach(date => {
			answersVar.push({
				beginning_date: date.beginning_date,
				ending_date: date.ending_date,
				answer: date.answer ? 'YES' : 'WAITING'
			})
		})
    RelayStore.commitUpdate(
      new InvitedAnswersSurveyMutation({
        sportunity: this.props.sportunity,
        userIdVar: this.props.viewer.me.id,
        answersVar,
	      viewer: this.props.viewer
      }), {
		    onFailure: error => {
			    this.msg.show(error.getError().source.errors[0].message, {
				    time: 2000,
				    type: 'error',
			    });
			    setTimeout(() => this.msg.removeAll(), 2000);

		    },
		    onSuccess: (response) => {
			    this.msg.show(localizations.popup_editCircle_update_success, {
				    time: 2000,
				    type: 'success',
			    });
			    setTimeout(() => this.msg.removeAll(), 2000);
		    },
	    }
    )
  };

	_handleSubmitDate = () => {
		let valideDate = [];
		this.state.organiserDates.forEach(date => {
			if (date.answer)
				valideDate.push({
					beginning_date: date.beginning_date,
					ending_date: date.ending_date,
					answer: 'YES'
				})
		});
    RelayStore.commitUpdate(
      new OrganizerPickDateMutation({
        sportunity: this.props.sportunity,
        answerVar: valideDate[0],
	      viewer: this.props.viewer
      }), {
		    onFailure: error => {
			    this.msg.show(error.getError().source.errors[0].message, {
				    time: 2000,
				    type: 'error',
			    });
			    setTimeout(() => this.msg.removeAll(), 2000);

		    },
		    onSuccess: (response) => {
			    this.msg.show(localizations.popup_editCircle_update_success, {
				    time: 2000,
				    type: 'success',
			    });
			    setTimeout(() => this.msg.removeAll(), 2000);
		    },
	    }
    )
  };

	_handleModify = () => {
		this.setState({canModify: !this.state.canModify})
	}

  render() {
    const {
      sportunity,
      status,
      showBook,
      showJoinWaitingList,
      enableBook,
      onBook,
      onCancel,
      onAdminModify,
      onAdminCancel,
      onAdminReOrganize,
      onDeclineInvitation,
      onOpponentBook,
      viewer,
      isActive,
      isPast,
      isSecondaryOrganizer,
      isPotentialSecondaryOrganizer,
      isAdmin,
      isAuthorizedAdmin,
      isPotentialOpponent,
      onSeeAsAdmin,
      props,
      isLogin,
      isCancelled,
      userIsInvited,
      userIsOnWaitingList,
      onAdminDisplayStatForm,
      userIsParticipant,
      organizers,
      isLoading,
      user
    } = this.props;


    let tmpUser = [].concat(
	    sportunity.invited.map(user => (user.user)),
	    sportunity.organizers.map(user => (user.organizer)),
    );

    let invitedUser = [];

    if (sportunity.survey && sportunity.survey.surveyDates && sportunity.survey.surveyDates.length > 1) {
	    tmpUser.forEach(user => {
	    	if ((sportunity.survey.surveyDates[0].answers.findIndex(answer => answer.user.id === user.id) >= 0 ||
			    (viewer.me && user.id === viewer.me.id && (userIsInvited || isAdmin))) &&
			    invitedUser.findIndex(invited => invited.id === user.id) < 0)
	    		invitedUser.push(user)
	    })
    }

    if (viewer.me && invitedUser.findIndex(invited => invited.id === viewer.me.id) < 0 && sportunity.kind === 'PUBLIC')
    	invitedUser.push(viewer.me)


	  let canValidDate = this.state.organiserDates.findIndex(date => date.answer === true) >= 0;
	  let canValidSuggestion = this.state.userDates.findIndex(date => date.answer === true) >= 0 ;

    return(
      <section>
	      <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
        <ReactTooltip effect="solid" multiline={true}/>
        {/*{!isAdmin && !isSecondaryOrganizer && !isPotentialSecondayOrganizer && !userIsInvited && !userIsParticipant && !userIsOnWaitingList && <FAQLinks />}*/}

	      <div style={this.statusStyle(status.color)}>
		      <Title >{this._capitalizeFirstLetter(status.displayStatus)}</Title>
		      {isActive &&
		      <p style={styles.description}>
			      {localizations.formatString(status.status_information, sportunity.participantRange.from)}
		      </p>
		      }
	      </div>

        {sportunity.survey && sportunity.survey.surveyDates && sportunity.survey.surveyDates.length > 1 &&
          <div style={{fontFamily: 'Lato', fontSize: 20}}>
            <Title>
              {localizations.sondage_title}
            </Title>
	          <div style={{width: '100%', overflow: 'auto'}}>
            <table style={styles.table}>
              <thead>
                <tr>
                  <td style={styles.headerText}/>
	                {sportunity.survey.surveyDates.map((date, index) => (
		                <td key={index} style={styles.headerText}>
                      {formatDate(date.beginning_date, date.ending_date)}
		                </td>
                  ))}
                </tr>
                <tr>
	                <td style={styles.headerText}>
                    <div>
                      <p>
                        {localizations['sondage_row_participent_0'] + ' (' + (sportunity.survey.surveyDates[0].answers ? sportunity.survey.surveyDates[0].answers.length : 0) + ')'}
                      </p>
                      <p>
                        {localizations['sondage_row_participent_1'] + (sportunity.invited && sportunity.survey.surveyDates[0].answers ? sportunity.invited.length - sportunity.survey.surveyDates[0].answers.length : 0) + localizations['sondage_row_participent_2']}
                      </p>
                    </div>
	                </td>
	                {sportunity.survey.surveyDates.map((date, index) => (
		                <td key={index} style={styles.headerText}>
                      <div>
                        <i
                          className='fa fa-check'
                          style={{marginRight: 5}}
                        />
                        {date.answers.filter(answer => answer.answer === 'YES').length}
                      </div>
		                </td>
	                ))}
                </tr>
              </thead>
              <tbody>
                {invitedUser.map((member, indexRow) => (
                  <tr key={indexRow}>
	                  <td style={styles.firstCol}>
		                  <div style={styles.firstColContent}>
			                  <span
				                  style={{...styles.iconImage, backgroundImage: member.avatar ? 'url('+ member.avatar +')' : 'url("https://sportunitydiag304.blob.core.windows.net/avatars/default-avatar.png")'}}
			                  />
			                  {member.pseudo}
			                  {viewer.me && member.id === viewer.me.id && !this.state.canModify && !sportunity.survey.isSurveyTransformed &&
				                  <i
					                  className='fa fa-pencil'
					                  style={{cursor: 'pointer', marginLeft: 5}}
					                  onClick={this._handleModify}
				                  />
			                  }
		                  </div>
                    </td>
                    {sportunity.survey.surveyDates.map((date, index) => (
                    <td key={index} style={!this.state.canModify || member.id !== viewer.me.id
	                    ? date.answers.findIndex(answer => answer.user.id === member.id && answer.answer === 'YES') >= 0
	                    ? {...styles.cellContent, backgroundColor: colors.blue}
	                    : {...styles.cellContent, backgroundColor: colors.red}
	                    : styles.cellContent}>
                      <div style={styles.inputContainer}>
	                    {!sportunity.survey.isSurveyTransformed && viewer.me && member.id === viewer.me.id && this.state.canModify ?
                          <Checkbox
                            checked={this.state.userDates[index] ? this.state.userDates[index].answer : null}
                            onChange={(e) => {
                              this._handleChangeSuggest(e.target.checked, index)
                            }}
                          />
                        : date.answers.findIndex(answer => answer.user.id === member.id && answer.answer === 'YES') >= 0
			                    ? <i
				                    className='fa fa-check'
				                    style={{color: colors.white}}
			                    />
			                    : date.answers.findIndex(answer => answer.user.id === member.id && answer.answer !== 'YES') >= 0
				                    ? <i
					                    className='fa fa-times'
					                    style={{color: colors.white}}
				                    />
				                    : null
	                    }
                      </div>
                    </td>
                    ))}
                  </tr>
                ))}
                {!sportunity.survey.isSurveyTransformed && isAdmin &&
                <tr>
                  <td style={styles.firstCol}>
                    <div style={styles.firstColContent}>
                      {localizations.sondage_selectDate}
		                </div>
	                </td>
	                {sportunity.survey.surveyDates.map((date, index) => (
		                <td key={index} style={styles.cellContent}>
			                <div style={styles.inputContainer}>
					                <Checkbox
						                checked={this.state.organiserDates[index] ? this.state.organiserDates[index].answer : null}
						                onChange={(e) => {
							                this._handleChangeDate(e.target.checked, index)
						                }}
					                />
			                </div>
		                </td>
	                ))}
                </tr>
                }
              </tbody>
            </table>
	          </div>
	          {!sportunity.survey.isSurveyTransformed &&
		          <div style={styles.buttonContainer}>
			          {isAdmin && canValidDate &&
			          <div style={{...styles.button, backgroundColor: colors.lightGray}} onClick={this._handleSubmitDate}>
				          {localizations.sondage_selectSondage['2']}
			          </div>
			          }
			          {(userIsInvited || sportunity.kind === 'PUBLIC' || isAdmin) && this.state.canModify && !canValidDate &&
			          <div style={{...styles.button, backgroundColor: colors.green, color: colors.white}}
			               onClick={this._handleSubmitSuggest}>
				          {canValidSuggestion ? localizations.sondage_valid : localizations.sondage_valid_empty}
			          </div>
			          }
		          </div>
	          }
          </div>
        }

        <GameInformationRow
          sportunity={sportunity}
          language={() => localizations.getLanguage()}
        />
        {isPotentialSecondaryOrganizer &&
        <AskedCoOrganization
          sportunity={sportunity}
          language={() => localizations.getLanguage()}
          user={user}
          onSelectRole={this.props.onSecondaryOrganizerSelectRole}
          onRefuseRole={this.props.onSecondaryOrganizerRefuseRole}
        />
        }
        {isSecondaryOrganizer && !isPast && !isCancelled &&
        <CoOrganizationCancelation
          onCancel={this.props.onSecondaryOrganizerCancel}
        />
        }
        <Title>{localizations.event_description}</Title>
        <p style={styles.description}>
          {sportunity.description}
        </p>
        {
          this._shouldShowRequirements(sportunity) &&
          <span>
            <Title>{localizations.event_additional_requirements}</Title>
            <p style={styles.description}>
              {this._renderRequirements(sportunity.sport)}
              {this._renderSexRestrictions(sportunity.sexRestriction)}
              {this._renderAgeRestrictions(sportunity.ageRestriction)}
            </p>
          </span>
        }
        <Title>{localizations.event_timeLocation}</Title>
        <TimeAndLocation
          sportunity={sportunity}
          {...this.props}
        />
        {
          !isAdmin && !isSecondaryOrganizer && isActive && !userIsParticipant && !isAuthorizedAdmin &&
          <Link to={'/profile-view/'+organizers[0].organizer.id} style={styles.ask_question_container}>
            <button style={styles.ask_question}>
              {localizations.event_ask_a_question}
            </button>
          </Link>
        }

        <Price
          sportunity={sportunity}
          viewer={viewer}
          isAdmin={isAdmin}
          {...props}
        />
      </section>
    )
  }
}

let styles = {
  black: {
    color: colors.black,
  },
  gray: {
    color: colors.gray,
  },
  yellow: {
    color: colors.yellow,
  },
  red: {
    color: colors.red,
  },
  lightGreen: {
    color: colors.lightGreen,
  },
  darkGreen: {
    color: colors.darkGreen
  },
	table: {
		boxShadow: '0 0 14px 0 rgba(0,0,0,0.12)',
		border: '2px solid #E7E7E7',
		fontSize: 18,
		lineHeight: '24px',
		color: 'rgba(0,0,0,0.65)',
		fontFamily: 'Lato',
		margin: 'auto',
	},
	headerText: {
		fontSize: 20,
		fontWeight: 'bold',
		padding: 10,
		minWidth: 120,
		textAlign: 'center',
		border: '1px solid '+colors.gray,
		backgroundColor: 'whitesmoke',
    verticalAlign: 'middle'
	},
	cellContent: {
		border: '1px solid '+colors.gray,
		padding: 7,
		verticalAlign: 'middle',
		textAlign: 'center',
		backgroundColor: 'whitesmoke',
	},
	iconImage: {
		color:colors.white,
		width: 40,
		height: 40,
		marginRight: 10,
		borderRadius: '50%',
		backgroundPosition: '50% 50%',
		backgroundSize: 'cover',
		backgroundRepeat: 'no-repeat',
		display: 'inline-block'
	},
	firstCol: {
		padding: '5px 10px',
		border: '1px solid '+colors.gray,
		backgroundColor: 'whitesmoke',
	},
	firstColContent: {
		display: 'flex',
		justifyContent: 'flex-start',
		alignItems: 'center',
		backgroundColor: 'whitesmoke',
	},
	inputContainer: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center'
	},
  error: {
    fontFamily: 'Lato',
    color: colors.red,
    fontSize: 16,
    marginTop: 10,
    width: 180,
  },
  errorFeedback: {
    fontFamily: 'Lato',
    color: colors.red,
    fontSize: 16,
  },
  content: {
    flexGrow: '1',
    padding: '40px',
    fontFamily: 'Lato',
    color: 'rgba(0,0,0,0.65)',
    '@media (max-width: 700px)': {
      padding: '20px',
    },
  },
  headerImage: {
    width: '100%'
  },

  title: {
    fontSize: 32,
    fontWeight: 500,
    marginBottom: 30,
  },

  description: {
    fontSize: 18,
    lineHeight: 1.2,
    marginBottom: 40,
    whiteSpace: 'pre-line',
    wordWrap: 'break-word'
  },
  book_container: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center'
  },
  book: {
    backgroundColor: colors.green,
    color: colors.white,
    width: 230,
    // height: 70,
    borderRadius: 100,
    borderStyle: 'none',
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
    fontSize: 22,
    cursor: 'pointer',
    padding: '13px 5px',

    position: 'relative',
    // left: 'calc(50% - 115px)',

    ':disabled': {
      cursor: 'not-allowed',
      backgroundColor: colors.gray,
    },
  },
  modify: {
    backgroundColor: colors.green,
    color: colors.white,
    width: 230,
    height: 70,
    borderRadius: 100,
    borderStyle: 'none',
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
    fontSize: 22,
    cursor: 'pointer',

    position: 'relative',
    marginBottom: '10px',

    ':disabled': {
      cursor: 'not-allowed',
      backgroundColor: colors.gray,
    },
  },
  ask_question_container: {
    display: 'flex',
    justifyContent: 'center',
    textDecoration: 'none',
    marginBottom: 20
  },
  ask_question: {
    backgroundColor: colors.green,
    color: colors.white,
    width: 290,
    height: 70,
    borderRadius: 100,
    borderStyle: 'none',
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
    fontSize: 22,
    cursor: 'pointer',

    position: 'relative',
    // left: 'calc(50% - 145px)',

    ':disabled': {
      cursor: 'not-allowed',
      backgroundColor: colors.gray,
    },
  },
  cancel: {
    backgroundColor: colors.red,
    color: colors.white,
    width: 230,
    // height: 70,
    borderRadius: 100,
    borderStyle: 'none',
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
    fontSize: 22,
    cursor: 'pointer',
    padding: '13px 5px',

    position: 'relative',
    // left: 'calc(50% - 115px)',

    ':disabled': {
      cursor: 'not-allowed',
      backgroundColor: colors.gray,
    },
  },
  buttonContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
	  margin: '5px 0px'
  },
  button: {
    padding: 10,
    margin: '5px 10px',
    borderRadius: 10,
	  cursor: 'pointer'
  },
  policy:{
    cursor: 'pointer',
    fontSize: 16,
    textAlign: 'center',
    marginTop: 15,
    marginBottom: 50,
  }
};


export default Relay.createContainer(Radium(Info), {
  fragments: {
    sportunity: () => Relay.QL`
      fragment on Sportunity {
        id
        description,
        kind
        ${TimeAndLocation.getFragment('sportunity')},
        ${Price.getFragment('sportunity')},
        ${GameInformationRow.getFragment('sportunity')}
        ${AskedCoOrganization.getFragment('sportunity')}
        ${InvitedAnswersSurveyMutation.getFragment('sportunity')}
        ${OrganizerPickDateMutation.getFragment('sportunity')}
        survey {
          isSurveyTransformed
          surveyDates {
            beginning_date
            ending_date
            answers {
              user {
                id
                pseudo
                avatar
              }
              answer
            }
          }
        }
        organizers {
          organizer {
            id
            pseudo
            avatar
          }
        }
        invited {
          user {
            id
            avatar
            pseudo
          }
        }
        participants {
          id
          pseudo
          avatar
        }
        participantRange {
          from,
          to,
        },
        address {
          address
          city
					position {
            lat
            lng
          }
        },
        images
        sexRestriction
        ageRestriction{
          from, 
          to
        }
        sport {
          allLevelSelected,
          levels {
            id
            EN {
              name
              skillLevel
              description
            }
            FR {
              name
              skillLevel
              description
            }
            DE {  
              name
              skillLevel
              description
            } 
          }
          certificates {
            name {
              EN,
              FR,
              DE
            }
          }
          positions {
            EN
            FR,
            DE
          }
        }
      }
    `,
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
        ${Price.getFragment('viewer')}
        me {
          id
          pseudo
          avatar
        }
      }
    `,
    user: () => Relay.QL`
      fragment on User {
        ${AskedCoOrganization.getFragment('user')}
        areStatisticsActivated
      }
    `
  },
});