import React from 'react';
import Radium from 'radium';
import AlertContainer from 'react-alert'
import Relay from 'react-relay'

import localizations from '../Localizations'
import { colors } from '../../theme';

let styles;

class RelaunchInvited extends React.Component {
    constructor() {
        super();
        this.alertOptions = {
            offset: 60,
            position: 'top right',
            theme: 'light',
            transition: 'fade',
        };
    }

    relaunchInvitedUsers = () => {
        if (!this.props.relay.variables.queryRelaunch) {
        this.props.relay.setVariables({
            id: this.props.sportunity.id,
            queryRelaunch: true
        })
        setTimeout(() => {
            this.msg.show(localizations.event_relaunch_invited_success, {
                time: 3000,
                type: 'success',
            }); 
            setTimeout(() => {
                this.msg.removeAll();
            }, 2000);
        }, 1000) 
        }
        else {
            this.msg.show(localizations.event_relaunch_invited_done, {
                time: 3000,
                type: 'info',
            }); 
            setTimeout(() => {
                this.msg.removeAll();
            }, 2000);
        }
    }

    render() {
        return (
            <div style={styles.container}>
                <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
                <span style={styles.separator}> - </span>
                <div style={styles.button} onClick={this.relaunchInvitedUsers}>{localizations.event_relaunch_invited}</div>
            </div>
        );
    }

}

styles = {
    container: {
        display: 'flex',
    },
    separator: {
        margin: '0 10px'
    },
    button: {
        cursor: 'pointer',
    }
};

export default Relay.createContainer(Radium(RelaunchInvited), {
    initialVariables: {
        id: '',
        queryRelaunch: false,
    },
    fragments: {
        viewer: () => Relay.QL`
            fragment on Viewer {
                id
                relaunchInviteds (sportunityID: $id) @include(if: $queryRelaunch) {
                    id
                }
            }
        `
    }
})