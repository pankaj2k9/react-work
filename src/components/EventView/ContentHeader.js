import React from 'react'
import Radium from 'radium'

import { colors } from '../../theme'
import localizations from '../Localizations'
import Slider from "./Slider";
import ReactTooltip from 'react-tooltip'
import withRouter from 'react-router/lib/withRouter';

let styles;

class ContentHeader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isMenuVisible: false,
            showJoinCommunity: false,
            mainCommunityCircle: null,
            notInCircle: false
        };
    }

    componentDidMount() {
        window.addEventListener('click', this._handleClickOutside);
        if (this.props.sportunity && this.props.sportunity.kind === 'PUBLIC' && this.props.sportunity.invited_circles && this.props.sportunity.invited_circles.edges && this.props.sportunity.invited_circles.edges.length > 0) {
            this.props.sportunity.invited_circles.edges.forEach(edge => {
                if (edge.node.mode === 'PUBLIC') {
                    if ((!this.props.user || (edge.node.owner.id !== this.props.user.id && edge.node.members.findIndex(member => member.id === this.props.user.id) < 0)))
                        this.setState({
                            notInCircle: true
                        });
                    this.setState({
                        showJoinCommunity: true,
                        mainCommunityCircle: edge.node
                    })
                }
            })
        }
    }

    joinMainCommunity = () => {
        let path = '/join-circle/' + this.state.mainCommunityCircle.id
        this.props.router.push({
          pathname: path,
        })
    }

    inviteMainCommunity = () => {
        let path = '/circle/' + this.state.mainCommunityCircle.id
        this.props.router.push({
          pathname: path,
        })
    }

    componentWillUnmount() {
        window.removeEventListener('click', this._handleClickOutside);
    }

    _handleClickOutside = (event) => {
        if (this._containerNode && !this._containerNode.contains(event.target)) {
            this.setState({ 
                isMenuVisible: false
            });
        }
    }

    render() {
        const {
        sportunity,
        status,
        showBook,
        showJoinWaitingList,
        enableBook,
        onBook,
        onCancel,
        onAdminModify,
        onAdminCancel,
        onAdminReOrganize,
        onAdminDisplayStatForm,
        onDeclineInvitation,
        onOpponentBook,
        viewer,
        isActive,
        isPast,
        isSecondaryOrganizer,
        isPotentialSecondaryOrganizer,
        isAdmin,
        isAuthorizedAdmin,
        isPotentialOpponent,
        onSeeAsAdmin,
        props,
        isLogin,
        isCancelled,
        userIsInvited,
        userIsOnWaitingList,
        userIsParticipant,
        shouldShowChat,
        chat,
        organizers,
        isLoading,
        user
        } = this.props ;

        let { notInCircle } = this.state;

        let hasImages = sportunity.images ? sportunity.images.length : 0 ;

        return(
            <div style={!isAdmin && (isSecondaryOrganizer || isPotentialSecondaryOrganizer)
              ? {...styles.container, height: 'auto'}
              : hasImages > 0
                ? {...styles.container, height: 'auto', minHeight: '90'}
                : {...styles.container, height: 150}
            }>
                <ReactTooltip effect="solid" multiline={true}/>
                
                { sportunity.images && sportunity.images.length > 0 &&
                    <div style={{width: '100%'}}>    
                        <Slider
                            images = {sportunity.images}
                        />
                    </div>
                }

                {isAuthorizedAdmin
                ?   <div style={hasImages > 0 ? {...styles.buttonsContainer, bottom: hasImages > 1 ? 70 : 25} : {...styles.buttonsContainer, top: 25}}>
                        <button
                            key={'green'}
                            style={styles.greenButton}
                            onClick={onSeeAsAdmin}
                            disabled={isLoading}
                        >
                            <i style={styles.icon} className="fa fa-eye" />
                            {localizations.event_see_as_organizer}
                        </button>
                    </div>
                : isAdmin 
                    ?   !isPast 
                        ?   <div style={hasImages > 0 ? {...styles.buttonsContainer, bottom: hasImages > 1 ? 70 : 25} : {...styles.buttonsContainer, top: 25}}>
                                <button 
                                    key={'green'} 
                                    style={styles.greenButton}
                                    onClick={onAdminModify}
                                    disabled={!isActive || isLoading}
                                >
                                    <i style={styles.icon} className="fa fa-edit" />
                                    {localizations.event_modify}
                                </button>
                                <button
                                    key={'red'}
                                    style={styles.redButton}
                                    onClick={onAdminCancel}
                                    disabled={!isActive || isLoading}
                                >
                                    <i style={styles.icon} className="fa fa-times" />
                                    {localizations.event_cancel}
                                </button>
                                <div style={styles.menuContainer} ref={node => { this._containerNode = node; }}>
                                    <button key={'menu'} style={styles.grayButton} onClick={() => this.setState({isMenuVisible: !this.state.isMenuVisible})}>
                                        <i className="fa fa-ellipsis-h" />
                                    </button>

                                    {this.state.isMenuVisible && 
                                        <div style={styles.plusMenuContainer} >
                                            {!userIsParticipant && user.profileType === 'PERSON' && 
                                                <button
                                                    key={0}
                                                    style={styles.plusMenuItem}
                                                    onClick={() => {this.setState({isMenuVisible: false}); onBook()}}
                                                    disabled={isLoading}
                                                >
                                                    {localizations.event_organizer_book}
                                                </button>
                                            }
                                            {!userIsParticipant && user.profileType === 'PERSON' && <hr style={styles.separator} />}
                                            <button
                                                key={1}
                                                style={styles.plusMenuItem}
                                                onClick={onAdminReOrganize}
                                                disabled={isLoading}
                                            >
                                                {localizations.event_organize_again}
                                            </button>
                                            {user && user.areStatisticsActivated && <hr style={styles.separator} />}
                                            {user && user.areStatisticsActivated && 
                                                <button
                                                    key={2}
                                                    style={styles.plusMenuItem}
                                                    onClick={onAdminDisplayStatForm}
                                                    disabled={isLoading}
                                                >
                                                    {localizations.event_fill_statistics}
                                                </button>
                                            }

                                        </div>
                                    }
                                </div>
                            </div>
                        :   <div style={hasImages ? {...styles.buttonsContainer, bottom: hasImages > 1 ? 70 : 25} : {...styles.buttonsContainer, top: 25}}>
                                <button
                                    key={'green'} 
                                    style={styles.greenButton}
                                    onClick={onAdminReOrganize}
                                    disabled={isLoading}
                                >
                                    <i style={styles.icon} className="fa fa-share" />
                                    {localizations.event_organize_again}
                                </button>
                                
                                {user && user.areStatisticsActivated && 
                                    <button
                                        key={'green2'} 
                                        style={styles.greenButton}
                                        onClick={onAdminDisplayStatForm}
                                        disabled={isLoading}
                                    >
                                        <i style={styles.icon} className="fa fa-line-chart" />
                                        {localizations.event_fill_statistics}
                                    </button>
                                }
                            </div>
                    :   !isCancelled && (
                        showBook 
                        ?   <div>
                                {this.state.showJoinCommunity && 
                                    <div style={styles.joinCommunityContainer}>
                                        <div style={styles.communityDesc}>
                                            <div style={styles.buttonIcon}>
                                                <img style={styles.circleIcon} src="/assets/images/icon_circle@3x.png"/>
                                                <div style={styles.numberContainer}>
                                                    <span style={styles.number}>
                                                        {this.state.mainCommunityCircle.memberCount}
                                                    </span>
                                                </div>
                                            </div>
                                            <div style={styles.circleDetails}>
                                                <div style={styles.leftSide}>
                                                    <div style={styles.top}>
                                                        <div style={styles.buttonText}>
                                                            {this.state.mainCommunityCircle.name}
                                                        </div>
                                                        {this.state.mainCommunityCircle.owner &&
                                                            <div style={styles.buttonPseudo}>
                                                                <div style={{...styles.avatar, backgroundImage: 'url('+this.state.mainCommunityCircle.owner.avatar || "../../img/profile.png" +')'}} />
                                                                {this.state.mainCommunityCircle.owner.pseudo}
                                                            </div>
                                                        }
                                                    </div>
                                                </div>
                                                <div style={styles.bottom}>
                                                    <div>
                                                        {this.state.mainCommunityCircle.mode === 'PRIVATE'
                                                        ? localizations.circles_private
                                                        : localizations.circles_public}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <button
                                            key={'join'}
                                            style={styles.orangeButton}
                                            onClick={notInCircle ? this.joinMainCommunity : this.inviteMainCommunity}
                                        >
                                            {notInCircle ? localizations.event_joinCommunity : localizations.event_inviteCommunity}
                                        </button>
                                    </div>
                                }
		                        {!(sportunity.survey && sportunity.survey.surveyDates && sportunity.survey.surveyDates.length > 1) &&
			                        <div style={hasImages > 0 ? {
				                        ...styles.buttonsContainer,
				                        bottom: hasImages > 1 ? 70 : 25
			                        } : {...styles.buttonsContainer, top: 25}}>
				                        <button
					                        key={'green'}
					                        style={styles.greenButton}
					                        onClick={isPotentialOpponent ? onOpponentBook : onBook}
					                        disabled={!enableBook && isLogin || !isActive || isLoading}
					                        title={enableBook ? 'Book' : 'You are not connected'}
				                        >
					                        <i style={styles.icon} className="fa fa-check"/>
					                        {showJoinWaitingList ? localizations.event_join_waiting_list : localizations.event_book}
					                        <span style={{marginLeft: 15}}
					                              data-tip={localizations.event_confirmation_popup_cancellation_policy_details}>
                                            <i
	                                            className="fa fa-question-circle"
	                                            aria-hidden="true"
                                            />
                                        </span>
				                        </button>
				                        {userIsInvited &&
				                        <button
					                        key={'red'}
					                        style={styles.redButton}
					                        onClick={onDeclineInvitation}
				                        >
					                        <i style={styles.icon} className="fa fa-times"/>
					                        {localizations.event_decline_invitation}
				                        </button>
				                        }
			                        </div>
		                        }
                            </div>
                        :   !isSecondaryOrganizer && !isPotentialSecondaryOrganizer &&
                                <div>
                                    {this.state.showJoinCommunity && 
                                        <div style={styles.joinCommunityContainer}>
                                            <div style={styles.communityDesc}>
                                                <div style={styles.buttonIcon}>
                                                    <img style={styles.circleIcon} src="/assets/images/icon_circle@3x.png"/>
                                                    <div style={styles.numberContainer}>
                                                        <span style={styles.number}>
                                                            {this.state.mainCommunityCircle.memberCount}
                                                        </span>
                                                    </div>
                                                </div>
                                                <div style={styles.circleDetails}>
                                                    <div style={styles.leftSide}>
                                                        <div style={styles.top}>
                                                            <div style={styles.buttonText}>
                                                                {this.state.mainCommunityCircle.name}
                                                            </div>
                                                            {this.state.mainCommunityCircle.owner &&
                                                                <div style={styles.buttonPseudo}>
                                                                    <div style={{...styles.avatar, backgroundImage: 'url('+this.state.mainCommunityCircle.owner.avatar || "../../img/profile.png" +')'}} />
                                                                    {this.state.mainCommunityCircle.owner.pseudo}
                                                                </div>
                                                            }
                                                        </div>
                                                    </div>
                                                    <div style={styles.bottom}>
                                                        <div>
                                                            {this.state.mainCommunityCircle.mode === 'PRIVATE'
                                                            ? localizations.circles_private
                                                            : localizations.circles_public}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button
                                                key={'join'}
                                                style={styles.orangeButton}
                                                onClick={notInCircle ? this.joinMainCommunity : this.inviteMainCommunity}
                                            >
                                                {notInCircle ? localizations.event_joinCommunity : localizations.event_inviteCommunity}
                                            </button>
                                        </div>
                                    }
                                    <div style={hasImages > 0 ? {...styles.buttonsContainer, bottom: hasImages > 1 ? 70 : 25} : {...styles.buttonsContainer, top: 25}}>
                                        <button
                                            key={'red'}
                                            style={styles.redButton}
                                            onClick={onCancel}
                                            title={'Cancel'}
                                            disabled={!isActive || isLoading}
                                        >
                                            <i style={styles.icon} className="fa fa-times" />
                                            {localizations.event_cancel}
                                            <span style={{marginLeft: 15}} data-tip={localizations.event_confirmation_popup_cancellation_policy_details}>
                                                <i
                                                    className="fa fa-question-circle"
                                                    aria-hidden="true"
                                                />
                                            </span>
                                        </button>
                                    </div>
                                </div>
                    )
                }
            </div>
        )
    }
}

styles = {
  container: {
    width: '100%',
    position: 'relative',
    borderBottom: '1px solid ' + colors.lightGray
  },
  menuContainer: {
    position: 'relative',
	  '@media (max-width: 900px)': {
		  width: '100%',
		  margin: '10px 0px'
	  },
  },
  buttonsContainer: {
    position: 'absolute',
    right: 25,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    '@media (max-width: 900px)': {
        position: 'relative',
        bottom: 'auto',
        top: 'auto',
        right: 'auto',
        flexDirection: 'column',
        marginTop: 5
    }
  },
  greenButton: {
    backgroundColor: colors.green,
    color: colors.white,
    padding: '7px',
    marginRight: 5,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 18,
    minWidth: 180,
    height: 40,
    fontFamily: 'Lato',
    cursor: 'pointer',
    border: 0,
    transition: 'all cubic-bezier(0.22,0.61,0.36,1) .3s',
    ':hover': {
        filter: 'brightness(0.9)'
    },
    ':disabled': {
        backgroundColor: colors.lightGray,
        color: colors.darkGray
    },
    ':active': {
        outline: 'none'
    },
    '@media (max-width: 900px)': {
        width: '100%'
    }
  },
  redButton: {
    backgroundColor: colors.redGoogle,
    color: colors.white,
    padding: '7px',
    marginRight: 5,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 18,
    minWidth: 180,
    height: 40,
    fontFamily: 'Lato',
    cursor: 'pointer',
    border: 0,
    transition: 'all cubic-bezier(0.22,0.61,0.36,1) .3s',
    ':hover': {
        filter: 'brightness(0.9)'
    },
    ':disabled': {
        backgroundColor: colors.lightGray,
        color: colors.darkGray
    },
    ':active': {
        outline: 'none'
    },
    '@media (max-width: 900px)': {
        width: '100%',
        marginTop: 10
    }
  },
  grayButton: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.lightGray,
    color: colors.darkGray,
    fontSize: 20,
    padding: '7px',
    cursor: 'pointer',
    height: 40,
    width: 40,
    border: 0,
    transition: 'all cubic-bezier(0.22,0.61,0.36,1) .3s',
	  '@media (max-width: 900px)': {
		  width: '100%',
	  },
    ':hover': {
        filter: 'brightness(0.9)'
    },
    ':active': {
        outline: 'none'
    },
  },
  icon: {
      fontSize: 24,
      marginRight: 7
  },
  plusMenuContainer: {
      position: 'absolute',
      zIndex: 200,
      color: colors.darkGray,
      width: 120,
      right: 0,
  },
  plusMenuItem: {
    width: '100%',
    backgroundColor: colors.lightGray,
    color: colors.darkGray,
    border: 'none',
    fontSize: 16,
    fontFamily: 'Lato',
    padding: '5px 10px',
    cursor: 'pointer',
    transition: 'all cubic-bezier(0.22,0.61,0.36,1) .3s',
    ':hover': {
        filter: 'brightness(0.9)'
    },
    ':active': {
        outline: 'none'
    }
  },
  separator: {
    borderTop: '1px solid rgba(0,0,0,0.25)',
    borderBottom: '0px solid rgba(0,0,0,0.25)',
    borderLeft: '0px solid rgba(0,0,0,0.25)',
    borderRight: '0px solid rgba(0,0,0,0.25)',
    margin: 0
  },
  joinCommunityContainer: {
    position: 'absolute',
    left: 25,
    top: 25, 
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    '@media (max-width: 900px)': {
        position: 'relative',
        bottom: 'auto',
        top: 'auto',
        left: 'auto',
        justifyContent: 'space-between',
        marginTop: 5
    }
  },
  communityDesc: {
    backgroundColor: colors.lightGray,
    padding: '7px',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 18,
    minWidth: 180,
    minHeight: 40,
    fontFamily: 'Lato',
    border: 0,
    '@media (max-width: 900px)': {
        width: '100%'
    }
  },
  orangeButton: {
    backgroundColor: colors.bloodOrange,
    color: colors.white,
    padding: '7px',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 18,
    height: 40,
    fontFamily: 'Lato',
    cursor: 'pointer',
    border: 0,
    width: '100%',
    marginTop: 3,
    transition: 'all cubic-bezier(0.22,0.61,0.36,1) .3s',
    ':hover': {
        filter: 'brightness(0.9)'
    },
    ':active': {
        outline: 'none'
    }
  },
  buttonIcon: {
    color: colors.blue,
    position: 'relative',
    display: 'flex',
    flex: 1,
    marginRight: 5
  },
  circleIcon: {
    width: 35,
    height: 25,
  },
  buttonLink: {
    color: colors.black,
    textDecoration: 'none',
    cursor: 'pointer',
    flex: 7,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  numberContainer: {
    position: 'absolute',
    top: '2px',
    left: '11px',
    width: 24,
    textAlign: 'center'
  },
  number: {
    fontSize: 15,
    fontWeight: 'bold'
  },
  buttonText: {
    textDecoration: 'none',
    color: colors.blue,
    fontSize: 18,
  },
  buttonPseudo: {
    textDecoration: 'none',
    color: colors.darkGray,
    fontSize: 16,
    lineHeight: '30px',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 3
  },
  circleDetails: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    flex: 8
  },
  top: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },
  bottom: {
    display: 'flex',
    flexDirection: 'column',
    fontSize: 14,
    color: colors.darkGray,
    marginLeft: 10
    //color: colors.blue
  },
  avatar: {
    width: 25,
    height: 25,
    borderRadius: '50%',
    marginRight: 7,
    backgroundPosition: '50% 50%',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
  }
};


export default withRouter(Radium(ContentHeader))