import Relay from 'react-relay';

export default class newCarPoolingMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      newCarPooling
    }`
  }
  
  getVariables() {
    return  {
      sportunityID: this.props.sportunityIDVar,
      carPooling: this.props.carPoolingVar
    }
  }

  getFatQuery() {
      return Relay.QL`
        fragment on newCarPoolingPayload {
          clientMutationId,
          viewer {
            id,
            sportunity 
          }
        }
      `
   
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
        fieldIDs: {
          viewer: this.props.viewer.id,
        },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
      }
    `
  };

}
