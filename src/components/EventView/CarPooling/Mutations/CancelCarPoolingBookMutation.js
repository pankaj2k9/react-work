import Relay from 'react-relay';

export default class cancelCarPoolingBookMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      cancelCarPoolingBook
    }`
  }
  
  getVariables() {
    return  {
      sportunityID: this.props.sportunityIDVar,
      carPoolingID: this.props.carPoolingIDVar,
      userID: this.props.userIDVar, 
    }
  }

  getFatQuery() {
      return Relay.QL`
        fragment on cancelCarPoolingBookPayload {
          clientMutationId,
          viewer {
            id,
            sportunity {
                carPoolings
            }
          }
        }
      `
   
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
        fieldIDs: {
          viewer: this.props.viewer.id,
        },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
      }
    `,
  };

}
