import React from 'react';
import Relay from 'react-relay';
import Radium from 'radium';
import AlertContainer from 'react-alert'
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import NewUpdateCarPoolingModal from './NewUpdateCarPoolingModal.js';

import CancelCarPoolingMutation from './Mutations/CancelCarPoolingMutation.js';
import BookCarPoolingMutation from './Mutations/BookCarPoolingMutation.js';
import CancelCarPoolingBookMutation from './Mutations/CancelCarPoolingBookMutation.js';
import newCarPoolingMutation from './Mutations/NewCarPoolingMutation';
import modifyCarPoolingMutation from './Mutations/ModifyCarPoolingMutation';
import askCarPoolingMutation from './Mutations/AskCarPoolingMutation';

import localizations from '../../Localizations'
import { colors } from '../../../theme';
import {styles} from './styles.js';

class CarPooling extends React.Component {

    constructor() {
        super(); 
        this.alertOptions = {
            offset: 60,
            position: 'top right',
            theme: 'light',
            transition: 'fade',
        };

        this.state = {
            displayNewCarPoolingModal: false,
            displayUpdateCarPoolingModal: false,
            updatingCarPooling: null,
            userIsDriver: null,
            userIsPassenger: null,
            displayAskCarPoolingButton: true,
        }
    }

    componentDidMount() {
        const {viewer:{me}, sportunity:{carPoolings}} = this.props; 
        this._userIsDriver(carPoolings, me);
        this._userIsPassenger(carPoolings, me)
    }

    componentWillReceiveProps = (nextProps) => {
        const {viewer:{me}, sportunity} = nextProps; 
        if (sportunity) {
            this._userIsDriver(sportunity.carPoolings, me);
            this._userIsPassenger(sportunity.carPoolings, me)
        }
    }

    _userIsDriver = (carPoolings, user) => {
        let result = null ;
        
        carPoolings.forEach(carPooling => {
            if (carPooling.driver.id === user.id)
                result = carPooling.id;
        })

        this.setState({
            userIsDriver: result
        })
    }

    _userIsPassenger = (carPoolings, user) => {
        let result = null ;
        carPoolings.forEach(carPooling => {
            if (carPooling.passengers && carPooling.passengers.length > 0) {
                if (carPooling.passengers.findIndex(passenger => passenger.id === user.id) >= 0)
                    result = carPooling.id;
            }
        })

        this.setState({
            userIsPassenger: result
        })
    }

    updateCarPooling = (carPooling) => {
        this.setState({
            updatingCarPooling: carPooling,
            displayUpdateCarPoolingModal: true
        })
    }

    cancelCarPooling = (carPooling) => {
        const {viewer, sportunity} = this.props;

        let params = {
            viewer: viewer,
            sportunityIDVar: sportunity.id,
            carPoolingIDVar: carPooling.id,
        }

        this.props.relay.commitUpdate(
            new CancelCarPoolingMutation(params),{
              onSuccess: (response) => {
                this.msg.show(localizations.event_carPooling_updateSucces, { ///////////////////////
                    time: 2000,
                    type: 'success',
                });
                this.props.relay.forceFetch()
                const that = this
                setTimeout(function() {
                    that.msg.removeAll();
                }, 2000);
              },
              onFailure: (error) => {
                this.msg.show(localizations.event_carPooling_updateFailed, {    ///////////////////////
                  time: 0,
                  type: 'error',
                });

                setTimeout(function() {
                    this.msg.removeAll();
                }, 2000);
              },
            }
        );
    }
    
    bookCarPooling = (carPooling) => {
        const {viewer, viewer: {me}, sportunity} = this.props;

        let params = {
            viewer: viewer,
            sportunityIDVar: sportunity.id,
            carPoolingIDVar: carPooling.id,
            userIDVar: me.id
        }

        this.props.relay.commitUpdate(
            new BookCarPoolingMutation(params),{
              onSuccess: (response) => {
                this.msg.show(localizations.event_carPooling_updateSucces, { ///////////////////////
                    time: 2000,
                    type: 'success',
                });
                this.props.relay.forceFetch()
                setTimeout(() => {
                    this.msg.removeAll();
                }, 2000);
              },
              onFailure: (error) => {
                this.msg.show(localizations.event_carPooling_updateFailed, {    ///////////////////////
                  time: 0,
                  type: 'error',
                });

                setTimeout(() => {
                    this.msg.removeAll();
                }, 2000);
              },
            }
        );
    }

    cancelCarPoolingBook = (carPooling) => {
        const {viewer, viewer: {me}, sportunity} = this.props;

        let params = {
            viewer: viewer,
            sportunityIDVar: sportunity.id,
            carPoolingIDVar: carPooling.id,
            userIDVar: me.id
        }

        this.props.relay.commitUpdate(
            new CancelCarPoolingBookMutation(params),{
              onSuccess: (response) => {
                this.msg.show(localizations.event_carPooling_updateSucces, { ///////////////////////
                    time: 2000,
                    type: 'success',
                });
                const that = this
                setTimeout(function() {
                    that.msg.removeAll();
                }, 1500);
                this.props.relay.forceFetch()
              },
              onFailure: (error) => {
                this.msg.show(localizations.event_carPooling_updateFailed, {    ///////////////////////
                  time: 0,
                  type: 'error',
                });

                setTimeout(function() {
                    this.msg.removeAll();
                }, 2000);
              },
            }
        );
    }

    newCarPooling = (params) => {
        this.props.relay.commitUpdate(
            new newCarPoolingMutation(params),{
              onSuccess: (response) => {
                this.msg.show(localizations.event_carPooling_updateSucces, { ///////////////////////
                    time: 2000,
                    type: 'success',
                });
                const that = this
                setTimeout(function() {
                    that.msg.removeAll();
                }, 1500);
                this.setState({
                    displayNewCarPoolingModal: false
                })
                this.props.relay.forceFetch()
              },
              onFailure: (error) => {
                this.msg.show(localizations.event_carPooling_updateFailed, {    ///////////////////////
                  time: 0,
                  type: 'error',
                });

                setTimeout(function() {
                    this.msg.removeAll();
                    this.setState({
                        displayNewCarPoolingModal: false
                    })                
                }, 2000);
              },
            }
        );
    }

    modifyCarPooling = (params) => {
        this.props.relay.commitUpdate(
            new modifyCarPoolingMutation(params),{
              onSuccess: (response) => {
                this.msg.show(localizations.event_carPooling_updateSucces, { ///////////////////////
                    time: 2000,
                    type: 'success',
                });
                this.setState({
                    displayUpdateCarPoolingModal: false
                })
                this.props.relay.forceFetch()
                const that = this
                setTimeout(function() {
                    that.msg.removeAll();
                }, 1500);
              },
              onFailure: (error) => {
                this.msg.show(localizations.event_carPooling_updateFailed, {    ///////////////////////
                  time: 0,
                  type: 'error',
                });

                setTimeout(function() {
                    this.msg.removeAll();
                    this.setState({
                        displayUpdateCarPoolingModal: false
                    })                
                }, 2000);
              },
            }
        );
    }

    askForACarPooling = () => {
        const {viewer, viewer: {me}, sportunity} = this.props;
        
        let params = {
            viewer: viewer,
            sportunityIDVar: sportunity.id,
        };
        this.props.relay.commitUpdate(
            new askCarPoolingMutation(params),{
              onSuccess: (response) => {
                this.msg.show(localizations.event_carPooling_askSuccess, { ///////////////////////
                    time: 2000,
                    type: 'success',
                });
                this.setState({
                    displayAskCarPoolingButton: false
                })
                setTimeout(() => {
                    this.msg.removeAll();
                }, 1500);
              },
              onFailure: (error) => {
                this.msg.show(localizations.event_carPooling_updateFailed, {    ///////////////////////
                  time: 0,
                  type: 'error',
                });

                setTimeout(() => {
                    this.msg.removeAll();
                }, 2000);
              },
            }
        );
    }

    render() {
        let {viewer, sportunity, isPast, isCancelled, isAdmin, userIsParticipant} = this.props;
        const {userIsDriver, userIsPassenger} = this.state; 

        console.log('carPoolings', sportunity.carPoolings);

        return (
            <div>
                <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
                <div style={styles.title}>
                    {localizations.event_carPooling}
                </div>

                {!isPast && !isCancelled && (isAdmin || userIsParticipant) &&
                (sportunity && sportunity.carPoolings && sportunity.carPoolings.length > 0 
                ?   sportunity.carPoolings.map((carPooling, index) => (
                        <div key={index}>
                            {index >= 1 && 
                                <div
                                    style={{borderBottom: '1px solid '+colors.lightGray, margin: '25px 20px'}}
                                />
                            }
                            <div style={styles.informationContainer}>
                                <span style={styles.label}>
                                    {localizations.event_carPooling_driver + ': '}
                                </span>
                                <span style={styles.driverContainer}>
                                    <div style={{...styles.icon, backgroundImage: carPooling.driver.avatar ? 'url('+ carPooling.driver.avatar +')' : 'url("https://sportunitydiag304.blob.core.windows.net/avatars/default-avatar.png")'}} />
                                    {carPooling.driver.pseudo}
                                </span>

                            </div>
                            <div style={styles.informationContainer}>
                                <span style={styles.label}>
                                    {localizations.event_carPooling_departure + ': '}
                                </span>
                                <span style={styles.information}>
                                    {moment(carPooling.starting_date).format('DD MMMM, H:mm')}
                                </span>
                            </div>

                            <div style={styles.informationContainer}>
                                <span style={styles.label}>
                                    {localizations.event_carPooling_place + ': '}
                                </span>
                              { carPooling.address &&
                                <span style={styles.information}>
                                    {carPooling.address.address + ', ' + carPooling.address.city}
                                </span>
                              }
                            </div>

                            <div style={styles.informationContainer}>
                                <span style={styles.label}>
                                    {localizations.event_carPooling_sitsNumber + ': '}
                                </span>
                                <span style={styles.information}>
                                    {carPooling.passengers 
                                    ?   carPooling.number_of_sits - carPooling.passengers.length 
                                    :   carPooling.number_of_sits
                                    }
                                </span>
                            </div>
                            
                            <div style={styles.informationContainer}>
                                <span style={styles.label}>
                                    {localizations.event_carPooling_passengers + ': '}
                                </span>
                                <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'flex-start', flex: 3}}>
                                    {carPooling.passengers && carPooling.passengers.length > 0
                                    ?   carPooling.passengers.map(passenger => (
                                            <span key={passenger.id} style={styles.driverContainer}>
                                                <div style={{...styles.icon, backgroundImage: passenger.avatar ? 'url('+ passenger.avatar +')' : 'url("https://sportunitydiag304.blob.core.windows.net/avatars/default-avatar.png")'}} />
                                                {passenger.pseudo}
                                            </span>
                                        ))
                                    :   <span style={styles.information}>
                                            {localizations.event_carPooling_none}
                                        </span>
                                    }
                                </div>
                            </div>

                            {!userIsDriver && !userIsPassenger && carPooling.number_of_sits > carPooling.passengers.length && 
                                <div style={{display: 'flex', flexDirection: 'row', width: '100%', justifyContent: 'space-around', alignItems: 'center'}}>
                                    <button
                                        style={styles.greenButton}
                                        onClick={() => this.bookCarPooling(carPooling)}
                                    >
                                        {localizations.event_carPooling_book}
                                    </button>
                                </div>
                            }

                            {userIsDriver === carPooling.id &&
                               <div style={{display: 'flex', flexDirection: 'row', width: '100%', justifyContent: 'space-around', alignItems: 'center'}}>
                                    <button
                                        style={styles.greenButton}
                                        onClick={() => this.updateCarPooling(carPooling)}
                                    >
                                        {localizations.event_carPooling_modify}
                                    </button>
                                </div>
                            }

                            {userIsDriver === carPooling.id &&
                               <div style={{display: 'flex', flexDirection: 'row', width: '100%', justifyContent: 'space-around', alignItems: 'center'}}>
                                    <button
                                        style={styles.redButton}
                                        onClick={() => this.cancelCarPooling(carPooling)}
                                    >
                                        {localizations.event_carPooling_cancel}
                                    </button>
                                </div>
                            }
                            {userIsPassenger === carPooling.id &&
                                <div style={{display: 'flex', flexDirection: 'row', width: '100%', justifyContent: 'space-around', alignItems: 'center'}}>
                                    <button
                                        style={styles.redButton}
                                        onClick={() => this.cancelCarPoolingBook(carPooling)}
                                    >
                                        {localizations.event_carPooling_cancel}
                                    </button>
                                </div>
                            }

                        </div>
                    ))
                :   <div style={styles.text}>
                        {localizations.event_carPooling_nothing}
                    </div>
                )}

                {!isPast && !isCancelled && (isAdmin || userIsParticipant) && !userIsDriver && !userIsPassenger &&
                    <div style={{display: 'flex', flexDirection: 'row', width: '100%', justifyContent: 'space-around', alignItems: 'center'}}>
                        <button
                            style={styles.greenButton}
                            onClick={() => this.setState({displayNewCarPoolingModal: true})}
                        >
                            {localizations.event_carPooling_create}
                        </button>
                    </div>
                }

                {!isPast && !isCancelled && (isAdmin || userIsParticipant) && !userIsDriver && !userIsPassenger && this.state.displayAskCarPoolingButton && 
                    <div style={{display: 'flex', flexDirection: 'row', width: '100%', justifyContent: 'space-around', alignItems: 'center'}}>
                        <button
                            style={styles.greenButton}
                            onClick={this.askForACarPooling}
                        >
                            {localizations.event_carPooling_ask}
                        </button>
                    </div>
                }

                {isPast && 
                    <div style={{...styles.msgContainer, marginTop: 10}}>
                        <i style={styles.exclamationIcon} className='fa fa-exclamation-circle fa-5x' />
                        <div>
                            <p style={styles.errorMsg}>{localizations.event_carPooling_past}</p>
                        </div>
                    </div>
                }
                {isCancelled && 
                    <div style={{...styles.msgContainer, marginTop: 10}}>
                        <i style={styles.exclamationIcon} className='fa fa-exclamation-circle fa-5x' />
                        <div>
                            <p style={styles.errorMsg}>{localizations.event_carPooling_cancelled}</p>
                        </div>
                    </div>
                }
                {!isPast && !isCancelled && !userIsParticipant && !isAdmin &&
                    <div>
                        <div style={styles.carPoolingStat}>
                            <div style={styles.image}>
                                <img src="/assets/images/car.png"/>
                            </div>
                            <div style={styles.textContainer}>
                                <div style={styles.row}>
                                    <div style={styles.text}>
                                        {localizations.event_carPooling_enabled}
                                    </div>
                                    <div style={styles.text}>
                                        {sportunity.carPoolings ? sportunity.carPoolings.length : 0}
                                    </div>
                                </div>
                                <div style={styles.row}>
                                    <div style={styles.text}>
                                        {localizations.event_carPooling_asked + " "}
                                    </div>
                                    <div style={styles.text}>
                                        {0}
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div style={{...styles.msgContainer, marginTop: 10}}>
                        <i style={styles.exclamationIcon} className='fa fa-exclamation-circle fa-5x' />
                            <div>
                            <p style={styles.errorMsg}>{localizations.event_carPooling_errorMessage}</p>
                            </div>
                        </div>
                    </div>
                }

                {this.state.displayNewCarPoolingModal && 
                    <NewUpdateCarPoolingModal
                        viewer={viewer}
                        sportunity={sportunity}
                        isModalVisible={this.state.displayNewCarPoolingModal}
                        closeModal={() => this.setState({displayNewCarPoolingModal: false})}
                        newCarPooling={this.newCarPooling}
                        userCountry={this.props.userCountry}
                    />
                }

                {this.state.displayUpdateCarPoolingModal && 
                    <NewUpdateCarPoolingModal
                        viewer={viewer}
                        sportunity={sportunity}
                        carPooling={this.state.updatingCarPooling}
                        isModalVisible={this.state.displayUpdateCarPoolingModal}
                        closeModal={() => this.setState({displayUpdateCarPoolingModal: false})}
                        updateCarPooling={this.modifyCarPooling}
                        userCountry={this.props.userCountry}
                    />
                }
            </div>
        )
    }
}

const dispatchToProps = (dispatch) => ({
})
  
const stateToProps = (state) => ({
    userCountry: state.globalReducer.userCountry,
})
  
let ReduxContainer = connect(
    stateToProps,
    dispatchToProps
)(Radium(CarPooling));

export default Relay.createContainer(Radium(ReduxContainer), {
    fragments: {
        viewer: () => Relay.QL`
            fragment on Viewer {
                id
                me {
                    id
                    appCountry
                }
                ${CancelCarPoolingMutation.getFragment('viewer')}
                ${BookCarPoolingMutation.getFragment('viewer')}
                ${CancelCarPoolingBookMutation.getFragment('viewer')}
                ${newCarPoolingMutation.getFragment('viewer')}
                ${modifyCarPoolingMutation.getFragment('viewer')}
                ${askCarPoolingMutation.getFragment('viewer')}
            }
        `,
        sportunity: () => Relay.QL`
            fragment on Sportunity {
                id
                beginning_date
                carPoolings {
                    id,
                    driver {
                        id,
                        pseudo,
                        avatar
                    },
                    address {
                        address,
                        city,
                        zip,
                        country
                    },
                    starting_date,
                    number_of_sits
                    passengers {
                        id,
                        pseudo,
                        avatar
                    }
                }
            }
        `
    }
});