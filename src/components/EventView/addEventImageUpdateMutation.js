import Relay from 'react-relay';

export default class AddEventImageUpdateMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      addEventImage
    }`;
  }

  getVariables() {
    return {
      sportunityID: this.props.sportunity.id,
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on addSportunityImagePayload{
        viewer {
          sportunity {
            images
          }
        }
        sportunity {
          id
          images
        }
      }
    `;
  }

  getFiles() {
    return {
      eventimage1: this.props.file,
    };
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        sportunity: this.props.sportunity.id,
      },
    }];
  }

  static fragments = {
    sportunity: () => Relay.QL`
      fragment on Sportunity {
        id,
        images
      }
    `,
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
      }
    `,
  };

}
