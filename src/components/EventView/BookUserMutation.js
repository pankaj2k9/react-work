import Relay from 'react-relay';

export default class UpdateSportunity extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation {updateSportunity}`;
  }

  getVariables() {
    // test status Sportunity
    // canceling

    return {
      sportunityID: this.props.sportunity.id,
      sportunity: {
        participants: this.props.user.id,
        paymentMethodId: this.props.paymentMethod,
        invited: this.props.invited,
        paymentByWallet: this.props.paymentByWallet,
      },
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on updateSportunityPayload{
        viewer {
          sportunity {
						participants
						waiting
            status
					}
        }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        viewer: this.props.viewer.id,
      },
    }];
  }

  getOptimisticResponse() {
    const viewerPayload = { id: this.props.viewer.id };

    return {
      sportunity: {
        id: this.props.sportunity.id,
        participants: this.props.sportunity.participants.concat(this.props.viewer.me),
      },
      viewer: viewerPayload,
    };
  }

  static fragments = {
    sportunity: () => Relay.QL`
      fragment on Sportunity {
        id,
        participants
				waiting
      }
    `,
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
        sportunities(last: 10) {
          edges
        },
        me
      }
    `,
    user: () => Relay.QL`
      fragment on User {
        id,
        paymentMethods,
      }
    `,
  };
}
