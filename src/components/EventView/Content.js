import React from 'react'
import Radium from 'radium'
import Relay from 'react-relay'
import AlertContainer from 'react-alert'
import withRouter from 'react-router/lib/withRouter'

import { colors } from '../../theme'
import Comments from './Comments'
import Feedback from './Feedback';
import CarPooling from './CarPooling';
import ContentHeader from './ContentHeader';
import TabHeader from './TabHeader';
import localizations from '../Localizations'
import Info from './Info';
import Media from './Media'
import {Link} from 'react-router'
import Slider from "./Slider";
import Sidebar from "./Sidebar";

let styles;

const Title = ({ children }) => <h2 style={styles.title}>{children}</h2>;

class Content extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoginError: false,
      showFeedBack: false,
      activeTab: 'main'
    };
    this.alertOptions = {
      offset: 14,
      position: 'top right',
      theme: 'light',
      transition: 'fade',
      time: 0,
    };
  }

  componentDidMount() {
    if (this.props.location.hash === '#chat') {
      this.setState({
        activeTab: 'chat'
      })
    }
  }


  _handlerFeedbackClicked = () => {
    this.setState({
      showFeedBack: true,
    })
  };

  showError = (errorMsg) => {
    this.msg.show(errorMsg, {
      time: 4000,
      type: 'error',
    });
    setTimeout(() => {
      this.msg.removeAll();
    }, 4000);
  };

  validTab = (newTab) => {
    if (newTab === 'carPooling' && (this.props.isCancelled || this.props.isPast))
    {
      if (this.props.isPast)
        this.showError(localizations.event_view_car_pooling_past_error);
      else
        this.showError(localizations.event_view_car_pooling_cancelled_error);
      return false
    }
    else if (newTab === 'chat' && (!this.props.shouldShowChat || !this.props.chat))
    {
      this.showError(localizations.event_view_chat_error);
      return false
    }
    return true
  };

  _changeActiveTab = (newTab) => {
    // if (this.validTab(newTab)) {
    console.log(newTab);
      this.setState({
        activeTab: newTab
      })
    // }
  };

  render() {
    const {
      sportunity,
      status,
      showBook,
      showJoinWaitingList,
      enableBook,
      onBook,
      onCancel,
      onAdminModify,
      onAdminCancel,
      onAdminReOrganize,
      onAdminDisplayStatForm,
      onDeclineInvitation,
      onOpponentBook,
      viewer,
      isActive,
      isPast,
      isSecondaryOrganizer,
      isPotentialSecondaryOrganizer,
      isAdmin,
      isAuthorizedAdmin,
      isPotentialOpponent,
      onSeeAsAdmin,
      props,
      isLogin,
      isCancelled,
      userIsInvited,
      userIsOnWaitingList,
      userIsParticipant,
      shouldShowChat,
      chat,
      organizers,
      isLoading,
      user,
	    userWasInvited
    } = this.props ;

    let mainOrganizer = sportunity.organizers.find(org => org.isAdmin);

    return(
      <div style={styles.wrapper}>
        <div style={(sportunity.images && sportunity.images.length > 0) ? styles.containerWithImages : styles.container}>
          <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
	        <ContentHeader
		        {...this.props}
	        />
          <TabHeader
            viewer={viewer}
            sportunity={sportunity}
            activeTab={this.state.activeTab}
            onChange={(tab) => this._changeActiveTab(tab)}
          />
        
          <div style={styles.content}>
            {
              this.state.activeTab === "main" &&
              <Info
                viewer={viewer}
                sportunity={sportunity}
                user={user}
                {...this.props}
              />
            }
            {this.state.activeTab === 'carPooling' &&
            <CarPooling
              language={() => localizations.getLanguage()}
              sportunity={sportunity}
              viewer={viewer}
              isCancelled={this.props.isCancelled}
              isPast={this.props.isPast}
              isAdmin={isAdmin}
              userIsParticipant={userIsParticipant}
            />
            }
            {
              this.state.activeTab === 'chat' && (isAdmin || userIsParticipant || isSecondaryOrganizer) &&
              <section>
                <Title>{localizations.event_comments}</Title>
                <Comments
                  viewer={viewer}
                  user={this.props.user}
                  me={viewer.me}
                  chat={chat}
                />
              </section>
            }
            {
              this.state.activeTab === 'chat' && !isAdmin && !userIsParticipant && !isSecondaryOrganizer &&

              <section style={styles.msgContainer}>
                <i
                  className='fa fa-exclamation-circle fa-5x'
                />
                <div>
                  {/*TODO ajouté dans localisation avec l'organisateur dynamique*/}
                  <p style={styles.text}>
                    {localizations.event_chat_errorMessage1 + mainOrganizer.organizer.pseudo + localizations.event_chat_errorMessage2}
                  </p>
                  <p style={styles.text}>
                    {localizations.event_chat_errorMessage3}
                    <Link to={'/profile-view/' + mainOrganizer.organizer.id}>
                      {mainOrganizer.organizer.pseudo}
                    </Link>
                    {localizations.event_chat_errorMessage4}
                  </p>
                </div>
              </section>
            }
            {
              this.state.activeTab === 'media' &&
              <Media
                isAdmin={isAdmin}
                sportunity={sportunity}
                viewer={viewer}
              />
            }
            {/* isCancelled &&
            <Feedback
              viewer={viewer}
              sportunity={sportunity}
            />
          */}
          </div>
        </div>
	      {this.props.sportunity &&
		      <Sidebar
			      viewer={this.props.viewer}
			      user={this.props.viewer.me}
			      sportunity={this.props.sportunity}
			      invited_circles={this.props.sportunity.invited_circles}
			      actionParticipant={this._handleShowConfirmationParticipantPopup}
			      onConfirm={this.props.onConfirm}
			      isAdmin={isAdmin}
			      isSecondaryOrganizer={isSecondaryOrganizer}
			      isPotentialOpponent={isPotentialOpponent}
			      userIsInvited={userIsInvited}
			      userWasInvited={userWasInvited}
			      userIsParticipant={userIsParticipant}
			      userIsOnWaitingList={userIsOnWaitingList}
			      isActive={isActive}
			      handleOrganizerAddParticipants={this._handleOrganizerAddParticipants}
			      handleOrganizerAddInviteds={this._handleOrganizerAddInviteds}
			      {...this.props}
		      />
	      }
      </div>
    )
  }
}

styles = {
  wrapper: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
	  '@media (max-width: 600px)': {
		  flexDirection: 'column',
	  }
  },
  container: {
    width: 'calc(100% - 200px)',
	  '@media (max-width: 600px)': {
		  width: '100%',
	  }
  },
  containerWithImages: {
    width: 'calc(100% - 200px)',
	  '@media (max-width: 600px)': {
		  width: '100%',
	  }
  },
  content: {
    flexGrow: '1',
    padding: '40px',
    fontFamily: 'Lato',
    color: 'rgba(0,0,0,0.65)',
    '@media (max-width: 700px)': {
      padding: '20px',
    },
  },
  book: {
    backgroundColor: colors.green,
    color: colors.white,
    width: 230,
    // height: 70,
    borderRadius: 100,
    borderStyle: 'none',
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
    fontSize: 22,
    cursor: 'pointer',
    padding: '13px 5px',

    position: 'relative',
    // left: 'calc(50% - 115px)',


    ':disabled': {
      cursor: 'not-allowed',
      backgroundColor: colors.gray,
    },
  },
  buttonContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 15,
  },
  title: {
    fontSize: 32,
    fontWeight: 500,
    marginBottom: 30,
  },
  msgContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    textAlign: 'center',
  },
  text: {
    fontSize: 15,
    fontFamily: 'lato',
  },
  textContainer: {
    width: '60%',
    display: 'flex',
    flexDirection: 'column',
    marginRight: 20,
  },
  carPoolingStat: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  }
};


export default Relay.createContainer(withRouter(Radium(Content)), {

  fragments: {
    sportunity: () => Relay.QL`
      fragment on Sportunity {
        id
        description,
        organizers {
          isAdmin
          organizer {
            pseudo
            id
          }
        }
        ${Info.getFragment('sportunity')},
        ${Feedback.getFragment('sportunity')},
        ${CarPooling.getFragment('sportunity')}
        ${Media.getFragment('sportunity')}
        ${Sidebar.getFragment('sportunity')},
        participantRange {
          from,
          to,
        },
        kind
        survey {
          isSurveyTransformed
          surveyDates {
            beginning_date
            ending_date
            answers {
              user {
                id
                pseudo
                avatar
              }
              answer
            }
          }
        }
        invited_circles (last: 10) {
          edges {
            node {
              id,
              name,
              mode
              type
              memberCount
              members {
                id
              }
              owner {
                id
                pseudo
                avatar
              }
            }
          }
        }
        address {
          address
          city
					position {
            lat
            lng
          }
        },
        images
        sexRestriction
        ageRestriction{
          from, 
          to
        }
        sport {
          allLevelSelected,
          levels {
            id
            EN {
              name
              skillLevel
              description
            }
            FR {
              name
              skillLevel
              description
            }
            DE {  
              name
              skillLevel
              description
            } 
          }
          certificates {
            name {
              EN,
              FR,
              DE
            }
          }
          positions {
            EN
            FR,
            DE
          }
        }
      }
    `,
    chat: () => Relay.QL`
      fragment on Chat {
        id,
        ${Comments.getFragment('chat')}
      }
    `,
    viewer: () => Relay.QL`
      fragment on Viewer {
        ${Comments.getFragment('viewer')}
        ${Feedback.getFragment('viewer')}
        ${CarPooling.getFragment('viewer')}
        ${Info.getFragment('viewer')}
        ${Media.getFragment('viewer')}
        ${Sidebar.getFragment('viewer')}
      }
    `,
    user: () => Relay.QL`
      fragment on User {
        id
        profileType
        ${Comments.getFragment('user')}
        ${Info.getFragment('user')}
        ${Sidebar.getFragment('user')},
        areStatisticsActivated
      }
    `
  },
});
