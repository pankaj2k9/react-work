import Relay from 'react-relay';
/**
*  Add feedback to user mutation
*/
export default class UpdateUserFeedbackMutation extends Relay.Mutation {
  /**
  *  Mutation
  */
  getMutation() {
    return Relay.QL`mutation Mutation{
      upUser
    }`;
  }
  /**
  *  Variables
  */
  getVariables() {
    return {
      userID: this.props.userID,
      user: {
        feedbacks: {
          text: this.props.text,
          rating: this.props.rating,
          author: this.props.author,
          createdAt: this.props.createdAt,
        }
      },
    };
  }
  /**
  *  Fat query
  */
  getFatQuery() {
    return Relay.QL`
      fragment on upUserPayload {
        clientMutationId,
        user {
          id,
          feedbacks
        }
      }
    `;
  }

  /**
  *  Config
  */
  getConfigs() {
    return [{
      type: 'RANGE_ADD',
      parentName: 'user',
      parentID: this.props.userID,
      connectionName: 'FeedbackConnection',
      edgeName: 'feedbacks',
      rangeBehaviors: () => 'prepend',
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        me {
          id
        }
      }
    `,
  };
}
