import Relay from 'react-relay';

export default class RefuseInvitationMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation {updateSportunity}`;
  }

  getVariables() { 
    return {
      sportunityID: this.props.sportunity.id,
      sportunity:{
        invited: {
          user: this.props.userId,
          answer: "NO"
        }
      },
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on updateSportunityPayload{
        viewer {
          sportunities
        }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        viewer: this.props.viewer.id,
      },
    }];
  }

  static fragments = {
    sportunity: () => Relay.QL`
      fragment on Sportunity {
        id,
        invited {
          user {
            id,
            pseudo,
            avatar
          }
          answer
        }
      }
    `,
    viewer: () => Relay.QL`
    fragment on Viewer {
        id
        sportunities(last: 10) {
          edges
        }
      }
    `
  };
}