import React from 'react';
import Radium from 'radium';
import AlertContainer from 'react-alert'
import Relay from 'react-relay'
import Helmet from 'react-helmet';
import platform from 'platform';
import ConfirmationModal, {confirmModal} from '../common/ConfirmationModal'
import {displayChooseAccountModal} from '../common/SwitchAccountOnPrivate';
//import moment from 'moment';
import moment from 'moment-timezone';
import { colors } from '../../theme';
import { browserHistory } from 'react-router'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import AppHeader from '../common/Header/Header'
import Footer from '../common/Footer/Footer'
import Wrapper from './Wrapper'
import Header from './Header'
import Sidebar from './Sidebar'
import Content from './Content'
import Sharing from './Sharing'
import ConfirmBookingPopup from './ConfirmBookingPopup'
import AddACard from './AddACard';
import CompletePersonProfilePopup from './CompletePersonProfilePopup'
import CompleteBusinessProfilePopup from './CompleteBusinessProfilePopup'
import SetStatus from '../common/Sportunity/SetStatus';
import StatsFilling from './StatsFilling';

import BookUserMutation from './BookUserMutation'
import CancelUserMutation from './CancelUserMutation'
import CancelSportunityMutation from './CancelSportunityMutation'
import RegisterCardDataMutation from './RegisterCardDataMutation'
import UpdateUserProfileMutation from './UpdateUserProfileMutation';
import CancelParticipantSportunityMutation from './CancelParticipantSportunityMutation';
import RefuseInvitationMutation from './RefuseInvitationMutation';
import NewOpponentSportunityMutation from './NewOpponentSportunityMutation';
import OrganizerAddParticipantMutation from './mutations/OrganizerAddParticipant'
import OrganizerAddInvitedMutation from './mutations/OrganizerAddInvited';
import SecondaryOrganizerPickRoleMutation from './mutations/SecondaryOrganizerPickRole';
import SecondaryOrganizerRefuseRoleMutation from './mutations/SecondaryOrganizerRefuseRole';
import SecondaryOrganizerCancelRoleMutation from './mutations/SecondaryOrganizerCancelRole'

import mangoPay from 'mangopay-cardregistration-js-kit'
import localizations from '../Localizations'
import Loading from '../common/Loading/Loading.js'

import { appUrl,mangoPayUrl, mangoPayClientId } from '../../../constants.json';
import * as types from '../../actions/actionTypes.js';

mangoPay.cardRegistration.baseURL = mangoPayUrl;
mangoPay.cardRegistration.clientId = mangoPayClientId;

let styles;

let MetaImage = ({images}) => <meta property="og:image" content={images[0]}/>;

class EventView extends React.Component {
  constructor() {
    super();
    this.alertOptions = {
      offset: 60,
      position: 'top right',
      theme: 'light',
      transition: 'fade',
    };

    this.state = {
      displayConfirmationPopup: false,
      displayAddACardPopup: false,
      displayCompletePersonProfilePopup: false,
      displayCompleteBusinessProfilePopup: false,
      displayPrivateSportunityPopup: false,
      isQueryingForPrivateSportunity: false,
      selectedCard: '',
      paymentWithWallet: false,
      cardJustAdded: false,
      me: '',
      process: false,
      language: localizations.getLanguage(),
      isAdmin: false,
      isSecondaryOrganizer: false,
      isPotentialSecondaryOrganizer: false,
      isPotentialSecondaryOrganizerSelectedRole: null,
      isAuthorizedAdmin: false,
      mainOrganizer: null, 
      isPotentialOpponent: false,
      isActive: true,
      isPast: false,
      shouldGoToJoinWaitingList: false,
      loading: true,
      isLoading: false,
      displayStatFilling: false,
      displayAndroidOpenApp: false,
      isOpponentSportunityCreated: false
    }
  }

  _setLanguage = (language) => {
    this.setState({ language: language })
  }

  componentDidMount() {
    if (platform.name.indexOf("Firefox") < 0) {
      if (platform.os.family === "iOS") {
        if (typeof window !== 'undefined')
          window.location.href = "sportunity://sportunity/"+this.props.viewer.sportunity.id;
      }
      else if (platform.os.family === "Android") {
        this.setState({displayAndroidOpenApp: true})   
      }
    }

    this.props.relay.forceFetch();
    setTimeout(() => this.setState({ loading: false }), 1000)
  }

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.viewer && nextProps.viewer.sportunity) {
      this.setState({
        me: nextProps.viewer.me,
      });
      nextProps.viewer.sportunity.organizers.forEach(organizer => {
        if (nextProps.viewer.me && organizer.isAdmin && organizer.organizer.id == nextProps.viewer.me.id) {
          this.setState({
            isAdmin: true,
          })
        }
        else if (nextProps.viewer.me && !organizer.isAdmin && organizer.organizer.id === nextProps.viewer.me.id) {
          this.setState({
            isSecondaryOrganizer: true
          })
        }
      })
      if (nextProps.viewer.sportunity.status == "Past" || nextProps.viewer.sportunity.status == "Cancelled")
        this.setState({
          isActive: false,
        })
      if (nextProps.viewer.sportunity.status == "Past")
        this.setState({
          isPast: true
        })

      if (nextProps.viewer.sportunity.status.indexOf("Asked-CoOrganization") >= 0) {
        this.setState({
          isPotentialSecondaryOrganizer: true,
          isSecondaryOrganizer: false
        })
      }

      let status = SetStatus(nextProps.viewer.sportunity, nextProps.viewer.sportunity.status, nextProps.viewer.me ? nextProps.viewer.me.id : null).displayStatus;
      this.setState({
        shouldGoToJoinWaitingList: status === "WAITING LIST" || (nextProps.viewer.sportunity.participants && nextProps.viewer.sportunity.participants.length === nextProps.viewer.sportunity.participantRange.to)
      })
    }

    if (nextProps.viewer && nextProps.viewer.sportunity) {
    const { viewer:{sportunity, me} } = nextProps;

      if (nextProps.viewer && nextProps.viewer.sportunity && !this.state.displayPrivateSportunityPopup) {
        const { viewer:{sportunity, me} } = nextProps;
        if (!me && sportunity.kind === "PRIVATE") {
          this.setState({
            displayPrivateSportunityPopup: true
          })
          confirmModal({
            title: localizations.event_alert_event_is_private_title,
            message: localizations.event_alert_event_is_private_message,
            confirmLabel: localizations.event_alert_event_is_private_ok,
            canCloseModal: false,
            onConfirm: () => {
              this.props.router.push({
                pathname : '/find-sportunity'
              })
            }
          })
        }
        else if (sportunity && me && !this.state.isQueryingForPrivateSportunity) {
          const isOrganized = !!sportunity.organizers.find((item) => item && item.organizer && item.organizer.id === me.id)
          const isSecondaryOrganizer = !!sportunity.organizers.find((item) => item && !item.isAdmin && item.organizer && item.organizer.id === me.id)
          const isPotentialSecondaryOrganizer = nextProps.viewer.sportunity.status.indexOf("Asked-CoOrganization") >= 0;
          let isParticipant = !!sportunity.participants.find((item) => item && item.id === me.id);
          let isInvited = !!sportunity.invited.find(item => item && item.user && item.user.id === me.id)
          let isPotentialOpponent = me.profileType === 'ORGANIZATION' && !isOrganized && 
            sportunity.game_information && sportunity.game_information.opponent && 
              (sportunity.game_information.opponent.lookingForAnOpponent || 
                  (sportunity.game_information.opponent.invitedOpponents && sportunity.game_information.opponent.invitedOpponents.edges && sportunity.game_information.opponent.invitedOpponents.edges.length > 0 && !!sportunity.game_information.opponent.invitedOpponents.edges[0].node.members.find(member => member.id === me.id))
                ) && 
              !sportunity.game_information.opponent.organizer && !sportunity.game_information.opponent.organizerPseudo ;

          this.setState({isPotentialOpponent})

          if (!isOrganized && !isParticipant && !isInvited && !isSecondaryOrganizer && !isPotentialOpponent && !this.state.isOpponentSportunityCreated && !isPotentialSecondaryOrganizer) {
            this.props.relay.setVariables({
              superToken:localStorage.getItem('superToken'),
              userToken:localStorage.getItem('userToken'),
              queryAuthorizedAccounts: true,
              querySuperMe: true,
            }, (state) => {
              if (state.ready) {
                this.waitForDataSuperMe();
              }
            })
            this.setState({
              isQueryingForPrivateSportunity: true
            })

          }
        }
      }
    }
  }

  waitForDataSuperMe = () => {
    if (this.props.viewer && this.props.viewer.superMe && this.props.viewer.superMe.id && this.props.viewer.authorizedAccounts && this.props.viewer.authorizedAccounts.id) {
      let accounts = []

      if (this.props.viewer.superMe.subAccounts && this.props.viewer.superMe.subAccounts.length > 0)
        this.props.viewer.superMe.subAccounts.forEach(subAccount => {
          accounts.push(subAccount)
        })

      if (this.props.viewer.authorizedAccounts.accounts && this.props.viewer.authorizedAccounts.accounts.length > 0)
        this.props.viewer.authorizedAccounts.accounts.forEach(account => {
          if (accounts.findIndex(item => item.id === account.id) < 0)
            accounts.push(account)
        })

      if (accounts.findIndex(item => item.id === this.props.viewer.superMe.id) < 0)
        accounts.push({
          id: this.props.viewer.superMe.id,
          pseudo: this.props.viewer.superMe.pseudo,
          avatar: this.props.viewer.superMe.avatar,
          token:  localStorage.getItem('superToken')
        })

      /*if (accounts.findIndex(item => item.id === this.props.viewer.authorizedAccounts.id) < 0)
        accounts.push({
          id: this.props.viewer.authorizedAccounts.id,
          pseudo: this.props.viewer.authorizedAccounts.pseudo,
          avatar: this.props.viewer.authorizedAccounts.avatar,
          token:  localStorage.getItem('userToken')
        })*/

      if (accounts.length > 1) {
        let mainOrganizer ;
        this.props.viewer.sportunity.organizers.forEach(organizer => {
          if (organizer.isAdmin)
            mainOrganizer = organizer.organizer
        })

        let organizerAccountIndex = accounts.findIndex(account => account.id === mainOrganizer.id) ;

        if (organizerAccountIndex >= 0) {
          this.setState({
            isAuthorizedAdmin: true,
            mainOrganizer: accounts[organizerAccountIndex]
          })
        }
        else if (this.props.viewer.sportunity.kind === "PRIVATE") {
          accounts = accounts
            .filter(account => 
              this.props.viewer.sportunity.participants.findIndex(participant => participant.id === account.id) >= 0
              || this.props.viewer.sportunity.waiting.findIndex(waiting => waiting.id === account.id) >= 0
              || this.props.viewer.sportunity.invited.findIndex(invited => invited.user.id === account.id) >= 0
              || this.props.viewer.sportunity.organizers.findIndex(organizer => organizer.organizer.id === account.id) >= 0
            )

          if (accounts.length > 0) {
            displayChooseAccountModal({
              title: localizations.event_alert_event_is_private_title,
              message: localizations.event_alert_event_is_private_message_switch,
              cancelLabel: localizations.event_alert_event_is_private_message_switch_close,
              accounts,
              canCloseModal: false,
              onClose: () => {
                this.props.router.push({
                  pathname : '/my-events'
                })
              }
            })
          }
          else {
            this.setState({
              displayPrivateSportunityPopup: true
            })
            confirmModal({
              title: localizations.event_alert_event_is_private_title,
              message: localizations.event_alert_event_is_private_message,
              confirmLabel: localizations.event_alert_event_is_private_ok,
              canCloseModal: false,
              onConfirm: () => {
                this.props.router.push({
                  pathname : '/my-events'
                })
              }
            })
          }
        }
        /*displayChooseAccountModal({
          title: localizations.event_alert_event_is_private_title,
          message: localizations.event_alert_event_is_private_message_switch,
          cancelLabel: localizations.event_alert_event_is_private_message_switch_close,
          accounts,
          canCloseModal: false,
          onClose: () => {
            this.props.router.push({
              pathname : '/find-sportunity'
            })
          }
        })*/
      }
      else if (this.props.viewer.sportunity.kind === "PRIVATE") {
        this.setState({
          displayPrivateSportunityPopup: true
        })
        confirmModal({
          title: localizations.event_alert_event_is_private_title,
          message: localizations.event_alert_event_is_private_message,
          confirmLabel: localizations.event_alert_event_is_private_ok,
          canCloseModal: false,
          onConfirm: () => {
            this.props.router.push({
              pathname : '/my-events'
            })
          }
        })
      }
    }
    else {
      setTimeout(() => this.waitForDataSuperMe(), 100)
    }
  }

  _handleShowConfirmationPopup = () => {
    if (this.props.viewer.me !== null) { // LOGGED IN
      this.setState({
        selectedCard: this.props.viewer.me.paymentMethods.length > 0 ? this.props.viewer.me.paymentMethods[this.props.viewer.me.paymentMethods.length-1] : '',
        displayConfirmationPopup: true,
      });
    }
    else {
      this.msg.show(localizations.event_login_needed, {
        time: 3000,
        type: 'info',
      });
	    setTimeout(() => {
		    this.msg.removeAll();
	    }, 3000);
      setTimeout(() => {
        this.props.router.push({
            pathname : '/register',
        })
      }, 2000)
    }
  }

  _handleHideConfirmationPopup = () => {
    this.setState({
      displayConfirmationPopup: false,
    })
  }

  _handleAddANewCard = () => {
    this.setState({
      displayConfirmationPopup: false,
      displayAddACardPopup: true,
    })
  }

  _handleShowCompleteProfile = () => {
    if (this.props.viewer.me.profileType === 'PERSON') {
      this.setState({
        displayConfirmationPopup: false,
        displayCompletePersonProfilePopup: true
      })
    }
    else {
      this.setState({
        displayConfirmationPopup: false,
        displayCompleteBusinessProfilePopup: true
      })
    }
  }

  _handleConfirmProfileUpdate = (data) => {
    this.props.relay.commitUpdate(
      new UpdateUserProfileMutation({
        viewer: this.props.viewer,
        userIDVar: this.props.viewer.me.id,
        lastNameVar: data.lastName,
        firstNameVar: data.firstName,
        addressVar: data.address,
        nationalityVar: data.nationality,
        birthdayVar: data.birthday
      }),{
        onSuccess: (response) => {
          this.msg.show(localizations.popup_completeProfile_update_success, {
              time: 2000,
              type: 'success',
            });
          this.props.relay.forceFetch();
          /*if (this.props.viewer.sportunity.price.cents > 0 && this.props.viewer.me.paymentMethods.length === 0)
            this.setState({
              displayAddACardPopup: true,
              displayCompletePersonProfilePopup: false,
            })
          else*/
          this.setState({
            displayConfirmationPopup: true,
            displayCompletePersonProfilePopup: false,
          })

          setTimeout(function() {
            this.msg.removeAll();
          }, 2000);
        },
        onFailure: (error) => {
          this.setState({
            processSaveProfile: false
          });
          this.msg.show(localizations.popup_completeProfile_update_failed, {
            time: 0,
            type: 'error',
          });
          setTimeout(function() {
            this.msg.removeAll();
          }, 2000);
        },
      }
    );
  }

  _handleConfirmBusinessProfileUpdate = (data) => {
    this.props.relay.commitUpdate(
      new UpdateUserProfileMutation({
        viewer: this.props.viewer,
        userIDVar: this.props.viewer.me.id,
        lastNameVar: data.lastName,
        firstNameVar: data.firstName,
        addressVar: data.address,
        nationalityVar: data.nationality,
        businessNameVar: data.businessName,
        businessEmailVar: data.businessEmail,
        VATNumberVar: data.VATNumber,
        headquarterAddressVar: data.headquarterAddress,
        shouldDeclareVATVar: data.shouldDeclareVAT,
        birthdayVar: data.birthday
      }),{
        onSuccess: (response) => {
          this.msg.show(localizations.popup_completeProfile_update_success, {
              time: 2000,
              type: 'success',
            });
          this.props.relay.forceFetch();
          if (this.props.viewer.sportunity.price.cents > 0 && this.props.viewer.me.paymentMethods.length === 0)
            this.setState({
              displayAddACardPopup: true,
              displayCompleteBusinessProfilePopup: false,
            })
          else
            this.setState({
              displayConfirmationPopup: true,
              displayCompleteBusinessProfilePopup: false,
            })

            setTimeout(() => {
              this.msg.removeAll();
            }, 2000);
        },
        onFailure: (error) => {
          this.setState({
            processSaveProfile: false
          });
          this.msg.show(localizations.popup_completeProfile_update_failed, {
            time: 0,
            type: 'error',
          });
          setTimeout(function() {
            this.msg.removeAll();
          }, 2000);
        },
      }
    );
  }

  _handleConfirmOpponentCreation = () => {
    confirmModal({
      title: localizations.event_newOpponentSportunityValidation,
      message: localizations.event_newOpponentSportunity,
      confirmLabel: localizations.event_alert_confirm_cancel_yes,
      cancelLabel: localizations.event_alert_confirm_cancel_no,
      canCloseModal: true,
      onConfirm: () => {
        this.createOpponentSportunity();
      },
      onCancel: () => {
      }
    })
  }

  createOpponentSportunity() {
    this.setState({
      process: true,
      isLoading: true
    })

    const { viewer, viewer:{me, sportunity }} = this.props;
    let params = { viewer, sportunity, user: me };
    
    this.props.relay.commitUpdate(
      new NewOpponentSportunityMutation(params),
      {
        onFailure: error => {
          this.setState({
            process: false,
            isLoading: false
          })

          this.msg.show(localizations.event_newOpponentSportunityFailed, {
            time: 2000,
            type: 'error',
          })
	        setTimeout(() => {
		        this.msg.removeAll();
	        }, 2000);
          let errors = JSON.parse(error.getError().source);
          console.log(errors.errors[0].message);
        },
        onSuccess: () => {
          this.setState({
            process: false,
            isLoading: false,
            isOpponentSportunityCreated: true
          })

          this.msg.show(localizations.event_newOpponentSportunitySuccess, {
            time: 2000,
            type: 'success',
          })
	        setTimeout(() => {
		        this.msg.removeAll();
	        }, 2000);
          this.props.router.push({
            pathname : '/my-events/'
          })
        },
      }
    );
  }

  _handleSecondaryOrganizerSelectRole = () => {
    const _handleChangeSelectedRole = selectedRole => this.setState({isPotentialSecondaryOrganizerSelectedRole: selectedRole})

    let askedRoles = []; 
    let user = this.props.viewer.me;
    let language = localizations.getLanguage();

    this.props.viewer.sportunity.pendingOrganizers.forEach(pendingOrganizer => {
        if (pendingOrganizer.circles.edges.findIndex(edge => edge.node.members.findIndex(member => member.id === user.id) >= 0) >= 0) {
            askedRoles.push({
                id: pendingOrganizer.id,
                name: pendingOrganizer.secondaryOrganizerType 
                  ? pendingOrganizer.price 
                    ? pendingOrganizer.secondaryOrganizerType.name[language.toUpperCase()] + ' (' + localizations.event_secondaryOrganizer_Price + ': ' + pendingOrganizer.price.currency + ' ' + pendingOrganizer.price.cents / 100 + ')'
                    : pendingOrganizer.secondaryOrganizerType.name[language.toUpperCase()]
                  : pendingOrganizer.price 
                    ? pendingOrganizer.customSecondaryOrganizerType + ' (' + localizations.event_secondaryOrganizer_Price + ': ' + pendingOrganizer.price.currency + ' ' + pendingOrganizer.price.cents / 100 + ')'
                    : pendingOrganizer.customSecondaryOrganizerType
            })
        }
    })

    this.setState({isPotentialSecondaryOrganizerSelectedRole: askedRoles[0]})
    
    let message = <div>
      {localizations.event_pendingSecondaryOrganizerValidationMessage}
      <select 
        style={styles.select} 
        onChange={_handleChangeSelectedRole}
        value={this.state.isPotentialSecondaryOrganizerSelectedRole ? this.state.isPotentialSecondaryOrganizerSelectedRole.id : askedRoles[0].id}
        >
        {askedRoles.map(role => (
          <option key={role.id} value={role.id}>{role.name}</option>
        ))}
      </select> 
    </div>;

    confirmModal({
      title: localizations.event_pendingSecondaryOrganizerValidationTitle,
      message,
      confirmLabel: localizations.event_alert_confirm_cancel_yes,
      cancelLabel: localizations.event_alert_confirm_cancel_no,
      canCloseModal: true,
      onConfirm: () => {
        this.secondaryOrganizerSelectRole();
      },
      onCancel: () => {
      }
    })
  }

  _handleSecondaryOrganizerRefuseRole = () => {
    const _handleChangeSelectedRole = selectedRole => this.setState({isPotentialSecondaryOrganizerSelectedRole: selectedRole})
    
    let askedRoles = []; 
    let user = this.props.viewer.me;
    let language = localizations.getLanguage();

    this.props.viewer.sportunity.pendingOrganizers.forEach(pendingOrganizer => {
        if (pendingOrganizer.circles.edges.findIndex(edge => edge.node.members.findIndex(member => member.id === user.id) >= 0) >= 0) {
            askedRoles.push({
                id: pendingOrganizer.id,
                name: pendingOrganizer.secondaryOrganizerType 
                  ? pendingOrganizer.price 
                    ? pendingOrganizer.secondaryOrganizerType.name[language.toUpperCase()] + ' (' + localizations.event_secondaryOrganizer_Price + ': ' + pendingOrganizer.price.currency + ' ' + pendingOrganizer.price.cents / 100 + ')'
                    : pendingOrganizer.secondaryOrganizerType.name[language.toUpperCase()]
                  : pendingOrganizer.price 
                    ? pendingOrganizer.customSecondaryOrganizerType + ' (' + localizations.event_secondaryOrganizer_Price + ': ' + pendingOrganizer.price.currency + ' ' + pendingOrganizer.price.cents / 100 + ')'
                    : pendingOrganizer.customSecondaryOrganizerType
            })
        }
    })

    this.setState({isPotentialSecondaryOrganizerSelectedRole: askedRoles[0]})
    
    let message = <div>
      {localizations.event_pendingSecondaryOrganizerRefuseMessage}
      <select 
        style={styles.select} 
        onChange={_handleChangeSelectedRole}
        value={this.state.isPotentialSecondaryOrganizerSelectedRole ? this.state.isPotentialSecondaryOrganizerSelectedRole.id : askedRoles[0].id}
        >
        {askedRoles.map(role => (
          <option key={role.id} value={role.id}>{role.name}</option>
        ))}
      </select> 
    </div>;

    confirmModal({
      title: localizations.event_pendingSecondaryOrganizerRefuseTitle,
      message,
      confirmLabel: localizations.event_alert_confirm_cancel_yes,
      cancelLabel: localizations.event_alert_confirm_cancel_no,
      canCloseModal: true,
      onConfirm: () => {
        this.secondaryOrganizerRefuseRole();
      },
      onCancel: () => {
      }
    })
  }

  secondaryOrganizerSelectRole = () => {
    if (!this.state.isPotentialSecondaryOrganizerSelectedRole) return 
    
    this.setState({
      process: true,
      isLoading: true
    })

    let params = {
      viewer: this.props.viewer, 
      sportunity: this.props.viewer.sportunity,
      pendingOrganizerIDVar: this.state.isPotentialSecondaryOrganizerSelectedRole.id,
    };
    
    this.props.relay.commitUpdate(
      new SecondaryOrganizerPickRoleMutation(params),
      {
        onFailure: error => {
          this.setState({
            process: false,
            isLoading: false
          })

          this.msg.show(localizations.event_fill_statistics_failed, {
            time: 2000,
            type: 'error',
          })
          let errors = JSON.parse(error.getError().source);
          console.log(errors.errors[0].message);
          setTimeout(() => {
            this.msg.removeAll();
          }, 2000);
        },
        onSuccess: () => {
          this.setState({
            process: false,
            isLoading: false,
          })

          this.msg.show(localizations.event_fill_statistics_success, {
            time: 2000,
            type: 'success',
          })
          setTimeout(() => {
            this.msg.removeAll();
          }, 2000);
        },
      }
    );
  }

  secondaryOrganizerRefuseRole = () => {
    if (!this.state.isPotentialSecondaryOrganizerSelectedRole) return 
    
    this.setState({
      process: true,
      isLoading: true
    })

    let params = {
      viewer: this.props.viewer, 
      sportunity: this.props.viewer.sportunity,
      pendingOrganizerIDVar: this.state.isPotentialSecondaryOrganizerSelectedRole.id,
    };
    
    this.props.relay.commitUpdate(
      new SecondaryOrganizerRefuseRoleMutation(params),
      {
        onFailure: error => {
          this.setState({
            process: false,
            isLoading: false
          })

          this.msg.show(localizations.event_fill_statistics_failed, {
            time: 2000,
            type: 'error',
          })
          let errors = JSON.parse(error.getError().source);
          console.log(errors.errors[0].message);
          setTimeout(() => {
            this.msg.removeAll();
          }, 2000);
        },
        onSuccess: () => {
          this.setState({
            process: false,
            isLoading: false,
          })

          this.msg.show(localizations.event_fill_statistics_success, {
            time: 2000,
            type: 'success',
          })
          setTimeout(() => {
            this.msg.removeAll();
          }, 2000);
        },
      }
    );
  }

  _handleSecondaryOrganizerCancel = () => {

    let language = localizations.getLanguage(); 
    let currentUserOrganizer = this.props.viewer.sportunity.organizers.find(organizer => organizer.organizer.id === this.props.viewer.me.id)

    let role = currentUserOrganizer.secondaryOrganizerType 
      ? currentUserOrganizer.price 
        ? currentUserOrganizer.secondaryOrganizerType.name[language.toUpperCase()].toLowerCase() + ' (' + localizations.event_secondaryOrganizer_Price.toLowerCase() + ': ' + currentUserOrganizer.price.currency + ' ' + currentUserOrganizer.price.cents / 100 + ')'
        : currentUserOrganizer.secondaryOrganizerType.name[language.toUpperCase()].toLowerCase() 
      : currentUserOrganizer.price 
        ? currentUserOrganizer.customSecondaryOrganizerType.toLowerCase() + ' (' + localizations.event_secondaryOrganizer_Price.toLowerCase() + ': ' + currentUserOrganizer.price.currency + ' ' + currentUserOrganizer.price.cents / 100 + ')'
        : currentUserOrganizer.customSecondaryOrganizerType.toLowerCase()


    this.props.relay.setVariables({
      queryIsCoOrganizerOnSerie: true
    }, readyState => {
      if (readyState.done) {
        setTimeout(() => {
          if (this.props.viewer.IsCoOrganizerOnSerie) {
            confirmModal({
              title: localizations.event_pendingSecondaryOrganizerCancelationTitle,
              message: localizations.event_pendingSecondaryOrganizerCancelationMessageSerie.replace('-role-', role),
              confirmLabel: localizations.event_alert_update_serie_or_occurence_no,
              cancelLabel: localizations.event_alert_update_serie_or_occurence_yes,
              canCloseModal: true,
              onConfirm: () => {
                this.secondaryOrganizerCancel(false);
              },
              onCancel: () => {
                this.secondaryOrganizerCancel(true);
              }
            })
          }
          else {
            confirmModal({
              title: localizations.event_pendingSecondaryOrganizerCancelationTitle,
              message: localizations.event_pendingSecondaryOrganizerCancelationMessage.replace('-role-', role),
              confirmLabel: localizations.event_alert_confirm_cancel_yes,
              cancelLabel: localizations.event_alert_confirm_cancel_no,
              canCloseModal: true,
              onConfirm: () => {
                this.secondaryOrganizerCancel(false);
              },
              onCancel: () => {
              }
            })
          }
        }
        , 50);
      }
    })
  }

  secondaryOrganizerCancel = (cancelSerie) => {
    this.setState({
      process: true,
      isLoading: true
    })

    let params = {
      viewer: this.props.viewer, 
      sportunity: this.props.viewer.sportunity,
      cancelSerieVar: cancelSerie
    };
    
    this.props.relay.commitUpdate(
      new SecondaryOrganizerCancelRoleMutation(params),
      {
        onFailure: error => {
          this.setState({
            process: false,
            isLoading: false
          })

          this.msg.show(localizations.event_fill_statistics_failed, {
            time: 2000,
            type: 'error',
          })
          let errors = JSON.parse(error.getError().source);
          console.log(errors.errors[0].message);
          setTimeout(() => {
            this.msg.removeAll();
          }, 2000);
        },
        onSuccess: () => {
          this.setState({
            process: false,
            isLoading: false,
            isSecondaryOrganizer: false
          })

          this.msg.show(localizations.event_fill_statistics_success, {
            time: 2000,
            type: 'success',
          })
          setTimeout(() => {
            this.msg.removeAll();
          }, 2000);
        },
      }
    );
  }

  _handleOrganizerAddParticipants = (users, circleToPutUsersIn) => {
    this.setState({
      process: true,
      isLoading: true
    })

    let params = {
      viewer: this.props.viewer, 
      sportunity: this.props.viewer.sportunity,
      participantsVar: users,
      putParticipantsInCircleVar: circleToPutUsersIn
    };
    
    this.props.relay.commitUpdate(
      new OrganizerAddParticipantMutation(params),
      {
        onFailure: error => {
          this.setState({
            process: false,
            isLoading: false
          })

          this.msg.show(localizations.event_addParticipantOrInvited_failed, {
            time: 2000,
            type: 'error',
          })
          let errors = JSON.parse(error.getError().source);
          console.log(errors.errors[0].message);
          setTimeout(() => {
            this.msg.removeAll();
          }, 2000);
        },
        onSuccess: () => {
          this.setState({
            process: false,
            isLoading: false,
          })

          this.msg.show(localizations.event_addParticipantOrInvited_success, {
            time: 2000,
            type: 'success',
          })
          setTimeout(() => {
            this.msg.removeAll();
          }, 2000);
        },
      }
    );
  }

  _handleOrganizerAddInviteds = (users, circleToPutUsersIn) => {
    this.setState({
      process: true,
      isLoading: true
    })

    let params = {
      viewer: this.props.viewer, 
      sportunity: this.props.viewer.sportunity,
      invitedsVar: users,
      putInvitedsInCircleVar: circleToPutUsersIn
    };
    
    this.props.relay.commitUpdate(
      new OrganizerAddInvitedMutation(params),
      {
        onFailure: error => {
          this.setState({
            process: false,
            isLoading: false
          })

          this.msg.show(localizations.event_addParticipantOrInvited_failed, {
            time: 2000,
            type: 'error',
          })
          let errors = JSON.parse(error.getError().source);
          console.log(errors.errors[0].message);
          setTimeout(() =>  this.msg.removeAll(), 2000);
        },
        onSuccess: () => {
          this.setState({
            process: false,
            isLoading: false,
          })

          this.msg.show(localizations.event_addParticipantOrInvited_success, {
            time: 2000,
            type: 'success',
          })
          setTimeout(() =>  this.msg.removeAll(), 2000);
        },
      }
    );
  }

  _handleChangeSelectedCard = (e) => {
    if (e === "wallet" || e.target.value === "wallet") {
      this.setState({
        selectedCard: '',
        paymentWithWallet: true
      })
      return true;
    }
    if (e === "addACard" || e.target.value === "addACard") {
      this.setState({
        selectedCard: '',
        paymentWithWallet: false,
      })
    }
    this.props.viewer.me.paymentMethods.find(paymentMethod => {
      if (paymentMethod.cardMask === e.target.value) {
        this.setState({
          selectedCard: paymentMethod,
          paymentWithWallet: false
        })
        return true;
      }
      return false;
    })
  }

  isValidCard(card) {
    return (
      card.cardNumber && card.cardNumber.length === 16
      && card.cardExpirationDate && card.cardExpirationDate.length === 4
      && card.cardCvx && card.cardCvx.length === 3
      && card.cardType)
  }

  _handleConfirmAddACard = (cardRegistration, card) => {
    if (!cardRegistration) {
      this.msg.show(localizations.popup_addACard_card_removed_error, {
        time: 2000,
        type: 'error',
      });
      setTimeout(function() {
        this.msg.removeAll();
      }, 2000);

      return;
    }

    if(!this.isValidCard(card)) {
      this.msg.show(localizations.popup_addACard_invalid_card, {
        time: 2000,
        type: 'error',
      });
      setTimeout(() => {
        this.msg.removeAll();
      }, 3000);
      return;
    }

    this.setState({process:true});

    let that = this ;
    if(card)
      mangoPay.cardRegistration.init({
        Id: cardRegistration.cardRegistrationId,
        cardRegistrationURL: cardRegistration.cardRegistrationURL,
        accessKey: cardRegistration.accessKey,
        preregistrationData: cardRegistration.preregistrationData,
      });
      mangoPay.cardRegistration.registerCard(
          card,
          function(res) {
              that.updateCard(cardRegistration, res.RegistrationData);
          },
          function(res) {
              // Handle error, see res.ResultCode and res.ResultMessage
              that.msg.show(res.ResultMessage, {
                time: 0,
                type: 'error',
              });
              setTimeout(() => {
                that.msg.removeAll();
              }, 3000);
              that.setState({
                process: false,
              })
          }
      );
  }

  updateCard(cardRegistration, registrationData) {
    this.props.relay.commitUpdate(
      new RegisterCardDataMutation({
        viewer: this.props.viewer,
        cardRegistration: cardRegistration,
        registrationData: registrationData,
      }),{
        onSuccess: (res) => {
          this.msg.show(localizations.popup_addACard_success, {
              time: 2000,
              type: 'success',
            });
          this.setState({
            selectedCard: res.registerCardData.viewer.me.paymentMethods.length > 0 ? res.registerCardData.viewer.me.paymentMethods[res.registerCardData.viewer.me.paymentMethods.length-1] : '',
            me: res.registerCardData.viewer.me,
            displayConfirmationPopup: true,
            displayAddACardPopup: false,
            cardJustAdded: true,
            process: false,
          })
          this.props.relay.forceFetch();
          setTimeout(() => {
            this.msg.removeAll();
          }, 3000);

        },
        onFailure: (error) => {
          this.msg.show(localizations.popup_addACard_error, {
            time: 5000,
            type: 'error',
          });
          this.setState({
            process:false,
          })
          setTimeout(() => {
            this.msg.removeAll();
          }, 3000);
        },
      }
    );
  }

  _handleHideACard = () => {
    this.setState({
      displayConfirmationPopup: true,
      displayAddACardPopup: false,
    })
  }

  _handleHideCompleteProfilePopup = () => {
    this.setState({
      displayConfirmationPopup: true,
      displayCompletePersonProfilePopup: false,
    })
  }

  _handleHideCompleteBusinessProfilePopup = () => {
    this.setState({
      displayConfirmationPopup: true,
      displayCompleteBusinessProfilePopup: false,
    })
  }

  _handleUserBook = () => {
    this.setState({
      process: true,
      isLoading: true
    });
    let userIsInvited = this.props.viewer.sportunity && this.props.viewer.sportunity.status &&
        (this.props.viewer.sportunity.status.toLowerCase() === 'invited-grey' || this.props.viewer.sportunity.status.toLowerCase() === 'invited-black');

    this.props.relay.commitUpdate(
      new BookUserMutation({
        viewer: this.props.viewer,
        sportunity: this.props.viewer.sportunity,
        user: this.props.viewer.me,
        paymentMethod: this.props.viewer.sportunity.price.cents > 0 && !this.state.paymentWithWallet ? this.state.selectedCard.id : null,
        paymentByWallet: this.state.paymentWithWallet,
        invited: userIsInvited ?
          {
            user: this.props.viewer.me.id,
            answer: 'YES'
          }
          : null
      }),{
        onSuccess: () => {
          this.msg.show(localizations.popup_sportunityBooking_success, {
              time: 2000,
              type: 'success',
            });
          this.setState({
            displayConfirmationPopup: false,
            process: false,
          });
          setTimeout(() => {
            this.setState({isLoading: false})
          }, 1000)
          this.props.relay.forceFetch();
          setTimeout(() => {
            this.msg.removeAll();
          }, 3000);

        },
        onFailure: (error) => {
          this.msg.show(error.getError().source.errors[0].message, {
            time: 0,
            type: 'error',
          });
          this.setState({
            process: false,
            isLoading: false
          })
          setTimeout(() => {
            this.msg.removeAll();
          }, 3000);
        },
      }
    );
  }

  _handleDeclineInvitation = () => {
    const { viewer, viewer:{sportunity} } = this.props;

    this.props.relay.commitUpdate(
      new RefuseInvitationMutation({
        viewer,
        sportunity,
        userId: this.props.viewer.me.id
      }),
      {
        onFailure: error => {
          this.msg.show(error.getError().source.errors[0].message, {
            time: 0,
            type: 'error',
          });
          console.log(error);
          setTimeout(() => {
            this.msg.removeAll();
          }, 3000);
        },
        onSuccess: () => {
          this.msg.show(localizations.event_refuse_invitation, {
            time: 2000,
            type: 'success',
          })
          this.props.relay.forceFetch();
          setTimeout(() => {
            this.msg.removeAll();
          }, 3000);
        },
      }
    );
  }

	_handleUserCancel = () => {

    this.setState({
      process: true,
      isLoading: true
    })

    let userwasInvited = this.props.viewer.sportunity && this.props.viewer.sportunity.invited && this.props.viewer.sportunity.invited.length > 0 &&
        !!this.props.viewer.sportunity.invited.find(item => item && item.user && item.user.id === this.props.viewer.me.id)

    this.props.relay.commitUpdate(
      new CancelUserMutation({
        viewer: this.props.viewer,
        sportunity: this.props.viewer.sportunity,
        user: this.props.viewer.me,
        invited: userwasInvited ?
          {
            user: this.props.viewer.me.id,
            answer: 'NO'
          }
          : null
      }), {
         onSuccess: () => {
          this.msg.show(localizations.popup_sportunityCancelBooking_success, {
              time: 2000,
              type: 'success',
            });
          this.props.relay.forceFetch();
          this.setState({
            displayConfirmationPopup: false,
            process: false,
          });
          setTimeout(() => {
            this.setState({isLoading: false})
          }, 1000)

          setTimeout(() => {
            this.msg.removeAll();
          }, 3000);

        },
        onFailure: (error) => {
          this.msg.show(error.getError().source.errors[0].message, {
            time: 0,
            type: 'error',
          });
          this.setState({
            process: false,
            isLoading: false
          })
          setTimeout(() => {
            this.msg.removeAll();
          }, 3000);
        },
      }
    );
  }

  _handleAdminModify = () => {
    let path = '/event-edit/'+this.props.viewer.sportunity.id ;
    if (this.props.viewer.sportunity.number_of_occurences > 1) {
      confirmModal({
        title: localizations.event_alert_update_serie_or_occurence_title,
        message: localizations.formatString(localizations.event_alert_update_serie_or_occurence_message, this.props.viewer.sportunity.number_of_occurences),
        confirmLabel: localizations.event_alert_update_serie_or_occurence_yes,
        cancelLabel: localizations.event_alert_update_serie_or_occurence_no,
        canCloseModal: true,
        onConfirm: () => {
          this._handleAdminModifySerie()
        },
        onCancel: () => {
          this.props.router.push({
            pathname : '/event-edit/'+this.props.viewer.sportunity.id
          })
        }
      })
    }
    else {
      this.props.router.push({
        pathname : '/event-edit/'+this.props.viewer.sportunity.id
      })
    }
  }

  _handleAdminModifySerie = () => {
    let path = '/event-edit/'+this.props.viewer.sportunity.id ;
    if (this.props.viewer.sportunity.number_of_occurences > 1) {
      confirmModal({
        title: localizations.event_alert_update_serie_title,
        message: localizations.formatString(localizations.event_alert_update_serie_message, this.props.viewer.sportunity.number_of_occurences),
        confirmLabel: localizations.event_alert_update_serie_yes,
        cancelLabel: localizations.event_alert_update_serie_no,
        canCloseModal: true,
        onConfirm: () => {
          this.props.router.push({
            pathname : '/serie-edit/'+this.props.viewer.sportunity.id
          })
        },
        onCancel: () => {
        }
      })
    }
    else {
      this.props.router.push({
        pathname : '/event-edit/'+this.props.viewer.sportunity.id
      })
    }
  }

  _handleAdminReOrganize = () => {
    let path = '/event-reorganize/'+this.props.viewer.sportunity.id ;
    this.props.router.push({
        pathname : path,
    })
  }

  _handleSeeAsAdmin = () => {
    this.props._loggedFromPage('/event-view/'+this.props.viewer.sportunity.id)
    localStorage.setItem('userId', this.state.mainOrganizer.id)
    setTimeout(() => {
      this.props.router.push({pathname: '/login-superuser/'+this.state.mainOrganizer.token});
    }, 100)
  }

  cancelSportunity = (cancelTheSerie) => {
    this.props.relay.commitUpdate(
      new CancelSportunityMutation({
        viewer: this.props.viewer,
        sportunity: this.props.viewer.sportunity,
        user: this.props.viewer.me,
        modifyRepeatedSportunities: cancelTheSerie
      }), {
         onSuccess: () => {
          this.msg.show(localizations.popup_sportunityCancelEvent_success, {
              time: 2000,
              type: 'success',
            });
          this.props.relay.forceFetch();
          this.setState({
            process: false,
            isActive: false,
          });
          setTimeout(() => {
            this.setState({isLoading: false})
          }, 1000)

          setTimeout(() => {
            this.msg.removeAll();
          }, 3000);

        },
        onFailure: (error) => {
          this.msg.show(error.getError().source.errors[0].message, {
            time: 0,
            type: 'error',
          });
          this.setState({
            process: false,
            isLoading:false
          })
          setTimeout(() => {
            this.msg.removeAll();
          }, 3000);
        },
      }
    );
  }

  _handleShowConfirmationParticipantPopup = (participant) => {
    confirmModal({
      title: localizations.event_cancelation_popup_participant_title,
      message: localizations.formatString(localizations.event_cancelation_popup_participant_message, participant.pseudo),
      confirmLabel: localizations.event_cancelation_popup_participant_yes,
      cancelLabel: localizations.event_cancelation_popup_participant_no,
      canCloseModal: true,
      onConfirm: () => {
        this._cancelParticipation(participant)
      },
      onCancel: () => {}
    })
  }

  _cancelParticipation = (participant) => {
    this.props.relay.commitUpdate(
      new CancelParticipantSportunityMutation({
        sportunityID: this.props.viewer.sportunity.id,
        viewer: this.props.viewer,
        sportunity: this.props.viewer.sportunity,
        user: participant.id,
      }),{
        onFailure: error => {
          this.msg.show(error.getError(), {
            time: 2000,
            type: 'error',
          });
          setTimeout(() => {
            this.msg.removeAll();
          }, 2000)
        },
        onSuccess: () => {
          this.msg.show(localizations.event_cancelation_popup_participation_success, {
            time: 2000,
            type: 'success',
          });
          setTimeout(() => {
            this.msg.removeAll();
          }, 2000)
          this.props.relay.forceFetch();
        },
      }
    );
  }


  confirmAdminCancel = (cancelTheSerie) => {
    confirmModal({
      title: localizations.event_alert_confirm_cancel_title,
      message: cancelTheSerie
        ? localizations.formatString(localizations.event_alert_confirm_cancel_serie_message, this.props.viewer.sportunity.number_of_occurences)
        : localizations.event_alert_confirm_cancel_sportunity_message,
      confirmLabel: localizations.event_alert_confirm_cancel_yes,
      cancelLabel: localizations.event_alert_confirm_cancel_no,
      canCloseModal: true,
      onConfirm: () => {
        this.cancelSportunity(cancelTheSerie)
      },
      onCancel: () => {}
    })
  }

  _handleAdminCancel = () => {
    this.setState({
      process: true,
      isLoading: true
    })
    if (this.props.viewer.sportunity.number_of_occurences > 1) {
      confirmModal({
        title: localizations.event_alert_cancel_serie_or_occurence_title,
        message: localizations.formatString(localizations.event_alert_cancel_serie_or_occurence_message, this.props.viewer.sportunity.number_of_occurences),
        confirmLabel: localizations.event_alert_cancel_serie_or_occurence_yes,
        cancelLabel: localizations.event_alert_cancel_serie_or_occurence_no,
        canCloseModal: true,
        onConfirm: () => {
          this.confirmAdminCancel(true)
        },
        onCancel: () => {
          this.confirmAdminCancel(false)
        }
      })
    }
    else
      this.confirmAdminCancel(false)

    this.setState({
      process: false,
      isLoading: false
    })
  }

  _handleToggleStatisticForm = () => {
    this.setState({
      displayStatFilling: !this.state.displayStatFilling
    })
  }

  openAndroidApp = () => {
    document.location="sportunity://sportunity/"+this.props.viewer.sportunity.id;
  }

  renderMetaImage = (images) => {
    return (
      images.map(image => <meta property="og:image" content={image}/>)
    )
  };

  renderMetaTags = (sportunity) => {
    //moment.tz.setDefault("Europe/Zurich");
    return <Helmet>
              <title>{"Sportunity - "+sportunity.title}</title>
              <meta name="robots" content="noindex"/>
              <meta name="description" content={localizations.meta_description}/>
              <meta property="fb:app_id" content="1759806787601548"/>
              <meta property="og:type" content="article"/>
              <meta property="og:title" content={"Sportunity - "+sportunity.title} />
              <meta property="og:description" content={
                sportunity.title + '\n' +
                'Date: '+moment.tz(sportunity.beginning_date, "Europe/Zurich").format('DD/MM/YYYY, H:mm') + '\n' +
                'Lieu: '+sportunity.address.city
              }/>
              <meta property="og:url" content={appUrl + this.props.location.pathname}/>
              { (sportunity.images && sportunity.images.length > 0) ?
                this.renderMetaImage(sportunity.images)
                :
                <meta property="og:image" content={sportunity.sport.sport.logo}/>
              }
              <meta property="og:image:width" content="250"/>
              <meta property="og:image:height" content="250"/>
          </Helmet>
  }

  render() {
    const { viewer } = this.props;
    const {isPotentialOpponent, isPotentialSecondaryOrganizer} = this.state; 
    let userIsParticipant = (this.props.viewer && this.props.viewer.me &&
      this.props.viewer.sportunity.participants.findIndex(
        p => p.id === this.props.viewer.me.id
      ) >= 0 ) ;

    let userIsOnWaitingList = (this.props.viewer && this.props.viewer.me &&
      this.props.viewer.sportunity.waiting.findIndex(
        p => p.id === this.props.viewer.me.id
      ) >= 0) ;

    let userIsInvited = viewer && viewer.sportunity && viewer.sportunity.status &&
        (viewer.sportunity.status.toLowerCase() === 'invited-grey' || viewer.sportunity.status.toLowerCase() === 'invited-black');
    
    let userWasInvited = viewer.sportunity && viewer.sportunity.invited && viewer.sportunity.invited.length > 0 && viewer.me && 
      !!viewer.sportunity.invited.find(item => item && item.user && item.user.id === viewer.me.id)
      
    let shouldShowBook = !userIsParticipant && !userIsOnWaitingList && !this.state.isSecondaryOrganizer && !isPotentialSecondaryOrganizer;
    let shouldShowChat = userIsParticipant || userIsOnWaitingList || this.state.isAdmin || this.state.isSecondaryOrganizer ;
    
    const shouldEnableBook = viewer && viewer.me ? true : false ;
    const isCancelled = viewer && viewer.sportunity.status.toLowerCase() === 'cancelled'
                        || viewer && viewer.sportunity.status.toLowerCase() === 'past'

    let status = viewer && SetStatus(viewer.sportunity, viewer.sportunity.status, viewer.me ? viewer.me.id : null)
    if (!viewer)
      return null;

    if (this.state.displayPrivateSportunityPopup)
      return (
        <div>
          {this.renderMetaTags(viewer.sportunity)}
          {this.state.loading && <Loading/>}
          {
            viewer.me ?
              <AppHeader
                user={viewer.me}
                viewer={viewer}
                {...this.state}
              />
            :
              <AppHeader viewer={viewer} user={null} {...this.state}/>
          }
        </div>
      )

    return (
      <div>
        {this.renderMetaTags(viewer.sportunity)}
        {this.state.loading && <Loading/>}
        {
          viewer.me ?
						<AppHeader
							user={viewer.me}
              viewer={viewer}
              {...this.state}
						/>
          :
            <AppHeader viewer={viewer} user={null} {...this.state}/>
        }
        <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
        {this.state.displayAndroidOpenApp && <ConfirmationModal
          isOpen={true}
          title={localizations.android_open_appTitle}
          message={localizations.android_open_appText}
          confirmLabel={localizations.android_open_appConfirm}
          cancelLabel={localizations.android_open_appCancel}
          canCloseModal={true}
          onConfirm= {this.openAndroidApp}
          onCancel={() => {}}
        />}
        {
          this.state.displayConfirmationPopup ?
            <ConfirmBookingPopup
              viewer={viewer}
              sportunity={viewer.sportunity}
              shouldGoToJoinWaitingList={this.state.shouldGoToJoinWaitingList}
              onConfirm={this._handleUserBook}
              onClose={this._handleHideConfirmationPopup}
              onAddACard={this._handleAddANewCard}
              onOpenProfilePopup={this._handleShowCompleteProfile}
              cardJustAdded={this.state.cardJustAdded}
              onChangeSelectedCard={this._handleChangeSelectedCard}
              selectedCard={this.state.selectedCard}
              paymentWithWallet={this.state.paymentWithWallet}
              me={viewer.me}
              router={this.props.router}
              processing={this.state.process}
            />
          : null
        }
        {
          viewer.me && this.state.displayCompletePersonProfilePopup &&
          <CompletePersonProfilePopup
              sportunity={viewer.sportunity}
              onConfirm={this._handleConfirmProfileUpdate}
              onClose={this._handleHideCompleteProfilePopup}
              viewer={viewer}
              me={this.state.me}
              processing={this.state.process}
          />
        }
        {
          viewer.me && this.state.displayCompleteBusinessProfilePopup &&
          <CompleteBusinessProfilePopup
              sportunity={viewer.sportunity}
              onConfirm={this._handleConfirmBusinessProfileUpdate}
              onClose={this._handleHideCompleteBusinessProfilePopup}
              viewer={viewer}
              me={this.state.me}
              processing={this.state.process}
          />
        }
        {
          viewer.me && this.state.displayAddACardPopup ?
            <AddACard
              sportunity={viewer.sportunity}
              onConfirm={this._handleConfirmAddACard}
              onClose={this._handleHideACard}
              viewer={viewer}
              me={this.state.me}
              processing={this.state.process}
            />
          : null
        }
        <section style={styles.pageContainer}>
          <div style={styles.sideContainer}>
          </div>
          <Wrapper style={styles.wrapper}>
            <Header
              viewer={viewer}
              style={styles.wrapper}
              sportunity={this.props.viewer.sportunity}
              showBook={shouldShowBook}
              enableBook={shouldEnableBook}
              showJoinWaitingList={this.state.shouldGoToJoinWaitingList}
              onBook={this._handleShowConfirmationPopup}
              onCancel={this._handleUserCancel}
              onAdminModify={this._handleAdminModify}
              onAdminReOrganize={this._handleAdminReOrganize}
              onAdminDisplayStatForm={this._handleToggleStatisticForm}
              isFillingStatForm={this.state.displayStatFilling}
              isAdmin={this.state.isAdmin}
              isSecondaryOrganizer={this.state.isSecondaryOrganizer}
              isPotentialSecondaryOrganizer={isPotentialSecondaryOrganizer}
              isAuthorizedAdmin={this.state.isAuthorizedAdmin}
              isPotentialOpponent={isPotentialOpponent}
              onSeeAsAdmin={this._handleSeeAsAdmin}
              onOpponentBook={this._handleConfirmOpponentCreation}
              isActive={this.state.isActive}
              isLogin={viewer.me !== null}
              isLoading={this.state.isLoading}
              user={viewer.me}
              pathname={this.props.location.pathname}
              title={viewer.sportunity.title}
              description={viewer.sportunity.description}
              location={this.props.location}
              {...this.state}
            />
            {this.state.displayStatFilling && this.state.isAdmin
            ? <StatsFilling
                onLeave={this._handleToggleStatisticForm}
                viewer={viewer}
                sportunityID={this.props.viewer.sportunity && this.props.viewer.sportunity.id}
                language={this.state.language}
                isPast={this.state.isPast}
                sportunity={this.props.viewer.sportunity}
              />
            : <div style={styles.container}>
                <Content
                  sportunity={this.props.viewer.sportunity}
                  chat={this.props.viewer.chat}
                  status={status}
                  showBook={shouldShowBook}
                  enableBook={shouldEnableBook}
                  showJoinWaitingList={this.state.shouldGoToJoinWaitingList}
                  onBook={this._handleShowConfirmationPopup}
                  onCancel={this._handleUserCancel}
                  onDeclineInvitation={this._handleDeclineInvitation}
                  onAdminModify={this._handleAdminModify}
                  onAdminCancel={this._handleAdminCancel}
                  onAdminReOrganize={this._handleAdminReOrganize}
                  onAdminDisplayStatForm={this._handleToggleStatisticForm}
                  userIsParticipant={userIsParticipant}
                  userIsOnWaitingList={userIsOnWaitingList}
                  isPotentialOpponent={isPotentialOpponent}
                  isPotentialSecondaryOrganizer={isPotentialSecondaryOrganizer}
                  viewer={viewer}
                  me={viewer.me}
                  user={viewer.me}
                  isAdmin={this.state.isAdmin}
                  isSecondaryOrganizer={this.state.isSecondaryOrganizer}
                  isAuthorizedAdmin={this.state.isAuthorizedAdmin}
                  onSeeAsAdmin={this._handleSeeAsAdmin}
                  onOpponentBook={this._handleConfirmOpponentCreation}
                  onSecondaryOrganizerSelectRole={this._handleSecondaryOrganizerSelectRole}
                  onSecondaryOrganizerRefuseRole={this._handleSecondaryOrganizerRefuseRole}
                  onSecondaryOrganizerCancel={this._handleSecondaryOrganizerCancel}
                  isPast={this.state.isPast}
                  isActive={this.state.isActive}
                  isCancelled={isCancelled}
                  userIsInvited={userIsInvited}
                  isLogin={viewer.me !== null}
                  shouldShowChat={shouldShowChat}
                  organizers={this.props.viewer.sportunity.organizers}
                  isLoading={this.state.isLoading}
                  {...this.state}
                  invited_circles={this.props.viewer.sportunity.invited_circles}
                  actionParticipant={this._handleShowConfirmationParticipantPopup}
                  onConfirm={this._confirmationUser}
                  userWasInvited={userWasInvited}
                  handleOrganizerAddParticipants={this._handleOrganizerAddParticipants}
                  handleOrganizerAddInviteds={this._handleOrganizerAddInviteds}
                  {...this.state}
                />
              </div>
            }
          </Wrapper>
          <div style={styles.sideContainer}>
            <Sharing
                sharedUrl = {appUrl + this.props.location.pathname}
                title = { 'Sportunity: ' + viewer.sportunity.title }
                description = {viewer.sportunity.description }
                {...this.state} />
          </div>
        </section>
        {viewer.me ? <Footer onUpdateLanguage={this._setLanguage} viewer={viewer} user={viewer.me}/> : <Footer onUpdateLanguage={this._setLanguage} viewer={viewer} user={null}/> }
      </div>
    );
  }
}

styles = {
  pageContainer: {
    display: 'flex',
    width: '100%',
    flexDirection: 'row',
  },
  sideContainer: {
    display: 'flex',
    flexDirection: 'column',
    marginTop: '25px',
    padding: 10,
    flex: 1,
    '@media (max-width: 700px)': {
      paddingLeft: 0,
      paddingRight: 0
    },
    '@media (max-width: 615px)': {
      position: 'absolute'
    }
  },
  container: {
    display: 'flex',
		boxShadow: '0 0 4px 0 rgba(0,0,0,0.4)',
    '@media (max-width: 600px)': {
      display: 'block',
    }
  },
	wrapper: {
		boxShadow: '0 0 4px 0 rgba(0,0,0,0.4)',
  },
  select: {
    marginTop: 15, 
    fontSize: 16, 
    minWidth: '50%',
    height: 28
  }
};

const _loggedFromPage = (value) => ({
  type: types.LOGGED_IN_FROM_PAGE,
  value,
})

const dispatchToProps = (dispatch) => ({
  _loggedFromPage: bindActionCreators(_loggedFromPage, dispatch),
})

const stateToProps = (state) => ({
})

let ReduxContainer = connect(
  stateToProps,
  dispatchToProps
)(Radium(EventView));

export default Relay.createContainer(Radium(ReduxContainer), {

  initialVariables: {
    sportunityId: null,
    userToken: null,
    queryAuthorizedAccounts: false,
    superToken: null,
    querySuperMe: false,
    queryIsCoOrganizerOnSerie: false
  },

  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        languages {
          id
          name
          code
        }
        IsCoOrganizerOnSerie (sportunityId: $sportunityId) @include(if: $queryIsCoOrganizerOnSerie)
        authorizedAccounts(userToken: $userToken) @include(if: $queryAuthorizedAccounts) {
          id
          avatar
          pseudo
          accounts {
            id,
            avatar
            token,
            pseudo
          }
        }
        superMe (superToken: $superToken) @include(if:$querySuperMe) {
          id,
          pseudo
          avatar
          subAccounts{
              id,
              avatar
              pseudo
              token
          }
        }
        me {
          id
          appCountry
          paymentMethods {
            id,
            cardMask
          }
          lastName,
          firstName,
          address {
            address,
            city,
            country
          }
          profileType,
          isProfileComplete,
          birthday,
          nationality,
          shouldDeclareVAT,
          business {
            businessName,
            businessEmail,
            headquarterAddress {
              address,
              city,
              country
            },
            VATNumber
          }
          ${BookUserMutation.getFragment('user')}
          ${NewOpponentSportunityMutation.getFragment('user')}
          ${AppHeader.getFragment('user')}
          ${Footer.getFragment('user')}
          ${Content.getFragment('user')},
          ${Header.getFragment('user')},
          ${Sidebar.getFragment('user')},
        }
        ${Header.getFragment('viewer')}
        ${BookUserMutation.getFragment('viewer')}
        ${AddACard.getFragment('viewer')}
        ${RegisterCardDataMutation.getFragment('viewer')}
        ${UpdateUserProfileMutation.getFragment('viewer')}
        ${RefuseInvitationMutation.getFragment('viewer')}
        ${NewOpponentSportunityMutation.getFragment('viewer')}
        ${OrganizerAddParticipantMutation.getFragment('viewer')}
        ${OrganizerAddInvitedMutation.getFragment('viewer')}
        ${SecondaryOrganizerPickRoleMutation.getFragment('viewer')}
        ${SecondaryOrganizerCancelRoleMutation.getFragment('viewer')}
        ${SecondaryOrganizerRefuseRoleMutation.getFragment('viewer')}
        ${StatsFilling.getFragment('viewer')}
        ${Sidebar.getFragment('viewer')},

        sportunity(id: $sportunityId) {
          id,
          title
          description,
          kind,
          images
          number_of_occurences,
          beginning_date, 
          ending_date,
          survey {
            isSurveyTransformed
            surveyDates {
              beginning_date
              ending_date
              answers {
                user {
                  id
                  pseudo
                  avatar
                }
                answer
              }
            }
          }
          participantRange {
            from,
            to
          },
          participants {
            id
            avatar
            pseudo
          },
          invited_circles (last: 10) {
            edges {
              node {
                id,
                name,
                mode
                type
                members {
                  id
                  pseudo, 
                  avatar
                }
                owner {
                  id
                  pseudo
                  avatar
                }
                askedInformation {
                  id, 
                  name
                }
                membersInformation {
                  information,
                  user {
                    id,
                  }
                  value
                }
              }
            }
          }
          hide_participant_list
          invited {
            user {
              id
              pseudo
              avatar
            }
          }
					waiting {
						id,
						pseudo
            avatar
					}
          price {
            currency,
            cents,
          },
          address {
            address
            city
            position {
              lat
              lng
            }
          }
          organizers {
            organizer {
              id
              pseudo
              avatar
              feedbacks {
                feedbacksList (last: 100) {
                  edges {
                    node {
                      author {
                        id
                      }
                    }
                  }
                }
              }
            }
            isAdmin
            role
            secondaryOrganizerType {
              id
              name {
                FR
                EN
                DE
                ES
              }
            }
            customSecondaryOrganizerType
            price {
              cents,
              currency
            }
          }
          pendingOrganizers {
            id
            circles (last: 20) {
              edges {
                node {
                  id
                  members {
                    id
                  }
                  name
                  memberCount
                }
              }
            }
            isAdmin
            role
            secondaryOrganizerType {
              id
              name {
                FR
                EN
                DE
                ES
              }
            }
            customSecondaryOrganizerType
            price {
              cents
              currency
            }
          }
          status
          sport {
            sport {
              logo
              name {
                EN
                FR
                DE
              }
            }
            allLevelSelected,
            levels {
              id
              EN {
                name
                skillLevel
                description
              }
              FR {
                name
                skillLevel
                description
              }
              DE {
                name
                skillLevel
                description
              }
            }
            certificates {
              name {
                EN,
                FR,
                DE
              }
            }
            positions {
              EN
              FR,
              DE
            }
          }
          game_information {
            opponent {
              organizer {
                id,
                pseudo,
                avatar
              }
              organizerPseudo 
              lookingForAnOpponent
              unknownOpponent
              invitedOpponents (last: 5) {
                edges {
                  node {
                    id,
                    name,
                    memberCount
                    members {
                      id
                    }
                  }
                }
              }
            }
          }
          ${Header.getFragment('sportunity')},
          ${Content.getFragment('sportunity')},
          ${Sidebar.getFragment('sportunity')},
          ${BookUserMutation.getFragment('sportunity')},
          ${ConfirmBookingPopup.getFragment('sportunity')}
          ${CancelParticipantSportunityMutation.getFragment('sportunity')}
          ${RefuseInvitationMutation.getFragment('sportunity')}
          ${NewOpponentSportunityMutation.getFragment('sportunity')}
          ${StatsFilling.getFragment('sportunity')}
        }
        chat(sportunityId: $sportunityId) {
          ${Content.getFragment('chat')}
        }
        ${ConfirmBookingPopup.getFragment('viewer')}
        ${AppHeader.getFragment('viewer')},
        ${Footer.getFragment('viewer')}
        ${Content.getFragment('viewer')}
      }
    `,
  },
});
