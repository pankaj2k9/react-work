import Relay from 'react-relay';
/**
*  Add new payment method mutation
*/
export default class RegisterCardDataMutation extends Relay.Mutation {
  /**
  *  Mutation
  */
  getMutation() {
    return Relay.QL`mutation Mutation{
      registerCardData
    }`;
  }
  /**
  *  Variables
  */
  getVariables = () => (
    {
      cardRegistrationId: this.props.cardRegistration.cardRegistrationId,
      registrationData: this.props.registrationData
    }
  )
  /**
  *  Fat query
  */
  getFatQuery() {
    return Relay.QL`
      fragment on registerCardDataPayload {
        viewer {
          me {
            paymentMethods {
              id,
              cardMask
            }
            lastName, 
            firstName,
            address {
              address,
              city, 
              country
            }
          }
        },
        paymentMethodId,
        clientMutationId
      }
    `;
  }

  /**
  *  Config
  */
  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        paymentMethodId: this.props.paymentMethodId,
        viewer: this.props.viewer.id,
      },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
        me {
          paymentMethods {
            id
            cardType
            cardMask
            expirationDate
          }
        }
      }
    `,
  };
}