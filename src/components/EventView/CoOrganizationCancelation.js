import React from 'react';

import { colors } from '../../theme';
import localizations from '../Localizations'

let styles;

const CoOrganizationCancelationRow = ({ onCancel }) => {
    
    return (
        <article style={styles.container}>
            <button
                style={styles.button}
                onClick={onCancel}
            >
                {localizations.event_pendingSecondaryOrganizerCancelationButton}
            </button> 
        </article>
    )
}


styles = {
  container: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row',
    fontFamily: 'Lato',
    marginBottom: 30,
    '@media (max-width: 480px)': {
      display: 'block',
    }
  },
  button: {
    backgroundColor: colors.red,
    color: colors.white,
    width: 280,
    borderRadius: 100,
    borderStyle: 'none',
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
    fontSize: 22,
    cursor: 'pointer',
    padding: '13px 5px',
  },
};



export default CoOrganizationCancelationRow; 