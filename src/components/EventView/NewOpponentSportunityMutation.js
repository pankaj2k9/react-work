import Relay from 'react-relay';

export default class newOpponentSportunityMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation {newOpponentSportunity}`;
  }

  getVariables() {
    return {
      sportunityId: this.props.sportunity.id,
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on newOpponentSportunityPayload{
        viewer {
          me {
            notifications
          }
          sportunities
          sportunity
        }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        viewer: this.props.viewer.id,
      },
    }];
  }

  getOptimisticResponse() {
    const viewerPayload = { id: this.props.viewer.id };

    return {
      sportunity: {
        id: this.props.sportunity.id,
      },
      viewer: viewerPayload,
    };
  }

  static fragments = {
    sportunity: () => Relay.QL`
      fragment on Sportunity {
        id,
      }
    `,
    viewer: () => Relay.QL`
    fragment on Viewer {
        id
      }
    `,
    user: () => Relay.QL`
      fragment on User {
        id
      }
    `,
  };
}
