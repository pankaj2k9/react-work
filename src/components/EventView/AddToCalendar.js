import React from 'react';
import Radium from 'radium';
import AlertContainer from 'react-alert'
import Relay from 'react-relay'

import localizations from '../Localizations'
import { colors } from '../../theme';
import UpdateCalendarMutation from './UpdateCalendarMutation'

let styles;

class AddToCalendar extends React.Component {
    constructor() {
        super();
        this.alertOptions = {
            offset: 60,
            position: 'top right',
            theme: 'light',
            transition: 'fade',
        };

        this.state = {
            isAlreadyAdded: false
        }
    }

    componentWillReceiveProps = (nextProps) => {
        let {calendar} = nextProps.user || {};
        
        if (calendar && calendar.sportunities && calendar.sportunities.edges && calendar.sportunities.edges.length > 0) {
            calendar.sportunities.edges.forEach(sportunity => {
            if (sportunity.node.id === nextProps.sportunity.id)
                this.setState({isAlreadyAdded: true}) ;
            })
        }
    }

    addToMyCalendar = () => {
        if (!this.props.user) {
            this.msg.show(localizations.event_login_needed_calendar, {
                time: 3000, 
                type: 'error'
            })
	        setTimeout(() => {
		        this.msg.removeAll();
	        }, 3000);
            return ;
        }
        let calendar = this.props.user.calendar 
        ? {
            sportunities: this.props.user.calendar.sportunities && this.props.user.calendar.sportunities.edges && this.props.user.calendar.sportunities.edges.length > 0 
            ? this.props.user.calendar.sportunities.edges.map(sportunity => sportunity.node.id) 
            : [],
            users: this.props.user.calendar.users && this.props.user.calendar.users.length > 0 
            ? this.props.user.calendar.users.map(user => user.id)
            : []
        } 
        : {
            sportunities:[],
            users:[]
        } ;
        calendar.sportunities.push(this.props.sportunity.id);
        
        this.props.relay.commitUpdate(
            new UpdateCalendarMutation({
                user: this.props.user,
                userIDVar: this.props.user.id,
                calendarVar: calendar,
            }),
            {
                onFailure: error => {
                    console.log(error);
                    this.msg.show(localizations.event_update_calendar_error, {
                        time: 3000,
                        type: 'error'
                    })
	                setTimeout(() => {
		                this.msg.removeAll();
	                }, 3000);
                },
                onSuccess: () => {
                    this.msg.show(localizations.event_update_calendar_success, {
                        time: 3000,
                        type: 'success',
                    });
	                setTimeout(() => {
		                this.msg.removeAll();
	                }, 3000);
                    this.setState({
                        isAlreadyAdded: true
                    })
                },
            }
        );
    }

    removeFromMyCalendar = () => {
        if (!this.props.user) {
            this.msg.show(localizations.event_login_needed_calendar, {
                time: 3000, 
                type: 'error'
            })
	        setTimeout(() => {
		        this.msg.removeAll();
	        }, 3000);
            return ;
        }
        let calendar = this.props.user.calendar 
            ? {
                sportunities: this.props.user.calendar.sportunities && this.props.user.calendar.sportunities.edges && this.props.user.calendar.sportunities.edges.length > 0 
                ? this.props.user.calendar.sportunities.edges.map(sportunity => sportunity.node.id) 
                : [],
                users: this.props.user.calendar.users && this.props.user.calendar.users.length > 0 
                ? this.props.user.calendar.users.map(user => user.id)
                : []
            } 
            : {
                sportunities:[],
                users:[]
            } ;

        let sportunityIndex = calendar.sportunities.findIndex(sportunity => {
            return (sportunity === this.props.sportunity.id)
        })
        calendar.sportunities.splice(sportunityIndex, 1);
        
        this.props.relay.commitUpdate(
            new UpdateCalendarMutation({
                user: this.props.user,
                userIDVar: this.props.user.id,
                calendarVar: calendar,
            }),
            {
                onFailure: error => {
                    console.log(error);
                    this.msg.show(localizations.event_update_calendar_error, {
                        time: 3000,
                        type: 'error'
                    });
	                setTimeout(() => {
		                this.msg.removeAll();
	                }, 3000);
                },
                onSuccess: () => {
                    this.msg.show(localizations.event_update_calendar_success, {
                        time: 3000,
                        type: 'success',
                    });
	                setTimeout(() => {
		                this.msg.removeAll();
	                }, 3000);
                    this.setState({
                        isAlreadyAdded: false
                    })
                },
            }
        );
    }

    render() {
        return (
            <div style={styles.container}>
                <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
                {this.state.isAlreadyAdded 
                ? <div style={styles.button} onClick={this.removeFromMyCalendar}>{localizations.event_remove_from_calendar}</div>
                : <div style={styles.button} onClick={this.addToMyCalendar}>{localizations.event_add_to_calendar}</div>
                }
            </div>
        );
    }

}

styles = {
    container: {
        
    },
    button: {
        cursor: 'pointer',
    }
};

export default Relay.createContainer(Radium(AddToCalendar), {
    fragments: {
        user: () => Relay.QL`
            fragment on User {
                id
                ${UpdateCalendarMutation.getFragment('user')}
                calendar {
                    sportunities (last: 100) {
                        edges {
                            node {
                                id
                            }
                        }
                    }
                    users {
                        id
                    }
                }
            }
        `
    }
})