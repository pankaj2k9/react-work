import Relay from 'react-relay';

export default class UpdateSportunityTypeMutation extends Relay.Mutation {

    getMutation() {
        return Relay.QL`mutation {updateSportunity}`;
    }

    getVariables() { 
        return {
            sportunityID: this.props.sportunity.id,
            sportunity:{
                sportunityType: this.props.sportunityTypeVar
            },
        };
    }

    getFatQuery() {
        return Relay.QL`
        fragment on updateSportunityPayload{
            viewer {
                sportunity {
                    sportunityType
                }
            }
        }
        `;
    }

    getConfigs() {
        return [{
            type: 'FIELDS_CHANGE',
            fieldIDs: {
                viewer: this.props.viewer.id,
            },
        }];
    }

    static fragments = {
        viewer: () => Relay.QL`
            fragment on Viewer {
                id,
            }
        `,
    };
}