import Relay from 'react-relay';

export default class VoteForManOfTheGameMutation extends Relay.Mutation {

    getMutation() {
        return Relay.QL`mutation {voteForManOfTheGame}`;
    }

    getVariables() {
        return {
            sportunityID: this.props.sportunity.id,
            participantID: this.props.voterForId,
        };
    }

    getFatQuery() {
      return Relay.QL`
        fragment on voteForManOfTheGamePayload {
          viewer {
            sportunity {
              manOfTheGameVotes
            }
          }
          edge {
            node {
              manOfTheGameVotes
            }
          }
        }
      `;
    }

    getConfigs() {
        return [{
            type: 'FIELDS_CHANGE',
            fieldIDs: {
                viewer: this.props.viewer.id,
            },
        }];
    }

    static fragments = {
      viewer: () => Relay.QL`
        fragment on Viewer {
          id
        }
      `,
      sportunity: () => Relay.QL`
        fragment on Sportunity {
          id
          manOfTheGameVotes {
            voter {
                id
              }
              votedFor {
                  id
                  pseudo,
                  avatar
              }
              date
          }
        }
      `,
    };
}
