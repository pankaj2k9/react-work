import React from 'react';
import Relay from 'react-relay';
import Radium from 'radium'

import { colors } from '../../theme';
import localizations from '../Localizations'

let styles;

const AskedCoOrganizationRow = ({ user, sportunity, onSelectRole, onRefuseRole, language }) => {
    
    let askedRoles = []; 
    sportunity.pendingOrganizers.forEach(pendingOrganizer => {
        if (pendingOrganizer.circles.edges.findIndex(edge => edge.node.members.findIndex(member => member.id === user.id) >= 0) >= 0) {
            askedRoles.push({
                id: pendingOrganizer.id,
                name: pendingOrganizer.secondaryOrganizerType ? pendingOrganizer.secondaryOrganizerType.name[language().toUpperCase()] : pendingOrganizer.customSecondaryOrganizerType
            })
        }
    })
    
    if (askedRoles.length > 0) 
      return (
        <article style={styles.container}>
          <button
            style={styles.button}
            onClick={onSelectRole}
          >
            {localizations.event_pendingSecondaryOrganizerValidationButton}
          </button> 
          <button
            style={styles.redButton}
            onClick={onRefuseRole}
          >
            {localizations.event_pendingSecondaryOrganizerRefuseButton}
          </button> 
        </article>
      )
    else
        return null; 
}


styles = {
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    fontFamily: 'Lato',
    marginBottom: 30,
    '@media (max-width: 480px)': {
      display: 'block',
    }
  },
  button: {
    backgroundColor: colors.green,
    color: colors.white,
    width: 280,
    borderRadius: 100,
    borderStyle: 'none',
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
    fontSize: 22,
    cursor: 'pointer',
    padding: '13px 5px',
    ':disabled': {
      cursor: 'not-allowed',
      backgroundColor: colors.gray,
    },
  },
  redButton: {
    backgroundColor: colors.red,
    color: colors.white,
    width: 280,
    borderRadius: 100,
    borderStyle: 'none',
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
    fontSize: 22,
    cursor: 'pointer',
    padding: '13px 5px',
    marginTop: 25, 
    ':disabled': {
      cursor: 'not-allowed',
      backgroundColor: colors.gray,
    },
  }
};



export default Relay.createContainer(Radium(AskedCoOrganizationRow), {
  fragments: {
    user: () => Relay.QL`
        fragment on User {
            id
        }
    `,
    sportunity: () => Relay.QL`
      fragment on Sportunity {
        pendingOrganizers {
            id
            circles (last: 20) {
              edges {
                node {
                  members {
                    id
                  }
                }
              }
            }
            role
            secondaryOrganizerType {
              id
              name {
                FR
                EN
                DE
                ES
              }
            }
            customSecondaryOrganizerType
          }
      }
    `,
    },
});
