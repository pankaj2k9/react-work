import React from 'react';
import Radium from 'radium';

import { colors } from '../../theme';
import localizations from '../Localizations'

let styles;

class TabHeader extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      hoverTab: null,
      width: null,
    }
	  this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
  }

	updateWindowDimensions() {
		this.setState({ width: window.innerWidth});
	}

	componentDidMount() {
    this.setState({width: window.innerWidth})
		if (typeof window !== 'undefined')
			window.addEventListener('resize', this.updateWindowDimensions);
  }

	componentWillUnmount() {
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

  onMouseEnter = tab => {
    this.setState({
        hoverTab: tab
    })
  }
  onMouseLeave = () => {
    this.setState({
        hoverTab: null
    })
  }

  render() {
    const { 
      viewer, 
      sportunity, 
      activeTab,
      isPast
    } = this.props

    const { 
        hoverTab
    } = this.state

    return(
        <div style={styles.container}>
            <div style={activeTab === 'main' ? styles.activeTab : hoverTab === 'main' ? styles.tabHover : styles.tab} key={"main"} onClick={() => this.props.onChange('main')} onMouseEnter={() => this.onMouseEnter('main')} onMouseLeave={this.onMouseLeave}>
                <img src="/assets/images/information.png" style={activeTab === 'main' ? styles.activeIcon : styles.icon}/>
              {this.state.width >= 600 && localizations.event_tab_info}
            </div>
            <div style={isPast ? styles.disabledTab : activeTab === 'chat' ? styles.activeTab : hoverTab === 'chat' ? styles.tabHover : styles.tab} key={"chat"} onClick={() => this.props.onChange('chat')} onMouseEnter={() => this.onMouseEnter('chat')} onMouseLeave={this.onMouseLeave}>
                <img src="/assets/images/comment_bubble.png" style={isPast ? styles.disabledIcon : activeTab === 'chat' ? styles.activeIcon : styles.icon}/>
              {this.state.width >= 600 && localizations.event_tab_chat}
            </div>
            <div style={isPast ? styles.disabledTab :  activeTab === 'carPooling' ? styles.activeTab : hoverTab === 'carPooling' ? styles.tabHover : styles.tab} key={"carPooling"} onClick={() => this.props.onChange('carPooling')} onMouseEnter={() => this.onMouseEnter('carPooling')} onMouseLeave={this.onMouseLeave}>
                <img src="/assets/images/car.png" style={isPast ? styles.disabledIcon : activeTab === 'carPooling' ? styles.activeIcon : styles.icon}/>
              {this.state.width >= 600 && localizations.event_tab_carPooling}
            </div>
            <div style={activeTab === 'media' ? styles.activeTab : hoverTab === 'media' ? styles.tabHover : styles.tab} key={"media"} onClick={() => this.props.onChange('media')} onMouseEnter={() => this.onMouseEnter('media')} onMouseLeave={this.onMouseLeave}>
                <img src="/assets/images/video-files.png" style={activeTab === 'media' ? styles.activeIcon : styles.icon}/>
              {this.state.width >= 600 && localizations.event_tab_media}
            </div>
        </div>
    )

    }
}

styles = {
    container: {
        minHeight: 60,
        backgroundColor: colors.lightGray,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: '-2px',
        flexWrap: 'wrap',
    },
    tab: {
        padding: '5px 10px',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        height: 60,
        cursor: 'pointer',
        fontSize: 16,
        fontFamily: 'Lato',
        transition: 'all cubic-bezier(0.22,0.61,0.36,1) .3s',
        fontWeight: 'bold',
        color: colors.darkGray,
    },
    tabHover: {
        padding: '5px 10px',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        height: 60,
        cursor: 'pointer',
        fontSize: 16,
        fontFamily: 'Lato',
        transition: 'all cubic-bezier(0.22,0.61,0.36,1) .3s',
        fontWeight: 'bold',
        backgroundColor: '#c9c9c9',
        color: colors.white, 
    },
    disabledTab: {
        padding: '5px 10px',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        height: 60,
        cursor: 'pointer',
        fontSize: 16,
        fontFamily: 'Lato',
        backgroundColor: 'rgb(225, 225, 225)',
        color: colors.gray,
        fontWeight: 'bold',
    },
    activeTab: {
        padding: '5px 10px',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.blue,
        flex: 1,
        height: 60,
        cursor: 'pointer',
        fontSize: 16,
        color: colors.white,
        fontFamily: 'Lato',
        fontWeight: 'bold',
    },
    icon: {
        width: 30,
        marginRight: 10
    },
    disabledIcon: {
        filter: 'grayscale(2)',
        width: 30,
        marginRight: 10
    },
    activeIcon: {
        filter: 'brightness(3)',
        width: 30,
        marginRight: 10
    }
};

export default Radium(TabHeader);