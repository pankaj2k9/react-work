import Relay from 'react-relay';

export default class RemoveEventImageUpdateMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      removeEventImage
    }`;
  }

  getVariables() {
    return {
      sportunityID: this.props.sportunity.id,
      imageUrl: this.props.url,
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on removeSportunityImagePayload{
        viewer {
          sportunity {
            images
          }
        }
        sportunity {
          id
          images
        }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        sportunity: this.props.sportunity.id,
      },
    }];
  }

  static fragments = {
    sportunity: () => Relay.QL`
      fragment on Sportunity {
        id,
        images
      }
    `,
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
      }
    `,
  };

}
