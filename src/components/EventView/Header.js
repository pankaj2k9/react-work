import React from 'react';
import Radium from 'radium';
import Relay from 'react-relay';
import AlertContainer from 'react-alert'
import { colors } from '../../theme';
import {formatDate} from './formatDate';
import localizations from '../Localizations'
import ReactTooltip from 'react-tooltip'
import AddToCalendar from './AddToCalendar';
import RelaunchInvited from './RelaunchInvited'
import Price from './Price';
import Sharing from "./Sharing";
import {appUrl} from "../../../constants";

let styles;

class Header extends React.Component {
  constructor(props) {
    super(props)
    this.alertOptions = {
      offset: 60,
      position: 'top right',
      theme: 'light',
      transition: 'fade',
    };
  }

  _sportNameTranslated = (sportName) => {
    let name = sportName.EN
    switch(localizations.getLanguage().toLowerCase()) {
      case 'en':
        name = sportName.EN
        break
      case 'fr':
        name = sportName.FR || sportName.EN
        break
      default:
        name = sportName.EN
        break
    }
    return name
  }

  _setLoginError = () => {
    this.msg.show(localizations.event_login_needed, {
      time: 3000,
      type: 'info',
    });
	  setTimeout(() => this.msg.removeAll(), 2000);
  }

  render() {
    const { 
      viewer, 
      sportunity, 
      user, 
      showBook, 
      showJoinWaitingList, 
      enableBook, 
      onBook, 
      onCancel, 
      onOpponentBook,
      isLogin, 
      onAdminModify, 
      onAdminReOrganize, 
      onAdminDisplayStatForm,
      isAuthorizedAdmin,
      isPotentialOpponent,
      onSeeAsAdmin,
      isFillingStatForm,
      isActive, 
      isAdmin, 
      isSecondaryOrganizer,
      isPotentialSecondaryOrganizer,
      isLoading,
      pathname,
      title,
      description
    } = this.props

  return(
  <header style={styles.header}>
    <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
    <ReactTooltip effect="solid" multiline={true}/>
    {/*<div style={styles.row}>*/}
      {/*<Sharing*/}
        {/*sharedUrl = {appUrl + pathname}*/}
        {/*title = { 'Sportunity: ' + title }*/}
        {/*description = { description }*/}
        {/*{...this.state}/>*/}
    {/*</div>*/}
    <div style={styles.row}>
      <div style={styles.left}>
        <div style={styles.sportCol}>
          <div style={{ ...styles.userpic, backgroundImage: `url(${sportunity.sport.sport.logo})` }} />
          <p style={styles.sportName}>{this._sportNameTranslated(sportunity.sport.sport.name)}</p>
        </div>
        <div style={styles.userInfo}>
          <h1 style={styles.title}>{sportunity.title}</h1>
          <div style={styles.address}>
            <i
              style={styles.marker}
              className="fa fa-map-marker"
              aria-hidden="true"
            />
            {sportunity.address && sportunity.address.address+', '+sportunity.address.city}
          </div>
          <div style={styles.participants}>
            <i
              style={styles.participantsMarker}
              className="fa fa-users"
              aria-hidden="true"
            />
            {localizations.formatString(sportunity.participants.length <= 1 
                ? localizations.event_numParticipant
                : localizations.event_numParticipants, sportunity.participants.length)}
                &nbsp;({localizations.formatString(localizations.event_min, sportunity.participantRange.from)} 
                &nbsp;- {localizations.formatString(localizations.event_max, sportunity.participantRange.to)})
          </div>
          <div style={styles.datetime}>
            <i
              style={styles.dateMarker}
              className="fa fa-calendar"
              aria-hidden="true"
            />
            {formatDate(sportunity.beginning_date, sportunity.ending_date)}
          </div>
          
        </div>
      </div>
      <div style={styles.info}>
        <Price 
          sportunity={sportunity}
          viewer={viewer}
          header={true}
        />
        <div style={styles.privateOrPublic}>
          {localizations['event_kind_' + sportunity.kind.toLowerCase()]}
        </div>
      </div>
    </div>
    <div style={styles.menuRow}>
      <AddToCalendar 
        user={user}
        sportunity={sportunity}
      />
      {isAdmin && sportunity.invited && sportunity.invited.length > 0 &&
        <RelaunchInvited
          sportunity={sportunity}
          viewer={viewer}
        />
      }
    </div>
  </header>
)}}

Header.defaultProps = {
  title: 'Basketball for fun',
  address: 'Gen�ve, Switzerland',
  participants: 50,
};

styles = {
  error: {
    fontFamily: 'Lato',
    color: colors.red,
    fontSize: 16,
    marginTop: 10,
    width: 180,
  },
  header: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    backgroundColor: colors.blue,
    color: colors.white,
    padding: '47px 42px 25px',
    '@media (max-width: 600px)': {
      padding: '47px 30px 25px',
    },
    '@media (max-width: 750px)': {
      display: 'block',
    }
  },
  menuRow: {
    marginTop: 15,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    fontSize: 16,
    fontFamily: 'Lato',
    color: colors.lightGray,
  },

  row: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    '@media (max-width: 750px)': {
      flexDirection: 'column'
    }
  },

  left: {
    display: 'flex',
    alignItems: 'flex-start',
    '@media (max-width: 480px)': {
      display: 'block',
      textAlign: 'center',
    }

  },

  sportCol: {
    display: 'flex',
    flexDirection: 'column',
    marginRight: 54,
    alignItems: 'center',
    '@media (max-width: 480px)': {
      margin: '0 auto 10px auto',
    }
  },

  sportName:{
    fontSize: 22,
    fontFamily: 'Lato',
    color: colors.black,
    marginTop: 5
  },

  userpic: {
    borderRadius: '50%',
    width: 120,
    height: 120,
     backgroundSize: 'cover',
    backgroundPosition: 'center',
  },

  userInfo: {
    color: colors.white,
    fontFamily: 'Lato',
    fontSize: 22,
    fontWeight: 500,
  },

  title: {
    fontSize: 36,
    marginBottom: 13,
    maxWidth: 500,
  },

  address: {
    marginBottom: 17,
  },

  marker: {
    color: colors.white,
    marginRight: 18,
  },

  participants: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: '15px',
    '@media (max-width: 480px)': {
      display: 'block',
      textAlign: 'center',
    }
  },

  participantsMarker: {
    color: colors.white,
    fontSize: 16,
    marginRight: 14,
  },

  price: {
    fontSize: 22,
    fontWeight: 'bold',
    color: colors.green,
    fontFamily: 'Lato',
  },
  privateOrPublic: {
    fontSize: 20,
    fontFamily: 'Lato',
    '@media (max-width: 480px)': {
      display: 'block',
      textAlign: 'center',
      marginBottom: '15px',
    }
  },

  info: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
  },

  datetime: {
    color: colors.white,
    fontFamily: 'Lato',
    marginBottom: 26,
  },

  dateMarker: {
    color: colors.white,
    fontSize: 16,
    marginRight: 12,
  },

  book: {
    backgroundColor: colors.green,
    color: colors.white,
    width: 180,
    // height: 57,
    borderRadius: 100,
    borderStyle: 'none',
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
    fontSize: 22,
    cursor: 'pointer',
    padding: '13px 5px',
    ':disabled': {
      cursor: 'not-allowed',
      backgroundColor: colors.gray,
    },
  },
  statButton: {
    backgroundColor: colors.green,
    color: colors.white,
    width: 180,
    borderRadius: 100,
    borderStyle: 'none',
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
    fontSize: 22,
    cursor: 'pointer',
    padding: '13px 5px',
    marginTop: 10
  },
	cancel: {
    backgroundColor: colors.red,
    color: colors.white,
    width: 180,
    height: 57,
    borderRadius: 100,
    borderStyle: 'none',
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
    fontSize: 22,
    cursor: 'pointer',

    ':disabled': {
      cursor: 'not-allowed',
      backgroundColor: colors.gray,
    },
  },
  policy:{
    cursor: 'pointer',
    fontSize: 16,
    textAlign: 'center',
    marginTop: 15,
    fontFamily: 'Lato',
  }
};

export default Relay.createContainer(Radium(Header), {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
        
        ${RelaunchInvited.getFragment('viewer')}
        ${Price.getFragment('viewer')}
      }
    `,
    user: () => Relay.QL`
      fragment on User {
        id,
        ${AddToCalendar.getFragment('user')}
        areStatisticsActivated
      }
    `,
    sportunity: () => Relay.QL`
      fragment on Sportunity {
        ${Price.getFragment('sportunity')}
        id
        title,
        address {
          address,
          city
        },
        participants {
          id
        },
        invited {
          user {
            id,
            pseudo,
            avatar
          }
        }
        price {
          cents, 
          currency
        }
        participantRange {
          from,
          to,
        },
        kind,
        beginning_date,
        ending_date,
        sport {
          sport {
            logo
            name {
              EN
              FR
              DE
            }
          }
          allLevelSelected,
          levels {
            id
            EN {
              name
              skillLevel
              description
            }
            FR {
              name
              skillLevel
              description
            }
            DE {  
              name
              skillLevel
              description
            } 
          }
          certificates {
            name {
              EN,
              FR,
              DE
            }
          }
          positions {
            EN
            FR,
            DE
          }
        }
      }
    `,
  },
});
