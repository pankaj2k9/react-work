import Relay from 'react-relay';

export default class UpdateUserProfileMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      upUser
    }`;
  }

  getVariables() {
    return {
      userID: this.props.userIDVar,
      user: {
        lastName: this.props.lastNameVar, 
        firstName: this.props.firstNameVar,
        address: {
          address:this.props.addressVar.address,
          city: this.props.addressVar.city,
          country: this.props.addressVar.country
        },
        nationality: this.props.nationalityVar,
        birthday: this.props.birthdayVar,
        shouldDeclareVAT: this.props.shouldDeclareVATVar,
        business: {
          businessName: this.props.businessNameVar,
          businessEmail: this.props.businessEmailVar,
          headquarterAddress: this.props.headquarterAddressVar ? {
            address: this.props.headquarterAddressVar.address,
            city: this.props.headquarterAddressVar.city,
            country: this.props.headquarterAddressVar.country
          } : null,
          VATNumber: this.props.VATNumberVar
        }
      },

    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on upUserPayload {
        clientMutationId,
        user {
          id
          firstName,
          lastName,
          nationality,
          birthday,
          shouldDeclareVAT,
          address {
            address,
            country,
            city,
            zip,
            position
          }
          business {
            businessName,
            businessEmail,
            VATNumber,
            headquarterAddress {
              address,
              city,
              country
            }
          }
          isProfileComplete
        }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
        fieldIDs: {
          user: this.props.userIDVar,
        },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        me {
          id
        }
      }
    `,
  };

}
