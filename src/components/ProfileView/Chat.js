import React from 'react';
import PureComponent, { pure } from '../common/PureComponent'
import Radium from 'radium';
import Relay from 'react-relay'

import Message from './Message';
import Form from './Form';

import SendMessageMutation from './SendMessageMutation';
import ReadChatMutation from './ReadChatMutation';

let styles;



class Chat extends PureComponent {
  constructor() {
    super()
    this.state = {
      sendingMessage: false
    }
  }
  componentDidMount() {
    this._scrollToBottom();
    this._readChat(this.props.chat);
    // this.props.relay.forceFetch();
  }

  _readChat = (chat) => {
      const viewer = this.props.viewer
      this.props.relay.commitUpdate(
        new ReadChatMutation({ viewer, chat }),
        {
          onFailure: error => console.log(error.getError()),
          onSuccess: (response) => {
            console.log(response)
          },
        }
      );
    }

  _scrollToBottom = () => {
    this._commentsNode.scrollTop = this._commentsNode.scrollHeight;
  }

  _scrollAfterNewMessage = () => {
    setTimeout(() => {
      if (this.props.relay.pendingVariables)
        this._scrollAfterNewMessage();
      else {
        this._scrollToBottom();
      }
    }, 100);
  }

  _handleSendMessage = (message) => {
    if (message && message.length > 0) {
      this.setState({
        sendingMessage: true
      })
      this.props.relay.commitUpdate(
        new SendMessageMutation({
          viewer: this.props.viewer,
          chat: this.props.chat,
          message: {
            text: message,
            author: this.props.viewer.me.id
          },
          chatId: this.props.chat.id
        }), {
          onSuccess: () => {
            this.setState({
              sendingMessage: false
            })
            this._scrollAfterNewMessage();
          },
          onFailure: (error) => {
            console.log("error sending the message")
            this.setState({
              sendingMessage: false
            })
          },
        }
      );
    }
  }

  render() {
    return (
      <div style={styles.container}>
        <div style={styles.comments} ref={node => { this._commentsNode = node; }}>
          <div style={styles.commentsList}>
            {this.props.chat && this.props.chat.messages.edges.map((message,index) => <Message key={index} message={message.node} me={this.props.viewer.me}/>)}
          </div>
        </div>
        { this.props.chat &&
          <Form
            sendMessage={this._handleSendMessage}
            user={this.props.me}
            sendingMessage={this.state.sendingMessage}
            />
        }
      </div>
    );
  }
}

styles = {
  container: {
    border: '1px solid #C3C3C3',
    borderRadius: '5px',

    // width: 720,
    height: 390,

    padding: 10,

    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    margin: 'auto'
  },

  comments: {
    flexGrow: 1,
    overflowX: 'hidden',
    overflowY: 'scroll',
    marginBottom: 10,
  },

  commentsList: {
  },
};

export default Relay.createContainer(Radium(Chat), {

  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
        me {
          id
          avatar
        }
        ${SendMessageMutation.getFragment('viewer')}
        ${ReadChatMutation.getFragment('viewer')}
      }
    `,
    me: () => Relay.QL`
      fragment on User {
        avatar
      }
    `,
    chat: () => Relay.QL`
      fragment on Chat {
        ${SendMessageMutation.getFragment('chat')}
        ${ReadChatMutation.getFragment('chat')}
        id
        isActive
        messages (last:20) {
          edges {
            node {
              ${Message.getFragment('message')}
            }
          }
        }
      }
    `
  }
});
