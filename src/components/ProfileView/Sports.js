import React from 'react';
import PureComponent, { pure } from '../common/PureComponent'
import Radium from 'radium';
import Relay from 'react-relay';
import localizations from '../Localizations'
import { colors, fonts, metrics } from '../../theme'

let styles;

import Sport from './Sport'


class Sports extends PureComponent {
  render() {
    const { sports } = this.props;

    return(
			<div>
				<h2 style={styles.title}>{localizations.profileView_sports}</h2>
        {sports && sports.length
          ? <ul style={styles.sports}>
              {sports.map(sport => <Sport key={sport.sport.id} sport={sport} />)}
            </ul>
          : <p style={styles.description}>{this.props.user.pseudo} {localizations.profileView_no_sport_selected}</p>}
		  </div>
    )
  }
}


styles = {

  sports: {
    display: 'flex',
    flexWrap: 'wrap',
    width: 600,
    margin: '0 0 40 0',
    padding: 0,

    fontFamily: 'Lato',
    color: 'rgba(0,0,0,0.65)',
    '@media (max-width: 610px)': {
      width: 400
    },
    '@media (max-width: 425px)': {
      width: 300
    }
  },
	title: {
    fontSize: 32,
    fontWeight: 500,
    marginBottom: 30,
  },
  description: {
    fontSize: 18,
    lineHeight: 1.2,
    marginBottom: 40,
  },
};

export default Relay.createContainer(Radium(Sports), {
  fragments: {
    sports: () => Relay.QL`
      fragment on SportDescriptor @relay(plural: true) {
        sport {
          id
          name {
            EN
            FR
            DE
          }
          logo
        }
        levels {
          id
          EN {
            name
          }
          FR {
            name
          }
          DE {
            name
          }
        }
        certificates {
          certificate {
            name {
              EN
              FR
              DE
            }
          }
        }
        positions {
          EN
          FR
          DE
        }
        assistantType {
          name {
            EN,
            FR,
            DE
          }
        }
      }
    `,
  },
});
