import Relay from 'react-relay';

export default class UnollowMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      unfollowUser
    }`;
  }

  getVariables() {
    return {
      userID: this.props.userId,
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on unfollowUserPayload @relay(pattern: true){
        clientMutationId,
        user,
        viewer
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
        fieldIDs: {
          viewer: this.props.viewer.id,
        },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
      }
    `,
  };
}
