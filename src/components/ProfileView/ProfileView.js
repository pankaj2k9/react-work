import React from 'react';
import PureComponent, { pure } from '../common/PureComponent'
import Radium from 'radium';
import AlertContainer from 'react-alert'
import platform from 'platform';
import Relay from 'react-relay';
import { Link } from 'react-router';

import AppHeader from '../common/Header/Header.js'
import Footer from '../common/Footer/Footer'
import Wrapper from './Wrapper';
import Header from './Header';
import Sidebar from './Sidebar';
import Content from './Content';
import Helmet from 'react-helmet';
import TabMenu from './TabMenu'
import OrganizedSportunitiesStatistics from './OrganizedSportunitiesStatistics';
import TeamsStats from './TeamsStats';
import ClubsStats from './ClubsStats';
import IndividualStats from './IndividualStats';
import ConfirmationModal from '../common/ConfirmationModal';

import localizations from '../Localizations'
import { colors, fonts } from '../../theme'
import { appUrl } from '../../../constants.json';
import Chat from "./Chat";
import Events from "./Events";
import {browserHistory} from "react-router";

let styles


class ProfileView extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      language: localizations.getLanguage(),
      blockModalIsOpen: false,
      showChat: false,
      chatIsQueried: false,
      userId: null, // work around for an issue with Chat
      avatar: null,
      activeTab: 'profile',
      loadMoreQueryIsLoading: false,
      pageSize: 10,
      itemCount: 2,
      displayAndroidOpenApp: false,
      circleId: null,
      showCircle: true,
    }
    this.alertOptions = {
      offset: 60,
      position: 'top right',
      theme: 'light',
      transition: 'fade',
    };
  }
  _setLanguage = (language) => {
    this.setState({ language: language })
  }
  renderMetaTags = (user) => {
    return <Helmet>
              <title>{"Sportunity - Profile de "+user.pseudo}</title>
              <meta name="robots" content="noindex"/>
              <meta name="description" content={localizations.meta_description}/>
              <meta property="fb:app_id" content="1759806787601548"/>
              <meta property="og:type" content="profile"/>
              <meta property="og:title" content={"Sportunity - Profile de "+user.pseudo} />
              <meta property="og:description" content={localizations.meta_description}/>
              <meta property="og:url" content={appUrl + this.props.location.pathname}/>
              <meta property="og:image" content={user.avatar} />
              <meta property="og:image:width" content="150"/>
              <meta property="og:image:height" content="150"/>
          </Helmet>
  }

  componentDidMount = () => {
    if (platform.name.indexOf("Firefox") < 0) {
      if (platform.os.family === "iOS") {
        if (typeof window !== 'undefined')
          window.location.href = "sportunity://profile/"+this.props.userId;
      }
      else if (platform.os.family === "Android") {
        this.setState({displayAndroidOpenApp: true})
      }
    }
    this.props.relay.forceFetch();
    if (this.props.userId) {
      this.setState({
        userId: this.props.userId
      })
    }
    if (this.props.viewer.user.avatar) {
      this.setState({avatar: this.props.viewer.user.avatar})
    }
    if (this.props.location.hash === '#chat') {
      this.setState({
        activeTab: 'chat'
      })
    }
  }

  componentWillReceiveProps = (nextProps) => {
    if (this.state.userId && this.state.userId !== nextProps.userId)
      this.setState({
        avatar: nextProps.viewer.user.avatar,
        userId: nextProps.userId,
      })
    if (nextProps.activeTab) {
      let tabList = ['profile', 'event', 'chat', 'statistics'];
      if (tabList.findIndex(tab => tab === this.props.activeTab) >= 0)
        this._changeTab(nextProps.activeTab)
    }
    if (nextProps.circleId)
      this.setState({
        circleId: nextProps.circleId
      });
    if (this.state.userId && this.state.userId !== nextProps.userId && this.state.showChat) {
      this.props.relay.setVariables({
        queryChat: false
      });
      this.setState({
        showChat: false,
        chatIsQueried: false
      })
    }
    if (nextProps.viewer && nextProps.viewer.me && nextProps.viewer.me.id && !this.state.chatIsQueried) {
      nextProps.relay.setVariables({
        queryChat: true
      })
      this.setState({
        showChat: true,
        chatIsQueried: true
      })
    }
  }

  _handleShowChat = () => { // As chat is displayed by default when user is logged in, only anonymous can call this function
    this.msg.show(localizations.popup_profileView_login_needed, {
      time: 3000,
      type: 'info',
    });
  }


  _showChatSection = () => {
    setTimeout(() => {
      if (this.props.relay.pendingVariables)
        this._showChatSection();
      else {
        this.setState({
          showChat: true
        })
      }
    }, 100);
  }

  _loadMoreQueryIsLoaded = () => {
		if (this.props.relay.variables.first === this.state.itemCount) {
			this.setState({
				loadMoreQueryIsLoading: false,
			})
		} else {
			setTimeout(this._loadMoreQueryIsLoaded, 200)
		}
	}

  _loadMore = () => {
    const nextCount = this.state.itemCount + this.state.pageSize
    this.props.relay.setVariables({ first: nextCount }, this._loadMoreQueryIsLoaded)
    this.setState({
      itemCount: nextCount,
			loadMoreQueryIsLoading: true,
    })
  }

  openAndroidApp = () => {
    document.location="sportunity://profile/"+this.props.userId;
  }

  _changeCircle = (circleID) => {
    this.setState({
      circleId: circleID,
      showCircle: circleID !== null
    })
  };

  _changeTab = (tab) => {
    const {viewer} = this.props;
    if (this.props.activeTab !== tab)
      browserHistory.push('/profile-view/' + this.props.userId + '/' + tab)
    this.setState({
      activeTab: tab
    });
  }

  render() {
    const { viewer } = this.props;
    const isSelfProfile = viewer.me && this.props.userId && viewer.me.id === this.props.userId ;

    return (
      <div style={styles.wrapper}>
        {this.renderMetaTags(viewer.user)}
        {
          viewer.me ?
						<AppHeader
							user={viewer.me}
              viewer={viewer}
              {...this.state}
						/>
          :
            <AppHeader viewer={viewer} user={null} {...this.state} />
        }
        <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
        {this.state.displayAndroidOpenApp && <ConfirmationModal
          isOpen={true}
          title={localizations.android_open_appTitle}
          message={localizations.android_open_appText}
          confirmLabel={localizations.android_open_appConfirm}
          cancelLabel={localizations.android_open_appCancel}
          canCloseModal={true}
          onConfirm= {this.openAndroidApp}
          onCancel={() => {}}
        />}
        <Wrapper>

          <Header
            user={this.props.viewer.user}
            userId={this.props.userId}
            viewer={viewer}
            me={viewer.me}
            isSelfProfile={isSelfProfile}
            {...this.state}
          />
          <div style={styles.container}>
            {/*{(isSelfProfile || !viewer.statisticPreferences.private)*/}
              {/*&& (viewer.user && viewer.user.areStatisticsActivated) &&*/}
              <TabMenu
                changeActiveTab={(tab) => this._changeTab(tab)}
                activeTab={this.state.activeTab}
                haveChat={this.state.chatIsQueried && this.props.viewer.chat && !isSelfProfile}
                />
            {/*}*/}
            {this.state.activeTab === 'profile' &&
              <Content  user={this.props.viewer.user}
                me={this.props.viewer.me}
                viewer={this.props.viewer}
                sportunities={this.props.viewer.sportunities}
                chat={this.props.viewer.chat ? this.props.viewer.chat : null}
                shouldShowChat={this.state.chatIsQueried && this.props.viewer.chat}
                onShowChat={this._handleShowChat}
                isSelfProfile={isSelfProfile}
                onLoadMore={this._loadMore}
                {...this.state}/>
            }
            { this.state.activeTab === 'event' &&
              <div style={styles.content}>
                {
                  this.props.viewer.sportunities &&
                  <Events
                    sportunities={this.props.viewer.sportunities}
                    title={localizations.profileView_current_events}
                    user={this.props.viewer.user}
                    viewer={this.props.viewer}
                    loadMoreQueryIsLoading={this.state.loadMoreQueryIsLoading}
                    onLoadMore={this._loadMore}
                    shouldQueryUserStatus={true}/>
                }
                {
                  this.props.viewer.user.sportunities &&
                  <Events
                    sportunities={this.props.viewer.user.sportunities}
                    title={localizations.profileView_past_events}
                    user={this.props.viewer.user}
                    viewer={this.props.viewer}
                    loadMoreQueryIsLoading={this.state.loadMoreQueryIsLoading}
                    onLoadMore={this._loadMore}
                    shouldQueryUserStatus={false}/>
                }
                {viewer.user && ((isSelfProfile && this.props.viewer.sportunities && this.props.viewer.sportunities.count === 0 && this.props.viewer.user.sportunities && this.props.viewer.user.sportunities.count === 0) || (!isSelfProfile && this.props.viewer.user.sportunities && this.props.viewer.user.sportunities.count === 0)) &&
                  <div style={styles.noActivity}>
                    {isSelfProfile 
                    ? localizations.profileView_no_sportunity_you
                    : viewer.user.pseudo + " " + localizations.profileView_no_sportunity
                    }
                    {isSelfProfile &&
                      <Link to='/new-sportunity' style={{textDecoration: 'none', color: colors.blue, marginLeft: 5}}>
                        {localizations.profileView_no_sportunity_organize}
                      </Link>
                    }
                  </div>
                }
              </div>
            }
            {
              this.state.activeTab === 'chat' && !(this.state.chatIsQueried && this.props.viewer.chat) && !isSelfProfile &&
              <div style={styles.chatButtonContainer}>
                <div style={styles.chatButton} onClick={this.props.onShowChat}>
                  {localizations.profile_chat_button+' '+this.props.viewer.user.pseudo}
                </div>
              </div>
            }
            {
              this.state.activeTab === 'chat' && this.state.chatIsQueried && this.props.viewer.chat && !isSelfProfile &&
              <section style={styles.chatBox}>
                <h2 style={styles.title}>{localizations.profile_chat_title}</h2>
                <Chat
                  viewer={this.props.viewer}
                  user={this.props.viewer.user}
                  me={this.props.viewer.me}
                  chat={this.props.viewer.chat ? this.props.viewer.chat : null}
                />
              </section>
            }
            {this.state.activeTab === 'statistics' &&
              <div>
                {/*<OrganizedSportunitiesStatistics*/}
                  {/*viewer={this.props.viewer}*/}
                  {/*userId={this.state.userId}*/}
                {/*/>*/}
                {viewer.user.profileType !== 'PERSON' ?
                  viewer.user.subAccounts && viewer.user.subAccounts.length > 0 ?
                      <ClubsStats
                        viewer={this.props.viewer}
                        user={this.props.viewer.user}
                        userId={this.state.userId}
                      />
                    :
                    <TeamsStats
                      viewer={this.props.viewer}
                      user={this.props.viewer.user}
                      userId={this.state.userId}
                    />
                  :
                  <IndividualStats
                    viewer={this.props.viewer}
                    user={this.props.viewer.user}
                    userId={this.state.userId}
                  />
                }
              </div>
            }
            {/*<Sidebar user={this.props.viewer.user} {...this.state}/>*/}
          </div>
        </Wrapper>
        {viewer.me ? <Footer onUpdateLanguage={this._setLanguage} viewer={viewer} user={viewer.me}/> : <Footer onUpdateLanguage={this._setLanguage} viewer={viewer} user={null}/> }

      </div>
    );
  }
}

styles = {
  wrapper: {
    overflowX: 'hidden',
  },
  container: {},
  title: {
    fontSize: 32,
    fontWeight: 500,
    marginBottom: 30,
  },
  chatBox: {
    marginBottom: 35,
    padding: '40px',
    fontFamily: 'Lato',
  },
  chatButtonContainer: {
    margin: '35px auto',
    textAlign: 'center'
  },
  chatButton: {
    textDecoration: 'none',
    backgroundColor: colors.green,
    color: colors.white,
    borderRadius: 100,
    borderStyle: 'none',
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
    fontSize: 22,
    cursor: 'pointer',
    padding: '10px 25px',
    maxWidth: 350,
    margin: 'auto',
    fontFamily: 'Lato'
  },
  content: {
    flexGrow: '1',
    padding: '40px',
    fontFamily: 'Lato',
    color: 'rgba(0,0,0,0.65)',
    '@media (max-width: 483px)': {
      padding: '40px 20px'
    }
  },
  noActivity: {
    fontSize: 18,
    fontFamily: 'Lato',
    color: colors.darkGray,
    marginTop: 30,
    marginLeft: 10
  },
}

export default Relay.createContainer(Radium(ProfileView), {

  initialVariables: {
    userId: null,
    queryChat: false,
    queryStatsIndividual: false,
    queryStatsClubs: false,
    queryStatsTeams: false,
    first: 2,
  },

  fragments: {
    viewer: ({userId}) => Relay.QL`
      fragment on Viewer {
        ${Header.getFragment('viewer')},
        ${Content.getFragment('viewer')},
        ${OrganizedSportunitiesStatistics.getFragment('viewer')}
        ${TeamsStats.getFragment('viewer')}
        ${ClubsStats.getFragment('viewer')}
        ${IndividualStats.getFragment('viewer')}
        ${Chat.getFragment('viewer')}
        ${Events.getFragment('viewer')},
        id,
        statisticPreferences (userID: $userId) {
          private
        }
        me {
          id
          ${AppHeader.getFragment('user')},
          ${Footer.getFragment('user')}
          ${Content.getFragment('me')},
          ${Header.getFragment('me')},
          ${Chat.getFragment('me')}
        }
        user(id: $userId) {
          pseudo
          avatar
          areStatisticsActivated
          profileType
          subAccounts {
            id
            pseudo
            avatar
            userStatistics {
              averageNumberOfParticipatedWeek
              averageNumberOfParticipatedMonth
              averageNumberOfParticipatedYear
            }
          }
          sportunities (first: $first) {
            count
            ${Events.getFragment('sportunities')},
          }
          ${Header.getFragment('user', {userId})},
          ${Content.getFragment('user')},
          ${Sidebar.getFragment('user')},
          ${TeamsStats.getFragment('user')}
          ${ClubsStats.getFragment('user')}
          ${IndividualStats.getFragment('user')}
        }
        chat (userId: $userId) @include(if: $queryChat) {
          ${Chat.getFragment('chat')}
        }
        sportunities (first:$first, userId: $userId, filter: {status: MySportunities}) {
          count
          ${Events.getFragment('sportunities')},
        }
        ${AppHeader.getFragment('viewer')},
        ${Footer.getFragment('viewer')}
      }
    `,
  },
});
