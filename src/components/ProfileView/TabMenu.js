import React from 'react';
import Radium from 'radium';

import { colors } from '../../theme';
import localizations from '../Localizations'

let styles;

class TabMenu extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      hoverTab: null
    }
  }
  onMouseEnter = tab => {
    this.setState({
        hoverTab: tab
    })
  }
  onMouseLeave = () => {
    this.setState({
        hoverTab: null
    })
  }

  render() {
    const {
      viewer,
      haveChat,
      sportunity,
      activeTab,
      isPast
    } = this.props
    const { 
      hoverTab
    } = this.state

    return(
      <div style={styles.container}>
        <div style={activeTab === 'profile' ? styles.activeTab : hoverTab === 'profile' ? styles.tabHover : styles.tab} key={"profile"} onClick={() => this.props.changeActiveTab('profile')} onMouseEnter={() => this.onMouseEnter('profile')} onMouseLeave={this.onMouseLeave}>
          <img src="/assets/images/information.png" style={activeTab === 'profile' ? styles.activeIcon : styles.icon}/>
          {localizations.profileView_tab_profile}
        </div>
        <div style={isPast ? styles.disabledTab :  activeTab === 'event' ? styles.activeTab : hoverTab === 'event' ? styles.tabHover : styles.tab} key={"event"} onClick={() => this.props.changeActiveTab('event')} onMouseEnter={() => this.onMouseEnter('event')} onMouseLeave={this.onMouseLeave}>
          <img src="/assets/images/activity_blue-01.png" style={isPast ? styles.disabledIcon : activeTab === 'event' ? styles.activeIcon : styles.icon}/>
          {localizations.profileView_tab_event}
        </div>
        <div style={!haveChat ? styles.disabledTab : activeTab === 'chat' ? styles.activeTab : hoverTab === 'chat' ? styles.tabHover : styles.tab} key={"chat"} onClick={() => haveChat && this.props.changeActiveTab('chat')} onMouseEnter={() => this.onMouseEnter('chat')} onMouseLeave={this.onMouseLeave}>
          <img src="/assets/images/comment_bubble.png" style={isPast || !haveChat ? styles.disabledIcon : activeTab === 'chat' ? styles.activeIcon : styles.icon}/>
          {localizations.profileView_tab_chat}
        </div>
        <div style={activeTab === 'statistics' ? styles.activeTab : hoverTab === 'statistics' ? styles.tabHover : styles.tab} key={"statistics"} onClick={() => this.props.changeActiveTab('statistics')} onMouseEnter={() => this.onMouseEnter('statistics')} onMouseLeave={this.onMouseLeave}>
          <img src="/assets/images/statistic_bleu-01.png" style={activeTab === 'statistics' ? styles.activeIcon : styles.icon}/>
          {localizations.profileView_tab_statistics}
        </div>
      </div>
    )

  }
}

styles = {
  container: {
    minHeight: 60,
    backgroundColor: colors.lightGray,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: '-2px',
    flexWrap: 'wrap',
  },
  tab: {
    padding: '5px 10px',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    height: 60,
    cursor: 'pointer',
    fontSize: 16,
    fontFamily: 'Lato',
    transition: 'all cubic-bezier(0.22,0.61,0.36,1) .3s',
    fontWeight: 'bold',
    color: colors.darkGray,
  },
  tabHover: {
    padding: '5px 10px',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    height: 60,
    cursor: 'pointer',
    fontSize: 16,
    fontFamily: 'Lato',
    transition: 'all cubic-bezier(0.22,0.61,0.36,1) .3s',
    fontWeight: 'bold',
    backgroundColor: '#c9c9c9',
    color: colors.white,
  },
  disabledTab: {
    padding: '5px 10px',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    height: 60,
    cursor: 'pointer',
    fontSize: 16,
    fontFamily: 'Lato',
    backgroundColor: 'rgb(225, 225, 225)',
    color: colors.gray,
    fontWeight: 'bold',
  },
  activeTab: {
    padding: '5px 10px',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.blue,
    flex: 1,
    height: 60,
    cursor: 'pointer',
    fontSize: 16,
    color: colors.white,
    fontFamily: 'Lato',
    fontWeight: 'bold',
  },
  icon: {
    width: 30,
    marginRight: 10
  },
  disabledIcon: {
    filter: 'grayscale(2)',
    width: 30,
    marginRight: 10
  },
  activeIcon: {
    filter: 'brightness(3)',
    width: 30,
    marginRight: 10
  }
};

export default Radium(TabMenu);