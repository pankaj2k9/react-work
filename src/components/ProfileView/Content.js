import React from 'react';
import PureComponent, { pure } from '../common/PureComponent'
import Radium from 'radium';
import Relay from 'react-relay';
import AlertContainer from 'react-alert'
import Modal from 'react-modal'

import RelayStore from '../../RelayStore.js'
import localizations from '../Localizations'
import { colors, fonts, metrics } from '../../theme'

import Sports from './Sports';
import Feedbacks from './Feedbacks';

import BlockMutation from './BlockMutation'
import ReportMutation from './ReportMutation'
import {Link} from "react-router";

let styles, modalStyles

const capitalize = s => s[0].toUpperCase() + s.substr(1);

const listLanguages = (langs) => {
  if (!Array.isArray(langs) || langs.length === 0) {
    return '';
  }

  if (langs.length === 1) {
    return capitalize(langs[0]);
  }

  if (langs.length === 2) {
    return langs.map(capitalize).join(' '+localizations.profile_and+' ');
  }
  return langs.slice(0, langs.length-1).map(capitalize).join(', ')+' '+localizations.profile_and+' '+capitalize(langs[langs.length-1]);

};

class Content extends PureComponent {
  constructor(props) {
    super(props)
    this.alertOptions = {
      offset: 60,
      position: 'top right',
      theme: 'light',
      transition: 'fade',
    };
    this.state = {
      blockModalIsOpen: false,
      reportModalIsOpen: false,
      reportMessage: '',
      loadMoreQueryIsLoading: false,
      pageSize: 10,
			itemCount: 2,
    }
  }

  _reportMessageChanged = (e) => {
    this.setState({
      reportMessage: e.target.value,
    })
  }

  _reportUser = () => {
    const viewer = this.props.viewer
    const userIDVar = this.props.user.id
    const reasonVar = this.state.reportMessage

    RelayStore.commitUpdate(
      new ReportMutation({
        viewer,
        userIDVar,
        reasonVar,
      }),
      {
        onFailure: error => {
          let errors = JSON.parse(error.getError().source);
          console.log(errors);
        },
        onSuccess: () => {
          this.msg.show(localizations.popup_profileView_report_user_success, {
              time: 2000,
              type: 'success',
            });
          this.setState({
            reportModalIsOpen: false,
            reportMessage: '',
          })
        },
      }
    );
  }

  _blockUser = () => {
    const blackListVar = this.props.me.blackList.map(bl => bl.id)
    blackListVar.push(this.props.user.id)
    console.log(blackListVar)
    const viewer = this.props.viewer
    const meId = this.props.me.id

    RelayStore.commitUpdate(
      new BlockMutation({
        viewer,
        meId,
        blackListVar,
      }),
      {
        onFailure: error => {
          let errors = JSON.parse(error.getError().source);
          console.log(errors);
        },
        onSuccess: () => {
          this.msg.show(localizations.popup_profileView_block_user_success, {
              time: 2000,
              type: 'success',
            });
          this.setState({
            blockModalIsOpen: false,
          })
        },
      }
    );
  }

  _handleReport = () => {
    this.setState({
      reportModalIsOpen: true,
      reportMessage: '',
    })
  }

  _handleBlock = () => {
    this.setState({
      blockModalIsOpen: true,
    })
  }

  _closeBlockModal = () => {
    this.setState({
      blockModalIsOpen: false,
    })
  }

  _closeReportModal = () => {
    this.setState({
      reportModalIsOpen: false,
      reportMessage: '',
    })
  }

  _getAge = (birthday) => {
    var today = new Date();
    var birthDate = new Date(birthday);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
  }

  _loadMoreQueryIsLoaded = () => {
		if (this.props.relay.variables.first === this.state.itemCount) {
			this.setState({
				loadMoreQueryIsLoading: false,
			})
		} else {
			setTimeout(this._loadMoreQueryIsLoaded, 200)
		}
	}

	_loadMore = () => {
    const nextCount = this.state.itemCount + this.state.pageSize
    this.props.relay.setVariables({ first: nextCount }, this._loadMoreQueryIsLoaded)
    this.setState({
      itemCount: nextCount,
			loadMoreQueryIsLoading: true,
    })
  }

  render() {
    const { user, me, isSelfProfile, loadMoreQueryIsLoading } = this.props;
    const sexOptions = [
      { value: '', label: '' },
      { value: 'MALE', label: localizations.profile_sex_male },
      { value: 'FEMALE', label: localizations.profile_sex_female },
      { value: 'OTHER', label: localizations.profile_sex_other },
    ];

    return(
      <div style={styles.content}>
        <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
        <div style={styles.modifyContainer}>
          {/*<div style={styles.followers}>*/}
            {/*<span style={{fontWeight: 'bold'}}>{user.followers.length}</span>*/}
            {/*{this.state.isFollowing*/}
              {/*? <i onClick={this.confirmFollowUser} style={styles.followerIcon} className="fa fa-heart fa-2x" />*/}
              {/*: <i onClick={this.confirmFollowUser} style={styles.followerIcon} className="fa fa-heart-o fa-2x" />*/}
            {/*}*/}
          {/*</div>*/}
          {isSelfProfile &&
          <Link to='/profile'>
            <button
              style={styles.modifyProfileButton}
            >
              {localizations.me_editProfile}
            </button>
          </Link>
          }
        </div>
        <h2 style={styles.title}>{localizations.profileView_description}</h2>
        <div style={styles.description}>
          <p style={styles.descriptionLine}>
            {user.description || <div style={styles.sport_no_data}>{user.pseudo}{localizations.profileView_no_description}</div>}
          </p>
          {user.profileType !== 'ORGANIZATION' && user.birthday &&
            <p style={styles.descriptionLine}>
              {localizations.profile_age + ': ' + this._getAge(user.birthday) + ' ' + localizations.profile_yearsOld}
            </p>
          }
          {user.profileType !== 'ORGANIZATION' && user.sex &&
            <p style={styles.descriptionLine}>
              {localizations.profile_gender + ': ' + sexOptions.find(sexOption => user.sex === sexOption.value ).label}
            </p>
          }
        </div>
        {
          user.languages.length > 0 &&
            <div>
              <h2 style={styles.title}>{localizations.profileView_language}</h2>
              <p style={styles.languages}>
                {user.pseudo} {localizations.profileView_speaks} {listLanguages(user.languages.map(l => l.name))}.
              </p>
            </div>
        }
        <Sports sports={user.sports} {...this.props} />
        <Feedbacks user={user} {...this.props}/>
        {!isSelfProfile && me && me.id &&
          <div style={styles.blockRow}>
            <a style={styles.blockLink} onClick={this._handleReport}>{localizations.profileView_report} </a>
            -
            <a style={styles.blockLink} onClick={this._handleBlock}> {localizations.profileView_block}</a>
          </div>
        }
        <Modal
            isOpen={this.state.blockModalIsOpen}
            onRequestClose={this._closeBlockModal}
            style={modalStyles}
            contentLabel="Block User"
          >
            <div style={styles.modalContent}>
              {localizations.profileView_block_confirmation}
              <div style={styles.modalButtonRow}>
                <button style={styles.submitButton}
                            onClick={this._blockUser}>{localizations.profileView_report_confirmation_yes}</button>&nbsp;&nbsp;&nbsp;&nbsp;
                <button style={styles.cancelButton}
                            onClick={this._closeBlockModal}>{localizations.profileView_report_confirmation_cancel}</button>
              </div>
            </div>

        </Modal>
        <Modal
            isOpen={this.state.reportModalIsOpen}
            onRequestClose={this._closeReportModal}
            style={modalStyles}
            contentLabel="Report User"
          >
            <div style={styles.modalContent}>
              {localizations.profileView_report_reasons_prompt}
              <textarea style={styles.reportInput}
                    onChange={this._reportMessageChanged}
                    value={this.state.reportMessage}
              ></textarea>
              <div style={styles.modalButtonRow}>
                <button style={styles.submitButton}
                            onClick={this._reportUser}>{localizations.profileView_report_confirmation_yes}</button>&nbsp;&nbsp;&nbsp;&nbsp;
                <button style={styles.cancelButton}
                            onClick={this._closeReportModal}>{localizations.profileView_report_confirmation_cancel}</button>
              </div>
            </div>

        </Modal>
      </div>
    )
  }
}

styles = {
  modifyContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
  modifyProfileButton: {
    backgroundColor: colors.green,
    color: colors.white,
    width: 180,
    height: 57,
    borderRadius: 100,
    borderStyle: 'none',
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
    fontSize: 22,
    cursor: 'pointer',
    marginTop: 15
  },
  sport_no_data: {
    fontFamily: 'Lato',
    fontSize: 16,
    margin: 30,
  },
  reportInput: {
    minHeight: '100px',
    borderTop: '1px solid rgba(0,0,0,0.2)',
    borderRight: '1px solid rgba(0,0,0,0.2)',
    borderLeft: '1px solid rgba(0,0,0,0.2)',
    borderColor: 'transparent',
    background: 'rgba(255,255,255,.5)',
    borderBottom: '2px solid '+colors.blue,
    fontSize: fonts.size.medium,
    outline: 'none',
    resize: 'none',
    fontFamily: 'Lato',
    marginTop: 15,
    color: colors.black,
  },
  modalButtonRow: {
    display:'flex',
    alignSelf: 'center',
    marginTop: 30,
  },
  block: {
    backgroundColor: colors.error,
    color: colors.white,
    width: 220,
    height: 57,
    borderRadius: 100,
    borderStyle: 'none',
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
    fontSize: 22,
    cursor: 'pointer',
    ':disabled': {
      cursor: 'not-allowed',
    },
    margin: 'auto',
  },
  content: {
    flexGrow: '1',
    padding: '40px',
    fontFamily: 'Lato',
    color: 'rgba(0,0,0,0.65)',
    '@media (max-width: 483px)': {
      padding: '40px 20px'
    }
  },

  title: {
    fontSize: 32,
    fontWeight: 500,
    marginBottom: 30,
  },

  description: {
    fontSize: 18,
    lineHeight: 1.2,
    marginBottom: 35,
    wordBreak: 'break-word'
  },
  descriptionLine: {
    wordBreak: 'break-word',
    marginBottom: 15
  },

  languages: {
    fontSize: 18,
    lineHeight: 1,
    marginBottom: 40,
  },
  blockRow: {
    width: '100%',
    textAlign: 'Right',
    fontSize: 16,
  },
  blockLink: {
    fontSize: 16,
    color: colors.black,
    textDecoration: 'none',
    cursor: 'pointer',
  },
  chatButtonContainer: {
    margin: '35px auto',
    textAlign: 'center'
  },
  chatButton: {
    textDecoration: 'none',
    backgroundColor: colors.green,
    color: colors.white,
    borderRadius: 100,
    borderStyle: 'none',
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
    fontSize: 22,
    cursor: 'pointer',
    padding: '10px 25px',
    maxWidth: 350,
    margin: 'auto'
  },
  chatBox: {
    marginBottom: 35
  },
  submitButton: {
		width: 80,
		backgroundColor: colors.blue,
    color: colors.white,
    fontSize: fonts.size.small,
    borderRadius: metrics.radius.tiny,
    outline: 'none',
		border: 'none',
		padding: '10px',
		cursor: 'pointer',
		boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
	},
  cancelButton: {
		width: 80,
		backgroundColor: colors.gray,
    color: colors.white,
    fontSize: fonts.size.small,
    borderRadius: metrics.radius.tiny,
    outline: 'none',
		border: 'none',
		padding: '10px',
		cursor: 'pointer',
		boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
	},
  modalContent: {
		display: 'flex',
		flexDirection: 'column',
    justifyContent: 'flex-start',
    '@media (max-width: 480px)': {
      width: '300px',
    },
    fontFamily: 'Lato',
    fontSize: 16,
    color: colors.black,
    margin: 20,
	},
	modalHeader: {
		display: 'flex',
		flexDirection: 'row',
    alignItems: 'flex-center',
		justifyContent: 'flex-center',
	},
	modalTitle: {
		fontFamily: 'Lato',
		fontSize:30,
		fontWeight: fonts.weight.medium,
		color: colors.blue,
		marginBottom: 20,
		flex: '2 0 0',
	},
	modalClose: {
		justifyContent: 'flex-center',
		paddingTop: 10,
		color: colors.gray,
		cursor: 'pointer',
	},
  errorFeedback: {
    fontFamily: 'Lato',
    color: colors.red,
    fontSize: 16,
  },
}

modalStyles = {
  overlay : {
    position          : 'fixed',
    top               : 0,
    left              : 0,
    right             : 0,
    bottom            : 0,
    backgroundColor   : 'rgba(255, 255, 255, 0.75)',
  },
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)',
    border                     : '1px solid #ccc',
    background                 : '#fff',
    overflow                   : 'auto',
    WebkitOverflowScrolling    : 'touch',
    borderRadius               : '4px',
    outline                    : 'none',
    padding                    : '20px',
  },
}

export default Relay.createContainer(Radium(Content), {
  initialVariables: {
    first: 2,
  },

  fragments: {
    user: () => Relay.QL`
      fragment on User {
        id
        profileType
        sex,
        birthday,
        avatar
        pseudo
        description
        languages {
          id
          code
          name
        }
        sports{
          ${Sports.getFragment('sports')},
        }
        ${Feedbacks.getFragment('user')}
      }
    `,
    me: () => Relay.QL`
      fragment on User {
        id
        blackList {
          id
        }
      }
    `,
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
        me {
          id,
          avatar
        }
      }
    `,
  },
});
