import Relay from 'react-relay';

export default class FollowMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      upUser
    }`;
  }

  getVariables() {
    return {
      userID: this.props.userId,
      user: {
        followers: this.props.meId,
      },
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on upUserPayload @relay(pattern: true){
        clientMutationId,
        user,
        viewer
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
        fieldIDs: {
          viewer: this.props.viewer.id,
        },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
      }
    `,
  };
}
