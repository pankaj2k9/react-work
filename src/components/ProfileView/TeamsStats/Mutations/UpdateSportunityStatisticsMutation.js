import Relay from 'react-relay';

export default class UpdateSportunityStatisticsMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      updateSportunityStatistic
    }`
  }
  
  getVariables() {
    return  {
      sportunityID: this.props.sportunityIDVar,
      sportunityStatistics: this.props.sportunityStatisticsVar,
    }
  }

  getFatQuery() {
      return Relay.QL`
        fragment on updateSportunityStatisticsPayload {
          clientMutationId,
          viewer {
            id,
            sportunityStatistics
          }
        }
      `
   
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
        fieldIDs: {
          viewer: this.props.viewer.id,
        },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
      }
    `,
  };

}
