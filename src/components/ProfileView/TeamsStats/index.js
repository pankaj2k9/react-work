import React from 'react';
import {Link} from 'react-router'
import Relay from 'react-relay';
import Radium from 'radium';
import ReactLoading from 'react-loading';
import Select from 'react-select';
import RelayStore from '../../../RelayStore'

import localizations from '../../Localizations'
import {colors} from '../../../theme';
import styles from './styles.js';
import Circle from './Circle';
import DatePicker from 'react-datepicker'
import moment from "moment/moment";
import RemoveFilterMutation from "./RemoveFilterMutation";
import AlertContainer from "react-alert";
import SelectFilter from "./SelectFilter";
import NewFilterMutation from "./NewFilterMutation";
import FilterModal from "./FilterModal"
import UpdateStatisticPreferencesMutation
	from "../../MyInfo/MyStatisticPreferences/Mutations/UpdateStatisticPreferencesMutation";
import CustomizeStatModal from './CustomizeStatModal'
import FillStatsModal from "./FillStatsModal";


const RLink = Radium(Link);

var Style = Radium.Style;

let RSelect = Radium(Select);

class TeamsStats extends React.Component {

    constructor() {
        super();
        this.alertOptions = {
            offset: 60,
            position: 'top right',
            theme: 'light',
            transition: 'fade',
        };
        this.state = {
            userStatistics: [],
            userStatisticsCols: [],
            isProcessing: false,
            isFilterProcessing: false,
            isCircleListOpen: false,
            selectedCircles: [],
            selectedFilter: null,
            date: null,
            nameFilter: null,
            openModal: false,
	          statisticIsChanging: false,
	          statisticIsFilling: false,
        }
    }

    componentDidMount() {
        // window.addEventListener('click', this._handleClickOutside);
      console.log('userId', this.props.userId)
        if (this.props.userId) {
            this.props.relay.setVariables({
                id: this.props.userId,
                query: true
            })
            if (this.props.user && this.props.user.circles && this.props.user.circles.edges && this.props.user.circles.edges.length > 0) {
                this.setState({
                    isProcessing: true
                })
              let circleList = this.props.user.circles.edges.map(circle => ({label: circle.node.name,value: circle}))
              this._changeCircles(circleList);
            }
        }
        setTimeout(() => {
          this._applyFilter();
        }, 150);
    }

    componentWillUnmount() {
        // window.removeEventListener('click', this._handleClickOutside);
    }

    _handleClickOutside = event => {
        if (!this._containerNode.contains(event.target)) {
        this.setState({ isCircleListOpen: false });
        }
    }

    componentWillReceiveProps = (nextProps) => {
      if (nextProps.user && nextProps.user.circles && nextProps.user.circles.edges && this.state.selectedCircles === []) {
        let circleList = nextProps.user.circles.edges.map(circle => ({label: circle.node.name,value: circle}))

        this._changeCircles(circleList);
        // this._changeCircle(circleList[0])
      }


      // if (nextProps.viewer.circle && nextProps.viewer.circle.id === this.state.selectedCircles.id) {
      let userStatisticsCols = this._getParticipantsStatsCols(nextProps.viewer.statisticPreferences);
      let userStatistics = this._getParticipantsStats(userStatisticsCols, nextProps.viewer.circlesStatistics);
      let opponentColStatistics = this. _getOpponantsColStats(nextProps.viewer.sportunitiesStatistics);
      if (nextProps.viewer.statisticPreferences.isManOfTheGameActivated) {
	      let nbManOfTheMatch = 0;
	      userStatistics.forEach(stat => {
		      stat.values.forEach(value => {
			      if (nextProps.viewer.statisticPreferences.userStats.statManOfTheGame && value.id === nextProps.viewer.statisticPreferences.userStats.statManOfTheGame.id)
				      nbManOfTheMatch += value.value
		      })
	      });
	      if (nextProps.viewer.statisticPreferences.userStats.statManOfTheGame && nbManOfTheMatch === 0) {
		      userStatisticsCols = userStatisticsCols.filter(col => col.id !== nextProps.viewer.statisticPreferences.userStats.statManOfTheGame.id)
		      userStatistics = userStatistics.map(stat => ({
			      participant: stat.participant,
			      values: stat.values.filter(value => value.id !== nextProps.viewer.statisticPreferences.userStats.statManOfTheGame.id)
		      }))
        }        
      }
      this.setState({
        userStatistics,
        userStatisticsCols,
        userBestStatistics : this._getParticipantBetterStats(userStatisticsCols, nextProps.viewer.circlesStatistics),
        sportunitiesStatistics: this._getSportunitiesStats(nextProps.viewer.sportunitiesStatistics),
        opponentColStatistics,
        opponentStatistics: this._getOpponentsStats(nextProps.viewer.sportunitiesStatistics, opponentColStatistics),
	      isFilterProcessing: false
      })
      setTimeout(() => {
        console.log(this.state);
        this.sortDown(0);
        this.setState({
          isProcessing: false
        })
      }, 150)
        // }
    }

    _getParticipantBetterStats = (circlesStatisticsCols, circlesStatistics) => {
        let result = [];
        if (circlesStatisticsCols && circlesStatisticsCols.length > 0) {
            circlesStatisticsCols.forEach((statCol, index) => {
                result[index] = {participant: null, value: 0};
                circlesStatistics.forEach((stat) => {
                    if (stat.statisticName.id === statCol.id && stat.value >= result[index].value)
                        result[index] = {participant: stat.participant, value: stat.value}
                })
            })
        }
        return result;
    };

    _getParticipantsStatsCols = (statisticPreferences) => {
        let results = [];
        if (statisticPreferences && statisticPreferences.userStats) {
            Object.keys(statisticPreferences.userStats).forEach(stat => {
                if (statisticPreferences.userStats[stat] && statisticPreferences.userStats[stat].id)
                    results.push(statisticPreferences.userStats[stat])
            })
        }
        return results ;
    }

    _getParticipantsStats = (userStatisticsCols, circlesStatistics) => {
        let results = [];

        if (circlesStatistics && circlesStatistics.length > 0) {
            circlesStatistics.forEach(stat => {
                if (stat.participant) {
                    let index = results.findIndex(result => result.participant && result.participant.id === stat.participant.id);
                    let colIndex = userStatisticsCols.findIndex(result => result.id === stat.statisticName.id);

                    if (index < 0) {
                        results.push({participant: stat.participant, values:[]})
                        results[results.length - 1].values[colIndex] = {id: stat.statisticName.id , value:stat.value};
                    }
                    else {
                        results[index].values[colIndex] = {id: stat.statisticName.id , value:stat.value}
                    }
                }
            })
        }
        return results ;
    }

  _changeCircles = (circles) => {
    const tmpCircles = circles ? circles.map(circle => (circle.value ? circle.value : circle)) : [];
    this.setState({
      selectedCircles: tmpCircles.map(circle => ({
        id: (circle.node ? circle.node.id : circle.id),
        name : (circle.node ? circle.node.name : circle.name)
      })),
      isCircleListOpen: false
    })
  }

  // _changeCircle = (circle) => {
  //   this.props.relay.setVariables({
  //     circleId: circle.node.id,
  //     query: true
  //   })
  //   this.setState({
  //     selectedCircles: {
  //       id: circle.node.id,
  //       name : circle.node.name
  //     },
  //     isProcessing: true,
  //     isCircleListOpen: false
  //   })
  // }


    onClose = () => {
        this.props.relay.setVariables({
            id: null,
            query: false,
        })
        this.props.onLeave();
    }

    sortUp = (colIndex) => {
        let userStats = this.state.userStatistics ;

        userStats = userStats.sort((a,b) => {
            if (a.values.length > 0 && a.values[colIndex].value - b.values[colIndex].value > 0)
                return 1;
            else if (a.values.length > 0 && a.values[colIndex].value - b.values[colIndex].value < 0)
                return -1
            else return 0;
        })
        this.setState({
            userStatistics: userStats
        })
    }

    sortDown = (colIndex) => {
        let userStats = this.state.userStatistics ;

        userStats = userStats.sort((a,b) => {
            if (b.values.length > 0 && b.values[colIndex].value - a.values[colIndex].value > 0)
                return 1;
            else if (b.values.length > 0 && b.values[colIndex].value - a.values[colIndex].value < 0)
                return -1
            else return 0;
        })
        this.setState({
            userStatistics: userStats
        })
    }

  _getOpponentsStats = (sportunitiesStatistics, opponentsColStats) => {
    let results = [];
    if (sportunitiesStatistics && sportunitiesStatistics.length > 0) {

      sportunitiesStatistics.forEach(stat => {
        let colIndex = opponentsColStats.findIndex(col => !!stat.sportunityTypeStatus && col.id === stat.sportunityTypeStatus.id)
        
        stat.details.forEach(detail => {
          if (detail.opponent) {
            let index = results.findIndex(result => result.opponent && result.opponent.id === detail.opponent.id)
            if (index < 0) {
              results.push({
                opponent: detail.opponent,
                values: opponentsColStats.map(i => 0)
              })
              index = results.length - 1; 
            }
            if (colIndex >= 0) {
              results[index].values[colIndex] = results[index].values[colIndex] + detail.value;
              results[index].values[results[index].values.length - 1] = results[index].values[results[index].values.length - 1] + detail.value
            }
            else {
              results[index].values[results[index].values.length - 2] = results[index].values[results[index].values.length - 2] + detail.value;
              results[index].values[results[index].values.length - 1] = results[index].values[results[index].values.length - 1] + detail.value
            }
          }
          else {
            let index = results.findIndex(result => result.opponent.pseudo === localizations.profile_statistics_not_completed)
            if (index < 0) {
              results.push({
                opponent: {pseudo: localizations.profile_statistics_not_completed},
                values: opponentsColStats.map(i => 0)
              })
              index = results.length - 1; 
            }
            if (colIndex >= 0) {
              results[index].values[colIndex] = results[index].values[colIndex] + detail.value;
              results[index].values[results[index].values.length - 1] = results[index].values[results[index].values.length - 1] + detail.value
            }
            else {
              results[index].values[results[index].values.length - 2] = results[index].values[results[index].values.length - 2] + detail.value;
              results[index].values[results[index].values.length - 1] = results[index].values[results[index].values.length - 1] + detail.value
            }
          }
        })
      });
    }

    return results
  };

  _getOpponantsColStats = (sportunitiesStatistics) => {
    let results = [];

    if (sportunitiesStatistics && sportunitiesStatistics.length > 0) {
      sportunitiesStatistics.forEach(stat => {
        if (results.findIndex(result => result.id === stat.sportunityType.id) < 0)
          results.push({
            id: stat.sportunityType.id,
            name: stat.sportunityType.name[localizations.getLanguage().toUpperCase()],
            isType: true
          })
        if (results.findIndex(result => !!stat.sportunityTypeStatus && result.id === stat.sportunityTypeStatus.id) < 0)
          results.push({
            id: !!stat.sportunityTypeStatus ? stat.sportunityTypeStatus.id : null,
            name: !!stat.sportunityTypeStatus ? stat.sportunityTypeStatus.name[localizations.getLanguage().toUpperCase()] : localizations.profile_statistics_not_completed,
            isType: !!stat.sportunityTypeStatus ? false : true
          })
      })
    }
    results.sort((a, b) => a.isType + b.isType);
    return results ;
  };

  _getSportunitiesStats = (sportunitiesStatistics) => {
    let results = [];
    let totalNumber = 0;

    if (sportunitiesStatistics && sportunitiesStatistics.length > 0) {
      sportunitiesStatistics.forEach(stat => {
        if (stat.sportunityTypeStatus) {
          results.splice(0,0, {
            sportunityType: stat.sportunityType.name[localizations.getLanguage().toUpperCase()],
            sportunityTypeStatus: !!stat.sportunityTypeStatus ? stat.sportunityTypeStatus.name[localizations.getLanguage().toUpperCase()] : null,
            value: stat.value
          })
        }
        else {
          results.push({
            sportunityType: stat.sportunityType.name[localizations.getLanguage().toUpperCase()],
            sportunityTypeStatus: localizations.profile_statistics_not_completed,
            value: stat.value
          })
        }
        totalNumber += stat.value;
      })
    }
    if (sportunitiesStatistics && sportunitiesStatistics.length > 0)
      results.splice(0,0, {
        sportunityType: sportunitiesStatistics[0].sportunityType.name[localizations.getLanguage().toUpperCase()],
        value: totalNumber
      });
    
    return results ;
  }

  _removeFilter = (item) => {
    RelayStore.commitUpdate(
      new RemoveFilterMutation({
        userId: this.props.userId,
        filterId: item.id
      }), {
        onFailure: error => {
          this.msg.show(error.getError().source.errors[0].message, {
            time: 2000,
            type: 'error',
          });
	        setTimeout(() => this.msg.removeAll(), 2000);
        },
        onSuccess: (response) => {
          this.msg.show(localizations.popup_editCircle_update_success, {
            time: 2000,
            type: 'success',
          });
	        setTimeout(() => this.msg.removeAll(), 2000);
        },
      }
    )
  };

  _changeFilter = (item) => {
    this.setState({
      selectedFilter: item,
    });
    if (item.date_begin !== null || item.date_end !== null) {
      this.setState({
        date: {
          from: item.date_begin ? moment(item.date_begin) : moment(),
          to: item.date_end ? moment(item.date_end): moment()
        }
      })
    }
    this._changeCircles(item.circleList.edges)
    this.msg.show('Le filtrer à été selectionnéer, Appuyer sur le bouton Appliqué', {
      time: 2000,
      type: 'success',
    });
    setTimeout(() => this.msg.removeAll(), 2000);
  };

  _changeDate = (from, to) => {
    this.setState({
      date: {
        from,
        to
      }
    })
  }

  _saveFilter = () => {
    // if (!this.state.selectedFilter) {
      this._toggleModal()
      // this._newFilter();
    // }
    // else
    //   RelayStore.commitUpdate(
    //     new UpdateFilterMutation({
    //       userId: this.props.userId,
    //       name: this.state.selectedFilter.name,
    //       from: this.state.date ? this.state.date.from : null,
    //       to: this.state.date ? this.state.date.to : null,
    //       filterId: this.state.selectedFilter.id,
    //       circleList: this.state.selectedCircles
    //     }), {
    //       onFailure: error => {
    //         this.msg.show(error.getError().source.errors[0].message, {
    //           time: 2000,
    //           type: 'error',
    //         });
    //
    //       },
    //       onSuccess: (response) => {
    //         this.msg.show(localizations.popup_editCircle_update_success, {
    //           time: 2000,
    //           type: 'success',
    //         });
    //       },
    //     })
  };

  _newFilter = () => {
    let name = this.state.nameFilter /*? this.state.nameFilter : prompt(localizations.profile_statistics_filter_name_title)*/
    if (name !== null)
      RelayStore.commitUpdate(
        new NewFilterMutation({
          userId: this.props.userId,
          name: this.state.nameFilter,
          from: this.state.date ? this.state.date.from : null,
          to: this.state.date ? this.state.date.to : null,
          circleList: this.state.selectedCircles.map(id => id.id)
        }), {
          onFailure: error => {
            this.msg.show(error.getError().source.errors[0].message, {
              time: 2000,
              type: 'error',
            });
	          setTimeout(() => this.msg.removeAll(), 2000);

          },
          onSuccess: (response) => {
            this.msg.show(localizations.popup_editCircle_update_success, {
              time: 2000,
              type: 'success',
            });
	          setTimeout(() => this.msg.removeAll(), 2000);
          },
        })
  };

  _changeFilterName = (name) => {
    this.setState({
      nameFilter: name
    })
  };

  _applyFilter = () => {
	  this.setState({
      isFilterProcessing: true,
		  isProcessing: true
    })
    console.log("hereeer" , this.state);
    this.props.relay.setVariables({
      dateInterval: {
        from : this.state.date && this.state.date.from ? this.state.date.from._d : moment().subtract(1, 'years')._d,
        to : this.state.date && this.state.date.to ? this.state.date.to._d : moment()._d,
      },
      circleIds: this.state.selectedCircles.map(id => id.id),
      query: true
    },)
  };

  _toggleModal = () => {
    this.setState({
      openModal: !this.state.openModal,
    });
  }

	_handleUpdateUserPreferences = (userStatPrefs) => {
		let params = {
			userIDVar: this.props.user.id,
			userStatsVar: userStatPrefs,
			viewer: this.props.viewer,
		} ;
		this.props.relay.commitUpdate(
			new UpdateStatisticPreferencesMutation(params),
			{
				onSuccess: () => {
					this.msg.show(localizations.myStatPrefs_update_success, {
						time: 2000,
						type: 'success',
					});
					setTimeout(() => this.msg.removeAll(), 2000);
				},
				onFailure: (error) => {
					this.msg.show(localizations.myStatPrefs_update_failed, {
						time: 2000,
						type: 'error',
					});
					setTimeout(() => this.msg.removeAll(), 2000);
					console.log(error);
				},
			}
		);
	};

	toggleCustomizeStat = () => {
	  this.setState({
		  statisticIsChanging: !this.state.statisticIsChanging
    })
  };

	toggleFillStat = () => {
	  this.setState({
		  statisticIsFilling: !this.state.statisticIsFilling
    })
  };

	openCustomizeStat = () => {
	  this.toggleCustomizeStat()
  };

	openFillStat = () => {
	  this.toggleFillStat()
  }

  removeFilter = () => {
	  this.setState({
      selectedFilter: null
    })
  }
	
    render() {
        let {viewer, user} = this.props;
console.log(this.props);
        const currentUserIsOwner = viewer.me && viewer.me.id === user.id;

        const {sportunitiesStatistics, opponentColStatistics, opponentStatistics, userStatisticsCols, userStatistics, userBestStatistics} = this.state;

        let circles = (user && user.circles && user.circles.edges) ? user.circles.edges
          .filter(circle => circle.node.type === 'ADULTS' || circle.node.type === 'CHILDREN')
          .map(circle => ({label: circle.node.name,value: circle})): [];

        return (

          <div>
	          <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
            {this.state.statisticIsChanging &&
              <CustomizeStatModal
                isOpen={true}
                user={user}
                viewer={viewer}
                statisticPreferences={viewer.statisticPreferences}
                canCloseModal
                toggleModal={this.toggleCustomizeStat}
                _handleUpdatePreferences={this._handleUpdateUserPreferences}
              />
            }
            {this.state.statisticIsFilling &&
              <FillStatsModal
                isOpen={true}
                user={user}
                viewer={viewer}
                canCloseModal
                toggleModal={this.toggleFillStat}
              />
            }
            <Style scopeSelector=".react-datepicker__input-container" rules={{
              "input": styles.date
            }}
            />
            <Style scopeSelector=".react-datepicker" rules={{
              top: -20,
              "div": {fontSize: '1.4rem'},
              ".react-datepicker__current-month": {fontSize: '1.5rem'},
              ".react-datepicker__month": {margin: '1rem'},
              ".react-datepicker__day": {width: '2rem', lineHeight: '2rem', fontSize: '1.4rem', margin: '0.2rem'},
              ".react-datepicker__day-names": {width: '100%', display: 'flex', flexDirection: 'row', justifyContent: 'space-between', marginTop: 5},
              ".react-datepicker__header": {padding: '1rem', display: 'flex', flexDirection: 'column',alignItems: 'center'}
            }}
            />
            <div style={styles.filterContainer}>
              <FilterModal
                isOpen={this.state.openModal}
                canCloseModal
                title={localizations.profile_statistics_filter_name_title}
                name={this.state.nameFilter}
                updateName={this._changeFilterName}
                onConfirm={this._newFilter}
                toggleModal={this._toggleModal}
                confirmLabel={localizations.profile_statistics_filter_save}
              />
              <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
              <div style={styles.filter}>
                <SelectFilter
                  label={localizations.profile_statistics_filter_label}
                  placeholder={localizations.profile_statistics_filter_placeholder}
                  selectedItem={this.state.selectedFilter}
                  onRemove={this._removeFilter}
                  list={user ? user.statisticFilters : []}
                  onSelectItem={this._changeFilter}
                  allowChange={viewer.me && viewer.me.id === user.id}
                />
              </div>
              <div style={styles.dateContainer}>
                <div style={styles.dateTitle}>
                  {localizations.profile_statistics_dateFrom}
                </div>
                <DatePicker
                  dateFormat="DD/MM/YYYY"
                  todayButton={localizations.newSportunity_today}
                  selected={((this.state.date && this.state.date.from !== null) ? this.state.date.from : moment().subtract(1, 'years'))}
                  maxDate={(this.state.date ? this.state.date.to : null)}
                  onChange={(moment) => {this._changeDate(moment, (this.state.date ? this.state.date.to : null)); this.removeFilter()}}
                  locale={localizations.getLanguage().toLowerCase()}
                  popperPlacement="top-end"
                  style={styles.date}
                />
              </div>
              <div style={styles.dateContainer}>
                <div style={styles.dateTitle}>
                  {localizations.profile_statistics_dateTo}
                </div>
                <DatePicker
                  dateFormat="DD/MM/YYYY"
                  todayButton={localizations.newSportunity_today}
                  selected={((this.state.date && this.state.date.to !== null) ? this.state.date.to : moment())}
                  minDate={(this.state.date ? this.state.date.from : null)}
                  onChange={(moment) => {this._changeDate((this.state.date ? this.state.date.from : null), moment); this.removeFilter()}}
                  locale={localizations.getLanguage().toLowerCase()}
                  popperPlacement="top-end"
                  style={styles.date}
                />
              </div>
              <div style={styles.circles}>
                <div style={styles.circleTitle}>
                  {localizations.profile_statistics_circles}
                </div>
                <RSelect
                  closeOnSelect={true}
                  multi
                  removeSelected={true}
                  onChange={(item) => {this._changeCircles(item); this.removeFilter()}}
                  options={circles}
                  placeholder={localizations.profile_statistics_select_circle}
                  value={this.state.selectedCircles ? this.state.selectedCircles.map(circle => ({label: circle.name, value: circle})) : null}
                  style={styles.listContainer}
                />
              </div>
              {!this.state.isFilterProcessing &&
              <div onClick={() => {
                this._applyFilter()
              }} style={{...styles.saveButton, backgroundColor: colors.green}}>
                {localizations.profile_statistics_filter_apply}
              </div>
              }
              {!this.state.isFilterProcessing && !this.state.selectedFilter && viewer.me && viewer.me.id === user.id &&
              <div onClick={() => {this._saveFilter()}} style={styles.saveButton}>
                {localizations.profile_statistics_filter_save}
              </div>
              }
            </div>
	          {user.areStatisticsActivated && userStatistics && userStatistics.length > 0 && currentUserIsOwner &&
	          <div style={{...styles.changeStatContainer, margin: '0px 40px'}}>
		          <div style={styles.optionRow}>
			          <p style={styles.text}>
				          {localizations.profile_statistics_fill_stat_text}
			          </p>
			          <div style={styles.button} onClick={this.openFillStat}>
				          {localizations.profile_statistics_fill_stat_button}
			          </div>
		          </div>
	          </div>
	          }
          <div style={styles.content}>
            <h1 style={styles.title}>
              {localizations.profile_statistics_teams_title}
            </h1>
            <div style={styles.teamRow}>
              {sportunitiesStatistics && user.areStatisticsActivated && sportunitiesStatistics.map((stat, index) => (
                <div key={index+'Stat'} style={styles.statItem}>
                  <div style={styles.statValue}>{stat.value}</div>
                  <div style={styles.statName}>{stat.sportunityTypeStatus ? stat.sportunityTypeStatus : stat.sportunityType}</div>
                </div>
              ))}
            </div>
            <div>
              <div style={styles.section}>
                {user.areStatisticsActivated && opponentStatistics && user.areStatisticsActivated && opponentStatistics.length > 0
                  ? <div>
                    <div style={{width: '100%', overflowX: 'auto'}}>
                      <table style={styles.table}>
                        <thead>
                        <tr style={{backgroundColor: '#abcff2'}}>
                          <th style={styles.colLabel}>{localizations.profile_statistics_sportunity_opponent}</th>
                          {opponentColStatistics.map((col, index) => (
                            <th style={styles.headerCol} key={index + 'Col'}>{localizations.profile_statistics_sportunity_nbrOf + col.name}</th>
                          ))}
                        </tr>
                        </thead>
                        <tbody>
                        {opponentStatistics && opponentStatistics.map((stat, index) => (
                          <tr key={index} style={{backgroundColor: (index % 2 === 1) ? '#FFF' : '#ddefff'}}>
                            <td style={styles.col}>
                              <div style={styles.circle}>
                                <div style={{...styles.icon, backgroundImage: stat.opponent && stat.opponent.avatar ? 'url('+ stat.opponent.avatar +')' : 'url("https://sportunitydiag304.blob.core.windows.net/avatars/default-avatar.png")'}} />
                                <div style={styles.name}>{stat.opponent ? stat.opponent.pseudo : ''}</div>
                              </div>
                            </td>
                            {stat.values.map((value, indexCol) => (
                              <td key={index+'-'+indexCol} style={styles.col}>
                                {value}
                              </td>
                            ))}
                          </tr>
                        ))}
                        </tbody>
                      </table>
                    </div>
                  </div>
                  : this.state.isProcessing
                    ? <div style={styles.loadingContainer}><ReactLoading type='cylon' color={colors.blue} /></div>
                    : currentUserIsOwner
                      ?
                      <div style={styles.section}>
                        <div style={{...styles.subtitle, textAlign: 'center'}}>
                          {localizations.profile_statistics_teams_none_owner['0']}
                          <Link to='/my-info' style={{textDecoration: 'none', color: colors.blue}}>{localizations.profile_statistics_teams_none_owner['1']}</Link>
                          {localizations.profile_statistics_teams_none_owner['2']}
                        </div>
                        <img style={styles.image} src={localizations.profile_statistics_teams_none_image_owner}/>
                      </div>
                      :
                      <div style={styles.section}>
                        <div style={{...styles.subtitle, textAlign: 'center'}}>
                          {localizations.profile_statistics_teams_none['0'] + user.pseudo + localizations.profile_statistics_teams_none['1']}
                        </div>
                        <img style={styles.image} src={localizations.profile_statistics_teams_none_image}/>
                      </div>
                }
              </div>
            </div>
          </div>
            <div style={{...styles.content, backgroundColor: '#f3f9fe'}}>
              <h1 style={styles.title}>
                {localizations.profile_statistics_team_member_title}
              </h1>
              {this.state.isProcessing
                ? <div style={styles.loadingContainer}><ReactLoading type='cylon' color={colors.blue} /></div>
                :
                <div>
                  {user.areStatisticsActivated && userStatistics && userStatistics.length > 0
                    ? <div style={styles.section}>
                      <h3 style={styles.subtitle}>
                        {localizations.profile_statistics_participants_title}
                      </h3>
                      {currentUserIsOwner &&
                      <div style={styles.changeStatContainer}>
                        <div style={styles.optionRow}>
                          <p style={styles.text}>
                            {localizations.profile_statistics_customize_stat['0']}
                            <span style={{color: colors.red, margin: '0.25em'}}>
                              {' ' + localizations.profile_statistics_customize_stat['1'] + ' '}
                            </span>
                            {localizations.profile_statistics_customize_stat['2']}
                          </p>
                          <div style={styles.button} onClick={this.openCustomizeStat}>
                            {localizations.profile_statistics_customize_stat_add}
                          </div>
                        </div>
                        <div style={styles.optionRow}>
                          <p style={styles.text}>
                            {localizations.profile_statistics_fill_stat_text}
                          </p>
                          <div style={styles.button} onClick={this.openFillStat}>
                            {localizations.profile_statistics_fill_stat_button}
                          </div>
                        </div>
                      </div>
                      }
                      <div style={styles.bestRow}>
                        {userBestStatistics.filter(stat => stat.value > 0).map((stat, index) => (
                          <div key={index+'Best'} style={styles.bestItem}>
                            <div style={{...styles.iconBest, backgroundImage: stat.participant && stat.participant.avatar ? 'url('+ stat.participant.avatar +')' : 'url("https://sportunitydiag304.blob.core.windows.net/avatars/default-avatar.png")'}} />
                            <div style={styles.moreOf}>{localizations.profile_statistics_more_of + userStatisticsCols[index].name.toUpperCase()}</div>
                            <div style={styles.pseudo}>{stat.participant ? stat.participant.pseudo : ''}</div>
                            <div style={styles.value}>{stat.value + ' ' + userStatisticsCols[index].name}</div>
                          </div>
                        ))}
                      </div>
                      <div style={{width: '100%', overflowX: 'auto'}}>
                        <table style={styles.table}>
                          <thead>
                          <tr style={{backgroundColor: '#abcff2'}}>
                            <th style={styles.colLabel}>{localizations.profile_statistics_participant}</th>
                            {userStatisticsCols.map((name, index) => (
                              <th key={index} style={styles.headerCol}>
                                <span style={styles.colName}>
                                  {name.name}
                                  <span style={styles.sortIcons}>
                                    <span style={styles.sortUpIcon} onClick={() => this.sortUp(index)}  />
                                    <span style={styles.sortDownIcon} onClick={() => this.sortDown(index)} />
                                  </span>
                                </span>
                              </th>
                            ))}
                          </tr>
                          </thead>
                          <tbody>
                          {userStatistics.map((stat, index) => (
                            <tr key={index} style={{backgroundColor: (index % 2 === 1) ? '#FFF' : '#ddefff'}}>
                              <td style={styles.colLabel}>
                                <Circle
                                  key={index}
                                  name={stat.participant.pseudo || '' }
                                  image={stat.participant ? stat.participant.avatar : null}
                                />
                              </td>
                              {stat.values.map((value, colIndex) => (
                                <td key={index+'-'+colIndex} style={styles.col}>
                                  {value.value}
                                </td>
                              ))}
                            </tr>
                          ))}
                          </tbody>
                        </table>
                      </div>
                    </div>
                    : this.state.isProcessing
                      ? <div style={styles.loadingContainer}><ReactLoading type='cylon' color={colors.blue} /></div>
                      : currentUserIsOwner
                        ?
                        <div style={styles.section}>
                          <div style={{...styles.subtitle, textAlign: 'center'}}>
                            {localizations.profile_statistics_user_none_owner['0']}
                            <span style={{color: colors.red}}>{localizations.profile_statistics_user_none_owner['1']}</span>
                            {localizations.profile_statistics_user_none_owner['2']}
                            <Link to='/my-info' style={{textDecoration: 'none', color: colors.blue}}>{localizations.profile_statistics_user_none_owner['3']}</Link>
                            {localizations.profile_statistics_user_none_owner['4']}
                          </div>
                          <img style={styles.image} src={localizations.profile_statistics_user_none_image_owner}/>
                        </div>
                        :
                        <div style={styles.section}>
                          <div style={{...styles.subtitle, textAlign: 'center'}}>
                            {localizations.profile_statistics_user_none['0'] + user.pseudo + localizations.profile_statistics_user_none['1']}
                          </div>
                          <img style={styles.image} src={localizations.profile_statistics_user_none_image}/>
                        </div>
                  }
                </div>
              }
            </div>
          </div>
        )
    }
}

export default Relay.createContainer(Radium(TeamsStats), {
    initialVariables: {
        id: null,
        circleIds: null,
        query: false,
        dateInterval: null
    },
    fragments: {
        user: () => Relay.QL`
            fragment on User {
                pseudo
                id
                areStatisticsActivated
                ${FillStatsModal.getFragment('user')}
                ${RemoveFilterMutation.getFragment('user')},
                statisticFilters @include(if:$query) {
                    id
                    name
                    date_begin
                    date_end
                    circleList(first: 20) {
                        edges { 
                            node { 
                                id
                                name
                            }
                        }
                    }
                }
                circles (last: 20) @include(if:$query) {
                    edges {
                        node {
                            id
                            name
                            memberCount
                            type
                        }
                    }
                }
            }
        `,
        viewer: () => Relay.QL`
            fragment on Viewer {
                ${FillStatsModal.getFragment('viewer')}
                me {
                    id
                }
                statisticPreferences (userID: $id) @include(if:$query) {
                    private,
                    isManOfTheGameActivated
                    userStats {
                        stat0 {
                            id
                            name
                        }
                        stat1 {
                            id,
                            name
                        }
                        stat2 {
                            id,
                            name
                        }
                        stat3 {
                            id,
                            name
                        }
                        stat4 {
                            id,
                            name
                        }
                        stat5 {
                            id,
                            name
                        }
                        statManOfTheGame {
                            id,
                            name
                        }
                    }
                }
                sportunitiesStatistics (userID: $id) @include(if:$query) {
                    sportunityType {
                        id
                        name {
                            EN,
                            FR
                        }
                    }
                    sportunityTypeStatus {
                        id
                        name {
                            EN,
                            FR
                        }
                    }
                    details {
                        opponent {
                            id
                            pseudo
                            avatar
                        }
                        value
                    }
                    value
                }
                circlesStatistics (userID: $id, circlesIDs: $circleIds, dateInterval: $dateInterval) @include(if:$query) {
                    statisticName {
                        id,
                        name
                    },
                    participant {
                        id
                        pseudo
                        avatar
                    }
                    value
                }
            }
        `,
    }
});
