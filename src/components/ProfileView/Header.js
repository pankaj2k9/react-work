import React from 'react';
import PureComponent, { pure } from '../common/PureComponent'
import Relay from 'react-relay';
import Radium from 'radium';
import AlertContainer from 'react-alert'
import { Link } from 'react-router'

import RelayStore from '../../RelayStore.js'

import { colors } from '../../theme';
import localizations from '../Localizations'
import {confirmModal} from '../common/ConfirmationModal'

import FollowMutation from './FollowUserMutation.js';
import UnfollowMutation from './UnfollowUserMutation.js';
import AddToCalendar from './AddToCalendar'


let styles;
class Header extends PureComponent {

  constructor () {
    super();
    this.state = {
      isFollowing: false
    }
    this.alertOptions = {
      offset: 60,
      position: 'top right',
      theme: 'light',
      transition: 'fade',
    };
  }

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.viewer && nextProps.viewer.me && nextProps.user)
      this.checkIfUserIsFollower(nextProps.user.followers, nextProps.viewer.me.id);
  }

  checkIfUserIsFollower = (followers, meId) => {
    followers.forEach(follower => {
      if(follower.id === meId) {
         this.setState({ isFollowing: true });
         return false;
      } else {
        this.setState({ isFollowing: false });
        return false;
      }
    });
  }

  confirmFollowUser = () => {
    if (this.props.viewer && this.props.viewer.me) {
      if (this.props.viewer.me.id === this.props.user.id) {
        this.msg.show(localizations.profile_follow_self_user_error, {
          time: 3000,
          type: 'info',
        });
        setTimeout(() => {
          this.msg.removeAll();
        }, 3000);

        return ;
      }

      if (this.state.isFollowing) {
        confirmModal({
          title: localizations.formatString(localizations.profile_unfollow_user_confirm_title, this.props.user.pseudo),
          message: localizations.formatString(localizations.profile_unfollow_user_confirm_message, this.props.user.pseudo),
          confirmLabel: localizations.profile_unfollow_user_confirm_yes,
          cancelLabel: localizations.profile_unfollow_user_confirm_no,
          onConfirm: () => {
            this.unfollowUser()
          },
          onCancel: () => {}
        })
      }
      else {
        this.followUser();
      }
    }
    else {
      this.msg.show(localizations.profile_follow_user_not_loggedin, {
        time: 3000,
        type: 'info',
      });
      setTimeout(() => {
        this.msg.removeAll();
      }, 3000);
    }
  }

  followUser = () => {

    const {viewer} = this.props ;

    RelayStore.commitUpdate(
      new FollowMutation({
        viewer,
        userId: this.props.user.id,
        meId: this.props.viewer.me.id,
      }),
      {
        onFailure: error => {
          this.msg.show(localizations.profile_follow_user_failed, {
            time: 3000,
            type: 'error',
          });
          setTimeout(() => {
            this.msg.removeAll();
          }, 3000);
          let errors = JSON.parse(error.getError().source);
          console.log(errors);
        },
        onSuccess: (response) => {
          this.setState({
            isFollowing: true
          })
          this.msg.show(localizations.formatString(localizations.profile_follow_user_success, this.props.user.pseudo), {
            time: 3000,
            type: 'success',
          });
          setTimeout(() => {
            this.msg.removeAll();
          }, 3000);
        },
      }
    )
  }

  unfollowUser = () => {
    const {viewer} = this.props ;

    RelayStore.commitUpdate(
      new UnfollowMutation({
        viewer,
        userId: this.props.user.id,
      }),
      {
        onFailure: error => {
          this.msg.show(localizations.profile_unfollow_user_failed, {
            time: 3000,
            type: 'error',
          });
          setTimeout(() => {
            this.msg.removeAll();
          }, 3000);
          let errors = JSON.parse(error.getError().source);
          console.log(errors);
        },
        onSuccess: (response) => {
          this.setState({
            isFollowing: false
          })
          this.msg.show(localizations.formatString(localizations.profile_unfollow_user_success, this.props.user.pseudo), {
            time: 3000,
            type: 'success',
          });
          setTimeout(() => {
            this.msg.removeAll();
          }, 3000);
        },
      }
    )
  }

  render() {
    const { user, viewer, isSelfProfile } = this.props ;

    let pastEventCount = user.sportunities.count ;
    let upcomingEventCount = viewer.sportunities ? viewer.sportunities.count : 0;

    return(
      <header style={styles.header}>
        <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
        <div style={styles.row}>
          <div style={styles.mainInfo}>
            <div style={{ ...styles.userpic, backgroundImage: this.props.avatar ? 'url('+ this.props.avatar +')' : 'url("https://sportunitydiag304.blob.core.windows.net/avatars/default-avatar.png")'}} />
              <div style={styles.userInfo}>
                <h1 style={styles.username}>
                  {user.pseudo}
                </h1>
                {
                  user.publicAddress &&
                  <div style={styles.location}>
                    <i
                      style={styles.marker}
                      className="fa fa-map-marker"
                      aria-hidden="true"
                    />
                    {user.publicAddress.city}, {user.publicAddress.country}
                  </div>
                }
                { upcomingEventCount > 0 &&
                  <div style={styles.events}>
                    {localizations.formatString(upcomingEventCount <= 1
                    ? localizations.profileView_header_upcoming
                    : localizations.profileView_header_upcomings, upcomingEventCount)}
                  </div>
                }
                { pastEventCount > 0 &&
                  <div style={styles.events}>
                    {localizations.formatString(pastEventCount <= 1
                    ? localizations.profileView_header_past_event
                    : localizations.profileView_header_past_events, pastEventCount)}
                  </div>
                }
            </div>
          </div>

        </div>
        <AddToCalendar
          me={this.props.me}
          userId={this.props.user.id}
        />
      </header>
    )
  }
}

styles = {
  header: {
    width: '100%',
    padding: '47px 42px 38px',
    backgroundColor: colors.blue,
    color: colors.white,
    display: 'flex',
    flexDirection: 'column',
    borderTopLeftRadius: '5px',
    borderTopRightRadius: '5px',
    '@media (max-width: 600px)': {
      display: 'block',
    },
  },

  row: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    '@media (max-width: 600px)': {
      flexDirection: 'column'
    },
  },

  mainInfo: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    '@media (max-width: 600px)': {
      flexDirection: 'column'
    },

  },

  userpic: {
    borderRadius: '50%',
    width: 120,
    height: 120,
    marginRight: 54,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    '@media (max-width: 450px)': {
      margin: '0 auto',
    },
  },

  userInfo: {
    color: colors.white,
    fontFamily: 'Lato',
    fontSize: 22,
    fontWeight: 500,
  },

  username: {
    fontSize: 36,
    marginBottom: 13,
  },

  location: {
    marginBottom: 17,
  },

  marker: {
    color: colors.white,
    marginRight: 18,
  },

  rightSection: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginRight: 65,
  },

  followers: {
    fontSize: 30,
    fontFamily: 'Lato',
    display: 'flex',
    '@media (max-width: 640px)': {
      marginRight: 10,
    },
    '@media (max-width: 600px)': {
      marginTop: 10,
    }
  },

  followerIcon: {
    fontSize: 30,
    marginLeft: 10,
    cursor: 'pointer'
  },

  modifyProfileButton: {
    backgroundColor: colors.green,
    color: colors.white,
    width: 180,
    height: 57,
    borderRadius: 100,
    borderStyle: 'none',
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
    fontSize: 22,
    cursor: 'pointer',
    marginTop: 15
  },
};

export default Relay.createContainer(Radium(Header), {
  initialVariables: {
    userId: null,
  },

  fragments: {
    user: () => Relay.QL`
      fragment on User {
        id
        pseudo
        avatar
        publicAddress {
          city
          country
        }
        sportunities(last:2) {
          count
        }
        followers{
          id
        }
      }
    `,
    me: () => Relay.QL`
      fragment on User {
        id
        ${AddToCalendar.getFragment('me')}
      }
    `,
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
        me {
          id
        }
        sportunities (last:2, userId: $userId, filter: {status: MySportunities}) {
          count
        }
      }
    `
  },
});
