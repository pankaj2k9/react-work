import React from 'react';
import PureComponent, { pure } from '../../common/PureComponent'
import Relay from 'react-relay';
import Radium from 'radium';
import ReactLoading from 'react-loading';
import Select from 'react-select';
import RelayStore from '../../../RelayStore'

import localizations from '../../Localizations'
import { colors } from '../../../theme';
import styles from './styles.js';

import Input from './Input'
import Circle from './Circle';
import DatePicker from 'react-datepicker'
import moment from "moment/moment";
import RemoveFilterMutation from "./RemoveFilterMutation";
import AlertContainer from "react-alert";
import SelectFilter from "./SelectFilter";
import NewFilterMutation from "./NewFilterMutation";
import UpdateFilterMutation from "./UpdateFilterMutation";
import FilterModal from "./FilterModal"
import {browserHistory, Link} from "react-router";

var Style = Radium.Style;

let RSelect = Radium(Select);

class ClubsStats extends React.Component {

    constructor() {
        super();
        this.alertOptions = {
            offset: 60,
            position: 'top right',
            theme: 'light',
            transition: 'fade',
        };
        this.state = {
            teamStatistics: [],
            teamStatisticsCols: [],
            isProcessing: false,
            isCircleListOpen: false,
            selectedCircles: [],
            selectedFilter: null,
            date: null,
            nameFilter: null,
            openModal: false,
            userStats: []
        }
    }

    componentDidMount() {
        // window.addEventListener('click', this._handleClickOutside);
        if (this.props.userId) {
            this.props.relay.setVariables({
                id: this.props.userId,
                query: true
            },readyState => {
                if (readyState.done) {
                    setTimeout(() => {
                        this.props.relay.setVariables({
                            query2: true
                        })
                    }, 50);
                }
            })
            if (this.props.user && this.props.user.circles && this.props.user.circles.edges && this.props.user.circles.edges.length > 0) {
                this.setState({
                    isProcessing: true
                })
            }
        }
    }

    componentWillUnmount() {
        // window.removeEventListener('click', this._handleClickOutside);
    }

    _handleClickOutside = event => {
        if (!this._containerNode.contains(event.target)) {
        this.setState({ isCircleListOpen: false });
        }
    }

    componentWillReceiveProps = (nextProps) => {
      this.setState({
        isProcessing: true
      })
      // if (nextProps.viewer.circle && nextProps.viewer.circle.id === this.state.selectedCircles.id) {
      let teamStatisticsCols = this._getTeamsStatsCols();
      let teamStatistics = this._getTeamsStats(nextProps.user);
      this.setState({
        teamStatistics,
        teamStatisticsCols,
        userStats: this._getUserStats(nextProps.user),
      })
      setTimeout(() => {
        this.sortDown(0);
        if (this.state.isProcessing)
          this.setState({
            isProcessing: false
          })
      }, teamStatistics.length > 0 ? 1500 : 20000)
        // }
    }

    _getUserStats = (user) => {
      console.log('userStats');
      let result = [
        {name: localizations.profile_statistics_club_member, value: this._getAllMember(user)},
        {name: localizations.profile_statistics_user_averageWeek, value: this._getAverageWeek(user)},
        {name: localizations.profile_statistics_user_averageMonth, value: this._getAverageMonth(user)},
        {name: localizations.profile_statistics_user_averageYear, value: this._getAverageYear(user)},
      ]
      return result
    };

    _getAllMember = (user) => {
      let result = [];
      user.allCircleMembers.forEach(member => {
        if (result.findIndex(id => id === member.user.id) < 0)
          result.push(member.user.id)
      });
      user.subAccounts && user.subAccounts.forEach(team => {
        team.allCircleMembers.forEach(member => {
          if (result.findIndex(id => id === member.user.id) < 0)
            result.push(member.user.id)
        });
      });
      return result.length;
    };

    _getAverageWeek = (user) => {
      let result = 0;
      result += user.userStatistics.averageNumberOfOrganizedWeek;
      user.subAccounts && user.subAccounts.forEach(team => {
        result += team.userStatistics.averageNumberOfOrganizedWeek;
      });
      return Math.round(result * 10) / 10;
    };

    _getAverageMonth = (user) => {
      let result = 0;
      result += user.userStatistics.averageNumberOfOrganizedMonth;
      user.subAccounts && user.subAccounts.forEach(team => {
        result += team.userStatistics.averageNumberOfOrganizedMonth;
      });
      return Math.round(result * 10) / 10;
    };

    _getAverageYear = (user) => {
      let result = 0;
      result += user.userStatistics.averageNumberOfOrganizedYear;
      user.subAccounts && user.subAccounts.forEach(team => {
        result += team.userStatistics.averageNumberOfOrganizedYear;
      });
      return Math.round(result * 10) / 10;
    };

    _getTeamsStatsCols = () => {
      let result = [
        {
          name: localizations.profile_statistics_club_team_member,
          id: 1,
        },
        {
          name: localizations.profile_statistics_club_team_event,
          id: 2
        }
      ];
      return result;
    };

    _getTeamsStats = (user) => {
      let result = [];

      user.subAccounts && user.subAccounts.forEach(team => {
        let values = [];
        let nbMember = 0;
        team.allCircleMembers.forEach(member => {
          //if (member.circles.findIndex(circle => circle.circle.owner.id === team.id) >= 0)
            nbMember += 1;
        });
        values.push({value: nbMember});
        values.push({value: team.sportunityNumber});
        result.push({participant: team, values})
      });
      return result
    };

    onClose = () => {
        this.props.relay.setVariables({
            id: null,
            query: false,
        })
        this.props.onLeave();
    }

    sortUp = (colIndex) => {
        let teamStats = this.state.teamStatistics ;

        teamStats = teamStats.sort((a,b) => {
            if (a.values[colIndex].value - b.values[colIndex].value > 0)
                return 1;
            else if (a.values[colIndex].value - b.values[colIndex].value < 0)
                return -1
            else return 0;
        })
        this.setState({
            teamStatistics: teamStats
        })
    }

    sortDown = (colIndex) => {
        let teamStats = this.state.teamStatistics ;

        teamStats = teamStats.sort((a,b) => {
            if (b.values[colIndex].value - a.values[colIndex].value > 0)
                return 1;
            else if (b.values[colIndex].value - a.values[colIndex].value < 0)
                return -1
            else return 0;
        })
        this.setState({
            teamStatistics: teamStats
        })
    }


  _toggleModal = () => {
    this.setState({
      openModal: !this.state.openModal,
    });
  }

  _goToStat = (teamId) => {
    browserHistory.push('/profile-view/' + teamId + '/statistics');
  };

    render() {
        let {viewer, user} = this.props;
console.log(user);
        const {userStats, teamStatisticsCols, teamStatistics, userBestStatistics} = this.state;

        console.log(userStats);

        return (

          <div>
            <div style={styles.content}>
              <div style={styles.section}>
                <h1 style={styles.title}>
                  {localizations.profile_statistics_sportunity_title}
                </h1>
                <div style={styles.teamRow}>
                  {userStats && userStats.map((stat, index) => (
                    <div key={index+'Stat'} style={styles.statItem}>
                      <div style={styles.statValue}>{stat.value}</div>
                      <div style={styles.statName}>{stat.name}</div>
                    </div>
                  ))}
                </div>
              </div>
            </div>
            <div style={{...styles.content, backgroundColor: '#f3f9fe'}}>
              <h1 style={styles.title}>
                {localizations.profile_statistics_club_team_title}
              </h1>
              {this.state.isProcessing
                ? <div style={styles.loadingContainer}><ReactLoading type='cylon' color={colors.blue} /></div>
                :
                <div>
                  {teamStatistics && teamStatistics.length > 0 ?
                    <div style={styles.section}>
                      <div style={styles.subtitle}>
                        {localizations.profile_statistics_club_team + ': ' + user.subAccounts.length}
                      </div>
                      <div style={{width: '100%', overflowX: 'auto'}}>
                        <table style={styles.table}>
                          <thead>
                          <tr style={{backgroundColor: '#abcff2'}}>
                            <th style={styles.colLabel}>{localizations.profile_statistics_participant}</th>
                            {teamStatisticsCols.map((name, index) => (
                              <th key={index} style={styles.headerCol}>
                                <span style={styles.colName}>
                                  {name.name}
                                  <span style={styles.sortIcons}>
                                    <span style={styles.sortUpIcon} onClick={() => this.sortUp(index)}  />
                                    <span style={styles.sortDownIcon} onClick={() => this.sortDown(index)} />
                                  </span>
                                </span>
                              </th>
                            ))}
                          </tr>
                          </thead>
                          <tbody>
                          {teamStatistics.map((stat, index) => (
                              <tr
                                key={index}
                                style={{backgroundColor: (index % 2 === 1) ? '#FFF' : '#ddefff', cursor: 'pointer'}}
                                onClick={() => this._goToStat(stat.participant.id)}>
                                  <td style={styles.colLabel}>
                                    <Circle
                                      key={index}
                                      name={stat.participant.pseudo || '' }
                                      image={stat.participant ? stat.participant.avatar : null}
                                    />
                                  </td>
                                  {stat.values.map((value, colIndex) => (
                                    <td key={index+'-'+colIndex} style={styles.col}>
                                      {value.value}
                                    </td>
                                  ))}
                              </tr>
                          ))}
                          </tbody>
                        </table>
                      </div>
                    </div>
                    :   <div style={styles.subtitle}>{localizations.profile_statistics_sportunity_none}</div>
                  }
                </div>
              }
            </div>
          </div>
        )
    }
}

export default Relay.createContainer(Radium(ClubsStats), {
  initialVariables: {
    id: null,
    circleIds: null,
    query: false,
    query2: false,
    dateInterval: null
  },
    fragments: {
        user: () => Relay.QL`
            fragment on User {
                pseudo
                id
                allCircleMembers @include(if:$query) {
                    user {
                        id
                    }
                }
                userStatistics @include(if:$query){
                    averageNumberOfOrganizedWeek
                    averageNumberOfOrganizedMonth
                    averageNumberOfOrganizedYear
                }
                ${RemoveFilterMutation.getFragment('user')},
                subAccounts @include(if:$query2){
                    id
                    pseudo
                    avatar
                    sportunityNumber
                    allCircleMembers {
                        user {
                            id
                        }
                    }
                    userStatistics {
                        averageNumberOfOrganizedWeek
                        averageNumberOfOrganizedMonth
                        averageNumberOfOrganizedYear
                    }
                }
            }
        `,
        viewer: () => Relay.QL`
            fragment on Viewer {
                me {
                    id
                }
                statisticPreferences (userID: $id) @include(if:$query) {
                    private,
                    userStats {
                        stat0 {
                            id
                            name
                        }
                        stat1 {
                            id,
                            name
                        }
                        stat2 {
                            id,
                            name
                        }
                        stat3 {
                            id,
                            name
                        }
                        stat4 {
                            id,
                            name
                        }
                        stat5 {
                            id,
                            name
                        }
                        statManOfTheGame {
                            id,
                            name
                        }
                    }
                }
                sportunitiesStatistics (userID: $id) @include(if:$query) {
                    sportunityType {
                        id
                        name {
                            EN,
                            FR
                        }
                    }
                    sportunityTypeStatus {
                        id
                        name {
                            EN,
                            FR
                        }
                    }
                    details {
                        opponent {
                            id
                            pseudo
                            avatar
                        }
                        value
                    }
                    value
                }
                circlesStatistics (userID: $id, circlesIDs: $circleIds, dateInterval: $dateInterval) @include(if:$query) {
                    statisticName {
                        id,
                        name
                    },
                    participant {
                        id
                        pseudo
                        avatar
                    }
                    value
                }
            }
        `,
    }
});
