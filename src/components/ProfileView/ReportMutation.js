import Relay from 'react-relay';

export default class ReportMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      reportUser
    }`;
  }

  getVariables() {
    return {
      userID: this.props.userIDVar,
      reason: this.props.reasonVar,
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on reportUserPayload {
        clientMutationId,
        viewer {
          id,
        }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
        fieldIDs: {
          viewer: this.props.viewer.id,
        },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
      }
    `,
  };
}
