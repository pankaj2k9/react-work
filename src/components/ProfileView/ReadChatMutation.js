import Relay from 'react-relay';

export default class ReadChatMutation extends Relay.Mutation {

  getVariables() {
    return {
      chatId: this.props.chat.id,
    };
  }

  getMutation() {
    return Relay.QL`mutation { readChat }`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on readChatPayload @relay(pattern: true) {
        viewer {
          chats,
          me {
            unreadChats
          }
        }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        viewer: this.props.viewer.id,
      },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
      }
    `,
    chat: () => Relay.QL`
      fragment on Chat {
        id
      }
    `,
  };

}
