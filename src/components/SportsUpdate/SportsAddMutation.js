import Relay from 'react-relay';
/**
*  Add sport to user mutation
*/
export default class SportsAddMutation extends Relay.Mutation {
  /**
  *  Mutation
  */
  getMutation() {
    return Relay.QL`mutation Mutation{
      upUser
    }`;
  }
  /**
  *  Variables
  */
  getVariables() {
    return {
      userID: this.props.userIDVar,
      user: {
        sports: this.props.userSPortsVar,
      },
    };
  }
  /**
  *  Fat query
  */
  getFatQuery() {
    return Relay.QL`
      fragment on upUserPayload {
        clientMutationId,
        viewer {
          id,
          me {
            sports
          }
        }
      }
    `;
  }

  /**
  *  Config
  */
  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
        fieldIDs: {
          viewer: this.props.viewer.id,
        },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        me {
          id
        }
      }
    `,
  };
}
