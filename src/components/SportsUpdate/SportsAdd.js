// This should be refactored to several components in the future

import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
// import { bindActionCreators } from 'redux';
// import { _updateSport } from 'sportunity/src/action/newActivityActions.js'
// import { ProfilePage } from 'sportunity/src/presentational/page/ProfilePage/ProfilePage.js';
import debounce from 'lodash.debounce';
import some from 'lodash/some';
import Relay from 'react-relay';
import SportsAddMutation from './SportsAddMutation';
import RelayStore from '../../RelayStore';
import { metrics, colors, fonts } from '../../theme';
import AlertContainer from 'react-alert';
import localizations from '../Localizations'

let styles;

class SportsAdd extends Component {
  constructor() {
    super();
    this.state = {
      id: '',
      name: '',
      positions: [],
      certificates: [],
      levels: [],
      levelStartIndex: '',
      levelEndIndex: '',
      levelStartName: '',
      levelEndName: '',
      key: 'sports',
      userSports: [],
      userSport: {},
      userLevels: [],
      userPositions: [],
      userCertificates: [],
      isUpperContentHidden: false,
      searchText: '',
    }

    this._updateSearchtext = debounce(this._updateSearchtext, 300);
    this.alertOptions = {
      offset: 14,
      position: 'top right',
      theme: 'light',
      time: 100,
      transition: 'fade',
    };
  }

  componentDidMount = () => {
    // If user comes from profile page
    if (this.props.viewer.me){
      // This function prepares data for mutation input (only need id's)
      // initialSports represents sports already added to user ( full objects for each sport )
      // updatedSports represents sports already added to user ( only id's of sport, levels, positions, certificates )
      // I need only id's for mutation inputs
      const initalSports = this.props.sports.slice();
      let updatedSports = [];
      // iterate initialSports
      for (let item of initalSports) {
        // push to updatedSports
        updatedSports.push({
          // sport value is sport id
          sport: item.sport.id,
          // iterate initialSports.levels array and return new array of level id's
          levels: (() => {
            let levelsArray = [];
            for( let level of item.levels) {
              levelsArray.push(level.id);
            }
            return levelsArray;
          })(),
          // iterate initialSports.positions array and return new array of position id's
          positions: (()=> {
            let positionsArray = [];
            for( let position of item.positions) {
              positionsArray.push(position.id);
            }
            return positionsArray;
          })(),
          // iterate initialSports.certificates array and return new array of certificates id's
          certificates: (()=> {
            let certificatesArray = [];
            for( let certificate of item.certificates) {
              certificatesArray.push({ certificate: certificate.certificate.id });
            }
            return certificatesArray;
          })(),
        })
      }
      // update state
      this.setState({
        userSports: updatedSports,
      });
    }
  }

  componentDidUpdate = () => {
    if(this.state.key === 'positions' && this.state.userLevels.length > 0 && this.state.positions.length === 0){
      this.setState({
        key: 'certificates',
      })
      // Actions.refresh({ title: 'Select certificate(s)' });
    }
    if(this.state.key === 'certificates' && this.state.userLevels.length > 0 && this.state.certificates.length === 0){
      this.setState({
        key: 'levels',
        isUpperContentHidden: true,
      })
      // Actions.refresh({ title: `Add ${this.state.name}` });
    }
  }

  _updateSearchtext = (text) => {
    this.setState({
      searchText: text,
    })
  }

  // Key is used to change views
  _updateKey = () => {
    if(this.state.key === 'sports') {
      this.setState({
        key: 'levelsStart',
      })
      // Actions.refresh({ title: 'Select Starting Level' });
    } else if (this.state.key === 'levelsStart') {
      this.setState({
        key: 'levelsEnd',
      })
      // Actions.refresh({ title: 'Select Ending Level' });
    }  else if (this.state.key === 'levelsEnd') {
      this.setState({
        key: 'positions',
      })
      // Actions.refresh({ title: 'Select position (s)' });
    } else if (this.state.key === 'positions') {
      this.setState({
        key: 'certificates',
      })
      // Actions.refresh({ title: 'Select certificate(s)' });
    } else if (this.state.key === 'certificates') {
      this.setState({
        key: 'levelsStart',
      })
      // Actions.refresh({ title: 'Select Starting Level' });
    }

  }

  // Select sport to be added to user's profile or sportunity
  _addSport = (item) => {
    this.setState({
      id: item.node.id,
      name: item.node.name.EN,
      positions: item.node.positions,
      certificates: item.node.certificates,
      levels: item.node.levels,
      key: 'levelsStart',
    })
    // Actions.refresh({ title: 'Select lowest level' });
  }

  // Add level to state if it doesn't exist already
  _addLevel = (item, index) => {
    // choosing start level (lowest)
    if(this.state.key === 'levelsStart'){
      let newArray = this.state.levels.slice();
      // delete levels lower then selected (from) the list
      newArray.splice(0, index)

      this.setState({
        levelStartIndex: index,
        levelStartName: item.EN.name,
        key: 'levelsEnd',
        levels: newArray,
      })

      // Actions.refresh({ title: 'Select highest level' });
      // choosing end level (highest)
    } else if(this.state.key === 'levelsEnd'){

      let newArray = this.state.levels.slice(0, index + 1);
      this.setState({
        userLevels: [...this.state.userLevels, ...newArray],
        levelEndName: item.EN.name,
        levelEndIndex: index,
        key: 'positions',
      })
      // Actions.refresh({ title: 'Select position (s)' });
    }
  }

  // Add position to state if it doesn't exist already
  _addPosition= (item, index) => {
    let newArray = this.state.userPositions.slice();
    if(some(newArray, { id: item.id })){
      return false;
    } else {
      newArray.push({
        EN: item.EN,
        id: item.id,
      });
    }
    this.setState({
      userPositions: newArray,
    })
    // remove chosen position from the list
    let updatedPositionsArray = this.state.positions.slice();
    updatedPositionsArray.splice(index, 1)
    this.setState({
      positions: updatedPositionsArray,
    })
  }

  // Add certificate to state if it doesn't exist already
  _addCertificate= (item, index) => {
    let newArray = this.state.userCertificates.slice();
    if(some(newArray, { id: item.id })){
      return false;
    } else {
      newArray.push({
        name: {
          EN: item.name.EN,
        },
        id: item.id,
      });
    }
    this.setState({
      userCertificates: newArray,
    })
    // remove chosen certificates from the list
    let updatedCertificatesArray = this.state.certificates.slice();
    updatedCertificatesArray.splice(index, 1)
    this.setState({
      certificates: updatedCertificatesArray,
    })
  }

  // Clear all fields
  _clearData = () => {
    this.setState({
      userLevels: [],
      userPositions: [],
      userCertificates: [],
      levelStartName: '',
      levelEndName: '',
      key: 'levelsStart',
    })
    // Actions.refresh({ title: 'Select lowest Level' });
  }

  // USER PROFILE SUBMIT

  // Update user sports on server
  _submitSport = () => {
    // Initial values
    const viewer = this.props.viewer;
    const userIDVar = this.props.viewer.me.id;
    const sportVar = this.state.id;
    const levelsVar = [];
    for (let item of this.state.userLevels) {
      levelsVar.push(item.id);
    }
    const positionsVar = [];
    for (let item of this.state.userPositions) {
      positionsVar.push(item.id);
    }
    const certificatesVar = [];
    for (let item of this.state.userCertificates) {
      certificatesVar.push({ certificate: item.id });
    }
    const userSPortsVar = this.state.userSports;

    // Check if this sport already exists
    if(some(userSPortsVar, { sport: sportVar })){
      // If sport exists, update it
      for (let item of userSPortsVar){
        if (item.sport === sportVar){
          item.levels = levelsVar;
          item.positions = positionsVar;
          item.certificates = certificatesVar;
        }
      }
    } else {
      // If sport doesn't exists, add it
      userSPortsVar.push(
        {
          sport: sportVar,
          levels: levelsVar,
          positions: positionsVar,
          certificates: certificatesVar,
        }
      )
    }
    // Commit mutation
    RelayStore.commitUpdate(
      new SportsAddMutation({
        viewer,
        userIDVar,
        userSPortsVar,
      }),
      {
        // handle success and errors
        onFailure: error => {
          let errors = JSON.parse(error.getError().source);
          console.log(errors);
        },
        onSuccess: (response) => {
          console.log(response);
          this.msg.show('Update successfull!', {
            time: 0,
            type: 'success',
          });
          setTimeout(function () {
            console.log(browserHistory)
            browserHistory.goBack()
          }, 1000);
        },
      }
    );
  }

  // SPORTUNITY SUBMIT

  // Add sport to newActivity reducer
  submitSportunitySport = () => {
    return false
    // const sportVar = this.state.id;
    // const sportName = this.state.name;
    // const sportLevelNames = [];
    // const levelsVar = [];
    //
    // for (let item of this.state.userLevels) {
    //   sportLevelNames.push(item.EN.name);
    // }
    //
    // for (let item of this.state.userLevels) {
    //   levelsVar.push(item.id);
    // }
    // const positionsVar = [];
    // for (let item of this.state.userPositions) {
    //   positionsVar.push(item.id);
    // }
    // const certificatesVar = [];
    // for (let item of this.state.userCertificates) {
    //   certificatesVar.push(item.id);
    // }
    //
    // const sportunitySportVar = {
    //   sport: sportVar,
    //   levels: levelsVar,
    //   positions: positionsVar,
    //   certificates: certificatesVar,
    // }
    // this.props._updateSport(sportName, sportLevelNames, sportunitySportVar);
    // Actions.pop();
  }

  render() {
    let { viewer, allSports } = this.props;

    return(
      <div style={styles.rootContainer}>
        <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
        <input
          type='text'
          placeholder='search sports'
          onChange={() => this._updateSearchtext}
        />
        {/* Subheader Text and button */}

        {
          this.state.key === 'levelsStart' || this.state.key === 'levelsEnd' ?
            <div style={styles.subHeaderContainer}>
              <h4 style={styles.text}>{this.state.name} Levels</h4>
            </div>
            : null
        }
        {
          this.state.key === 'positions' ?
            <div style={styles.subHeaderContainer}>
              <h4 style={styles.text}>{this.state.name}  Positions</h4>
              <div style={styles.buttonContainer} onClick={this.updateKey}>
                <h4 style={styles.buttonText}>
                  Next
                </h4>
              </div>
            </div>
            : null
        }
        {
          this.state.key === 'certificates' ?
            <div style={styles.subHeaderContainer}>
              <h4 style={styles.text}>{this.state.name}  Certificates</h4>
            </div>
            : null
        }

        {/* Levels list for choosing from/to levels */}
        {
          !this.state.isUpperContentHidden ?
            <div style={styles.container}>

              {
                this.state.key === 'sports' ?
                  allSports.edges
                    .filter((item) => item.node.name.EN.toLowerCase().indexOf(this.state.searchText) >= 0)
                    .map((item, index) => (
                      <div
                        key={index}
                        style={styles.itemContainer}
                        onClick={() => this._addSport(item)}
                      >
                        <img
                          style={styles.icon}
                          src={item.node.logo}
                          alt='sport-icon'
                        />
                      <h4 style={styles.name}>
                          {item.node.name.EN}
                        </h4>

                      </div>
                    ))
                    : null
              }
              {
                this.state.key === 'levelsStart' || this.state.key === 'levelsEnd' ?
                  this.state.levels
                    .filter((item) => item.EN.name.toLowerCase().indexOf(this.state.searchText) >= 0)
                    .map((item, index) => (
                      <div
                        key={index}
                        style={styles.itemContainer}
                        onClick={() => this._addLevel(item, index)}
                      >
                        <h4 style={styles.name}>
                          {index + 1}:  {item.EN.name}
                        </h4>

                      </div>
                    ))
                    : null
              }

              {/* Positions list */}

              {
                this.state.key === 'positions' ?
                  this.state.positions
                    .filter((item) => item.EN.toLowerCase().indexOf(this.state.searchText) >= 0)
                    .map((item, index) => (
                      <div
                        key={index}
                        style={styles.itemContainer}
                        onClick={() => this._addPosition(item, index)}
                      >
                        <h4 style={styles.name}>
                          {item.EN}
                        </h4>

                      </div>
                    ))
                    : null
              }

              {/* Certificates list */}

              {
                this.state.key === 'certificates' ?
                  this.state.certificates
                    .filter((item) => item.name.EN.toLowerCase().indexOf(this.state.searchText) >= 0)
                    .map((item, index) => (
                      <div
                        key={index}
                        style={styles.itemContainer}
                        onClick={() => this._addCertificate(item, index)}
                      >
                        <h4 style={styles.name}>
                          {item.name.EN}
                        </h4>

                      </div>
                    ))
                    : null
              }

            </div> : null
        }


        {/* Bottom container represents chosen levels, positions and certificates */}

        <div style={styles.bottomContainer}>

          <div style={styles.bottomColumn}>
            <h4 style={styles.bottomText}>
              From Level
            </h4>

            <div>
              <h4 style={styles.name}>
                {this.state.levelStartName}
              </h4>
            </div>

            <h4 style={styles.bottomText}>
              To Level
            </h4>

            <div>
              <h4 style={styles.name}>
                {this.state.levelEndName}
              </h4>
            </div>
          </div>

          <div style={styles.bottomColumn}>
            <h4 style={styles.bottomText}>
              Positions
            </h4>
            {
              this.state.userPositions
                .map((position, index) => (
                  <h4 key={index} style={styles.name}>{position.EN}</h4>
                ))
            }
          </div>

          <div style={styles.bottomColumn}>
            <h4 style={styles.bottomText}>
              Certificates
            </h4>
            {
              this.state.userCertificates
                .map((certificate, index) => (
                  <h4 key={index} style={styles.name}>{certificate.name.EN}</h4>
                ))
            }
          </div>

        </div>

        {
          this.state.userLevels.length > 0 && viewer.me ?
            <div style={styles.addSportButtonContainer} onClick={this._submitSport}>
              <h4 style={styles.addSportButtonText}>
                Add sport
              </h4>
            </div>
            : null
        }

        {
          this.state.userLevels.length > 0 && !viewer.me ?
            <div style={styles.addSportButtonContainer} onClick={this._submitSportunitySport}>
              <h4 style={styles.addSportButtonText}>
                Add Sportunity Sport
              </h4>
            </div>
            : null
        }

      </div>
    )
  }

}

SportsAdd.propTypes = {
  viewer: React.PropTypes.object.isRequired,
  sports: React.PropTypes.array,
  allSports: React.PropTypes.object.isRequired,
  // _updateSport: React.PropTypes.func.isRequired,
};

/**
 * Triggered when sport remove button is pressed
 */
// const _updateSport = (text) => ({
//   type: types.UPDATE_SEARCHTEXT,
//   text,
// });

// const dispatchToProps = (dispatch) => ({
//   _updateSport: bindActionCreators(_updateSport, dispatch),
// })

const ReduxContainer = connect(
  null,
  null
)(SportsAdd)

export default Relay.createContainer(ReduxContainer, {
  fragments: {
    sports: () => Relay.QL`
      fragment on SportDescriptor @relay(plural: true){
        sport{
          id,
          name{
            EN
          },
          logo,
          positions{
            EN
          },
          certificates{
            id,
            name{
              EN
            }
          },
          levels{
            id,
            EN {
              name,
              description,
              skillLevel
            }
          }
        },
        positions{
          id
          EN
        },
        certificates{
          certificate{
            id,
            name{
              id,
              EN
            }
          }
        },
        levels{
          id,
          EN {
            name,
            description,
            skillLevel
          }
        }
      }
    `,
    allSports: () => Relay.QL`
      fragment on SportConnection {
        edges {
          node {
            id,
            name {
              id,
              EN
            },
            logo,
            positions {
              id,
              EN
            },
            certificates {
              id,
              name {
                EN
              }
            },
            levels {
              id,
              EN {
                name,
                description,
                skillLevel
              }
            }
          }
        }
      }
    `,
  },
});


styles = {
  rootContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    flex: 1,
  },
  container: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    marginBottom: metrics.margin.medium,
    borderBottomWidth: 1,
    borderBottomColor: colors.steel,
  },
  itemContainer: {
    display: 'flex',
    marginHorizontal: metrics.margin.medium,
    paddingVertical: metrics.margin.medium,
    borderBottomWidth: 1,
    borderBottomColor: colors.blue,
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    width: 35,
    height: 35,
  },
  name: {
    paddingHorizontal: metrics.margin.medium,
    paddingVertical: 5,
    fontSize: fonts.size.medium,
    fontWeight: '400',
    color: colors.blue,
  },
  subHeaderContainer: {
    display: 'flex',
    margin: metrics.margin.medium,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  text: {
    color: colors.blue,
    fontSize: fonts.size.regular,
    fontWeight: '600',
  },
  buttonContainer: {
    backgroundColor: colors.skyBlue,
    marginLeft: metrics.margin.medium,
    padding: metrics.padding.medium,
    borderRadius: 5,
  },
  buttonText: {
    color: colors.snow,
  },
  bottomContainer: {
    marginTop: metrics.margin.medium,
    flex: 1,
    flexDirection: 'row',
  },
  bottomText: {
    color: colors.blue,
    fontSize: fonts.size.regular,
    fontWeight: '600',
    textDecorationStyle: 'solid',
    textDecorationLine: 'underline',
  },
  bottomColumn: {
    flex: 1,
    alignItems: 'center',
  },
  addSportButtonContainer: {
    display: 'flex',
    backgroundColor: colors.skyBlue,
    margin: metrics.margin.medium,
    padding: metrics.margin.medium,
    borderRadius: 50,
    alignItems: 'center',
  },
  addSportButtonText: {
    fontSize: fonts.size.xl,
    color: colors.white,
    textAlign: 'center',
  },
};
