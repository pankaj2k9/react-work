import React from 'react';
import Relay from 'react-relay';
import SportsAddMutation from './SportsAddMutation.js';
import SportsAdd from './SportsAdd.js'
import { metrics, colors, fonts } from '../../theme';

let styles;


// import UserSportsAdd from './UserSportsAdd/UserSportsAdd.js';
// import SearchBar from './SearchBar/SearchBar.js';


const SportsUpdate = ({ viewer }) => {
  return(
    <div style={styles.container}>
      {
        viewer.me &&
          <SportsAdd
            sports={viewer.me.sports}
            allSports={viewer.sports}
            viewer={viewer}
          />
      }
    </div>
  )
};

SportsUpdate.propTypes = {
  // searchText: React.PropTypes.string.isRequired,
  // updateSearchText: React.PropTypes.func.isRequired,
  viewer: React.PropTypes.object.isRequired,
};

// const stateToProps = (state) => ({
//   searchText: state.sportunitySport.searchText,
// });

// const dispatchToProps = (dispatch) => ({
//   updateSearchText: bindActionCreators(updateSearchText, dispatch),
// });

// const ReduxContainer = connect(
//   stateToProps,
//   null
// )(SportsUpdate);

export default Relay.createContainer(SportsUpdate, {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        ${SportsAddMutation.getFragment('viewer')},
        me {
          id,
          sports {
            ${SportsAdd.getFragment('sports')},
          }
        },
        sports(last: 200){
          ${SportsAdd.getFragment('allSports')},
        }
      }
    `,
  },
});

//
// export const SportunitySport =  Relay.createContainer(ReduxContainer, {
//   fragments: {
//     viewer: () => Relay.QL`
//       fragment on Viewer {
//         id,
//         ${SportsAddMutation.getFragment('viewer')},
//         sports (last: 10000){
//           ${SportsAdd.getFragment('allSports')}
//         }
//       }
//     `,
//   },
// });

styles = {
  container: {
    // marginTop: metrics.navBarHeight,
    flex: 1,
  },
  inputContainer: {
    backgroundColor: 'transparent',
    marginHorizontal: metrics.margin.medium,
    height: 80,
  },
  input: {
    flex: 1,
    borderBottomWidth: 2,
    borderBottomColor: colors.skyBlue,
    fontSize: fonts.size.medium,
    height: 40,
    maxHeight: 40,
  },
  text: {
    color: colors.blue,
    fontWeight: '500',
    marginTop: metrics.margin.medium,
  },
  button: {
    backgroundColor: colors.skyBlue,
    padding:  metrics.margin.medium,
    marginTop:  metrics.margin.medium,
    marginHorizontal:  metrics.margin.medium,
    marginBottom: 60,
    borderRadius: 50,
  },
  buttonText: {
    fontSize: fonts.size.h3,
    color: colors.white,
    textAlign: 'center',
  },
};
