import React from 'react';
import PureComponent, { pure } from '../common/PureComponent'
import Relay from 'react-relay';
import { styles } from './styles'
import BodyHeader from './BodyHeader';
import BodyContent from './BodyContent';
import Sidebar from './Sidebar';


const SportunityDetail = pure(() => {

  return(
    <div style={styles.main}>
     <div style={styles.body}>
      <BodyHeader />
      <div style={styles.body_content_main}>
       <BodyContent />
       <Sidebar />
      </div>
      </div>
    </div>
)
})

export default Relay.createContainer(SportunityDetail, {
  fragments: {
    userId: () => Relay.QL`
      fragment on Viewer {
        id,
        me {
          id
          numberOfUnreadNotifications
          notifications(last: 5) {
            edges {
              node {
                id
                text
                link
                created
              }
            }
          }
        }
      }
    `,
  },
});
