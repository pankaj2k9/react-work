import React from 'react';
import PureComponent, { pure } from '../common/PureComponent'
import Relay from 'react-relay';
import { styles } from './styles';
const BodyContent = pure(() => {
  return(
    <div style={styles.body_content}>
     <div style={styles.Margin}>
      <div style={styles.Description}>Description </div>
      <div style={styles.Lorem}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </div>
      <div style={styles.Time}>Time and location</div>
      <div style={styles.body_content_second}>
       <div style={styles.body_content_second_left}>
        <div style={styles.body_content_second_left_icons}>
         <div style={styles.img_b}></div>
        </div>
        <div style={styles.body_content_second_left_text}>
        </div>
       </div>
       <div style={styles.body_content_second_right}>
        <div style={styles.map}></div>
       </div>
      </div>
      <div style={styles.Price}>Price</div>
      <div style={styles.Lorem}>Participation: <span style={styles.span_g}>10CHF</span></div>
      <div style={styles.Optionals}> Optionals additional prices : </div>
      <div style={styles.Lorem}>- Work out and filing  <span style={styles.span_g}>  +5CHF</span></div>
      <div style={styles.If}> If the minimum number of participants (10) is not reached, the event will not attend and you will not be charged.</div>
      <div style={styles.Book_2}> <div style={styles.Book_Text}> BOOK </div> </div>
      <div style={styles.Price}>Comments</div>
      <div style={styles.Comments}></div>
     </div>
    </div>
)
})
export default Relay.createContainer(BodyContent, {
  fragments: {
    userId: () => Relay.QL`
      fragment on Viewer {
        id,
        me {
          id
        }
      }
    `,
  },
});
