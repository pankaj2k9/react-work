import React from 'react';
import PureComponent, { pure } from '../common/PureComponent'
import Relay from 'react-relay';
import { styles } from './styles'
const Sidebar = pure(() => {
  return(
    <div style={styles.sidebar}>
     <span style={styles.Organizer}>Organizer </span>
     <div style={styles.img}></div>
     <span style={styles.Name}>Ronnie</span>
     <span style={styles.Organizer}>9 Participants </span>
     <div style={styles.img}></div>
     <span style={styles.Name}>Ronnie</span>
     <div style={styles.img}></div>
     <span style={styles.Name}>Connor</span>
     <div style={styles.img}></div>
     <span style={styles.Name}>Oscar</span>
     <div style={styles.img}></div>
     <span style={styles.Name}>Gene</span>
     <div style={styles.img}></div>
     <span style={styles.Name}>Stella</span>
     <div style={styles.img}></div>
     <span style={styles.Name}>Bernard</span>
     <div style={styles.img}></div>
     <span style={styles.Name}>Jeremy</span>
     <div style={styles.img}></div>
     <span style={styles.Name}>Nathan</span>
     <div style={styles.img}></div>
     <span style={styles.Name}>Mabel</span>
    </div>
)
})
export default Relay.createContainer(Sidebar, {
  fragments: {
    userId: () => Relay.QL`
      fragment on Viewer {
        id,
        me {
          id
        }
      }
    `,
  },
});
