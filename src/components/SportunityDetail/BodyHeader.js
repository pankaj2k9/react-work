import React from 'react';
import PureComponent, { pure } from '../common/PureComponent'
import Relay from 'react-relay';
import { styles } from './styles'

const BodyHeader = pure(() => {
  return(
<div style={styles.body_header}>
 <div style={styles.body_header_icon}> icon
 </div>
 <div style={styles.body_header_text}>
  <span style={styles.body_header_text_1}> Basketball for fun </span>
  <span style={styles.body_header_text_2}> Olympique de la Pontaise </span>
  <span style={styles.body_header_text_3}> 9 participants   (min 10 - max 15) </span>
 </div>
 <div style={styles.body_header_right}>
  <span style={styles.body_header_right_text}> 3 Mars, 10:00 - 12:00 </span>
  <div style={styles.body_header_right_button}> <div style={styles.body_header_right_button_text}>BOOK </div> </div>
 </div>
</div>
)
})

export default Relay.createContainer(BodyHeader, {
  fragments: {
    userId: () => Relay.QL`
      fragment on Viewer {
        id,
        me {
          id
        }
      }
    `,
  },
});
