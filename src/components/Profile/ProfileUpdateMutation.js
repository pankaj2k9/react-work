import Relay from 'react-relay';

export default class ProfileUpdateMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      upUser
    }`;
  }

  getVariables() {
    
    let var1 ={
      userID: this.props.userIDVar,
      user: {
        //pseudo: this.props.usernameVar,
        description: this.props.descriptionVar,
        sex: this.props.sexVar,
        languages: this.props.languagesVar,
        sports: this.props.sportsVar,
        pseudo: this.props.usernameVar,
        birthday: this.props.birthdayVar,
        hideMyAge: this.props.hideMyAgeVar,
        //address: this.props.addressVar,
        //firstName: this.props.firstNameVar,
        //lastName: this.props.lastNameVar,
        publicAddress: this.props.publicAddressVar
      },

    };
    return var1
  }

  getFiles() {
    return {
      avatars: this.props.file,
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on upUserPayload {
        clientMutationId,
        user {
          id, 
          birthday
          pseudo,
          hideMyAge,
          publicAddress {
            city
          },
          sports  {
            sport {
              id
              logo
              name {
                EN
                DE
                FR
              }
            } 
            levels {
              id
              EN {
                name
              }
              DE {
                name
              }
              FR {
                name
              }
            }
            positions {
              id
              EN
              DE
              FR
            }
            certificates {
              certificate {
                id
                name {
                  EN,
                  FR,
                  DE
                }
              }
            }
          }
        }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
        fieldIDs: {
          user: this.props.userIDVar,
        },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
        me {
          id
        }
      }
    `,
    user: () => Relay.QL`
      fragment on User {
        id
      }
    `
  };

}
