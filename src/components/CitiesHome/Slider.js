import React, { Component } from 'react';

import Radium from 'radium'
import localizations from "../Localizations";
import Slide from './Slide';

let styles ;

class Slider extends Component {

  constructor(props) {
    super(props);
    this.state = {
      currentSlide: 0,
      nextSlide: 0,
      delay: 0,
      isTransition: false,
    }
  }

  transitionTo = (next) => {
    this.setState({
      isTransition: true,
      delay: 0,
      nextSlide: next,
    });
    this.transitionTimer = setInterval(this.tick, 10)
  };

  tick = () => {
    if (this.state.delay < 1)
      this.setState({
        delay: this.state.delay + 0.01
      });
    else
      this.endTransition()
  };
    
  endTransition = () => {
    this.setState({
      isTransition: false,
      currentSlide: this.state.nextSlide,
    });
    clearInterval(this.transitionTimer);
  }; 

  toSlide = (next) => {
    this.transitionTo(next)
  };

  nextSlide = () => {
    this.transitionTo((this.state.currentSlide + 1) % items.length)
  };

  autoSlideStart = () => {
    clearInterval(this.timer);
    this.timer = setInterval(this.nextSlide, 4000);
  };

  autoSlideStop = () => {
    clearInterval(this.timer);
  };

  componentDidMount() {
    this.autoSlideStart();
  };

  componentWillUnmount() {
    this.autoSlideStop();
  };

  render() {
    let items = [
      {
        icon: '/assets/images/cercles.png',
        title: localizations.home_slide_circle_title,
        desc: localizations.home_slide_circle_desc,
        image: '/assets/images/ta_communaute_smartphone.png',
        goTo: null,
      },
      {
        icon: '/assets/images/loupe.png',
        title: localizations.home_slide_explorer_title,
        desc: localizations.home_slide_explorer_desc,
        image: '/assets/images/explorer_smartphone.png',
        goTo: null,
      },
      {
        icon: '/assets/images/organise.png',
        title: localizations.home_slide_organise_title,
        desc: localizations.home_slide_organise_desc,
        image: '/assets/images/creer_une_activite.png',
        goTo: null,
      },
    ];
    return  (
      <div onMouseEnter={this.autoSlideStop} onMouseLeave={this.autoSlideStart}>
        <div style={styles.slideContainer}>
        {items.map((item, index) =>
            <Slide
              icon={item.icon}
              title={item.title}
              descr={item.desc}
              image={item.image}
              link={item.goTo}
              id={index}
              {...this.state}
            />
        )}
        </div>
        <div style={styles.checkContainer}>
          {items.map((item, index) =>
            <i
              style={{margin: 10}}
              className={(index ===  this.state.nextSlide) ? 'fa fa-circle fa-2x' : 'fa fa-circle-o fa-2x'}
              key={index}
              onClick={() => this.toSlide(index)}
            />
          )}
        </div>
      </div>
    );
  }
}

styles = {
  slideContainer: {
    display: 'flex',
    flexDirection: 'row',
    height: 550,
    width: '100%',
    position: 'relative',
    overflow: 'hidden'
  },
  checkContainer: {
    display: 'flex',
    position: 'relative',
    marginTop: -40,
    marginBottom: 70,
    justifyContent: 'center',
  },
};

export default Radium(Slider);