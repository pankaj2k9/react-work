import React, { Component } from 'react';
import Radium from 'radium'
import Relay from 'react-relay'
import localizations from '../Localizations'
import ModalVideo from 'react-modal-video'

import { colors } from '../../theme';
import {Link} from "react-router";
let styles;

class HeaderImage extends Component {
  constructor () {
    super();
    this.state = {
      isOpen: false
    }
  }

  render() {
    const { viewer } = this.props
    return (
      <div style={styles.container}>
        <div style={styles.containerFilter}>
          <p style={styles.headingTextMaj}>
            {localizations.home_headerTextMaj}
          </p>
          <p style={styles.headingTextMin}>
            {localizations.home_headerTextMin}
          </p>
          <ModalVideo
            channel='youtube'
            isOpen={this.state.isOpen}
            videoId='2ZVQNZMP16E'
            onClose={() => this.setState({isOpen: false})}
            youtube={{fs:0, autoplay: 1, cc_load_policy: 1, controls: 1 }}
          />
          <span onClick={() => {this.setState({isOpen: true})}} style={styles.playButton}>
            <i style={styles.playIcon} className="fa fa-play fa-2x" />
          </span>
          <div style={styles.containerButton}>
            <Link to='/register' style={styles.searchButton}>
              <p style={styles.inputSearch}>{localizations.home_createAccount}</p>
            </Link>
          </div>
        </div>
        {/*<HeaderImageNavigator {...this.props} viewer={viewer} />*/}
      </div>
    );
  }
}


styles = {
  containerButton: {
    height: '60px',
    boxShadow: '0 0 6px 0 rgba(0,0,0,0.5)',
    borderRadius: '10px',
    // zIndex: '1',
    marginTop: 100,
    position: 'absolute',
    top: '270px',
    justifyContent: 'flex-end',
    '@media (max-width: 610px)': {
      display: 'block',
      top: 320
    },
  },
  containerFilter: {
    height: '100%',
    backgroundSize: 'cover',
    backgroundColor: '#00000044',
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: '25px 80px 150px 80px',
    '@media only screen and (max-width : 850px)': {
      padding: '25px 25px 150px 25px',
    },
  },
  container: {
    width: '100%',
    height: '410px',
    // opacity: 0.9,
    backgroundImage: 'url("assets/images/background-cities.jpg")',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    '@media (max-width: 320px)': {
      height: '210px',
    },
    '@media only screen and (max-width : 850px)': {
      height: 480,
    },
    '@media only screen and (min-width : 1530px)': {
      height: '600px',
      backgroundPosition: 'center -250px',
    },
    '@media only screen and (max-width : 1980px)': {
      height: 600,
    },
  },
  headingTextMaj: {
    // width: '36%',
    fontFamily: 'Lato',
    fontSize: '44px',
    lineHeight: '52px',
    color: colors.white,
    fontWeight: 'bold',
    // position: 'absolute',
    // top: '15px',
    // left: '19%',
    textAlign: 'center',
    '@media (max-width: 1280px)': {
      fontSize: '38px',
    },
    '@media (max-width: 978px)': {
      fontSize: '36px',
      // width: '50%',
    },
    '@media (max-width: 768px)': {
      // width: '80%',
      fontSize: '30px',
      lineHeight: '44px',
      // left: '5%',
    },
    '@media (max-width: 425px)': {
      // width: '80%',
      fontSize: '24px',
      lineHeight: '30px',
      // left: '5%',
    },
  },
  headingTextMin: {
    // width: '36%',
    fontFamily: 'Lato',
    fontSize: '34px',
    lineHeight: '52px',
    color: colors.white,
    // position: 'absolute',
    // top: '15px',
    // left: '19%',
    textAlign: 'center',
    '@media (max-width: 1280px)': {
      fontSize: '28px',
    },
    '@media (max-width: 978px)': {
      fontSize: '26px',
      // width: '50%',
    },
    '@media (max-width: 768px)': {
      // width: '80%',
      fontSize: '20px',
      lineHeight: '44px',
      // left: '5%',
    },
    '@media (max-width: 425px)': {
      // width: '80%',
      fontSize: '14px',
      lineHeight: '30px',
      // left: '5%',
    },
  },
  inputSearch: {
    fontFamily: 'Lato',
    fontSize: '22px',
    display: 'inline',
    padding: '5px 25px',
    color: colors.white,
    '@media (max-width: 425px)': {
      fontSize: '20px',
    }
  },
  playButton: {
    cursor: 'pointer',
    fontSize: '15px',
    height: 70,
    width: 70,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
    backgroundColor: 'rgba(25, 25, 25, 0.95)',
    color: colors.white,
    transition: 'all cubic-bezier(0.22,0.61,0.36,1) .3s',
    opacity: 0.9,
    marginTop: 70,
    ':hover': {
      opacity: 1,
      transform:'scale(1.1)',
      boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)'
    },
    '@media (max-width: 850px)': {
      // marginTop: 50
    }
  },
  playIcon: {
    marginLeft: 4
  },
  searchButton: {
    height: '100%',
    backgroundColor: colors.blue,
    boxSizing: 'border-box',
    paddingRight: 10,
    paddingLeft: 10,
    borderRadius: 10,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    textDecoration: 'none',
  },
};

export default Relay.createContainer(Radium(HeaderImage), {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        me
      }
    `,
  },
});
