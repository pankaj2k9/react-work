import React, { Component } from 'react';
import { browserHistory } from 'react-router';

import colors from '../../theme/colors';

import Radium from 'radium'
import localizations from "../Localizations";

let styles ;
class TagItem extends Component {

  _handleClick = (event) => {
    if (this.props.onClickGoTo) 
      browserHistory.push(this.props.onClickGoTo)
  }

  render() {
    const { image, Title, descr1, descr2, descr3, color } = this.props ;
    let containerStyle = {
      ...styles.container
    };

    let separator = {...styles.separator, backgroundColor: color};
    let headingText = {...styles.headingText, color: color};
    let seeMoreButton = {...styles.seeMoreButton, borderColor: color};

    return  (
      <div style={containerStyle}>
        <img style={styles.tagItemImage} src={image} onClick={this._handleClick}/>
        <h3 style={headingText}>{Title}</h3>
        <p style={styles.tagItemText}>
          {descr1}
        </p>
        <i style={separator}/>
        <p style={styles.tagItemText}>
          {descr2}
        </p>
        <i style={separator}/>
        <p style={styles.tagItemText}>
          {descr3}
        </p>
        {this.props.onClickGoTo &&
          <div style={{...seeMoreButton, ':hover': {color: colors.white, backgroundColor: color}}} onClick={this._handleClick}>
            {localizations.home_seeMore}
          </div>
        }
      </div>
    );
  }
}

styles = {
  container: {
    // height: '312px',
    width: '229px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-around',
    margin: '10px',
    '@media (max-width: 600px)': {
      width: '190px',
      margin: '10px 10px',
    },
    '@media (max-width: 480px)': {
      width: '290px',
      margin: '10px auto',
    }
  },
  seeMoreButton: {
    border: '1px solid',
    width: '75%',
    padding: 10,
    marginTop: 20,
    textAlign: 'center',
    fontFamily: 'Lato',
    fontSize: '18px',
    cursor: 'pointer',
    '@media (max-width: 600px)': {
      fontSize: '14px',
    },
  },
  separator: {
    height: 1,
    width: '10%',
    marginTop: 10,
  },
  tagItemImage: {
    width: '100px',
    height: '100px',
    marginBottom: 10,
    fontFamily: 'FontAwesome',
    fontSize: '50px',
    textAlign: 'center',
    lineHeight: '50px',
    color: '#fff',
    // color: '#FFFFFF',
  },
  headingText: {
    // width: '160px',
    fontFamily: 'Lato',
    fontSize: '26px',
    fontWeight: '500',
    textAlign: 'center',
    color: 'rgba(0,0,0,0.65)',
    marginBottom: 10,
    '@media (max-width: 600px)': {
      fontSize: '22px',
    },
  },
  tagItemText: {
    // width: '229px',
    // height: '150px',
    textAlign: 'center',
    fontFamily: 'Lato',
    fontSize: '18px',
    // textAlign: 'center',
    lineHeight: '23px',
    color: 'rgba(0,0,0,0.44)',
    alignSelf: 'flex-end',
    marginTop: '10px',
    width: '100%',
    '@media (max-width: 600px)': {
      fontSize: '14px',
      lineHeight: '20px',
    },
    '@media (max-width: 480px)': {
      marginBottom: '20',
      fontSize: '14px',
      lineHeight: '20px',
    },
  },
};

export default Radium(TagItem);