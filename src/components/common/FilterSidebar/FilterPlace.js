import React from 'react';
import PureComponent, { pure } from '../PureComponent'
import Radium from 'radium';
import Geosuggest from 'react-geosuggest'

import { colors, metrics, fonts } from '../../../theme';

let inputStyles
let styles;

class FilterPlace extends PureComponent {

  render() {
    const { label,
        distanceLabel,
        locationName,
        _handleRemoveLocation,
        placeholder,
        distanceRange,
        _distanceRangeChanged,
        _distanceRangeBlur,
        userLocation, 
        radius,
        _handleLocationChange,
        _locationSelected,
        hideRange } = this.props;

    return (
      <div style={styles.container}>
            <div style={styles.row}>
                <div style={styles.label}>{label}</div>
                <div style={{position: 'relative'}}>
                  {
                    locationName && (
                      <span onClick={_handleRemoveLocation} style={styles.closeCross}>
                      <i className="fa fa-times" style={styles.cancelIcon} aria-hidden="true"></i>
                    </span>)
                  }
                  <Geosuggest
                    style={inputStyles}
                    placeholder={placeholder}
                    onSuggestSelect={_locationSelected}
                    initialValue={locationName}
                    location={userLocation}
                    radius={radius}
                    onChange={() => typeof _handleLocationChange !== 'undefined' ? _handleLocationChange() : null}
                  />
                </div>
                {!hideRange && 
                    <div style={styles.distanceRangeContainer}>
                        {distanceLabel && 
                            <div style={styles.label}>{distanceLabel}</div>
                        }
                        <input
                            style={styles.distanceRange}
                            type="number"
                            placeholder="0"
                            value={distanceRange}
                            onChange={_distanceRangeChanged}
                            onBlur={_distanceRangeBlur}
                        />
                        <span style={styles.labelKm}> Km</span>
                    </div>
                }
              </div>
      </div>
    );
  }
}

styles = {
    container: {
        marginTop: 10,
        marginBottom: 10, 
        paddingLeft: 10,
        width: '100%',
        fontFamily: 'Lato',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    closeCross: {
        position: 'absolute',
        right: 0,
        top: 4,
        width: 0,
        height: 0,
        color: colors.gray,
        marginRight: '15px',
        cursor: 'pointer',
        fontSize: '16px',
    },
    label: {
        display: 'block',
        color: colors.blueLight,
        fontSize: 16,
        lineHeight: 1,
    },
    row: {
        display:'flex',
        padding: 0,
        flexDirection: 'column',
        alignItems: 'flex-start',
        position: 'relative',
        '@media (max-width: 420px)': {
          display:'block',
        }
    },
    cancelIcon: {
        marginRight: 15,
    },
    distanceRangeContainer: {
        display: 'flex',
        alignItems: 'center',
        marginTop: 10,
        //marginLeft: 15
    },
    distanceRange: {
        width: 50,
        backgroundColor: '#FFFFFF',
        border: '2px solid #5E9FDF',
        borderRadius: '3px',
        marginLeft: 3,
        marginRight: 5,
        height: 35,
        textAlign: 'center',
        fontFamily: fonts.size.xl,
        color: 'rgba(146,146,146,0.87)',
        fontSize: 16,
        marginLeft: 10
    },
    labelKm: {
        color: colors.blue,
        fontSize: '18px',
        fontWeight: fonts.weight.medium,
        marginRight: 10
    },
}

inputStyles = {
	'input': {
        borderTop: 'none',
        borderLeft: 'none',
        borderRight: 'none',
        borderBottomWidth: 2,
        borderBottomColor: colors.blue,
        fontSize: 18,
        fontFamily: 'Lato',
        lineHeight: 1,
        color: 'rgba(0, 0, 0, 0.64)',
        paddingBottom: 3,
        outline: 'none',
        ':focus': {
          borderBottomColor: colors.green,
        },
    },
    'suggests': {
        width: '100%',
        position: 'absolute',
        top: 30,
        zIndex: 100,
	},
	'suggests--hidden': {
		width: '0',
		display: 'none',
	},
    'suggestItem': {
        marginHorizontal: metrics.margin.medium,
        padding: metrics.padding.medium,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: colors.blue,
        color: colors.blue,
        fontFamily: 'Lato',
        fontSize: fonts.size.medium,
        cursor: 'pointer',
        backgroundColor: colors.white,
    },
}

export default Radium(FilterPlace);
