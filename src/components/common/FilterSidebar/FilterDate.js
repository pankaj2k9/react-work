import React from 'react';
import DatePicker from 'react-datepicker'
import Radium from 'radium';
import moment from 'moment'
var Style = Radium.Style;

import localizations from '../../Localizations';
import PureComponent, { pure } from '../PureComponent'
import { colors, metrics, fonts } from '../../../theme';

let styles;

class FilterDate extends PureComponent {

  render() {
    const { value, containerStyle, inputStyle, onChange, label, minDate } = this.props;
    const finalContainerStyle = Object.assign({}, styles.container, containerStyle);
    const finalInputStyle = Object.assign({}, styles.input, inputStyle);

    return (
      <div style={finalContainerStyle}>
            <Style scopeSelector=".datetime-hours" rules={{
              ".rdtPicker": {borderRadius: '3px', width: '100px', border: '2px solid #5E9FDF'},
              ".form-control": styles.time,
              }}
            />
            <Style scopeSelector=".datetime-day" rules={{
              "input": styles.date,
              }}
            />
            <Style scopeSelector=".react-datepicker" rules={{
                "div": {fontSize: '1.4rem'},
                ".react-datepicker__current-month": {fontSize: '1.5rem'},
                ".react-datepicker__month": {margin: '1rem'},
                ".react-datepicker__day": {width: '2rem', lineHeight: '2rem', fontSize: '1.4rem', margin: '0.2rem'},
                ".react-datepicker__day-names": {width: '100%', display: 'flex', flexDirection: 'row', justifyContent: 'space-between', marginTop: 5},
                ".react-datepicker__header": {padding: '1rem', display: 'flex', flexDirection: 'column',alignItems: 'center'}
              }}
            />
            <Style scopeSelector=".react-datepicker-popper" rules={{
                zIndex: 2
            }}/>

            <label style={styles.label}>{label}</label>
            <div style={{flex: 5}} className="datetime-day">
                <DatePicker
                    dateFormat="DD/MM/YYYY"
                    todayButton={localizations.newSportunity_today}
                    selected={value ? moment(value) : null}
                    onChange={onChange}
                    minDate={moment(minDate)}
                    locale={localizations.getLanguage().toLowerCase()}
                    popperPlacement="top-end"
                />
            </div>
            {/*<input 
                style={finalInputStyle}
                type='date'
                onChange={onChange}
                placeholder="dd/mm/yyyy"
                value={value}
            />*/}
      </div>
    );
  }
}

styles = {
  container: {
    width: '100%',
    fontFamily: 'Lato',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: 10,
    marginBottom: 10, 
    paddingLeft: 10
  },

  label: {
    display: 'block',
    color: colors.darkGray,
    fontSize: 16,
    marginRight: 8,
    flex: 1
  },

  date: {
    backgroundColor: '#FFFFFF',
    border: '2px solid #5E9FDF',
    borderRadius: '3px',
    marginLeft: 3,

    height: 35,

    textAlign: 'center',
    fontFamily: 'Lato',
    color: colors.darkGray,
    fontSize: 14
  },

  inputError: {
    input: {
    width: '100%',
    borderTop: 'none',
    borderLeft: 'none',
    borderRight: 'none',
    borderBottomWidth: 2,
    borderBottomColor: colors.red,

    fontSize: 18,
    fontFamily: 'Lato',
    lineHeight: 1,
    color: 'rgba(0, 0, 0, 0.64)',

    paddingBottom: 3,

    outline: 'none',

    ':focus': {
      borderBottomColor: colors.green,
    },

    ':disabled': {
      borderBottomColor: '#D1D1D1',
      backgroundColor: 'transparent',
    },
  },
  },

  input: {
    borderTop: 'none',
    borderLeft: 'none',
    borderRight: 'none',
    borderBottomWidth: 2,
    borderBottomColor: colors.blue,
    fontSize: 18,
    fontFamily: 'Lato',
    lineHeight: 1,
    color: 'rgba(0, 0, 0, 0.64)',
    paddingBottom: 3,
    outline: 'none',
    '@media (max-width: 768px)': {
      width: 180,
    },
    '@media (max-width: 600px)': {
      width: 240,
    },
    ':focus': {
      borderBottomColor: colors.green,
    },
    ':disabled': {
      borderBottomColor: '#D1D1D1',
      backgroundColor: 'transparent',
    },
  },
}


export default Radium(FilterDate);
