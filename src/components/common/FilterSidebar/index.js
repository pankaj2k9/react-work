import React from 'react';
import PureComponent, { pure } from '../PureComponent'
import { colors, metrics, fonts } from '../../../theme';

let styles

class FilterSidebar extends PureComponent {
    constructor(props) {
        super(props)
    }

    componentDidMount = () => {
  
    }

    render() {
        const { title } = this.props

        return(
            <div style={styles.container}>
                <div style={styles.blockTitle}>
                    {title}
                </div>
                <div style={styles.content}>
                    {this.props.children}
                </div>
            </div>
        )
    }
}

styles = {
    container: {
        border: '1px solid ' + colors.blue,
        fontFamily: 'Lato',
        width: '100%',
        borderRadius: 5,
        marginBottom: 30
    },
	blockTitle: {
        fontSize: fonts.size.medium,
        width: '100%',
        fontWeight: 'bold',
        padding: 10,
        color: colors.white,
        backgroundColor: colors.blue
    },    
    content: {
        padding: 4, 
        width: '100%'
    },
};

export default FilterSidebar;
