import React from 'react';
import PureComponent, { pure } from '../PureComponent'
import { colors, metrics, fonts } from '../../../theme';

let styles

const Title = ({ children, filterMax, selectedFilter, open, toggleOpen }) => (
  <div style={styles.titleContainer} onClick={toggleOpen}>
    <h2 style={styles.title}>{children}</h2>
    <div style={styles.rightSide}>
        {typeof selectedFilter !== 'undefined' && typeof filterMax !== 'undefined' && 
            <div style={{fontSize: 18, marginRight: 5, color: colors.darkGray}}>{selectedFilter + '/' + filterMax}</div>
        }
        <span style={open ? styles.triangleOpen : styles.triangle} />
    </div>
  </div>)

class FilterMenu extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
           listIsOpen: true,
        }
    }

    componentDidMount = () => {
  
    }

    toggleList = () => {
        this.setState({listIsOpen: !this.state.listIsOpen})
    }

    render() {
        const { title, filterMax, filterLength, scroll } = this.props

        return(
            <div style={styles.menu_container}>
                <Title 
                    filterMax={filterMax}
                    selectedFilter={filterLength}
                    open={this.state.listIsOpen}
                    toggleOpen={this.toggleList}
                >
                    {title}
                </Title>
                {this.state.listIsOpen && 
                    <div style={scroll ? {...styles.localFilterContainer, maxHeight: 300, overflowY: 'scroll'} : styles.localFilterContainer}>
                        {this.props.children}
                    </div>
                }
            </div>
        )
    }
}

styles = {
	localFilterContainer: {
        borderBottom: '1px solid ' + colors.blue,
        borderLeft: '1px solid ' + colors.blue,
        borderRight: '1px solid ' + colors.blue,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        margin: '0 5px',
        boxShadow: 'rgba(0, 0, 0, 0.12) 0px 0px 4px 0px',
    },
    triangle: {
        width: 0,
        height: 0,

        transition: 'border 100ms',
        transitionOrigin: 'left',

        color: colors.blue,

        cursor: 'pointer',

        borderLeft: '8px solid transparent',
        borderRight: '8px solid transparent',
        borderTop: `8px solid ${colors.blue}`,
    },
    triangleOpen: {
        width: 0,
        height: 0,

        transition: 'border 100ms',
        transitionOrigin: 'left',

        color: colors.blue,

        cursor: 'pointer',

        borderLeft: '8px solid transparent',
        borderRight: '8px solid transparent',
        borderBottom: `8px solid ${colors.blue}`,
    },
    title: {
        fontSize: 18,
        fontWeight: 'bold',
        color: colors.blue,
        marginRight: 5
    },
    titleContainer: {
        position: 'relative',
        display: 'flex',
        alignItems: 'center',
        padding: '7px 5px',
        width: '100%',
        justifyContent: 'space-between',
        backgroundColor: colors.blueLighter, 
        border: '1px solid ' + colors.gray,
        cursor: 'pointer',
        boxShadow: 'rgba(0, 0, 0, 0.12) 0px 0px 4px 0px',
        borderRadius: 3
    },
    menu_container: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        marginBottom: 5
    },
    rightSide: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    }
};

export default FilterMenu;
