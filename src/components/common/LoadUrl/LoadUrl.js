import React, { Component } from 'react';
import Relay from 'react-relay';
import Radium from 'radium'

import Footer from '../Footer/Footer.js'
import Loading from '../Loading/Loading.js'
import colors from '../../../theme/colors'

let styles;

class LoadUrl extends Component {

  render() {
    const { viewer } = this.props

    return (
      <div style={styles.container}>
        {<Loading />}
        {viewer && viewer.me ?
          <Footer onUpdateLanguage={this._resetState} viewer={viewer} user={viewer.me}/> :
          <Footer onUpdateLanguage={this._resetState} viewer={viewer ? viewer : null} user={null}/> }
      </div>

    );
  }
}

styles = {
    container: {
    }
}

export default Relay.createContainer(Radium(LoadUrl), {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        ${Footer.getFragment('viewer')}
        me {
            id
        }
      }
    `
  },
});