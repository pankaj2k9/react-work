import React from 'react';
import Relay from 'react-relay';
import { Link } from 'react-router';
import withRouter from 'react-router/lib/withRouter'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Radium from 'radium';
import { browserHistory } from 'react-router';

import * as types from '../../../actions/actionTypes.js';
import { styles } from './styles'
import AuthorizedContent from './AuthorizedContent';
import UnauthorizedContent from './UnauthorizedContent';

import constants from "../../../../constants";
import localizations from '../../Localizations'

class Header extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount = () => {
		this.props.relay.forceFetch({}, 
			readyState => {
				if (readyState.done) {
          if (this.props.location.pathname === '/' && this.props.viewer && this.props.viewer.me && this.props.viewer.me.id && !this.props.isWebSiteOpened) {
            if (this.props.viewer.me.homePagePreference === 'ORGANIZED')
              browserHistory.push(`/my-events`)
          }
          this.props._updateOpenedWebSite(true);
				}
			}
		)
	}

  render() {
    const { viewer, user } = this.props
    return (
      <div>
        <div style={user ? styles.loggedInContainer : styles.container}>
          <div style={user ? styles.loggedInLogo : styles.logo}>
            <Link to='/' style={styles.logoLink}>
              <img src="/assets/images/logo@3x.png"
                alt="Sportunity.com"
                style={styles.logoImg}
                title='Sportunity.com'
                />
            </Link>
          </div>
          {
            user
            ? <AuthorizedContent user={this.props.user} viewer={viewer} {...this.props}/>
            : <UnauthorizedContent {...this.props} viewer={viewer} />
          }
        </div>
        <div style={styles.download_icons}>
          <span style={styles.download_icons_text}>
            {localizations.home_download_app_text}
          </span>
          <div style={{display: 'flex', justifyContent:'center',flexDirection:'row', marginTop: 7}}>
            <div style={{width:'50%'}}>
              <a target="_blank" href={constants.appLinkAppStore}>
                <img style={{width:"75%"}} src="/assets/images/icon_appstore.png"/>
              </a>
            </div>
            <div style={{width:'50%'}}>
              <a target="_blank" href={constants.appLinkPlayStore}>
                <img style={{width:"75%"}} src="/assets/images/icon_playstore.png"/>
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const _updateOpenedWebSite = (value) => ({
  type: types.GLOBAL_SET_SITE_OPENED,
  value
})

const dispatchToProps = (dispatch) => ({
  _updateOpenedWebSite: bindActionCreators(_updateOpenedWebSite, dispatch),
})

const stateToProps = (state) => ({
  isWebSiteOpened: state.globalReducer.isWebSiteOpened,  
})

const ReduxContainer = connect(
  stateToProps,
  dispatchToProps,
)(Radium(Header));

export default Relay.createContainer(withRouter(Radium(ReduxContainer)), {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
				me {
          id
          profileType
          homePagePreference
          ${AuthorizedContent.getFragment('user')}
        }
        ${AuthorizedContent.getFragment('viewer')},
      }
    `,
    user: () => Relay.QL`
      fragment on User {
        id
        ${AuthorizedContent.getFragment('user')},
      }
    `,
  },
});
