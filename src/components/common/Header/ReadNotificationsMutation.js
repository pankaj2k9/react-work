import Relay from 'react-relay';

export default class NotificationMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      readNotifications
    }`;
  }

  getVariables() {
    return {}
  }

  getFatQuery() {
    return Relay.QL`
      fragment on readNotificationsPayload {
        clientMutationId,
        user {
          numberOfUnreadNotifications
          notifications
        }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        user: this.props.user.id,
      },
    }];
  }

  static fragments = {
    user: () => Relay.QL`
      fragment on User {
        id,
        numberOfUnreadNotifications
      }
    `,
  };

}
