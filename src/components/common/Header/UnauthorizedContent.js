import React from 'react';
import { Link } from 'react-router';
import { colors, metrics } from '../../../theme';
import localizations from '../../Localizations'
import Radium from 'radium'

let RadiumLink = Radium(Link);

let styles;


class UnauthorizedContent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isMobileMenuOpen: false,
      width: '0', 
      height: '0'
    }
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
  }

	componentWillReceiveProps =nextProps => {
		if (this.state.width !== window.innerWidth)
			this.updateWindowDimensions()
	}

  componentDidMount() {
    this.updateWindowDimensions();
    if (typeof window !== 'undefined')
      window.addEventListener('resize', this.updateWindowDimensions);
  }

  componentWillUnmount() {
    if (typeof window !== 'undefined')
      window.removeEventListener('resize', this.updateWindowDimensions);
  }

  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }

  _toggleMobileMenu = () => {
    this.setState({isMobileMenuOpen: !this.state.isMobileMenuOpen})
  }

  _getActivePage = () => {
    if (this.props.location.pathname.indexOf('circle') >= 0) 
      return 'circles';
    if (this.props.location.pathname.indexOf('find') >= 0)
      return 'find'
    if (this.props.location.pathname.indexOf('new-sportunity') >= 0)
      return 'new-sportunity'
    if (this.props.location.pathname.indexOf('event') >= 0)
      return 'events';
    if (this.props.location.pathname.indexOf('blog') >= 0)
      return 'blog';
    if (this.props.location.pathname.indexOf('venue') >= 0)
      return 'venue';
  }

  render() {
    let leftStyle = this.state.width > 500
      ? styles.left
      : this.state.isMobileMenuOpen
        ? styles.left
        : styles.leftHidden

    let activePage = this._getActivePage();

    return (
      <div style={styles.container}>
        {(this.state.width > 500 || this.state.isMobileMenuOpen) &&
          <div style={{...leftStyle}}>
            <RadiumLink style={activePage === 'find' ? styles.activeLink : styles.link} to="/find-sportunity">{localizations.header_findSportunities}</RadiumLink>
            <RadiumLink style={activePage === 'new-sportunity' ? styles.activeLink : styles.link} to="/new-sportunity">{localizations.header_organizeSportunities}</RadiumLink>
            <RadiumLink style={activePage === 'blog' ? styles.activeLink : styles.link} to="/blog">{localizations.blog_link}</RadiumLink>
            <RadiumLink style={activePage === 'circles' ? styles.activeLink : styles.link} to="/my-circles">{localizations.header_find_circles}</RadiumLink>
          </div>
        }
        <div style={styles.right}>
          {/*<div style={styles.clubs}>
            
          </div>*/}
          <div style={styles.register}>
            <RadiumLink style={styles.link} to="/register">{localizations.header_register}</RadiumLink>
          </div>
          <div style={styles.login}>
            <RadiumLink style={styles.link} to="/login">{localizations.header_login}</RadiumLink>
          </div>
        </div>
        <div style={styles.mobileMenu} onClick={this._toggleMobileMenu}>
          <i className="fa fa-bars fa-2x" />
        </div>
      </div>
    );
  }
}

styles = {
  container: {
    flexGrow: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    fontFamily: 'Lato',
    '@media (min-width: 321px) and (max-width: 480px)': {
      textAlign: 'center',
    },
    '@media (max-width: 320px)': {
      display: 'flex'
    },
  },
  register: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: colors.white,
    marginLeft: metrics.margin.small,
    borderLeft: metrics.border.tiny,
    borderLeftStyle: 'solid',
    borderColor: colors.white,
    height: 62,
    // lineHeight: '62px',
    '@media (max-width: 480px)': {
      //display: 'inline-block',
      //height: 40,
      marginLeft: 0,
      border: 'none',
      lineHeight: '20px',
      flex: 1
    },
    '@media (max-width: 320px)': {
      height: 55
    },
  },
  login: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: colors.white,
    marginLeft: 0,
    borderLeft: metrics.border.tiny,
    borderLeftStyle: 'solid',
    borderColor: colors.white,
    height: 62,
    // lineHeight: '62px',
    '@media (max-width: 480px)': {
      //display: 'inline-block',
      //height: 40,
      border: 'none',
      lineHeight: '20px',
      flex: 1
    },
    '@media (max-width: 320px)': {
      height: 55
    },
  },
  clubs: {
    display: 'flex',
    alignItems: 'center',
    fontSize: 19,
    marginRight: 75,
    '@media (max-width: 730px)': {
      //fontSize: 18,
      marginRight: 0
    },
    '@media (max-width: 500px)': {
      fontSize: 16
    },
    '@media (max-width: 480px)': {
      justifyContent: 'center',
      //fontSize: 16,
      flex: 1
    }
  },
  link: {
    color:colors.white,
    textDecoration:'none',
    paddingLeft: 20,
    paddingRight: 20,
    // fontSize: 14,
    textAlign: 'center',
    '@media (max-width: 650px)': {
      paddingLeft: 15,
      paddingRight: 15
    },
    '@media (max-width: 580px)': {
      paddingLeft: 7,
      paddingRight: 7,
    },
    '@media (max-width: 320px)': {
      //display: 'none',
      fontSize: 15
    }
  },
  activeLink: {
    color:colors.white,
    textDecoration:'none',
    paddingLeft: 20,
    paddingRight: 20,
    // fontSize: 14,
    textAlign: 'center',
    backgroundColor: '#85C2FF',
    padding: '10px 13px',
    borderRadius: 5, 
    '@media (max-width: 650px)': {
      paddingLeft: 15,
      paddingRight: 15
    },
    '@media (max-width: 580px)': {
      paddingLeft: 7,
      paddingRight: 7,
    },
    '@media (max-width: 320px)': {
      //display: 'none',
      fontSize: 15
    }
  },
  header_button: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',

    border: '1px solid #FFFFFF',
    color: colors.blue,
    borderRadius: '3px',
    fontSize: 14,
    fontWeight: 'bold',
    textDecoration: 'none',
    backgroundColor: colors.white,
    width: 140,
    height: 34,
    marginRight: 20,
    cursor: 'pointer',
    '@media (max-width: 730px)': {
      width: 120, 
    },
    '@media (max-width: 480px)': {
      width: 100,
      fontSize: 12,
      display: 'inline-block',
      lineHeight: '34px',
    },
    '@media (max-width: 320px)': {
       marginRight: 4,
    }
  },

  left:{
    display: 'flex',
    alignItems: 'center',
    fontSize: 19,
    '@media (max-width: 660px)': {
      fontSize: 18
    },
    '@media (max-width: 500px)': {
      fontSize: 16,
      position: 'absolute',
      width: '100%',
      top: 60,
      left: 0,
      flexDirection: 'column',
      backgroundColor: colors.blue,
      borderTop: '1px solid '+ colors.white,
      zIndex: 1,
      justifyContent: 'space-between',
      padding: 8,
      height: '15vh',
    }
  },
  leftHidden: {
    height: 0,
  },
	mobileMenu: {
		display: 'none',
		'@media (max-width: 500px)': {
			display: 'flex',
			color: colors.white,
			flex: 1,
			justifyContent: 'center',
			cursor: 'pointer'
		}
	},
  right: {
    display: 'flex',
    fontSize: 14,
    '@media (max-width: 480px)': {
      justifyContent: 'center',
      fontSize: 18
    },
    '@media (max-width: 320px)': {
      flex: 7
    }
  }
};

export default Radium(UnauthorizedContent);
