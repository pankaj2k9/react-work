import React from 'react'
import Relay from 'react-relay'
import withRouter from 'react-router/lib/withRouter'
import { Link } from 'react-router'
import { colors } from '../../../theme'
import moment from 'moment'
import ReactLoading from 'react-loading'
import { connect } from 'react-redux';

import TutorialModal from '../Tutorial/index.js'
import ReadNotificationsMutation from './ReadNotificationsMutation'
import ReadNotificationMutation from './ReadNotificationMutation'
import RelayStore from '../../../RelayStore.js'
import localizations from '../../Localizations'
import SubAccounts from './SubAccounts'
import Circle from './Circle'
import Loading from '../Loading/Loading'


import Radium from 'radium'

let RadiumLink = Radium(Link);

let styles;

const truncateStr = (str, limit) => !str || str.length<=limit?
  str :
  str.substring(0, limit-3)+'...'

class AuthorizedContent extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        popup_profile_visible: false,
        popup_profile_isactive: false,
        popup_notification_visible: false,
        popup_notification_isactive: false,
        popup_chat_visible: false,
        popup_chat_isactive: false,
        isMobileMenuOpen: false,
        width: '0', 
        height: '0',
        tutorial1IsVisible: false,
        tutorial2IsVisible: false,
        tutorial3IsVisible: false,
        tutorial6IsVisible: false,
        hoverCircle: false
      }
      
      this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
      // this._hidePopups = this._hidePopups.bind(this);
    }

    componentDidMount() {
      // Hide dropdown block on click outside the block
      window.addEventListener('click', this._hidePopups);
      this.updateWindowDimensions();

      if (typeof window !== 'undefined')
        window.addEventListener('resize', this.updateWindowDimensions);

      this.props.relay.setVariables({delayedQuery: true});
	    const superToken = localStorage.getItem('superToken');
	    const userToken = localStorage.getItem('userToken');
	    if (superToken) {
		    this.setState({
			    superToken,
			    userToken
		    })
		    this.props.relay.setVariables({
			    superToken,
			    query: true,
			    userToken,
		    },readyState => {
          if (readyState.done) {
            setTimeout(() =>
              this.shouldDisplayTutorials()
            , 50);
          }
        })
	    }
    }

    componentWillUnmount() {
      // Remove click event listener on component unmount
      window.removeEventListener('click', this._hidePopups);
      window.removeEventListener('resize', this.updateWindowDimensions);
    }

    componentWillReceiveProps =nextProps => {
      if (this.props.location.pathname !== nextProps.location.pathname) {
        this.setState({hoverCircle: false})
      }
      if (this.state.width !== window.innerWidth)
        this.updateWindowDimensions()
    }

    updateWindowDimensions() {
      this.setState({ width: window.innerWidth, height: window.innerHeight });
    }

    shouldDisplayTutorials() {
      if (this.props.viewer && this.props.viewer.me && this.props.viewer.me.id && this.props.viewer.superMe && (this.props.viewer.superMe.profileType === 'BUSINESS' || this.props.viewer.superMe.profileType === 'ORGANIZATION')) {
        if (this.props.shownTutorial.indexOf(1) < 0)
          this.setState({
            tutorial1IsVisible: true
          })

        if (this.props.shownTutorial.indexOf(2) < 0)
          this.setState({
            tutorial2IsVisible: true
          })

        if (this.props.shownTutorial.indexOf(3) < 0)
          this.setState({  
            tutorial3IsVisible: true
          })

        if (this.props.shownTutorial.indexOf(7) < 0)
          this.setState({
            tutorial6IsVisible: true
          })
      }      
    }

    _toggleMobileMenu = () => {
      this.setState({isMobileMenuOpen: !this.state.isMobileMenuOpen})
    }

    _readNotification = (event) => {
      if (event)
        event.preventDefault();
      const viewer = this.props.viewer
      RelayStore.commitUpdate(

        new ReadNotificationsMutation({ viewer, user: this.props.user }),
        {
          onFailure: error => {

          },
          onSuccess: (response) => {

          },
        }
      );
    }

    _onNotificationClick = (notification) => {
      const viewer = this.props.viewer
      RelayStore.commitUpdate(

        new ReadNotificationMutation({ viewer, user: this.props.user, notificationIdVar: notification.id }),
        {
          onFailure: error => {

          },
          onSuccess: (response) => {

          },
        }
      );
      let to = notification.notificationType === 'circleAskedInfo' 
        ? '/my-shared-info' 
        : notification.notificationType === 'circle' 
          ? notification.link && '/circle/' + notification.link 
          : notification.link && '/event-view/'+ notification.link ;

      if (to) {
        this.props.router.push({
          pathname : to,
        })
      }
    }

    _toggleProfilePopup = () => {
      this.setState({
        popup_profile_visible: !this.state.popup_profile_visible
      })
    }

    _toggleNotificationPopup() {
      this.setState({
        popup_notification_visible: !this.state.popup_notification_visible,
      })
      if (!this.props.relay.variables.queryNotifications)
        this.props.relay.setVariables({
          queryNotifications: true
        })

    }

    _toggleChatPopup() {
      this.setState({
        popup_chat_visible: !this.state.popup_chat_visible
      })
      if (!this.props.relay.variables.queryChats)
        this.props.relay.setVariables({
          queryChats: true
        })
    }

    _hidePopups = (event) => {
      if (!this || !(
          this._containerProfileNode &&
          this._containerNotificationNode &&
          this._containerChatNode
        )
      ) return false

      if (!this._containerProfileNode.contains(event.target)) {
        this.setState({
          popup_profile_visible: false,
        })
      }
      if (!this._containerNotificationNode.contains(event.target) && (!this._readAllNode || (this._readAllNode && !this._readAllNode.contains(event.target)))) {
        this.setState({
          popup_notification_visible: false,
        })
      }
      if (!this._containerChatNode.contains(event.target)) {
        this.setState({
          popup_chat_visible: false,
        })
      }
    }

    _handleProfileFocus() {
      this.setState({ popup_profile_isactive:true })
    }

    _handleProfileBlur = (e) => {
      this.setState({
        popup_profile_isactive: false,
        popup_profile_visible: false,
      })
    }

    _handleNotificationFocus() {
      this.setState({ popup_notification_isactive:true })
    }

    _handleNotificationBlur() {
      this.setState({
        popup_notification_isactive: false,
        popup_notification_visible: false,
      })
    }

    _handleChatPopupFocus() {
      this.setState({ popup_chat_isactive: true })
    }

    _handleChatPopupBlur() {
      this.setState({
        popup_chat_isactive: false,
        popup_chat_visible: false
      })
    }

    _handleClick(e) {this.props.router.push({
      pathname : e,
    })}

    _timesBefore = (date1) => {
      return moment(date1).fromNow();
    }

    _filterChats = (chats) => {
      if (chats) {
        let filteredChats = chats.edges.filter(chat => {
          return (chat.node.messages.edges.length > 0)
        })
        return filteredChats;
      }
      else return []
    }

    _sortChats = (chats) => {
      if (chats) {
        return chats.sort((chatA, chatB) => {
          if (chatA.node.messages.edges.length > 0 && chatB.node.messages.edges.length > 0) {
            if (!chatA.node.read && !chatB.node.read) {
              if (new Date(chatB.node.messages.edges[0].node.created) - new Date(chatA.node.messages.edges[0].node.created) > 0)
                return 1
              else
                return -1
            }
            else if (!chatA.node.read)
              return -1;
            else if (!chatB.node.read)
              return 1;
            else if (new Date(chatB.node.messages.edges[0].node.created) - new Date(chatA.node.messages.edges[0].node.created) > 0)
              return 1
            else
              return -1
          }
          else if (chatA.node.messages.edges.length > 0)
            return -1 ;
          else return 1;

        })
      }
      else return []
    }

    _transformChats = (chats, me) => {
      if (chats) {
        chats = chats.map(edge => {
          let title ;
          if (edge.node.sportunity)
            title = edge.node.sportunity.title ;
          else if (edge.node.circle)
            title = edge.node.circle.name; 
          else { 
            edge.node.users.forEach(user => {
              if (user.id !== me.id) {
                if (title)
                  title = title + ', ' + user.pseudo
                else 
                  title = user.pseudo 
              }
            })
          }
          return {
            title,
            lastAuthor: me.id === edge.node.messages.edges[0].node.author.id ? localizations.header_you : edge.node.messages.edges[0].node.author.pseudo, 
            lastMessageText: edge.node.messages.edges[0].node.text.length > 50 
              ? edge.node.messages.edges[0].node.text.substr(0, 50) + '...'
              : edge.node.messages.edges[0].node.text,
            lastMessageDate: edge.node.messages.edges[0].node.created,
            isChatRead: edge.node.read,
            link: this._getChatLink(edge.node),
            key: edge.node.id
          }
        })
        return chats
      }
      else return []
    }

    _getChatLink = (chat) => {
      let link = '';
      if (chat.sportunity)
        link = '/event-view/'+chat.sportunity.id+'#chat'
      else if (chat.circle) 
        link = '/circle/'+chat.circle.id+'#chat'
      else {
        chat.users.forEach(user => {
          if (user.id !== this.props.viewer.me.id)
            link = '/profile-view/'+user.id+'#chat';
        });
      }
      return link ;
    }

    _forecedReload = () => {
      this.setState({
        isReloading: true,
      })
    }

    _getActivePage = () => {
      if (this.props.location.pathname.indexOf('circle') >= 0) 
        return 'circles';
      if (this.props.location.pathname.indexOf('find') >= 0)
        return 'find'
      if (this.props.location.pathname.indexOf('new-sportunity') >= 0)
        return 'new-sportunity'
      if (this.props.location.pathname.indexOf('event') >= 0)
        return 'events';
      if (this.props.location.pathname.indexOf('blog') >= 0)
        return 'blog';
      if (this.props.location.pathname.indexOf('venue') >= 0)
        return 'venue';
    }

    render() {
      const { viewer, user } = this.props;
      const { notifications, numberOfUnreadNotifications, unreadChats, chats } = user;

      if (this.state.isReloading) {
        return (<div style={styles.fullscreen}><Loading /></div>)
      }

      let filteredChats = this._filterChats(chats);
      filteredChats = this._sortChats(filteredChats);
      filteredChats = this._transformChats(filteredChats, user);

      let leftStyle = this.state.width > 500
      ? styles.navigation
      : this.state.isMobileMenuOpen
        ? styles.navigation
        : styles.leftHidden

      let subAccountsUnread = 0;
      viewer.superMe && viewer.superMe.subAccounts.map(({numberOfUnreadNotifications, unreadChats}) => {
        subAccountsUnread += numberOfUnreadNotifications + unreadChats;
      });
      viewer.authorizedAccounts && viewer.authorizedAccounts.accounts.map(({numberOfUnreadNotifications, unreadChats}) => {
        subAccountsUnread += numberOfUnreadNotifications + unreadChats;
      });

      let activePage = this._getActivePage();
      
      return (
        <div style={styles.container}>
          {(this.state.width > 500 || this.state.isMobileMenuOpen) && 
            <nav style={{...leftStyle}}>
              <div style={activePage === 'find' ? styles.activeLinkContainer : styles.linkContainer} >
                <RadiumLink style={styles.link} to="/find-sportunity">{localizations.header_findSportunities}</RadiumLink>
              </div>

              <div style={activePage === 'new-sportunity' ? styles.activeLinkContainer : styles.linkContainer} onClick={() => this.setState({tutorial6IsVisible: false})}>
                <RadiumLink style={styles.link} to="/new-sportunity">{localizations.header_organizeSportunities}</RadiumLink>
                <TutorialModal
                  isOpen={!this.state.popup_profile_visible && !this.state.tutorial1IsVisible && !this.state.tutorial2IsVisible && !this.state.tutorial3IsVisible && this.state.tutorial6IsVisible}
                  tutorialNumber={7}
                  tutorialName={"team_small_tutorial6"}
                  message={localizations.team_small_tutorial6}
                  confirmLabel={localizations.team_small_tutorial_ok}
                  onPass={() => this.setState({tutorial6IsVisible: false})}
                  position={{
                    top: 40,
                    left: -95
                  }}
                  arrowPosition= {{
                    top: -8,
                    left: 130
                  }}
                />
              </div>

              <div style={activePage === 'events' ? styles.activeLinkContainer : styles.linkContainer}>
                <RadiumLink style={styles.link} to="/my-events">{localizations.header_mySportunities}</RadiumLink>
              </div>
              
              {(this.state.width >= 550 || this.state.width <= 500) && (user.userCanManageVenue) && 
                <div style={activePage === 'venue' ? styles.activeLinkContainer : styles.linkContainer}>
                  <RadiumLink style={styles.link} to="/manage-venue">{localizations.header_manageMyVenue}</RadiumLink>
                </div>
              }

              <div 
                style={activePage === 'circles' ? styles.activeLinkContainer : styles.linkContainer} 
                onClick={() => this.setState({tutorial3IsVisible: false})}
                onMouseEnter={() => {this.setState({hoverCircle: true})}} 
                onMouseLeave={() => {setTimeout(() => this.setState({hoverCircle: false}), 50)}}>
                <RadiumLink style={styles.link} to="/my-circles">
                  {user.profileType === 'PERSON'
                  ? localizations.header_my_community
                  : localizations.header_my_teams
                  }
                </RadiumLink>
                {this.state.hoverCircle && 
                <div style={styles.dropdownMenuContainer}>
                  <ul style={styles.dropdownMenu}>
	                  {user &&
                      <RadiumLink to="/my-circles"
                        style={this.props.activeSection === 'myCircles' ? styles.menuActive : styles.menu}
                      >
                        {localizations.circles_title}
                      </RadiumLink>
                    }
                    {user && (user.circles && user.circles.edges && user.circles.edges.length > 0) &&
                      <RadiumLink to="/my-circles/all-members"
                        style={this.props.activeSection === 'allMembers' ? styles.menuActive : styles.menu}
	                      >
		                    {localizations.circles_allMembers}
	                    </RadiumLink>
	                  }
	                  {user && user.profileType !== 'PERSON' && ((user.circles && user.circles.edges && user.circles.edges.length > 0) || (user.circlesSuperUser && user.circlesSuperUser.edges && user.circlesSuperUser.edges.length > 0)) &&
	                    <RadiumLink to="/my-circles/members-info" 
                        style={this.props.activeSection === 'membersInfo' ? styles.menuActive : styles.menu}
	                      >
		                    {localizations.circles_information}
                      </RadiumLink>
	                  }
	                  {user && user.profileType !== 'PERSON' && ((user.circles && user.circles.edges && user.circles.edges.length > 0) || (user.circlesSuperUser && user.circlesSuperUser.edges && user.circlesSuperUser.edges.length > 0)) &&
                      <RadiumLink to="/my-circles/payment-models" 
                        style={this.props.activeSection === 'paymentModels' ? styles.menuActive : styles.menu}
                      >
		                    {localizations.circles_paymentModels}
                      </RadiumLink>
	                  }
	                  {user && user.profileType !== 'PERSON' && ((user.circles && user.circles.edges && user.circles.edges.length > 0) || (user.circlesSuperUser && user.circlesSuperUser.edges && user.circlesSuperUser.edges.length > 0)) &&
	                    <RadiumLink to="/my-circles/terms-of-uses"  
                        style={this.props.activeSection === 'termOfUse' ? styles.menuActive : styles.menu} 
                        >
		                    {localizations.circles_termOfUse}
                      </RadiumLink>
	                  }
                    {/*user &&
	                  <div style={this.props.activeSection === 'myCircles' ? styles.menuActive : styles.menu}
	                       onClick={this.props.onChangeSection.bind(this, 'myCircles')}>
		                  {localizations.circles_title}
	                  </div>
                    }
	                  {user && (user.circles && user.circles.edges && user.circles.edges.length > 0) &&
	                  <li style={this.props.activeSection === 'allMembers' ? styles.menuActive : styles.menu}
	                      onClick={this.props.onChangeSection.bind(this, 'allMembers')}>
		                  {localizations.circles_allMembers}
	                  </li>
	                  }
	                  {user && user.profileType !== 'PERSON' && ((user.circles && user.circles.edges && user.circles.edges.length > 0) || (user.circlesSuperUser && user.circlesSuperUser.edges && user.circlesSuperUser.edges.length > 0)) &&
	                  <li style={this.props.activeSection === 'membersInfo' ? styles.menuActive : styles.menu}
	                      onClick={this.props.onChangeSection.bind(this, 'membersInfo')}>
		                  {localizations.circles_information}
	                  </li>
	                  }
	                  {user && user.profileType !== 'PERSON' && ((user.circles && user.circles.edges && user.circles.edges.length > 0) || (user.circlesSuperUser && user.circlesSuperUser.edges && user.circlesSuperUser.edges.length > 0)) &&
	                  <li style={this.props.activeSection === 'paymentModels' ? styles.menuActive : styles.menu}
	                      onClick={this.props.onChangeSection.bind(this, 'paymentModels')}>
		                  {localizations.circles_paymentModels}
	                  </li>
	                  }
	                  {user && user.profileType !== 'PERSON' && ((user.circles && user.circles.edges && user.circles.edges.length > 0) || (user.circlesSuperUser && user.circlesSuperUser.edges && user.circlesSuperUser.edges.length > 0)) &&
	                  <li style={this.props.activeSection === 'termOfUse' ? styles.menuActive : styles.menu} onClick={this.props.onChangeSection.bind(this, 'termOfUse')}>
		                  {localizations.circles_termOfUse}
	                  </li>
	                  */}
                  </ul>
                  </div>
                }
                <TutorialModal
                  isOpen={!this.state.popup_profile_visible && !this.state.tutorial1IsVisible && !this.state.tutorial2IsVisible && this.state.tutorial3IsVisible}
                  tutorialNumber={3}
                  tutorialName={"team_small_tutorial3"}
                  message={localizations.team_small_tutorial3}
                  confirmLabel={localizations.team_small_tutorial_ok}
                  onPass={() => this.setState({tutorial3IsVisible: false})}
                  position={{
                    top: 40,
                    right: -115
                  }}
                  arrowPosition= {{
                    top: -8,
                    right: 180
                  }}
                />
              </div>

              {(this.state.width >= 620 || this.state.width <= 500) && 
                <div style={activePage === 'blog' ? styles.activeLinkContainer : styles.linkContainer}>
                  <RadiumLink style={styles.link} to="/blog">{localizations.blog_link}</RadiumLink>
                </div>
              }
            </nav>
          }
          <div style={styles.right}>
            <div style={styles.icons}>
              <div style={styles.notification}
                    tabIndex="-1"
                    ref={node => { this._containerNotificationNode = node; }}
                    >
                { numberOfUnreadNotifications ?
                  <span style={styles.marked}>{numberOfUnreadNotifications}</span> : null
                }
                <img style={styles.icon} src="/assets/images/bell.svg" alt="See notifications" onClick={() => this._toggleNotificationPopup()} />
              </div>
              {this.state.popup_notification_visible
                &&
                (this.props.relay.pendingVariables && this.props.relay.pendingVariables.queryNotifications
                ? <div style={styles.notification_popup}>
                    <ReactLoading type='cylon' color={colors.blue}/>
                  </div>
                : <div style={styles.notification_popup}>
                    <div style={styles.notifications_container}>
                      {notifications && notifications.edges && notifications.edges.length > 0 
                        ? notifications.edges.map((edge, index) =>
                            <div 
                              onClick={() => this._onNotificationClick(edge.node)} 
                              style={edge.node.isRead ? styles.notification_link : styles.unread_notification_link} 
                              key={edge.node.id}>
                              <div style={[styles.notification_item, index === notifications.edges.length - 1 ? {borderBottomWidth: 0} : null]} key={edge.node.id}>
                                <div style={styles.notification_header_row}>
                                  <div style={styles.notification_header_col}>
                                    <div style={styles.notification_title}>
                                      {edge.node.title}
                                    </div>
                                    <div style={edge.node.title ? styles.notification_text : styles.notification_title}>
                                      {edge.node.text}
                                    </div>
                                  </div>
                                  <div style={styles.notification_header_time}>
                                    {this._timesBefore(new Date(edge.node.created)) }
                                  </div>
                                </div>
                                <div style={styles.notification_time}>
                                  {moment(edge.node.created).format('LLLL')}
                                </div>
                              </div>
                            </div>
                          )
                        : <div style={styles.notification_item}>
                              <div style={styles.chatTitle}>
                                {localizations.header_no_notification}
                              </div>
                            </div>
                      }
                    </div>
                      {numberOfUnreadNotifications > 0 && 
                        <div style={styles.readAllButton} ref={node => { this._readAllNode = node; }}>
                          <span style={{cursor: 'pointer'}} onClick={this._readNotification}>
                            {localizations.readAllNotifications}
                          </span>
                        </div>
                      }
                  </div>
                )
              }

              <div style={styles.notification}
                    tabIndex="-1"
                    ref={node => { this._containerChatNode = node; }}
                    >
                { unreadChats ?
                  <span style={styles.marked}>{unreadChats}</span> : null
                }
                {/*<img style={styles.icon} src="/assets/images/bell.svg" alt="See chats" onClick={() => this._toggleChatPopup()} />*/}
                <span onClick={() => this._toggleChatPopup()} style={styles.chatIconContainer}>
                  <i className="fa fa-comment-o" style={styles.chatIcon} aria-hidden="true"></i>
                </span>
              </div>
              {this.state.popup_chat_visible
                &&
                (this.props.relay.pendingVariables && this.props.relay.pendingVariables.queryChats
                ? <div style={styles.notification_popup}>
                    <ReactLoading type='cylon' color={colors.blue}/>
                  </div>
                : <div style={styles.notification_popup}>
                    <div style={styles.notifications_container}>
                      {filteredChats && filteredChats.length > 0 
                      ? filteredChats.map((chat, index) =>
                        <RadiumLink to={chat.link} style={styles.notification_link} key={chat.key}>
                          <div style={[styles.notification_item, index === filteredChats.length - 1 ? {borderBottomWidth: 0} : null]} key={chat.key}>
                            <div style={styles.notification_header_row}>
                              <div style={styles.chatTitle}>
                                {
                                  chat.title
                                }
                              </div>
                              <div style={styles.notification_header_time}>
                                {this._timesBefore(new Date(chat.lastMessageDate)) }
                              </div>
                            </div>
                            <div style={chat.isChatRead ? styles.chat_text : {...styles.chat_text, fontWeight: 'bold'}} >
                              {chat.lastAuthor + ': ' + chat.lastMessageText}
                            </div>
                          </div>
                        </RadiumLink>
                      )
                      : <div style={styles.notification_item}>
                          <div style={styles.chatTitle}>
                            {localizations.header_no_chat}
                          </div>
                        </div>
                      }
                    </div>
                  </div>
                )
              }

              <div
                style={styles.notification}
                ref={node => { this._containerProfileNode = node; }}
                tabIndex="-1"
              >
                
                <TutorialModal
                  isOpen={!this.state.popup_profile_visible && (this.state.tutorial1IsVisible || this.state.tutorial2IsVisible)}
                  tutorialNumber={this.state.tutorial1IsVisible ? 1 : 2}
                  tutorialName={this.state.tutorial1IsVisible ? "team_small_tutorial1" : "team_small_tutorial2"}
                  message={this.state.tutorial1IsVisible ? localizations.team_small_tutorial1 : localizations.team_small_tutorial2}
                  confirmLabel={localizations.team_small_tutorial_ok}
                  onPass={() => {if (this.state.tutorial1IsVisible) {this.setState({tutorial1IsVisible: false})} else {this.setState({tutorial2IsVisible: false})}}}
                  hideLabel={this.state.tutorial1IsVisible ? localizations.team_small_tutorial_hide : null}
                  position={{
                    top: 50,
                    right: 22
                  }}
                  arrowPosition= {{
                    top: -8,
                    right: 5
                  }}
                />
                <div style={styles.userContainer} onClick={this._toggleProfilePopup}>
                  <Circle
                    image={user.avatar || "/assets/images/profile.svg"}
                    name={user.pseudo}
                    style={styles.icon}
                    unreadNotif={subAccountsUnread}
                  />
                  <span style={styles.userPseudo} >{truncateStr(user.pseudo, 18)}</span>
                  {this.state.popup_profile_visible
                  ? <i className="fa fa-angle-up" style={styles.menuArrow} aria-hidden="true"/>
                  : <i className="fa fa-angle-down" style={styles.menuArrow} aria-hidden="true"/>
                  }
                </div>
              </div>

                <ul style={this.state.popup_profile_visible ? styles.profile_popup : styles.hidedPopup}>
                  {this.props.relay.pendingVariables && this.props.relay.pendingVariables.superToken
                  ? <div style={styles.spinnerContainer}><ReactLoading type='cylon' color={colors.white}/></div>
                  : <SubAccounts
                      viewer={viewer}
                      user={user}
                      forceReload={this._forecedReload}
                      isMenuOpen={this.state.popup_profile_visible}
                    />
                  }
                  <li onClick={() => this._handleClick('/profile-view/'+this.props.user.id)} style={styles.popup_link}>
                    <img style={styles.menuIcon} src="/assets/images/profil-01.png" />
                    {localizations.header_menu_profile}
                  </li>
                  <li onClick={() => this._handleClick('/profile-view/'+this.props.user.id+'/statistics')} style={styles.popup_link}>
                    <img style={{...styles.menuIcon, filter: 'brightness(0%)'}} src="/assets/images/statistic_bleu-01.png" />
                    {localizations.header_menu_statistic}
                  </li>
                  <li onClick={() => this._handleClick('/profile-view/'+this.props.user.id+'/event')} style={styles.popup_link}>
                    <img style={{...styles.menuIcon, filter: 'brightness(0%)'}} src="/assets/images/activity_blue-01.png" />
                    {localizations.header_menu_event}
                  </li>
                  <li onClick={() => this._handleClick('/my-wallet')} style={styles.popup_link}>
                    <img style={styles.menuIcon} src="/assets/images/wallet.png" />
                    {localizations.header_menu_wallet}
                  </li>
                  <li onClick={() => this._handleClick('/my-info')} style={styles.popup_link}>
                    <img style={styles.menuIcon} src="/assets/images/settings-01.png" />
                    {localizations.header_menu_settings}
                  </li>
                  <li onClick={() => this._handleClick('/logout')} style={styles.popup_link}>
                    <img style={styles.menuIcon} src="/assets/images/logout-01.png" />
                    {localizations.header_menu_logout}
                  </li>
                </ul>
              
            </div>
          </div>
          <div style={styles.mobileMenu} onClick={this._toggleMobileMenu}>
            <i className="fa fa-bars fa-2x" />
          </div>
        </div>
      )
    }
}

const dispatchToProps = (dispatch) => ({
})

const stateToProps = (state) => ({
  shownTutorial: state.tutorialReducer.shownTutorial,
})

const ReduxContainer = connect(
  stateToProps,
  dispatchToProps,
)(Radium(AuthorizedContent));


export default Relay.createContainer(withRouter(Radium(ReduxContainer)), {
  initialVariables: {
    superToken: null,
    query: false,
    userToken: null,
    queryNotifications: false,
    queryChats: false,
    delayedQuery: false
  },
  fragments: {
    user: () => Relay.QL`fragment on User {
      id
      ${ReadNotificationsMutation.getFragment('user')}
      ${ReadNotificationMutation.getFragment('user')}
      avatar
      pseudo
      unreadChats @include(if: $delayedQuery)
      profileType
      numberOfUnreadNotifications @include(if: $delayedQuery)
      userCanManageVenue
      circles(last: 100) @include(if: $delayedQuery){
        edges {
          node {
            id
            name
            type
          }
        }
      }
      circlesSuperUser(last: 5) @include(if: $delayedQuery){
        edges {
          node {
            id, 
            name 
            owner { 
              pseudo
              avatar
            }
            type
          }
        }
      }
      notifications(first: 20) @include(if: $queryNotifications) {
        edges {
          node {
            id
            title
            text
            link
            created
            notificationType
            isRead
          }
        }
      }
      chats (last:100) @include(if: $queryChats) {
        edges {
          node {
            id
            sportunity {
              id
              title
            }
            circle {
              id,
              name
            }
            read
            users {
              id,
              pseudo
            }
            messages (last: 1)  {
              edges {
                node {
                  text,
                  author {
                    id
                    pseudo
                  }
                  created
                }
              }
            }
          }
        }
      }
    }`,
    viewer: () => Relay.QL`fragment on Viewer {
      id
      me {
        id
        avatar
        profileType
      }
      superMe (superToken: $superToken) @include(if:$query) {
        id
        pseudo
        avatar
        profileType
        isSubAccount
        userPreferences {
          areSubAccountsActivated
        }
        subAccounts{
          id
          pseudo
          avatar
          token
          numberOfUnreadNotifications
          unreadChats
        }
      }

      authorizedAccounts(userToken: $userToken) @include(if: $query) {
        id
        pseudo,
        avatar
        profileType,
        numberOfUnreadNotifications
        unreadChats
        accounts {
          id,
          pseudo,
          avatar,
          token,
          authorization_level
          numberOfUnreadNotifications
          unreadChats
        }
      }
    }`
  }
});


styles = {
  container: {
    display: 'flex',
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'space-between',
    fontFamily: 'Lato',
    lineHeight: 1,
    /*'@media (min-width: 321px) and (max-width: 550px)': {
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'center',
    },*/
    '@media (max-width: 500px)': {
      display: 'flex',
      flex: 4
    },
  },

  dropdownMenuContainer: {
    position: 'absolute',
    top: 'calc(100% - 5px)',
    zIndex: 1000,
    width: 200,
  },

	dropdownMenu: {
    marginTop: 10,
    border: '1px solid ' + colors.blue,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    
    borderRadius: '0px 0px 5px 5px',
    backgroundColor: colors.background,
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
  },

	menu: {
		fontFamily: 'Lato',
		fontSize: 17,
    color: colors.blue,
		padding: '10px 15px 10px 15px',
		//marginLeft: 15,
		cursor: 'pointer',
		position: 'relative',
    width: '100%',
    textDecoration: 'none',
    ':hover': {
		  backgroundColor: '#0002'
    }
	},

	menuActive: {
		fontFamily: 'Lato',
		fontSize: 16,
		color: colors.white,
		padding: '10px 15px',
    borderTop: '1px solid ' + colors.blue,
    borderBottom: '1px solid ' + colors.blue,
    borderLeft: '3px solid ' + colors.blue,
		cursor: 'pointer',
		position: 'relative',
		width: '100%',
    fontWeight: 'bold',
    textDecoration: 'none',
    backgroundColor: colors.blue
	},

  navigation: {
    display: 'flex',
    alignItems: 'center',
    fontSize: 19,
    height: 50, 
    '@media (min-width: 1150px)': {
      width: 850
    },
    '@media (max-width: 1150px)': {
      fontSize: 18,
      width: '70%'
    },
    '@media (max-width: 950px)': {
      fontSize: 15
    },
    '@media (max-width: 990px)': {
      fontSize: 15,
      textAlign: 'center',
      justifyContent: 'space-around',
      //flexWrap: 'warp',
    },
    /*'@media (min-width: 321px) and (max-width: 550px)': {
      //flexWrap: 'wrap',
    },*/
    '@media (max-width: 700px)': {
      fontSize: 13
    },
    '@media (max-width: 500px)': {
      position: 'absolute',
      width: '100%',
      top: 62,
      left: 0,
      flexDirection: 'column',
      backgroundColor: colors.blue,
      borderTop: '1px solid '+ colors.white,
      zIndex: 1,
      justifyContent: 'space-between',
      padding: 8,
      height: '20vh',
    }
  },
  leftHidden: {
    height: 0,
  },
  mobileMenu: {
    display: 'none',
    '@media (max-width: 500px)': {
      display: 'flex',
      color: colors.white,
      flex: 1,
      justifyContent: 'center',
      cursor: 'pointer'
    }
  },

  notification_link: {
    textDecoration: 'none',
    cursor: 'pointer'
  },
  unread_notification_link: {
    textDecoration: 'none',
    cursor: 'pointer',
    backgroundColor: colors.lightGray
  },

  linkContainer: {
    flex: 1, 
    position: 'relative',
    padding: '5px 10px',
    margin: '0px 5px',
    display: 'flex',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    '@media (max-width: 700px)': {
      margin: '0 2px'
    }
  },
  activeLinkContainer: {
    flex: 1, 
    position: 'relative',
    padding: '5px 10px',
    margin: '0px 5px',
    backgroundColor: '#85C2FF',
    borderRadius: 5, 
    display: 'flex',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    '@media (max-width: 700px)': {
      margin: '0 2px'
    }
  },

  link: {
    color: 'white',
    textDecoration: 'none',
    position: 'relative',
  },
  chatIconContainer: {
    display: 'flex',
  },
  chatIcon: {
    fontSize: 32,
    color: colors.white,
    paddingBottom: 5,
    cursor: 'pointer'
  },
  marked: {
    backgroundColor: '#E64131',
    width: 17,
    height: 17,
    position: 'absolute',
    top: -10,
    right: -4,
    borderRadius: '50%',
    color: colors.white,
    textAlign: 'center',
    paddingTop: 3,
    fontWeight: 'bold',
  },

  header_button: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',

    border: '1px solid #FFFFFF',
    color: colors.blue,
    borderRadius: '3px',
    fontSize: 14,
    fontWeight: 'bold',
    textDecoration: 'none',
    backgroundColor: colors.white,

    width: 140,
    height: 34,

    marginRight: 20,

    cursor: 'pointer',
  },

  right:{
    display: 'flex',
    alignItems: 'center',
    '@media (max-width: 551px)': {
      marginRight: 0,
    },
    '@media (max-width: 500px)': {
      flex: 3
    }
  },

  icons: {
    display: 'flex',
    alignItems: 'center',
    '@media (max-width: 990px)': {
      width: '100%',
      //display: 'block',
      justifyContent: 'center',
      textAlign: 'center',
      marginTop: 5
    },
    '@media (max-width:500px)': {
      justifyContent: 'space-around'
    }
  },

  userContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    cursor: 'pointer',
  },
  icon: {
    minWidth: 32,
    minHeight: 32,
    maxWidth: 32,
    maxHeight: 32,
    cursor:'pointer'
  },
  userPseudo: {
    fontSize: 14,
    color: colors.white,
    marginRight: 8,
    marginLeft: 8,
    '@media (max-width: 750px)': {
      display: 'none'
    }
  },
  menuArrow: {
    fontSize: 18,
    color: colors.white,
    fontWeight: 'bold',
    marginBottom: 3,
    '@media (max-width: 750px)': {
      marginLeft: 5
    }
  },
  notification: {
    position: 'relative',
    marginRight: 10,
    '@media (max-width: 990px)': {
      display: 'inline-block',
      margin: '10px 5px',
    },
  },
  profile_popup: {
    // display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-around',
    position: 'absolute',
    top: '62px',
    right: '2px',
    width: '250px',
    // height: '120px',
    backgroundColor: colors.blue,
    zIndex: 1000,
    borderWidth: '1px 0px',
    borderStyle: 'solid',
    borderColor: colors.blue,
    boxShadow: '0 2px 8px 0 rgba(0,0,0,0.48), 0 0 8px 0 rgba(0,0,0,0.24)',
    '@media (max-width: 990px)': {
      right: 10,
      top: 130
    },
    '@media (max-width: 550px)': {
      top: 180
    },
    /*'@media (max-width: 370px)': {
      top: 206
    },*/
    '@media (max-width: 500px)': {
      width: '96%',
      right: 0,
      margin: '0 3px',
      top: 65
    }
  },
  notification_popup: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    // justifyContent: 'space-around',
    position: 'absolute',
    top: '60px',
    right: '10px',
    width: '310px',
    //maxHeight: 500,
    backgroundColor: colors.white,
    zIndex: 1000,
    borderTop: '2px solid ' + colors.blue,
    borderLeft: '2px solid ' + colors.blue,
    borderRight: '2px solid ' + colors.blue,
    borderBottom: '2px solid ' + colors.blue,
    boxShadow: '0 2px 8px 0 rgba(0,0,0,0.48), 0 0 8px 0 rgba(0,0,0,0.24)',
    borderRadius: '5px',
    overflow: 'hidden',
    
    maxHeight: '50vh',
    '@media (max-width: 990px) and (min-width: 321px)': {
      //right: 'calc(50% - 100px)',
      top: '130px',
      maxWidth: '100%'
    },
    '@media (max-width: 550px)': {
      top: '180px'
    },
    /*'@media (max-width: 370px)': {
      top: '210px'
    },*/
    '@media (max-width: 500px)': {
      right: 0, 
      margin: '0 3px',
      width: '98%',
      top: 60
    },
  },
  notifications_container: {
    overflowY: 'scroll',
    overflowX: 'hidden',
  },
  readAllButton: {
    borderTop: '1px solid ' + colors.blue,
    //borderBottom: '2px solid ' + colors.blue,
    height: 50, 
    width: '100%',
    padding: '10px 15px',
    textAlign: 'center',
    fontFamily: 'Lato',
    fontSize: 14,
    color: colors.darkBlue,
  },
  notification_item: {
    width: 300,
    borderBottom: '2px solid ' + colors.blue,
    padding: '10px 15px',
    //backgroundColor: '#FF9999',
  },
  notification_title: {
    fontSize: 16,
    color: colors.black,
    fontWeight: 'bold',
    lineHeight: '22px',
    width: 220,
  },
  notification_text: {
    fontSize: 14,
    color: colors.black,
    lineHeight: '20px',
    width: 220,
  },
  notification_header_row: {
    display: 'flex',
    flexDirection: 'row',
  },
  notification_header_col: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start'
  },
  notification_header_time: {
    fontSize: 12,
    textAlign: 'right',
    top: 5,
    right: 0,
    position: 'relative',
    //backgroundColor: '#99FF99',
    width: 70,
  },
  notification_time: {
    fontSize: 14,
    color: colors.black,
    marginTop: 10,
  },
  notification_location: {
    fontSize: 14,
  },
  chat_text: {
    fontSize: 16,
    color: colors.black,
    lineHeight: '22px',
    width: 220,
  },
  marker: {
    marginRight: 8,
    color: colors.blue,
  },
  popup_link: {
    borderWidth: '1px 2px',
    borderStyle: 'solid',
    borderColor: colors.blue,
    width: '100%',
    height: '100%',
    textAlign: 'center',
    lineHeight: '26px',
    fontFamily: 'Lato',
    fontWeight: 'bold',
    fontSize: 14,
    textDecoration: 'none',
    color: colors.black,
    cursor: 'pointer',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    //justifyContent: 'center',
    marginLeft: 70,
    marginBottom: 8
  },
  menuIcon: {
    height: 24,
    width: 24,
    marginRight: 6,
    filter: 'grayscale(100%)'
  },

  chatTitle: {
    fontSize: 16,
    color: colors.black,
    lineHeight: '22px',
    width: 220,
    fontWeight: 'bold'
  },
  hidedPopup: {
    display: 'none',
  },
  fullscreen: {
    position: 'absolute',
    display: 'block',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  },
  spinnerContainer: {
    display: 'flex',
    justifyContent: 'center'
  }
};
