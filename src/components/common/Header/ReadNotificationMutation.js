import Relay from 'react-relay';

export default class NotificationMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      readNotification
    }`;
  }

  getVariables() {
    return {
        notificationId: this.props.notificationIdVar
    }
  }

  getFatQuery() {
    return Relay.QL`
      fragment on readNotificationPayload {
        clientMutationId,
        user {
          numberOfUnreadNotifications
          notifications
        }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        user: this.props.user.id,
      },
    }];
  }

  static fragments = {
    user: () => Relay.QL`
      fragment on User {
        id,
        numberOfUnreadNotifications
      }
    `,
  };

}
