import { colors } from '../../../theme';

export const styles = {
  container: {
    flex: 1,
    // maxWidth: 1400,
    width: '100%',
    margin: 'auto',
    display: 'flex',
    alignItems: 'center',
    backgroundColor: colors.blue,
    boxShadow: '0 2px 4px 0 rgba(0,0,0,0.24)',
    paddingLeft: 20,
    /*'@media (max-width: 500px)': {
      height: 'auto',
      display: 'block',
      paddingLeft: 10,
      paddingRight: 10,
    },*/
    '@media (max-width: 500px)': {
      display: 'flex'
    }
  },
  loggedInContainer: {
    flex: 1,
    // maxWidth: 1400,
    width: '100%',
    margin: 'auto',
    display: 'flex',
    alignItems: 'center',
    backgroundColor: colors.blue,
    boxShadow: '0 2px 4px 0 rgba(0,0,0,0.24)',
    paddingLeft: 20,
    '@media (max-width: 990px)': {
      height: 'auto',
      display: 'block',
      paddingLeft: 10,
      paddingRight: 10,
    },
    '@media (max-width: 500px)': {
      display: 'flex'
    }
  },
  logo: {
    color: colors.white,
    flexShrink: 0,
    height: 62,
    marginRight: 40,
    lineHeight: '62px',
    '@media (max-width: 850px)': {
      marginRight: 20
    },
    '@media (max-width: 500px)': {
      height: 'auto',
      display: 'block',
      textAlign: 'center',
      marginRight: 0,
    },
  },
  loggedInLogo: {
    color: colors.white,
    flexShrink: 0,
    height: 62,
    marginRight: 40,
    lineHeight: '62px',
    '@media (max-width: 850px)': {
      marginRight: 20
    },
    '@media (max-width: 990px)': {
      height: 'auto',
      display: 'block',
      textAlign: 'center',
      marginRight: 0,
    },
    '@media (max-width: 500px)': {
      flex: 1
    }
  },
  logoImg: {
    maxHeight: 30,
    verticalAlign: 'middle',
  },
  logoLink: {
    height: 62,
    display: 'inline-block',
  },
  download_icons:{
    display: 'none',
    '@media (max-width: 500px)': {
      display: 'block',
      textAlign: 'center',
      paddingTop: 7,
      paddingBottom: 7
    },
  },
  download_icons_text: {
    fontSize: 14,
    fontFamily: 'Lato',
  },
// =======
//     display: 'flex',
//     width: '1440px',
//     height: '62px',
//     backgroundColor: '#5E9FDF',
//     boxShadow: '0 2px 4px 0 rgba(0,0,0,0.24)',
//     margin: 'auto',
//     justifyContent: 'space-between',
//   },
//   container_1: {
//     display: 'flex',
//     flexDirection: 'row',
//     alignItems: 'center',
//     justifyContent: 'flex-start',
//     height: 62,
//     },
//   container_2: {
//     display: 'flex',
//     flexDirection: 'row',
//     alignItems: 'center',
//     justifyContent: 'flex-end',
//
//     height: 62,
//       },
//   logo: {
//     color: colors.white,
//     height: 62,
//     lineHeight: '62px',
//     },
//   logoImg: {
//     maxHeight: 30,
//     verticalAlign: 'middle'
//   },
//   logoLink: {
//     height: 62,
//     display: 'inline-block',
//     paddingLeft: 20
//   },
//   Find_A_Sportunity: {
//     width: '200px',
//     height: '23px',
//     fontFamily: 'Lato',
//     fontSize: '19px',
//     fontWeight: 'bold',
//     lineHeight: '23px',
//     color: '#FFFFFF'
//   },
//   event_border: {
//     display: 'flex',
//     justifyContent: 'center',
//     alignItems: 'center',
//     width: '140px',
//     height: '34px',
//     border: '1px solid #FFFFFF',
//     borderRadius: '3px',
//   },
//   event_text: {
//     width: '113px',
//     height: '17px',
//     fontFamily: 'Lato',
//     fontSize: '14px',
//     fontWeight: '500',
//     lineHeight: '17px',
//     color: '#FFFFFF'
//   },
//   register: {
//     alignItems: 'center',
//     justifyContent: 'center',
//     color: colors.white,
//     marginLeft: metrics.margin.small,
//     borderLeft: metrics.border.tiny,
//     borderLeftStyle: 'solid',
//     borderColor: colors.white,
//     height: 62,
//     lineHeight: '62px'
//   },
//   login: {
//     display: 'flex',
//     alignItems: 'center',
//     justifyContent: 'center',
//     color: colors.white,
//     marginLeft: 0,
//     borderLeft: metrics.border.tiny,
//     borderLeftStyle: 'solid',
//     borderColor: colors.white,
//     height: 62,
//     lineHeight: '62px'
//   },
//   link: {
//     color:colors.white,
//     textDecoration:'none',
//     paddingLeft: 20,
//     paddingRight: 20,
//     fontFamily: 'Lato',
//     fontSize: 14,
//     textAlign: 'center',
//   }
// >>>>>>> sportunity_detail_branch
}
