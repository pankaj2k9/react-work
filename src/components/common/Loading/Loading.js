import React from 'react'
import ReactLoading from 'react-loading'
import { colors } from '../../../theme'

let styles 
const Loading = () => {
  return(
    <div style={styles.container}>
      <img src={'/assets/images/logo-blue@3x.png' } width='120'/>
      <ReactLoading type='cylon' color={colors.blue} />
    </div>
  )
}

styles = {
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'fixed',
    flexDirection: 'column',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    overflow: 'auto',
    background: colors.white,
    zIndex: 1000,
  },
}

export default Loading 