import React from 'react';
import PureComponent, { pure } from '../PureComponent'

import Radium from 'radium';
import moment from 'moment'
import { colors, fonts } from '../../../theme';
import { Link } from 'react-router'
import Relay from 'react-relay'
import SetStatus from './SetStatus'
import IconTint from 'react-icon-tint';
import isSameDay from 'date-fns/is_same_day';
import localizations from '../../Localizations'
import InputCheckbox from "../Inputs/InputCheckbox";

let styles;

const statusStyle = (status) => {
  switch(status) {
		case 'RED':
			return styles.red;
		case 'YELLOW':
			return styles.yellow;
		case 'GREY':
			return styles.lightGreen;
		case 'BLACK':
			return styles.darkGreen;
		case 'GREEN':
			return styles.darkGreen;
		default:
			return styles.lightGreen;
  }
}

const statusColor = (status) => {
	switch(status) {
		case 'RED':
			return styles.colorRed;
		case 'YELLOW':
			return styles.colorYellow;
		case 'GREY':
			return styles.colorLightGreen;
		case 'BLACK':
			return styles.colorDarkGreen;
		case 'GREEN':
			return styles.colorDarkGreen;
		default:
			return styles.colorLightGreen;
  }
}

class Sportunity extends PureComponent {
	constructor(props) {
		super(props)
		this.state = {
			isHover: false,
		}
  }

  componentDidMount = () => {
    if (this.props.userId) {
      this.props.relay.setVariables({
        shouldQueryUserStatus: true,
        sportunityID: this.props.sportunity.id,
        userID: this.props.userId,
      })
    }
  }

	_truncateString = (value, length) => {
		if (value.length <= length) {
			return(value)
		} else {
			return (value.substring(0, length-3)+'...')
		}
	}

  _sportNameTranslated = (sportName) => {
    let name = sportName.EN
    switch(localizations.getLanguage().toLowerCase()) {
      case 'en':
        name = sportName.EN
        break
      case 'fr':
        name = sportName.FR || sportName.EN
        break
      default:
        name = sportName.EN
        break
    }
    return name
  }

  displayLevel = (sport) => {
    if (sport.levels && sport.levels.length > 0)
      sport.levels = sport.levels.sort((a,b)=>{return a.EN.skillLevel - b.EN.skillLevel})

    let sports
    if (sport.allLevelSelected) {
      sports = sport.sport.levels.map(level => this._translatedLevelName(level))
    } else {
      sports = sport.levels.map(level => this._translatedLevelName(level))
    }

    if (sport.allLevelSelected)
      return localizations.event_allLevelSelected
    else if (sports.length === 1) {
      return sports[0]
    } else {
      return sports[0] + ' ' + localizations.find_to +  ' ' + sports[sports.length - 1]
    }
  }

  _translatedLevelName = (levelName) => {
    let translatedName = levelName.EN.name
    switch(localizations.getLanguage().toLowerCase()) {
      case 'en':
        translatedName = levelName.EN.name
        break
      case 'fr':
        translatedName = levelName.FR.name || levelName.EN.name
        break
      case 'it':
        translatedName = levelName.IT.name || levelName.EN.name
        break
      case 'de':
        translatedName = levelName.DE.name || levelName.EN.name
        break
      default:
        translatedName = levelName.EN.name
        break
    }
    return translatedName
  }

	_onHover = () => {
		if (!this.state.isHover) {
			this.setState({
				isHover: true,
			})
		}
    if (typeof this.props.onHover === 'function') {
      this.props.onHover(this.props.sportunity.id)
    }
	}

	_onLeave = () => {
		if (this.state.isHover) {
			this.setState({
				isHover: false,
			})
		}
    if (typeof this.props.onLeave === 'function') {
      this.props.onLeave(this.props.sportunity.id)
    }
	}

  getUserSpecificPrice = (user, sportunity) => {
    if (sportunity.paymentStatus) {
      let index = sportunity.paymentStatus.findIndex(paymentStatus => {
        return paymentStatus.status !== 'Canceled' && user && paymentStatus && paymentStatus.user && user.id === paymentStatus.user.id && paymentStatus.price;
      });

      if (index >= 0)
        return sportunity.paymentStatus[index].price
      else
        return sportunity.price ;
    }
    else
      return sportunity.price;
  }

  _selectEvent = (event) => {
	  /*event.preventDefault();
	  event.stopPropagation();*/
    this.props.selectEvent(this.props.sportunity)
  }

  render() {
		const sportunity = SetStatus(this.props.sportunity, this.props.viewer.sportunityStatus ? this.props.viewer.sportunityStatus : this.props.sportunity.status, this.props.viewer.me ? this.props.viewer.me.id : null);
    const sportunityPrice = this.getUserSpecificPrice(this.props.viewer.me, sportunity);

    let {isSelecting} = this.props

    let organizer = sportunity.organizers.find(organizer => organizer.isAdmin)

	  isSelecting = isSelecting && organizer.organizer.id === this.props.viewer.me.id

    return(
			<div style={styles.link}
      onMouseOver={this._onHover}
					onMouseLeave={this._onLeave}
					>
      <Link to={`/event-view/${sportunity.id}`} style={styles.card}>
        <div style={statusColor(sportunity.color)} />
        <div style={styles.container}>
          <div style={styles.top}>
            <span style={statusStyle(sportunity.color)}>
              {!(sportunity.survey && !sportunity.survey.isSurveyTransformed && sportunity.survey.surveyDates.length > 1) ? sportunity.displayStatus : localizations.status_survey}
            </span>
            {isSameDay(new Date(sportunity.beginning_date), new Date(sportunity.ending_date))
            ? <time style={isSelecting ? {...styles.datetime, marginRight: 20} : styles.datetime}>
                <div style={styles.date}>
                  {moment(new Date(sportunity.beginning_date)).format('ddd DD MMM')}
                </div>
                <div style={styles.time}>
                  {moment(new Date(sportunity.beginning_date)).format('H:mm')} - {moment(new Date(sportunity.ending_date)).format('H:mm')}
                </div>
              </time>
            : <time style={isSelecting ? {...styles.datetime, marginRight: 20} : styles.datetime}>
                <div style={styles.date}>
                    {moment(new Date(sportunity.beginning_date)).format('ddd DD MMM') + ' - '}
                    <div style={styles.time}>
                      {moment(new Date(sportunity.beginning_date)).format('H:mm')}
                    </div>
                </div>
                <div style={styles.date}>
                  {moment(new Date(sportunity.ending_date)).format('ddd DD MMM') + ' - '}
                  <div style={styles.time}>
                    {moment(new Date(sportunity.ending_date)).format('H:mm')}
                  </div>
              </div>
              </time>
            }
          </div>
          <div style={styles.content}>
            <div style={styles.icon} >
							{
								this.state.isHover && !this.props.staticDisplay
								?
								<IconTint width='74' height='74' src={sportunity.sport.sport.logo} color={colors.blue}/>
								:
								<img src={sportunity.sport.sport.logo} style={styles.iconImage}/>
							}
            </div>
            <div style={styles.info}>
              <div style={styles.name}>
								{this._truncateString(sportunity.title, 38)}
							</div>
              <div style={styles.sport}>
                <span style={styles.sportName}>{this._sportNameTranslated(sportunity.sport.sport.name)}</span>
                {!sportunity.sport.allLevelSelected && 
                  <span style={styles.qualification}>&nbsp;-&nbsp;{this.displayLevel(sportunity.sport)}</span>
                }
              </div>
              <div style={styles.location}>
                <i
                  style={styles.marker}
                  className="fa fa-map-marker"
                  aria-hidden="true"
                />
                  <div style={styles.addressContainer}>
                    {sportunity.venue && sportunity.infrastructure ?
                      <span style={{marginBottom: 2}}>
                        {(sportunity.venue.name + ' - ' + sportunity.infrastructure.name).length > 50
                          ? (sportunity.venue.name + ' - ' + sportunity.infrastructure.name).substring(0, 50) + '...'
                          : sportunity.venue.name + ' - ' + sportunity.infrastructure.name
                          }
                      </span>
                      : sportunity.venue && 
                        <span style={{marginBottom: 2}}>
                          {(sportunity.venue.name).length > 50
                            ? (sportunity.venue.name).substring(0, 50) + '...'
                            : sportunity.venue.name
                          }
                        </span>
                    }
                    <span>
                      {(sportunity.address && (sportunity.address.city).length > 50)
                        ? (sportunity.address && sportunity.address.city).substring(0, 50) + '...'
                        : sportunity.address && sportunity.address.city}
                    </span>
                  </div>
              </div>
              {sportunity.organizers
                .filter(organizer => organizer.isAdmin)
                .map((organizer, index) => (
                  <span style={styles.organizerContainer} key={index}>
                    <div style={{...styles.organizerAvatar, backgroundImage: organizer.organizer.avatar ? 'url('+ organizer.organizer.avatar +')' : 'url("https://sportunitydiag304.blob.core.windows.net/avatars/default-avatar.png")'}} />
                    <div style={styles.organizerPseudo}>
                      {organizer.organizer.pseudo}
                    </div>
                    
                    {sportunity.game_information && sportunity.game_information.opponent && sportunity.game_information.opponent.organizer &&
                      <span style={styles.opponentContainer}>  
                        <div style={{...styles.organizerPseudo, marginRight: 5, color: colors.blue}}>
                          {' ' + localizations.against_short}
                        </div>
                        <div style={{...styles.organizerAvatar, backgroundImage: sportunity.game_information.opponent.organizer.avatar ? 'url('+ sportunity.game_information.opponent.organizer.avatar +')' : 'url("https://sportunitydiag304.blob.core.windows.net/avatars/default-avatar.png")'}} />
                        <div style={styles.organizerPseudo}>
                          {sportunity.game_information.opponent.organizer.pseudo}
                        </div>
                      </span>
                    }
                    {sportunity.game_information && sportunity.game_information.opponent && !sportunity.game_information.opponent.organizer && sportunity.game_information.opponent.organizerPseudo &&
                      <span style={styles.opponentContainer}>  
                        <div style={{...styles.organizerPseudo, marginRight: 5, color: colors.blue}}>
                          {' ' + localizations.against_short}
                        </div>
                        <div style={{...styles.organizerAvatar, backgroundImage: 'url("https://sportunitydiag304.blob.core.windows.net/avatars/default-avatar.png")'}} />
                        <div style={styles.organizerPseudo}>
                          {sportunity.game_information.opponent.organizerPseudo}
                        </div>
                      </span>
                    }
                    {sportunity.game_information && sportunity.game_information.opponent && sportunity.game_information.opponent.unknownOpponent && 
                      <span style={styles.opponentContainer}>  
                        <div style={{...styles.organizerPseudo, marginRight: 5, color: colors.blue}}>
                          {' ' + localizations.against_short}
                        </div>
                        <div style={{...styles.organizerAvatar, backgroundImage: 'url("https://sportunitydiag304.blob.core.windows.net/avatars/default-avatar.png")'}} />
                        <div style={styles.organizerPseudo}>
                          {localizations.newSportunity_unknown_opponent_short}
                        </div>
                      </span>
                    }
                    {this.props.sportunity.status.indexOf('Organized') >= 0 && sportunity.game_information && sportunity.game_information.opponent && 
                      (sportunity.game_information.opponent.lookingForAnOpponent || (sportunity.game_information.opponent.invitedOpponents && sportunity.game_information.opponent.invitedOpponents.edges && sportunity.game_information.opponent.invitedOpponents.edges.length > 0)) && 
                      <span style={styles.opponentContainer}>  
                        <div style={{...styles.organizerPseudo, marginRight: 5, color: colors.blue}}>
                          {' ' + localizations.against_short}
                        </div>
                        <div style={{...styles.organizerAvatar, backgroundImage: 'url("https://sportunitydiag304.blob.core.windows.net/avatars/default-avatar.png")'}} />
                        <div style={styles.organizerPseudo}>
                          {localizations.newSportunity_waiting_opponent}
                        </div>
                      </span>
                    }
                  </span>
                ))
              }
            </div>
          </div>
          <div style={styles.bottom}>
            <span style={styles.participants}>
              {sportunity.participants.length} Participant{sportunity.participants.length > 1 ? 's' : ''}
            </span>
            {sportunity.status === 'Past' || sportunity.status === 'Cancelled'
              ? sportunity.score && sportunity.sportunityTypeStatus &&
                <div style={styles.score}>
                  {sportunity.sportunityType.name[localizations.getLanguage().toUpperCase()]}<br/>
                  {sportunity.sportunityTypeStatus.name[localizations.getLanguage().toUpperCase()] + ' '}
                  {sportunity.score.currentTeam + ' - ' + sportunity.score.adversaryTeam}
                </div>
              : <span style={styles.price}>
                  {sportunityPrice.cents === 0
                    ? localizations.event_free
                    : sportunityPrice.currency + ' '+ (sportunityPrice.cents / 100)
                  }
                </span>
            }
          </div>
        </div>
      </Link>
				{isSelecting && this.props.sportunity && 
				<div style={styles.selectEvent}>
					{/*<InputCheckbox
						checked={this.props.checked}
            sportunity={this.props.sportunity}
						onChange={(e) => this._selectEvent(e)}
					/>*/}
          <input
            style={styles.checkbox}
            type='checkbox'
            checked={this.props.checked}
            onChange={this._selectEvent}
          />
				</div>
				}
			</div>)
  }
}


styles = {
	selectEvent: {
		position: 'absolute',
		right: 10,
		top: 10,
	},
  card: {
	  textDecoration: 'none',
    width: '100%',
    height: 215,
    display: 'flex',
    backgroundColor: colors.white,
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.12)',
    border: '1px solid #E7E7E7',
    overflow: 'hidden',
    marginBottom: 15,
    fontFamily: 'Lato',
    '@media (max-width: 600px)': {
      height: 250,
    },

  },
  colorActive: {
    width: 8,
    height: '100%',
    backgroundColor: colors.green,
  },
  colorGray: {
    width: 8,
    height: '100%',
    backgroundColor: colors.gray,
  },
  colorBlack: {
    width: 8,
    height: '100%',
    backgroundColor: colors.black,
  },
	colorYellow: {
    width: 8,
    height: '100%',
    backgroundColor: colors.yellow,
  },
	colorGreen: {
    width: 8,
    height: '100%',
    backgroundColor: colors.green,
  },
	colorRed: {
    width: 8,
    height: '100%',
    backgroundColor: colors.red,
  },
  colorLightGreen: {
    width: 8,
    height: '100%',
    backgroundColor: colors.lightGreen,
  },
  colorDarkGreen: {
    width: 8,
    height: '100%',
    backgroundColor: colors.darkGreen,
  },
  black: {
    textTranform: 'uppercase',
    fontWeight: 'bold',
    fontSize: 20,
    color: colors.black,
  },
  gray: {
    textTranform: 'uppercase',
    fontWeight: 'bold',
    fontSize: 20,
    color: colors.gray,
  },
	yellow: {
		textTranform: 'uppercase',
    fontWeight: 'bold',
    fontSize: 20,
    color: colors.yellow,
	},
	red: {
		textTranform: 'uppercase',
    fontWeight: 'bold',
    fontSize: 20,
    color: colors.red,
	},
  lightGreen: {
    textTranform: 'uppercase',
    fontWeight: 'bold',
    fontSize: 20,
		color: colors.lightGreen,
	},
  darkGreen: {
    textTranform: 'uppercase',
    fontWeight: 'bold',
    fontSize: 20,
    color: colors.darkGreen
  },
	green: {
		textTranform: 'uppercase',
    fontWeight: 'bold',
    fontSize: 20,
    color: colors.green,
	},
  color: {
    width: 8,
    height: '100%',
    backgroundColor: colors.green,
  },
  container: {
    width: '100%',
    height: '100%',
    padding: '10px 15px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  top: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: 10,
		width:'100%',
	},
	paid: {
    textTranform: 'uppercase',
    fontWeight: 'bold',
    fontSize: 20,
    color: colors.green,
  },
  datetime: {
    fontSize: 15,
		marginRight: 0,
  },
  date: {
    fontWeight: 'bold',
    lineHeight: 1.2,
    color: '#5e5e5e',
    display: 'flex'
  },
  time: {
    color: '#939393',
  },
  content: {
    display: 'flex',
    // marginBottom: 26,
  },
  icon: {
    width: 74,
    height: 74,
    marginRight: 15,
    borderRadius: '50%',
    backgroundColor: colors.white,
	},
	iconHover: {
    width: 74,
    height: 74,
    marginRight: 15,
    borderRadius: '50%',
    backgroundColor: colors.blue,
	},
  iconImage: {
		color:colors.white,
		width: 74,
    height: 74,
  },
  organizerContainer: {
    marginTop: 5,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    fontSize: 16,
  },
  opponentContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    fontSize: 16,
    marginLeft: 5
  },
  organizerPseudo: {
    fontWeight: 500,
    color: colors.black,
    fontFamily: 'Lato',
  },
  organizerAvatar: {
    width: 15,
    height: 15,
    marginRight: 5,
    borderRadius: '50%',
    backgroundColor: colors.white,
    backgroundPosition: '50% 50%',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
  },
	info: {
  },
  name: {
    color: 'rgba(0, 0, 0, 0.65)',
    fontSize: 20,
    fontWeight: 'bold',
    display: 'block',
    marginBottom: 11,
    textDecoration: 'none',
    maxWidth: '300px',
    marginRight: 20,
	},

  sport: {
    marginBottom: 10,
  },

  sportName: {
    color: colors.blue,
    fontSize: 16,
  },

  qualification: {
    fontSize: 13,
		color: colors.black,
  },

  location: {
    fontSize: 16,
    fontWeight: 500,
    color: colors.black,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },

  addressContainer: {
    display: 'flex',
    flexDirection: 'column'
  },

  marker: {
    marginRight: 11,
    color: colors.blue,
  },

  score: {
    color: colors.blue,
    fontSize: 16,
    marginTop: 5,
    marginLeft: 15,
    textAlign: 'center'
  },

  bottom: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  participants: {
    fontSize: 14,
    fontWeight: 'bold',
    color: 'rgba(0, 0, 0, 0.65)',
  },

  price: {
    fontSize: 26,
    color: colors.green,
  },
	link: {
  	position: 'relative',
		width:'100%',
  },
  checkbox: {
    borderWidth: 0,
    borderBottomWidth: 2,
    borderStyle: 'solid',
    borderColor: colors.blue,
    lineHeight: '32px',
    fontFamily: 'Lato',
    color: 'rgba(0,0,0,0.65)',
    display: 'block',
    background: 'transparent',
    fontSize: fonts.size.medium,
    outline: 'none',
    cursor: 'pointer',
    width: 18, 
    height: 18, 
    ':disabled': {
      backgroundColor: colors.gray,
    },
  },
};

export default Relay.createContainer(Radium(Sportunity), {
  initialVariables: {
    shouldQueryUserStatus: false,
    sportunityID: null,
    userID: null
  },
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
        me {
          id
        }
        sportunityStatus(sportunityId: $sportunityID, userId: $userID) @include(if: $shouldQueryUserStatus) 
      }
    `,
    sportunity: () => Relay.QL`
      fragment on Sportunity {
        id
        title
        description
        beginning_date
        ending_date
        survey {
          isSurveyTransformed
          surveyDates {
            answers {
              user {
                id
                pseudo
              }
              answer
            }
          }
        }
        sportunityType {
          id,
          name {
            EN,
            FR
          }
        }
        sportunityTypeStatus {
          id,
          name {
            EN,
            FR
          }
        }
        score {
          currentTeam,
          adversaryTeam
        }
        game_information {
          opponent {
            organizerPseudo
            unknownOpponent
            lookingForAnOpponent
            organizer {
              id,
              pseudo,
              avatar
            }
            invitedOpponents (last: 5) {
              edges {
                node {
                  id,
                  members {
                    id
                  }
                }
              }
            }
          }
        }
        address {
          address
          city
          country
          position {
            lat
            lng
          }
        }
        status
        venue {
          id
          name
        }
        infrastructure {
          id
          name
        }
        participants {
          id
        }
        price {
          currency
          cents
        }
        paymentStatus {
          status
          user {
            id
          }
          price {
            currency,
            cents
          }
        }
        organizers {
          organizer {
            id
            pseudo
            avatar
          }
          isAdmin
          role
          secondaryOrganizerType {
            id
            name {
              FR
              EN
              DE
              ES
            }
          }
          customSecondaryOrganizerType
        }
        sport {
          sport {
            logo
            id
            name {
              EN
              FR
              DE
            }
            assistantTypes {
                    id,
                    name {
                        FR,
                        EN,
                        DE, 
                        ES
                    }
            }
            levels {
              id
              EN {
                name
                skillLevel
              }
              FR {
                name
                skillLevel
              }
              DE {
                name
                skillLevel
              }
            }
          }
          allLevelSelected
          levels {
            id
            EN {
              name
              skillLevel
            }
            FR {
              name
              skillLevel
            }
            DE {
              name
              skillLevel
            }
          }
        }
      }
    `,
  },
});
