import React from 'react';
import PureComponent, { forceUpdate } from '../../common/PureComponent'
import Relay from 'react-relay'
import RelayStore from '../../../RelayStore.js'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router'
import moment from 'moment';
import withRouter from 'react-router/lib/withRouter'
import { browserHistory } from 'react-router';

import { appUrl } from '../../../../constants.json';
import * as types from '../../../actions/actionTypes.js';
import { colors } from '../../../theme'
import localizations from '../../Localizations'

import Radium from 'radium'

import UpdateLanguageMutation from './UpdateLanguageMutation';
import UpdateCountryMutation from './UpdateCountryMutation';

let styles

const socialArray = [
  {
    href: 'https://www.facebook.com/sportunitysarl/?ref:bookm' +
        'arks',
    icon: 'fa fa-facebook',
  }, {
    href: 'https://twitter.com/Sportunitysarl',
    icon: 'fa fa-twitter',
  }, {
    href: 'https://www.linkedin.com/company/sportunity-sarl?t' +
        'rk:company_logo',
    icon: 'fa fa-linkedin',
  }, {
    href: 'https://www.youtube.com/channel/UCUxriN0QQpU6Rx3n0F6Ybaw',
    icon: 'fa fa-youtube',
  },
];

class Footer extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      url: ''
    }
  }

  componentWillMount() {
    super.componentWillMount()
    if (this.props.user && this.props.user.appLanguage && this.props.user.appLanguage.toLowerCase() !== localizations.getLanguage())
      localizations.setLanguage(this.props.user.appLanguage.toLowerCase());

    if (this.props.user && this.props.user.appLanguage && this.props.user.appLanguage.toLowerCase() !== moment.locale())
      moment.locale(this.props.user.appLanguage.toLowerCase())

    if (!this.props.user && localizations.getLanguage().toLowerCase() !== moment.locale().toLowerCase())
      moment.locale(localizations.getLanguage().toLowerCase())
  }

  componentDidMount() {

    fetch('https://ipapi.co/json')
    .then(res => res.json())
    .then(json => {
      if (this.props.user && this.props.user.appCountry) {
        this.props._updateUserCountry(this.props.user.appCountry)
        if (this.props.user.appCurrency) 
          this.props._updateUserCurrency(this.props.user.appCurrency)
      }
      else if (!this.props.user) {
        this.props._updateUserCountry(json.country)
        this.props._updateUserCurrency(this.getCountryCurrency(json.country))
      }  
      else if (!this.props.user.appCountry) {
        this.props._updateUserCountry(json.country);
        this.props._updateUserCurrency(this.getCountryCurrency(json.country))
        this._changeUserCountry(json.country, this.getCountryCurrency(json.country));
      }
      this.props._updateUserCity(json.city)
      this.props._updateUserLocation(new google.maps.LatLng(json.latitude, json.longitude))
    })  
    
    let path = this.props.router.location.pathname; 
    
    if (path.indexOf('/s/') >= 0) { 
      let hash = path.split('/') ;
      console.log(hash);
      this.props.relay.setVariables({
        queryLongUrl: true,
        shortUrl: hash[2]
      },readyState => {
        if (readyState.done) {
            setTimeout(() => {
                if (this.props.viewer.url) {
                  browserHistory.push({
                    pathname : this.props.viewer.url
                  })
                }
                else {
                  browserHistory.push({
                    pathname : '/'
                  })
                }
            }, 100);
        }
      })
    }
  }

  getCountryCurrency = countryCode => {
    if (countryCode === 'CH')
      return 'CHF'
    else 
      return 'EUR'
  }

  _changeLanguage = (e) => {
    localizations.setLanguage(e.target.value)
    moment.locale(e.target.value)
    forceUpdate({ lang: e.target.value })
    this.setState({})
    this.props.onUpdateLanguage(e.target.value)
    if (this.props.viewer.me) {
      RelayStore.commitUpdate(
        new UpdateLanguageMutation({
          viewer: this.props.viewer,
          userIDVar: this.props.viewer.me.id,
          appLanguageVar: localizations.getLanguage().toUpperCase()
        }),
        {
          onFailure: error => {
            let errors = JSON.parse(error.getError().source);
            console.log(errors);
          },
          onSuccess: (response) => {
            console.log("language changed")
          },
        }
      );
    };
  }

  _changeUserCountry = (e, currency) => {
    if (this.props.viewer.me) {
      RelayStore.commitUpdate(
        new UpdateCountryMutation({
          viewer: this.props.viewer,
          userIDVar: this.props.viewer.me.id,
          countryVar: e,
          currencyVar: currency
        }),
        {
          onFailure: error => {
            let errors = JSON.parse(error.getError().source);
            console.log(errors);
          },
          onSuccess: (response) => {
          },
        }
      );
    };
  }

  shareUrl = () => {
    if (this.state.url) {
      this.synchronizeLink.select();
      document.execCommand('copy');
      this.setState({url: ''})
    }
    else {
      let path = this.props.router.location.pathname; 
      
      if (path === '/find-sportunity' || path === '/find-sportunity/') {
        path="/find-sportunity/lat=&lng=/range=/sport=/fromDate=&toDate=/free="

        let splitted = path.split('/');

        if (this.props.locationLat && this.props.locationLng) {
          let index = splitted.findIndex(split => split.indexOf('lat=') >= 0);
          if (index >= 0)
            splitted[index] = "lat="+this.props.locationLat+"&lng="+this.props.locationLng

          if (this.props.distanceRange || this.props.distanceRange === 0) {
            let index = splitted.findIndex(split => split.indexOf('range=') >= 0);
            if (index >= 0)
              splitted[index] = "range="+this.props.distanceRange
          }
          else {
            let index = splitted.findIndex(split => split.indexOf('range=') >= 0);
            if (index >= 0)
              splitted[index] = "range="
          }
        }
        else {
          let index = splitted.findIndex(split => split.indexOf('lat=') >= 0);
          if (index >= 0)
            splitted[index] = "lat=&lng="

          index = splitted.findIndex(split => split.indexOf('range=') >= 0);
          if (index >= 0)
            splitted[index] = "range="
        }

        if (this.props.sportId) {
          let index = splitted.findIndex(split => split.indexOf('sport=') >= 0);
          if (index >= 0)
            splitted[index] = "sport=" + this.props.sportId
        }
        else {
          let index = splitted.findIndex(split => split.indexOf('sport=') >= 0);
          if (index >= 0)
            splitted[index] = "sport="
        }

        if (this.props.dateFrom && this.props.dateTo) {
          let index = splitted.findIndex(split => split.indexOf('fromDate=') >= 0);
          if (index >= 0)
            splitted[index] = "fromDate="+new Date(this.props.dateFrom).valueOf()+"&toDate="+new Date(this.props.dateTo).valueOf()
        }
        else {
          let index = splitted.findIndex(split => split.indexOf('fromDate=') >= 0);
          if (index >= 0)
            splitted[index] = "fromDate=&toDate="
        }

        if (typeof this.props.isFreeOnly !== 'undefined') {
          let index = splitted.findIndex(split => split.indexOf('free=') >= 0);
          if (index >= 0)
            splitted[index] = this.props.isFreeOnly ? "free=true" :'free='
        }
        else {
          let index = splitted.findIndex(split => split.indexOf('free=') >= 0);
          if (index >= 0)
            splitted[index] = "free="
        }
        path = splitted.join('/')
      }

      if (path) {
        this.props.relay.setVariables({
          queryShortUrl: true, 
          url: path
        },readyState => {
          if (readyState.done) {
              setTimeout(() => {
                  this.setState({url: appUrl + '/s/' + this.props.viewer.createShortUrl})
              }, 100);
          }
        })
      }
    }
  }

  render() {
    return (
      <div style={styles.container}>
        <div style={styles.topLine}>
          <div style={styles.topLeft}>
            <img src='/assets/images/white-logo.png' width='80' height='80' />
            <span style={{color: colors.black, cursor: 'pointer', marginTop: 10}} onClick={this.shareUrl}>
              {localizations.shareThisUrl}
              <input 
                ref={(ref) => this.synchronizeLink = ref}
                style={{opacity: 0, position: "absolute", width: 8}} 
                value={this.state.url} 
                />
            </span>
          </div>
          <div style={styles.topCenter}>
            <div>{localizations.footer_followUs}</div>
            <div style={styles.iconsLine}>
                  {socialArray.map((item, index) => <a
                    key={`socialIcon${index}`}
                    href={item.href}
                    target="_blank"
                    style={styles.icon}
                  >
                    <i className={item.icon} />
                </a>)}

            </div>
          </div>
          <div style={styles.topRight}>
	          <div style={{...styles.row, marginTop: 15}}>{localizations.footer_language}&nbsp;&nbsp;&nbsp;&nbsp;
		          <select onChange={this._changeLanguage}
		                  value={localizations.getLanguage()}
		                  style={styles.input}>
			          <option value='en'>english</option>
			          <option value='fr'>français</option>
		          </select>
	          </div>
            {/*<div style={styles.row}>*/}
              {/*<span>{'Sportunity SARL, Rue du bugnon 20, 1005 Lausanne'}</span>*/}
              {/*&nbsp;&nbsp;&nbsp;&nbsp;<i className="fa fa-map-marker" />*/}
            {/*</div>*/}
            <div style={styles.row}>
              <a style={styles.mailLink} href="mailto:info@sportunity.ch">info@sportunity.ch</a>
              &nbsp;&nbsp;&nbsp;&nbsp;<i className="fa fa-envelope" />
            </div>
            <div style={styles.row}>
              <span>{localizations.contactUs_phoneNumber}</span>
              &nbsp;&nbsp;&nbsp;&nbsp;<img src='/assets/images/AboutUs/swiss.png' style={{width: '1em'}}/>
            </div>
            <div style={styles.row}>
              <span>{localizations.contactUs_phoneNumber_FR}</span>
              &nbsp;&nbsp;&nbsp;&nbsp;<img src='/assets/images/AboutUs/france.png' style={{width: '1em'}}/>
            </div>
          </div>
        </div>
        <div style={styles.bottomLine}>
          <Link to='/about-us' style={styles.link}>{localizations.footer_aboutUs}</Link>
          <Link to='/privacy' style={styles.link}>{localizations.footer_privacy}</Link>
          <Link to='/term' style={styles.link}>{localizations.footer_terms}</Link>
          <div>
            <div>
              <Link to='/faq/tutorial' style={styles.link}>{localizations.footer_tutorials}</Link>
            </div>
            {/*<div style={styles.groupLink}>
              <Link to='/faq/team-tutorial' style={styles.subLink}>{localizations.footer_tutorials_clubs}</Link>
              <Link to='/faq/companies-tutorial' style={styles.subLink}>{localizations.footer_tutorials_companies}</Link>
            </div>*/}
          </div>
        </div>
      </div>
    );
  }
}

styles = {
  container: {
    display: 'flex',
    color: colors.white,
    width: '100%',
    alignItems: 'center',
    backgroundColor: '#1a1a1a',
    paddingRight: 20,
    flexDirection: 'column',
    fontFamily: 'Lato',
    fontSize: 16,
    '@media (max-width: 600px)': {
      paddingLeft: 0,
    }
  },
  topLine: {
    display: 'flex',
    flexDirection: 'row',
    width: '1000px',
    maxWidth: '100%',
    padding: '0 40px',
    marginTop: 40,
    marginBottom: 40,
    '@media (max-width: 768px)': {
      width: '100%',
      padding: '0 10px',
      flexDirection: 'column',
      marginBottom: 5
    }
  },
  topLeft: {
    display: 'flex',
    flex: 4,
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
    '@media (max-width: 768px)': {
      marginBottom: 25,
      flex: 1
    }
  },
  topCenter: {
    display: 'flex',
    flex: 3,
    justifyContent: 'flex-start',
    fontSize: 16,
    flexDirection: 'column',
    width: '100%',
    textAlign: 'center',
    top: 0,
    '@media (max-width: 768px)': {
      marginBottom: 25,
    },
  },
  topRight: {
    display: 'flex',
    flex: 4,
    justifyContent: 'flex-start',
    flexDirection: 'column',
    '@media (max-width: 600px)': {
      flexDirection: 'column',
      marginBottom: 10,
    },
  },
  iconsLine: {
    color: colors.white,

    marginTop: 15,
    textDecortation: 'none',

  },
  icon: {
    margin: 10,
    color: colors.white,
    fontSize: 30,
  },
  row: {
    width: '100%',
    textAlign: 'right',
    marginBottom: 7,
    marginRight: 15,
    '@media (max-width: 768px)': {
      textAlign: 'center',
    },
  },
  bottomLine: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    padding: '0 40px',
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: 40,
    '@media (max-width: 650px)': {
      padding: '0 10px',
      flexDirection: 'column',
      textAlign: 'center',
    }
  },
  link: {
    color: colors.white,
    marginLeft: 13,
    marginRight: 13,
    fontSize: 16,
    textDecoration: 'none',
    marginBottom: 10,
  },
  groupLink: {
    display: 'flex',
    flexDirection: 'column',
    marginLeft: 13,
    marginTop: 5,
    marginBottom: 10,
    marginBottom: 5,
    maxWidth: 260,
    '@media (max-width: 650px)': {
      maxWidth: 'none',
    }
  },
  subLink: {
    color: colors.gray,
    lineHeight: '18px',
    fontSize: 12,
    textDecoration: 'none',
  },
  mailLink: {
    color: colors.white,
    fontSize: 16,
    textDecoration: 'none',
  }
}

const _updateUserCountry = (value) => ({
  type: types.GLOBAL_SET_USER_COUNTRY,
  value
});
const _updateUserCurrency = (value) => ({
  type: types.GLOBAL_SET_USER_CURRENCY,
  value
})
const _updateUserLocation = (value) => ({
  type: types.GLOBAL_SET_USER_LOCATION,
  value
})
const _updateUserCity = (value) => ({
  type: types.GLOBAL_SET_USER_CITY,
  value
})

const dispatchToProps = (dispatch) => ({
  _updateUserCountry: bindActionCreators(_updateUserCountry, dispatch),
  _updateUserCurrency: bindActionCreators(_updateUserCurrency, dispatch),
  _updateUserLocation: bindActionCreators(_updateUserLocation, dispatch),
  _updateUserCity: bindActionCreators(_updateUserCity, dispatch)
})

const stateToProps = (state) => ({
  userCountry: state.globalReducer.userCountry,
  userCurrency: state.globalReducer.userCurrency,
  userLocation: state.globalReducer.userLocation,
  userCity: state.globalReducer.userCity,
  sportId: state.sportunitySearchReducer.sportId,
  locationLat: state.sportunitySearchReducer.locationLat,
  locationLng: state.sportunitySearchReducer.locationLng,
  distanceRange: state.sportunitySearchReducer.distanceRange,
  isFreeOnly: state.sportunitySearchReducer.isFreeOnly,
  dateFrom: state.sportunitySearchReducer.dateFrom,
  dateTo: state.sportunitySearchReducer.dateTo,
})

let ReduxContainer = connect(
  stateToProps,
  dispatchToProps
)(Radium(Footer));

export default Relay.createContainer(withRouter(Radium(ReduxContainer)), {
  initialVariables: {
    queryShortUrl: false,
    url: '',
    queryLongUrl: false,
    shortUrl: ''
  },
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        ${UpdateLanguageMutation.getFragment('viewer')}
        ${UpdateCountryMutation.getFragment('viewer')}
        me {
          id
        }
        createShortUrl(url: $url) @include(if: $queryShortUrl)
        url(shortUrl: $shortUrl) @include(if: $queryLongUrl)
      }
    `,
    user: () => Relay.QL`
      fragment on User {
        id
        appLanguage
        appCountry
        appCurrency
      }
    `
  }
});
