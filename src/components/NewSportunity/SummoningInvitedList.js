import React from 'react';
import Relay from 'react-relay';
import PureComponent, { pure } from '../common/PureComponent'
import Radium from 'radium';

import Switch from '../common/Switch';
import { colors } from '../../theme';
import localizations from '../Localizations'

import InvitedCircleDetails from './InvitedCircleDetails'
import CircleAutoParticipate from './CircleAutoParticipate';

let styles;

class SummoningInvitedList extends PureComponent {
    componentDidMount() {
        this.state = {
            isFullListVisible: false,
            circleDetailsModalIsOpen: false,
            selectedCircle: null
        }
    }

    componentWillReceiveProps = (nextProps) => {
        if (this.state.selectedCircle && nextProps.invitedCircles && this.state.circleDetailsModalIsOpen) {
            let index = nextProps.invitedCircles.findIndex(invitedCircle => invitedCircle.circle.id === this.state.selectedCircle.circle.id)
            this.setState({
                selectedCircle: nextProps.invitedCircles[index]
            })
        }
    }
    
    openCircleDetails = (item) => {
        this.setState({
            selectedCircle: item,
            circleDetailsModalIsOpen: true
        });
        this.props.relay.setVariables({
            queryCircle: true,
            circleId: item.circle.id
        })
    }

    closeCircleDetails = () => {
        this.setState({
            selectedCircle: null,
            circleDetailsModalIsOpen: false
        })
    }

    render() {
        const { 
            list, 
            invitedCircles, 
            onRemoveItem, 
            onRemoveInvitedCircle, 
            onChangeCirclePrice, 
            onChangeCircleAutoParticipate, 
            onChangeUserAutoParticipate,
            viewer
        } = this.props;

        let itemList = [];
        if (list && list.length > 0) {
            list.forEach(item => 
                itemList.push({
                    type: 'user',
                    item
                })
            )
        }
        if (invitedCircles && invitedCircles.length > 0) {
            invitedCircles.forEach(item => 
                itemList.push({
                    type: 'circle',
                    item
                })
            )
        }

        return (
            <div
                style={styles.container}
            >
                <InvitedCircleDetails
                    viewer={viewer}
                    itemList={itemList}
                    user={viewer.me}
                    circleDetails={viewer.circle}
                    isModalOpen={this.state.circleDetailsModalIsOpen}
                    selectedCircle={this.state.selectedCircle}
                    closeModal={this.closeCircleDetails}
                    onChangeCircleAutoParticipate={onChangeCircleAutoParticipate}
                    onChangeUserAutoParticipate={onChangeUserAutoParticipate}
                    onChangeCirclePrice={onChangeCirclePrice}
                    _handleNotificationTypeChange={this.props._handleNotificationTypeChange}
                    _handleNotificationAutoXDaysBeforeChange={this.props._handleNotificationAutoXDaysBeforeChange}
                    isModifying={this.props.isModifying}
                    isSurvey={this.props.isSurvey}
                    fields={this.props.fields}
                />

                {itemList && itemList.length > 0 && 
                    <div style={styles.invitedList} >
                        <ul style={styles.list}>
                            {(this.state.isFullListVisible ? itemList : itemList.filter((el, index) => index < 5)).map((el, index) => (
                                el.type === 'user'
                                ?   <li
                                        key={index}
                                        style={{...styles.listItem, alignItems: 'center'}}
                                    >
                                        <div style={styles.avatarContainer}>
                                            <div style={{ ...styles.avatar, backgroundImage: `url(${el.item.avatar})` }} />
                                        </div>
                                        <div style={styles.userLinePseudo}>
                                            <span style={styles.userName}>
                                                {el.item.pseudo}
                                            </span>
                                        </div>
                                        <div style={styles.removeCross} onClick={e => {this.props.onRemoveItem(index, e); if (list.length === 0) this.setState({invitedListIsOpen: false}) }}>
                                            <i className="fa fa-times" style={styles.cancelIcon} aria-hidden="true"></i>
                                        </div>
                                    </li>
                                :   <li
                                        key={index}
                                        style={styles.listItem}
                                    >
                                        <div style={styles.buttonIcon}>
                                            <img src="/assets/images/icon_circle@3x.png" height="40px" />
                                            <div style={styles.numberContainer}>
                                                <span style={styles.number}>
                                                    {el.item.circle.memberCount}
                                                </span>
                                            </div>
                                        </div>

                                        <div style={styles.circleItemContainer}>
                                            <div style={styles.circleTopContent}>
                                                <span style={styles.circleName}>
                                                    {el.item.circle.name}
                                                </span>
                                                <span style={styles.detailsButton} onClick={() => this.openCircleDetails(el.item)}>
                                                    
                                                    {localizations.newSportunity_circle_show_details}
                                                </span>
                                            </div>
                                            <div style={styles.circleBottomContent}>
                                                {el.item.circle.owner 
                                                ?   <div style={styles.ownerContainer}>
                                                        <div style={{...styles.smallAvatar, backgroundImage: el.item.circle.owner.avatar ? 'url('+ el.item.circle.owner.avatar +')' : 'url("https://sportunitydiag304.blob.core.windows.net/avatars/default-avatar.png")'}} />
                                                        {el.item.circle.owner.pseudo}
                                                    </div>
                                                :   <div style={styles.ownerContainer}>
                                                        <div style={{...styles.smallAvatar, backgroundImage: viewer.me.avatar ? 'url('+ viewer.me.avatar +')' : 'url("https://sportunitydiag304.blob.core.windows.net/avatars/default-avatar.png")'}} />
                                                        {viewer.me.pseudo}
                                                    </div>
                                                }

                                                
                                            </div>
                                        </div>

                                        <div style={styles.removeCross} onClick={onRemoveInvitedCircle.bind(this, el)}>
                                            <i className="fa fa-times" style={styles.cancelIcon} aria-hidden="true"></i>
                                        </div>
                                    </li>
                            
                            ))}
                        </ul>
                    </div>                        
                }

	            {itemList.length > 3 &&
	            <span style={{...styles.detailsButton, float: 'right'}} onClick={() => this.setState({isFullListVisible: !this.state.isFullListVisible})}>
                                {this.state.isFullListVisible
	                                ?   localizations.newSportunity_invitedList_seeLess
	                                :   localizations.newSportunity_invitedList_seeMore
                                }
                            </span>
	            }
            </div>
        );
    }
}

styles = {
    container: {
        position: 'relative',
        width: '100%',
        marginBottom: 25
    },

    title: {
        color: colors.darkBlue,
        fontSize: 16,
        marginTop: 20
    },
    invitedList: {
        marginLeft: 0,
        marginTop: 10,
        maxHeight: 300,
        overflow: 'scroll'
    },
    
    invitedCircleList: {
        marginLeft: 0
    },
    
    list: {},
    
    listItem: {
        color: '#515151',
        fontSize: 16,
        fontFamily: 'Lato',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 5,
        border: '0px solid ' + colors.gray,
        boxShadow: '0 2px 4px 0 rgba(0,0,0,0.24), 0 0 4px 0 rgba(0,0,0,0.12)',
        padding: '0px 0px 0px 0px',
    },
    
    removeCross: {
        width: 24,
        height: 60, 
        paddingTop: 6,
        paddingLeft: 4,
        color: '#212121',
        cursor: 'pointer',
        fontSize: '13px',
    },
    cancelIcon: {
    },
    circleItemContainer: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        flexDirection: 'column',
        fontSize: 13,
        flex: 4
    },
    circleTopContent: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%'
    },
    circleBottomContent: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%'
    },
    userName: {
        color: colors.blue,
        fontWeight: 'bold',
    },
    circleName: {
        color: colors.blue,
        fontWeight: 'bold',
        marginBottom: 5
    },
    detailsButton: {
        color: '#fff',
        fontSize: 10,
        marginRight: 10,
        cursor: 'pointer',
        backgroundColor: '#64A5D7',
        padding:'6px 10px',
        position: 'relative',
        top: 13,
        left: 20,

    },
    detailsIcon: {
        marginRight: 5,
        fontSize: 13
    },
    
    ownerContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        fontSize: 11
    },
    price: {
        width: 60,
        height: 36,

        border: '2px solid #5E9FDF',
        borderRadius: '3px',
        textAlign: 'center',

        fontSize: 13,
        lineHeight: 1,
        marginRight: 10,

        color: 'rgb(111, 101, 101)',
    },

    bold: {
        fontWeight: 'bold',
    },
    buttonIcon: {
        color: colors.blue,
        position: 'relative',
        flex: 1,
        marginRight: 5,
        color: '#fff',
        backgroundColor: '#64a5d8',
        padding: '8px 5px',
    },
    numberContainer: {
        position: 'absolute',
        top: '17px',
        left: '27px',
        width: 24,
        textAlign: 'center'
    },
    number: {
        fontSize: 19,
        fontWeight: 'bold'
    },
    avatarContainer: {
        flex: 1,
        display: 'flex',
        marginRight: 5,
        justifyContent: 'center'
    },
    avatar: {
        width: 39,
        height: 39,
        marginRight: 10,
        color: colors.blue,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        borderRadius: '50%',
    },
    smallAvatar: {
        width: 20,
        height: 20,
        marginRight: 10,
        color: colors.blue,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        borderRadius: '50%',
    },
    userLinePseudo: {
      display: 'flex',
      alignItems: 'center',
      fontSize: 13,
      color: colors.blue,
      fontWeight: 'bold',
      flex: 4
    },
};

export default Relay.createContainer(Radium(SummoningInvitedList), {
    initialVariables: {
        queryCircle: false,
        circleId: null
    },
    fragments: {
        viewer: () => Relay.QL`
            fragment on Viewer {
                ${InvitedCircleDetails.getFragment('viewer')}
                me {
                    id
                    pseudo
                    avatar
                    profileType
                }
                circle(id: $circleId) @include(if: $queryCircle) {
                    id
                    members {
                        id,
                        avatar,
                        pseudo,
                    }
                    memberStatus {
                        starting_date
                        member {
                            id
                            pseudo
                        }
                        status
                    }
                }
            }
        `
    }
})