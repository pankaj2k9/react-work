import Relay from 'react-relay';
/**
*  New sportunity mutation
*/
export default class NewSportunityMutation extends Relay.Mutation {
  /**
  *  Mutation
  */
  getMutation() {
    return Relay.QL`mutation Mutation {newSportunity}`;
  }
  /**
  *  Variables
  */
  getVariables() {
    return {
      sportunity: {
        title: this.props.sportunity.title,
        description: this.props.sportunity.description,
        address: this.props.sportunity.address,
        venue: this.props.sportunity.venue,
        infrastructure: this.props.sportunity.infrastructure,
        slot: this.props.sportunity.slot,
        organizers: this.props.sportunity.organizers,
        pendingOrganizers: this.props.sportunity.pendingOrganizers,
        sport: this.props.sportunity.sport,
        mode: this.props.sportunity.mode,
        kind: this.props.sportunity.kind,
        participantRange: this.props.sportunity.participantRange,
        price: this.props.sportunity.price,
        ageRestriction: this.props.sportunity.ageRestriction,
        sexRestriction: this.props.sportunity.sexRestriction, 
        beginning_date: this.props.sportunity.beginningDate,
        ending_date: this.props.sportunity.endingDate,
        survey_dates: this.props.sportunity.survey_dates,
        repeat: this.props.sportunity.repeat,
        participants: this.props.sportunity.participants,
        paymentMethodId: this.props.sportunity.paymentMethodId,
        secondaryOrganizersPaymentMethod: this.props.sportunity.secondaryOrganizersPaymentMethod,
        secondaryOrganizersPaymentByWallet: this.props.sportunity.secondaryOrganizersPaymentByWallet,
        invited: this.props.sportunity.invited,
        invited_circles: this.props.sportunity.invited_circles,
        price_for_circle: this.props.sportunity.price_for_circle,
        notification_preference: this.props.sportunity.notification_preference,
        privacy_switch_preference: this.props.sportunity.privacy_switch_preference,
        hide_participant_list: this.props.sportunity.hide_participant_list,
        sportunityType: this.props.sportunity.sportunityType,
        game_information: this.props.sportunity.game_information
      },
    };
  }
  /**
  *  Fat query
  */
  getFatQuery() {
    return Relay.QL`
      fragment on newSportunityPayload @relay(pattern: true){
        edge,
        clientMutationId,
        viewer {
          id,
          sportunities
        }
      }
    `;
  }

  /**
  *  Config
  */
  getConfigs() {
    return [{
      type: 'RANGE_ADD',
      parentName: 'viewer',
      parentID: this.props.viewerID,
      connectionName: 'SportunityConnection',
      edgeName: 'edge',
      rangeBehaviors: () => 'prepend',
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        sportunities(last: 1) {
          edges {
            node {
              id
              title
            }
          }
        }
      }
    `,
  };
}
