import React from 'react';
import Relay from 'react-relay'
import Radium from 'radium';

import { colors } from '../../theme';
import localizations from '../Localizations'

let styles;


class Organizer extends React.Component {
    state = {
        roleOther: false,
    }

    componentDidMount = () => {
        if (this.props.organizer && this.props.organizer.organizer) {
            this.props.relay.setVariables({
                organizerId: this.props.organizer.organizer, 
                query: true
            })
            if (this.props.sport && this.props.sport.id) {
                this.props.relay.setVariables({
                    sportId: this.props.sport.id,
                    querySport: true
                })
            }
            
            if (this.props.sport && this.props.sport.value) {
                this.props.relay.setVariables({
                    sportId: this.props.sport.value,
                    querySport: true
                })
            }

            if (this.props.organizer.customSecondaryOrganizerType) {
                this.setState({
                    roleOther: true
                })
            }
        }        
    }

    componentWillReceiveProps = (nextProps) => {
        if (this.props.organizer && this.props.organizer.organizer && nextProps.organizer && nextProps.organizer.organizer && this.props.organizer.organizer !== nextProps.organizer.organizer) {
            this.props.relay.setVariables({
                organizerId: nextProps.organizer.organizer, 
                query: true
            })
        }
        if (nextProps.sport && nextProps.sport.id && (!this.props.sport || (this.props.sport.id && this.props.sport.id !== nextProps.sport.id))) {
            this.props.relay.setVariables({
                sportId: nextProps.sport.id,
                querySport: true
            })
        }
    }

    _renderSportInformation = (organizer, selectedSport) => {
        if (!selectedSport)
            return <div>-</div>; 

        if (selectedSport.assistantTypes && selectedSport.assistantTypes.length > 0 && !this.state.roleOther) {
            return (
                <div style={styles.sportInfo}>
                    <select value={organizer.secondaryOrganizerType} style={styles.assistantSelect} onChange={this._handleUpdateRole}>
                        <option key={0} value={""}>
                            {localizations.newSportunity_organizerChoose}
                        </option>}
                        {
                            selectedSport.assistantTypes.map(assistantType => (
                                <option key={assistantType.id} value={assistantType.id}>
                                    {assistantType.name[localizations.getLanguage().toUpperCase()]}
                                </option>
                            ))
                        }
                        <option key={"localOther"} value={"localOther"}>
                            {localizations.other}
                        </option>}
                    </select>
                </div>
            )
        }
        else if (selectedSport.assistantTypes && selectedSport.assistantTypes.length > 0 && this.state.roleOther) {
            return (
                <div style={styles.sportInfo}>
                    <input 
                        type="text" 
                        maxLength="30"
                        style={styles.textInput}
                        value={organizer.customSecondaryOrganizerType || ''} 
                        onChange={this._handleUpdateCustomRole}
                    />
                    <div style={styles.resetRoleOther} onClick={this._handleResetRoleOther}>
                        <i className="fa fa-times" aria-hidden="true"></i>
                    </div>
                </div>
            )
        }
        else 
            return <div>-</div>; 
    }

    _handleUpdatePrice = (event) => {
        this.props.updatePrice(this.props.organizer, Number(event.target.value))
    }

    _handleUpdateRole = (event) => {
        if (!event.target.value || event.target.value === "")
            this.props.updateRole(this.props.organizer, null)
        else if (event.target.value == "localOther") {
            this.props.updateRole(this.props.organizer, null)
            this.setState({roleOther: true})
        }
        else 
            this.props.updateRole(this.props.organizer, event.target.value)
    }

    _handleUpdateCustomRole = event => {
        if (!event.target.value || event.target.value === "")
            this.props.updateCustomRole(this.props.organizer, null)
        else 
            this.props.updateCustomRole(this.props.organizer, event.target.value)   
    }

    _handleResetRoleOther = () => {
        this.setState({
            roleOther: false
        })
        this.props.updateCustomRole(this.props.organizer, null)
    }

    render() {
        const { organizer, viewer, sport, isModifying } = this.props;
        
        return (
            viewer.user && viewer.user.id
            ?   <tr style={styles.container}>
                    <td style={styles.pseudo}>{viewer.user.pseudo}</td>
                    <td style={styles.role}>{this._renderSportInformation(organizer, viewer.sport ? viewer.sport : sport)}</td>
                    <td style={styles.priceInputContainer}>
                        <input
                            type="number"
                            style={styles.priceInput}
                            value={organizer.price.cents}
                            onChange={this._handleUpdatePrice}
                        />
                    </td>
                    <td style={styles.removeIcon} onClick={() => this.props.removeOrganizer(organizer.organizer)}>
                        <i className="fa fa-times" aria-hidden="true"></i>
                    </td>
                </tr>
            :   null
        );
    }
}

styles = {
    container: {
        marginTop: 5,      
        fontSize: 20,
        color: '#515151',
        position: 'relative',
        lineHeight: '50px'
    },
    pseudo: {
        fontWeight: 500, 
    },
    sportInfo: {
        paddingLeft: 10,
        position: 'relative'
    },
    resetRoleOther: {
        position: 'absolute',
        top: 0,
        right: 20,
        fontSize: 12,
        cursor: 'pointer'
    },
    assistantSelect: {
        width: '80%',
        minWidth: 90,
        height: 25,
        fontFamily: 'Lato',
        fontSize: 14
    },
    price: {
        color: colors.gray,
    },
    priceInputContainer: {

    },
    priceInput: {
        width: 90,
        height: 36,
        border: '2px solid #5E9FDF',
        borderRadius: '3px',
        textAlign: 'center',    
        fontFamily: 18,
        lineHeight: 1,    
        color: 'rgba(146,146,146,0.87)',
        fontSize: 16
    },
    removeIcon: {
        cursor: 'pointer',
        textAlign: 'right'
    },
    textInput: {
        width: '80%',
        minWidth: 90,
        height: 25,
        fontFamily: 'Lato',
        fontSize: 14
    }
};

export default Relay.createContainer(Radium(Organizer), {
  initialVariables: {
    organizerId: null,
    query: false,
    sportId: null,
    querySport: false,
  },
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        sport(id: $sportId) @include(if: $querySport) {
            id
            assistantTypes {
                id,
                name {
                    EN,
                    FR,
                    DE,
                    ES
                }
            }
        }
        user (id: $organizerId) @include(if: $query) {
            id
            pseudo
            sports {
                sport {
                    id, 
                },
                assistantType {
                    id,
                    name {
                        FR,
                        EN,
                        DE, 
                        ES
                    }
                }
            }
        }
      }
    `
  }
});