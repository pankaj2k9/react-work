import React from 'react';
import PureComponent, { pure } from '../common/PureComponent'
import Radium from 'radium';
import ReactTooltip from 'react-tooltip'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Switch from '../common/Switch';

import Input from './Input';
import Dropdown from './Dropdown';
import Radio from './Radio';
import localizations from '../Localizations'
import { colors, fonts } from '../../theme';

/*
const GET_PAID = 0;
const PARTICIPATE_AND_PAY = 1;
const PARTICIPATE_FOR_FREE = 2;
*/

let styles;
class PrivatePrice extends PureComponent {
  constructor(props) {
    super(props);
    
  }


  state = {
    
  }

  componentDidMount() {
    

  }


  componentWillUnmount() {
   
  }

  tableRowRender = () => {
      console.log(this.props.priceList);
      if(this.props.priceList instanceof Array){
        

          return this.props.priceList.map((object, i) => (
            <tr key={i}>
                
              <td>
                <div style={styles.buttonIcon}>
                <img src="/assets/images/icon_circle@3x.png" />
                <div style={styles.numberContainer}>
                    <span style={styles.numberInCircle}>
                      {object.circle && object.circle.memberCount}
                  </span>
                </div>
                  <div style={styles.circleName}>{object.circle.name}</div> 
                </div>
              </td> 
              <td style={styles.tableNumber}>
                       <input
                        style={styles.number}
                        type="number"
                        value={object.price.cents}
                        onChange={(e) => this.props.priceValueChangeByCommunity(object.circle,i, e) }
                        maxLength={3}
                        ref={(node) => this.props.onRefC(i, node)}
                        /> 
                       <span style={styles.currency}>{object.price.currency}</span>
                </td> 
             </tr>
           ))
      }
  }



 resumeRender = () =>  {
    const { price, fees, organizerParticipation, participantRange, currency } = this.props;
    return (
        <div style={styles.multiResume}>
         <div style={styles.singleResume}>
              {localizations.newSportunity_min_revenue} {(Math.round((participantRange.from * price * (1 - fees) + organizerParticipation) * 100 ) /100).toFixed(2) + ' ' + currency}
         </div>
     
        <div style={styles.singleResume}>
            {localizations.newSportunity_max_revenue} {(Math.round((participantRange.to * price * (1 - fees) + organizerParticipation) * 100) / 100).toFixed(2) + ' ' + currency}
        </div>
        </div>
    );
  }

  


  render() {
    const { price, fees, organizerParticipation, participantRange, priceList,onRef, priceValueChange, priceValueChangeByCommunity, currency } = this.props;
  
    return (
      <div style={styles.container} ref={node => { this._containerNode = node; }}>
        <div style={styles.col} >
          <div style={styles.row}>
                <label style={styles.label}>
                    {localizations.event_public_price}
                </label> 
                   <input
                        style={styles.number}
                        type="number"
                        value={price}
                        onChange={priceValueChange}
                        maxLength={3}
                        ref={onRef}
                        />
                    <div style={styles.currency}>{currency}</div>
            </div>

             <table>
                 <thead>
                    <tr style={styles.tableTh}> 
                      <th style={styles.tableThCol}>Guests</th> 
                       <th>Price </th> 
                    </tr>
                 </thead>
                 <tbody>
                     {this.tableRowRender()}
                 </tbody>
             </table>


         </div>


         <div style={styles.col} >
          <div style={styles.rowResume}>
                <label style={styles.label}>
                    Resume
                </label> 

                {this.resumeRender()}   

            </div>
         </div>


       </div>
    );
  }
}

styles = {
  container: {
    width: '100%',
    position: 'relative',
    marginBottom: 25,
    display: 'flex',
  },

  col:{
    flex: 1,
  },
  number: {
        height: 24,
        width: 54,
        border: '0px solid #5E9FDF',
        borderRadius: 3,
        textAlign: 'center',
        fontSize: 16,
        fontFamily: 18,
        lineHeight: 1,
        color: 'rgba(146,146,146,0.87)',
        marginLeft: 10,
        marginRight: 10,
        backgroundColor:'#E2E2E2',
        
  },
  tableNumber: {
    position: 'relative',
    top: '-11px',
  },
  row: {
    position: 'relative',
    marginBottom: 24,
    display: 'flex',
    alignItems: 'center',
  },
  rowResume: {
    position: 'relative',
    marginBottom: 24,
    alignItems: 'center',
  },

  label: {
    fontSize: 20,
    fontFamily: 'Lato',
    color: '#515151',
  },
  currency:{
    fontSize: 16,
    fontFamily: 'Lato',
    color: 'rgba(146,146,146,0.87)',
  },
  singleResume:{
    fontSize: 16,
    fontFamily: 'Lato',
    color: 'rgba(146,146,146,0.87)',
  },
  buttonIcon: {
    color: colors.blue,
    position: 'relative',
    display: 'flex',
    marginRight: 5,
    padding: '8px 5px',
  },
  numberContainer: {
    position: 'absolute',
    top: '16px',
    left: '24px',
    width: 27,
    textAlign: 'center'
  },
  numberInCircle: {
    fontSize: 19,
    fontWeight: 'bold'
  },
  tableTh: {
    fontSize: 20,
    fontFamily: 'Lato',
    color: '#515151',
  },
  circleName: {
    color: colors.blue,
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: '10px',
    marginLeft: '10px',
  },
  tableThCol: {
    textAlign: 'left',
    paddingBottom: '20px',
    paddingTop: '20px',
  },
  multiResume: {
    margin: '19px',
  },
  singleResume: {
    marginBottom: '5px',
    marginBottom: '5px',
    fontSize: '16px',
    color: 'rgba(146,146,146,0.87)',
  },
};




export default Radium(PrivatePrice);