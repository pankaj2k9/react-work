import React from 'react';
import PureComponent, { pure } from '../common/PureComponent'
import Radium from 'radium';
import ToggleDisplay from 'react-toggle-display'
import Switch from '../common/Switch';

import Input from './Input';
import Dropdown from './Dropdown';
import localizations from '../Localizations'

import { colors } from '../../theme';

let styles;

class Participants extends PureComponent {
  state = {
    dropdownOpen: false,
    exactly: false,
    inValidPax: true,
    minNotReached: false,
  }

  componentDidMount() {
    window.addEventListener('click', this._onClickOutside);
  }

  componentWillUnmount() {
    window.removeEventListener('click', this._onClickOutside);
  }

  _onClickOutside = (event) => {
    const { dropdownOpen } = this.state;
    if (dropdownOpen) {
      if (!this._containerNode.contains(event.target)) {
        this._closeDropdown();
      }
    }
  }

  _openDropdown = () => {
    this.refs._inputNode._focus();
    this.setState({ dropdownOpen: true });
  }

  _closeDropdown = () => {
    this.setState({ inValidPax: this.props.value.to >= this.props.value.from}) ;
    if (!this.props.value.to && !this.props.value.from) 
      this.props.onChange({from: 1, to: 1})
    
    this.setState({ dropdownOpen: false });
    this.refs._inputNode._onBlur();
  }

  _toggleDropdown = () => {
    if (this.state.dropdownOpen) {
      this._closeDropdown();
    }
    else {
      this._openDropdown();
    }
  }

  _handleExactlyChange = checked => {
    const { value, onChange } = this.props;
    this.setState({ exactly: checked });
    const newValue = {
      from: value.to,
      to: value.to,
    }

    if (typeof onChange === 'function') {
      onChange(newValue);
    }
  }

  _handleHideParticipantChange = checked => {
    const { onSwitchHideParticipantList } = this.props;

    if (typeof onSwitchHideParticipantList === 'function') {
      onSwitchHideParticipantList(checked);
    }
  }

  _handleOrganizerIsParticipantChange = checked => {
    const { onSwitchOrganizerIsParticipant } = this.props;

    if (typeof onSwitchOrganizerIsParticipant === 'function') {
      onSwitchOrganizerIsParticipant(checked);
    }
  }

  _handleChange = (field, event) => {
    const { value, onChange } = this.props;
    const { exactly } = this.state;

    const newValue = {
      ...value,
      [field]: Number(event.target.value),
    };

    if (exactly) { newValue.from = newValue.to = Number(event.target.value); }
    if (typeof onChange === 'function') {
      onChange(newValue);
    }
  }


  render() {
    const { viewer, isLoggedIn, value, required, error, hideParticipantList, organizerParticipates } = this.props;

    const triangleStyle = this.state.dropdownOpen ? styles.triangleOpen : styles.triangle ;
    const finalTriangleStyle = {
      ...triangleStyle,
      borderBottomColor: this.state.dropdownOpen ? colors.green : colors.blue,
    }

    return (
      <div style={styles.container} ref={node => { this._containerNode = node }}>
       {/*  <Input
          label={localizations.newSportunity_participants}
          ref="_inputNode"
          value={this.state.exactly ? value.to : `${value.from} - ${value.to}`}
          onFocus={this._openDropdown}
          required={required}
          error={error}
          readOnly
        /> */}
        <div style = {styles.title}>
        Number Of Participants
        </div>
        <ToggleDisplay show={!this.state.inValidPax || this.state.minNotReached}>
          <label style={styles.error}>{this.state.minNotReached ? localizations.newSportunity_participant_shouldnot_be_null : localizations.newSportunity_participantInvalid}</label>
        </ToggleDisplay>
        {/* <span style={finalTriangleStyle} onClick={this._toggleDropdown}/> */}
        {/* <Dropdown style={styles.dropdown} open={this.state.dropdownOpen}> */}
          <div style={styles.dropdownContent} onKeyPress={e => e.key === 'Enter' && this._closeDropdown()}>
            <div style={styles.column}>
              {
                !this.state.exactly &&
                  <div style={{ ...styles.inputGroup, marginBottom: 20 }}>
                    <div style={styles.label}>
                      {localizations.newSportunity_min}
                    </div>
                    <input
                      style={styles.number}
                      type="number"
                      value={value.from > 0 ? value.from : ''}
                      onChange={this._handleChange.bind(this, 'from')}
                    />
                  </div>
              }
              <div style={styles.inputGroup}>
                <div style={styles.label}>
                  { this.state.exactly ? localizations.newSportunity_exact : localizations.newSportunity_max}
                </div>
                <input
                  style={styles.number}
                  type="number"
                  value={value.to > 0 ? value.to : ''}
                  onChange={this._handleChange.bind(this, 'to')}
                />
              </div>
            </div>
           {/*  <div style={styles.exactly}>
              {localizations.newSportunity_exactly}
              <Switch
                containerStyle={styles.switch}
                checked={this.state.exactly}
                onChange={this._handleExactlyChange}
              />
            </div> */}
          </div>
          <div style={styles.hideParticipant}>
            {localizations.newSportunity_hide_participant_list}
            <Switch
              containerStyle={styles.switch}
              checked={hideParticipantList}
              onChange={this._handleHideParticipantChange}
            />
          </div>
          {isLoggedIn && viewer.me && viewer.me.profileType === 'PERSON' && this.props.price.cents === 0 && 
            <div style={styles.hideParticipant}>
              {localizations.newSportunity_participate}
              <Switch
                containerStyle={styles.switch}
                checked={organizerParticipates}
                onChange={this._handleOrganizerIsParticipantChange}
              />
            </div>
          }
        {/* </Dropdown> */}
      </div>
    );
  }
}

styles = {
  container: {
    position: 'relative',
    width: '100%',
    marginBottom: 50,
    fontFamily: 'Lato',
  },

  triangle: {
    position: 'absolute',
    right: 0,
    top: 35,
    width: 0,
    height: 0,

    transition: 'border 100ms',
    transitionOrigin: 'left',

    color: colors.blue,

    cursor: 'pointer',

    borderLeft: '8px solid transparent',
    borderRight: '8px solid transparent',
    borderTop: `8px solid ${colors.blue}`,
  },

  triangleOpen: {
    position: 'absolute',
    right: 0,
    top: 35,
    width: 0,
    height: 0,

    transition: 'border 100ms',
    transitionOrigin: 'left',

    color: colors.blue,

    cursor: 'pointer',

    borderLeft: '8px solid transparent',
    borderRight: '8px solid transparent',
    borderBottom: `8px solid ${colors.blue}`,
  },

  dropdown: {
    position: 'absolute',
    top: 70,
    width: '100%',
    overflow: 'hidden',
  },

  dropdownContent: {
    display: 'flex',
    justifyContent: 'space-between',
  },

  column: {
  },

  inputGroup: {
     marginTop :15,
    display: 'inline-block',
    width : 150
  },

  label: {
   display: 'inline-block',
   width : 40,
    fontFamily: 'Lato',
    fontSize: 16,
    lineHeight: '23px',
    color: '#515151',
    '@media (max-width: 768px)': {
      marginRight: 10,
    }
  },

  number: {
   display: 'inline-block',
    height: 35,
    width: 60,
   // border: '2px solid #5E9FDF',
    borderRadius: 3,
    textAlign: 'center',
    fontSize: 16,
    fontFamily: 18,
    lineHeight: 1,
    marginLeft: 10,
    color: 'rgba(146,146,146,0.87)',
    border: 'none',
    backgroundColor: '#E2E2E2',
  },

  exactly: {
    display: 'flex',
    alignItems: 'center',

    fontFamily: 'Lato',
    fontSize: 16,
    fontWeight: 500,
    lineHeight: '23px',
    color: '#515151',
  },
  hideParticipant: {
    display: 'flex',
    alignItems: 'center',

    fontFamily: 'Lato',
    fontSize: 16,
    fontWeight: 500,
    lineHeight: '23px',
    color: '#515151',
    marginTop: 15
  },
  error: {
    color: colors.red,
    fontSize: 14,
    marginTop: 15,
  },
  switch: {
    marginLeft: 10,
  },
  title : {
     fontSize : 18,
     fontFamily: 'Lato',
  }
}
export default Radium(Participants);
