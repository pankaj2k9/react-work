import React from 'react';
import Relay from 'react-relay'
import Radium from 'radium';

import { colors } from '../../theme';
import localizations from '../Localizations'

import Dropdown from './AddOrganizerDropdown';
import AddPlaceModal from './AddPlaceModal';
import FindPlaceModal from './FindPlaceModal.js';
import CalendarModal from './CalendarModal'

let styles;



class VenueOrAddress extends React.Component {
  state = {
    showDropdown: false,
    chosenModal: 0,
  }

  componentDidMount() {
      this.props.relay.setVariables({
          query: true,
	        filter: {
		        users: [this.props.viewer.me.id]
  	      },
          slotFilter: {
              sport: this.props.sport ? {sportID: this.props.sport.id} : null,
              users: [this.props.viewer.me.id]
          }
      })
  }

  componentWillReceiveProps = (nextProps) => {
    if (this.props.viewer.me && this.props.viewer.me.id && (this.props.viewer.me.id !== nextProps.viewer.me.id || nextProps.sport !== this.props.sport)) {
	    this.props.relay.setVariables({
		    query: true,
		    filter: {
			    users: [this.props.viewer.me.id]
		    },
		    slotFilter: {
			    sport: nextProps.sport ? {sportID: nextProps.sport.id} : null,
			    users: [this.props.viewer.me.id]
		    }
	    })
    }
  }

  _handleAddClick = (event) => {
    event.preventDefault();
    setTimeout(() => {
      if ((this.props.viewer.slots && this.props.viewer.slots.length > 0) || (this.props.viewer.infrastructures && this.props.viewer.infrastructures.length > 0))
        this.setState({ showDropdown: true });
      else 
        this._handleChooseModal(2);
    }, 200)
    
  }

  _handleCloseModal = () => {
    this.setState({showDropdown: false})
  }

  _handleChooseFindModal = () => {
    this.setState({
      chosenModal: 0
    })
  }

  _handleChooseModal = (value) => {
    this.setState({
      showDropdown: false,
      chosenModal: value
    })
  }

  _handleChooseAddress = (e) => {
    this._handleChooseFindModal();
    this._handleCloseModal();
    document.getElementById("search_address_div").style.display = "none";
    this.props.onChangeAddress(e);
  }

  _handleChooseSlot = (slot) => {
    this._handleChooseFindModal();
    this._handleCloseModal();
    this.props.onChangeSlot(slot);
  }

  _handleChooseInfrastructure = (infrastructure) => {
    this._handleChooseFindModal();
    this._handleCloseModal();
    this.props.onChangeInfrastructure(infrastructure);
  }

  render() {
    const { style, sport, viewer, isModifying, isLoggedIn, address, venue, infrastructure, slot } = this.props;
    const finalContainerStyles = Object.assign({}, styles.container, style);

    return (
      <div style={finalContainerStyles} ref={node => { this._containerNode = node; }}>
        

        {/* <button
            style={styles.add}
            onClick={this._handleAddClick}
          >
            {address ? localizations.newSportunity_place_modify : localizations.newSportunity_place_add}
        </button> */}

        {/* <AddPlaceModal
            viewer={viewer}
            isOpen={true}
            closeModal={this._handleCloseModal}
            chooseModal={this._handleChooseModal}
          /> */}

         <FindPlaceModal
            isLoggedIn={isLoggedIn}
            viewer={viewer}
            sport={sport}
            isOpen={true}
            openedModal={0}
            closeModal={this._handleChooseFindModal}
            onChooseSlot={this._handleChooseSlot}
            onChangeAddress={this._handleChooseAddress}
            onChooseInfrastructure={this._handleChooseInfrastructure}
            sportunityId={this.props.sportunityId}
            address = {address}
          />

          {venue && venue.name &&
            <div style={styles.text}>
              <h3 style = {{fontWeight :'bold'}}>Chosen Venue :</h3>
              {venue.name + ' - ' + infrastructure.name}
            </div>
          }
          {address && 
              <div style={styles.text}>
                  <h3 style = {{fontWeight :'bold'}}>Chosen Address :</h3>
                  {address}
              </div>
          }
      </div>
    );
  }
}

styles = {
  container: {
    fontFamily: 'Lato',
    position: 'relative',
    marginBottom: 27
  },
  label: {
    display: 'block',
    color: colors.blueLight,
    fontSize: 16,
    lineHeight: 1,
    marginBottom: 8,
  },

  add: {
    border: 'none',
    backgroundColor: colors.blue,
    color: colors.white,

    fontSize: 18,
    fontWeight: 500,
    lineHeight: 1,

    padding: '8.5px 13px 7.5px',

    cursor: 'pointer',

    borderRadius: 3,
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
  },
  text: {
    display: 'block',
    color: 'rgba(0, 0, 0, 0.64)',
    fontSize: 18,
    lineHeight: 1,
    marginBottom: 8,
  }
};

export default Relay.createContainer(Radium(VenueOrAddress), {
  initialVariables: {
    slotFilter: null,
    filter: null,
    query: false,
  },
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        ${CalendarModal.getFragment('viewer')}
        id
        me {
          id
          profileType
          isSubAccount
        }
        infrastructures (filter: $filter) @include (if: $query) {
          id
          name
          venue {
            id
            name 
            address {
              address, 
              city,
              zip,
              country
            }
            infrastructures {
                  id
                  name
                  authorized_managers {
                    user { id }
                    circle {
                      members { id }
                    }
                  }
            }
          }
          logo
          sport {
            id
            name {
              EN, FR
            }
            logo
          }
        }
        slots (filter: $slotFilter) @include (if: $query) {
          id
          venue {
              id
              name,
              address {
                  address, 
                  city,
                  zip,
                  country
              }
          },
          infrastructure {
              id, 
              name,
              logo,
              sport {
                  id
                  name {
                      EN,
                      FR
                  }
                  logo
              }
          },
          from,
          end, 
          price {
              cents, 
              currency
          },
          serie_information {
              firstDate
              lastDate
              remainingSlots
          }
        }
      }
    `
  }
});