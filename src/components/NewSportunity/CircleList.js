import React from 'react';
import PureComponent, { pure } from '../common/PureComponent';
import Relay from 'react-relay';
import Radium from 'radium';
import ReactTooltip from 'react-tooltip';

import { colors } from '../../theme';
import localizations from '../Localizations';

import Switch from '../common/Switch';
import SelectCircle from '../common/Inputs/SelectCircle';
import Dropdown from './Dropdown';
import Input from './Input';
import Paper from 'material-ui/Paper';
import {Tabs, Tab} from 'material-ui/Tabs';
 let styles;
 const isEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
 
 class CircleList extends PureComponent {

    state = {
        isLoading: false,
        inputContent: '',
        dropdownOpen: false,
        userListIsOpen: false,
        circleListIsOpen: false,
        circleList: []
    }

    componentDidMount() {
        window.addEventListener('click', this._handleClickOutside);
        

    }

    componentWillReceiveProps = (nextProps) => {
        if (this.state.circleList.length === 0 && ((nextProps.circlesList && nextProps.circlesList.length > 0) || (nextProps.circlesCurrentUserIsIn && nextProps.circlesCurrentUserIsIn.length > 0) || (nextProps.circlesFromClub && nextProps.circlesFromClub.length > 0))) {
            let circleList = [];
            nextProps.circlesList.forEach(edge => circleList.push(edge.node));

            if (nextProps.circlesFromClub && nextProps.circlesFromClub.length > 0) 
                nextProps.circlesFromClub.forEach(edge => circleList.push(edge.node));

            if (nextProps.circlesCurrentUserIsIn && nextProps.circlesCurrentUserIsIn.length > 0) 
                nextProps.circlesCurrentUserIsIn.forEach(edge => circleList.push(edge.node))

            this.setState({circleList});
        }
    }

    componentWillUnmount() {
        window.removeEventListener('click', this._handleClickOutside);
    }

    
    _handleClickOutside = event => {
        if (this.state.circleListIsOpen && this._containerCircleNode && !this._containerCircleNode.contains(event.target)) {
            this.setState({ 
                inputContent: '',
                userListIsOpen: false,
                circleListIsOpen: false,
            });
        }
        else if (this.state.userListIsOpen && this._containerUserNode && !this._containerUserNode.contains(event.target)) {
            this.setState({ 
                inputContent: '',
                userListIsOpen: false,
                circleListIsOpen: false,
            });
        }
        else if ((!this._containerUserNode || !this._containerUserNode.contains(event.target)) && 
            (!this._containerCircleNode || !this._containerCircleNode.contains(event.target))) {
            this._closeDropdown()

            this.props .relay.setVariables({
                requestUsersAutocompletion: false,
                sportId: null,
                pseudo: '_',
            })
        }
    }

    _openDropdown = () => {
        this.refs._inputNode._focus();
        this.setState({dropdownOpen: true})
    }

    _closeDropdown = () => {
      //  this.refs._inputNode._onBlur();
        this.setState({ 
            isLoading: false,
            inputContent: '',
            dropdownOpen: false,
            userListIsOpen: false,
            circleListIsOpen: false
        });
    }

    _toggleDropdown = () => {
        if (!this.state.dropdownOpen)
            this._openDropdown()
        else 
            this._closeDropdown()
    }

    _handleInputClick = () => {
        const { userListIsOpen } = this.state;
        if (!userListIsOpen)
            return this.setState({ userListIsOpen: true});
    }

    _handleAutocompleteClicked = (user) => {
        this.props.onChange(user);
        this.setState({
            inputContent: '',
        })

        setTimeout(() => this.setState({userListIsOpen: false}),20)
        
        setTimeout(() => {
            this.props.relay.setVariables({
                pseudo: '_',
                sportId: null,
                requestUsersAutocompletion: false
            });
        }, 400);
    }

    _handleInputChange = event => {
        this.setState({
            inputContent: event.target.value,
        })
        if (event.target.value.length)
            this.setState({
                userListIsOpen: false
            })
        if (event.target.value.length >= 1 && this.props.isLoggedIn) {
            this.setState({
                isLoading: true
            })

            if (isEmail.test(event.target.value)) {
                this.props.relay.setVariables({
                    email: event.target.value,
                    requestUsersByEmail: true, 
                    requestUsersAutocompletion: false,
                },readyState => {
                    if (readyState.done) {
                      setTimeout(() =>
                        this.setState({
                            isLoading: false
                        })
                      , 50);
                    }
                })
            }
            else {
                this.props.relay.setVariables({
                    pseudo: event.target.value,
                    requestUsersAutocompletion: true,
                    sportId: this.props.sport.id ? this.props.sport.id : this.props.sport.value ? this.props.sport.value : null,
                },readyState => {
                    if (readyState.done) {
                      setTimeout(() =>
                        this.setState({
                            isLoading: false
                        })
                      , 50);
                    }
                })
            }
            this.setState({
                userListIsOpen: true
            })    
        }
        else {
            this.props.relay.setVariables({
                pseudo: null,
                requestUsersAutocompletion: false,
                email: null,
                requestUsersByEmail: false
            })
        }
    }

    _handleRemoveSelection = () => {
        const { onChange } = this.props;
        
        if (typeof onChange === 'function') {
          setTimeout(() => onChange(), 20)
        }
    }

    _handleRemoveCircleSelection = () => {
        const { onChangeCircle } = this.props;
        
        if (typeof onChangeCircle === 'function') {
          setTimeout(() => onChangeCircle(), 20)
        }
    }

    _openCircleListDropdown = () => {
        this.setState({
            circleListIsOpen: true
        })
    }

    render() {

        const { userListIsOpen, inputContent, isLoading,  circleListIsOpen} = this.state;
        const { selectedOpponent, viewer, isOpenMatch, unknownOpponent, circleOfOpponents, error, circleList , renderStepActions } = this.props;

        let isEmailWritten = isEmail.test(inputContent);
        let autoCompletionList = isEmailWritten
            ?   viewer.users && viewer.users.edges.length > 0 
                ?   viewer.users.edges.map(edge => edge.node)
                :   []
            :   viewer.opponents && viewer.opponents.edges.length > 0 
                    ?   viewer.opponents.edges.map(edge => edge.node)
                    :   [];
        if (viewer.users && viewer.users.edges.length > 0) {
          viewer.users.edges.map(edge => edge.node).forEach(user => {
            if (autoCompletionList.findIndex(item => item.id === user.id) < 0)
              autoCompletionList.push(user)
          })
        }
        if (autoCompletionList.length > 0 && viewer.me)
            autoCompletionList = autoCompletionList.filter(user => user.id !== viewer.me.id)

	    const triangleStyle = this.state.dropdownOpen ? styles.triangleOpen : styles.triangle ;
	    const finalTriangleStyle = {
		    ...triangleStyle,
		    borderBottomColor: this.state.dropdownOpen ? colors.green : colors.blue,
	    };
        return (

            <div>
        
        {circleList && circleList.length > 0 &&

           <Paper zDepth={4} style={styles.paperStyle}>
           <div style={styles.section}>
           <Tabs style = {{ width:'calc(100% + 140px)', marginLeft: '-70px', marginRight: '-70px'}} inkBarStyle = {{background:'#5EA1D9'}}>

           <Tab label={localizations.myOpponent} value = "one" style = {{backgroundColor: '#FFFFFF', color: '#000000', 
												borderBottom : '1px solid  #9A9A9A'}}>
             
               
               <div style={{margin: '26px 70px', width: 300}} ref={node => { this._containerCircleNode = node; }}>
                  <Input
                    label=""
                    disabled={false}
                    onChange={()=> {}}
                    onClick={()=> {}}
                    placeholder='Identify my opponent'
                    required
                    value={''}
                    error={''}
                    errorMessage={''}
                    color="#5F9FDF"
                    ref={node => {this.circleCreateSection = node}}
                   />
                 
               </div>
               </Tab>

	             <Tab label={localizations.myOpponentPropose} value = "two" style = {{backgroundColor: '#FFFFFF', color: '#000000', 
												borderBottom : '1px solid  #9A9A9A'}}>
            
              
               <div style={{margin: '26px 70px', width: 300}} ref={node => { this._containerCircleNode = node; }}>
               <SelectCircle
                   label={localizations.newSportunity_opponent_circles_label}
                   list={circleList}
                   value={circleOfOpponents}
                   onClick={() => this.setState({ circleListIsOpen: true })}
                   onClose={() => setTimeout(() => this.setState({ circleListIsOpen: false }), 20)}
                   onChange={(el) => { this.props.onChangeCircle(el); setTimeout(() => this.setState({ circleListIsOpen: false }), 20) }}
                   clearSelection={this._handleRemoveCircleSelection}
                   placeholder={localizations.newSportunity_opponent_circles_select}
                   term={circleOfOpponents ? circleOfOpponents.name : ''}
                   disabled={unknownOpponent || isOpenMatch}
                 /> 
                 
               </div>
               </Tab>
               </Tabs>
               <hr style={styles.hr}></hr>
               {renderStepActions}
               </div>
              </Paper>  
              }
             
             </div>
        );
    }
}

var spinKeyframes = Radium.keyframes({
    '0%': { transform: 'rotate(0deg)' },
    '100%' :{ transform: 'rotate(360deg)' },
}, 'spin');

const stylesBases = {
  autocompletion_dropdown: {
    position: 'absolute',
    left: 0,

    width: '100%',
    maxHeight: 220,

    backgroundColor: colors.white,

    boxShadow: '0 2px 4px 0 rgba(0,0,0,0.24), 0 0 4px 0 rgba(0,0,0,0.12)',
    border: '2px solid rgba(94,159,223,0.83)',
    padding: 20,

    overflowY: 'scroll',
    overflowX: 'hidden',

    zIndex: 100,
  }
}

styles = {
    container: {
        position: 'relative',
        width: '100%',
        marginBottom: 25
    },
    dropdown: {
        position: 'absolute',
        top: 65,
        width: '100%',
        overflow: 'visible',
    },
    paperStyle: {
        padding: '8px 70px 1px',
        marginTop:'20px'
        
    },
	triangle: {
		position: 'absolute',
		right: 0,
		top: 35,
		width: 0,
		height: 0,

		transition: 'border 100ms',
		transitionOrigin: 'left',

		color: colors.blue,

		cursor: 'pointer',

		borderLeft: '8px solid transparent',
		borderRight: '8px solid transparent',
		borderTop: `8px solid ${colors.blue}`,
	},
	triangleOpen: {
		position: 'absolute',
		right: 0,
		top: 35,
		width: 0,
		height: 0,

		transition: 'border 100ms',
		transitionOrigin: 'left',

		color: colors.blue,

		cursor: 'pointer',

		borderLeft: '8px solid transparent',
		borderRight: '8px solid transparent',
		borderBottom: `8px solid ${colors.blue}`,
	},
    section: {
      //   backgroundColor: colors.lightGray,
        padding: '10px 0px 10px 0px',
        marginBottom: 10,
        borderRadius: 5
    },
    sectionTitle: {
        fontFamily: 'Lato',
        fontSize: '18px',
        marginBottom: 15,
        paddingBottom: 5, 
        borderBottom: '1px solid '+colors.darkGray,
        color: colors.darkGray
    },

    autocompletion_dropdown: {
      ...stylesBases.autocompletion_dropdown,
      top: 60,
    },

    removeCross: {
        float: 'right',
        width: 0,
        color: colors.gray,
        marginRight: '15px',
        cursor: 'pointer',
        fontSize: '16px',
    },

    list: {},

    listItem: {
        paddingBottom: 10,
        color: '#515151',
        fontSize: 20,
        fontWeight: 500,
        fontFamily: 'Lato',
        borderBottomWidth: 1,
        borderColor: colors.blue,
        borderStyle: 'solid',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'baseline',
        marginBottom: 5
    },

    listItemClickable: {
        paddingBottom: 10,
        color: '#515151',
        fontSize: 20,
        fontWeight: 500,
        fontFamily: 'Lato',
        borderBottomWidth: 1,
        borderColor: colors.blue,
        borderStyle: 'solid',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginBottom: 5,
        cursor: 'pointer'
    },
    listItemClickableFullWidth: {
        paddingBottom: 10,
        color: '#515151',
        fontSize: 20,
        fontWeight: 500,
        fontFamily: 'Lato',
        borderBottomWidth: 1,
        borderColor: colors.blue,
        borderStyle: 'solid',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 5,
        cursor: 'pointer'
    },
    listItemClickableColumn: {
        paddingBottom: 10,
        color: '#515151',
        fontSize: 20,
        fontWeight: 500,
        fontFamily: 'Lato',
        borderBottomWidth: 1,
        borderColor: colors.blue,
        borderStyle: 'solid',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        marginBottom: 5,
        cursor: 'pointer'
    },
    note: {
        fontSize: 16, 
        cursor: 'auto',
        fontStyle: 'italic',
        marginBottom: 10
    },
    avatar: {
        width: 39,
        height: 39,
        marginRight: 10,
        color: colors.blue,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        borderRadius: '50%',
    },
    closeCross: {
        position: 'absolute',
        right: 0,
        top: 30,
        width: 0, 
        height: 0,
        color: colors.gray,
        marginRight: '15px',
        cursor: 'pointer',
        fontSize: '16px',
    },
    cancelIcon: {
        marginRight: 15,
    },
    spinnerItem: {
        borderLeft: '6px solid #f3f3f3',
        borderRight: '6px solid #f3f3f3',
        borderBottom: '6px solid #f3f3f3',
        borderTop: '6px solid #3498db',
        borderRadius: '50%',
        width: '20px',
        height: '20px',
        marginRight: '20px',
        animation: 'x 1.5s ease 0s infinite',
        animationName: spinKeyframes,
    },
    inputRow: {
        marginBottom: 25,
        position: 'relative',
    },
    switchRow: {
        display: 'flex',
        alignItems: 'center',
        marginBottom: 25,
        width : '40%',
    },
    label: {
        fontFamily: 'Lato',
        fontSize: '18px',
        //textAlign: 'right',
        lineHeight: 1,
        color: '#316394',
        display: 'block',
        marginRight: 20,
        flex: 1
    },
    disabledLabel: {
        fontFamily: 'Lato',
        fontSize: '18px',
        //textAlign: 'right',
        lineHeight: 1,
        color: '#D1D1D1',
        display: 'block',
        marginRight: 20,
        flex: 1
      },
    openMatchToolTip: {
        marginLeft: 10,
        fontSize: 16,
        cursor: 'pointer'
    },
    buttonIcon: {
        color: colors.blue,
        position: 'relative',
        marginLeft: 10
    },
    numberContainer: {
        position: 'absolute',
        top: '4px',
        left: '15px',
        width: 24,
        textAlign: 'center'
    },
    number: {
        fontSize: 17,
        fontWeight: 'bold'
    },
    hr: {
        marginLeft: -70,
        marginRight: -70,
    },
};

export default Relay.createContainer(Radium(CircleList), {
    initialVariables: {
        pseudo: null,
        requestUsersAutocompletion: false,
        sportId: null,
        email: null,
        requestUsersByEmail: false,
      },
      fragments: {
        viewer: () => Relay.QL`
          fragment on Viewer {
            me {
                id
            }
            opponents (sportId: $sportId, pseudo: $pseudo, first: 8) @include(if: $requestUsersAutocompletion) {
                edges {
                    node {
                        id
                        avatar
                        pseudo
                    }
                }
            }
            users (email: $email, first: 10) @include(if: $requestUsersByEmail) {
                edges {
                    node {
                        id
                        avatar
                        pseudo
                    }
                }
            }
            users (pseudo: $pseudo, first: 10) @include(if: $requestUsersAutocompletion) {
                edges {
                    node {
                        id
                        avatar
                        pseudo
                    }
                }
            }
          }
        `,
      },
});