import Relay from 'react-relay';

export default class UpdateSportunityMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation {updateSportunity}`;
  }

  getVariables() {
    return {
      sportunityID: this.props.sportunity.id,
      sportunity: {
        title: this.props.sportunity.title,
        description: this.props.sportunity.description,
        address: this.props.sportunity.address,
        venue: this.props.sportunity.venue,
        infrastructure: this.props.sportunity.infrastructure,
        slot: this.props.sportunity.slot,
        organizers: this.props.sportunity.organizers,
        pendingOrganizers: this.props.sportunity.pendingOrganizers,
        secondaryOrganizersPaymentMethod: this.props.sportunity.secondaryOrganizersPaymentMethod,
        secondaryOrganizersPaymentByWallet: this.props.sportunity.secondaryOrganizersPaymentByWallet,
        sport: this.props.sportunity.sport,
        mode: this.props.sportunity.mode,
        kind: this.props.sportunity.kind,
        participantRange: this.props.sportunity.participantRange,
        price: this.props.sportunity.price,
        ageRestriction: this.props.sportunity.ageRestriction,
        sexRestriction: this.props.sportunity.sexRestriction,
        beginning_date: this.props.sportunity.beginningDate,
        ending_date: this.props.sportunity.endingDate,
	      survey_dates: this.props.sportunity.survey_dates,
        participants: this.props.sportunity.participants, 
        invited: this.props.sportunity.invited,
        invited_circles: this.props.sportunity.invited_circles,
        price_for_circle: this.props.sportunity.price_for_circle,
        notification_preference: this.props.sportunity.notification_preference,
        privacy_switch_preference: this.props.sportunity.privacy_switch_preference,
        modifyRepeatedSportunities: this.props.modifyRepeatedSportunities,
        hide_participant_list: this.props.sportunity.hide_participant_list,
        sportunityType: this.props.sportunity.sportunityType,
        game_information: this.props.sportunity.game_information
      },
      notify_people: this.props.notify_people,
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on updateSportunityPayload{
        viewer {
          sportunity {
						id,
            title
            description
            kind
            privacy_switch_preference {
              privacy_switch_type,
              switch_privacy_x_days_before
            }
            participants {
              id
              avatar
              pseudo
            },
            waiting {
              id,
              pseudo
              avatar
            }
            invited {
              user {
                id,
                pseudo
              }
              answer
            }
            survey {
              isSurveyTransformed
              surveyDates {
                beginning_date
                ending_date
              }
            }
            invited_circles (last: 10) {
              edges {
                node {
                  id,
                  name,
                  members {
                    id
                  }
                }
              }
            }
            price_for_circle
            notification_preference {
              notification_type,
              send_notification_x_days_before
            }
            participantRange {
              from
              to
            }
            beginning_date
            ending_date
            price {
              currency,
              cents,
            },
            sport {
              sport {
                id,
                name {
                  EN
                }
                logo
              }
              positions {
                id
                EN
              }
              certificates {
                id,
                name {
                  EN
                }
              }
              levels {
                id
                EN {
                  name
                  skillLevel
                }
              }
            }
            address {
              address
              country
              city
              position {
                lat
                lng
              }
            }
            organizers {
              organizer {
                id
                pseudo
              }
              isAdmin
              role
              price {
                cents,
                currency
              }
            }
            pendingOrganizers {
              id
              circles (last: 20) {
                edges {
                  node {
                    id, 
                    name,
                    memberCount
                  }
                }
              }
              isAdmin
              role
              price {
                cents,
                currency
              },
              secondaryOrganizerType {
                id
              }
              customSecondaryOrganizerType
            }
            venue {
              id
              name
              address {
                address,
                city,
                country
              }
            }
            infrastructure {
              id
              name
            }
            slot {
              id
              from
              end
            }
            status
            ageRestriction {from, to},
            sexRestriction,
            sportunityType {
              id,
              name {
                FR,
                EN
              }
            }
					}
        }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        viewer: this.props.viewer.id,
      },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
      }
    `,
  };
}
