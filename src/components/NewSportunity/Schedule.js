import React from 'react';
import Radium from 'radium';
import { colors, fonts } from '../../theme';
import format from 'date-fns/format';
import localizations from '../Localizations';
import {Link} from 'react-router';
import ReactTooltip from 'react-tooltip';
import CustomTab from '../common/CustomTab';

import AddTimeDropdown from './AddTimeDropdown';
import Switch from "../common/Switch";
import AddSchedule from "./AddSchedule";

import {Tabs, Tab} from 'material-ui/Tabs';

let styles;

const Item = ({ beginningDate, endingDate, repeat, scheduleId, onDelete, onEdit, isSurvey, isSurveyTransformed, isEdit, index }) => {
  return (
    <li style={styles.item} onClick={isSurvey ? null : onEdit.bind(this, scheduleId)} >
	    {!isSurveyTransformed &&
		    <div style={styles.close} value={scheduleId} onClick={onDelete.bind(this, scheduleId)}>
			    <i className="fa fa-times" value={scheduleId} style={styles.icon} aria-hidden="true"/>
		    </div>
	    }
	    {isSurvey && !isSurveyTransformed &&
		    <div style={isEdit ? {...styles.edit, backgroundColor: colors.green} : styles.edit} value={scheduleId} onClick={onEdit.bind(this, scheduleId, index)}>
			    <i className="fa fa-pencil" value={scheduleId} style={styles.icon} aria-hidden="true"/>
		    </div>
	    }
      {format(beginningDate, 'DD/MM/YYYY') !== format(endingDate, 'DD/MM/YYYY') 

      ? <div style={styles.editWrapper}>
          <div style={styles.starting}  value={scheduleId}>
            {format(beginningDate, 'DD/MM/YYYY')}
            <time style={styles.time} value={scheduleId} >
              {format(beginningDate, 'H:mm')}
            </time>
          </div>
          <div style={styles.starting}  value={scheduleId}>
            {format(endingDate, 'DD/MM/YYYY')}
            <time style={styles.time} value={scheduleId} >
              {format(endingDate, 'H:mm')}
            </time>
          </div>
          {repeat > 0 && <span style={styles.meetingLabel}>{localizations.newSportunity_schedule_first_date}</span>}
        </div>

      : <div style={styles.editWrapper}>
          <div style={styles.starting}  value={scheduleId}>
            {format(beginningDate, 'DD/MM/YYYY')}
          </div>
          <time style={styles.time} value={scheduleId} >
            {format(beginningDate, 'H:mm')} - {format(endingDate, 'H:mm')}
          </time>
          {repeat > 0 && <span style={styles.meetingLabel}>{localizations.newSportunity_schedule_first_date}</span>}
        </div>
      }

      {repeat > 0 &&
        <div style={styles.editWrapper}>
          <div style={styles.starting}  value={scheduleId}>{format(new Date(new Date(beginningDate).getTime()+repeat*7*24*3600*1000), 'DD/MM/YYYY')}</div>
          <time style={styles.time} value={scheduleId} >
            {format(beginningDate, 'H:mm')} - {format(endingDate, 'H:mm')}
          </time>
          <span style={styles.meetingLabel}>{localizations.newSportunity_schedule_last_date}</span>
        </div>      
      }
      {
        repeat > 0 && 
          <div style={styles.iterationNumberLabel}>{localizations.newSportunity_schedule_total_number_of_iteration}: {repeat}</div>
      }
    </li>
  );
}

class Schedule extends React.Component {
  state = {
    dropdownOpen: true,
    isEdit: false,
      index: null,
      poll_is_active:false
  }

  componentDidMount() {
    window.addEventListener('click', this._onClickOutside);
  }


  componentWillUnmount() {
    window.removeEventListener('click', this._onClickOutside);
  }


  _onClickOutside = (event) => {
    const { dropdownOpen } = this.state;
    if (dropdownOpen) {
      if (!this._containerNode.contains(event.target) && event.target.className.search('react-datepicker') < 0) {
        this._closeDropdown();
      }
    }
  }

  _openDropdown = () => this.setState({ dropdownOpen: true });

  _closeDropdown = () => this.setState({ dropdownOpen: false });

  _handleSubmit = (...args) => {
    this.props.onSubmit(...args);
    this.setState({ dropdownOpen: false, isEdit: false });
  }

  _handleItemDelete = (scheduleId) => {
		this.props.onDelete(scheduleId);
  }

  _handleAddClick = (event) => {
    event.preventDefault();
    this.setState({index: null, isEdit: false, dropdownOpen: false})
    setTimeout(() => {
      this._openDropdown();
      this.props.onAdd();
    }, 200)
  }

	_handleEditClick = (scheduleId, index) => {
		// event.preventDefault();
    this.setState({
      isEdit: true,
      index,
      dropdownOpen: false
    })
		this.props.onEdit(scheduleId)
		setTimeout(() => this._openDropdown(), 200)
	}

  poll_is_active = () => {
    if (!this.state.poll_is_active)
    {
      this.props.onChangeSurvey();
    }
    this.setState({
      poll_is_active: true,
    })
  }

  poll_is_inactive = () => {
    if (this.state.poll_is_active)
    {
      this.props.onChangeSurvey();
    }
    this.setState({
      poll_is_active: false,
    })
  }

  singleDateRender = () => {
    const { scheduleId, beginningDate, endingDate, repeat, schedules, error, isSurvey, isSurveyTransformed } = this.props;
    const { localBeginningDate, localEndingDate } = this.state;
    let sortedSchedules = schedules.sort((a, b) => { return a.beginningDate - b.beginningDate })
    return (
      <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', marginTop: '20px' }}>
        <AddSchedule
          style={isSurvey ? { display: "block" } : { display: "none" }}
          beginningDate={beginningDate}
          endingDate={endingDate}
          scheduleId={scheduleId}
          onSubmit={this._handleSubmit}
          isModifying={this.props.isModifying}
          isEdit={this.state.isEdit}
          disabled={isSurveyTransformed}
          index={this.state.index}
        />

        <AddTimeDropdown
          style={!isSurvey ? { display: "block" } : { display: "none" }}
          beginningDate={beginningDate}
          endingDate={endingDate}
          scheduleId={scheduleId}
          repeat={repeat}
          onSubmit={this._handleSubmit}
          isModifying={this.props.isModifying}
          repeatDisabled={true}
        />
      </div>
    )
  }


  weeklyReceiptionRender = () => {
    const { scheduleId, beginningDate, endingDate, repeat, schedules, error, isSurvey, isSurveyTransformed } = this.props;
    const { localBeginningDate, localEndingDate } = this.state;
    let sortedSchedules = schedules.sort((a, b) => { return a.beginningDate - b.beginningDate })
   return( <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', marginTop: '20px' }}>
      <AddSchedule
        style={isSurvey ? { display: "block" } : { display: "none" }}
        beginningDate={beginningDate}
        endingDate={endingDate}
        scheduleId={scheduleId}
        onSubmit={this._handleSubmit}
        isModifying={this.props.isModifying}
        isEdit={this.state.isEdit}
        disabled={isSurveyTransformed}
        index={this.state.index}
      />

      <AddTimeDropdown
        style={!isSurvey ? { display: "block" } : { display: "none" }}
        beginningDate={beginningDate}
        endingDate={endingDate}
        scheduleId={scheduleId}
        repeat={repeat}
        onSubmit={this._handleSubmit}
        isModifying={this.props.isModifying}
        repeatDisabled={false}
      />
   </div>
   )
  }


  pollRender = () => {
    const { scheduleId, beginningDate, endingDate, repeat, schedules, error, isSurvey, isSurveyTransformed } = this.props;
    const { localBeginningDate, localEndingDate } = this.state;
    let sortedSchedules = schedules.sort((a, b) => { return a.beginningDate - b.beginningDate })
    return (
      <div>
      <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', marginTop: '20px' }}>
        <AddSchedule
          style={isSurvey ? { display: "block" } : { display: "none" }}
          beginningDate={beginningDate}
          endingDate={endingDate}
          scheduleId={scheduleId}
          onSubmit={this._handleSubmit}
          isModifying={this.props.isModifying}
          isEdit={this.state.isEdit}
          disabled={isSurveyTransformed}
          index={this.state.index}
        />

        <AddTimeDropdown
          style={!isSurvey ? { display: "block" } : { display: "none" }}
          beginningDate={beginningDate}
          endingDate={endingDate}
          scheduleId={scheduleId}
          repeat={repeat}
          onSubmit={this._handleSubmit}
          isModifying={this.props.isModifying}
        />
      </div>
      <div style={{ display: 'none', alignItems: 'center', marginTop: '20px' }}>
        <label style={styles.label}>
          <ReactTooltip effect="solid" multiline={true} />
          {localizations.newSportunity_survey_label}
          <Link target='_blank' to='/faq/tutorial/how-to-do-a-participants-survey'>
            <i
              data-tip={localizations.newSportunity_survey_tooltip}
              style={styles.sportToolTipIcon}
              className="fa fa-question-circle"
              aria-hidden="true"
            />
          </Link>
        </label>
        <Switch
          checked={this.state.poll_is_active}
          disabled={isSurveyTransformed}
          onChange={this.props.onChangeSurvey}
        />
      </div>
      </div>
    )
  }

  render() {
    const { scheduleId, beginningDate, endingDate, repeat, schedules, error, isSurvey, isSurveyTransformed } = this.props;
    const { localBeginningDate, localEndingDate } = this.state;

    let sortedSchedules = schedules.sort((a, b) => {return a.beginningDate - b.beginningDate})

		return (
      <div style={styles.container} ref={node => { this._containerNode = node; }}>
        <CustomTab 
           tab1={this.singleDateRender()}
           tab2={this.weeklyReceiptionRender()}
           tab3={this.weeklyReceiptionRender()}
           tab1Level="Single Date"
           tab2Level="Weekly Repetetion"
           tab3Level="Poll"
           />
       
	      <ul style={styles.list}>

		      { sortedSchedules.map((schedule, index) =>
			      <Item
				      key={index}
				      beginningDate={schedule.beginningDate}
				      endingDate={schedule.endingDate}
				      repeat={schedule.repeat}
				      onDelete={this._handleItemDelete}
				      onEdit={this._handleEditClick}
				      scheduleId={schedule.scheduleId}
                      index={index}
                      isSurvey={isSurvey}
				      isSurveyTransformed={isSurveyTransformed}
              isEdit={this.state.dropdownOpen && this.state.isEdit && schedule.scheduleId === scheduleId}
			      />
		      )

		      }
        </ul>
       
      </div>
    );
  }
}

Schedule.defaultProps = {
  items: [],
};


styles = {
	sportToolTipIcon: {
		marginLeft: 15,
		fontSize: 22,
		cursor: 'pointer',
    textDecoration: 'none',
    color: colors.blue
	},
  container: {
    position: 'relative',
    fontFamily: 'Lato',
    marginBottom: 27,
  },
	editWrapper : {
		cursor: 'pointer',
    borderBottom: '1px solid '+colors.blue,
    marginBottom:15
	},
  meetingLabel: {
    fontSize: 18,
    float: 'right'
  },
  iterationNumberLabel: {
    fontSize: 18
  },

  list: {
    width : '50%',
    marginLeft:'50%',
    marginBottom: 1.8,
    marginTop:'60px',
  },
  add: {
    border: 'none',
    backgroundColor: colors.blue,
    color: colors.white,

    fontSize: 18,
    fontWeight: 500,
    lineHeight: 1,

    padding: '8.5px 13px 7.5px',

    cursor: 'pointer',

    borderRadius: 3,
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
  },

	label: {
		fontFamily: 'Lato',
		fontSize: '22px',
		//textAlign: 'right',
		lineHeight: 1,
		color: '#316394',
		display: 'block',
		marginRight: 20,
		flex: 1
	},

  addError: {
    border: 'none',
    backgroundColor: colors.red,
    color: colors.white,

    fontSize: 18,
    fontWeight: 500,
    lineHeight: 1,

    padding: '8.5px 13px 7.5px',

    cursor: 'pointer',

    borderRadius: 3,
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
  },

  item: {
    position: 'relative',
    fontFamily: 'Lato',
    fontSize: fonts.size.xl,
    color: 'rgba(0,0,0,0.64)',
  },

  close: {
    position: 'absolute',
    top: '50%',
    right: 0,

    transform: 'translateY(-50%)',

    width: 20,
    height: 20,

    cursor: 'pointer',

    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',

    borderRadius: '50%',

    backgroundColor: '#5E9FDF',
    boxShadow: '0 0 2px 0 rgba(0,0,0,0.12), 0 1px 2px 0 rgba(0,0,0,0.24)',
  },

	edit: {
    position: 'absolute',
    top: '50%',
    right: 30,

    transform: 'translateY(-50%)',

    width: 20,
    height: 20,

    cursor: 'pointer',

    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',

    borderRadius: '50%',

    backgroundColor: '#5E9FDF',
    boxShadow: '0 0 2px 0 rgba(0,0,0,0.12), 0 1px 2px 0 rgba(0,0,0,0.24)',
  },

  icon: {
    color: colors.white,
    fontSize: 12,
  },

  time: {
    fontSize: '18px',
    fontWeight: 500,
    lineHeight: 1.2,
    color: colors.black,
    marginLeft: 10,
  },

  repeat: {
    lineHeight: 1.2,
  },

  starting: {
    fontSize: '20px',
    marginBottom: 16,
    marginTop: 8,
    color: '#5E9FDF',
  },
};


export default Radium(Schedule);
