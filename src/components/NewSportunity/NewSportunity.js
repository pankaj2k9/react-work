import React from 'react';
import PureComponent, { pure } from '../common/PureComponent'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment'
import Relay from 'react-relay';
import { Link } from 'react-router';
import withRouter from 'react-router/lib/withRouter';
import ToggleDisplay from 'react-toggle-display'
import AlertContainer from 'react-alert';
import ReactTooltip from 'react-tooltip'
import { cloneDeep, isEqual } from 'lodash';

import mangoPay from 'mangopay-cardregistration-js-kit'

import AppHeader from '../common/Header/Header.js'
import Loading from '../common/Loading/Loading.js'

import Input from './Input';
import Select from './Select';
import Button from './Button';
import SelectTemplate from './SelectTemplate';
import Switch from '../common/Switch';

import Price from './Price';
import Participants from './Participants';


import Sports from './Sports';
import SportLevels from './SportLevels';
import Address from './Address';
import Invited from './Invited';
import Organizers from './Organizers';
import Schedule from './Schedule';
import Details from './Details';
import VenueSlots from './VenueSlots';
import Privacy from './Privacy';
import NotificationToInvitees from './NotificationToInvitees';
import VenueOrAddress from './VenueOrAddress';
import Opponent from './Opponent';
import CircleList from './CircleList';
import ParticipantList from './ParticipantList';
import InvitedCircleDetails from './InvitedCircleDetails';
import PrivatePrice from './PrivatePrice';



import ConfirmCreationPopup from './ConfirmCreationPopup';
import AddBankAccountPopup from './AddBankAccountPopup';
import AddACardPopup from './AddACardPopup';
import CompleteBusinessProfilePopup from '../EventView/CompleteBusinessProfilePopup';
import CompletePersonProfilePopup from '../EventView/CompletePersonProfilePopup';

import NewSportunityMutation from './NewSportunityMutation';
import UpdateSportunityMutation from './UpdateSportunityMutation';
import NewSportunityTemplateMutation from './NewSportunityTemplateMutation';
import UpdateSportunityTemplateMutation from './UpdateSportunityTemplateMutation';
import RemoveSportunityTemplateMutation from './RemoveSportunityTemplateMutation';
import AddBankAccountMutation from './AddBankAccountMutation';
import RegisterCardDataMutation from '../EventView/RegisterCardDataMutation'
import UpdateUserProfileMutation from '../EventView/UpdateUserProfileMutation';
import CircleMutation from '../Circle/CircleMutation'

import {confirmModal} from '../common/ConfirmationModal'
import { colors } from '../../theme';
import Footer from '../common/Footer/Footer'
import localizations from '../Localizations'

import { mangoPayUrl, mangoPayClientId } from '../../../constants.json';

mangoPay.cardRegistration.baseURL = mangoPayUrl;
mangoPay.cardRegistration.clientId = mangoPayClientId;

import Radium from 'radium'




/* import {
  Step,
  Stepper,
  StepLabel,
  StepContent,
} from 'material-ui/Stepper'; */
import Paper from 'material-ui/Paper';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';

let styles;

const isEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

const Subtitle = pure(({ children, withoutMargin }) => (
  <h2 style={withoutMargin ? styles.subtitleWithoutMargin : styles.subtitle}>{children}</h2>)
);
const Column = pure(Radium(({ children }) => (<div style={styles.column}>{children}</div>)));

let nextScheduleId = 1

let initialState = {
  fields: {
    title: '',
    description: '',
    participantRange: {
      from: 0,
      to: 0,
    },
    price: {
      cents: 0,
      currency: 'CHF',
    },
    venue: {
      id: '',
      name: '',
      price: {
        cents: 0,
        currency: 'CHF',
      },
      address: {
        address: '',
      },
    },
    infrastructure: {
      id: '',
      name: ''
    },
    slot: {
      id: '',
      from: '',
      end: '',
      price: {
        cents: 0,
        currency: 'CHF'
      }
    },
    schedules: [],
	  isSurveyTransformed: false,
    beginningDate: '',
    endingDate: '',
    repeat: 0,
    scheduleId: 0,
    sport: null,
    levelFrom: null,
    levelTo: null,
    positions: [],
    certificates: [],
    organizers: [],
    circlesOfPendingOrganizers: [],
    private: true,
    autoSwitchPrivacy: false,
    autoSwitchPrivacyXDaysBefore: 15,
    invited: null,
    invited_circles: null,
    invited_circles_and_prices: [],
    notificationType: 'Now',
    notificationAutoXDaysBefore: 15,
    hideParticipantList: false,
    sportunityType: null,
    opponent: null,
    isOpenMatch: false,
    unknownOpponent: false,
    circleOfOpponents: null,

    address: {
      address: '',
      city: '',
      country: ''
    },
    organizerParticipates: false,
    organizerParticipation: 0,

    ageRestriction: {
      from: 0,
      to: 100
    },
    sexRestriction: 'NONE',
  },

  saveEventsType: false,
  allSportsLoaded: false,
  sportSearch: '',

  confirmPopupOpen: false,
  addBankAccountOpen: false,
  addCardPopupOpen: false,
  displayCompletePersonProfilePopup: false,
  displayCompleteBusinessProfilePopup: false,
  bankAcccountJustAdded: '',
  submitClicked: false,
  cardJustAdded: false,
  selectedCard: '',
  selectedCardToPaySecondaryOrganizers: '',
  paySecondaryOrganizersWithWallet: false,
  loading: true,
  process: false,
  language: localizations.getLanguage(),
  errors: [],
  isModifying: false,
  isSurvey: false,
  isReorganizing: false,
  isModifyingASerie: false,
  isParticipant: false,
  selectedTemplate: null,
  fromTemplate: false,
  saveTemplate: true,
  finished: false,
  selectedTab: 1,
  circleList:[],
}

class NewSportunity extends PureComponent {

  constructor(props) {
    super(props);
    this.alertOptions = {
      offset: 60,
      position: 'top right',
      theme: 'light',
      transition: 'fade',
    };
    this.state = initialState;

    this.handleChangeTabButtonPress = this.handleChangeTabButtonPress.bind(this);

  }

  _setLanguage = (language) => {
    this.setState({ language: language })
  }

  _validate(step = 0) {
    const { fields } = this.state;
    const errors = [];

    
    if (!fields.sport && step === 1) {
      errors.push({
        element: 'Sport',
        message: 'No sports provided',
      })
    }

 

    if (!fields.address.address && step === 2) {
      errors.push({
        element: 'Address',
        message: 'No address provided',
      });
    }


    if (((!fields.beginningDate || !fields.endingDate) && !this.state.isSurvey) && step === 3) {
      errors.push({
        element: 'Date',
        message:'No date provided',
      });
    }

    if ((fields.beginningDate && fields.endingDate && fields.endingDate < fields.beginningDate) && step === 3) {
      errors.push({
        element: 'Date',
        message:'Invalid hours range',
      });
    }


    if (!fields.title && step === 0) {
      errors.push({
        element: 'Name',
        message:'No title provided',
      });
    }

    if (!fields.description && step === 0) {
      errors.push({
        element: 'Description',
        message: 'No description provided',
      });
    }

    if (!fields.price && step === 5) {
      errors.push({
        element: 'Price',
        message: 'No price provided',
      });
    }

    if ((!fields.participantRange.from || !fields.participantRange.to) && step === 4) {
      errors.push({
        element: 'ParticipantRange',
        message: 'No number of participants provided',
      });
    }
    else if ((fields.participantRange.from > fields.participantRange.to) && step === 4) {
      errors.push({
        element: 'ParticipantRange',
        message: 'Invalid number of participants',
      });
    }

    if ((fields.private && fields.autoSwitchPrivacy && new Date(new Date(fields.beginningDate) - fields.autoSwitchPrivacyXDaysBefore * 24 * 3600 * 1000) < new Date()) && step === 4) {
      errors.push({
        element: 'Privacy',
        message: localizations.newSportunity_autoSwitchPrivacyXDaysError
      })
    }

    if (fields.notificationType === 'Automatically' && new Date(new Date(fields.beginningDate) - fields.notificationAutoXDaysBefore * 24 * 3600 * 1000) < new Date()) {
      errors.push({
        element: 'NotificationToInvitees',
        message: localizations.newSportunity_notificationPreferenceAutomaticallyNumberOfDaysError
      })
    }
    
    const sportunityTypesList = fields.sport && this.props.viewer.sports.edges.find(({ node }) => this._translatedName(node.name) === fields.sport.name)
    ? this.props.viewer.sports.edges
      .find(({ node }) => this._translatedName(node.name) === fields.sport.name).node.sportunityTypes
      .map(p => ({ name: this._translatedName(p.name), value: p.id, isScoreRelevant: p.isScoreRelevant }))
    : [];

    if (fields.sportunityType && sportunityTypesList.findIndex(type => type.value === fields.sportunityType.value && type.isScoreRelevant) >= 0 && 
      !fields.unknownOpponent && !fields.isOpenMatch && !fields.opponent && !fields.circleOfOpponents) {
      errors.push({
        element: 'Opponent',
        message: localizations.newSportunity_opponent_error
      })
    }

    /*let allParticipants = []
    if (fields.invited_circles_and_prices && fields.invited_circles_and_prices.length > 0) {
      fields.invited_circles_and_prices.forEach(item => {
        if (item.participantByDefault) {
          item.circle.members.forEach(member => {
            if (allParticipants.findIndex(participant => participant === member.id) < 0)
              allParticipants.push(member.id)
          })
        }
      })

      if (this.props.viewer.sportunity && this.props.viewer.sportunity.participants && this.props.viewer.sportunity.participants.length > 0) {
        this.props.viewer.sportunity.participants.forEach(item => {
          if (allParticipants.findIndex(participant => participant === item) < 0)
            allParticipants.push(item)
        })
      }
  }*/

    if ((fields.organizers && fields.organizers.length > 0) || (fields.circlesOfPendingOrganizers && fields.circlesOfPendingOrganizers.length > 0)) {
      let missingOrganizerType = false; 
      if (fields.organizers && fields.organizers.length > 0) {
        fields.organizers.forEach(organizer => {
          if (!organizer.secondaryOrganizerType && !organizer.customSecondaryOrganizerType) 
            missingOrganizerType = true;
        })
      }
      if (fields.circlesOfPendingOrganizers && fields.circlesOfPendingOrganizers.length > 0) {
        fields.circlesOfPendingOrganizers.forEach(organizer => {
          if (!organizer.secondaryOrganizerType && !organizer.customSecondaryOrganizerType) 
            missingOrganizerType = true;
        })
      }
      if (missingOrganizerType) {
        errors.push({
          element: 'Organizers',
          message: localizations.newSportunity_schedule_field_error
        })
      }
    }

    /*if (fields.organizers && fields.organizers.length > 0) {
      let totalCost = 0 ; 
      fields.organizers.forEach(organizer => {
        totalCost = totalCost + organizer.price.cents;
      });
      let minimumRevenue = 0 ;
      let fees = this.props.viewer.me ? this.props.viewer.me.fees / 100 : 0.2 ;
      let price = fields.price.cents ; 
      if (fields.price)
        minimumRevenue = (Math.round((fields.participantRange.from * price * (1 - fees)) * 100) / 100)

      if (totalCost > minimumRevenue)
        errors.push({
          element: 'Price',
          message: localizations.newSportunity_organizerPriceError
        })
    }*/

    /*if (fields.participantRange.to < allParticipants.length) {
      errors.push({
        element: 'ParticipantRange',
        message: localizations.newSportunity_autoParticipateErrorToManyParticipants
      })
    }*/
    return errors;
  }
  _updateCircleList = () => {
    let circlesCurrentUserIsInConst = this.props.viewer && this.props.viewer.me && this.props.viewer.me.circlesUserIsIn && this.props.viewer.me.circlesUserIsIn.edges && this.props.viewer.me.circlesUserIsIn.edges.length > 0
      ? this.props.viewer.me.circlesUserIsIn.edges
        .map(edge => {
          if (edge.node.isCircleUsableByMembers)
            return edge
          else
            return false
        })
        .filter(i => Boolean(i))
      : [];

    let circlesList = this.props.viewer.me && this.props.viewer.me.circles && this.props.viewer.me.circles.edges ? this.props.viewer.me.circles.edges.filter(edge => edge.node.type === "TEAMS" || edge.node.type === "CLUBS") : [];
    let circlesCurrentUserIsIn = circlesCurrentUserIsInConst.filter(edge => edge.node.type === "TEAMS" || edge.node.type === "CLUBS");
    let circlesFromClub = [];
    if(this.state.circleList.length === 0 && ((circlesList && circlesList.length > 0) || (circlesCurrentUserIsIn && circlesCurrentUserIsIn.length > 0) || (circlesFromClub && circlesFromClub.length > 0))) {
        let circleListInner = [];
        circlesList.forEach(edge => circleListInner.push(edge.node));

    if (circlesFromClub && circlesFromClub.length > 0)
      circlesFromClub.forEach(edge => circleListInner.push(edge.node));

    if (circlesCurrentUserIsIn && circlesCurrentUserIsIn.length > 0)
      circlesCurrentUserIsIn.forEach(edge => circleListInner.push(edge.node))
      return circleListInner;
      
      
  }


   }
  _handleSubmit = (event) => {
    event.preventDefault();
    if (this.state.confirmPopupOpen
      || this.state.addBankAccountOpen
      || this.state.addCardPopupOpen
      || this.state.displayCompletePersonProfilePopup
      || this.state.displayCompleteBusinessProfilePopup)
      return;

    this.setState({
      submitClicked: true,
    })

    const errors = this.state.errors;
    if (errors.length > 0) {
      this.setState({
        errors: errors.map(error => error.element),
      });
      this.msg.show(errors[0].message, {
        time: 3000,
        type: 'info',
      });
	    setTimeout(() => {
		    this.msg.removeAll();
	    }, 2000);
    }
    else {
      if (!this.state.isModifying && !this.state.fields.private && (this.state.fields.invited_circles_and_prices.length === 0 || this.state.fields.invited_circles_and_prices.findIndex(item => item.circle.memberCount > 0) < 0) && (!this.state.fields.invited || this.state.fields.invited.length === 0)) {
        confirmModal({
          title: localizations.newSportunityAlertNoCommunityTitle,
          message: localizations.newSportunityAlertNoCommunityText,
          confirmLabel: localizations.newSportunityAlertNoCommunityYes,
          cancelLabel: localizations.newSportunityAlertNoCommunityNo,
          canCloseModal: true,
          onConfirm: () => {
            this.setState({
              selectedCard: this.props.viewer.me.paymentMethods.length > 0 ? this.props.viewer.me.paymentMethods[this.props.viewer.me.paymentMethods.length - 1] : '',
              selectedCardToPaySecondaryOrganizers: this.props.viewer.me.paymentMethods.length > 0 ? this.props.viewer.me.paymentMethods[this.props.viewer.me.paymentMethods.length - 1] : '',
              confirmPopupOpen: true,
            });
          },
          onCancel: () => {}        
        })
      }
      else {
        this.setState({
          selectedCard: this.props.viewer.me.paymentMethods.length > 0 ? this.props.viewer.me.paymentMethods[this.props.viewer.me.paymentMethods.length - 1] : '',
          selectedCardToPaySecondaryOrganizers: this.props.viewer.me.paymentMethods.length > 0 ? this.props.viewer.me.paymentMethods[this.props.viewer.me.paymentMethods.length - 1] : '',
          confirmPopupOpen: true,
        });
      }
    }
    setTimeout(() => {
      this.msg.removeAll();
    }, 2000);
  }

  _confirmCreation = () => {
    if (this.state.process) 
      return ;
      
    this.setState({
      process: true,
    });

    const levelsList = this.state.fields.sport
      ? this.props.viewer.sports.edges
        .find(({ node }) => this._translatedName(node.name) === this.state.fields.sport.name).node.levels
        .sort((a, b) => { return a.EN.skillLevel - b.EN.skillLevel })
        .map(l => ({ name: this._translatedLevelName(l), value: l.id }))
      : [];

    let allLevelSelected = true;
    let levels = null;

    if (this.state.fields.levelFrom && this.state.fields.levelTo) {
      let fromIndex = levelsList.findIndex((e) => e.value == this.state.fields.levelFrom.value);
      let toIndex = levelsList.findIndex((e) => e.value == this.state.fields.levelTo.value);

      levels = levelsList.map((level, index) => {
        if (fromIndex >= 0 && index < fromIndex) return false;
        if (toIndex >= 0 && index > toIndex) return false;
        return level.value;
      })
      levels = levels.filter(i => Boolean(i));
      if (levels.length > 0 && levels.length < levelsList.length)
        allLevelSelected = false;
    }

    if (allLevelSelected) {
      levels = levelsList.map(level => level.value);
    }

    let certificates = this.state.fields.certificates !== null
      ? this.state.fields.certificates.map(certificate => certificate.value)
      : null;

    let positions = this.state.fields.positions !== null
      ? this.state.fields.positions.map(position => position.value)
      : null;

    let price_for_circle = [];
    price_for_circle = this.state.fields.invited_circles_and_prices.map(item => {
      return {
        circle: item.circle.id,
        price: {
          cents: item.price.cents * 100,
          currency: item.price.currency
        },
        participantByDefault: item.participantByDefault,
	      excludedParticipantByDefault: item.participantByDefault && item.excludedParticipantByDefault ? {
		      excludedMembers: item.excludedParticipantByDefault.excludedMembers.map(user => user.id)
        } : null
      }
    })

    let organizers = [{
      organizer: this.props.viewer.me.id,
      isAdmin: true,
      role: 'COACH',
      price: this.state.fields.organizerParticipates && this.state.fields.organizerParticipation > 0 ? {
        cents: -100 * this.state.fields.organizerParticipation,
        currency: this.props.userCurrency
      }
        : {
          cents: 0,
          currency: this.props.userCurrency
        }
    }];
    let totalCost = 0 ;
    if (this.state.fields.organizers && this.state.fields.organizers.length > 0) {
      this.state.fields.organizers.forEach(organizer => {
        organizers.push({
          organizer: organizer.organizer,
          isAdmin: false,
          role: 'COACH',
          price: {
            cents: organizer.price.cents * 100,
            currency: organizer.price.currency
          },
          secondaryOrganizerType: organizer.secondaryOrganizerType,
          customSecondaryOrganizerType: organizer.customSecondaryOrganizerType
        })
        totalCost = totalCost + organizer.price.cents
      })
    }
    let pendingOrganizers = [];
    
    if (this.state.fields.circlesOfPendingOrganizers) {
      this.state.fields.circlesOfPendingOrganizers.forEach(item => {
        pendingOrganizers.push({
          id: item.id ? item.id : null,
          circles: item.circles.map(circle => circle.id),
          isAdmin: false,
          role: 'COACH',
          price: {
            cents: item.price.cents * 100,
            currency: item.price.currency
          },
          secondaryOrganizerType: item.secondaryOrganizerType,
          customSecondaryOrganizerType: item.customSecondaryOrganizerType
        })
        totalCost = totalCost + item.price.cents
      })
    }

    if (this.state.isModifying) {
      this.props.relay.commitUpdate(
        new UpdateSportunityMutation({
          sportunity: {
            id: this.props.relay.variables.sportunityId,
            title: this.state.fields.title,
            description: this.state.fields.description,
            participantRange: this.state.fields.participantRange,
            price: {
              cents: this.state.fields.price.cents * 100,
              currency: this.state.fields.price.currency
            },
            mode: 'RANDOM',
            kind: this.state.fields.private ? 'PRIVATE' : 'PUBLIC',
            privacy_switch_preference: this.state.fields.private ? {
              privacy_switch_type: this.state.fields.autoSwitchPrivacy ? 'Automatically' : 'Manually',
              switch_privacy_x_days_before: this.state.fields.autoSwitchPrivacyXDaysBefore
            } : null,
            beginningDate: this.state.isSurvey ? this.state.fields.schedules[0].beginningDate : this.state.fields.beginningDate,
            endingDate: this.state.isSurvey ? this.state.fields.schedules[this.state.fields.schedules.length - 1].endingDate : this.state.fields.endingDate,
	          survey_dates: this.state.isSurvey ? this.state.fields.schedules.map((schedule) => ({beginning_date: schedule.beginningDate, ending_date: schedule.endingDate})) : null,
            repeat: this.state.fields.repeat,
            participants: !this.state.isParticipant && this.state.fields.organizerParticipates ? this.props.viewer.me.id : null,
            paymentMethodId: this.state.fields.organizerParticipates && this.state.fields.organizerParticipation > 0 ? this.state.selectedCard.id : null,
            sport: {
              sport: this.state.fields.sport.value,
              levels,
              certificates,
              positions,
              allLevelSelected,
            },
            organizers,
            pendingOrganizers,
            secondaryOrganizersPaymentMethod: totalCost > 0 && ! this.state.paySecondaryOrganizersWithWallet ? this.state.selectedCardToPaySecondaryOrganizers.id : null,
            secondaryOrganizersPaymentByWallet: totalCost > 0 && this.state.paySecondaryOrganizersWithWallet ? this.state.paySecondaryOrganizersWithWallet : false,
            invited: this.state.fields.invited && this.state.fields.invited.map(invitedUser => { 
              if (invitedUser.id)
                return {user: invitedUser.id, answer: "WAITING"}
              else
                return { pseudo: invitedUser.pseudo, answer: "WAITING" } 
            }),
            invited_circles: this.state.fields.invited_circles && this.state.fields.invited_circles.map(circle => circle.id),
            price_for_circle: price_for_circle,
            notification_preference: {
              notification_type: this.state.fields.notificationType,
              send_notification_x_days_before: this.state.fields.notificationAutoXDaysBefore,
            },

            address: this.state.fields.address,
            venue: this.state.fields.venue && this.state.fields.venue.id ? this.state.fields.venue.id : null,
            infrastructure: this.state.fields.infrastructure && this.state.fields.infrastructure.id ? this.state.fields.infrastructure.id : null,
            slot: this.state.fields.slot && this.state.fields.slot.id ? this.state.fields.slot.id : null,
            sexRestriction: this.state.fields.sexRestriction,
            ageRestriction: this.state.fields.ageRestriction,
            hide_participant_list: this.state.fields.hideParticipantList,
            sportunityType: this.state.fields.sportunityType ? this.state.fields.sportunityType.value : null,
            game_information: {
              opponent: {
                organizer: this.state.fields.opponent && this.state.fields.opponent.id 
                  ? this.state.fields.opponent.id 
                  : null,
                organizerEmail: this.state.fields.opponent && this.state.fields.opponent.pseudo && isEmail.test(this.state.fields.opponent.pseudo) && !this.state.fields.opponent.id
                  ? this.state.fields.opponent.pseudo 
                  : null,
                organizerPseudo: this.state.fields.opponent && this.state.fields.opponent.pseudo && !isEmail.test(this.state.fields.opponent.pseudo) && !this.state.fields.opponent.id ? this.state.fields.opponent.pseudo : null,
                lookingForAnOpponent: this.state.fields.isOpenMatch,
                unknownOpponent: this.state.fields.unknownOpponent,
                invitedOpponents: this.state.fields.circleOfOpponents ? [this.state.fields.circleOfOpponents.id] : null
              }
            } 
          },
          modifyRepeatedSportunities: this.state.isModifyingASerie,
          notify_people: this.state.notifyPeople,
          viewer: this.props.viewer,
        }),
        {
          onSuccess: () => {
            this.msg.show(this.state.isModifyingASerie
              ? localizations.popup_newSportunity_update_serie_success
              : localizations.popup_newSportunity_update_success, {
                time: 3500,
                type: 'success',
              });
	          setTimeout(() => this.msg.removeAll(), 2000);
            // this.setState({
            //   process: false,
            // });
            if (this.state.saveTemplate && !this.state.isModifying) {
              this._confirmCreationTemplate()
            }
            setTimeout(() => {
              let path = '/event-view/' + this.props.relay.variables.sportunityId;
              this.props.router.push({
                pathname: path,
              })
              // this.setState({
              //   process: false
              // })
            }, 2000);
          },
          onFailure: (error) => {
            this.setState({
              process: false,
            });
            console.log(error.getError());
          },
        }
      );
    }
    else {
      let viewer = this.props.viewer;
      let sportunity = {
        title: this.state.fields.title,
        description: this.state.fields.description,
        participantRange: this.state.fields.participantRange,
        price: {
          cents: this.state.fields.price.cents * 100,
          currency: this.state.fields.price.currency
        },
        mode: 'RANDOM',
        kind: this.state.fields.private ? 'PRIVATE' : 'PUBLIC',
        privacy_switch_preference: this.state.fields.private ? {
          privacy_switch_type: this.state.fields.autoSwitchPrivacy ? 'Automatically' : 'Manually',
          switch_privacy_x_days_before: this.state.fields.autoSwitchPrivacyXDaysBefore
        } : null,
        beginningDate: !this.state.isSurvey ? this.state.fields.beginningDate.toISOString() : this.state.fields.schedules[0].beginningDate,
        endingDate: !this.state.isSurvey ? this.state.fields.endingDate.toISOString() : this.state.fields.schedules[this.state.fields.schedules.length - 1].endingDate,
        survey_dates: this.state.isSurvey ? this.state.fields.schedules.map((schedule) => ({beginning_date: schedule.beginningDate, ending_date: schedule.endingDate})) : null,
        repeat: this.state.fields.repeat,
        participants: this.state.fields.organizerParticipates ? this.props.viewer.me.id : null,
        paymentMethodId: this.state.fields.organizerParticipates && this.state.fields.organizerParticipation > 0 ? this.state.selectedCard.id : null,
        sport: {
          sport: this.state.fields.sport.value,
          levels,
          certificates,
          positions,
          allLevelSelected,
        },
        organizers,
        pendingOrganizers,
        secondaryOrganizersPaymentMethod: totalCost > 0 && ! this.state.paySecondaryOrganizersWithWallet ? this.state.selectedCardToPaySecondaryOrganizers.id : null,
        secondaryOrganizersPaymentByWallet: totalCost > 0 && this.state.paySecondaryOrganizersWithWallet ? this.state.paySecondaryOrganizersWithWallet : false,
        invited: this.state.fields.invited && this.state.fields.invited.map(invitedUser => { 
          if (invitedUser.id)
            return {user: invitedUser.id, answer: "WAITING"}
          else
            return { pseudo: invitedUser.pseudo, answer: "WAITING" } 
          }
        ),
        invited_circles: this.state.fields.invited_circles && this.state.fields.invited_circles.map(circle => circle.id),
        price_for_circle: price_for_circle,
        notification_preference: {
          notification_type: this.state.fields.notificationType,
          send_notification_x_days_before: this.state.fields.notificationAutoXDaysBefore,
        },
        address: this.state.fields.address,
        venue: this.state.fields.venue && this.state.fields.venue.id ? this.state.fields.venue.id : null,
        infrastructure: this.state.fields.infrastructure && this.state.fields.infrastructure.id ? this.state.fields.infrastructure.id : null,
        slot: this.state.fields.slot && this.state.fields.slot.id ? this.state.fields.slot.id : null,
        sexRestriction: this.state.fields.sexRestriction,
        ageRestriction: this.state.fields.ageRestriction,
        hide_participant_list: this.state.fields.hideParticipantList,
        sportunityType: this.state.fields.sportunityType ? this.state.fields.sportunityType.value : null,
        game_information: {
          opponent: {
            organizer: this.state.fields.opponent && this.state.fields.opponent.id 
              ? this.state.fields.opponent.id 
              : null,
            organizerEmail: this.state.fields.opponent && this.state.fields.opponent.pseudo && isEmail.test(this.state.fields.opponent.pseudo) && !this.state.fields.opponent.id
              ? this.state.fields.opponent.pseudo 
              : null,
            organizerPseudo: this.state.fields.opponent && this.state.fields.opponent.pseudo && !isEmail.test(this.state.fields.opponent.pseudo) && !this.state.fields.opponent.id ? this.state.fields.opponent.pseudo : null,
            lookingForAnOpponent: this.state.fields.isOpenMatch,
            unknownOpponent: this.state.fields.unknownOpponent,
            invitedOpponents: this.state.fields.circleOfOpponents ? [this.state.fields.circleOfOpponents.id] : null
          }
        }
      }
      this.props.relay.commitUpdate(
        new NewSportunityMutation({
          viewer: this.props.viewer,
          sportunity: sportunity,
          viewerID: this.props.viewer.id
        }),
        {
          onSuccess: () => {
            this.msg.show(this.state.fields.repeat && this.state.fields.repeat > 0
              ? localizations.popup_newSportunity_created_serie_success
              : localizations.popup_newSportunity_created_success, {
                time: 2000,
                type: 'success',
              });
              this.state.fields.repeat && this.state.fields.repeat > 0 &&
              this.msg.show(localizations.popup_newSportunity_created_serie_succes_refresh, {
                time: 4000,
                type: 'error',
              });
	          setTimeout(() => this.msg.removeAll(), 2000);
            // this.setState({
            //   process: false
            // })
            if (this.state.saveTemplate && !this.state.isModifying) {
              this._confirmCreationTemplate()
            }
            setTimeout(() => {
              let path = '/my-events';
              this.props.router.push({
                pathname: path,
              })
            }, 2000);
          },
          onFailure: (error) => {
            this.setState({
              process: false,
            });
            console.log(error.getError());
          },
        }
      );
    }
  }

  _confirmCreationTemplate = () => {
    this.setState({
      process: true,
    });

    const levelsList = this.state.fields.sport
      ? this.props.viewer.sports.edges
        .find(({ node }) => this._translatedName(node.name) === this.state.fields.sport.name).node.levels
        .sort((a, b) => { return a.EN.skillLevel - b.EN.skillLevel })
        .map(l => ({ name: this._translatedLevelName(l), value: l.id }))
      : [];

    let allLevelSelected = true;
    let levels = null;

    if (this.state.fields.levelFrom && this.state.fields.levelTo) {
      let fromIndex = levelsList.findIndex((e) => e.value == this.state.fields.levelFrom.value);
      let toIndex = levelsList.findIndex((e) => e.value == this.state.fields.levelTo.value);

      levels = levelsList.map((level, index) => {
        if (fromIndex >= 0 && index < fromIndex) return false;
        if (toIndex >= 0 && index > toIndex) return false;
        return level.value;
      })
      levels = levels.filter(i => Boolean(i));
      if (levels.length > 0 && levels.length < levelsList.length)
        allLevelSelected = false;
    }

    if (allLevelSelected) {
      levels = levelsList.map(level => level.value);
    }

    let certificates = this.state.fields.certificates !== null
      ? this.state.fields.certificates.map(certificate => certificate.value)
      : null;

    let positions = this.state.fields.positions !== null
      ? this.state.fields.positions.map(position => position.value)
      : null;

    let price_for_circle = [];
    price_for_circle = this.state.fields.invited_circles_and_prices.map(item => {
      return {
        circle: item.circle.id,
        price: {
          cents: item.price.cents * 100,
          currency: item.price.currency
        },
        participantByDefault: item.participantByDefault,
	      excludedParticipantByDefault: item.participantByDefault ? {
		      excludedMembers: item.excludedParticipantByDefault.excludedMembers.map(user => user.id)
	      } : null
      }
    })

    let organizers = [{
      organizer: this.props.viewer.me.id,
      isAdmin: true,
      role: 'COACH',
      price: this.state.fields.organizerParticipates && this.state.fields.organizerParticipation > 0 ? {
          cents: -100 * this.state.fields.organizerParticipation,
          currency: this.props.userCurrency
        }
        : {
          cents: 0,
          currency: this.props.userCurrency
        }
    }];
    let totalCost = 0 ;
    if (this.state.fields.organizers && this.state.fields.organizers.length > 0) {
      this.state.fields.organizers.forEach(organizer => {
        organizers.push({
          organizer: organizer.organizer,
          isAdmin: false,
          role: 'COACH',
          price: {
            cents: organizer.price.cents * 100,
            currency: organizer.price.currency
          },
          secondaryOrganizerType: organizer.secondaryOrganizerType,
          customSecondaryOrganizerType: organizer.customSecondaryOrganizerType
        })
        totalCost = totalCost + organizer.price.cents
      })
    }
    let pendingOrganizers = [];

    if (this.state.fields.circlesOfPendingOrganizers) {
      this.state.fields.circlesOfPendingOrganizers.forEach(item => {
        pendingOrganizers.push({
          id: item.id ? item.id : null,
          circles: item.circles.map(circle => circle.id),
          isAdmin: false,
          role: 'COACH',
          price: {
            cents: item.price.cents * 100,
            currency: item.price.currency
          },
          secondaryOrganizerType: item.secondaryOrganizerType,
          customSecondaryOrganizerType: item.customSecondaryOrganizerType
        })
        totalCost = totalCost + item.price.cents
      })
    }

    if (this.state.fromTemplate) {
      this.props.relay.commitUpdate(
        new UpdateSportunityTemplateMutation({
          sportunity: {
            id: this.state.selectedTemplate.value.id,
            title: this.state.fields.title,
            description: this.state.fields.description,
            participantRange: this.state.fields.participantRange,
            price: {
              cents: this.state.fields.price.cents * 100,
              currency: this.state.fields.price.currency
            },
            mode: 'RANDOM',
            kind: this.state.fields.private ? 'PRIVATE' : 'PUBLIC',
            privacy_switch_preference: this.state.fields.private ? {
              privacy_switch_type: this.state.fields.autoSwitchPrivacy ? 'Automatically' : 'Manually',
              switch_privacy_x_days_before: this.state.fields.autoSwitchPrivacyXDaysBefore
            } : null,
            repeat: this.state.fields.repeat,
            participants: !this.state.isParticipant && this.state.fields.organizerParticipates ? this.props.viewer.me.id : null,
            paymentMethodId: this.state.fields.organizerParticipates && this.state.fields.organizerParticipation > 0 ? this.state.selectedCard.id : null,
            sport: {
              sport: this.state.fields.sport.value,
              levels,
              certificates,
              positions,
              allLevelSelected,
            },
            organizers,
            pendingOrganizers,
            address: this.state.fields.address,
            secondaryOrganizersPaymentMethod: totalCost > 0 && ! this.state.paySecondaryOrganizersWithWallet ? this.state.selectedCardToPaySecondaryOrganizers.id : null,
            secondaryOrganizersPaymentByWallet: totalCost > 0 && this.state.paySecondaryOrganizersWithWallet ? this.state.paySecondaryOrganizersWithWallet : false,
            invited: this.state.fields.invited && this.state.fields.invited.map(invitedUser => {
              if (invitedUser.id)
                return {user: invitedUser.id, answer: "WAITING"}
              else
                return { pseudo: invitedUser.pseudo, answer: "WAITING" }
            }),
            invited_circles: this.state.fields.invited_circles && this.state.fields.invited_circles.map(circle => circle.id),
            price_for_circle: price_for_circle,
            notification_preference: {
              notification_type: this.state.fields.notificationType,
              send_notification_x_days_before: this.state.fields.notificationAutoXDaysBefore,
            },
            sexRestriction: this.state.fields.sexRestriction,
            ageRestriction: this.state.fields.ageRestriction,
            hide_participant_list: this.state.fields.hideParticipantList,
            sportunityType: this.state.fields.sportunityType ? this.state.fields.sportunityType.value : null,
            game_information: {
              opponent: {
                organizer: this.state.fields.opponent && this.state.fields.opponent.id
                  ? this.state.fields.opponent.id
                  : null,
                organizerEmail: this.state.fields.opponent && this.state.fields.opponent.pseudo && isEmail.test(this.state.fields.opponent.pseudo) && !this.state.fields.opponent.id
                  ? this.state.fields.opponent.pseudo
                  : null,
                organizerPseudo: this.state.fields.opponent && this.state.fields.opponent.pseudo && !isEmail.test(this.state.fields.opponent.pseudo) && !this.state.fields.opponent.id ? this.state.fields.opponent.pseudo : null,
                lookingForAnOpponent: this.state.fields.isOpenMatch,
                unknownOpponent: this.state.fields.unknownOpponent,
                invitedOpponents: this.state.fields.circleOfOpponents ? [this.state.fields.circleOfOpponents.id] : null
              }
            }
          },
          modifyRepeatedSportunities: this.state.isModifyingASerie,
          viewer: this.props.viewer,
        }),
        {
          onSuccess: () => {
            this.msg.show(localizations.popup_newSportunity_update_template_success, {
              time: 3500,
              type: 'success',
            });
	          setTimeout(() => this.msg.removeAll(), 2000);
            /*this.setState({
              process: false,
            });*/
            //this._confirmCreation()
          },
          onFailure: (error) => {
            this.setState({
              process: false,
            });
            console.log(error.getError());
          },
        }
      );
    }
    else {
      let viewer = this.props.viewer;
      let sportunity = {
        title: this.state.fields.title,
        description: this.state.fields.description,
        participantRange: this.state.fields.participantRange,
        price: {
          cents: this.state.fields.price.cents * 100,
          currency: this.state.fields.price.currency
        },
        mode: 'RANDOM',
        kind: this.state.fields.private ? 'PRIVATE' : 'PUBLIC',
        privacy_switch_preference: this.state.fields.private ? {
          privacy_switch_type: this.state.fields.autoSwitchPrivacy ? 'Automatically' : 'Manually',
          switch_privacy_x_days_before: this.state.fields.autoSwitchPrivacyXDaysBefore
        } : null,
        repeat: this.state.fields.repeat,
        participants: this.state.fields.organizerParticipates ? this.props.viewer.me.id : null,
        paymentMethodId: this.state.fields.organizerParticipates && this.state.fields.organizerParticipation > 0 ? this.state.selectedCard.id : null,
        sport: {
          sport: this.state.fields.sport.value,
          levels,
          certificates,
          positions,
          allLevelSelected,
        },
        organizers,
        pendingOrganizers,
        address: this.state.fields.address,
        secondaryOrganizersPaymentMethod: totalCost > 0 && ! this.state.paySecondaryOrganizersWithWallet ? this.state.selectedCardToPaySecondaryOrganizers.id : null,
        secondaryOrganizersPaymentByWallet: totalCost > 0 && this.state.paySecondaryOrganizersWithWallet ? this.state.paySecondaryOrganizersWithWallet : false,
        invited: this.state.fields.invited && this.state.fields.invited.map(invitedUser => {
            if (invitedUser.id)
              return {user: invitedUser.id, answer: "WAITING"}
            else
              return { pseudo: invitedUser.pseudo, answer: "WAITING" }
          }
        ),
        invited_circles: this.state.fields.invited_circles && this.state.fields.invited_circles.map(circle => circle.id),
        price_for_circle: price_for_circle,
        notification_preference: {
          notification_type: this.state.fields.notificationType,
          send_notification_x_days_before: this.state.fields.notificationAutoXDaysBefore,
        },
        sexRestriction: this.state.fields.sexRestriction,
        ageRestriction: this.state.fields.ageRestriction,
        hide_participant_list: this.state.fields.hideParticipantList,
        sportunityType: this.state.fields.sportunityType ? this.state.fields.sportunityType.value : null,
        game_information: {
          opponent: {
            organizer: this.state.fields.opponent && this.state.fields.opponent.id
              ? this.state.fields.opponent.id
              : null,
            organizerEmail: this.state.fields.opponent && this.state.fields.opponent.pseudo && isEmail.test(this.state.fields.opponent.pseudo) && !this.state.fields.opponent.id
              ? this.state.fields.opponent.pseudo
              : null,
            organizerPseudo: this.state.fields.opponent && this.state.fields.opponent.pseudo && !isEmail.test(this.state.fields.opponent.pseudo) && !this.state.fields.opponent.id ? this.state.fields.opponent.pseudo : null,
            lookingForAnOpponent: this.state.fields.isOpenMatch,
            unknownOpponent: this.state.fields.unknownOpponent,
            invitedOpponents: this.state.fields.circleOfOpponents ? [this.state.fields.circleOfOpponents.id] : null
          }
        }
      }
      this.props.relay.commitUpdate(
        new NewSportunityTemplateMutation({
          viewer: this.props.viewer,
          sportunity: sportunity,
          viewerID: this.props.viewer.id
        }),
        {
          onSuccess: () => {
            this.msg.show(localizations.popup_newSportunity_created_template_success, {
              time: 2000,
              type: 'success',
            });
	          setTimeout(() => this.msg.removeAll(), 2000);
            /*this.setState({
              process: false
            });*/
            //this._confirmCreation()
          },
          onFailure: (error) => {
            this.setState({
              process: false,
            });
            console.log(error.getError());
          },
        }
      );
    }
  }

  _updateField(name, value) {
    if(name =='title'){
      setTimeout(() => { this.textInputTitle.focus(); }, 10);
    } else if (name =='description' ){
      setTimeout(() => { this.textInputDescription.focus(); }, 10);
    }  else if (name =='textInputSports1' ){
      setTimeout(() => { this.textInputSports1.focus(); }, 10);
    } else {
     // setTimeout(() => { this.textInput.focus(); }, 10);
    }
    return this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        [name]: value,
      },
    }));
  }

  _updateFieldFinal(name, value) {
  return this.setState(prevState => ({
    ...prevState,
    fields: {
      ...prevState.fields,
      [name]: value,
    },
    errors:[]
  }));
}

  _updatePriceField(name, value) {
    return this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        [name]: value,
        organizerParticipates: false
      },
    }));
  }

  _addFieldToList(name, value) {
    let newValue = this.state.fields[name];
    let index = -1;
    if (value && newValue.length > 0) {
      index = newValue.findIndex((e) => e.value == value.value);
      if (index >= 0) newValue.splice(index, 1);
    }
    if (value && index < 0)
      newValue.push(value);
    if (value == null) newValue = [];

    return this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        [name]: newValue,
      },
    }))
  }

  _handleLoadAllSports() {
    this.props.relay.setVariables({ sportsNb: 1000, filterName: { name: '', language: 'EN' }, });
    this.setState({
      allSportsLoaded: true,
    })
  }

  _updateSportFilter(value) {
    this.setState({
      sportSearch: value
    })

    let tempo = value ;
    setTimeout(() => {
      if (tempo.length > 0 && tempo === this.state.sportSearch) {
        if (!this.state.allSportsLoaded && value.length >= 1) {
          this.props.relay.setVariables({
            filterName: { name: value, language: localizations.getLanguage().toUpperCase() },
            sportsNb: 5
          });
        }
      }
      if (value.length === 0) {
        this.props.relay.setVariables({
          filterName: { name: '', language: localizations.getLanguage().toUpperCase() },
        })
      }
    }, 350);
  }

  _updateSportField(value) {
    if(value){
      setTimeout(() => { this.textInputSports1.focus(); }, 10);
    } else {

    }
    if (!value)
      this.props.relay.setVariables({
        filterName: { name: '', language: localizations.getLanguage().toUpperCase() },
      });
    
    if (value && this.state.fields.invited_circles && this.state.fields.invited_circles.length > 0) {
      let circleList = this.state.fields.invited_circles ;
      circleList = circleList.sort((a,b) => {
        if (a.sport && a.sport.sport && a.sport.sport.id === value.id)
          return -1;
        else if (b.sport && b.sport.sport && b.sport.sport.id === value.id)
          return 1;
        else return 0;
      })
      
      this.setState(prevState => ({
        ...prevState,
        fields: {
          ...prevState.fields,
          invited_circles: circleList
        }
      }))
      if (this.state.fields.invited_circles_and_prices && this.state.fields.invited_circles_and_prices.length > 0) {
        let circleList = this.state.fields.invited_circles_and_prices ;
        
        circleList = circleList.sort((a,b) => {
          if (a.circle.sport && a.circle.sport.sport && a.circle.sport.sport.id === value.id)
            return -1;
          else if (b.circle.sport && b.circle.sport.sport && b.circle.sport.sport.id === value.id)
            return 1;
          else return 0;
        })
        
        this.setState(prevState => ({
          ...prevState,
          fields: {
            ...prevState.fields,
            invited_circles_and_prices: circleList
          }
        }))
      }
    }

    return this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        sport: value ? value : '',
        levelFrom: null,
        levelTo: null,
        positions: [],
        certificates: [],
        venue: {
          id: '',
          name: '',
          price: {
            cents: 0,
            currency: this.props.userCurrency,
          },
          address: {
            address: '',
          },
        },
        infrastructure: {
          id: '',
          name: ''
        },
        slot: {
          id: '',
          from: '',
          end: '',
          price: {
            cents: 0,
            currency: this.props.userCurrency
          }
        },
        sportunityType: null,
        opponent: null,
      },
      sportSearch: value ? value : '',
      circleList:[]
    }));
  }

  _updateSportunityTypeField(value) {
    const sportunityTypesList = this.state.fields.sport && this.props.viewer.sports.edges.find(({ node }) => this._translatedName(node.name) === this.state.fields.sport.name)
    ? this.props.viewer.sports.edges
      .find(({ node }) => this._translatedName(node.name) === this.state.fields.sport.name).node.sportunityTypes
      .map(p => ({ name: this._translatedName(p.name), value: p.id, isScoreRelevant: p.isScoreRelevant }))
    : [];
    let selectedType = sportunityTypesList.find(item => value && item.value === value.value);
    if (selectedType && selectedType.isScoreRelevant) {
      let currentMemberCount = -1;
      if (this.props.viewer && this.props.viewer.me && (this.props.viewer.me.circles || this.props.viewer.me.circlesFromClub)) {
          if (this.props.viewer.me.circles && this.props.viewer.me.circles.edges && this.props.viewer.me.circles.edges.length > 0) {
              this.props.viewer.me.circles.edges.filter(edge => edge.node.type === "TEAMS" || edge.node.type === "CLUBS").forEach(edge => {
                  if (edge.node.memberCount > currentMemberCount) {
                      currentMemberCount = edge.node.memberCount;
                      this._updateCircleOpponentField(edge.node)
                  }
              })
          }
          if (this.props.viewer.me.circlesFromClub && this.props.viewer.me.circlesFromClub.edges && this.props.viewer.me.circlesFromClub.edges.length > 0) {
              this.props.viewer.me.circlesFromClub.edges.filter(edge => edge.node.type === "TEAMS" || edge.node.type === "CLUBS").forEach(edge => {
                  if (edge.node.memberCount > currentMemberCount) {
                      currentMemberCount = edge.node.memberCount;
                      this._updateCircleOpponentField(edge.node)
                  }
              })
          }
      }
    }
    else {
      this._updateCircleOpponentField(null)
    }

    return this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        sportunityType: value,
        opponent: null
      }
    }))
  }

  _changeTemplate = (value, item) => {
    this.props.relay.setVariables({
      filterName: {
        name: value.sport.sport.name[localizations.getLanguage().toUpperCase()],
        language: localizations.getLanguage().toUpperCase()
      },
      sportsNb: 10
    });
    this.setState(prevState => ({
      ...prevState,
      fromTemplate: true,
      fields: {
        ...prevState.fields,
        title: value.title,
        description: value.description,
        participantRange: {
          from: value.participantRange.from,
          to: value.participantRange.to
        },
        price: {
          cents: value.price ? value.price.cents / 100 : 0,
          currency: value.price ? value.price.currency : this.props.userCurrency,
        },
        organizers: value.organizers.map(organizer => {
          if (organizer.isAdmin)
            return false;
          else
            return {
              organizer: organizer.organizer.id,
              price: {
                cents: value.price ? organizer.price.cents / 100 : 0,
                currency: value.price ? organizer.price.currency : this.props.userCurrency
              },
              secondaryOrganizerType: organizer.secondaryOrganizerType ? organizer.secondaryOrganizerType.id : null,
              customSecondaryOrganizerType: organizer.customSecondaryOrganizerType,
            }
        }).filter(i => Boolean(i)),
        circlesOfPendingOrganizers: value.pendingOrganizers && value.pendingOrganizers.length > 0
          ? value.pendingOrganizers.map(org => ({
            id: org.id,
            circles: org.circles.edges.map(edge => edge.node),
            price: {cents: org.price.cents / 100, currency: org.price.currency},
            secondaryOrganizerType: org.secondaryOrganizerType ? org.secondaryOrganizerType.id : null,
            customSecondaryOrganizerType: org.customSecondaryOrganizerType,
          }))
          : [],
        private: value.kind === 'PRIVATE',
        autoSwitchPrivacy: value.privacy_switch_preference &&
        value.privacy_switch_preference.privacy_switch_type === 'Automatically',
        autoSwitchPrivacyXDaysBefore: value.privacy_switch_preference && value.privacy_switch_preference.privacy_switch_type === 'Automatically'
          ? value.privacy_switch_preference.switch_privacy_x_days_before
          : 15,
        sport: {
          bold: {
            from: 0,
            to: 0
          },
          name: value.sport.sport.name[localizations.getLanguage().toUpperCase()],
          logo: value.sport.sport.logo,
          value: value.sport.sport.id,
          id: value.sport.sport.id
        },
        levelFrom: value.sport.levels.length > 0 ? { name: value.sport.levels[0][localizations.getLanguage().toUpperCase()].name, value: value.sport.levels[0].id } : null,
        levelTo: value.sport.levels.length > 0 ? { name: value.sport.levels[value.sport.levels.length - 1][localizations.getLanguage().toUpperCase()].name, value: value.sport.levels[value.sport.levels.length - 1].id } : null,
        positions: value.sport.positions ? value.sport.positions.map(position => { return { name: position[localizations.getLanguage().toUpperCase()], value: position.id } }) : [],
        certificates: value.sport.certificates ? value.sport.certificates.map(certificate => { return { name: certificate.name[localizations.getLanguage().toUpperCase()], value: certificate.id } }) : [],
        sportunityType: value.sportunityType ? { name: value.sportunityType.name[localizations.getLanguage().toUpperCase()], value: value.sportunityType.id } : null,
        opponent:
          value.game_information && value.game_information.opponent && value.game_information.opponent.organizer
            ? value.game_information.opponent.organizer
            : value.game_information.opponent.organizerPseudo
            ? {pseudo: value.game_information.opponent.organizerPseudo}
            : null,
        isOpenMatch: value.game_information.opponent.lookingForAnOpponent,
        unknownOpponent: value.game_information.opponent.unknownOpponent,
        circleOfOpponents: value.game_information.opponent.invitedOpponents && value.game_information.opponent.invitedOpponents.edges && value.game_information.opponent.invitedOpponents.edges.length > 0
          ? value.game_information.opponent.invitedOpponents.edges[0].node
          : null,
        address: value.address ? {
          address: value.address.address,
          country: value.address.country,
          city: value.address.city,
        } : null,
        invited: (value.invited.length > 0? value.invited : [])
          .filter(invited => {
            let isInACircle = false;
            if (value.invited_circles && value.invited_circles.edges && value.invited_circles.edges.length > 0) {
              value.invited_circles.edges.forEach(edge => {
                if (edge.node.members && edge.node.members.length > 0) {
                  if (edge.node.members.findIndex(member => member.id === invited.user.id) >= 0)
                    isInACircle = true
                }
              })
            }

            return !isInACircle;
          })
          .map(invited => ({ pseudo: invited.user.pseudo, avatar: invited.user.avatar })),
        invited_circles: value.invited_circles && value.invited_circles.edges && value.invited_circles.edges.length > 0
          ? value.invited_circles.edges.map(edge => edge.node)
          : [],
        invited_circles_and_prices:
          value.price_for_circle && value.invited_circles && value.invited_circles.edges && value.invited_circles.edges.length > 0
            ? value.invited_circles.edges.map(edge => {
              let circlePriceIndex = value.price_for_circle.findIndex(item => item.circle.id === edge.node.id);

              if (circlePriceIndex >= 0)
                return {
                  circle: edge.node,
                  price: {
                    cents: value.price_for_circle[circlePriceIndex].price.cents / 100,
                    currency: value.price_for_circle[circlePriceIndex].price.currency
                  },
                  participantByDefault: value.price_for_circle[circlePriceIndex].participantByDefault,
	                excludedParticipantByDefault: value.price_for_circle[circlePriceIndex].participantByDefault && value.price_for_circle[circlePriceIndex].excludedParticipantByDefault ? {
		                excludedMembers: value.price_for_circle[circlePriceIndex].excludedParticipantByDefault.excludedMembers.map(user => ({
			                id: user.id,
		                }))
	                } : null
                };
              else
                return {
                  circle: edge.node,
                  price: {
                    cents: value.price.cents / 100,
                    currency: value.price.currency,
                  }
                }
            })
            : [],
        notificationType: value.notification_preference && value.notification_preference.notification_type ? value.notification_preference.notification_type : 'Now',
        notificationAutoXDaysBefore: value.notification_preference && value.notification_preference.send_notification_x_days_before ? value.notification_preference.send_notification_x_days_before : 15,
        organizerParticipation: value.organizers[0].price ? -1 * value.organizers[0].price.cents / 100 : 0,
        sexRestriction: value.sexRestriction,
        ageRestriction: { from: value.ageRestriction.from, to: value.ageRestriction.to },
        hideParticipantList: value.hide_participant_list,
      },
      selectedTemplate: item,
    }))
  };

  _updateOpponentField(value) {
    return this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        opponent: value,
        isOpenMatch: false,
        unknownOpponent: false,
        circleOfOpponents: null,
        title: value ? this.state.fields.sportunityType.name + ' ' + localizations.against + ' ' + value.pseudo : ''
      }
    }))
  }

  _updateCircleOpponentField(value) {
    return this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        opponent: null,
        isOpenMatch: false,
        unknownOpponent: false,
        circleOfOpponents: value
      }
    }))
  }

  _updateVenueField(value) {
    return this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        infrastructure: value.infrastructure,
        venue: value.venue,
        slot: {
          id: '',
          from: '',
          end: '',
          price: {
            cents: 0,
            currency: this.props.userCurrency
          }
        }
      },
    }));
  }

  _updateVenueSlotField(value) {
    return this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        slot: value,
        beginningDate: new Date(value.from),
        endingDate: new Date(value.end)
      },
    }));
  }

  _updateTextField(name, event) {
    
    
    return this._updateField(name, event.target.value);
  }
  _updateTextFieldFinal(name, event) {
   return this._updateFieldFinal(name, event.target.value);
 }

  _handlePrivateChange = (e) => {
    this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        private: !this.state.fields.private,
        price: {
          cents: 0,
          currency: this.props.userCurrency
        }
      }
    }))
  }

  _handleAutoSwitchPrivacyChange = (e) => {
    this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        autoSwitchPrivacy: !this.state.fields.autoSwitchPrivacy,
      }
    }))
  }


  _handleOpenMatchSwitch = e => {
  
    this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        isOpenMatch: !this.state.fields.isOpenMatch,
        opponent: null,
        unknownOpponent: false,
        circleOfOpponents: null,
      },
      circleList:[],
    }))
     
    let circleListInner = this._updateCircleList();
    return this.setState({ circleList: [...circleListInner] }, () => console.log(this.state.circleList));

    
  }

  _handleUnknownOpponentSwitch = e => {
    this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        isOpenMatch: false,
        opponent: null,
        unknownOpponent: !this.state.fields.unknownOpponent,
        circleOfOpponents: null,
      }
    }))
  }


  _handleRemoveInvitedItem = (index, event) => {
    let newList = this.state.fields.invited;
    event.preventDefault();
    event.stopPropagation();

    newList.splice(index, 1);
    this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        invited: newList,
      }
    }))
  }

  _handleRemoveInvitedCircle = (index, event) => {
    let newList = cloneDeep(this.state.fields.invited_circles);

    newList.splice(index, 1);
    this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        invited_circles: newList,
      }
    }))
  }

  _handleRemoveInvitedCircleAndPrice = (item, event) => {
    event.preventDefault();
    event.stopPropagation();

    let newList = cloneDeep(this.state.fields.invited_circles_and_prices);
    let index = newList.findIndex(el => el.circle.id === item.item.circle.id);

    newList.splice(index, 1);
    this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        invited_circles_and_prices: newList,
      }
    }))
    this._handleRemoveInvitedCircle(index, event)
  }

  _handleAddInvitedItem = (name, avatar, id) => {
    if (this.state.fields.invited && this.state.fields.invited.indexOf({ pseudo: name }) > 0) return;
    if (this.props.viewer.me && name.toLowerCase() === this.props.viewer.me.pseudo.toLowerCase()) {
      this.msg.show(localizations.newSportunity_invitation_error, {
        time: 2000,
        type: 'error',
      });
      setTimeout(function () {
        this.msg.removeAll();
      }, 2000);
      return ;
    }
    let newList = cloneDeep(this.state.fields.invited) || [];

    newList.push({ id, pseudo: name, avatar });
    this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        invited: newList,
      }
    }))
  }

  _handleAddInvitedCircle = (circle) => {
    let newList = this.state.fields.invited_circles ? cloneDeep(this.state.fields.invited_circles) : [];

    newList.push(circle);
    this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        invited_circles: newList,
      }
    }))
  }

  _handleAddInvitedCircleAndPrice = (circle) => {
    let newList = this.state.fields.invited_circles_and_prices ? cloneDeep(this.state.fields.invited_circles_and_prices) : [];

    newList.push({ circle, price: this.state.fields.price, participantByDefault: false });
    this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        invited_circles_and_prices: newList,
      }
    }))
    this._handleAddInvitedCircle(circle)
  }

  _handleChangeInvitedCirclePrice = (circle, event) => {

    let newList = this.state.fields.invited_circles_and_prices ? cloneDeep(this.state.fields.invited_circles_and_prices) : [];

    let index = newList.findIndex(itemInList => itemInList.circle.id === circle.id);

    newList[index].price = {
      cents: event.target.value,
      currency: this.state.fields.price.currency
    };

	  newList[index].participantByDefault = newList[index].price.cents !== 0 ? false : newList[index].participantByDefault;
	  newList[index].excludedParticipantByDefault = newList[index].price.cents !== 0 ? false : newList[index].excludedParticipantByDefault

	  this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        invited_circles_and_prices: newList,
      }
    }))
  }

  _handleChangeCircleAutoParticipate = (circle, checked) => {

    let newList = cloneDeep(this.state.fields.invited_circles_and_prices) || [];
    
    let index = newList.findIndex(itemInList => itemInList.circle.id === circle.id);

    newList[index].participantByDefault = checked;
    let excludedMembers = []
	  let statusList = [];

	  circle.memberStatus.forEach(status => {
	    if (circle.members.findIndex(member => member.id === status.member.id) >= 0) {
		    let index = statusList.findIndex(tmpStatus => tmpStatus.member.id === status.member.id)
		    if (index < 0) {
			    statusList.push(status)
		    }
		    else if (statusList[index].starting_date < status.starting_date)
			    statusList[index] = status
	    }
	  });
	  statusList.forEach(member => {
      if (member.status && member.status !== 'ACTIVE') {
	      excludedMembers.push(member.member)
      }
    })
	  newList[index].excludedParticipantByDefault = {excludedMembers};

    this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        invited_circles_and_prices: newList,
      }
    }))
  }

  _handleChangeUserAutoParticipate = (circle, user) => {

    let newList = cloneDeep(this.state.fields.invited_circles_and_prices) || [];

    let index = newList.findIndex(itemInList => itemInList.circle.id === circle.id);

    if (index >= 0) {
	    let newExcludedList = [];
      if (newList[index].participantByDefault) {
        let isInExcluded = false
	      if (newList[index].excludedParticipantByDefault)
		      newList[index].excludedParticipantByDefault.excludedMembers.forEach(member => {
			      if (member.id !== user.id)
				      newExcludedList.push(member)
			      else
				      isInExcluded = true
		      })
        if (!isInExcluded)
          newExcludedList.push(user)
      }
      else {
	      newList[index].participantByDefault = true;
	      circle.members.forEach(member => {
	        if (member.id !== user.id)
            newExcludedList.push(member)
        })
      }
	    newList[index].participantByDefault = newExcludedList.length !== circle.members.length;
      newList[index].excludedParticipantByDefault = {
        excludedMembers: newExcludedList
      }
    }

    this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        invited_circles_and_prices: newList,
      }
    }))
  }

  _handleDateAdd = () => {
    this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        scheduleId: nextScheduleId++,
        beginningDate: null,
        endingDate: null,
        repeat: null,
      },
    }));
  }

  _handleAgeRestrictionChange = (ageRestriction) => {
    this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        ageRestriction
      }
    }))
  }

  _handleSexRestrictionChange = (sexRestriction) => {
    this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        sexRestriction
      }
    }))
  }

  _handleDateChange = ({ beginningDate, endingDate, repeat, scheduleId }) => {
    this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        schedules: [...prevState.fields.schedules.filter(s => s.scheduleId.toString() !== scheduleId.toString()),
        { scheduleId: scheduleId /*|| nextScheduleId++*/, beginningDate: beginningDate, endingDate: endingDate, repeat: repeat }],
        beginningDate,
        endingDate,
        repeat,
      },
    }));
  }

  _handleDateEdit = (scheduleId) => {
    const schedule = this.state.fields.schedules.filter(s => s.scheduleId.toString() === scheduleId.toString())

    if (schedule.length) {
      this.setState(prevState => ({
        ...prevState,
        fields: {
          ...prevState.fields,
          scheduleId: schedule[0].scheduleId,
          beginningDate: schedule[0].beginningDate,
          endingDate: schedule[0].endingDate,
        },
      }));
    }
  }

  _handleDateDelete = (id) => {
    this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        schedules: prevState.fields.schedules
          .filter((s) => s.scheduleId.toString() !== id.toString()),
      },
    }));
  }

  _handleAddressChange = (label) => {
    const splitted = label.split(', ');
    const country = splitted[splitted.length - 1] || '';
    const city = splitted[splitted.length - 2] || '';

    this._updateField('address', {
      address: splitted[0],
      country,
      city,
    });

    if (this.state.fields.slot.id !== '' && !this.state.isSurvey) {
      this.setState(prevState => ({
        ...prevState,
        fields: {
          ...prevState.fields,
          schedules: [],
          beginningDate: '',
          endingDate: '',
          repeat: 0,
        },
      }));
    }

    this._updateField('venue', {
      id: '',
      name: '',
      price: {
        cents: 0,
        currency: this.props.userCurrency,
      },
      address: {
        address: '',
      },
    });

    this._updateField('infrastructure', {
      id: '',
      name: ''
    });

    this._updateField('slot', {
      id: '',
      from: '',
      end: '',
      price: {
        cents: 0,
        currency: this.props.userCurrency
      }
    })
  }

  _handleSlotChange = (slot) => {
    this._updateField('venue', {
      id: slot.venue.id,
      name: slot.venue.name, 
      address: {address: slot.venue.address.address + ', ' + slot.venue.address.city},
      price: {
        cents: 0,
        currency: this.props.userCurrency,
      },
    });

    this._updateField('infrastructure', {
      id: slot.infrastructure.id, 
      name: slot.infrastructure.name
    });

    this._updateField('slot', {
      id: slot.id,
      from: slot.from,
      end: slot.end, 
      price: {
        cents: slot.price.cents,
        currency: slot.price.currency
      }
    })
  
    this._updateField('address', {
      address: slot.venue.address.address,
      city: slot.venue.address.city,
      country: slot.venue.address.country
    });

    if (slot.serie_information && slot.serie_information.remainingSlots > 1) {
      this._handleDateChange({
        beginningDate: moment(slot.from)._d,
        endingDate: moment(slot.end)._d,
        repeat: slot.serie_information.remainingSlots, 
        scheduleId: 0
      })
    }
    else {
      this._handleDateChange({
        beginningDate: moment(slot.from)._d,
        endingDate: moment(slot.end)._d,
        repeat: 0, 
        scheduleId: 0
      })
    }    
  }

  _handleInfrastructureChange = (infrastructure) => {
    this._updateField('venue', {
      id: infrastructure.venue.id,
      name: infrastructure.venue.name, 
      address: {address: infrastructure.venue.address.address + ', ' + infrastructure.venue.address.city},
      price: {
        cents: 0,
        currency: this.props.userCurrency,
      },
    });

    this._updateField('infrastructure', {
      id: infrastructure.id, 
      name: infrastructure.name
    });
  
    this._updateField('address', {
      address: infrastructure.venue.address.address,
      city: infrastructure.venue.address.city,
      country: infrastructure.venue.address.country
    });

    this._updateField('slot', {
      id: '',
      from: '',
      end: '',
      price: {
        cents: 0,
        currency: this.props.userCurrency
      }
    })

    if (!this.state.isSurvey)
      this.setState(prevState => ({
        ...prevState,
        fields: {
          ...prevState.fields,
          schedules: [],
          beginningDate: '',
          endingDate: '',
          repeat: 0,
        },
      }));
  }

  _handleAddOrganizer = (assistant, sport) => {
    let newList = this.state.fields.organizers ? cloneDeep(this.state.fields.organizers) : [];
    let index = newList.findIndex(item => item.organizer === assistant.id);
    if (index >= 0) {
      this.msg.show(localizations.newSportunity_organizerAlreadyInList, {
        time: 0,
        type: 'info',
        onClose: () => console.log('--Close notif--')
      })
      setTimeout(() => {
        this.msg.removeAll();
      }, 2000);
      return ;
    }
    if (this.props.viewer.me && this.props.viewer.me.id === assistant.id) {
      this.msg.show(localizations.newSportunity_organizerCanAddSelf, {
        time: 0,
        type: 'info',
	      onClose: () => console.log('--Close notif--')
      })
      setTimeout(() => {
        this.msg.removeAll();
      }, 2000);
      return ;
    }

    let secondaryOrganizerType ;  
    if (sport) {
      let userSport ;
      assistant.sports.forEach(item => {
        if (item.sport.id === sport.id)
            userSport = item
      });

      if (sport.assistantTypes && sport.assistantTypes.length > 0) {
        if (userSport && userSport.assistantType && userSport.assistantType.length > 0) {
            secondaryOrganizerType = userSport.assistantType[0].id;
        }
      }
    }

    newList.push(
      {
        organizer: assistant.id, 
        price: {
          cents: 0, 
          currency: this.props.userCurrency
        },
        secondaryOrganizerType,
        customSecondaryOrganizerType: null
      }
    )

    this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        organizers: newList
      },
    }));
  }

  _handleRemoveOrganizer = (assistantId) => {
    let newList = cloneDeep(this.state.fields.organizers);
    let index = newList.findIndex(item => item.organizer === assistantId);
    
    newList.splice(index, 1);
    
    this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        organizers: newList,
      }
    }))
  }

  _handleUpdateOrganizerPrice = (assistant, price) => {
    let newList = cloneDeep(this.state.fields.organizers);
    let index = newList.findIndex(item => item.organizer === assistant.organizer);
    newList[index].price = {
      cents: price,
      currency: this.props.userCurrency
    }
    this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        organizers: newList,
      }
    }))
  }

  _handleUpdateOrganizerRole = (assistant, role) => {
    let newList = cloneDeep(this.state.fields.organizers);
    let index = newList.findIndex(item => item.organizer === assistant.organizer);
    newList[index].secondaryOrganizerType = role
    this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        organizers: newList,
      }
    }))
  }

  _handleUpdateOrganizerCustomRole = (assistant, role) => {
    let newList = cloneDeep(this.state.fields.organizers);
    let index = newList.findIndex(item => item.organizer === assistant.organizer);
    newList[index].customSecondaryOrganizerType = role
    this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        organizers: newList,
      }
    }))
  }


  _handleAddCirclesOfPendingOrganizers = (circles) => {
    let newList = this.state.fields.circlesOfPendingOrganizers ? cloneDeep(this.state.fields.circlesOfPendingOrganizers) : [];
    
    newList.push(
      {
        circles, 
        price: {
          cents: 0, 
          currency: this.props.userCurrency
        },
        secondaryOrganizerType: null,
        customSecondaryOrganizerType: null
      }
    )

    this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        circlesOfPendingOrganizers: newList
      },
    }));
  }
  
  _handleRemoveCirclesOfPendingOrganizers = (index) => {
    let newList = cloneDeep(this.state.fields.circlesOfPendingOrganizers);
    
    newList.splice(index, 1);
    
    this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        circlesOfPendingOrganizers: newList,
      }
    }))
  }

  _handleUpdateCirclesOfPendingOrganizersPrice = (index, price) => {
    let newList = cloneDeep(this.state.fields.circlesOfPendingOrganizers);
    
    newList[index].price = {
      cents: price,
      currency: this.props.userCurrency
    }
    this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        circlesOfPendingOrganizers: newList,
      }
    }))
  }
  
  _handleUpdateCirclesOfPendingOrganizersRole = (index, role) => {
    let newList = cloneDeep(this.state.fields.circlesOfPendingOrganizers);
    
    newList[index].secondaryOrganizerType = role
    this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        circlesOfPendingOrganizers: newList,
      }
    }))
  }

  _handleUpdateCirclesOfPendingOrganizersCustomRole = (index, role) => {
    let newList = cloneDeep(this.state.fields.circlesOfPendingOrganizers);
    
    newList[index].customSecondaryOrganizerType = role
    this.setState(prevState => ({
      ...prevState,
      fields: {
        ...prevState.fields,
        circlesOfPendingOrganizers: newList,
      }
    }))
  }

  _hideConfirmationCreationPopup = () => {
    this.setState({ confirmPopupOpen: false })
  }

  _handleShowCompleteProfile = () => {
    if (this.props.viewer.me.profileType === 'PERSON') {
      this.setState({
        confirmPopupOpen: false,
        displayCompletePersonProfilePopup: true
      })
    }
    else {
      this.setState({
        confirmPopupOpen: false,
        displayCompleteBusinessProfilePopup: true
      })
    }
  }

  _showAddBankAccount = () => {
    this.setState({
      addBankAccountOpen: true,
      confirmPopupOpen: false,
    })
  }

  _showAddACardPopup = () => {
    this.setState({
      addCardPopupOpen: true,
      confirmPopupOpen: false,
    })
  }

  _showAddACardToPaySecondaryOrganizersPopup = () => {
    this.setState({
      addACardToPaySecondaryOrganizers: true,
      confirmPopupOpen: false,
    }) 
  }

  _handleHideAddCard = () => {
    this.setState({
      addCardPopupOpen: false,
      confirmPopupOpen: true,
    })
  }

  _handleHideAddCardToPaySecondaryOrganizers = () => {
    this.setState({
      addACardToPaySecondaryOrganizers: false,
      confirmPopupOpen: true,
    })
  }

  _hideAddBankAccount = () => {
    this.setState({
      addBankAccountOpen: false,
      confirmPopupOpen: true,
    })
  }

  _handleHideCompleteProfilePopup = () => {
    this.setState({
      displayCompletePersonProfilePopup: false,
      confirmPopupOpen: true
    })
  }

  _handleHideCompleteBusinessProfilePopup = () => {
    this.setState({
      displayCompleteBusinessProfilePopup: false,
      confirmPopupOpen: true
    })
  }

  _handleConfirmProfileUpdate = (data) => {
    this.props.relay.commitUpdate(
      new UpdateUserProfileMutation({
        viewer: this.props.viewer,
        userIDVar: this.props.viewer.me.id,
        lastNameVar: data.lastName,
        firstNameVar: data.firstName,
        addressVar: data.address,
        nationalityVar: data.nationality,
        birthdayVar: data.birthday,
      }), {
        onSuccess: (response) => {
          this.msg.show(localizations.popup_newSportunity_profile_update_success, {
            time: 2000,
            type: 'success',
          });
	        setTimeout(() => this.msg.removeAll(), 2000);
          if (this.state.fields.price.cents > 0 && !this.props.viewer.me.bankAccount)
            this.setState({
              addBankAccountOpen: true,
              displayCompletePersonProfilePopup: false,
              process: false,
            })
          else
            this.setState({
              confirmPopupOpen: true,
              displayCompletePersonProfilePopup: false,
              process: false,
            })
        },
        onFailure: (error) => {
          this.setState({
            process: false
          });
          this.msg.show(localizations.popup_newSportunity_profile_update_error, {
            time: 0,
            type: 'error',
          });
	        setTimeout(() => this.msg.removeAll(), 2000);
        },
      }
    );
  }

  _handleConfirmBusinessProfileUpdate = (data) => {
    this.props.relay.commitUpdate(
      new UpdateUserProfileMutation({
        viewer: this.props.viewer,
        userIDVar: this.props.viewer.me.id,
        lastNameVar: data.lastName,
        firstNameVar: data.firstName,
        addressVar: data.address,
        nationalityVar: data.nationality,
        businessNameVar: data.businessName,
        businessEmailVar: data.businessEmail,
        VATNumberVar: data.VATNumber,
        headquarterAddressVar: data.headquarterAddress,
        shouldDeclareVATVar: data.shouldDeclareVAT,
        birthdayVar: data.birthday
      }), {
        onSuccess: (response) => {
          this.msg.show(localizations.popup_newSportunity_profile_update_success, {
            time: 2000,
            type: 'success',
          });
	        setTimeout(() => this.msg.removeAll(), 2000);
          if (this.state.fields.price.cents > 0 && !this.props.viewer.me.bankAccount)
            this.setState({
              addBankAccountOpen: true,
              displayCompleteBusinessProfilePopup: false,
              process: false,
            })
          else
            this.setState({
              confirmPopupOpen: true,
              displayCompleteBusinessProfilePopup: false,
              process: false,
            })
        },
        onFailure: (error) => {
          this.setState({
            process: false
          });
          this.msg.show(localizations.popup_newSportunity_profile_update_error, {
            time: 0,
            type: 'error',
          });
          setTimeout(function () {
            this.msg.removeAll();
          }, 2000);
        },
      }
    );
  }

  /**
   *  Edited by: Vishal Deep
   *  Purpose : Revamp/Organize
   */

  handleChangeTabButtonPress(key) {
    this.setState({tabKey:key});
  }

  verifyBankAccount = (bankAccount) => {
    if (!bankAccount.addressLine1 || !bankAccount.city || !bankAccount.country || !bankAccount.postalCode || !bankAccount.ownerName || !bankAccount.IBAN) {
      this.msg.show(localizations.popup_newSportunity_required_fields, {
        time: 3000,
        type: 'info',
      });
	    setTimeout(() => this.msg.removeAll(), 2000);
      return false;
    }
    else
      return true
  }

  _confirmAddBankAccount = (bankAccount) => {

    if (!this.verifyBankAccount(bankAccount)) return;
    this.setState({
      process: true
    });

    this.props.relay.commitUpdate(
      new AddBankAccountMutation({
        addressLine1Var: bankAccount.addressLine1,
        addressLine2Var: bankAccount.addressLine2,
        cityVar: bankAccount.city,
        postalCodeVar: bankAccount.postalCode,
        countryVar: bankAccount.country,
        ownerNameVar: bankAccount.ownerName,
        IBANVar: bankAccount.IBAN,
        BICVar: bankAccount.BIC,
        viewer: this.props.viewer,
      }),
      {
        onSuccess: (response) => {
          this.msg.show(localizations.popup_newSportunity_update_success, {
            time: 2000,
            type: 'success',
          });
	        setTimeout(() => this.msg.removeAll(), 2000);
          this.setState({
            process: false,
          });
          this._hideAddBankAccount();
        },
        onFailure: (error) => {
          this.setState({
            process: false,
          });
          let errorField = '';
          for (var error in JSON.parse(error.getError().source.errors[0].message)) {
            errorField = error;
          }
          this.msg.show(localizations.popup_newSportunity_invalid_field + errorField, {
            time: 2000,
            type: 'error',
          });
	        setTimeout(() => this.msg.removeAll(), 2000);
        },
      }
    );
  }

  isValidCard(card) {
    return (card.cardNumber && card.cardExpirationDate && card.cardCvx && card.cardType)
  }

  _handleConfirmAddACard = (cardRegistration, card) => {

    if (!this.isValidCard(card)) {
      this.msg.show(localizations.popup_addACard_invalid_card, {
        time: 2000,
        type: 'error',
      });
	    setTimeout(() => this.msg.removeAll(), 2000);
      return;
    }

    this.setState({
      process: true,
    });

    let that = this;
    if (card)
      mangoPay.cardRegistration.init({
        Id: cardRegistration.cardRegistrationId,
        cardRegistrationURL: cardRegistration.cardRegistrationURL,
        accessKey: cardRegistration.accessKey,
        preregistrationData: cardRegistration.preregistrationData,
      });
    mangoPay.cardRegistration.registerCard(
      card,
      function (res) {
        that.updateCard(cardRegistration, res.RegistrationData);
      },
      function (res) {
        // Handle error, see res.ResultCode and res.ResultMessage
        that.setState({
          process: false,
        });
        if (res.ResultCode === "105202")
          that.msg.show(localizations.popup_addACard_error_105202, {
            time: 0,
            type: 'error',
          });
        else if (res.ResultCode === "105203")
          that.msg.show(localizations.popup_addACard_error_105203, {
            time: 0,
            type: 'error',
          });
        else if (res.ResultCode === "105204")
          that.msg.show(localizations.popup_addACard_error_105204, {
            time: 0,
            type: 'error',
          });
        else 
          that.msg.show(res.ResultMessage, {
            time: 0,
            type: 'error',
          });
      }
    );
  }

  updateCard(cardRegistration, registrationData) {
    this.props.relay.commitUpdate(
      new RegisterCardDataMutation({
        viewer: this.props.viewer,
        cardRegistration: cardRegistration,
        registrationData: registrationData,
      }), {
        onSuccess: (res) => {
          this.msg.show(localizations.popup_addACard_success, {
            time: 2000,
            type: 'success',
          });
	        setTimeout(() => this.msg.removeAll(), 2000);
          this.setState({
            selectedCard: res.registerCardData.viewer.me.paymentMethods.length > 0 ? res.registerCardData.viewer.me.paymentMethods[res.registerCardData.viewer.me.paymentMethods.length - 1] : '',
            selectedCardToPaySecondaryOrganizers: res.registerCardData.viewer.me.paymentMethods.length > 0 ? res.registerCardData.viewer.me.paymentMethods[res.registerCardData.viewer.me.paymentMethods.length - 1] : '',
            me: res.registerCardData.viewer.me,
            confirmPopupOpen: true,
            addCardPopupOpen: false,
            addACardToPaySecondaryOrganizers: false,
            cardJustAdded: true,
            process: false,
          })

        },
        onFailure: (error) => {
          this.setState({
            process: false,
          });
          this.msg.show(localizations.popup_addACard_error, {
            time: 5000,
            type: 'error',
          });
	        setTimeout(() => this.msg.removeAll(), 2000);
        },
      }
    );
  }

  _handleChangeSelectedCard = (e) => {
    this.props.viewer.me.paymentMethods.find(paymentMethod => {
      if (paymentMethod.cardMask == e.target.value) {
        this.setState({
          selectedCard: paymentMethod,
        })
        return true;
      }
      return false;
    })
  }

  _handleChangeSelectedCardToPaySecondaryOrganizer = (e) => {
    if (e === "wallet" || e.target.value === "wallet") {
      this.setState({
        paySecondaryOrganizersWithWallet: true,
        selectedCardToPaySecondaryOrganizers: '',
      })
    }
    else if (e === "addACard" || e.target.value === "addACard") {
      this.setState({
        paySecondaryOrganizersWithWallet: false,
        selectedCardToPaySecondaryOrganizers: ''
      })
    }
    else {
      this.props.viewer.me.paymentMethods.find(paymentMethod => {
        if (paymentMethod.cardMask == e.target.value) {
          this.setState({
            selectedCardToPaySecondaryOrganizers: paymentMethod,
            paySecondaryOrganizersWithWallet: false
          })
          return true;
        }
        return false;
      })
    }
  }

  componentDidMount = () => {
    this.props.relay.setVariables({
      queryLanguage: localizations.getLanguage().toUpperCase()
    })
    if (this.props.relay.variables.sportunityId) {
      let isAdmin = false;
      this.props.viewer.sportunity.organizers.forEach(organizer => {
        if (this.props.viewer.me && organizer.organizer.id == this.props.viewer.me.id)
          isAdmin = true;
      });

      if (!isAdmin) {
        let path = '/event-view/' + this.props.viewer.sportunity.id;
        this.props.router.push({
          pathname: path,
        })
      }
      else {
        let route = this.props.route.path;
        let isReorganizing = false
        if (route.indexOf('event-reorganize') >= 0)
          isReorganizing = true;
        let sportunity = this.props.viewer.sportunity;
        this.props.relay.setVariables({
          filterName: {
            name: sportunity.sport.sport.name[localizations.getLanguage().toUpperCase()],
            language: localizations.getLanguage().toUpperCase()
          },
          sportsNb: 10
        });
        this.setState(initialState);
        if (this.props.userCurrency && this.props.userCurrency !== 'CHF') {
          this.setState(prevState => ({
            ...prevState,
            fields: {
              ...prevState.fields,
              price: {
                cents: 0,
                currency: this.props.userCurrency,
              },
              venue: {
                id: '',
                name: '',
                price: {
                  cents: 0,
                  currency: this.props.userCurrency,
                },
                address: {
                  address: '',
                },
              },
              slot: {
                id: '',
                from: '',
                end: '',
                price: {
                  cents: 0,
                  currency: this.props.userCurrency
                }
              },
            },
          }));
        }

        this.setState(prevState => ({
          ...prevState,
          isModifying: !isReorganizing,
          isReorganizing: isReorganizing,
          fields: {
            ...prevState.fields,
            title: sportunity.title,
            description: sportunity.description,
            participantRange: {
              from: sportunity.participantRange.from,
              to: sportunity.participantRange.to
            },
            price: {
              cents: sportunity.price.cents / 100,
              currency: sportunity.price.currency,
            },
            organizers: sportunity.organizers.map(organizer => {
                if (organizer.isAdmin)
                  return false;
                else 
                  return {
                    organizer: organizer.organizer.id,
                    price: {
                      cents: organizer.price.cents / 100,
                      currency: organizer.price.currency
                    },
                    secondaryOrganizerType: organizer.secondaryOrganizerType ? organizer.secondaryOrganizerType.id : null,
                    customSecondaryOrganizerType: organizer.customSecondaryOrganizerType,
                  }
              }).filter(i => Boolean(i)),
            circlesOfPendingOrganizers: sportunity.pendingOrganizers && sportunity.pendingOrganizers.length > 0 
              ? sportunity.pendingOrganizers.map(org => ({
                  id: org.id, 
                  circles: org.circles.edges.map(edge => edge.node),
                  price: {cents: org.price.cents / 100, currency: org.price.currency},
                  secondaryOrganizerType: org.secondaryOrganizerType ? org.secondaryOrganizerType.id : null,
                  customSecondaryOrganizerType: org.customSecondaryOrganizerType,
                }))
              : [],
            private: sportunity.kind === 'PRIVATE',
            autoSwitchPrivacy: sportunity.privacy_switch_preference &&
            sportunity.privacy_switch_preference.privacy_switch_type === 'Automatically',
            autoSwitchPrivacyXDaysBefore: sportunity.privacy_switch_preference && sportunity.privacy_switch_preference.privacy_switch_type === 'Automatically'
              ? sportunity.privacy_switch_preference.switch_privacy_x_days_before
              : 15,
            venue: sportunity.venue,
            infrastructure: sportunity.infrastructure, 
            slot: sportunity.slot, 
            sport: {
              bold: {
                from: 0,
                to: 0
              },
              name: sportunity.sport.sport.name[localizations.getLanguage().toUpperCase()],
              logo: sportunity.sport.sport.logo,
              value: sportunity.sport.sport.id,
              id: sportunity.sport.sport.id
            },
            levelFrom: sportunity.sport.levels.length > 0 ? { name: sportunity.sport.levels[0][localizations.getLanguage().toUpperCase()].name, value: sportunity.sport.levels[0].id } : null,
            levelTo: sportunity.sport.levels.length > 0 ? { name: sportunity.sport.levels[sportunity.sport.levels.length - 1][localizations.getLanguage().toUpperCase()].name, value: sportunity.sport.levels[sportunity.sport.levels.length - 1].id } : null,
            positions: sportunity.sport.positions ? sportunity.sport.positions.map(position => { return { name: position[localizations.getLanguage().toUpperCase()], value: position.id } }) : [],
            certificates: sportunity.sport.certificates ? sportunity.sport.certificates.map(certificate => { return { name: certificate.name[localizations.getLanguage().toUpperCase()], value: certificate.id } }) : [],
            sportunityType: sportunity.sportunityType ? { name: sportunity.sportunityType.name[localizations.getLanguage().toUpperCase()], value: sportunity.sportunityType.id } : null,
            opponent: 
              sportunity.game_information && sportunity.game_information.opponent && sportunity.game_information.opponent.organizer 
              ? sportunity.game_information.opponent.organizer 
              : sportunity.game_information.opponent.organizerPseudo
                ? {pseudo: sportunity.game_information.opponent.organizerPseudo}
                : null,
            isOpenMatch: sportunity.game_information.opponent.lookingForAnOpponent,
            unknownOpponent: sportunity.game_information.opponent.unknownOpponent,
            circleOfOpponents: sportunity.game_information.opponent.invitedOpponents && sportunity.game_information.opponent.invitedOpponents.edges && sportunity.game_information.opponent.invitedOpponents.edges.length > 0
              ? sportunity.game_information.opponent.invitedOpponents.edges[0].node
              : null, 
            address: {
              address: sportunity.address.address,
              country: sportunity.address.country,
              city: sportunity.address.city,
            },
            invited: (sportunity.invited.length > 0? sportunity.invited : [])
                .filter(invited => {
                  let isInACircle = false;
                  if (sportunity.invited_circles && sportunity.invited_circles.edges && sportunity.invited_circles.edges.length > 0) {
                    sportunity.invited_circles.edges.forEach(edge => {
                      if (edge.node.members && edge.node.members.length > 0) {
                        if (edge.node.members.findIndex(member => member.id === invited.user.id) >= 0)
                          isInACircle = true
                      }
                    })
                  }

                  return !isInACircle;
                })
                .map(invited => ({ pseudo: invited.user.pseudo, avatar: invited.user.avatar })),
            invited_circles: sportunity.invited_circles && sportunity.invited_circles.edges && sportunity.invited_circles.edges.length > 0
              ? sportunity.invited_circles.edges.map(edge => edge.node)
              : [],
            invited_circles_and_prices:
            sportunity.price_for_circle && sportunity.invited_circles && sportunity.invited_circles.edges && sportunity.invited_circles.edges.length > 0
              ? sportunity.invited_circles.edges.map(edge => {
                let circlePriceIndex = sportunity.price_for_circle.findIndex(item => item.circle.id === edge.node.id);

                if (circlePriceIndex >= 0)
                  return {
                    circle: edge.node,
                    price: {
                      cents: sportunity.price_for_circle[circlePriceIndex].price.cents / 100,
                      currency: sportunity.price_for_circle[circlePriceIndex].price.currency
                    },
                    participantByDefault: sportunity.price_for_circle[circlePriceIndex].participantByDefault,
                    excludedParticipantByDefault: sportunity.price_for_circle[circlePriceIndex].participantByDefault ? {
                      excludedMembers: sportunity.price_for_circle[circlePriceIndex].excludedParticipantByDefault.excludedMembers.map(user => ({
                        id: user.id,
                      }))
                    } : null
                  };
                else
                  return {
                    circle: edge.node,
                    price: {
                      cents: sportunity.price.cents / 100,
                      currency: sportunity.price.currency,
                    }
                  }
              })
              : [],
            notificationType: sportunity.notification_preference && sportunity.notification_preference.notification_type ? sportunity.notification_preference.notification_type : 'Now',
            notificationAutoXDaysBefore: sportunity.notification_preference && sportunity.notification_preference.send_notification_x_days_before ? sportunity.notification_preference.send_notification_x_days_before : 15,
            organizerParticipates: sportunity.participants.find(participant => { return participant.id == this.props.viewer.me.id }),
            organizerParticipation: sportunity.organizers[0].price ? -1 * sportunity.organizers[0].price.cents / 100 : 0,
            sexRestriction: sportunity.sexRestriction,
            ageRestriction: { from: sportunity.ageRestriction.from, to: sportunity.ageRestriction.to },
            hideParticipantList: sportunity.hide_participant_list,
          },
          isParticipant: !!sportunity.participants.find(participant => { return participant.id == this.props.viewer.me.id }),
          notifyPeople: false,
        }));
        if (!isReorganizing) {
	        if (sportunity.survey && sportunity.survey.surveyDates && sportunity.survey.surveyDates.length > 0) {
	          this.setState({isSurvey: true});
	          this.setState({isSurveyTransformed: sportunity.survey.isSurveyTransformed});
		        sportunity.survey.surveyDates.forEach((date, index) => {
			        this._handleDateChange({
				        beginningDate: date.beginning_date,
				        endingDate: date.ending_date,
				        repeat: route.indexOf('serie-edit') >= 0 ? sportunity.number_of_occurences - sportunity.is_repeated_occurence_number - 1 : 0,
				        scheduleId: nextScheduleId++
			        });
            })
          }
          else
            this._handleDateChange({
              beginningDate: sportunity.beginning_date,
              endingDate: sportunity.ending_date,
              repeat: route.indexOf('serie-edit') >= 0 ? sportunity.number_of_occurences - sportunity.is_repeated_occurence_number - 1 : 0,
              scheduleId: 0
		        });
        }
        if (route.indexOf('serie-edit') >= 0)
          this.setState({ isModifyingASerie: true });

      }
      this.props.relay.setVariables({
        queryDetails: true, 
        querySuperMe: true,
        superToken:localStorage.getItem('superToken'),
      })
    }
    else {
      this.setState(initialState);
      if (this.props.userCurrency && this.props.userCurrency !== 'CHF') {
        this.setState(prevState => ({
          ...prevState,
          fields: {
            ...prevState.fields,
            price: {
              cents: 0,
              currency: this.props.userCurrency,
            },
            venue: {
              id: '',
              name: '',
              price: {
                cents: 0,
                currency: this.props.userCurrency,
              },
              address: {
                address: '',
              },
            },
            slot: {
              id: '',
              from: '',
              end: '',
              price: {
                cents: 0,
                currency: this.props.userCurrency
              }
            },
          },
        }));
      }

      this.props.relay.setVariables({
        queryDetails: true, 
        querySuperMe: true,
        superToken:localStorage.getItem('superToken'),
      },readyState => {
        if (readyState.done) {
          setTimeout(() => {
            if ((this.props.viewer.me && this.props.viewer.me.circles && this.props.viewer.me.circles.edges && this.props.viewer.me.circles.edges.length > 0) ||
              (this.props.viewer.me && this.props.viewer.me.circlesUserIsIn && this.props.viewer.me.circlesUserIsIn.edges && this.props.viewer.me.circlesUserIsIn.edges.length > 0)) {
              let circleList = [];
      
              if (this.props.viewer.me && this.props.viewer.me.circles && this.props.viewer.me.circles.edges && this.props.viewer.me.circles.edges.length > 0)
                this.props.viewer.me.circles.edges.forEach(edge => circleList.findIndex(circle => circle.node.id === edge.node.id) < 0 && circleList.push(edge))
      
              if ((this.props.viewer.me && this.props.viewer.me.circlesUserIsIn && this.props.viewer.me.circlesUserIsIn.edges && this.props.viewer.me.circlesUserIsIn.edges.length > 0))
                this.props.viewer.me.circlesUserIsIn.edges.forEach(edge => circleList.findIndex(circle => circle.node.id === edge.node.id) < 0 && circleList.push(edge))
              
              circleList
                .filter(c => c.node.type === 'CHILDREN' || c.node.type === 'ADULTS')
                .sort((a,b) => {
                  if (a.node.memberCount < b.node.memberCount) return 1;
                  if (a.node.memberCount > b.node.memberCount) return -1;
                  else return 0; 
                })
                .filter((e,i) => i <= 2)
                .forEach(item => {
                  setTimeout(() => this._handleAddInvitedCircleAndPrice(item.node), 50)
                })
            }
          }, 100);
        }
      })
    }   

    setTimeout(() => this.setState({ loading: false }), 1500);
  }

  toggleSurvey = () => {
	  let newList = cloneDeep(this.state.fields.invited_circles_and_prices) || [];
	  newList.forEach((item, index) => {
		  newList[index].participantByDefault = false;
	  })
    this.setState((prevState) => ({
	    ...prevState,
	    fields: {
		    ...prevState.fields,
		    schedules: [],
        beginningDate: null,
		    endingDate: null,
		    repeat: null,
		    invited_circles_and_prices: newList,
	    },
	    isSurvey: !prevState.isSurvey
    }))
  }

  componentWillReceiveProps = (nextProps) => {
    if ('/new-sportunity' == this.props.location.pathname && this.props.relay.variables.sportunityId) {
      this.props.relay.setVariables({
        sportunityId: null
      })
      this.setState(initialState);
      setTimeout(() => this.setState({ loading: false }), 1500);
    }
    if (this.props.viewer.me && !isEqual(this.props.viewer.me, nextProps.viewer.me) && this.state.fields.invited_circles_and_prices && this.state.fields.invited_circles_and_prices.length > 0) {
      this.state.fields.invited_circles_and_prices.forEach((invitedCircle, index)  => {
        let newIndex = nextProps.viewer.me.circles.edges.findIndex(circle => circle.node.id === invitedCircle.circle.id);
        if (newIndex >= 0) {
          let newList = cloneDeep(this.state.fields.invited_circles_and_prices) || [];
          
          newList[index].circle = nextProps.viewer.me.circles.edges[newIndex].node;

          this.setState(prevState => ({
            ...prevState,
            fields: {
              ...prevState.fields,
              invited_circles_and_prices: newList,
            }
          }))
        }
      })
    }
    if (this.props.userCurrency !== nextProps.userCurrency) {
      let newList = cloneDeep(this.state.fields.invited_circles_and_prices) || [];

      this.state.fields.invited_circles_and_prices.forEach((invitedCircle, index)  => {
        newList[index].price = {cents: newList[index].price.cents, currency: nextProps.userCurrency}
      })
      this.setState(prevState => ({
        ...prevState,
        fields: {
          ...prevState.fields,
          invited_circles_and_prices: newList,
        }
      }))

      this.setState(prevState => ({
        ...prevState,
        fields: {
          ...prevState.fields,
          price: {
            cents: 0,
            currency: nextProps.userCurrency,
          },
          venue: {
            id: '',
            name: '',
            price: {
              cents: 0,
              currency: nextProps.userCurrency,
            },
            address: {
              address: '',
            },
          },
          slot: {
            id: '',
            from: '',
            end: '',
            price: {
              cents: 0,
              currency: nextProps.userCurrency
            }
          },
        },
      }));
    }
  }

  _translatedName = (name) => {
    let translatedName = name.EN
    switch (localizations.getLanguage().toLowerCase()) {
      case 'en':
        translatedName = name.EN
        break
      case 'fr':
        translatedName = name.FR || name.EN
        break
      case 'it':
        translatedName = name.IT || name.EN
        break
      case 'de':
        translatedName = name.DE || name.EN
        break
      default:
        translatedName = name.EN
        break
    }
    return translatedName
  }

  _translatedLevelName = (levelName) => {
    let translatedName = levelName.EN.name
    switch (localizations.getLanguage().toLowerCase()) {
      case 'en':
        translatedName = levelName.EN.name
        break
      case 'fr':
        translatedName = levelName.FR.name || levelName.EN.name
        break
      case 'it':
        translatedName = levelName.IT.name || levelName.EN.name
        break
      case 'de':
        translatedName = levelName.DE.name || levelName.EN.name
        break
      default:
        translatedName = levelName.EN.name
        break
    }
    return translatedName
  }

  removeTemplate = (value) => {
    if (this.state.selectedTemplate && value.id === this.state.selectedTemplate.value.id)
      this.removeSelection();
    this.props.relay.commitUpdate(
      new RemoveSportunityTemplateMutation({
        viewer: this.props.viewer,
        sportunity: value
      }),
      {
        onSuccess: () => {
          this.msg.show(this.state.isModifyingASerie
            ? localizations.popup_newSportunity_update_serie_success
            : localizations.popup_newSportunity_update_success, {
            time: 3500,
            type: 'success',
          });
          this.setState({
            process: false,
          });
          setTimeout(() => {
            this.msg.removeAll();
          }, 2000);
        },
        onFailure: (error) => {
          this.setState({
            process: false,
          });
          console.log(error.getError());
        },
      })
  };

  onEdit = (item) => {
    this.setState({
      fromTemplate: true
    });
    this._changeTemplate(item.value, item)
  };

  removeSelection = () => {
    this.setState({
      fromTemplate: false,
      selectedTemplate: null
    });
  };

  handleNext = (step) => {
    const { selectedTab, errors } = this.state;
    let customErrors = this._validate(step);
    if(this.state.errors.length > 0 || customErrors.length > 0) {
      this.setState({
        selectedTab: selectedTab,
      finished: selectedTab >= 8,
      errors: [...errors, ...customErrors],
    });

    } else {
      this.setState({
      selectedTab: selectedTab + 1,
      finished: selectedTab >= 8,
      errors: [...errors, ...customErrors],
    });
    }
   

   

    
  };

  handlePrev = () => {
    const {selectedTab} = this.state;
    if (selectedTab > 1) {
      this.setState({selectedTab: selectedTab - 1});
    }
  };

  activateTabPressed = (value) => {

    const { selectedTab, errors } = this.state;
    let step = selectedTab - 1;
    let customErrors = this._validate(step);

    if(this.state.errors.length > 0 || customErrors.length > 0) {
      this.setState({
     // selectedTab: selectedTab,
      selectedTab: value,
      errors:[...customErrors]
    });

    } else {
      this.setState({
      selectedTab: value,
    });
    }
  };

  renderStepActions(step) {
    const { selectedTab } = this.state;

    return (
      <div style={{margin: '12px 0'}}>
        {step > 0 && (
          <FlatButton
            label="Back"
            disabled={selectedTab === 1}
            disableTouchRipple={true}
            disableFocusRipple={true}
            onClick={this.handlePrev}
            style={{marginRight: 12}}
          />
        )}
        <RaisedButton
          label={selectedTab === 8 ? 'Cancel' : 'Next'}
          disableTouchRipple={true}
          disableFocusRipple={true}
          onClick={()=> this.handleNext(step)}
          backgroundColor = '#5EA1D9'
          labelColor='#FFFFFF' 
        
        />
      </div>
    );
  }
  _priceValueChange = (e) => {
   let value = e.target.value;
   this.setState(prevState => ({
    ...prevState,
        fields: {
          ...prevState.fields,
          price: {
            ...prevState.fields.price,
            cents: value,
          },
        }
   }))
   setTimeout(() => { this.privatePriceInput.focus(); }, 10);
   
     
  }

  _priceValueChangeByCommunity = (circle, i, e) => {
     this._handleChangeInvitedCirclePrice(circle, e);

   setTimeout(() => { this['privatePriceInput' + i].focus(); }, 10);
  }

  showSelectedTabContent = () => {
    
   const { viewer } = this.props;

   const sportsList = viewer && viewer.sports ?
     viewer.sports.edges.map(({ node }) => ({ ...node, name: this._translatedName(node.name), value: node.id }))
     : [];

    const errors = this._validate();

   const levelsList = this.state.fields.sport && viewer.sports.edges.find(({ node }) => this._translatedName(node.name) === this.state.fields.sport.name)
     ? viewer.sports.edges
       .find(({ node }) => this._translatedName(node.name) === this.state.fields.sport.name).node.levels
       .map(l => ({ name: this._translatedLevelName(l), value: l.id, skillLevel: l[localizations.getLanguage().toUpperCase()].skillLevel, description: l[localizations.getLanguage().toUpperCase()].description }))
       .sort((a, b) => { return a.skillLevel - b.skillLevel })
     : [];
   const certificatesList = this.state.fields.sport && viewer.sports.edges.find(({ node }) => this._translatedName(node.name) === this.state.fields.sport.name)
     ? viewer.sports.edges
       .find(({ node }) => this._translatedName(node.name) === this.state.fields.sport.name).node.certificates
       .map(c => ({ name: this._translatedName(c.name), value: c.id }))
     : [];

   const templateList = viewer.me && viewer.me.sportunityTemplates ? viewer.me.sportunityTemplates.map(template =>  ({name: template.title, value: template})) : [];

   const positionsList = this.state.fields.sport && viewer.sports.edges.find(({ node }) => this._translatedName(node.name) === this.state.fields.sport.name)
     ? viewer.sports.edges
       .find(({ node }) => this._translatedName(node.name) === this.state.fields.sport.name).node.positions
       .map(p => ({ name: this._translatedName(p), value: p.id }))
     : [];

   const sportunityTypesList = this.state.fields.sport && viewer.sports.edges.find(({ node }) => this._translatedName(node.name) === this.state.fields.sport.name)
     ? viewer.sports.edges
       .find(({ node }) => this._translatedName(node.name) === this.state.fields.sport.name).node.sportunityTypes
       .map(p => ({ name: this._translatedName(p.name), value: p.id, isScoreRelevant: p.isScoreRelevant }))
     : [];

   const pendingVariables = this.props.relay.pendingVariables;

   const {finished, selectedTab} = this.state;

   const circlesCurrentUserIsIn = viewer && viewer.me && viewer.me.circlesUserIsIn && viewer.me.circlesUserIsIn.edges && viewer.me.circlesUserIsIn.edges.length > 0
     ? viewer.me.circlesUserIsIn.edges
       .map(edge => {
         if (edge.node.isCircleUsableByMembers)
           return edge
         else
           return false
       })
       .filter(i => Boolean(i))
     : []
   switch(this.state.selectedTab) {
      case 1: 
       return (<Paper zDepth={4} style={{ padding: '8px 70px 1px' }}>
         <div>
         <div style = {{display : 'inline-block', width : '50%', verticalAlign: 'top'}}>
            <h1 style = {styles.title}>{localizations.newSportunity_autoParticipateUnswitchACircleModalTitle}</h1>
            <h3 style = {styles.subtitle}>Step 1 of 8</h3>
         </div>
         <div style = {{display : 'inline-block', width : '50%', verticalAlign: 'top'}}>
            <SelectTemplate
            style={styles.select}
            label= "Or use a saved activity"
            list={templateList}
            values={this.state.selectedTemplate}
            placeholder={templateList === [] ? "Activity name" : "Activity name"}
            onChange={(item) => item ? this._changeTemplate(item.value, item) : this.removeSelection()}
            onEdit={this.onEdit}
            disabled={templateList === []}
            onRemove={this.removeTemplate}
            />
         </div>
         <hr style= {{marginBottom : 25, marginLeft :-70, marginRight : -70}}></hr>
         <div style = {{display : 'inline-block', width : '50%', verticalAlign: 'top'}}>
         <Input
            containerStyle={styles.input}
            label="Title"
            placeholder= "Activity name"
            onChange={this._updateTextField.bind(this, "title")}
            onBlur={this._updateTextFieldFinal.bind(this, "title")}
            value={this.state.fields.title}
            error={this.state.errors.some(() => "Name")}
            errorMessage={this.showErrorMessge("Name")}
            maxLength="31"
            onRef={node =>{this.textInputTitle = node} }
        />
        
         <Input
            containerStyle={styles.input}
            label={localizations.newSportunity_description}
            placeholder={localizations.newSportunity_descriptionHolder}
            onChange={this._updateTextField.bind(this, 'description')}
            onBlur={this._updateTextFieldFinal.bind(this, 'description')}
            value={this.state.fields.description}
            errorMessage={this.showErrorMessge("Description")}
            error={this.state.errors.some(() => "Description")}
            onRef={node => {this.textInputDescription = node} }
            type="textarea"
       />
         </div>
         {/* <Subtitle>
            {localizations.newSportunity_template}
         </Subtitle> */}
         <hr style = {styles.hr}></hr>
        {this.renderStepActions(0)}
      </div></Paper>);
      case 2: 
      return (<div><Paper zDepth={4} style={{ padding: '8px 70px 1px' }}>
         <h1 style = {styles.title}>{localizations.newSportunity_sport}</h1>
         <h3 style = {styles.subtitle}>Step 2 of 8</h3>
         <hr style={{ marginBottom: 25, marginLeft :-70, marginRight : -70 }}></hr>

      
        <div style={{ width: '40%' }}>
         <Sports
               style={styles.select}
               label={localizations.newSportunity_sport}
               onChange={this._updateSportField.bind(this)}
               onSearching={this._updateSportFilter.bind(this)}
               list={sportsList}
               placeholder={localizations.newSportunity_sportHolder}
               onLoadAllClick={this._handleLoadAllSports.bind(this)}
               allSportLoaded={this.state.allSportsLoaded}
               loadingAllSports={pendingVariables}
               error={this.state.errors.some(() => "Sport")}
               errorMessage={this.showErrorMessge("Sport")}
               value={this.state.fields.sport ? this.state.fields.sport.name : this.state.sportSearch}
               onRefS={node => {this.textInputSports1 = node} }
               passingRef={this.textInputSports1}
               
         />
         <SportLevels
               style={styles.select}
               label={localizations.newSportunity_level}
               list={levelsList}
               from={this.state.fields.levelFrom}
               to={this.state.fields.levelTo}
               placeholder={!this.state.fields.sport ? localizations.newSportunity_levelHolderBefore : localizations.newSportunity_levelHolder }
               onFromChange={this._updateField.bind(this, 'levelFrom')}
               onToChange={this._updateField.bind(this, 'levelTo')}
               disabled={!this.state.fields.sport}
         />
         {/* positionsList.length > 0 &&
               <Select
               style={styles.select}
               label={localizations.newSportunity_position}
               list={positionsList}
               values={this.state.fields.positions}
               placeholder={!this.state.fields.sport ? localizations.newSportunity_positionHolderBefore : localizations.newSportunity_positionHolder}
               onChange={this._addFieldToList.bind(this, 'positions')}
               disabled={!this.state.fields.sport}
               />
         */}
         { certificatesList.length > 0 && 
               <Select
               style={styles.select}
               label={localizations.newSportunity_certificate}
               list={certificatesList}
               values={this.state.fields.certificates}
               placeholder={!this.state.fields.sport ? localizations.newSportunity_certificateHolderBefore : localizations.newSportunity_certificateHolder}
               onChange={this._addFieldToList.bind(this, 'certificates')}
               disabled={!this.state.fields.sport}
               />
         }
         {viewer && viewer.me && sportunityTypesList && sportunityTypesList.length > 0 &&
               <Select
               style={styles.select}
               label={localizations.newSportunity_sportunityType}
               list={sportunityTypesList}
               values={this.state.fields.sportunityType}
               placeholder={!this.state.fields.sport ? localizations.newSportunity_sportunityTypeHolderBefore : localizations.newSportunity_sportunityTypeHolder}
               onChange={this._updateSportunityTypeField.bind(this)}
               disabled={!this.state.fields.sport}
               singleChoice={true}
               />
         }
         </div>
         {viewer && viewer.me && /*viewer.me.profileType !== 'PERSON' &&*/ this.state.fields.sportunityType && sportunityTypesList.findIndex(type => type.value === this.state.fields.sportunityType.value && type.isScoreRelevant) >= 0 && 
          
            <Opponent
               viewer={viewer}
               sport={this.state.fields.sport}
               isLoggedIn={viewer.me ? true : false}
               selectedOpponent={this.state.fields.opponent}
               isOpenMatch={this.state.fields.isOpenMatch}
               unknownOpponent={this.state.fields.unknownOpponent}
               circleOfOpponents={this.state.fields.circleOfOpponents}
               onChange={this._updateOpponentField.bind(this)}
               openMatchSwitch={this._handleOpenMatchSwitch}
               unknownOppponentSwitch={this._handleUnknownOpponentSwitch}
               onChangeCircle={this._updateCircleOpponentField.bind(this)}
               error={this.state.errors.some(() => "Opponent")}
               errorMessage={this.showErrorMessge("Opponent")}
               stepAction={this.renderStepActions(1)}
               circlesList={this.props.viewer.me && this.props.viewer.me.circles && this.props.viewer.me.circles.edges ? this.props.viewer.me.circles.edges.filter(edge => edge.node.type === "TEAMS" || edge.node.type === "CLUBS") : []}
               circlesCurrentUserIsIn={circlesCurrentUserIsIn.filter(edge => edge.node.type === "TEAMS" || edge.node.type === "CLUBS")}
          />
        
         }
         
      
       </Paper>

       <CircleList 
                unknownOpponent={this.state.fields.unknownOpponent}
                circleOfOpponents={this.state.fields.circleOfOpponents}
                selectedOpponent={this.state.fields.opponent} 
                viewer={viewer} 
                isOpenMatch={this.state.fields.isOpenMatch} 
                circleList={this.state.circleList} 
                label={localizations.myOpponentPropose} 
                value={this.state.fields.circleOfOpponents}
                error={this.state.errors.some(() => "Opponent")} 
                renderStepActions ={this.renderStepActions(1)}
                />

       </div>);
       
      case 3: 
       return (<Paper zDepth={4} style={{ padding: '8px 70px 1px' }}><div>
         <h1 style = {styles.title}>Location</h1>
         <h3 style = {styles.subtitle}>Step 3 of 8</h3>
         <hr style= {{marginBottom : 25,  marginLeft :-70, marginRight : -70}}></hr>
         <VenueOrAddress 
            viewer={viewer}
            sport={this.state.fields.sport}
            onChangeAddress={this._handleAddressChange}
            onChangeSlot={this._handleSlotChange}
            onChangeInfrastructure={this._handleInfrastructureChange}
            address={this.state.fields.address.address 
            ? this.state.fields.address.address + ', ' + this.state.fields.address.city + ', ' + this.state.fields.address.country 
            : this.state.fields.venue && this.state.fields.venue.address && this.state.fields.venue.address.address
                  ? this.state.fields.venue.address.address
                  : ''
            }
            venue={this.state.fields.venue}
            infrastructure={this.state.fields.infrastructure}
            slot={this.state.fields.slot}
            isLoggedIn={viewer.me ? true : false}
            isModifying={this.state.isModifying}
            sportunityId={this.props.relay.variables.sportunityId}
         />
         <hr style = {styles.hr}></hr>
         {this.renderStepActions(2)}
      </div></Paper>);
      case 4: 
       return (<Paper zDepth={4} style={{ padding: '8px 70px 1px' }}><div>
         <h1 style = {styles.title}>{localizations.newSportunity_schedule}</h1>
         <h3 style = {styles.subtitle}>Step 4 of 8</h3>
         <hr style= {{marginBottom : 25,  marginLeft :-70, marginRight : -70}}></hr>
         {this.state.fields.slot && this.state.fields.slot.from && this.state.fields.slot.end
         ?   <VenueSlots 
               slot={this.state.fields.slot}
               error={this.state.errors.some(() => "Slot")}
               errorMessage={this.showErrorMessge("Slot")}
               repeat={this.state.fields.repeat}
               />
         :
            <Schedule
               onSubmit={this._handleDateChange}
               onDelete={this._handleDateDelete}
               onEdit={this._handleDateEdit}
               onAdd={this._handleDateAdd}
               beginningDate={this.state.fields.beginningDate}
               endingDate={this.state.fields.endingDate}
               repeat={this.state.fields.repeat}
               scheduleId={this.state.fields.scheduleId}
               schedules={this.state.fields.schedules}
               error={this.state.errors.some(() => "Date")}
               errorMessage={this.showErrorMessge("Date")}
               isModifying={this.state.isModifying}
               isSurvey={this.state.isSurvey}
               onChangeSurvey={this.toggleSurvey}
               isSurveyTransformed={this.state.isSurveyTransformed}
            />
         } 
         <div style = {{height:350, width:400}}>
         </div>
         <hr style = {styles.hr}></hr>
      {this.renderStepActions(3)}
      </div></Paper>);
      case 5: 
       return (<div><Paper zDepth={4} style={{ padding: '8px 70px 1px' }}>
         <h1 style = {styles.title}>Participants</h1>
         <h3 style = {styles.subtitle}>Step 5 of 8</h3>
         <hr style= {{marginBottom : 25,  marginLeft :-70, marginRight : -70}}></hr>
          <Participants
            viewer={viewer}
            isLoggedIn={viewer.me ? true : false}
            value={this.state.fields.participantRange}
            hideParticipantList={this.state.fields.hideParticipantList}
            organizerParticipates={this.state.fields.organizerParticipates}
            price={this.state.fields.price}
            isModifying={this.state.isModifying}
            onChange={this._updateField.bind(this, 'participantRange')}
            onSwitchHideParticipantList={this._updateField.bind(this, 'hideParticipantList')}
            onSwitchOrganizerIsParticipant={this._updateField.bind(this, 'organizerParticipates')}
            error={this.state.errors.some(() => 'ParticipantRange')}
            errorMessage={this.showErrorMessge('ParticipantRange')}
            {...this.state}
         /> 
         {/* <Subtitle>{this.props.viewer.me && this.props.viewer.me.profileType !== 'PERSON' ? localizations.newSportunity_invitedList_title : localizations.newSportunity_invitedList_title_person}</Subtitle> */}
         {/* <Invited
            list={this.state.fields.invited}
            invitedCircles={this.state.fields.invited_circles_and_prices}
            onRemoveItem={this._handleRemoveInvitedItem}
            onRemoveInvitedCircle={this._handleRemoveInvitedCircleAndPrice}
            onAddItem={this._handleAddInvitedItem}
            onAddCircle={this._handleAddInvitedCircleAndPrice}
            onChangeCirclePrice={this._handleChangeInvitedCirclePrice}
            onChangeCircleAutoParticipate={this._handleChangeCircleAutoParticipate}
            onChangeUserAutoParticipate={this._handleChangeUserAutoParticipate}
            viewer={this.props.viewer}
            superMe={this.props.viewer.superMe}
            isSurvey={this.state.isSurvey}
            isLoggedIn={viewer.me ? true : false}
            circlesList={this.props.viewer.me && this.props.viewer.me.circles && this.props.viewer.me.circles.edges ? this.props.viewer.me.circles.edges : []}
            circlesCurrentUserIsIn={circlesCurrentUserIsIn}
            circlesFromClub={this.props.viewer.me && this.props.viewer.me.circlesFromClub && this.props.viewer.me.circlesFromClub.edges ? this.props.viewer.me.circlesFromClub.edges.filter(el => el.node.memberCount > 0) : []}
            isModifying={this.state.isModifying}
            _handleNotificationTypeChange={this._updateTextField.bind(this, 'notificationType')}
            _handleNotificationAutoXDaysBeforeChange={this._updateField.bind(this, 'notificationAutoXDaysBefore')}
            fields={this.state.fields}
            /> */}
            <div style = {{position : 'absolute', width : '370', right : 88, top : 86}}>
         <Privacy
            privateChecked={this.state.fields.private}
            me={this.props.viewer.me}
            superMe={this.props.viewer.superMe}
            autoSwitchPrivacyChecked={this.state.fields.autoSwitchPrivacy}
            autoSwitchPrivacyXDaysBefore={this.state.fields.autoSwitchPrivacyXDaysBefore}
            _handlePrivateChange={this._handlePrivateChange}
            _handleAutoSwitchPrivacyChange={this._handleAutoSwitchPrivacyChange}
            _handleAutoSwitchPrivacyXDaysBeforeChange={this._updateField.bind(this, 'autoSwitchPrivacyXDaysBefore')}
            error={this.state.errors.some(() => 'Privacy')}
            errorMessage={this.showErrorMessge('Privacy')}
            
         />
         </div>
         </Paper>

        
        {/*  {!this.state.fields.private &&
         <Price
            price={this.state.fields.price}
            fees={this.props.viewer.me ? this.props.viewer.me.fees / 100 : 0.2}
            venue={this.state.fields.venue}
            onPriceChange={this._updatePriceField.bind(this, 'price')}
            onVenueChange={this._updateField.bind(this, 'venue')}
            participantRange={this.state.fields.participantRange}
            organizerParticipation={this.state.fields.organizerParticipation}
            disabled={!this.state.fields.participantRange.to}
            isModifying={this.state.isModifying}
            placeholder={!this.state.fields.participantRange.from ? localizations.newSportunity_priceHolderBefore : localizations.newSportunity_priceHolder}
         />
         } */}
         {/* !this.state.fields.private &&
         <Details
            onSexRestrictionChange={this._handleSexRestrictionChange}
            onAgeRestrictionChange={this._handleAgeRestrictionChange}
            sexRestriction={this.state.fields.sexRestriction}
            ageRestriction={this.state.fields.ageRestriction}
         /> */
         }
         {/*((this.state.fields.invited && this.state.fields.invited.length > 0)
            || (this.state.fields.invited_circles && this.state.fields.invited_circles.length > 0)) &&
            <NotificationToInvitees
            notificationType={this.state.fields.notificationType}
            notificationAutoXDaysBefore={this.state.fields.notificationAutoXDaysBefore}
            _handleNotificationTypeChange={this._updateTextField.bind(this, 'notificationType')}
            _handleNotificationAutoXDaysBeforeChange={this._updateField.bind(this, 'notificationAutoXDaysBefore')}
            error={this.state.errors.includes('NotificationToInvitees')}
            />
         */}

         <ParticipantList
           list={this.state.fields.invited}
           invitedCircles={this.state.fields.invited_circles_and_prices}
           onRemoveItem={this._handleRemoveInvitedItem}
           onRemoveInvitedCircle={this._handleRemoveInvitedCircleAndPrice}
           onAddItem={this._handleAddInvitedItem}
           onAddCircle={this._handleAddInvitedCircleAndPrice}
           onChangeCirclePrice={this._handleChangeInvitedCirclePrice}
           onChangeCircleAutoParticipate={this._handleChangeCircleAutoParticipate}
           onChangeUserAutoParticipate={this._handleChangeUserAutoParticipate}
           viewer={this.props.viewer}
           superMe={this.props.viewer.superMe}
           isSurvey={this.state.isSurvey}
           isLoggedIn={viewer.me ? true : false}
           circlesList={this.props.viewer.me && this.props.viewer.me.circles && this.props.viewer.me.circles.edges ? this.props.viewer.me.circles.edges : []}
           circlesCurrentUserIsIn={circlesCurrentUserIsIn}
           circlesFromClub={this.props.viewer.me && this.props.viewer.me.circlesFromClub && this.props.viewer.me.circlesFromClub.edges ? this.props.viewer.me.circlesFromClub.edges.filter(el => el.node.memberCount > 0) : []}
           isModifying={this.state.isModifying}
           _handleNotificationTypeChange={this._updateTextField.bind(this, 'notificationType')}
           _handleNotificationAutoXDaysBeforeChange={this._updateField.bind(this, 'notificationAutoXDaysBefore')}
           fields={this.state.fields}
           renderStepActions={this.renderStepActions(4)}
         />
 
      </div>);
      case 6: 
       return (<Paper zDepth={4} style={{ padding: '8px 70px 1px' }}><div>
         <h1 style = {styles.title}>{localizations.newSportunity_confirmation_popup_price}</h1>
         <h3 style = {styles.subtitle}>Step 6 of 8</h3>
         <hr style= {{marginBottom : 25, marginLeft :-70, marginRight : -70}}></hr>
         {this.state.fields.private &&
          <PrivatePrice 
             price={this.state.fields.price.cents} 
             fees={this.props.viewer.me ? this.props.viewer.me.fees / 100 : 0.2}
             participantRange={this.state.fields.participantRange}
             organizerParticipation={this.state.fields.organizerParticipation}
             currency={this.state.fields.price.currency}
             priceList={this.state.fields.invited_circles_and_prices}
             priceValueChange={this._priceValueChange}
             priceValueChangeByCommunity={this._priceValueChangeByCommunity}
             onRef={(node) => {this.privatePriceInput = node}} 
             onRefC={(i, node) => {this['privatePriceInput'+i] = node}}
             />
         }
         {!this.state.fields.private &&
         <Price
            price={this.state.fields.price}
            fees={this.props.viewer.me ? this.props.viewer.me.fees / 100 : 0.2}
            venue={this.state.fields.venue}
            onPriceChange={this._updatePriceField.bind(this, 'price')}
            onVenueChange={this._updateField.bind(this, 'venue')}
            participantRange={this.state.fields.participantRange}
            organizerParticipation={this.state.fields.organizerParticipation}
            disabled={!this.state.fields.participantRange.to}
            isModifying={this.state.isModifying}
            placeholder={!this.state.fields.participantRange.from ? localizations.newSportunity_priceHolderBefore : localizations.newSportunity_priceHolder}
         />
         }
         <hr style = {styles.hr}></hr>
         {this.renderStepActions(5)}
      </div></Paper>);
      case 7: 
       return (<Paper zDepth={4} style={{ padding: '8px 70px 1px' }}><div>
         <h1 style = {styles.title}>{localizations.event_secondary_organizer}</h1>
         <h3 style = {styles.subtitle}>Step 7 of 8</h3>
         <hr style= {{marginBottom : 25,  marginLeft :-70, marginRight : -70}}></hr>
         <Organizers
            isLoggedIn={viewer.me ? true : false}
            buttonLabel={localizations.newSportunity_addOrganizers}
            sport={this.state.fields.sport}
            viewer={viewer}
            user={viewer.me}
            organizers={this.state.fields.organizers}
            circlesOfPendingOrganizers={this.state.fields.circlesOfPendingOrganizers}
            addOrganizer={this._handleAddOrganizer}
            addCirclesOfPendingOrganizers={this._handleAddCirclesOfPendingOrganizers}
            removeOrganizer={this._handleRemoveOrganizer}
            removeCirclesOfPendingOrganizers={this._handleRemoveCirclesOfPendingOrganizers}
            updateOrganizerPrice={this._handleUpdateOrganizerPrice}
            updateCirclesOfPendingOrganizersPrice={this._handleUpdateCirclesOfPendingOrganizersPrice}
            updateOrganizerRole={this._handleUpdateOrganizerRole}
            updateCirclesOfPendingOrganizersRole={this._handleUpdateCirclesOfPendingOrganizersRole}
            updateOrganizerCustomRole={this._handleUpdateOrganizerCustomRole}
            updateCirclesOfPendingOrganizersCustomRole={this._handleUpdateCirclesOfPendingOrganizersCustomRole}
            isModifying={this.state.isModifying}
            error={this.state.errors.some(() => "Organizers")}
            errorMessage={this.showErrorMessge("Organizers")}
            
         />
         {this.state.isModifying &&
         <div style={styles.input}>
            <label style={styles.label}>
               {localizations.newSportunity_notify_people}
            </label>
            <Switch
               checked={this.state.notifyPeople}
               onChange={(checked) => this.setState({ notifyPeople: checked }) }
            />
         </div>
         }
         <hr style = {styles.hr}></hr>
         {this.renderStepActions(6)}
      </div></Paper>);
      case 8: 
       return (<Paper zDepth={4} style={{ padding: '8px 70px 1px' }}><div>
         <h1 style = {styles.title}>Validation</h1>
         <h3 style = {styles.subtitle}>Step 8 of 8</h3>
         <hr style= {{marginBottom : 25,  marginLeft :-70, marginRight : -70}}></hr>
         {
            this.state.confirmPopupOpen &&
            <ConfirmCreationPopup
            sportunity={this.state.fields}
            viewer={this.props.viewer}
            me={this.props.viewer.me}
            onClose={this._hideConfirmationCreationPopup}
            onOpenProfilePopup={this._handleShowCompleteProfile}
            onAddBankAccount={this._showAddBankAccount}
            bankAcccountJustAdded={this.state.bankAcccountJustAdded}
            onAddCard={this._showAddACardPopup}
            cardJustAdded={this.state.cardJustAdded}
            onChangeSelectedCard={this._handleChangeSelectedCard}
            selectedCard={this.state.selectedCard}
            onAddCardToPaySecondaryOrganizers={this._showAddACardToPaySecondaryOrganizersPopup}
            onChangeSelectedCardToPaySecondaryOrganizers={this._handleChangeSelectedCardToPaySecondaryOrganizer}
            selectedCardToPaySecondaryOrganizers={this.state.selectedCardToPaySecondaryOrganizers}
            paySecondaryOrganizersWithWallet={this.state.paySecondaryOrganizersWithWallet}
            onConfirm={this._confirmCreation}
            onConfirmTemplate={this._confirmCreationTemplate}
            processing={this.state.process}
            router={this.props.router}
            isModifying={this.state.isModifying}
            fromTemplate={this.state.fromTemplate}
            saveTemplate={this.state.saveTemplate}
            updateSaveTemplate={() => this.setState({saveTemplate: !this.state.saveTemplate})}
            />
         }
            {!this.state.confirmPopupOpen &&
               (this.state.isModifying ?
                  <Button
                  style={styles.submit}
                  type="submit"
                  disabled={!viewer.me}
                  onClick={this._handleSubmit}
                  >
                  Validate
                  </Button>
                  :
                  <Button
                  style={styles.submit}
                  type="submit"
                  disabled={!viewer.me}
                  onClick={this._handleSubmit}
                  >
                  Validate
                  </Button>)
            }
            <hr style = {styles.hr}></hr>
            {this.renderStepActions(7)}
      </div></Paper>);

   }
  }
  
  showErrorMessge = (elm)  => {
    let error = this.state.errors.find(a => a.element === elm);
    if (error) {
      return error.message;
    } else {
      return "";
    }
  }

  render() {
   
    
    if (this.state.loading) {
      return (<Loading />)
    }
    const { viewer } = this.props;
    

    const sportsList = viewer && viewer.sports ?
      viewer.sports.edges.map(({ node }) => ({ ...node, name: this._translatedName(node.name), value: node.id }))
      : [];

    const errors = this.state.errors;

    const levelsList = this.state.fields.sport && viewer.sports.edges.find(({ node }) => this._translatedName(node.name) === this.state.fields.sport.name)
      ? viewer.sports.edges
        .find(({ node }) => this._translatedName(node.name) === this.state.fields.sport.name).node.levels
        .map(l => ({ name: this._translatedLevelName(l), value: l.id, skillLevel: l[localizations.getLanguage().toUpperCase()].skillLevel, description: l[localizations.getLanguage().toUpperCase()].description }))
        .sort((a, b) => { return a.skillLevel - b.skillLevel })
      : [];
    const certificatesList = this.state.fields.sport && viewer.sports.edges.find(({ node }) => this._translatedName(node.name) === this.state.fields.sport.name)
      ? viewer.sports.edges
        .find(({ node }) => this._translatedName(node.name) === this.state.fields.sport.name).node.certificates
        .map(c => ({ name: this._translatedName(c.name), value: c.id }))
      : [];

    const templateList = viewer.me && viewer.me.sportunityTemplates ? viewer.me.sportunityTemplates.map(template =>  ({name: template.title, value: template})) : [];

    const positionsList = this.state.fields.sport && viewer.sports.edges.find(({ node }) => this._translatedName(node.name) === this.state.fields.sport.name)
      ? viewer.sports.edges
        .find(({ node }) => this._translatedName(node.name) === this.state.fields.sport.name).node.positions
        .map(p => ({ name: this._translatedName(p), value: p.id }))
      : [];

    const sportunityTypesList = this.state.fields.sport && viewer.sports.edges.find(({ node }) => this._translatedName(node.name) === this.state.fields.sport.name)
      ? viewer.sports.edges
        .find(({ node }) => this._translatedName(node.name) === this.state.fields.sport.name).node.sportunityTypes
        .map(p => ({ name: this._translatedName(p.name), value: p.id, isScoreRelevant: p.isScoreRelevant }))
      : [];

    const pendingVariables = this.props.relay.pendingVariables;

    const {finished, selectedTab} = this.state;

    const circlesCurrentUserIsIn = viewer && viewer.me && viewer.me.circlesUserIsIn && viewer.me.circlesUserIsIn.edges && viewer.me.circlesUserIsIn.edges.length > 0
      ? viewer.me.circlesUserIsIn.edges
        .map(edge => {
          if (edge.node.isCircleUsableByMembers)
            return edge
          else
            return false
        })
        .filter(i => Boolean(i))
      : []
    return (
      <div>
        {
          viewer && viewer.me ? <AppHeader user={viewer.me} viewer={viewer} {...this.state} /> : <AppHeader user={null} viewer={viewer} {...this.state} />
        }
        <div style={styles.container} onSubmit={e => {e.preventDefault(); e.stopPropagation();}}>
          <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
          
          {
            viewer.me && this.state.displayCompletePersonProfilePopup &&
            <CompletePersonProfilePopup
              sportunity={this.state.fields}
              onConfirm={this._handleConfirmProfileUpdate}
              onClose={this._handleHideCompleteProfilePopup}
              viewer={this.props.viewer}
              me={this.props.viewer.me}
              processing={this.state.process}
            />
          }
          {
            viewer.me && this.state.displayCompleteBusinessProfilePopup &&
            <CompleteBusinessProfilePopup
              sportunity={this.state.fields}
              onConfirm={this._handleConfirmBusinessProfileUpdate}
              onClose={this._handleHideCompleteBusinessProfilePopup}
              viewer={this.props.viewer}
              me={this.props.viewer.me}
              processing={this.state.process}
            />
          }
          {
            this.state.addBankAccountOpen &&
            <AddBankAccountPopup
              viewer={this.props.viewer}
              me={this.props.viewer.me}
              onClose={this._hideAddBankAccount}
              onConfirm={this._confirmAddBankAccount}
              processing={this.state.process}
            />
          }
          {
            this.state.addCardPopupOpen &&
            <AddACardPopup
              sportunity={this.state.fields}
              onConfirm={this._handleConfirmAddACard}
              onClose={this._handleHideAddCard}
              viewer={this.props.viewer}
              me={this.props.viewer.me}
              processing={this.state.process}
              price={{cents: sportunity.organizerParticipation, currency: this.props.userCurrency}}
            />
          }
          {
            this.state.addACardToPaySecondaryOrganizers && 
            <AddACardPopup
              sportunity={this.state.fields}
              onConfirm={this._handleConfirmAddACard}
              onClose={this._handleHideAddCardToPaySecondaryOrganizers}
              viewer={this.props.viewer}
              me={this.props.viewer.me}
              processing={this.state.process}
              price={
                (this.state.fields.organizers && this.state.fields.organizers.length > 0) 
                ? {cents: this.state.fields.organizers.map(a => a.price.cents).reduce((a,b) => {
                      return a + b
                    }), 
                    currency: this.props.userCurrency}
                : {cents: 0, currency: this.props.userCurrency}
              }
            />
          }
          <div style={styles.modal}>
            <header style={styles.header}>
              <h1 style={styles.title}>
                {/* this.state.isModifying
                  ?
                  localizations.updateSportunity_header
                  :
                  localizations.newSportunity_header */
                }
              </h1>
            </header>
            <ToggleDisplay show={!viewer.me}>
              <label style={{ ...styles.error, marginBottom: 25 }}>
                <span>{localizations.newSportunity_login_needed}{localizations.newSportunity_login_needed_1}
                  <Link style={{ color: colors.error }} to="/login">{localizations.newSportunity_login_link_text}</Link>
                  {localizations.newSportunity_login_needed_2}</span>
              </label>
            </ToggleDisplay>

            <div style = {{width : '100%', margin :'auto'}}>
               <div style={{width : '20%', display : 'inline-block', overflow:'hidden', marginRight:'0%', verticalAlign:'top'}}>
                  <Menu style = {{display:'flex', width: '218px!important'}} >
                     <MenuItem style = {this.state.selectedTab == 1 ? {...styles.menuStyles} : ''} onClick = {this.activateTabPressed.bind(this, 1)} value = '1' primaryText= {localizations.newSportunity_autoParticipateUnswitchACircleModalTitle} />
                     <MenuItem style = {this.state.selectedTab == 2 ? {...styles.menuStyles} : ''} onClick = {this.activateTabPressed.bind(this, 2)} value = '2' primaryText={localizations.newSportunity_sport} />
                     <MenuItem style = {this.state.selectedTab == 3 ? {...styles.menuStyles} : ''} onClick = {this.activateTabPressed.bind(this, 3)} value = '3' primaryText="Location" />
                     <MenuItem style = {this.state.selectedTab == 4 ? {...styles.menuStyles} : ''} onClick = {this.activateTabPressed.bind(this, 4)} value = '4' primaryText={localizations.newSportunity_schedule} />
                     <MenuItem style = {this.state.selectedTab == 5 ? {...styles.menuStyles} : ''} onClick = {this.activateTabPressed.bind(this, 5)} value = '5' primaryText="Participants" />
                     <MenuItem style = {this.state.selectedTab == 6 ? {...styles.menuStyles} : ''} onClick = {this.activateTabPressed.bind(this, 6)} value = '6' primaryText={localizations.newSportunity_confirmation_popup_price} />
                     <MenuItem style = {this.state.selectedTab == 7 ? {...styles.menuStyles} : ''} onClick = {this.activateTabPressed.bind(this, 7)} value = '7' primaryText={localizations.event_secondary_organizer} />
                     <MenuItem style = {this.state.selectedTab == 8 ? {...styles.menuStyles} : ''} onClick = {this.activateTabPressed.bind(this, 8)} value = '8' primaryText="Validation" />
                  </Menu>           
               </div>  
                  
               <div style = {{width : '80%', display : 'inline-block', verticalAlign:'top'}}>
              
                  {this.showSelectedTabContent()}
                
               </div>       
            
            </div>
              {finished && (
                <p style={{margin: '20px 0', textAlign: 'center'}}>
                  <a
                    href="#"
                    onClick={(event) => {
                      event.preventDefault();
                      this.setState({selectedTab: 1, finished: false});
                    }}
                  >
                    Click here
                  </a> to reset.
                </p>
              )}

            <ToggleDisplay show={errors.length > 0 && this.state.submitClicked}>
              <label style={styles.error}>Please fullfil missing fields</label>
            </ToggleDisplay>
          </div>

        </div>
        {viewer.me ? <Footer onUpdateLanguage={this._setLanguage} viewer={viewer} user={viewer.me} /> : <Footer onUpdateLanguage={this._setLanguage} viewer={viewer} user={null} />}
      </div>
    );
  }
}

const dispatchToProps = (dispatch) => ({
})
  
const stateToProps = (state) => ({
    userCurrency: state.globalReducer.userCurrency,
})

let ReduxContainer = connect(
    stateToProps,
    dispatchToProps
)(Radium(NewSportunity));

export default Relay.createContainer(withRouter(ReduxContainer), {
  initialVariables: {
    sportsNb: 10,
    filterName: { name: '', language: 'EN' },
    sportunityId: null,
    superToken: null,
    querySuperMe: false,
    queryDetails: false,
    queryLanguage: 'EN'
  },

  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        languages {
          id
          name
          code
        }
        ${AppHeader.getFragment('viewer')}
        ${Footer.getFragment('viewer')}
        ${AddACardPopup.getFragment('viewer')}
        ${VenueOrAddress.getFragment('viewer')}
        ${Opponent.getFragment('viewer')}
        ${Invited.getFragment('viewer')}
        ${NewSportunityTemplateMutation.getFragment('viewer')},
        ${UpdateSportunityTemplateMutation.getFragment('viewer')}
        ${RemoveSportunityTemplateMutation.getFragment('viewer')},
        ${Organizers.getFragment('viewer')}
        ${ConfirmCreationPopup.getFragment('viewer')}
        ${CircleMutation.getFragment('viewer')}
        id,
        superMe (superToken: $superToken) @include(if:$querySuperMe) {
          id,
          profileType
        }
        me {
          ${AppHeader.getFragment('user')}
          ${Footer.getFragment('user')}
          ${Organizers.getFragment('user')}
          id,
          pseudo,
          firstName,
          lastName,
          nationality
          sportunityTemplates {
            id,
            title
            description
            kind
            privacy_switch_preference {
              privacy_switch_type,
              switch_privacy_x_days_before
            }
            invited {
              user {
                id,
                pseudo
                avatar
              }
              answer
            }
            invited_circles (last: 10) {
              edges {
                node {
                  id,
                  name,
                  members {
                   id
                  }
                  memberStatus {
                    starting_date
                    member {
                      id
                      pseudo
                    }
                    status
                  }
                  owner {
                    id
                    pseudo
                    avatar
                  }
                  type
                  memberCount
                }
              }
            }
            price_for_circle {
              circle {
                id
              }
              price {
                cents,
                currency
              }
              participantByDefault
              excludedParticipantByDefault {
                excludedMembers {
                  id
                  pseudo
                }
              }
            }
            notification_preference {
              notification_type,
              send_notification_x_days_before
            }
            participantRange {
              from
              to
            }
            hide_participant_list
            price {
              currency,
              cents,
            },
            is_repeated_occurence_number
            number_of_occurences
            sport {
              sport {
                id,
                name {
                  EN
                  DE
                  FR
                }
                logo
              }
              positions {
                id
               EN
               FR
               DE
              }
              certificates {
                id,
                name {
                  EN
                  FR
                  DE
                }
              }
              levels {
                id
                EN {
                  name
                  skillLevel
                  description
                }
                FR {
                  name
                  skillLevel
                  description
                }
                DE {
                  name
                  skillLevel
                  description
                }
              }
            }
            ageRestriction {
              from, to
            }
            sexRestriction
            address {
              address
              country
              city
              position {
                lat
                lng
              }
            }
            organizers {
              organizer {
                id
                pseudo
              }
              isAdmin
              role
              price {
                cents,
                currency
              },
              secondaryOrganizerType {
                id
              }
              customSecondaryOrganizerType
            }
            pendingOrganizers { 
              id
              circles (last: 20) {
                edges {
                  node {
                    id, 
                    name,
                    memberCount
                    type
                    members {
                      id
                    }
                    owner {
                      id
                      pseudo
                    }
                  }
                }
              }
              isAdmin
              role
              price {
                cents,
                currency
              },
              secondaryOrganizerType {
                id
                name {
                  FR
                  EN
                  DE
                  ES
                }
              }
              customSecondaryOrganizerType
            }
            sportunityType {
              id,
              isScoreRelevant
              name {
                FR,
                EN
              }
            }
            game_information {
              opponent {
                organizer {
                  id, 
                  pseudo,
                  avatar
                }
                organizerPseudo
                lookingForAnOpponent
                invitedOpponents (last: 5) {
                  edges {
                    node {
                      id
                      name
                      memberCount
                    }
                  }
                }
                unknownOpponent
              }
            }
          }
          address {
            address,
            city,
            country
          }
          fees
          paymentMethods {
            id,
            cardMask
          }
          circles (last:20) @include(if: $queryDetails) {
            edges {
              node {
                ${CircleMutation.getFragment('circle')}
                id,
                name,
                memberCount
                type
                owner {
                  id
                  pseudo
                }
                memberStatus {
                  starting_date
                  member {
                    id
                    pseudo
                  }
                  status
                }
                circlePreferences {
                  isChildrenCircle
                }
                members {
                  id
                  pseudo
                }
                sport {
                  sport {
                    id
                    name {
                      FR
                    }
                  }
                }
              }
            }
          }
          circlesUserIsIn(last: 20) @include(if: $queryDetails){
            edges {
              node {
                id
                isCircleUsableByMembers
                name,
                type
                memberStatus {
                  starting_date
                  member {
                    id
                    pseudo
                  }
                  status
                }
                owner {
                  id
                  pseudo
                  avatar
                }
                members {
                  id
                  pseudo
                }
                memberCount
                sport {
                  sport {
                    id
                    name {
                      FR
                    }
                  }
                }
              }
            }
          }
          circlesFromClub(last: 100) @include(if: $queryDetails){
            edges {
              node {
                id
                name,
                type
                memberStatus {
                  starting_date
                  member {
                    id
                    pseudo
                  }
                  status
                }
                owner {
                  id
                  pseudo
                  avatar
                }
                memberCount
                members {
                  id
                  pseudo
                }
                sport {
                  sport {
                    id
                    name {
                      FR
                    }
                  }
                }
              }
            }
          }
          bankAccount {
            addressLine1,
            addressLine2,
            city,
            postalCode,
            country,
            ownerName,
            IBAN,
            BIC
          }
          profileType,
          isProfileComplete,
          shouldDeclareVAT,
          nationality,
          birthday,
          business {
            businessName,
            businessEmail,
            headquarterAddress {
              address,
              city,
              country
            },
            VATNumber
          }
          areStatisticsActivated
        },
        sports(first: $sportsNb, filter: $filterName, language: $queryLanguage) @include(if: $queryDetails){
          edges {
            node {
              id,
              name {
                EN
                FR
                DE
              },
              logo,
              levels {
                id
                EN {
                  name,
                  skillLevel
                  description
                }
                FR {
                  name,
                  skillLevel
                  description
                },
                DE {
                  name,
                  skillLevel
                  description
                }
              },
              positions {
                id,
                EN,
                FR,
                DE,
              },
              certificates {
                id,
                name {
                  EN,
                  FR,
                  DE
                }
              }
              type
              sportunityTypes {
                id,
                isScoreRelevant
                name {
                  FR,
                  EN
                }
              }
            }
          }
        },
        sportunity(id: $sportunityId) {
          id,
          title
          description
          kind
          privacy_switch_preference {
            privacy_switch_type,
            switch_privacy_x_days_before
          }
          participants {
            id
            avatar
            pseudo
          },
					waiting {
						id,
						pseudo
            avatar
					}
          invited {
            user {
              id,
              pseudo
              avatar
            }
            answer
          }
          survey {
            isSurveyTransformed
            surveyDates {
              beginning_date
              ending_date
            }
          }
          invited_circles (last: 10) {
            edges {
              node {
                id,
                name,
                memberStatus {
                  starting_date
                  member {
                    id
                    pseudo
                  }
                  status
                }
                members {
                  id
                }
                owner {
                  id
                  pseudo
                  avatar
                }
                type
                memberCount
              }
            }
          }
          price_for_circle {
            circle {
              id
            }
            price {
              cents,
              currency
            }
            participantByDefault
            excludedParticipantByDefault {
              excludedMembers {
                id
                pseudo
              }
            }
          }
          notification_preference {
            notification_type,
            send_notification_x_days_before
          }
          participantRange {
            from
            to
          }
          hide_participant_list
          beginning_date
          ending_date
          price {
            currency,
            cents,
          },
          is_repeated_occurence_number
          number_of_occurences
          sport {
            sport {
              id,
              name {
                EN
                DE
                FR
              }
              logo
            }
            positions {
              id
              EN
              FR
              DE
            }
            certificates {
              id,
              name {
                EN
                FR
                DE
              }
            }
            levels {
              id
              EN {
                name
                skillLevel
                description
              }
              FR {
                name
                skillLevel
                description
              }
              DE {
                name
                skillLevel
                description
              }
            }
          }
          ageRestriction {
            from, to
          }
          sexRestriction
          address {
            address
            country
            city
            position {
              lat
              lng
            }
          }
          organizers {
            organizer {
              id
              pseudo
            }
            isAdmin
            role
            price {
              cents,
              currency
            },
            secondaryOrganizerType {
              id
            }
            customSecondaryOrganizerType
          }
          pendingOrganizers { 
            id
            circles (last: 20) {
              edges {
                node {
                  id, 
                  name,
                  memberCount
                  type
                  members {
                    id
                  }
                  owner {
                    id
                    pseudo
                  }
                }
              }
            }
            isAdmin
            role
            price {
              cents,
              currency
            },
            secondaryOrganizerType {
              id
              name {
                FR
                EN
                DE
                ES
              }
            }
            customSecondaryOrganizerType
          }
          venue {
            id
            name
            address {
              address,
              city,
              country
            }
          }
          infrastructure {
            id, 
            name
          }
          slot {
            id, 
            from, 
            end, 
            price {
              cents, 
              currency
            }
          }
          sportunityType {
            id,
            isScoreRelevant
            name {
              FR,
              EN
            }
          }
          game_information {
            opponent {
              organizer {
                id, 
                pseudo,
                avatar
              }
              organizerPseudo
              lookingForAnOpponent
              invitedOpponents (last: 5) {
                edges {
                  node {
                    id
                    name
                    memberCount
                  }
                }
              }
              unknownOpponent
            }
          }
          status
        }
      }
    `,
  },
});


styles = {
  paperStyle: {
    padding: '8px 70px 1px',
    marginTop:'20px',

  },
   hr : {
      marginLeft : -70,
      marginRight : -70,
   },
  container: {
    paddingTop: 40,
    paddingBottom: 63,

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    width: '100%',

    fontFamily: 'Lato',
    backgroundColor:'#F3F3F3',

  },

  modal: {
    position: 'relative',
    width: 1084,
    display: 'flex',
    flexDirection: 'column',
    borderRadius: 5,
    boxShadow: 'box-shadow: 0 0 4px 0 rgba(0,0,0,0.4)',
    backgroundColor: '#F3F3F3',
    overlay: {zIndex: 10},
    '@media (max-width: 1204px)': {
      width: '94%',
    }
  },

  header: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },

  cancel: {
    position: 'absolute',
    top: 40,
    right: 40,
    fontSize: 24,
    lineHeight: 1,
    color: colors.gray,
    backgroundColor: 'transparent',
    border: 'none',

    cursor: 'pointer',
  },

  cancelIcon: {
    marginLeft: 15,
  },

  title: {
    marginBottom: 10,
    color: '#4E4E4E',
    fontFamily: 'Lato',

    fontSize: 24,
    fontWeight: 'bold',
  },

  menuItemSelected : {
   backgroundColor :'#5EA1D9',
  },
  subtitle: {
    marginBottom: 5,
    color: '#646464',
    fontSize: 15,
  },

  subtitleWithoutMargin: {
    color: colors.blue,
    fontSize: 28,
    lineHeight: 1,
    fontWeight: 500,
  },

  content: {
    display: 'flex',
    marginBottom: 25,
    '@media (max-width: 600px)': {
      display: 'block',
      width: '94%',
      margin: '0 auto',
    }
  },

  next_tab_button:{
      color: colors.white,
      backgroundColor: colors.blueLight,
      lineHeight:'0px',
      borderRadius: '0px',
  },

  previous_tab_button:{
    color: colors.blueLight,
    backgroundColor: colors.white,
    lineHeight:'0px',
    borderRadius: '0px',
    marginRight: '20px',
  },

  label: {
    display: 'block',
    color: colors.blueLight,
    fontSize: 16,
    lineHeight: 1,
    marginBottom: 8,
  },

  footer: {
    display: 'flex',
    '@media (max-width: 600px)': {
      flexDirection: 'column',
      width: '94%',
      margin: '0 auto',
    }
  },

  column: {
    paddingRight: 119,
    width: '50%',
    '@media (max-width: 600px)': {
      paddingRight: 0,
      width: '100%',
    },
    '@media (max-width: 480px)': {
      paddingRight: 0,
      width: '100%',
    }
  },

  input: {
    marginBottom: 25,
  },

  select: {
    marginBottom: 4,
    /* overflow : 'scroll',
    height: 200, */
  },

  checkbox: {
    alignSelf: 'center',
    marginBottom: 20,
  },

  sportToolTipIcon: {
    marginLeft: 15,
    fontSize: 22,
    cursor: 'pointer'
  },

  submit: {
    lineHeight:'0px',
    borderRadius: '0px',
  },

  error: {
    /* display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center', */
    display:'block',
    color: colors.error,
    fontSize: 18,
    marginBottom: 10,
  },
  menuStyles: {
      borderLeft : '5px solid #5EA1D9' , 
      backgroundColor: '#FFFFFF', 
      boxShadow: 'box-shadow: inset -7px 0 9px -7px rgba(0,0,0,0.4)',
  },
   section: {
    //   backgroundColor: colors.lightGray,
    padding: '10px 15px 10px 0px',
    marginBottom: 10,
    borderRadius: 5
  },
  sectionTitle: {
    fontFamily: 'Lato',
    fontSize: '18px',
    marginBottom: 15,
    paddingBottom: 5,
    borderBottom: '1px solid ' + colors.darkGray,
    color: colors.darkGray
  },
};
