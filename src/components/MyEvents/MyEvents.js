import React from 'react'
import PureComponent, { pure } from '../common/PureComponent'
import Radium from 'radium'
import Relay from 'react-relay'
import { connect } from 'react-redux'
import ReactLoading from 'react-loading';
import {cloneDeep} from 'lodash'

import Header from '../common/Header/Header.js'
import Footer from '../common/Footer/Footer'
import Loading from '../common/Loading/Loading.js'
import Sidebar from './Sidebar'
import Events from './Events'
import GMap from './Map'
import { bindActionCreators } from 'redux';
import * as types from '../../actions/actionTypes.js';
import localizations from '../Localizations'
import { metrics, colors, fonts } from '../../theme';
import { isEqual} from 'lodash';
import SaveFilterMutation from "./SaveFilterMutation";
import AlertContainer from "react-alert";
import SetDefaultFilterMutation from "./SetDefaultFilterMutation";
import InputCheckbox from "../common/Inputs/InputCheckbox";
import ActionSelect from "./ActionSelect";
import CancelEventMutation from "./Mutation/CancelEventMutation";
import {confirmModal} from "./ConfirmationModal";
import {browserHistory} from "react-router";
import {removeOrganizerModal} from "./RemoveSecondaryOrganizerModal";
import RemoveSecondaryOrganizerMutation from "./Mutation/RemoveSecondaryOrganizerMutation";
import Organizer from "./Organizer";
import AddOrganizerModal from "./AddOrganizerModal";
import FindOrganizerModal from "./FindOrganizerModal";
import {addOrganizerModal} from "./AddSecondaryOrganizerModal";
import AddSecondaryOrganizerMutation from "./Mutation/AddSecondaryOrganizerMutation";
import AddSecondaryOrganizerModal from "./AddSecondaryOrganizerModal";
import ReactTooltip from 'react-tooltip'

let styles;


class MyEvents extends PureComponent {
	constructor(props) {
		super(props)
		this.state = {
			highlightedId: null,
			loading: true,
			queryIsLoading: false,
			loadMoreQueryIsLoading: false,
			language: localizations.getLanguage(),
			isSelecting: false,
			sportunitySelected: [],
			allEventSelected: false,
			pageSize: 15,
			itemCount: 5,
			passedFirst: {
				pageSize: 15,
				itemCount: 5,
			}
		}
		this.alertOptions = {
			offset: 14,
			position: 'top right',
			theme: 'light',
			transition: 'fade',
			time: 0,
		};
	}

	componentDidMount = () => {
		document.title = 'Booked, Organized and past sportunities'
		//setTimeout(() => this.setState({ loading: false }), 1500)
		this.setState({
			loading: false
		})
		
		if (this.props.viewer && this.props.viewer.me && !this.props.viewer.me.basicSavedFiltersCreated) {
			this._createDefaultFilters();
		}
		else if (!this.props.hasFilterChanged && this.props.viewer && this.props.viewer.me && this.props.viewer.me.defaultSavedFilter) {
			let defaultFilter = [];
			defaultFilter.push(this.props.viewer.me.defaultSavedFilter)
			this.props._updateSelectedFilters(defaultFilter)
		}
		else {
			this._setFilter(this.props);
		}
	}

	componentWillMount = () => {
		this.props.relay.forceFetch()
	}

	componentWillReceiveProps = (nextProps) => {
		if (!isEqual(this.props.selectedFilters, nextProps.selectedFilters) && nextProps.selectedFilters.length > 0) {
			let statusFilter = [];
			let userFilter = [];
			let sportunityTypeFilter = [];
			let organizersFilter = [];
			let opponentsFilter = [];
			nextProps.selectedFilters.forEach((filter, index) => {
				if (filter.statuses !== null && filter.statuses !== undefined)
					filter.statuses.forEach((status) => {
						if (statusFilter.indexOf(status) < 0)
							statusFilter.push(status)
					})
				if (filter.subAccounts !== null && filter.subAccounts !== undefined)
					filter.subAccounts.forEach((user) => {
						if (userFilter.indexOf(user.id) < 0)
							userFilter.push(user.id)
					})
				if (filter.users !== null && filter.users !== undefined)
					filter.users.forEach((user) => {
						if (organizersFilter.indexOf(user.id) < 0)
							organizersFilter.push(user.id)
					})
				if (filter.sportunityTypes !== null && filter.sportunityTypes !== undefined)
					filter.sportunityTypes.forEach(type => {
						if (sportunityTypeFilter.indexOf(type.id) < 0)
							sportunityTypeFilter.push(type.id)
					})
			})
			this.props._updateFilter(statusFilter);
			this.props._updateUserFilter(userFilter);
			this.props._updateOrganizersFilter(organizersFilter);
			this.props._updateOpponentsFilter(opponentsFilter);
			this.props._updateSportunityTypeFilter(sportunityTypeFilter);

			setTimeout(() => {
				if (!nextProps.relay.variables.query 
					&& isEqual(this.props.filter, nextProps.filter) 
					&& isEqual(this.props.userFilter, nextProps.userFilter)
					&& isEqual(this.props.sportunityTypeFilter, nextProps.sportunityTypeFilter) 
					&& isEqual(this.props.organizersFilter, nextProps.organizersFilter)
					&& isEqual(this.props.opponentsFilter, nextProps.opponentsFilter))
					this._setFilter(nextProps)
			}, 200)
		}
		else if (!isEqual(this.props.filter, nextProps.filter) 
			|| !isEqual(this.props.userFilter, nextProps.userFilter)
			|| !isEqual(this.props.sportunityTypeFilter, nextProps.sportunityTypeFilter) 
			|| !isEqual(this.props.organizersFilter, nextProps.organizersFilter)
			|| !isEqual(this.props.opponentsFilter, nextProps.opponentsFilter)
			|| !isEqual(this.props.selectedClubs, nextProps.selectedClubs)) {
			if (!this.props.userFilter && nextProps.userFilter.length === 1 && nextProps.userFilter[0] === this.props.viewer.me.id)
			{}
			else 
		  		this._setFilter(nextProps)
		}
	}

	_createDefaultFilters = () => {
		let savedFilters = [];
		if (this.props.viewer.me.savedFilters && this.props.viewer.me.savedFilters.length > 0) {
			this.props.viewer.me.savedFilters
				.forEach(item => savedFilters.push({
					userFilterId: item.id,
					filterName: item.filterName,
					statuses: item.statuses,
					subAccounts: item.subAccounts.map(user => user.id),
					users: item.users.map(user => user.id),
					sportunityTypes: item.sportunityTypes.map(user => user.id),
					page: item.page
				}))
		}

		if (this.props.viewer.me.profileType === 'PERSON') {
			savedFilters.push({
				filterName: localizations.myEvents_defaultFilters_all,
				statuses: ["Organized","Booked","Invited","CoOrganizer","AskedCoOrganizer"],
				subAccounts: [],
				users: [],
				sportunityTypes: [],
				page: 'ORGANIZED'
			});
			savedFilters.push({
				filterName: localizations.myEvents_defaultFilters_organized,
				statuses: ["Organized"],
				subAccounts: [],
				users: [],
				sportunityTypes: [],
				page: 'ORGANIZED'
			});
			if (this.props.viewer.me.subAccounts && this.props.viewer.me.subAccounts.length > 0) {
				savedFilters.push({
					filterName: localizations.myEvents_defaultFilters_children,
					statuses: ["Invited", "Booked"],
					subAccounts: this.props.viewer.me.subAccounts.map(sub => sub.id),
					users: [],
					sportunityTypes: [],
					page: 'ORGANIZED'
				});
			}
		}
		else if (this.props.viewer.me.profileType === 'ORGANIZATION') {
			let sportunityTypeMatch = this.props.viewer.sportunityTypes.find(type => type.name.EN === "Match")
			let sportunityTypeTraining = this.props.viewer.sportunityTypes.find(type => type.name.EN === "Training")
			let {canQuerySportunityTypeFilter} = this.props.viewer.me
			savedFilters.push({
				filterName: localizations.myEvents_defaultFilters_all,
				statuses: ["Organized","Booked","Invited","CoOrganizer","AskedCoOrganizer"],
				subAccounts: [],
				users: [],
				sportunityTypes: [],
				page: 'ORGANIZED'
			});
			savedFilters.push({
				filterName: localizations.myEvents_defaultFilters_organized,
				statuses: ["Organized"],
				subAccounts: [],
				users: [],
				sportunityTypes: [],
				page: 'ORGANIZED'
			});
			if (canQuerySportunityTypeFilter && sportunityTypeMatch)
				savedFilters.push({
					filterName: localizations.myEvents_defaultFilters_competition,
					statuses: ["Organized"],
					subAccounts: [],
					users: [],
					sportunityTypes: [sportunityTypeMatch.id],
					page: 'ORGANIZED'
				});
			if (canQuerySportunityTypeFilter && sportunityTypeTraining)
				savedFilters.push({
					filterName: localizations.myEvents_defaultFilters_training,
					statuses: ["Organized"],
					subAccounts: [],
					users: [],
					sportunityTypes: [sportunityTypeTraining.id],
					page: 'ORGANIZED'
				});
			if (this.props.viewer.me.subAccounts && this.props.viewer.me.subAccounts.length > 0) {
				savedFilters.push({
					filterName: localizations.myEvents_defaultFilters_teams,
					statuses: ["Organized"],
					subAccounts: this.props.viewer.me.subAccounts.map(sub => sub.id),
					users: [],
					sportunityTypes: [],
					page: 'ORGANIZED'
				});
			}
			if (canQuerySportunityTypeFilter && sportunityTypeMatch)
				savedFilters.push({
					filterName: localizations.myEvents_defaultFilters_match,
					statuses: ["Invited"],
					subAccounts: [],
					users: [],
					sportunityTypes: [sportunityTypeMatch.id],
					page: 'ORGANIZED'
				});
		}
		else if (this.props.viewer.me.profileType === 'BUSINESS') {
			savedFilters.push({
				filterName: localizations.myEvents_defaultFilters_all,
				statuses: ["Organized","Booked","Invited","CoOrganizer","AskedCoOrganizer"],
				subAccounts: [],
				users: [],
				sportunityTypes: [],
				page: 'ORGANIZED'
			});
			savedFilters.push({
				filterName: localizations.myEvents_defaultFilters_organized,
				statuses: ["Organized"],
				subAccounts: [],
				users: [],
				sportunityTypes: [],
				page: 'ORGANIZED'
			});
			if (this.props.viewer.me.subAccounts && this.props.viewer.me.subAccounts.length > 0) {
				savedFilters.push({
					filterName: localizations.myEvents_defaultFilters_subAccounts,
					statuses: ["Organized"],
					subAccounts: this.props.viewer.me.subAccounts.map(sub => sub.id),
					users: [],
					sportunityTypes: [],
					page: 'ORGANIZED'
				});
			}
		}

		if (savedFilters.length > 0) {
			this.updateSavedFilter(savedFilters, true, (props) => {
				if (props.viewer.me.savedFilters.length > 0) {
					let defaultFilter = props.viewer.me.savedFilters.find(filter => filter.filterName === localizations.myEvents_defaultFilters_all)
					if (defaultFilter) {
						this.updateDefaultFilter(defaultFilter.id, () => {
							this.props._updateSelectedFilters([defaultFilter])
						})
					}
				}
			});
		}
	}

	_loadMoreQueryIsLoaded = () => {
		if (this.props.relay.variables.first === this.state.itemCount) {
			this.setState({
				loadMoreQueryIsLoading: false,
			})
		} else {
			setTimeout(this._loadMoreQueryIsLoaded, 200)
		}
	}

	_loadMore = () => {
		const nextCount = this.state.itemCount + this.state.pageSize
		this.props.relay.setVariables({ first: nextCount }, this._loadMoreQueryIsLoaded)
		this.setState({
			itemCount: nextCount,
			loadMoreQueryIsLoading: true,
		})
  }

	_loadMoreQueryPassedIsLoaded = () => {
		if (this.props.relay.variables.passedFirst ===
			this.state.passedFirst.itemCount) {
			this.setState({
				loadMoreQueryIsLoading: false,
			})
		} else {
			setTimeout(this._loadMoreQueryPassedIsLoaded, 200)
		}
	}

	_loadMorePassed = () => {
		const nextCount = this.state.passedFirst.itemCount + this.state.passedFirst.pageSize
		this.props.relay.setVariables({ passedFirst: nextCount }, this._loadMoreQueryPassedIsLoaded)
		this.setState({
			passedFirst: {
				...this.state.passedFirst,
				itemCount: nextCount,
			},
			loadMoreQueryIsLoading: true,
    	})
  	}

	fetch = () => {
		this.props.relay.forceFetch();
	}

	_setLanguage = (language) => {
		this.setState({ language: language })
	}

	_onEventHoverHandler = (id) => {
		this.setState({
			highlightedId: id,
		})
	}

	_onEventLeaveHandler = () => {
		this.setState({
			highlightedId: null,
		})
	}

	_setFilter = (props) => {
		this.setState({
			queryIsLoading: true
		})
		this.props._updateHasChangedFilter(true);

		let users = props.organizersFilter;
		if (props.selectedClubs && props.selectedClubs.length > 0)  {
			props.selectedClubs.forEach(selectedClub => users.push(selectedClub.owner.id))
		}

		this.props.relay.setVariables({
			filter: {
				statuses: props.filter,
				subAccounts: props.userFilter,
				sportunityTypes: props.sportunityTypeFilter,
				users: users,
				opponents: props.opponentsFilter
			},
			filterOrganizer: {
				statuses: props.filter,
				subAccounts: props.userFilter,
				sportunityTypes: props.sportunityTypeFilter,
			},
			orderBy: props.filter && (props.filter.length === 1 && 
					(props.filter[0] === 'Past' || props.filter[0] === 'Cancelled') || 
					(props.filter.length === 2 && (props.filter.indexOf('Past') >= 0 && props.filter.indexOf('Cancelled') >= 0)))
				? 'BEGINNING_DATE_DESC' 
				: 'BEGINNING_DATE_ASC' ,
			first: 5,
			query: true
		}, 
			readyState => {
				if (readyState.done) {
						setTimeout(() => {// Needed to wait for Relay to re-fetch data in this.props.viewer
								this.setState({
									queryIsLoading: false,
									loading: false
								})
								this.props.relay.setVariables({queryMain: true})
						}, 50);
				}
			}
		)
	}

	updateSavedFilter = (data, basicSavedFiltersCreated = false, callback) => {
		this.props.relay.commitUpdate(
			new SaveFilterMutation(
				basicSavedFiltersCreated 
				?	{
						viewer: this.props.viewer,
						user: this.props.viewer.me,
						userIDVar: this.props.viewer.me.id,
						savedFiltersVar: data,
						basicSavedFiltersCreatedVar: basicSavedFiltersCreated
					}
				:	{
						viewer: this.props.viewer,
						user: this.props.viewer.me,
						userIDVar: this.props.viewer.me.id,
						savedFiltersVar: data,
					}
				),
			{
				onSuccess: () => {
					if (typeof callback !== "undefined")
						setTimeout(() => callback(this.props), 150);
					else 
						this.msg.show(localizations.popup_findSportunity_filter_success, {
							time: 3000,
							type: 'success',
						});
				},
				onFailure: (error) => {
					console.log(error.getError());
					this.msg.show(error.getError(), {
						time: 3000,
						type: 'error',
					});
				},
			}
		)

		setTimeout(() => {
			this.msg.removeAll();
		}, 3000);
	}

	updateDefaultFilter = (data, callback) => {
		this.props.relay.commitUpdate(
			new SetDefaultFilterMutation({
				viewer: this.props.viewer,
				user: this.props.viewer.me,
				filterIDVar: data
			}),
			{
				onSuccess: () => {
					if (typeof callback !== 'undefined') {
						callback();
					}
					else {
						this.msg.show(localizations.popup_findSportunity_filter_success, {
							time: 3000,
							type: 'success',
						});
					}
				},
				onFailure: (error) => {
					console.log(error.getError());
					this.msg.show(error.getError(), {
						time: 3000,
						type: 'error',
					});
				},
			}
		)

		setTimeout(() => {
			this.msg.removeAll();
		}, 3000);
	}

	_handleSeeMore = (newStatus) => {
		if (newStatus)
			this.props.relay.setVariables({
				organizersNumber: this.props.viewer.sportunitiesOrganizers.count
			})
		else
			this.props.relay.setVariables({
				organizersNumber: 5
			})
	};

	_handleSwitchSelecting = () => {
		this.setState({isSelecting: !this.state.isSelecting})
	}

	_handleSelectEvent = (event) => {
		let tsportunitySelected = cloneDeep(this.state.sportunitySelected)

		let index = tsportunitySelected.findIndex(sportunity => sportunity.id === event.id);
		
		if (index < 0)
			tsportunitySelected.push(event)
		else
			tsportunitySelected.splice(index, 1)

		let sportunitiesList = this.props.viewer.sportunities.edges.filter(sportunity => sportunity.node.organizers.find(organizer => organizer.isAdmin).organizer.id === this.props.viewer.me.id)
		let allEventSelected = true;
		sportunitiesList.forEach(sportunity => {
			if (tsportunitySelected.findIndex(selected => selected.id === sportunity.node.id) < 0)
				allEventSelected = false;
		})

		this.setState({
			sportunitySelected: tsportunitySelected,
			allEventSelected 
		})
	}

	_handleSelectAllEvent = () => {
		
		let sportunitySelected = []

		if (!this.state.allEventSelected)
			this.props.viewer.sportunities.edges.forEach(sportunity => {
				if (sportunity.node.organizers.find(organizer => organizer.isAdmin).organizer.id === this.props.viewer.me.id)
					sportunitySelected.push(sportunity.node)
			})
		this.setState({
			sportunitySelected,
			allEventSelected: !this.state.allEventSelected,
		})
	}

	cancelEvents = () => {
		let {sportunitySelected} = this.state
		confirmModal({
			title: localizations.selectingEvent_confirmModal_title,
			nbSelected: sportunitySelected ? sportunitySelected.length : 0,
			confirmLabel: localizations.selectingEvent_valid,
			cancelLabel: localizations.selectingEvent_cancel,
			canCloseModal: true,
			onConfirm: () => {
				this.props.relay.commitUpdate(
					new CancelEventMutation({
						viewer: this.props.viewer,
						sportunityIDsVar: sportunitySelected ? sportunitySelected.map(sportunity => sportunity.id) : []
					}),
					{
						onSuccess: () => {
							this.msg.show(localizations.popup_newSportunity_update_success, {
								time: 3500,
								type: 'success',
							});
							setTimeout(() => this.msg.removeAll(), 2000);
						},
						onFailure: error => {
							this.msg.show(error.getError().source.errors[0].message, {
								time: 2000,
								type: 'error',
							});
							setTimeout(() => {
								this.msg.removeAll();
							}, 2000);
						}
					}
				)
			}
		})
	}

	removeOrganizer = () => {
		let {sportunitySelected} = this.state;
		let organizers = []
		sportunitySelected.forEach(sportunity => {
			sportunity.organizers.forEach(organizer => {
				if (!organizer.isAdmin && organizers.findIndex(item =>
					item.organizer.id === organizer.organizer.id && (item.secondaryOrganizerType !== null && organizer.secondaryOrganizerType !== null
					? item.secondaryOrganizerType.id === organizer.secondaryOrganizerType.id
					: item.customSecondaryOrganizerType === organizer.customSecondaryOrganizerType)
				) < 0)
					organizers.push(organizer)
			})
		})
		if (organizers.length > 0)
			removeOrganizerModal({
				title: localizations.selectingEvent_action_removeOrganizer_select,
				organizers,
				confirmLabel: localizations.selectingEvent_valid,
				cancelLabel: localizations.selectingEvent_cancel,
				canCloseModal: false,
				onConfirm: (selectedOrganizer) => {
					confirmModal({
						title: localizations.selectingEvent_confirmModal_title,
						nbSelected: sportunitySelected ? sportunitySelected.length : 0,
						confirmLabel: localizations.selectingEvent_valid,
						cancelLabel: localizations.selectingEvent_cancel,
						canCloseModal: true,
						onConfirm: () => {
							this.props.relay.commitUpdate(
								new RemoveSecondaryOrganizerMutation({
									viewer: this.props.viewer,
									sportunityIDsVar: sportunitySelected ? sportunitySelected.map(sportunity => sportunity.id) : [],
									organizerVar: selectedOrganizer,
								}),
								{
									onSuccess: () => {
										this.msg.show(localizations.popup_newSportunity_update_success, {
											time: 3500,
											type: 'success',
										});
										setTimeout(() => this.msg.removeAll(), 2000);
									},
									onFailure: error => {
										this.msg.show(error.getError().source.errors[0].message, {
											time: 2000,
											type: 'error',
										});
										setTimeout(() => {
											this.msg.removeAll();
										}, 2000);
									}
								}
							)
						}
					})
				}
			})
		else {
			this.msg.show('Aucun organisateur secondaire dans la sélection', {
				time: 3000,
				type: 'info',
			});
			setTimeout(() => {
				this.msg.removeAll();
			}, 3000);
		}
	}

	addOrganizer = (selectedOrganizer) => {
		let {sportunitySelected} = this.state;
		confirmModal({
			title: localizations.selectingEvent_confirmModal_title,
			nbSelected: sportunitySelected ? sportunitySelected.length : 0,
			confirmLabel: localizations.selectingEvent_valid,
			cancelLabel: localizations.selectingEvent_cancel,
			canCloseModal: true,
			onConfirm: () => {
				this.props.relay.commitUpdate(
					new AddSecondaryOrganizerMutation({
						viewer: this.props.viewer,
						sportunityIDsVar: sportunitySelected ? sportunitySelected.map(sportunity => sportunity.id) : [],
						organizerVar: selectedOrganizer,
					}),
					{
						onSuccess: () => {
							this.msg.show(localizations.popup_newSportunity_update_success, {
								time: 3500,
								type: 'success',
							});
							setTimeout(() => this.msg.removeAll(), 2000);
						},
						onFailure: error => {
							this.msg.show(error.getError().source.errors[0].message, {
								time: 2000,
								type: 'error',
							});
							setTimeout(() => {
								this.msg.removeAll();
							}, 2000);
						}
					}
				)
			}
		})
	}


	_toggleOpenModal = () => {
		this.setState({openModal: !this.state.openModal})
	}

  render() {
		if (this.state.loading) {
			return(<Loading />)
		}
    	const { viewer } = this.props ;

		let actionList = [
			{name: localizations.selectingEvent_action_cancel, onClickAction: this.cancelEvents, tip: localizations.selectingEvent_action_cancel_tip},
			{name: localizations.selectingEvent_action_removeOrganizer, onClickAction: this.removeOrganizer},
			{name: localizations.selectingEvent_action_addOrganizer, onClickAction: this._toggleOpenModal},
		]
		let {
			isSelecting,
			sportunitySelected,
			allEventSelected,
			openModal
		} = this.state

		return (
			<div style={styles.container}>

				{viewer.me ? <Header user={viewer.me} viewer={viewer} {...this.state}/> : <Header user={null} {...this.state}/>}
				{viewer.me &&
				<div style={styles.content}>
					<ReactTooltip effect="solid" multiline={true}/>
					<AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
					{openModal &&
						<AddSecondaryOrganizerModal
							title={localizations.selectingEvent_action_removeOrganizer_select}
							user={this.props.viewer.me}
							viewer={this.props.viewer}
							sprotunities={sportunitySelected}
							confirmLabel={localizations.selectingEvent_valid}
							cancelLabel={localizations.selectingEvent_cancel}
							canCloseModal={false}
							onConfirm={this.addOrganizer}
							isOpen={openModal}
							onClose={this._toggleOpenModal}
						/>
					}
					<div style={{display: 'flex', flexDirection: 'column', flex: '2 1 0%'}}>
						{!isSelecting
							? <div onClick={this._handleSwitchSelecting} style={{...styles.selectBar, width: 250, cursor: 'pointer'}}>
								{localizations.selectingEvent_showSelecting}
								<i
									className='fa fa-angle-right fa-2x'
								/>
							</div>
							: <div style={styles.selectBar}>
								<ActionSelect
									list={actionList}
									label={localizations.selectingEvent_showSelecting}
									isDisabled={!(sportunitySelected && sportunitySelected.length > 0)}
								/>
								<div>
									{localizations.formatString(localizations.selectingEvent_nbSelected, sportunitySelected ? sportunitySelected.length : 0)}
								</div>
								<div style={{display: 'flex', alignItems: 'center'}}>
									{localizations.selectingEvent_selectAll}
									<InputCheckbox
										checked={allEventSelected}
										onChange={this._handleSelectAllEvent}
									/>
								</div>
								<i
									className='fa fa-angle-left fa-2x'
									style={{cursor: 'pointer'}}
									onClick={this._handleSwitchSelecting}
								/>
							</div>
						}
						<div style={{width: '100%', display: 'flex'}}>
							<Sidebar {...this.state} viewer={viewer} user={viewer.me} selectAllEvent={this._handleSelectAllEvent} updateSavedFilter={this.updateSavedFilter} updateDefaultFilter={this.updateDefaultFilter} onSeeMore={this._handleSeeMore}/>
							{this.state.queryIsLoading &&
							<div style={styles.loadingSpinner}>
								<ReactLoading type='cylon' color={colors.blue} />
							</div>
							}
							{viewer.sportunities && !this.state.queryIsLoading &&
							<Events
								sportunities={viewer.sportunities}
								onHover={this._onEventHoverHandler}
								onLeave={this._onEventLeaveHandler}
								viewer={viewer}
								queryIsLoading={this.state.queryIsLoading}
								onLoadMore={this._loadMore}
								numberOfEvents={this.props.relay.variables.first}
								selectEvent={this._handleSelectEvent}
								{...this.state}
							/>
							}
						</div>
					</div>
					{viewer.sportunities &&
					<GMap
						sportunities={viewer.sportunities}
						highlightedId={this.state.highlightedId}
					/>
					}
				</div>
				}
				{viewer.me ? <Footer onUpdateLanguage={this._setLanguage} viewer={viewer} user={viewer.me}/> : <Footer onUpdateLanguage={this._setLanguage} viewer={viewer} user={null}/> }
			</div>
		);
  }
}

styles = {
	selectBar: {
		margin: 20,
		padding: 10,
		backgroundColor: colors.lightGray,
		display: 'flex',
		flexDirection: 'row',
		fontSize: fonts.size.small,
		alignItems: 'center',
		justifyContent: 'space-between',
		borderRadius: 5
	},
	container: {
		flex: 1,
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'center',
		fontFamily: 'Lato',
		width: '100%',
	},
	h1: {
		fontSize: fonts.size.xxl,
		color: colors.blue,
		fontWeight: fonts.weight.xxl,
	},
	title: {
		display: 'flex',
		alignItems: 'flex-end',
		margin: metrics.margin.xxxl,
	},
	content: {
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'left',
		paddingTop: 15,
		// height: '100%',
		'@media (max-width: 960px)': {
      		display: 'block',
    	}
	},
	loadingSpinner:{
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
};

const _updateFilter = (value) => {
  return {
      type: types.UPDATE_MY_EVENT_FILTER,
      value,
  }
}

const _updateSelectedFilters = value => {
	return {
		type: types.UPDATE_MY_EVENT_SELECTED_FILTERS,
		value
	}
}

const _updateUserFilter = (value) => {
	return {
		type: types.UPDATE_MY_EVENT_USER_FILTER,
		value
	}
}

const _updateSportunityTypeFilter = value => {
	return {
		type: types.UPDATE_MY_EVENT_SPORTUNITY_TYPE_FILTER,
		value
	}
}

const stateToProps = (state) => ({
	filter: state.myEventFilterReducer.filter,
	userFilter: state.myEventFilterReducer.userFilter,
	sportunityTypeFilter: state.myEventFilterReducer.sportunityTypeFilter,
	organizersFilter: state.myEventFilterReducer.organizersFilter,
	opponentsFilter: state.myEventFilterReducer.opponentsFilter,
	selectedFilters: state.myEventFilterReducer.selectedFilters,
	hasFilterChanged: state.myEventFilterReducer.hasFilterChanged,
	selectedClubs: state.myEventFilterReducer.selectedClubs,
});

const _resetAction = () => ({
  type: types.UPDATE_MY_EVENT_RESET_FILTER,
})

const _updateOrganizersFilter = value => {
	return {
		type: types.UPDATE_MY_EVENT_ORGANIZERS_FILTER,
		value
	}
}

const _updateOpponentsFilter = value => {
	return {
	  type: types.UPDATE_MY_EVENT_OPPONENTS_FILTER,
	  value
	}
}

const _updateHasChangedFilter = value => {
	return {
		type: types.UPDATE_MY_EVENT_FILTER_HAS_CHANGED,
		value
	}
}

const dispatchToProps = (dispatch) => ({
	_resetAction: bindActionCreators(_resetAction,dispatch),
	_updateFilter: bindActionCreators(_updateFilter, dispatch),
	_updateUserFilter: bindActionCreators(_updateUserFilter, dispatch),
	_updateSportunityTypeFilter: bindActionCreators(_updateSportunityTypeFilter, dispatch),
	_updateOrganizersFilter: bindActionCreators(_updateOrganizersFilter, dispatch),
	_updateOpponentsFilter: bindActionCreators(_updateOpponentsFilter, dispatch),
	_updateSelectedFilters: bindActionCreators(_updateSelectedFilters, dispatch),
	_updateHasChangedFilter: bindActionCreators(_updateHasChangedFilter, dispatch),
})

const ReduxContainer = connect(
  stateToProps,
	dispatchToProps,
)(Radium(MyEvents));

export default Relay.createContainer(Radium(ReduxContainer), {
	initialVariables: {
    	filter: { status: 'MySportunities' },
		filterOrganizer: { status: 'MySportunities' },
		organizersNumber: 5,
		first: 5,
		orderBy: 'BEGINNING_DATE_ASC',
		query: false,
		queryMain: true
  	},
  fragments: {
    viewer: () => Relay.QL`
      	fragment on Viewer {
			${Header.getFragment('viewer')}
			${Footer.getFragment('viewer')}
			${Events.getFragment('viewer')}
					${CancelEventMutation.getFragment('viewer')}
					${RemoveSecondaryOrganizerMutation.getFragment('viewer')}
        ${AddSecondaryOrganizerModal.getFragment('viewer')}
        ${AddSecondaryOrganizerMutation.getFragment('viewer')}
			id
			sportunityTypes (sportType: COLLECTIVE) @include(if: $queryMain){
				id
				name{FR,EN}
			}
			sportunities(first: $first, filter: $filter, orderBy: $orderBy) @include(if: $query) {
					edges {
						node {
							id
							title
							sport {
								sport {
	                assistantTypes {
                    id,
                    name {
                        FR,
                        EN,
                        DE, 
                        ES
                    }
                  }
                }
							}
							organizers {
								id
                isAdmin
                role
			          secondaryOrganizerType {
                  id
                  name {
			              FR
                    EN
                    DE
			              ES
                  }
                }
                customSecondaryOrganizerType
								organizer {
									id
									pseudo
								}
							}
						}
					}
					${Events.getFragment('sportunities')}
					${GMap.getFragment('sportunities')}
				}
			sportunitiesOrganizers(first: $organizersNumber, filter: $filterOrganizer) {
				count
				edges {
					node {
						id
						pseudo
					}
				}
			}
			myOpponents (last: 20) @include(if: $queryMain){
				count
				edges {
					node {
						id
						pseudo
					}
				}
			}
        	me {
				${SaveFilterMutation.getFragment('user')}
				${SetDefaultFilterMutation.getFragment('user')}
        ${AddSecondaryOrganizerModal.getFragment('user')}
				id
				canQuerySportunityTypeFilter
				basicSavedFiltersCreated
				profileType
				savedFilters {
					id
					page
					filterName
					statuses
					users {
						id
						pseudo
					}
					subAccounts {
						id
						pseudo
					}
					sportunityTypes {
						id
						name {
							FR
							EN
						}
					}
				}
				defaultSavedFilter {
					id
					filterName
					statuses
					users {
						id
						pseudo
					}
					subAccounts {
						id
						pseudo
					}
					sportunityTypes {
						id
						name {
							FR
							EN
						}
					}
				}
				circlesUserIsIn (last:20) {
					edges {
					  node {
						id
						name
						owner {
							id
							pseudo
							avatar
						}
						mode
						isCircleUpdatableByMembers
						isCircleUsableByMembers
						memberCount
					  }
					}
				}
				subAccounts @include(if: $queryMain){
					id,
					pseudo
				}
				${Header.getFragment('user')}
				${Footer.getFragment('user')}
			}
      	}
    `,
  },
});
