import Relay from 'react-relay';

export default class SaveFilterMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      upUser
    }`;
  }

  getVariables() {
    return {
      userID: this.props.userIDVar,
      user: {
        savedFilters: this.props.savedFiltersVar,
        basicSavedFiltersCreated: this.props.basicSavedFiltersCreatedVar
      }
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on upUserPayload {
        clientMutationId,
        user {
          basicSavedFiltersCreated
          savedFilters {
						id
						filterName
						statuses
						page
						subAccounts {
							id
							pseudo
						}
						sportunityTypes {
							id
							name {
								FR
								EN
							}
						}
					}
        }
      }
    `;
  }

  getConfigs() {
    return [{
        type: 'FIELDS_CHANGE',
        fieldIDs: {
            user: this.props.userIDVar,
        },
    }];
  }

  static fragments = {
    user: () => Relay.QL`
      fragment on User {
        id
        basicSavedFiltersCreated
        savedFilters {
						id
						filterName
						statuses
						page
						subAccounts {
							id
							pseudo
						}
						sportunityTypes {
							id
							name {
								FR
								EN
							}
						}
				}
      }
    `,
  };

}
