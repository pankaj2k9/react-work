import Relay from 'react-relay';

export default class AddSecondaryOrganizerMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      addSecondaryOrganizer
    }`;
  }

  getVariables() {
    return {
	    sportunityIDs: this.props.sportunityIDsVar,
	    organizer: {
		    organizer: this.props.organizerVar.organizer,
        isAdmin: false,
		    role: 'COACH',
		    secondaryOrganizerType: this.props.organizerVar.secondaryOrganizerType,
		    customSecondaryOrganizerType: this.props.organizerVar.customSecondaryOrganizerType
      }
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on addSecondaryOrganizerPayload {
        clientMutationId,
        viewer {
          sportunities
        }
      }
    `;
  }

  getConfigs() {
    return [{
        type: 'FIELDS_CHANGE',
        fieldIDs: {
            viewer: this.props.viewer.id,
        },
    }];
  }

  static fragments = {
	  viewer: () => Relay.QL`
      fragment on Viewer {
            id
      }
    `,
  };

}
