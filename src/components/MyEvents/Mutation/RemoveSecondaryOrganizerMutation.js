import Relay from 'react-relay';

export default class RemoveSecondaryOrganizerMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      removeSecondaryOrganizer
    }`;
  }

  getVariables() {
    return {
	    sportunityIDs: this.props.sportunityIDsVar,
	    organizer: {
		    organizer: this.props.organizerVar.organizer.id,
        isAdmin: false,
		    role: this.props.organizerVar.role,
		    secondaryOrganizerType: this.props.organizerVar.secondaryOrganizerType ? this.props.organizerVar.secondaryOrganizerType.id : null,
		    customSecondaryOrganizerType: this.props.organizerVar.customSecondaryOrganizerType
      }
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on removeSecondaryOrganizerPayload {
        clientMutationId,
        viewer {
          sportunities
        }
      }
    `;
  }

  getConfigs() {
    return [{
        type: 'FIELDS_CHANGE',
        fieldIDs: {
            viewer: this.props.viewer.id,
        },
    }];
  }

  static fragments = {
	  viewer: () => Relay.QL`
      fragment on Viewer {
            id
      }
    `,
  };

}
