import Relay from 'react-relay';

export default class CancelEventMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      cancelSportunity
    }`;
  }

  getVariables() {
    return {
	    sportunityIDs: this.props.sportunityIDsVar
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on cancelSportunityPayload {
        clientMutationId,
        viewer {
          sportunities
        }
      }
    `;
  }

  getConfigs() {
    return [{
        type: 'FIELDS_CHANGE',
        fieldIDs: {
            viewer: this.props.viewer.id,
        },
    }];
  }

  static fragments = {
	  viewer: () => Relay.QL`
      fragment on Viewer {
            id
      }
    `,
  };

}
