/* global google */
import React from 'react'
import PureComponent, { pure } from '../common/PureComponent'
import Radium from 'radium'
import Relay from 'react-relay'
import { connect } from 'react-redux'
import {
  GoogleMapLoader,
  GoogleMap,
  Marker,
} from 'react-google-maps'

let styles

class Map extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      sportunities: null,
    }
  }

  _setBound = () => {
		let bounds = new google.maps.LatLngBounds();
    this.props.sportunities.edges.map(edge => {
			const position = new google.maps.LatLng(
				edge.node.address.position.lat,
				edge.node.address.position.lng)
			bounds.extend(position)
			return null
		})

		if(this._map) {
			this._map.fitBounds(bounds);
		}
	}

  _handleMapMounted = (map) => {
   // console.log(map)
    this._map = map;
		this._setBound()
  }

  _handleCenterChanged = () => {
		const nextCenter = this._map.getCenter();
		this.props.onLocationChange(nextCenter.lat(), nextCenter.lng())
  }

  componentDidUpdate() {
		//console.log('componentDidUpdate')
    this._setBound()
	}

  render() {
    const {sportunities} = this.props;

    let lat = sportunities.length > 0 ? sportunities[0].node.address.position.lat || 46.52 : 46.52
    let lng = sportunities.length > 0 ? sportunities[0].node.address.position.lng || 6.6336 : 6.6336

    return (
      <section style={styles.container}>

      <GoogleMapLoader
        containerElement={
					<div
            {...this.props.containerElementProps}
            style={{
              height: 800,
            }}
          />
        }
        googleMapElement={
          <GoogleMap
            ref={this._handleMapMounted}
						defaultZoom={8}
            defaultCenter={{ lat: lat , lng: lng }}
            onClick={this.props.onMapClick}
          >
          {sportunities.edges && sportunities.edges.length > 0 && sportunities.edges.map(edge => {
							return(
              <Marker
                  key={edge.node.id}
                  position={{ lat: edge.node.address.position.lat,
                    lng: edge.node.address.position.lng }}
                  defaultAnimation='2'
									icon={ edge.node.id === this.props.highlightedId ? '/assets/images/marker-active.png' : null }
                />)
							}
            )}

          </GoogleMap>
        }
      />
    </section>
    );
  }
}

styles = {
  container: {
    width: '100%',
    maxWidth: 800,
    flex: 1,
    '@media (max-width: 960px)': {
          maxWidth: 960,
    }
  },
}

const stateToProps = (state) => ({
});

const ReduxContainer = connect(
  stateToProps,
)(Radium(Map));

export default Relay.createContainer(Radium(ReduxContainer), {
  fragments: {
    sportunities: () => Relay.QL`
      fragment on SportunityConnection {
        edges {
          node {
						id
            address {
              position {
                lat
                lng
              }
            }
            beginning_date
          }

        }
      }
    `
  },
});
