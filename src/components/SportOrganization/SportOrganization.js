import React, { Component } from 'react';
import Relay from 'react-relay';
import Header from '../common/Header/Header.js'

import HeaderImage from './HeaderImage';
import Organizers from './Organizers';
import TagBox from './TagBox';
import VideoArea from './VideoArea';

let styles;

class Venues extends Component {
  render() {

    const { viewer } = this.props

    return (
      <div style={styles.container}>
        {
          viewer.me ?
            <Header user={viewer.me} viewer={viewer} {...this.state}/>
          :
            <Header user={null} viewer={viewer} {...this.state}/>
        }
        <HeaderImage />
        <TagBox />
        <Organizers />
        <VideoArea />
      </div>
    );
  }
}

export default Relay.createContainer(Venues, {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        me {
          id          
          numberOfUnreadNotifications
          notifications(last: 5) {
            edges {
              node {
                id
                text
                link
                created
              }
            }
          }
        }
        ${Header.getFragment('viewer')},
      }
    `,
  },
});

styles = {
  container: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
}
