import React, { Component } from 'react';
import Organizer from './Organizer';

import Radium from 'radium';

let styles ;

class Organizers extends Component {
  render () {
    return (
      <div style={styles.container} >
        <h3 style={styles.heading}>Organizers on the application</h3>
        <div style={styles.itemContainer} >
          <Organizer />
          <Organizer />
          <Organizer />
          <Organizer />
        </div>
      </div>
    );
  }
}

styles = {
  container: {
    width: '65%',
    height: '538px',
    position: 'relative',
    margin: '0 auto',
    '@media (max-width: 960px)': {
      width: '94%',
    },
    '@media (max-width: 768px)': {
      height: 'auto',
    }
  },
  heading: {
    width: '100%',
    height: '35px',
    fontFamily: 'Lato',
    fontSize: '29px',
    lineHeight: '35px',
    color: 'rgba(0,0,0,0.65)',
    '@media (max-width: 360px)': {
      fontSize: '22px',
    }
  },
  itemContainer: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
}

export default Radium(Organizers);