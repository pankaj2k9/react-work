import React, { Component } from 'react';
import colors from '../../theme/colors';

import Radium from 'radium';

let styles;

class Organizer extends Component {
  render () {
    return (
      <div style={styles.container} >
        <div style={styles.avatarContainer} >
            {/* <img alt="avatar.png" /> */}
        </div>
        <div style={styles.infoContainer} >
          <span style={styles.name} >Peter Griffins</span>
          <span style={styles.sports} >
            Basketball, Football, Handball
          </span>
          <span style={styles.event} >
            <i style={styles.icon} className="fa fa-calendar-o" aria-hidden="true"></i>
            35 upcoming events
          </span>
        </div>
        <div style={styles.likesContainer} >
          <i style={styles.icon} className="fa fa-heart" aria-hidden="true"></i>
          <span style={styles.likesCount} >5k+</span>
        </div>
      </div>
    );
  }
}

styles = {
  container: {
    width: '48%',
    marginRight: '16px',
    marginTop: '23px',
    height: '213px',
    backgroundColor: '#FFFFFF',
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.12)',
    border: '1px solid #E7E7E7',
    display: 'flex',
    flexDirection: 'row',
    position: 'relative',
    '@media (max-width: 768px)': {
      width: '400px',
      marginRight: '0px',
    },
    '@media (max-width: 360px)': {
      width: '94%',
      marginRight: '0px',
      display: 'block',
    }
  },
  avatarContainer: {
    width: '100px',
    height: '100px',
    border: '1px solid #ddd',
    borderRadius: '100px',
    marginTop: '16%',
    marginLeft: '5%',
    backgroundColor: colors.blue,
    '@media (max-width: 360px)': {
      width: '70px',
      height: '70px',
      marginTop: '5%',
    }
  },
  avatar: {
    // fetch and style avatar
  },
  infoContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignSelf: 'center',
    marginLeft: '4%',
  },
  name: {
    width: '110%',
    height: '24px',
    fontFamily: 'Lato',
    fontSize: '20px',
    fontWeight: 'bold',
    lineHeight: '24px',
    color: 'rgba(0,0,0,0.65)',
  },
  sports: {
    width: '100%',
    marginLeft: '2%',
    marginTop: '5%',
    fontFamily: 'Lato',
    fontSize: '16px',
    fontWeight: '500',
    lineHeight: '19px',
    color: 'rgba(0,0,0,0.65)',
  },
  event: {
    width: '100%',
    marginTop: '5%',
    fontFamily: 'Lato',
    fontSize: '16px',
    lineHeight: '19px',
    fontWeight: '500',
    opacity: '0.8',
  },
  likesContainer: {
      alignSelf: 'flex-end',
      marginRight: '4%',
      marginBottom: '4%',
      fontFamily: 'Lato',
      fontSize: '11px',
      display: 'flex',
      flexDirection: 'row',
      lineHeight: '10px',
      '@media (max-width: 360px)': {
        textAlign: 'right',
        display: 'block',
      }
  },
  likesCount: {
    alignSelf: 'center',
    opacity: '0.7',
  },
  icon: {
      fontSize: '16px',
      color: colors.blue,
      marginRight: '10%',
      '@media (max-width: 360px)': {
        marginRight: '2%',
      }
  },
}

export default Radium(Organizer);

