import React, { Component } from 'react'
import colors from './../../theme/colors'
import { Link } from 'react-router'

import Radium from 'radium';

let styles ;
class HeaderImageButton extends Component {
  render() {
    return (
      <Link to='/new-sportunity' style={styles.linkText}>
        <button style={styles.container}>
          <span>Organize your event</span>
        </button>
      </Link>
    );
  }
}

styles = {
  container: {
    width: '402px',
    margin: '18% auto 0 auto',
    height: '77px',
    border: 'none',
    backgroundColor: colors.green,
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
    borderRadius: '100px',
    display: 'flex',
    fontFamily: 'Lato',
    fontSize: '30px',
    fontWeight: '500',
    justifyContent: 'center',
    letterSpacing: '1px',
    color: colors.white,
    cursor: 'pointer',
    '@media (max-width: 600px)': {
      margin: '150px auto 0 auto',
      width: "350px",
    },
    '@media (max-width: 360px)': {
      width: '100%',
    }
    
  },
  linkText: {
    textDecoration: 'none',
  },
}


export default Radium(HeaderImageButton);