import React, { Component } from 'react';
import HeaderImageButton from './HeaderImageButton';

import Radium from 'radium';

import colors from '../../theme/colors';
let styles ;
class HeaderImage extends Component {
  render() {
    return (
      <div style={styles.container}>
        <p style={styles.headingText}>
          Organize hundreds of events, <br />
          Get paid.
        </p>
        <HeaderImageButton />
      </div>
    );
  }
}


styles = {
  container: {
    width: '100%',
    height: '410px',
    opacity: 0.9,
    backgroundImage: 'url("assets/images/background-landingvenues.jpg")',
    backgroundSize: '100%',
    backgroundRepeat: 'round',
    backgroundColor: '#000000',
    position: 'relative',
  },
  headingText: {
    width: '48%',
    fontFamily: 'Lato',
    fontSize: '44px',
    lineHeight: '52px',
    color: colors.white,
    position: 'absolute',
    top: '15px',
    left: '19%',
    '@media (max-width: 960px)': {
      width: '94%',
      left: '3%',
      fontSize: '34px',
      lineHeight: '40px',
      textAlign: 'center',
    }
 },
}

export default Radium(HeaderImage);
