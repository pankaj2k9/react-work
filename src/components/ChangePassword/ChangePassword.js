import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import Relay from 'react-relay';

import Logo from './Logo.js';
import Submit from './Submit.js';
import Button from './Button.js';
import { colors } from '../../theme'
import Inputs from './Inputs'
import { Link } from 'react-router'
import AlertContainer from 'react-alert'

import localizations from '../Localizations'
import UpdatePasswordMutation from './UpdatePasswordMutation';

let styles;

class ChangePassword extends Component {
  constructor() {
    super();
    this.state = {
      pass1: '',
      pass2: '',
      mutationFailed: false,
    }

    this.alertOptions = {
      offset: 14,
      position: 'top right',
      theme: 'light',
      transition: 'fade',
      time: 0,
    };
  }

  _onChangePass1(pass) {
    this.setState({ pass1: pass });
  }

  _onChangePass2(pass) {
    this.setState({ pass2: pass });
  }

  _handleGoToAskMail() {
    browserHistory.push(`/resetpassword`);
  }

  _handleSubmit() {
    const { tokenId } = this.props.relay.route.params;
    const { pass1, pass2 } = this.state;

    if (pass1 && pass2) {
      if (pass1 != pass2) {
        this.msg.show(localizations.popup_changePassword_pass_differents, {
          time: 0,
          type: 'error',
        });
      }
      else if (pass1.length < 6) {
        this.msg.show(localizations.popup_changePassword_pass_not_long_enough, {
          time: 0,
          type: 'error',
        });
      }
      else {
        this.props.relay.commitUpdate(
          new UpdatePasswordMutation({
            password: this.state.pass1,
            token: tokenId,
            viewer: this.props.viewer,
          }),
          {
            onSuccess: () => {
              this.msg.show(localizations.popup_changePassword_update_success, {
                  time: 2000,
                  type: 'success',
                });
              setTimeout(() => {
                browserHistory.push(`/login`);
              }, 1000);
            },
            onFailure: (error) => {
              this.msg.show(error.getError().source.errors[0].message, {
                time: 0,
                type: 'error',
              });
              this.setState({mutationFailed: true});
            },
          }
        );
      }
    }
    else {
      this.msg.show(localizations.popup_changePassword_please_fill, {
        time: 0,
        type: 'error',
      });
    }
    setTimeout(() => {
      this.msg.removeAll();
    }, 2000);
  }

  render() {
    //const { updateToken } = this.props.route;
    
    return (
      <div style={styles.container}>
        <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
        <div style={styles.signup}>{localizations.login_dontHaveAccount}<Link to='/register' style={styles.link}>{localizations.login_joinUs}</Link>
        </div>
        <div style={styles.modal}>
          <Logo />
          <Inputs 
            pass1={this.state.pass1}
            pass2={this.state.pass2}
            onChangePass1={this._onChangePass1.bind(this)} 
            onChangePass2={this._onChangePass2.bind(this)}
          />
          {this.state.mutationFailed ? <Button onSubmit={this._handleGoToAskMail.bind(this)} />: <Submit onSubmit={this._handleSubmit.bind(this)} />}
        </div>
      </div>
    );
  }
}

export default Relay.createContainer(ChangePassword, {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
        ${UpdatePasswordMutation.getFragment('viewer')}
      }
    `,
  },
});

styles = {
  separator: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 16,
    marginTop: 10,
    marginBottom: 10,
  },
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    paddingTop: 63,
    paddingBottom: 63,


    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    width: '100vw',
    minHeight: '100vh',

    backgroundColor: colors.black,
    fontFamily: 'Lato',

    backgroundImage: 'url(assets/images/background-signup.jpg',

  },
  modal: {
    position: 'relative',
    margin: 'auto',
    width: 480,
    display: 'flex',
    flexDirection: 'column',
    padding: 30,
    borderRadius: 16,
    backgroundColor: 'rgba(255,255,255,0.8)',
  },
  signup: {
    position: 'absolute',
    right: 40,
    top: 30,
    color: colors.gray,
    fontFamily: 'Lato',
    fontSize: 18,
  },
  link: {
    color: colors.gray,
    fontFamily: 'Lato',
    fontSize: 18,
    textTransform: 'none',
  },
}
