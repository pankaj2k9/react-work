import React from 'react'
import Relay from 'react-relay'
import { colors, fonts, metrics } from '../../theme'
import Modal from 'react-modal'
import Radium from 'radium'
import RelayStore from '../../RelayStore.js'
import Link from 'react-router/lib/Link'
import ReactTooltip from 'react-tooltip'
import moment from 'moment'
import ReactLoading from 'react-loading'

import localizations from '../Localizations'
import UpdateMemberStatusMutation from "./MembersInformation/UpdateMemberStatusMutation";

let styles, modalStyles

class MemberRow extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			modalIsOpen: false,
			memberToRemove: null,
			isLoading: false,
		}
	}

	_handleOnRemove(id) {
		this.setState({
			modalIsOpen: true,
			memberToRemove: id,
		})
	}

	_closeModal = () => {
		this.setState({
			modalIsOpen: false,
		})
	}

	_confirmDelete = () => {
		this.setState({isLoading: true})
		this.props.onDeleteMember(this.state.memberToRemove);
		/*const viewer = this.props.viewer
		const userIDVar = this.props.viewer.id
		const idVar = this.props.circleId
		const removedUserIdVar = this.state.memberToRemove

		RelayStore.commitUpdate(
			new DeleteMemberMutation({
				viewer,
				userIDVar,
				idVar,
				removedUserIdVar
			}),
			{
				onFailure: error => {
					this.msg.show(localizations.circle_removeMemberFail, {
						time: 2000,
						type: 'error',
					});
					let errors = JSON.parse(error.getError().source);
					console.log(errors);
					this.setState({isLoading: false})
				},
				onSuccess: (response) => {
					this.msg.show(localizations.circle_removeMemberSuccess, {
						time: 2000,
						type: 'success',
					});
					console.log(response);
					//this.setState({isLoading: false})
					//this._closeModal();
				},
			}
		)*/
	}

	render() {
		const { member, userCanRemoveMember, userFilledInfos, existingAskedInformation, filledInformation } = this.props
		let statusList = [
	      {
	        type: 'ACTIVE',
	        name: localizations.circle_status_active,
	      },
	      {
	        type: 'INJURED',
	        name: localizations.circle_status_injured,
	      },
	      {
	        type: 'INACTIVE',
	        name: localizations.circle_status_inactive,
		  },
		  {
			type: 'OLD',
			name: localizations.circle_status_old,
		  },
	      {
	        type: 'OTHER',
	        name: localizations.circle_status_other,
	      },
	    ];



		return(
				<tr style={styles.row}>
						<td style={styles.memberDetail}>
							<ReactTooltip effect="solid" multiline={true}/>
							{userCanRemoveMember && 
								<input 
									type="checkbox"
									style={styles.checkBox}
									onChange={() => this.props.handleUserClicked(member)}
									checked={this.props.selectedUserList.indexOf(member.id) >= 0}
								/>
							}
                            <Link to={'/profile-view/'+member.id} style={{textDecoration: 'none', display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
                                <div style={styles.icon} >
                                    <div
                                        style={{...styles.iconImage, backgroundImage: member.avatar ? 'url('+ member.avatar +')' : 'url("https://sportunitydiag304.blob.core.windows.net/avatars/default-avatar.png")'}}
                                    />
                                </div>
                                <div style={styles.memberName}>
                                    {member.pseudo}
                                </div>
                            </Link>                            
                        </td>

						{filledInformation && filledInformation.answers && filledInformation.answers.length > 0 &&
							filledInformation.answers.map((answer, index) => (
								<td key={index} style={styles.memberAnswer}>
									<div style={
										answer.askedInfo && (answer.askedInfo.type === 'STATUS' || (answer.askedInfo.askedInfo && answer.askedInfo.askedInfo.type)) 
											?	answer.answer === 'ACTIVE'
											? 	{...styles.answer, color: '#20bf70'} 
											:	answer.answer === 'INJURED' 
												? 	{...styles.answer, color: '#f7ba17'} 
												:	answer.answer === 'INACTIVE' 
													? 	{...styles.answer, color: '#ff0000'} 
													:	styles.answer
											: 	styles.answer}>
										{answer.askedInfo && answer.askedInfo.type === 'BOOLEAN' || (answer.askedInfo.askedInfo && answer.askedInfo.askedInfo.type === 'BOOLEAN')
										?	answer.answer ? localizations.circle_yes : localizations.circle_no
										:	answer.askedInfo && answer.askedInfo.type === 'DATE' || (answer.askedInfo.askedInfo && answer.askedInfo.askedInfo.type === 'DATE')
											?	answer.answer ? moment(new Date(answer.answer)).format('DD/MM/YYYY') : '-'
											:	answer.askedInfo && (answer.askedInfo.type === 'STATUS' || (answer.askedInfo.askedInfo && answer.askedInfo.askedInfo.type === 'STATUS')) && statusList.findIndex((status) => answer.answer === status.type) >= 0
													? 	statusList.find((status) => answer.answer === status.type).name
													: answer.askedInfo && answer.askedInfo.type === 'TERM' || (answer.askedInfo.askedInfo && answer.askedInfo.askedInfo.type === 'TERM')
														? answer.answer ? localizations.circle_termOfUse_isAccepted : localizations.circle_termOfUse_isNotAccepted
														: answer.answer !== undefined ? answer.answer : '-' }
									</div>
								</td>
							))
						}

						{userCanRemoveMember &&
							<td style={styles.remove}  onClick={this._handleOnRemove.bind(this, member.id)}>
								<i className="fa fa-times" style={styles.iconRemove}></i>
								<Modal
									isOpen={this.state.modalIsOpen}
									onRequestClose={this.closeModal}
									style={modalStyles}
									contentLabel={localizations.circle_removeMember}
								>
									<div style={styles.modalContent}>
										<div style={styles.modalHeader}>
											<div style={styles.modalContent}>
												<div style={styles.modalText}>
													{localizations.circle_removeMember} ?
												</div>
												<div style={styles.modalExplanation}>
													{localizations.circle_removeMemberExplanation}
												</div>
												{this.state.isLoading
													?	<div style={styles.modalButtonContainer}><ReactLoading type='cylon' color={colors.blue}/></div>
													:	<div style={styles.modalButtonContainer}>
														<button style={styles.submitButton} onClick={this._confirmDelete}>{localizations.circle_yes}</button>
														<button style={styles.cancelButton} onClick={this._closeModal}>{localizations.circle_no}</button>
													</div>
												}
											</div>
										</div>
									</div>
								</Modal>
							</td>
						}

			</tr>)
	}
}

styles = {
	row: {
        width: 'auto',
        height: 50,
        backgroundColor: colors.white,
        boxShadow: '0 0 4px 0 rgba(0,0,0,0.12)',
        border: '1px solid #E7E7E7',
        overflow: 'hidden',
        fontFamily: 'Lato',
		margin: '1px 0',
		padding: 15,
		textDecoration: 'none',
    },
	middle: {
		flex: 2,
		padding: 5,
		justifyContent: 'space-between',
		alignItems: 'center',
	},
	checkBox: {
		cursor: 'pointer',
		height: 14,
		width: 14,
		userSelect: 'all',
		marginRight: 5
	},
	icon: {
        width: 25,
        height: 25,
        marginRight: 10,
        borderRadius: '50%',
		backgroundColor: colors.white,
		'@media (max-width: 768px)': {
			width: 'auto'
		}
	},
    iconImage: {
		color:colors.white,
		width: 25,
        height: 25,
		borderRadius: '50%',
		backgroundPosition: '50% 50%',
        backgroundSize: 'cover',
		backgroundRepeat: 'no-repeat',
	},
	memberDetail: {
		display: 'flex',
		padding: 5,
		justifyContent: 'flex-start',
		marginRight: 10,
		alignItems: 'center'
	},
	memberAnswer: {
		justifyContent: 'flex-start',
		marginRight: 10,
		padding: 5,
		textAlign: 'center',
	},
	memberName: {
		fontSize: 16,
		lineHeight: '20px',
		color: '#A6A6A6',
		fontWeight: 'bold'
	},
	remove: {
		fontSize: 24,
		cursor: 'pointer',
		textAlign: 'end',
		padding: 5,
	},
	iconRemove: {
		color: '#A6A6A6',
	},
	modalContent: {
		justifyContent: 'flex-start',
		width: 300,
		padding: 10,
		'@media (max-width: 768px)': {
			width: 'auto'
		}
	},
	modalHeader: {
		display: 'flex',
		flexDirection: 'row',
        alignItems: 'flex-center',
		justifyContent: 'flex-center',
	},
	modalTitle: {
		fontFamily: 'Lato',
		fontSize:24,
		fontWeight: fonts.weight.medium,
		color: colors.blue,
		marginBottom: 20,
	},
	modalClose: {
		justifyContent: 'flex-center',
		paddingTop: 10,
		color: colors.gray,
		cursor: 'pointer',
	},
	modalText: {
		fontSize: 18,
		justifyContent: 'center',
		width: '100%',
		fontFamily: 'Lato',
	},
	modalExplanation: {
		fontSize: 16,
		color: colors.gray,
		justifyContent: 'center',
		width: '100%',
		fontFamily: 'Lato',
		fontStyle: 'italic',
		marginTop: 10,
		textAlign: 'justify'
	},
	modalButtonContainer: {
		fontSize: 18,
		justifyContent: 'center',
		width: '100%',
		fontFamily: 'Lato',
		marginTop: 30,
	},
	submitButton: {
		width: 80,
		backgroundColor: colors.blue,
        color: colors.white,
        fontSize: fonts.size.small,
        borderRadius: metrics.radius.tiny,
        outline: 'none',
		border: 'none',
		padding: '10px',
		cursor: 'pointer',
		boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
		margin: 10,
	},
  	cancelButton: {
		width: 80,
		backgroundColor: colors.gray,
        color: colors.white,
        fontSize: fonts.size.small,
        borderRadius: metrics.radius.tiny,
        outline: 'none',
		border: 'none',
		padding: '10px',
		cursor: 'pointer',
		boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
		margin: 10,
	},
	answer: {
		fontSize: 16,
		lineHeight: '20px',
		color: 'rgba(0,0,0,0.65)',
	}
}


modalStyles = {
  overlay : {
    position          : 'fixed',
    top               : 0,
    left              : 0,
    right             : 0,
    bottom            : 0,
    backgroundColor   : 'rgba(255, 255, 255, 0.75)',
  },
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)',
    border                     : '1px solid #ccc',
    background                 : '#fff',
    overflow                   : 'auto',
    WebkitOverflowScrolling    : 'touch',
    borderRadius               : '4px',
    outline                    : 'none',
    padding                    : '20px',
  },
}

export default Relay.createContainer(Radium(MemberRow), {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
				id
				${UpdateMemberStatusMutation.getFragment('viewer')}
			}
		`
	}
})
