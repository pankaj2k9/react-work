import Relay from 'react-relay';

export default class CircleMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      deleteCircle
    }`
  }
  
  getVariables() {
    return  {
      circleId: this.props.idVar,
     // email: this.props.nameVar,
    }
  }

  getFatQuery() {
      return Relay.QL`
        fragment on deleteCirclePayload {
          clientMutationId,
          viewer {
            id,
            me {
              circles
            }
            circle {
              members
            }
          }
        }
      `
   
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
        fieldIDs: {
          viewer: this.props.viewer.id,
        },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        me {
          id
        }
        
      }
    `,
  };

}
