import React from 'react'
import Relay from 'react-relay'
import { Link } from 'react-router'
import Radium from 'radium';
import platform from 'platform';
import { browserHistory } from 'react-router';
import withRouter from 'react-router/lib/withRouter'
import ReactLoading from 'react-loading';
import { isEqual} from 'lodash';
import ReactTooltip from 'react-tooltip'
import Helmet from 'react-helmet';

import AlertContainer from 'react-alert';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as types from '../../actions/actionTypes.js';

import ConfirmationModal, {confirmModal} from '../common/ConfirmationModal'
import {displayChooseAccountModal} from '../common/SwitchAccountOnPrivate';
import {displayChooseSubAccountModal} from '../common/SubAccountsListModal';
import { colors, fonts } from '../../theme'
import { appUrl } from '../../../constants.json';
import RelayStore from '../../RelayStore.js'

import Sharing from './Sharing'
import Wrapper from './Wrapper';
import Header from './Header'
import TabHeader from './TabHeader';
import Details from './Tabs/Details';
import Activities from './Tabs/Activities'
import Members from './Tabs/Members'
import Chat from './Tabs/Chat'
import MembersInformation from './MembersInformation/'
import Subscribe from './Subscribe';
import AddMember from './AddMember'
import AddMyChild from './AddMyChild';
import MemberCard from './MemberCard'
import MemberRow from './MemberRow';
import EditCircle from './EditCircle'
import CircleMutation from './CircleMutation'
import AddMembersMutation from './AddMembersMutation';
import AddChildMutation from './AddChildMutation';
import RelaunchMembersMutation from './MembersInformation/RelaunchMembersMutation.js';
import DeleteMemberMutation from './DeleteMemberMutation'
import localizations from '../Localizations'
import {transferModal} from './TransferToAnotherCircle';
import CircleStats from './CircleStats'
import TermOfUseModal from "./TermOfUseModal";

var Style = Radium.Style;

let styles

class Circle extends React.Component {
  constructor(props) {
    super(props)

    this.alertOptions = {
      offset: 14,
      position: 'top right',
      theme: 'light',
      transition: 'fade',
      time: 0,
    };

    this.state = {
      loading: true,
      isError: false,
      user: null,
      parent1: '',
      parent2: '',
      language: localizations.getLanguage(),
      displayPrivateCirclePopup: false,
      isQueryingForPrivateCircle: false,
      isCurrentUserAMember: false,
      isCurrentUserTheOwner: false,
      isCurrentUserCoOwner: false,
      isCurrentUserAParent: false,
      isCurrentUserAChild: false,
      displayAndroidOpenApp: false,
      displayDetailledMemberInformation: false,
      activeTab: 'details',
      activeSection: 'members',
      rowMembers: true,
      columns: [],
      rows: [],
      isRelaunchButtonVisible: false,
      selectedUserList: [],
      showParents: false,
      isTermOfUseCheck: true,
	    isCurrentUserWantToBeMember: false,
    }
  }

  _setLanguage = (language) => {
    this.setState({ language: language })
  }

  componentDidMount = () => {
    if (this.props.viewer.me && this.props.viewer.me.id !== this.props.circle.owner.id) {
      this.setState({rowMembers: false, queryCircle: true})
    }
    if (this.props.createProfileFrom) {
      this.props._updateRegisterFromAction(null);
      let superToken = localStorage.getItem('superToken');
      let userToken = localStorage.getItem('userToken');

      this.props.relay.setVariables({
        queryAuthorizedAccounts: userToken ? true : false,
        querySuperMe: superToken ? true : false,
        superToken:superToken,
        userToken:userToken,
        queryCircle: true
      },readyState => {
        if (readyState.done) {
          setTimeout(() => {
            this.props.relay.forceFetch();
            this.setState({queryCircle: false})
            }
            , 50);
          }
        });

      this._checkIfUserIsAuthorized(this.props);
      setTimeout(() => this.setState({ loading: false }), 200)
    }
    else {
      let superToken = localStorage.getItem('superToken');
      let userToken = localStorage.getItem('userToken');

      this.props.relay.setVariables({
        queryAuthorizedAccounts: userToken ? true : false,
        querySuperMe: superToken ? true : false,
        superToken:superToken,
        userToken:userToken,
        queryCircle: true
      },readyState => {
        if (readyState.done) {
          setTimeout(() => {
              this.setState({queryCircle: false})
            }
            , 50);
          }
        })
      this._checkIfUserIsAuthorized(this.props);
      setTimeout(() => this.setState({ loading: false }), 200)
    }

    this.setState({
	    rowMembers: this.props.circle.owner.profileType !== 'PERSON',
    })

    setTimeout(() => {
      if (this.props.location.pathname.indexOf('join-circle') >= 0 && !this.state.isCurrentUserAMember) {
        this._handleSubscribe()
      }
      if (this.props.location.hash === '#chat') {
        this.setState({
          activeTab: 'chat',
        })
      }
    }, 50)
  }

  componentWillReceiveProps = (nextProps) => {
    this._checkIfUserIsAuthorized(nextProps)


    if (nextProps.activeTab) {
      let tabList = ['profile', 'event', 'chat', 'statistics'];
      if (tabList.findIndex(tab => tab === this.props.activeTab) >= 0)
        this.setState({
          activeTab: nextProps.activeTab
        });
    }

    if (this.state.rowMembers && (!isEqual(nextProps.circle, this.props.circle) || this.state.columns.length === 0)) {
      let columns = [];
      columns.push({type:'STATUS', name: 'Status', id: 'SERKET', filledByOwner: true});
	    nextProps.circle.termsOfUses.forEach(term => {
		    columns.push({type: 'TERM', name: term.name, id: term.id, acceptedBy: term.acceptedBy})
	    });
      nextProps.circle.askedInformation.map(askedInfo => {
        columns.push(askedInfo);
      });
      let rows = [];
	    let statusList = [];

	    nextProps.circle.memberStatus.forEach(status => {
		    let index = statusList.findIndex(tmpStatus => tmpStatus.member.id === status.member.id)
		    if (index < 0) {
			    statusList.push(status)
		    }
		    else if (statusList[index].starting_date < status.starting_date)
			    statusList[index] = status
	    });
	    nextProps.circle.members.map(member => {
		    let answers = [];
		    statusList.map(status => {
			    if (status.member.id === member.id) {
				    answers.push({askedInfo: columns[0], answer: status.status})
			    }
		    });
        if (answers.length < 1)
          answers.push({askedInfo: columns[0], answer: 'ACTIVE'});
	      columns.forEach(col => {
	      	if (col.type === 'TERM')
			      answers.push({askedInfo : col, answer: (col.acceptedBy.findIndex(accept => accept.user.id === member.id) >= 0)});
	      });
        nextProps.circle.askedInformation.map(askedInfo => {
          let answer ;
          nextProps.circle.membersInformation.map(info => {
            if (info.user.id === member.id && askedInfo.id === info.information) {
              if (askedInfo.type === 'NUMBER')
                answer = parseInt(info.value);
              else if (askedInfo.type === 'BOOLEAN')
                answer = info.value === 'false' ? false : true ;
              else
                answer = info.value;
            }
          })
          answers.push({askedInfo, answer})
        })
        rows.push({user: member, answers})
      })

      this.setState({rows, columns})
    }

    if (this.props.subAccountCreation && this.props.viewer.superMe && this.props.viewer.superMe.subAccounts && this.props.viewer.superMe.subAccounts.length < nextProps.viewer.superMe.subAccounts.length) {
      this.props._updateSubAccountCreation(false);
      this._handleOpenSubAccountList(nextProps)
    }
  }

  _checkIfUserIsAuthorized = (props) => {
    if (props.circle && props.circle.members && !this.state.displayPrivateCirclePopup) {
      const { circle, viewer:{me} } = props;
      let isCurrentUserTheOwner = !!me && circle.owner.id === me.id;
      let isCurrentUserCoOwner = !!me && circle.coOwners.findIndex(coOwner => coOwner.id === me.id) >= 0;
      let isCurrentUserAMember = !!me && circle.members && circle.members.length > 0 && circle.members.findIndex(member => member.id === me.id) >= 0;
      let isCurrentUserAddedAsParent = !!me && circle.memberParents && circle.memberParents.length > 0 && circle.memberParents.findIndex(parent => parent.id === me.id) >= 0;
      let isCurrentUserAParent = !!me && !me.isSubAccount && me.profileType === 'PERSON';
      let isCurrentUserAChild = !!me && me.isSubAccount && me.profileType === 'PERSON';

      this.setState({
        isCurrentUserTheOwner,
        isCurrentUserAMember,
        isCurrentUserCoOwner,
        isCurrentUserAParent,
        isCurrentUserAChild,
        isRelaunchButtonVisible: isCurrentUserTheOwner
      })
      if (!circle.isCircleAccessibleFromUrl && ((!isCurrentUserTheOwner && !isCurrentUserCoOwner && !isCurrentUserAddedAsParent && !isCurrentUserAMember) || !me)) {
        if (!me) {
          this.setState({
            displayPrivateCirclePopup: true
          })
          confirmModal({
            title: localizations.circle_alert_circle_is_private_title,
            message: localizations.circle_alert_circle_is_private_message,
            confirmLabel: localizations.circle_alert_circle_is_private_ok,
            canCloseModal: false,
            onConfirm: () => {
              browserHistory.push({
                pathname : this.props.viewer.me ? 'my-circles' : '/find-sportunity'
              })
            }
          })
        }
        else if (!this.state.isQueryingForPrivateCircle) {
          this.setState({
            isQueryingForPrivateCircle: true
          })
          this.waitForDataSuperMe();
        }
      }
      else {
        if (platform.name.indexOf("Firefox") < 0) {
          if (platform.os.family === "iOS") {
            if (typeof window !== 'undefined')
              window.location.href = "sportunity://circle/"+circle.id;
          }
          else if (platform.os.family === "Android") {
            this.setState({displayAndroidOpenApp: true})
          }
        }
      }

      if ((isCurrentUserAMember || this.state.isCurrentUserWantToBeMember) && circle.termsOfUses){
        let isTermOfUseCheck = true;
        circle.termsOfUses.forEach(term => {
          let acceptedTerm = false;
          term.acceptedBy.forEach(accepted => {
            if (accepted.user.id === me.id)
              acceptedTerm = true
          });
          isTermOfUseCheck = acceptedTerm && isTermOfUseCheck;
        })
        this.setState({isTermOfUseCheck})
      }
    }
  }

  toggleTermModal = () => {
  	if (!this.state.isCurrentUserAMember)
  		this._handleSubscribe();
	  else
		  this.setState({isTermOfUseCheck: true})
  }

	_handleWantToBe = () => {
    const {circle} = this.props;
		if (circle.termsOfUses && circle.termsOfUses.length > 0) {
			this.setState({
				isCurrentUserWantToBeMember: true
			});
		}
    else
      this._handleSubscribe()
  }

  waitForDataSuperMe = () => {
    if (this.props.viewer && this.props.viewer.superMe && this.props.viewer.superMe.id && this.props.viewer.authorizedAccounts && this.props.viewer.authorizedAccounts.id) {
      let accounts = []

      if (this.props.viewer.superMe.subAccounts && this.props.viewer.superMe.subAccounts.length > 0)
        this.props.viewer.superMe.subAccounts.forEach(subAccount => {
          accounts.push(subAccount)
        })

      if (this.props.viewer.authorizedAccounts.accounts && this.props.viewer.authorizedAccounts.accounts.length > 0)
        this.props.viewer.authorizedAccounts.accounts.forEach(account => {
          if (accounts.findIndex(item => item.id === account.id) < 0)
            accounts.push(account)
        })

      if (accounts.findIndex(item => item.id === this.props.viewer.superMe.id) < 0)
        accounts.push({
          id: this.props.viewer.superMe.id,
          pseudo: this.props.viewer.superMe.pseudo,
          avatar: this.props.viewer.superMe.avatar,
          token:  localStorage.getItem('superToken')
        })

      if (accounts.findIndex(item => item.id === this.props.viewer.authorizedAccounts.id) < 0)
        accounts.push({
          id: this.props.viewer.authorizedAccounts.id,
          pseudo: this.props.viewer.authorizedAccounts.pseudo,
          avatar: this.props.viewer.authorizedAccounts.avatar,
          token:  localStorage.getItem('userToken')
        })

      if (accounts.length > 1) {
        this.setState({
          displayPrivateCirclePopup: true
        })
        displayChooseAccountModal({
          title: localizations.circle_alert_circle_is_private_title,
          message: localizations.circle_alert_circle_is_private_message_switch,
          cancelLabel: localizations.circle_alert_circle_is_private_message_switch_cancel,
          accounts,
          canCloseModal: false,
          onClose: () => {
            browserHistory.push({
              pathname : this.props.viewer.me ? '/my-circles' : '/find-sportunity'
            })
          }
        })
      }
      else {
        this.setState({
          displayPrivateSportunityPopup: true
        })
        confirmModal({
          title: localizations.circle_alert_circle_is_private_title,
          message: localizations.circle_alert_circle_is_private_message,
          confirmLabel: localizations.circle_alert_circle_is_private_ok,
          canCloseModal: false,
          onConfirm: () => {
            browserHistory.push({
              pathname : this.props.viewer.me ? '/my-circles' : '/find-sportunity'
            })
          }
        })
      }
    }
    else {
      setTimeout(() => this.waitForDataSuperMe(), 100)
    }
  }

  unSubscribe = () => {
    let callback = () => {
      setTimeout(() => {
        this.props.relay.forceFetch();
        this.setState({isCurrentUserAMember: false})
      }, 200)
    }
    this.props.unSubscribe(this.props.circle, callback)
  }

  _setIsError = (value) => {
    this.setState({
      isError: value,
    })
  }

  _handleChange = (user) => {
    if (!this.state.user || (this.state.user && user !== this.state.user)) {
      this.setState({
        user: user,
      })
    }
  }

  _handleParent1Change = (parent) => {
    if (!this.state.parent1 || (this.state.parent1 && parent !== this.state.parent1)) {
      this.setState({
        parent1: parent,
      })
    }
  }

  _handleParent2Change = (parent) => {
    if (!this.state.parent2 || (this.state.parent2 && parent !== this.state.parent2)) {
      this.setState({
        parent2: parent,
      })
    }
  }

  _handleSubscribe = () => {
    if (this.props.viewer.me) {
      RelayStore.commitUpdate(
        new CircleMutation({
          viewer: this.props.viewer,
          idVar: this.props.circle.id,
          newUserIdVar: this.props.viewer.me.id,
          circle: this.props.circle
        }),
        {
          onFailure: error => {
            this.msg.show(error.getError().source.errors[0].message, {
              time: 2000,
              type: 'error',
            });
            this.setState({isLoading: false})
	          setTimeout(() => {
		          this.msg.removeAll();
	          }, 2000);
          },
          onSuccess: (response) => {
            console.log(response);
            this.msg.show(localizations.circle_subscribe_success, {
              time: 2000,
              type: 'success',
            });
	          setTimeout(() => {
		          this.msg.removeAll();
	          }, 2000);
            setTimeout(() => {
              this.setState({
                isLoading: false,
                isTermOfUseCheck: true,
	              isCurrentUserWantToBeMember: false
              })
            }, 1500);
          },
        }
      )
    }
    else {
      this.msg.show(localizations.circle_subscribe_login, {
        time: 2000,
        type: 'info',
      });
	    setTimeout(() => {
		    this.msg.removeAll();
	    }, 2000);
    }
  }

  _handleOpenSubAccountList = (props) => {
    let accounts = []

    if (props.viewer.superMe.subAccounts && props.viewer.superMe.subAccounts.length > 0)
      props.viewer.superMe.subAccounts.forEach(subAccount => {
        accounts.push(subAccount)
      })

    displayChooseSubAccountModal({
      title: localizations.circle_addMyChild,
      message: localizations.circle_addMyChild_text,
      cancelLabel: localizations.circle_alert_circle_is_private_message_switch_cancel,
      accounts,
      showNewAccountButton: true,
      canCloseModal: true,
      onSelect: (e) => {
        RelayStore.commitUpdate(
          new CircleMutation({
            viewer: this.props.viewer,
            idVar: this.props.circle.id,
            newUserIdVar: e.id,
            circle: this.props.circle
          }),
          {
            onFailure: error => {
              this.msg.show(error.getError().source.errors[0].message, {
                time: 2000,
                type: 'error',
              });
              this.setState({isLoading: false})
	            setTimeout(() => {
		            this.msg.removeAll();
	            }, 2000);
            },
            onSuccess: (response) => {
              console.log(response);
              this.msg.show(localizations.popup_editCircle_update_success, {
                time: 2000,
                type: 'success',
              });
	            setTimeout(() => {
		            this.msg.removeAll();
	            }, 2000);
              setTimeout(() => {
                this.setState({isLoading: false})
              }, 1500);
            },
          }
        )
      },
      onSelectNew: () => {
        this.props._updateRegisterFromAction('/circle/'+this.props.circle.id)
        this.props._updateSubAccountCreation(true);
        browserHistory.push({
          pathname : '/register'
        })
      },
      onClose: () => {}
    })
  }

  _resetMemberName = () => {
    this.setState({
      user: null,
      parent1: null,
      parent2: null
    })
  }

  openAndroidApp = () => {
    document.location="sportunity://circle/"+this.props.circle.id;
  }

  switchToTab = (tab) => this.setState({activeTab: tab})

  _changeSection = (name) => {
    this.setState({
      activeSection: name,
    })
  }

  isUserAsked = (user, question, responses) => {

    let result = responses
      .filter((response) => response.user.id === user.id)
      .find((response) => question.id === response.information)

    return result
  }

  isUserFilledInformation = (user, circle) =>
    circle.askedInformation
      .filter(askedInfo => !askedInfo.filledByOwner)
      .reduce(
        (mem, question) => mem && !!this.isUserAsked(user, question, circle.membersInformation),
        true
      )

  isUserMemberOfMergedCircle = (user, circle) => {
    if (circle.subCircles && circle.subCircles.edges && circle.subCircles.edges.length > 0) {
      let result = false;
      circle.subCircles.edges.forEach(edge => {
        if (edge.node.members && edge.node.members.length > 0 && edge.node.members.map(member => member.id).indexOf(user.id) >= 0)
          result = true;
      })
      return result
    }
    else
      return false
  }

  _displayRowsOrCards = () => {
    if (!this.state.rowMembers && (this.state.columns.length === 0 || this.state.rows.length === 0)) {
      let columns = [];
      columns.push({type:'STATUS', name: 'Status', id: 'SERKET', filledByOwner: true});
	    this.props.circle.termsOfUses.forEach(term => {
		    columns.push({type: 'TERM', name: term.name, id: term.id, acceptedBy: term.acceptedBy})
	    });
      this.props.circle.askedInformation.map(askedInfo => {
        columns.push(askedInfo);
      });

      let rows = [];
	    let statusList = [];

	    this.props.circle.memberStatus.forEach(status => {
		    let index = statusList.findIndex(tmpStatus => tmpStatus.member.id === status.member.id)
		    if (index < 0) {
			    statusList.push(status)
		    }
		    else if (statusList[index].starting_date < status.starting_date)
			    statusList[index] = status
	    });
	    this.props.circle.members.map(member => {
		    let answers = [];
		    statusList.map(status => {
			    if (status.member.id === member.id) {
				    answers.push({askedInfo: columns[0], answer: status.status})
			    }
		    });
        if (answers.length < 1)
          answers.push({askedInfo: columns[0], answer: 'ACTIVE'});
	      columns.forEach(col => {
		      if (col.type === 'TERM')
			      answers.push({askedInfo : col, answer: (col.acceptedBy.findIndex(accept => accept.user.id === member.id) >= 0)});
	      });
        this.props.circle.askedInformation.map(askedInfo => {
          let answer ;
          this.props.circle.membersInformation.map(info => {
            if (info.user.id === member.id && askedInfo.id === info.information) {
              if (askedInfo.type === 'NUMBER')
                answer = parseInt(info.value);
              else if (askedInfo.type === 'BOOLEAN')
                answer = info.value === 'false' ? false : true ;
              else
                answer = info.value;
            }
          })
          answers.push({askedInfo, answer})
        })
        rows.push({user: member, answers})
      })

      this.setState({rows, columns})
    }

    this.setState({rowMembers: !this.state.rowMembers})
  }

  _relaunchMembers = () => {
    const idVar = this.props.circle.id;
    const viewer = this.props.viewer ;

    RelayStore.commitUpdate(
      new RelaunchMembersMutation({
        viewer,
        idVar,
      }),
      {
        onFailure: error => {
          this.msg.show(localizations.popup_editCircle_update_failed, {
            time: 2000,
            type: 'error',
          });
          let errors = JSON.parse(error.getError().source);
          console.log(errors);
	        setTimeout(() => {
		        this.msg.removeAll();
	        }, 2000);
        },
        onSuccess: (response) => {
          this.msg.show(localizations.popup_editCircle_update_success, {
            time: 2000,
            type: 'success',
          });
	        setTimeout(() => {
		        this.msg.removeAll();
	        }, 2000);
          this.setState({isRelaunchButtonVisible: false})
        },
      }
    )
  }

  getType = (type, isSubAccount) => {
    if (type === null)
      return '';
    let list = [
      [localizations.circle_adult, localizations.circle_child],
      [localizations.circle_club, localizations.circle_teams]
    ];
    if (type !== 'PERSON' && type !== 'ORGANIZATION' && type !== 'ADULTS' && type !== 'CLUBS')
      return type.toLowerCase();
    let categorie = (type === 'ADULTS' || type === 'PERSON') ? 0 : 1;
    let index = isSubAccount ? 1 : 0;
    return list[categorie][index];
  };

  _deleteMember = (member, hideConfirmationPopup, shouldFetch) => {
    this.setState({isLoading: true})
    const viewer = this.props.viewer
    const userIDVar = this.props.viewer.id
    const idVar = this.props.circle.id
    const removedUserIdVar = member

    RelayStore.commitUpdate(
      new DeleteMemberMutation({
        viewer,
        userIDVar,
        idVar,
        removedUserIdVar,
        circle: this.props.circle
      }),
      {
        onFailure: error => {
          this.msg.show(localizations.circle_removeMemberFail, {
            time: 2000,
            type: 'error',
          });
	        setTimeout(() => {
		        this.msg.removeAll();
	        }, 2000);
          this.setState({isLoading: false})
          let errors = JSON.parse(error.getError().source);
          console.log(errors);
        },
        onSuccess: (response) => {
          if (!hideConfirmationPopup) {
	          this.msg.show(localizations.circle_removeMemberSuccess, {
		          time: 2000,
		          type: 'success',
	          });
	          setTimeout(() => {
		          this.msg.removeAll();
	          }, 2000);
          }
          if (shouldFetch)
            this.props.relay.forceFetch()
          //this.setState({isLoading: false})
        },
      }
    )
  }

  _handleUserClicked = (member) => {
    let userList = this.state.selectedUserList;
    let index = userList.indexOf(member.id);
    if (index >= 0)
      userList.splice(index, 1);
    else
      userList.push(member.id);

    this.setState({
      selectedUserList: userList
    })
  }

  _handleTransfer = () => {
    transferModal({
      canCloseModal: true,
      title: localizations.circle_transferMembers_validation,
      message: localizations.circle_transferMembers_explanation,
      cancelLabel: localizations.info_cancel,
      confirmLabel: localizations.info_update,
      profileType: this.props.viewer.me.profileType,
      circleList: this.props.viewer.me.circles.edges.map(edge => edge.node).filter(node => node.id !== this.props.circle.id),
      circlesFromClub: this.props.viewer.me.circlesFromClub && this.props.viewer.me.circlesFromClub.edges && this.props.viewer.me.circlesFromClub.edges.length > 0
        ? this.props.viewer.me.circlesFromClub.edges.map(edge => edge.node).filter(node => node.id !== this.props.circle.id)
        : [],
      onCancel:() => {},
      onConfirm:(circle, deleteMembers) => this.transferUsersToCircle(circle, deleteMembers)
    })
  }

  transferUsersToCircle = (circle, deleteMembers) => {
    let newUsersVar = this.state.selectedUserList

    RelayStore.commitUpdate(
      new AddMembersMutation({
        viewer: this.props.viewer,
        idVar: circle.id,
        newUsersVar,
        circle: this.props.circle
      }),
      {
        onFailure: error => {
          this.msg.show(error.getError().source.errors[0].message, {
            time: 2000,
            type: 'error',
          });
          this.setState({isLoading: false})
	        setTimeout(() => {
		        this.msg.removeAll();
	        }, 2000);
        },
        onSuccess: (response) => {
          if (deleteMembers) {
            this.state.selectedUserList.forEach((user, index) => {
              if (index === this.state.selectedUserList.length - 1)
                this._deleteMember(user, true, true)
              else
                this._deleteMember(user, true, false)
            })
          }
          this.msg.show(localizations.popup_editCircle_update_success, {
            time: 2000,
            type: 'success',
          });
	        setTimeout(() => {
		        this.msg.removeAll();
	        }, 2000);
          this.setState({selectedUserList: []})

        },
      }
    )
  }

  _handleCopyURL = () => {
    this.synchronizeLink.disabled = false;
    this.synchronizeLink.select();
    document.execCommand('copy');
    this.synchronizeLink.disabled = true;
    this.msg.show(localizations.popup_copyCircle_link, {
      time: 2000,
      type: 'success',
    });
	  setTimeout(() => {
		  this.msg.removeAll();
	  }, 2000);
  }

  renderMetaTags = (circle) => {
    return <Helmet>
      <title>{"Sportunity - Cercle " + circle.name + " de " + circle.owner.pseudo}</title>
      <meta name="robots" content="noindex"/>
      <meta name="description" content={localizations.meta_description}/>
      <meta property="fb:app_id" content="1759806787601548"/>
      <meta property="og:type" content="article"/>
      <meta property="og:title" content={"Sportunity - Cercle "+circle.name + " de " + circle.owner.pseudo} />
      <meta property="og:description" content={
        (!circle.isCircleAccessibleFromUrl ? 'Cercle privé' + '\n' : '')  +
        localizations.circle_slogan
      }/>
      <meta property="og:url" content={appUrl + 'circle/'+ circle.id}/>
      <meta property="og:image" content={appUrl+"assets/images/icon_circle@3x.png"} />
      <meta property="og:image:width" content="250"/>
      <meta property="og:image:height" content="250"/>
    </Helmet>
  }

  render() {
    const { viewer, circle } = this.props
    const {isCurrentUserTheOwner, isCurrentUserAMember, isCurrentUserCoOwner, isCurrentUserAChild, isCurrentUserAParent, columns, rows } = this.state;

    let statusList = ['ACTIVE', 'INJURED', 'INACTIVE', 'OTHER'];
    let members = (circle && circle.members) ? circle.members.filter(e => true) : [];
    if (members && this.state.rowMembers) {
      members.sort((a, b) => {
        let rowA = rows.find(row => a.id === row.user.id);
        let rowB = rows.find(row => b.id === row.user.id);
        let indexA = statusList.findIndex((status) => status === rowA.answers[0].answer);
        let indexB = statusList.findIndex((status) => status === rowB.answers[0].answer);
        return indexA - indexB;
      });
    }
    let profileType = viewer.me ? viewer.me.profileType : null ;
    let isSubAccount = viewer.me ? viewer.me.isSubAccount : null ;
    let listType = {
      adults: localizations.circles_member_type_0,
      children: localizations.circles_member_type_1,
      teams: localizations.circles_member_type_2,
      clubs: localizations.circles_member_type_3,
      companies: localizations.circles_member_type_4,
    };

    
    return(
      <div style={styles.mainContainer}>
        {this.renderMetaTags(circle)}
        <ReactTooltip effect="solid" multiline={true}/>
        <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />

	      <Link to={'/my-circles'} style={styles.back}>
		      {localizations.circles_back}
	      </Link>

        {this.state.displayAndroidOpenApp &&
        <ConfirmationModal
          isOpen={true}
          title={localizations.android_open_appTitle}
          message={localizations.android_open_appText}
          confirmLabel={localizations.android_open_appConfirm}
          cancelLabel={localizations.android_open_appCancel}
          canCloseModal={true}
          onConfirm= {this.openAndroidApp}
          onCancel={() => {}}
        />
        }

        {(!this.state.isTermOfUseCheck || this.state.isCurrentUserWantToBeMember) &&
          <TermOfUseModal
            isOpen={true}
            circle={this.props.circle}
            viewer={this.props.viewer}
            toggleTermModal={this.toggleTermModal}
            onClose={() => {this.setState({isCurrentUserWantToBeMember: false, isTermOfUseCheck: true})}}
          />
        }
        
        {!this.state.displayPrivateCirclePopup &&

        <Wrapper style={styles.wrapper}>
            <Header
              viewer={viewer}
              circle={circle}
              user={viewer.me}
              isLogin={viewer.me !== null}
              isLoading={this.state.isLoading}
              isCurrentUserTheOwner={isCurrentUserTheOwner}
              isCurrentUserCoOwner={isCurrentUserCoOwner}
              isCurrentUserAMember={isCurrentUserAMember}
              activeTab={this.state.activeTab}
              switchToTab={this.switchToTab}
            />
            <TabHeader
              viewer={viewer}
              circle={circle}
              isCurrentUserTheOwner={isCurrentUserTheOwner}
              isCurrentUserCoOwner={isCurrentUserCoOwner}
              isCurrentUserAMember={isCurrentUserAMember}
              activeTab={this.state.activeTab}
              switchToTab={(tab) => this.switchToTab(tab)}
            />

          <div style={styles.bodyContainer}>

            {(this.state.activeTab === 'details' || this.state.activeTab === 'members' || this.state.activeTab === 'activities') && (
              (!isSubAccount ||
              (circle && ((circle.type === 'CHILDREN' && profileType === 'PERSON') ||
                (circle.type === 'TEAMS' && profileType === 'ORGANIZATION'))) ||
                (isCurrentUserTheOwner || isCurrentUserCoOwner)
              )) &&
              <div style={styles.newMemberSection}>
                {circle.type === 'CHILDREN' && !isCurrentUserTheOwner && !isCurrentUserCoOwner && isCurrentUserAParent 
                ? <AddMyChild
                    onClick={() => this._handleOpenSubAccountList(this.props)}
                  />
                : !this.state.isCurrentUserAMember &&
                    (viewer.me &&
                    ((circle.type === 'ADULTS' && viewer.me.profileType === 'PERSON') ||
                    (circle.type === 'CHILDREN' && isCurrentUserAChild) ||
                    ((circle.type === 'TEAMS' || circle.type === 'CLUBS') && viewer.me.profileType === 'ORGANIZATION') ||
                    (circle.type === 'COMPANIES' && (viewer.me.profileType === 'BUSINESS' || viewer.me.profileType === 'SOLETRADER' ))) ||
                    (!viewer.me)) &&
                  <Subscribe viewer={viewer}
                    circleId={this.props.circle.id}
                    isError={this.state.isError}
                    user={this.state.user}
                    onChange={this._handleChange}
                    onErrorChange={this._setIsError}
                    onCloseModal={this._resetMemberName}
                    isLoggedIn={viewer.me ? true : false}
                    onSubscribe={this._handleWantToBe}
                    {...this.state}
                  />
              }

              {(isCurrentUserTheOwner || isCurrentUserCoOwner) &&
                  <AddMember viewer={viewer}
                    circle={circle}
                    onRef={ref => this.addMemberRef = ref}
                    circleId={this.props.circle.id}
                    isError={this.state.isError}
                    user={this.state.user}
                    parent1={this.state.parent1}
                    parent2={this.state.parent2}
                    onChange={this._handleChange}
                    onParent1Change={this._handleParent1Change}
                    onParent2Change={this._handleParent2Change}
                    onErrorChange={this._setIsError}
                    onCloseModal={this._resetMemberName}
                    isLoggedIn={viewer.me ? true : false}
                    superMe={viewer.superMe}
                    me={viewer.me}
                    handleTransfer={this._handleTransfer}
                    selectedUserList={this.state.selectedUserList}
                    {...this.state}
                  />
                }

                {circle.isCircleAccessibleFromUrl && 
                  <div onClick={this._handleCopyURL} style={styles.shareButton}>
                    <input 
                      ref={(ref) => this.synchronizeLink = ref}
                      style={{opacity: 0, position: "absolute", width: 8}} 
                      value={appUrl + '/circle/' + circle.id} 
                      />
                    <i className='fa fa-link' style={styles.shareIcon} />
                    {localizations.circle_share_url}
                  </div>
                }

                {isCurrentUserAMember && 
                  <div onClick={this.unSubscribe} style={styles.shareButton}>
                    {localizations.circles_unsubscribe}
                  </div>
                }
            </div>
          }


          {this.state.activeTab === 'details' &&
            <Details
              viewer={viewer}
              circle={circle}
              isCurrentUserTheOwner={isCurrentUserTheOwner}
              isCurrentUserCoOwner={isCurrentUserCoOwner}
              switchToTab={(tab) => this.switchToTab(tab)}
            />
          }

          {this.state.activeTab === 'activities' && 
            <Activities
              viewer={viewer}
              user={viewer.me}
              circle={circle}
              isCurrentUserTheOwner={isCurrentUserTheOwner}
              isCurrentUserCoOwner={isCurrentUserCoOwner}
              switchToTab={(tab) => this.switchToTab(tab)}
            />
          }

          {this.state.activeTab === 'members' && this.state.activeSection === 'members' && 
            <Members
              viewer={viewer}
              user={viewer.me}
              circle={circle}
              activeSection={this.state.activeSection}
              isCurrentUserTheOwner={isCurrentUserTheOwner}
              isCurrentUserCoOwner={isCurrentUserCoOwner}
              isCurrentUserAMember={isCurrentUserAMember}
              rowMembers={this.state.rowMembers}
              switchToTab={(tab) => this.switchToTab(tab)}
              swtichToSection={(section) => this.setState({activeSection: section})}
              selectedUserList={this.state.selectedUserList}
              isRelaunchButtonVisible={this.state.isRelaunchButtonVisible}
              _displayRowsOrCards={this._displayRowsOrCards}
              _deleteMember={this._deleteMember}
              _handleUserClicked={this._handleUserClicked}
              isUserFilledInformation={this.isUserFilledInformation}
              _relaunchMembers={this._relaunchMembers}
              isUserMemberOfMergedCircle={this.isUserMemberOfMergedCircle}
              rows={rows}
              columns={columns}
              members={members}
              onLink={this._handleCopyURL}
              addMember={this.addMemberRef ? (circle.type === 'CHILDREN' ? this.addMemberRef._openChildModal : this.addMemberRef._openModal) : null}
              onSubscribe={this._handleWantToBe}
              queryCircle={this.state.queryCircle}
            />
          }
              
          {this.state.activeSection === 'information' && this.state.activeTab === 'members' && 
            <MembersInformation
              viewer={viewer}
              circle={circle}
              onLeave={() => this.setState({activeSection: 'members'})}
            />
          }

            {this.state.activeTab === 'settings' &&
              <EditCircle
                viewer={viewer}
                circle={circle}
                circleId={this.props.circle.id}
                {...this.state}
                onLeave={() => this.setState({activeSection: 'members', activeTab: 'members'})}
              />
            }

            {this.state.activeTab === 'statistics' &&
              <CircleStats
                viewer={this.props.viewer}
                user={this.props.circle.owner}
                userId={this.props.circle.owner.id}
                circleId={this.props.circle.id}
              />
            }

            {this.state.activeTab === 'chat' && 
              <Chat 
                viewer={viewer}
                circle={circle}
                isCurrentUserAMember={isCurrentUserAMember}
                isCurrentUserCoOwner={isCurrentUserCoOwner}
                isCurrentUserTheOwner={isCurrentUserTheOwner}
              />
            }

            <table id="table-to-xls" style={{display: 'none'}}>
              <tr>
                <td>Pseudo</td>
                {columns.map(info =>
                  <td key={info.id}>
                    {info.name}
                  </td>
                )}
              </tr>
              { rows.map((row, rowIndex) => (
                <tr key={rowIndex}>
                  <td >
                    {row.user.pseudo}
                  </td>
                  {row.answers.map((info, colIndex) => (
                    <td key={colIndex} >
                      {   info.askedInfo.type === "BOOLEAN"
                        ? info.answer ? localizations.circle_yes : info.answer === false ? localizations.circle_no : ''
                        : info.answer
                      }
                    </td>
                  ))}
                </tr>
              ))}
            </table>
          </div>
          </Wrapper>
        }
      </div>

    )
  }
}

styles = {
  mainContainer: {
    width: '100%', 
    display: 'flex',
    flexDirection: 'column'
  },
  loadingSpinner:{
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  pageHeader: {
    fontFamily: 'Lato',
    fontSize: 34,
    fontWeight: fonts.weight.large,
    color: colors.blue,
    display: 'flex',
    marginTop: 30,
    flexDirection: 'row',
    alignItems: 'baseline',
    justifyContent: 'space-between',
    width: 1665,
    '@media (max-width: 1930px)': {
      width: 1245
    },
    '@media (max-width: 1490px)': {
      width: 825
    },
    '@media (max-width: 1070px)': {
      width: '100%'
    },
    '@media (max-width: 900px)': {
      flexDirection: 'column',
      marginBottom: 0
    },
    '@media (max-width: 768px)': {
      paddingLeft: 20
    }
  },
  param: {
    fontSize: 18,
    color: colors.black,
    marginRight: 20
  },
  iconEdit: {
    cursor: 'pointer',
    color: colors.blueLight,
    fontSize: 18,
    ':hover': {
      color: colors.blue
    }
  },
  policyIcon: {
    marginLeft: 5,
    color: colors.gray
  },
  bodyContainer: {
    display: 'flex',
    //width: 'calc(100% - 250px)',

    margin: '0px 0 50px 0',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    minHeight: 600,
    padding: '0 15px',
    '@media (max-width: 677px)': {
      width: '97%',
      margin: 'auto'
    }
  },
  newMemberSection: {
    marginTop: 25,
    display: 'flex',
    alignItems: 'center'
  },
  ownerContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10
  },
  smallAvatar: {
    width: 30,
    height: 30,
    marginRight: 10,
    color: colors.blue,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    borderRadius: '50%',
  },
  ownerName: {
    color: colors.gray,
    fontSize: 22,
    fontWeight: 'normal'
  },
  memberList: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginTop: 15,
    width: '100%',
    padding: 0,
    flexWrap: 'wrap',
    '@media (max-width: 1070px)': {
      justifyContent: 'center'
    }
  },
  memberListRow: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginTop: 15,
    width: '100%',
    padding: 0,
    flexWrap: 'wrap',
  },
  navLink: {
    color: colors.blue,
    textDecoration: 'none',
    marginRight: '10px',
  },
  headerRow: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: 1665,
    '@media (max-width: 1930px)': {
      width: 1245
    },
    '@media (max-width: 1490px)': {
      width: 825
    },
    '@media (max-width: 1070px)': {
      width: '100%'
    },
  },
  headerRowFullWidth: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%'
  },
  buttonSection: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    flex: 1
  },
  icon: {
    cursor: 'pointer',
    color: colors.gray,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: 40,
    height: 40,
    transition: 'all cubic-bezier(0.22,0.61,0.36,1) .3s',
    borderRadius: 20,
    marginLeft: 5,
    ':hover': {
      backgroundColor: colors.gray,
      color: colors.white,
    }
  },
  textButton: {
    cursor: 'pointer',
    color: colors.gray,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    transition: 'all cubic-bezier(0.22,0.61,0.36,1) .3s',
    border: 'none',
    marginLeft: 5,
    fontFamily: 'Lato',
    fontSize: 14,
    lineHeight: '16px',
    padding: '5px 10px',
    ':hover': {
      borderRadius: '5px',
      backgroundColor: colors.gray,
      color: colors.white,
    }
  },
  button: {
    cursor: 'pointer',
    color: colors.gray,
    display: 'flex',
    fontSize: 16,
    justifyContent: 'center',
    alignItems: 'center',
    transition: 'all cubic-bezier(0.22,0.61,0.36,1) .3s',
    border: 'none',
    margin: '20px auto',
    padding: '5px 10px',
    ':active': {
      border: 'none'
    },
    ':hover': {
      borderRadius: '5px',
      backgroundColor: colors.gray,
      color: colors.white,
    }
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
    alignSelf: 'flex-start',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 15,
    marginLeft: 15
  },
  label: {
    fontFamily: 'Lato',
    fontSize: 16,
    color: colors.blue,
    marginRight: 10
  },
  bigLabel: {
    fontFamily: 'Lato',
    fontSize: 20,
    color: colors.blue,
    marginRight: 10,
    marginTop: 25
  },
  switchContainer: {
    marginLeft: 15
  },
  wrapper: {
    margin: '35px auto',
    display: 'flex',
    flexDirection: 'row',
    fontFamily: 'Lato',
    '@media (max-width: 960px)': {
      width: '100%',
    },
    '@media (max-width: 580px)': {
      display: 'block',
    }
  },
  tableRowHeader: {
    width: '100%',
    height: 50,
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: colors.white,
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.12)',
    border: '1px solid #E7E7E7',
    overflow: 'hidden',
    fontFamily: 'Lato',
    margin: '1px 0',
    padding: 15,
    textDecoration: 'none',
    justifyContent: 'space-between',
    alignItems: 'center',
    '@media (max-width: 768px)': {
      width: 'auto'
    }
  },
  tableRowHeaderPseudo: {
    flex: 3,
    marginRight: 10,
    fontWeight: 'bold',
    fontSize: 16,
    color: 'rgba(0,0,0,0.65)'
  },
  tableRowHeaderTitle: {
    flex: 2,
    marginRight: 10,
    fontWeight: 'bold',
    fontSize: 16,
    color: 'rgba(0,0,0,0.65)'
  },
  msgContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  msgIcon: {
    fontSize: '1.5em',
    color: '#a6a6a6',
    verticalAlign: 'sub'
  },
  msgHeader: {
    fontSize: 22,
    color: colors.blue,
    fontFamily: 'Lato',
    textAlign: 'center',
    lineHeight: '26px',
    fontWeight: 'bold'
  },
  msgText: {
    fontSize: 18,
    color: '#838383',
    fontFamily: 'Lato',
    textAlign: 'center',
    lineHeight: '26px',
    width: '75%',
  },
  msgLink: {
    color: colors.blue,
    textDecoration: 'none'
  },
  separator: {
    height: 1,
    width: '10%',
    backgroundColor: '#000',
    margin: '20px 0px',
  },
  shareButton: {
    marginLeft: 20,
    fontFamily: 'Lato',
    fontSize: 18,
    color: colors.blue,
    cursor: 'pointer'
  },
  shareIcon: {
    color: colors.darkGray,
    marginRight: 5,
    fontSize: 20
  },
  back: {
    fontSize: fonts.size.large,
    textDecoration: 'none',
    color: colors.blue,
    margin: '0px 10px 10px',
	  fontFamily: 'lato',
  }
};


const _updateRegisterFromAction = (text) => ({
  type: types.UPDATE_REGISTER_FROM,
  text
})

const _updateSubAccountCreation = (value) => ({
  type: types.UPDATE_REGISTER_SUBACCOUNT_CREATION, 
  value
})

const stateToProps = (state) => ({
  createProfileFrom: state.registerReducer.createProfileFrom,
  subAccountCreation: state.registerReducer.subAccountCreation
})

const dispatchToProps = (dispatch) => ({
  _updateRegisterFromAction: bindActionCreators(_updateRegisterFromAction, dispatch),
  _updateSubAccountCreation: bindActionCreators(_updateSubAccountCreation, dispatch)
})

const ReduxContainer = connect(stateToProps,dispatchToProps)(Radium(Circle));

export default Relay.createContainer(withRouter(ReduxContainer), {
  initialVariables: {
    userToken: null,
    queryAuthorizedAccounts: false,
    superToken: null,
    querySuperMe: false,
    queryCircle: false,
  },
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
        ${CircleMutation.getFragment('viewer')}
        ${AddMembersMutation.getFragment('viewer')}
        ${RelaunchMembersMutation.getFragment('viewer')}
        ${AddChildMutation.getFragment('viewer')}
        ${CircleStats.getFragment('viewer')}
        ${AddMember.getFragment('viewer')}
        ${Subscribe.getFragment('viewer')}
        ${EditCircle.getFragment('viewer')}
        ${MemberCard.getFragment('viewer')}
        ${MemberRow.getFragment('viewer')}
        ${MembersInformation.getFragment('viewer')}
        ${DeleteMemberMutation.getFragment('viewer')}
        ${Header.getFragment('viewer')}
        ${Activities.getFragment('viewer')}
        ${Chat.getFragment('viewer')}
        ${TermOfUseModal.getFragment('viewer')}
        me {
          ${Header.getFragment('user')}
          ${Activities.getFragment('user')}
          id
          pseudo
          avatar
          profileType
          isSubAccount
          circles (last:20) {
            edges {
              node {
                id,
                name
                memberCount
              }
            }
          }
          circlesFromClub(last: 100) {
            edges {
              node {
                id
                name,
                owner {
                  id
                  pseudo
                  avatar
                }
                memberCount
                members {
                  id
                  pseudo, 
                  avatar
                }
              }
            }
          }
        }
        authorizedAccounts(userToken: $userToken) @include(if: $queryAuthorizedAccounts) {
          id
          avatar
          pseudo
          accounts {
            id,
            avatar
            token,
            pseudo
          }
        }
        superMe (superToken: $superToken) @include(if:$querySuperMe) {
          id,
          pseudo
          avatar
          profileType
          subAccounts{
              id,
              avatar
              pseudo
              token
          }
        }
      }
      `,
    circle: () => Relay.QL`
        fragment on Circle {
          ${DeleteMemberMutation.getFragment('circle')}
          ${CircleMutation.getFragment('circle')}
          ${AddMembersMutation.getFragment('circle')}
          ${AddChildMutation.getFragment('circle')}
          ${Header.getFragment('circle')}
          ${Details.getFragment('circle')}
          ${Activities.getFragment('circle')}
          ${TermOfUseModal.getFragment('circle')}
          sportunities {
            edges {
              node {
                status
              }
            }
          }
          termsOfUses {
            id
            name
            link
            content
            acceptedBy {
              user {
                id
              }
            }
          }
          memberStatus @include (if: $queryCircle) {
            member {
              id
            }
            status
            starting_date
          }
          description
          id
          name
          address {
            address,
            city,
            country,
            position {
              lat, lng
            }
          }
          owner {
            id
            pseudo
            avatar
            ${CircleStats.getFragment('user')}
            profileType
          }
          coOwners {
            id
          }
          memberCount
          mode
          type
          isCircleUpdatableByMembers
          isCircleUsableByMembers
          isCircleAccessibleFromUrl
          circlePreferences {
            isChildrenCircle
          }
          members @include (if: $queryCircle) {
            id
            pseudo
            avatar
            lastConnexionDate
						sports {
							sport {
								id
								name {
                  EN
                  FR
                  DE
								}
							}
						}
						sportunityNumber
						followers
          }
          sport {
            sport {
              id,
              logo
              name {
                  EN,
                  FR,
              }
              levels {
                id,
                EN {
                    name,
                    skillLevel
                    description
                }
                FR {
                    name,
                    skillLevel
                    description
                }
              }
            }
            levels {
              id,
              EN {
                  name,
                  skillLevel
                  description
              }
              FR {
                  name,
                  skillLevel
                  description
              }
            }
          }
          memberParents {
            id
            pseudo
            avatar
            lastConnexionDate
						sports {
							sport {
								id
								name {
                  EN
                  FR
                  DE
								}
							}
						}
						sportunityNumber
						followers
          }
          subCircles(last: 20) @include (if: $queryCircle) {
            edges {
              node {
                id,
                members {
                  id
                }
              }
            }
          }
          askedInformation @include (if: $queryCircle) {
            id, 
            name,
            type,
            filledByOwner
          }
          membersInformation @include (if: $queryCircle) {
            id,
            information,
            user {
              id,
            }
            value
          }
        }
      `,
  },
});
