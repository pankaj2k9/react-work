import React from 'react';
import Radium from 'radium';
import Relay from 'react-relay';
import withRouter from 'react-router/lib/withRouter'
import { browserHistory } from 'react-router';

import { colors } from '../../theme';
import localizations from '../Localizations'
import Sharing from "./Sharing";
import {appUrl} from "../../../constants";

let styles;

class Header extends React.Component {
  constructor(props) {
    super(props)
  }

  openOwnerProfile = () => {
    let to = '/profile-view/' + this.props.circle.owner.id 
    browserHistory.push(to);
  }

  render() {
    const { 
      viewer, 
      user, 
      circle,
      isCurrentUserTheOwner,
      isCurrentUserCoOwner
    } = this.props

    let listType = {
        adults: localizations.circles_member_type_0,
        children: localizations.circles_member_type_1,
        teams: localizations.circles_member_type_2,
        clubs: localizations.circles_member_type_3,
        companies: localizations.circles_member_type_4,
      };

    return(
        <header style={styles.header}>
            <div style={styles.row}>
              <div style={styles.left}>
                  <div style={styles.leftCol}>
                      <img src="/assets/images/icon_circle@3x.png" width="70px"/>
                      <div style={styles.numberContainer}>
                          <span style={styles.number}>
                          {circle.memberCount}
                          </span>
                      </div>
                  </div>
                  <div style={styles.circleInfo}>
                      <h1 style={styles.title}>{circle.name}</h1>
                  </div>
              </div>
            </div>
            <div style={styles.secondRow}>
                <div onClick={this.openOwnerProfile} style={{...styles.icon, backgroundImage: circle.owner.avatar ? 'url('+ circle.owner.avatar +')' : 'url("https://sportunitydiag304.blob.core.windows.net/avatars/default-avatar.png")'}} />
                <div style={styles.column}>
                    <div style={styles.ownerPseudo} onClick={this.openOwnerProfile}>
                        {circle.owner.pseudo}
                    </div>
                    <div style={styles.info}>
                        {localizations.circles_member_type + ' : '}
                        <span style={{color: colors.black}}>{listType[circle.type.toLowerCase()]}</span>
                    </div>
                </div>
                <div style={styles.column}>
                    <div style={styles.privateOrPublic}>
                        {circle.mode === 'PUBLIC' ? localizations.circles_public : localizations.circles_private}
                    </div>
                    <div style={styles.privateOrPublic}>
                        {circle.isCircleUsableByMembers ? localizations.circles_shared : localizations.circles_not_shared}
                    </div>
                </div>
            </div>
            <div style={styles.sideContainer}>
              <Sharing
                sharedUrl = {appUrl + this.props.location.pathname}
                title = { 'Circle: ' + circle.name }
              />
            </div>
        </header>
    )
}}

styles = {
  header: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    backgroundColor: colors.blue,
    color: colors.white,
    fontFamily: 'Lato',
    padding: '30px 42px 30px',
    position: 'relative',
    '@media (max-width: 600px)': {
      padding: '30px 30px 30px',
    },
    '@media (max-width: 750px)': {
      display: 'block',
    }
  },
  sideContainer: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    position: 'absolute',
    right: 5, 
    top: 5, 
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
  secondRow: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    marginLeft: 100,
    '@media (max-width: 850px)': {
        marginLeft: 10,
        marginTop: 20
    }
  },
  column: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    flex: 9
  },
  title: {
    fontSize: 36,
    marginBottom: 13,
    maxWidth: 500,
  },

  left: {
    display: 'flex',
    alignItems: 'flex-start',
    flex: 1,
    '@media (max-width: 480px)': {
      display: 'block',
      textAlign: 'center',
    }
  },
  right: {
    display: 'flex',
    alignItems: 'flex-start',
    '@media (max-width: 480px)': {
      display: 'block',
      textAlign: 'center',
    }
  },

  leftCol: {
    color: colors.white,
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    marginRight: 30,
    '@media (max-width: 480px)': {
      margin: '0 auto 10px auto',
    }
  },
  circleInfo: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    flex: 1, 
  },

  numberContainer: {
    position: 'absolute',
    top: '10px',
    left: '26px',
    width: 35,
    textAlign: 'center'
  },
  number: {
    fontSize: 28,
    fontWeight: 'bold',
    fontFamily: 'Lato',
  },
  icon: {
    minWidth: 30,
    minHeight: 30,
    maxWidth: 30,
    maxHeight: 30,
    borderRadius: '50%',
    marginRight: 7,
    backgroundPosition: '50% 50%',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    flex: 1,
    cursor: 'pointer'
  },
  ownerPseudo: {
    textDecoration: 'none',
    color: colors.white,
    fontSize: 18,
    lineHeight: '30px',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
    cursor: 'pointer'
  },
  info: {
    color: colors.white,
    fontSize: 18,
  },
  privateOrPublic: {
      fontSize: 20,
      color: colors.black,
      fontFamily: 'Lato',
      marginLeft: 90,
      marginBottom: 10
    },
  editButton: {
      marginLeft: 25, 
      fontSize: 22,
      color: colors.darkBlue,
      cursor: 'pointer'
  }, 

};

export default Relay.createContainer(withRouter(Radium(Header)), {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
      }
    `,
    user: () => Relay.QL`
      fragment on User {
        id,
      }
    `,
    circle: () => Relay.QL`
      fragment on Circle {
          name
          owner {
              id
              pseudo
              avatar
          }
          memberCount
          type
          isCircleUsableByMembers
          mode
      }
    `,
  },
});
