import React from 'react'
import Relay from 'react-relay'
import Radium from 'radium'
import {
    GoogleMapLoader,
    GoogleMap,
    Marker,
  } from 'react-google-maps'

import localizations from '../../Localizations'
import { colors, fonts } from '../../../theme'

let styles ;

class DetailsTab extends React.Component {
    constructor(props) {
        super(props)
    }

    _sportNameTranslated = (sportName) => {
        let name = sportName.EN
        switch(localizations.getLanguage().toLowerCase()) {
          case 'en':
            name = sportName.EN
            break
          case 'fr':
            name = sportName.FR || sportName.EN
            break
          default:
            name = sportName.EN
            break
        }
        return name
      }
    
    displayLevel = (sport) => {
        if (sport.levels && sport.levels.length > 0)
            sport.levels = sport.levels.sort((a,b)=>{return a.EN.skillLevel - b.EN.skillLevel})
    
        let sports
        if (sport.allLevelSelected) {
            sports = sport.sport.levels.map(level => this._translatedLevelName(level))
        } else {
            sports = sport.levels.map(level => this._translatedLevelName(level))
        }
        
        if (!sport.levels || sport.levels.length === 0)
            return localizations.event_allLevelSelected
        else if (sports.length === 1) {
            return sports[0]
        } else {
            return sports[0] + ' ' + localizations.find_to +  ' ' + sports[sports.length - 1]
        }
    }
    
    _translatedLevelName = (levelName) => {
        let translatedName = levelName.EN.name
        switch(localizations.getLanguage().toLowerCase()) {
            case 'en':
                translatedName = levelName.EN.name
                break
            case 'fr':
                translatedName = levelName.FR.name || levelName.EN.name
                break
            default:
                translatedName = levelName.EN.name
                break
        }
        return translatedName
    }

    render() {
        
        const { circle, isCurrentUserTheOwner, isCurrentUserCoOwner } = this.props;

        let listType = {
            adults: localizations.circles_member_type_0,
            children: localizations.circles_member_type_1,
            teams: localizations.circles_member_type_2,
            clubs: localizations.circles_member_type_3,
            companies: localizations.circles_member_type_4,
        };
        
        return (
            <div style={styles.container}>
                <section style={styles.section}>
                    <div style={styles.row}>
                        <div style={styles.title}>
                            {localizations.circle_title_description}
                        </div>
                    </div>
                    <div style={styles.description}>
                        {circle.description ? circle.description : (isCurrentUserTheOwner || isCurrentUserCoOwner) ? localizations.circle_owner_complete_description : localizations.circle_uncomplete_description}
                    </div>
                </section>

                <section style={styles.section}>
                    <div style={styles.row}>
                        <div style={styles.title}>
                            {localizations.circle_title_description_advanced}
                        </div>
                    </div>
                    <div style={styles.description}>
                        <div style={styles.info}>
                            <span style={styles.infoTitle}>
                                {localizations.circles_member_type + ' : '}
                            </span>
                            {listType[circle.type.toLowerCase()]}
                            <div style={styles.infoExplanation}>
                                {localizations.circles_member_type_explanation.replace('{0}', listType[circle.type.toLowerCase()])}
                            </div>                            
                        </div>
                        {circle.mode === 'PUBLIC'
                        ?   <div style={styles.info}>
                                <span style={styles.infoTitle}>
                                    {localizations.circle_public + ' : '}
                                </span>
                                {localizations.circle_public_explaination2}
                            </div>
                        :   <div style={styles.info}>
                                <span style={styles.infoTitle}>
                                    {localizations.circle_publicFalse + ' : '}
                                </span>
                                {localizations.circle_publicFalse_explaination2}
                            </div>
                        }
                        {circle.isCircleUsableByMembers 
                        ?   <div style={styles.info}>
                                <span style={styles.infoTitle}>
                                    {localizations.circle_usable_by_members + ' : '}
                                </span>
                                {localizations.circle_usable_by_membersExplanation2}
                            </div>
                        :   <div style={styles.info}>
                                <span style={styles.infoTitle}>
                                    {localizations.circle_usable_by_membersFalse + ' : '}
                                </span>
                                {localizations.circle_usable_by_membersFalseExplanation2}
                            </div>
                        }
                    </div>
                </section>

                {circle.sport
                ?   <section style={styles.section}>
                        <div style={styles.title}>
                            {localizations.circle_title_sports}
                        </div>
                        <div style={styles.sportRow}>
                            <img src={circle.sport.sport.logo} style={styles.sportImage}/>
                            <div style={styles.sport}>
                                <span style={styles.sportName}>{this._sportNameTranslated(circle.sport.sport.name)}</span>
                                <span style={styles.qualification}>{this.displayLevel(circle.sport)}</span>
                            </div>
                        </div>
                    </section>
                :   <section style={styles.section}>
                        <div style={styles.title}>
                            {localizations.circle_title_sports}
                        </div>
                        <div style={styles.description}>
                            {(isCurrentUserTheOwner || isCurrentUserCoOwner) ? localizations.circle_owner_complete_sport : localizations.circle_uncomplete_sport}
                        </div>
                    </section>
                }
                {circle.address 
                ?   <section style={styles.section}>
                        <div style={styles.title}>
                            {localizations.circle_place}
                        </div>
                        <div style={styles.addressLine}>
                            {circle.address.address}
                        </div>
                        <div style={styles.addressLine}>
                            {circle.address.city}
                        </div>
                        <div style={styles.addressLine}>
                            {circle.address.country}
                        </div>
                        <div style={styles.map}>
                            <GoogleMapLoader
                                containerElement={
                                    <div style={{ 
                                        height: '100%' 
                                    }}/>
                                }
                                googleMapElement={
                                    <GoogleMap 
                                        defaultZoom={15}
                                        defaultCenter={{ lat: circle.address.position.lat , lng: circle.address.position.lng }}
                                        options={{ scrollwheel: false, navigationControl: false,mapTypeControl: false, scaleControl: false, draggable: false}} 
                                    >
                                        <Marker 
                                            position={{ 
                                                lat: circle.address.position.lat,
                                                lng: circle.address.position.lng }}
                                            defaultAnimation='2'
                                        />
                                    </GoogleMap>
                                }
                            />
                        </div>
                    </section>
                :   <section style={styles.section}>
                        <div style={styles.title}>
                            {localizations.circle_place}
                        </div>
                        <div style={styles.description}>
                        {(isCurrentUserTheOwner || isCurrentUserCoOwner) ? localizations.circle_owner_complete_address : localizations.circle_uncomplete_address}
                        </div>
                    </section>
                }
            </div>
        )
    }
}

styles = {
    container: {

    },
    section: {
        marginBottom: 40
    },
    row: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    title: {
        fontSize: 22,
        fontFamily: 'Lato',
        color: colors.darkGray,
        margin: '20px 0px'
    },
    editButton: {
      fontSize: 18,
      color: colors.darkGray, 
      fontFamily: 'Lato',
      cursor: 'pointer'
    },
    editIcon: {
        fontSize: 18,
        color: colors.darkBlue,
        marginRight: 10,
    }, 
    sportRow: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    sportImage: {
		color:colors.white,
		width: 74,
        height: 74,
    },
    sport: {
        marginLeft: 15, 
        marginBottom: 10,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        fontFamily: 'Lato',
        fontSize: 18
    },
    sportName: {
        color: colors.blue,
        marginBottom: 14
    },
    qualification: {
        color: colors.darkGray,
        fontSize: 16
    },
    description: {
        color: colors.darkGray,
        fontSize: 16,
        fontFamily: 'Lato',
        margin: 10
    },
    addressLine: {
        color: colors.darkGray,
        fontSize: 16,
        fontFamily: 'Lato',
        margin: 10
    },
    map: {
        width: '100%',
        height: 400,
        marginTop: 25,
        '@media (max-width: 480px)': {
          width: '100%',
        }
    },
    info: {
        marginBottom: 15
    },
    infoTitle: {
        fontWeight: 'bold'
    },
    infoExplanation: {
        fontStyle: 'italic',
        marginTop: 5
    }
}

export default Relay.createContainer(Radium(DetailsTab), {
    fragments: {
      circle: () => Relay.QL`
        fragment on Circle {
          id
          description
          type
          mode
          isCircleUsableByMembers
          address {
              address,
              city, 
              country
              position {
                  lat,
                  lng
              }
          }
          sport {
              sport {
                id,
                logo
                name {
                    EN,
                    FR,
                }
              }
              levels {
                id,
                EN {
                    name,
                    skillLevel
                }
                FR {
                    name,
                    skillLevel
                }
              }
          }
        }
      `
    }
  });