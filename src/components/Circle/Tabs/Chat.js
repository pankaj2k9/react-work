import React from 'react'
import Relay from 'react-relay'
import Radium from 'radium'
import Loading from 'react-loading';
import { browserHistory } from 'react-router';

import localizations from '../../Localizations'
import { colors, fonts } from '../../../theme'

import Message from '../../ProfileView/Message';
import Form from '../../ProfileView/Form';

import SendMessageMutation from '../../ProfileView/SendMessageMutation';
import ReadChatMutation from '../../ProfileView/ReadChatMutation';

let styles;

class ChatTab extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: false,
            sendingMessage: false
        }
    }

    componentDidMount() {
        if (this.props.circle) {
            this.setState({isLoading: true})
            this.props.relay.setVariables({
                queryChat: true, 
                circleId: this.props.circle.id
            }, readyState => {
                if (readyState.done) {
                    setTimeout(() => {
                        this.setState({ isLoading: false })
                        setTimeout(() => {
                            this._readChat(this.props.viewer.chat);
                            this._scrollToBottom()
                        }, 100);
                    }, 50);
                }
            })
        }
    }
    
    _readChat = (chat) => {
        const viewer = this.props.viewer
        this.props.relay.commitUpdate(
            new ReadChatMutation({ viewer, chat }),
            {
                onFailure: error => console.log(error.getError()),
                onSuccess: (response) => {
                    console.log(response)
                },
            }
        );
    }
    
    _scrollToBottom = () => {
        this._commentsNode.scrollTop = this._commentsNode.scrollHeight;
    }
    
    _scrollAfterNewMessage = () => {
        setTimeout(() => {
            if (this.props.relay.pendingVariables)
                this._scrollAfterNewMessage();
            else {
                this._scrollToBottom();
            } 
        }, 100);
    }
    
    _handleSendMessage = (message) => {
        if (message && message.length > 0) {
            this.setState({
                sendingMessage: true
            })
            this.props.relay.commitUpdate(
                new SendMessageMutation({
                    viewer: this.props.viewer,
                    chat: this.props.viewer.chat,
                    message: {
                        text: message,
                        author: this.props.viewer.me.id
                    },
                    chatId: this.props.viewer.chat.id
                }), {
                    onSuccess: () => {
                        this.setState({
                            sendingMessage: false
                        })
                        this._scrollAfterNewMessage();
                    },
                    onFailure: (error) => {
                        console.log("error sending the message")
                        this.setState({
                            sendingMessage: false
                        })
                    },
                }
            );
        }
    }

    openLogin = () => {
        browserHistory.push('/login');
    }

    render() {

        const { isCurrentUserAMember, isCurrentUserCoOwner, isCurrentUserTheOwner } = this.props;

        return (
            <div style={styles.container}>
                <div style={styles.title}>
                    {localizations.circle_title_chat}
                </div>
                {this.state.isLoading  
                ?   <div style={styles.loadingContainer}><Loading type='spinningBubbles' color={colors.blue} /></div>
                :   (isCurrentUserAMember || isCurrentUserCoOwner || isCurrentUserTheOwner) 
                    ?   <div style={styles.chatContainer}>
                            <div style={styles.comments} ref={node => { this._commentsNode = node; }}>
                                <div style={styles.commentsList}>
                                    {this.props.viewer.chat && this.props.viewer.chat.messages.edges.map((message,index) => 
                                        <Message 
                                            key={index} 
                                            message={message.node} 
                                            me={this.props.viewer.me}
                                        />
                                    )}
                                </div>
                            </div>
                            { this.props.viewer.chat &&
                                <Form
                                    sendMessage={this._handleSendMessage}
                                    user={this.props.viewer.me}
                                    sendingMessage={this.state.sendingMessage}
                                />
                            }
                        </div>
                    :   <div style={styles.cannotAccessContainer}>
                            <i style={styles.alertIcon} className="fa fa-exclamation-circle fa-2x" />
                            <div style={styles.cannotAccessTitle}>
                                {localizations.circle_chat_not_available}
                            </div>
                            <div style={styles.cannotAccessText}>
                                {this.props.viewer.me 
                                ?   localizations.circle_chat_not_availableTextSubscribe
                                :   <span>
                                        {localizations.circle_chat_not_availableTextLogin}
                                        <span style={styles.cannotAccessLink} onClick={this.openLogin}>{localizations.circle_chat_not_availableTextLogin2}</span>
                                    </span>
                                }
                            </div>
                        </div>
                }
            </div>
        )
    }
}

styles = {
    container: {

    },
    title: {
        fontSize: 22,
        fontFamily: 'Lato',
        color: colors.darkGray,
        margin: '25px 0px'
    },
    loadingContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 50
    },
    chatContainer: {
        border: '1px solid #C3C3C3',
        borderRadius: '5px',
    
        // width: 720,
        height: 390,
    
        padding: 10,
    
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-end',
        margin: 'auto'
    },
    comments: {
        flexGrow: 1,
        overflowX: 'hidden',
        overflowY: 'scroll',
        marginBottom: 10,
    },
    cannotAccessContainer: {
        marginLeft: 25, 
        fontFamily: 'Lato',
        textAlign: 'center'
    },
    alertIcon: {
        color: colors.blue,
        fontSize: 60,
        marginBottom: 25
    },
    cannotAccessTitle: {
        color: colors.blue,
        fontSize: 22,
        textAlign: 'center',
    },
    cannotAccessText: {
        color: colors.darkGray,
        fontSize: 16,
        textAlign: 'center',
        marginTop: 20
    },
    cannotAccessLink: {
        color: colors.blue,
        cursor: 'pointer'
    }
}

export default Relay.createContainer(Radium(ChatTab), {
    initialVariables: {
        circleId: null,
        queryChat: false,
    },
    fragments: {
      viewer: () => Relay.QL`
        fragment on Viewer {
            id
            me {
                id
                avatar
                pseudo
            }
            ${SendMessageMutation.getFragment('viewer')}
            ${ReadChatMutation.getFragment('viewer')}
            chat (circleId: $circleId) @include (if: $queryChat) {
                ${SendMessageMutation.getFragment('chat')}
                ${ReadChatMutation.getFragment('chat')}
                id
                isActive
                messages (last:20) {
                  edges {
                    node {
                        ${Message.getFragment('message')}
                    }
                  }
                }
            }
        }
      `
    }
  });