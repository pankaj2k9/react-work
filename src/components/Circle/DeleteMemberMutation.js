import Relay from 'react-relay';

export default class DeleteMemberMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      removeCircleMember
    }`
  }
  
  getVariables() {
    return  {
      circleId: this.props.idVar,
      pseudo: this.props.nameVar,
      userId: this.props.removedUserIdVar,
     // email: this.props.nameVar,
    }
  }

  getFatQuery() {
      return Relay.QL`
        fragment on removeCircleMemberPayload {
          clientMutationId,
          viewer {
            id,
            me {
              circles
            }
            circle {
              members,
            }
          }
        }
      `
   
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
        fieldIDs: {
          viewer: this.props.viewer.id,
        },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        me {
          id
          circles(last: 100) {
            edges {
              node {
                memberCount
              }
            }
          }
        }
        
      }
    `,
    circle: () => Relay.QL`
    fragment on Circle {
      id,
    }
  `
  };

}
