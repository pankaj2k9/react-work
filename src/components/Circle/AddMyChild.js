import React from 'react'
import { colors, fonts } from '../../theme'
import localizations from '../Localizations'

let styles

class AddMyChild extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    
    return(
        <section>
            <div style={styles.button} onClick={this.props.onClick}>{localizations.circle_addMemberChild}</div>
        </section>)
  }
}

styles = {
  button: {
    fontFamily: 'Lato',
    fontSize: 18,
    color: colors.blue,
    cursor: 'pointer',
  },
}

export default AddMyChild