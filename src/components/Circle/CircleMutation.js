import Relay from 'react-relay';

export default class CircleMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      addCircleMember
    }`
  }
  
  getVariables() {
    return  {
      circleId: this.props.idVar,
      pseudo: this.props.nameVar,
      email: this.props.emailVar,
      userId: this.props.newUserIdVar,
    }
  }

  getFatQuery() {
      return Relay.QL`
        fragment on addCircleMemberPayload {
          clientMutationId,
          viewer {
            id,
            me {
              circles
              circlesUserIsIn
            }
            circle {
              members,
              memberParents
              memberCount
            }
          }
          circle {
            members,
            memberParents
            memberCount
          }
        }
      `
   
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
        fieldIDs: {
          viewer: this.props.viewer.id,
          circle: this.props.circle.id,
        },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        me {
          id
        }
        
      }
    `,
    circle: () => Relay.QL`
      fragment on Circle {
        id, 
      }
    `
  };

}
