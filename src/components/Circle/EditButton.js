import React, { Component } from 'react';
import Relay from 'react-relay'
import ReactLoading from 'react-loading'
import AlertContainer from 'react-alert';
import { browserHistory } from 'react-router';
import RelayStore from '../../RelayStore.js'
import ToggleDisplay from 'react-toggle-display'
import UpdateCircleMutation from './UpdateCircleMutation'
import DeleteCircleMutation from './DeleteCircleMutation'
import localizations from '../Localizations'
import { colors } from '../../theme';


let styles

class EditButton extends Component {

  constructor() {
    super();
    this.state = {
      isProcessing: false,
      isError: false,
			isDuplicateName: false,
			isEmptyName: false,
      showConfirmEdit: false,
      showConfirmDelete: false,
    }
    this.alertOptions = {
      offset: 14,
      position: 'top right',
      theme: 'light',
      time: 100,
      transition: 'fade',
    };
  }

  _submitUpdate = () => {
    this.setState({ isProcessing: true })
    //this._changeLoadingStatus(true);
    const viewer = this.props.viewer
    const userIDVar = this.props.viewer.id
    const idVar = this.props.circleId
    const nameVar = this.props.circleName
    const modeVar = this.props.isCirclePublic ? 'PUBLIC' : 'PRIVATE';
    const isCircleUpdatableByMembersVar = this.props.isCircleUpdatableByMembers;
    
    RelayStore.commitUpdate(
      new UpdateCircleMutation({
        viewer,
        userIDVar,
        idVar,
        nameVar,
        modeVar,
        isCircleUpdatableByMembersVar
      }),
      {
        onFailure: error => {
          setTimeout(() => {
            this.setState({ isProcessing: false }) 
            this.props.onClose();}, 1500)
          this.msg.show(localizations.popup_editCircle_update_failed, {
            time: 2000,
            type: 'error',
          });
          let errors = JSON.parse(error.getError().source);
          console.log(errors);
	        setTimeout(() => this.msg.removeAll(), 2000);
        },
        onSuccess: (response) => {
          console.log(response);
          //this._changeLoadingStatus(false);
          setTimeout(() => {
            this.setState({ isProcessing: false }) 
            this.props.onClose();}, 1500)
          this.msg.show(localizations.popup_editCircle_update_success, {
            time: 2000,
            type: 'success',
          });
	        setTimeout(() => this.msg.removeAll(), 2000);
          
        },
      }
    )
  }

  _submitDelete = () => {
    this.setState({ isProcessing: true })
    //this._changeLoadingStatus(true);
    const viewer = this.props.viewer
    const userIDVar = this.props.viewer.id
    const idVar = this.props.circleId
    
    RelayStore.commitUpdate(
      new DeleteCircleMutation({
        viewer,
        userIDVar,
        idVar,
      }),
      {
        onFailure: error => {
          setTimeout(() => {
            this.setState({ isProcessing: false }) 
            this.props.onClose();}, 1500)
          this.msg.show(localizations.popup_editCircle_update_failed, {
            time: 2000,
            type: 'error',
          });
	        setTimeout(() => this.msg.removeAll(), 2000);
          let errors = JSON.parse(error.getError().source);
          console.log(errors);
          
        },
        onSuccess: (response) => {
          console.log(response);
          //this._changeLoadingStatus(false);
          setTimeout(() => {
            browserHistory.push(`/my-circles`)
          }, 1500)
          this.msg.show(localizations.popup_editCircle_update_success, {
            time: 2000,
            type: 'success',
          });
	        setTimeout(() => this.msg.removeAll(), 2000);
          
        },
      }
    )
  }

  _checkName = () => {
    if(this.props.circleName.length) {
      this.props.onErrorChange(false);
      this.setState({isEmptyName: false});
      return true;
    } else {
      this.props.onErrorChange(true);
      this.setState({isEmptyName: true});
      return false
    }
  }

  _checkDuplicate = () => {
    const { viewer } = this.props
    if(this.props.viewer.me.circles.edges
      .filter(edge => edge.node.name === this.props.circleName && edge.node.id !== this.props.circle.id).length > 0) {
        this.setState({
          isDuplicateName: true,
        })
        return true
    }
    else {
      this.setState({
        isDuplicateName: false,
      })
      return false
    }
  }

  _handleEdit = () => {
    if(this._checkName() && !this._checkDuplicate()) {
      this.setState({
        showConfirmEdit: true,
      })
    }
  }

  _handleDelete = () => {
    this.setState({
      showConfirmDelete: true,
    })
  }

  _confirmEdit = () => {
    this._submitUpdate()
  }

  _cancelEdit = () => {
    this.setState({
      showConfirmEdit: false,
    })
  }

  _confirmDelete = () => {
    this._submitDelete()
  }

  _cancelDelete = () => {
    this.setState({
      showConfirmDelete: false,
    })
  }

  render() {
    return(
      <section>
        <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
        <div style={styles.container}>
          {
            this.state.isProcessing 
            ? <div style={{margin: 'auto',display: 'flex', justifyContent: 'center'}}><ReactLoading type='cylon' color={colors.blue} /></div>
            : <section>
                <ToggleDisplay show={this.state.isDuplicateName}>
                  <div style={styles.error}>This name already exists.</div>
                </ToggleDisplay>
                <ToggleDisplay show={this.state.isEmptyName}>
                  <div style={styles.error}>This name is empty.</div>
                </ToggleDisplay>
                <ToggleDisplay show={this.state.showConfirmEdit}>
                  <div>
                    <span style={styles.confirm}>{localizations.circles_editConfirm}</span>&nbsp;&nbsp;&nbsp;
                    <span style={styles.linkYes} onClick={this._confirmEdit}>{localizations.circle_yes}</span>&nbsp;&nbsp;&nbsp;
                    <span style={styles.linkNo} onClick={this._cancelEdit}>{localizations.circle_no}</span> 
                  </div>
                </ToggleDisplay>
                <ToggleDisplay show={this.state.showConfirmDelete}>
                  <div>
                    <span style={styles.error}>{localizations.circles_deleteConfirm}</span>&nbsp;&nbsp;&nbsp;
                    <span style={styles.linkYes} onClick={this._confirmDelete}>{localizations.circle_yes}</span>&nbsp;&nbsp;&nbsp;
                    <span style={styles.linkNo} onClick={this._cancelDelete}>{localizations.circle_no}</span> 
                  </div>
                </ToggleDisplay>
                <button onClick={this._handleEdit} style={styles.editButton}>{localizations.circles_save}</button>
                <button onClick={this._handleDelete} style={styles.deleteButton}>{localizations.circles_delete}</button>
              </section>
          }
          
        </div>
      </section>
    )
  }
}

styles = {
  container: {
    width: '80%',
  },
  editButton: {
    width: '400px',
		height: '50px',
		backgroundColor: colors.green,
		boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
		borderRadius: '3px',
    display: 'inline-block',
    fontFamily: 'Lato',
    fontSize: '22px',
    textAlign: 'center',
    color: colors.white,
    borderWidth: 0,
    marginTop: 10,
    marginBottom: 10,
    cursor: 'pointer',
		lineHeight: '27px',
  },
  deleteButton: {
		width: '400px',
		height: '50px',
		backgroundColor: colors.redGoogle,
		boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
		borderRadius: '3px',
    display: 'inline-block',
    fontFamily: 'Lato',
    fontSize: '22px',
    textAlign: 'center',
    color: colors.white,
    borderWidth: 0,
    marginTop: 10,
    marginBottom: 10,
    cursor: 'pointer',
		lineHeight: '27px',
  },
  error: {
    color: colors.red,
    fontSize: 16,
    fontFamily: 'Lato',
    width: 300,
    margin:0,
  },
  confirm: {
    color: colors.green,
    fontSize: 16,
    fontFamily: 'Lato',
    width: 300,
    marginTop:20,
    marginBottom: 10,
  },
  linkYes: {
    color: colors.blue,
    fontSize: 16,
    fontFamily: 'Lato',
    marginTop:10,
    marginBottom: 20,
    width:40,
    cursor:'pointer',
  },
  linkNo: {
    color: colors.gray,
    fontSize: 16,
    fontFamily: 'Lato',
    marginTop:10,
    marginBottom: 20,
    width:40,
    cursor:'pointer',
  },
}


export default Relay.createContainer(EditButton, {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
        me {
          id
          circles (last:50) {
            edges {
              node {
                id,
                name
              }
            }
          }
        }
        ${UpdateCircleMutation.getFragment('viewer')}
        ${DeleteCircleMutation.getFragment('viewer')}
      }
    `
  }
});