import Relay from 'react-relay';

export default class AddChildMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      addParentMember
    }`
  }
  
  getVariables() {
    return  {
      circleId: this.props.idVar,
      parent1Id: this.props.parent1IdVar,
      parent1Email: this.props.parent1EmailVar,
      parent2Id: this.props.parent2IdVar, 
      parent2Email: this.props.parent2EmailVar,
      childPseudo: this.props.childPseudoVar
    }
  }

  getFatQuery() {
      return Relay.QL`
        fragment on addParentMemberPayload {
          clientMutationId,
          viewer {
            id,
            circle {
              members
              memberParents
            }
          }
        }
      `
   
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
        fieldIDs: {
          viewer: this.props.viewer.id,
        },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        me {
          id
        }
      }
    `,
    circle: () => Relay.QL`
    fragment on Circle {
      id,
      memberParents
    }
  `
  };

}
