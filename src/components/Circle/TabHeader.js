import React from 'react';
import Radium from 'radium';

import { colors } from '../../theme';
import localizations from '../Localizations'

let styles;

class TabHeader extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
        hoverTab: null
    }
  }

  onMouseEnter = tab => {
      this.setState({
          hoverTab: tab
      })
  }
  onMouseLeave = () => {
    this.setState({
        hoverTab: null
    })
  }

  render() {
    const { 
      viewer, 
      circle, 
      activeTab,
      isCurrentUserTheOwner,
      isCurrentUserCoOwner,
      isCurrentUserAMember
    } = this.props;
    const {
        hoverTab
    } = this.state ;

	  const count = circle.sportunities.edges.filter(e => e.node.status !== 'Past').length

    return(
        <div style={styles.container}>
            <div style={activeTab === 'details' ? styles.activeTab : hoverTab === 'details' ? styles.tabHover : styles.tab} key={"details"} onClick={() => this.props.switchToTab('details')} onMouseEnter={() => this.onMouseEnter('details')} onMouseLeave={this.onMouseLeave}>
                <img src="/assets/images/information.png" style={activeTab === 'details' ? styles.activeIcon : styles.icon}/>
                {localizations.circle_tab_details}
            </div>
            <div style={activeTab === 'members' ? styles.activeTab : hoverTab === 'members' ? styles.tabHover : styles.tab} key={"members"} onClick={() => this.props.switchToTab('members')} onMouseEnter={() => this.onMouseEnter('members')} onMouseLeave={this.onMouseLeave}>
                <img src="/assets/images/members-01.png" style={activeTab === 'members' ? styles.activeIcon : styles.icon}/>
                {localizations.circle_tab_members + ' (' + circle.memberCount + ')'}
            </div>
            <div style={isCurrentUserTheOwner || isCurrentUserCoOwner || isCurrentUserAMember ? activeTab === 'chat' ? styles.activeTab : hoverTab === 'chat' ? styles.tabHover : styles.tab : styles.disabledTab} key={"chat"} onClick={() => (isCurrentUserTheOwner || isCurrentUserCoOwner || isCurrentUserAMember) && this.props.switchToTab('chat')} onMouseEnter={() => this.onMouseEnter('chat')} onMouseLeave={this.onMouseLeave} >
                <img src="/assets/images/comment_bubble.png" style={isCurrentUserTheOwner || isCurrentUserCoOwner || isCurrentUserAMember ? activeTab === 'chat' ? styles.activeIcon : styles.icon : styles.disabledIcon} />
                {localizations.circle_tab_chat}
            </div>
            <div style={activeTab === 'activities' ? styles.activeTab : hoverTab === 'activities' ? styles.tabHover : styles.tab} key={"activities"} onClick={() => this.props.switchToTab('activities')} onMouseEnter={() => this.onMouseEnter('activities')} onMouseLeave={this.onMouseLeave}>
                <img src="/assets/images/activity_blue-01.png" style={activeTab === 'activities' ? styles.activeIcon : styles.icon}/>
                {localizations.circle_tab_activities + (count > 0 ? ' (' +  count + ')' : '')}
            </div>
            {circle.owner.profileType === 'PERSON' &&
                <div
                  style={activeTab === 'statistics' ? styles.activeTab : hoverTab === 'statistics' ? styles.tabHover : styles.tab}
                  key={"statistics"} onClick={() => this.props.switchToTab('statistics')}
                  onMouseEnter={() => this.onMouseEnter('statistics')} onMouseLeave={this.onMouseLeave}>
                  <img src="/assets/images/statistic_bleu-01.png"
                       style={activeTab === 'statistics' ? styles.activeIcon : styles.icon}/>
                  {localizations.profileView_tab_statistics}
                </div>
            }
            {(isCurrentUserTheOwner || isCurrentUserCoOwner) && 
                <div style={activeTab === 'settings' ? styles.activeTab : hoverTab === 'settings' ? styles.tabHover : styles.tab} key={"settings"} onClick={() => this.props.switchToTab('settings')} onMouseEnter={() => this.onMouseEnter('settings')} onMouseLeave={this.onMouseLeave}>
                <img src="/assets/images/settings-02.png" style={(isCurrentUserTheOwner || isCurrentUserCoOwner) ? activeTab === 'settings' ? styles.activeIcon : styles.icon : styles.disabledIcon}/>
                    {localizations.circle_tab_settings}
                </div>
            }
        </div>
    )

    }
}

styles = {
    container: {
        minHeight: 60,
        backgroundColor: colors.lightGray,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: '-2px',
        flexWrap: 'wrap',
        '@media (max-width: 600px)': {
            flexDirection: 'column',
            alignItems: 'normal'
        }
    },
    tab: {
        padding: '5px 10px',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        height: 60,
        cursor: 'pointer',
        fontSize: 16,
        fontFamily: 'Lato',
        transition: 'all cubic-bezier(0.22,0.61,0.36,1) .3s',
        fontWeight: 'bold',
        color: colors.darkGray,
    },
    tabHover: {
        padding: '5px 10px',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        height: 60,
        cursor: 'pointer',
        fontSize: 16,
        fontFamily: 'Lato',
        transition: 'all cubic-bezier(0.22,0.61,0.36,1) .3s',
        fontWeight: 'bold',
        backgroundColor: '#c9c9c9',
        color: colors.white, 
    },
    disabledTab: {
        padding: '5px 10px',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        height: 60,
        cursor: 'pointer',
        fontSize: 16,
        fontFamily: 'Lato',
        backgroundColor: 'rgb(225, 225, 225)',
        color: colors.gray,
        fontWeight: 'bold',
    },
    activeTab: {
        padding: '5px 10px',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.blue,
        flex: 1,
        height: 60,
        cursor: 'pointer',
        fontSize: 16,
        color: colors.white,
        fontFamily: 'Lato',
        fontWeight: 'bold',
    },
    icon: {
        width: 30,
        marginRight: 10,
        fontSize: 25,
        color: colors.blue
    },
    disabledIcon: {
        filter: 'grayscale(2)',
        width: 30,
        marginRight: 10,
        fontSize: 25,
        color: colors.darkGray
    },
    activeIcon: {
        filter: 'brightness(3)',
        width: 30,
        marginRight: 10,
        fontSize: 25,
        color: colors.white
    }
};

export default Radium(TabHeader);