import Relay from 'react-relay';

export default class CircleMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      relaunchMembersForAskedInformation
    }`
  }
  
  getVariables() {
    return  {
        circleId: this.props.idVar,
    }
  }

  getFatQuery() {
      return Relay.QL`
        fragment on relaunchMembersForAskedInformationPayload {
          clientMutationId,
          viewer {
            id,
            me
            circle 
          }
        }
      `
   
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
        fieldIDs: {
          viewer: this.props.viewer.id,
        },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        me {
          id
        }
        
      }
    `,
  };

}
