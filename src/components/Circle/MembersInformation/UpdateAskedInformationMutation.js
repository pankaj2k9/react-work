import Relay from 'react-relay';

export default class CircleMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      updateAskedInformation
    }`
  }
  
  getVariables() {
    return  {
        circleId: this.props.idVar,
        askedInformation: this.props.askedInformationVar
    }
  }

  getFatQuery() {
      return Relay.QL`
        fragment on updateAskedInformationPayload {
          clientMutationId,
          viewer {
            id,
            me
            circle {
                askedInformation
            }
          }
        }
      `
   
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
        fieldIDs: {
          viewer: this.props.viewer.id,
        },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        me {
          id
        }
        
      }
    `,
  };

}
