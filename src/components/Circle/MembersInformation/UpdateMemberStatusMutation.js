import Relay from 'react-relay';

export default class UpdateMemberStatusMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      updateMemberStatus
    }`
  }

  getVariables() {
    return  {
      circleId: this.props.idVar,
      memberStatus: this.props.memberStatusVar,
    }
  }

  getFatQuery() {
    return Relay.QL`
        fragment on updateCircleMemberStatusPayload {
          clientMutationId,
          viewer {
            id,
            me
            circle {
                memberStatus
            }
          }
          circle {
            memberStatus
          }
        }
      `

  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        circle: this.props.circle.id,
      },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        me {
          id
        }
        
      }
    `,
    circle: () => Relay.QL`
    fragment on Circle {
      id
      memberStatus {
        member {
          id
        }
        status
      }
    }`
  };

}