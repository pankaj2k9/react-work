import Relay from 'react-relay';

export default class CircleMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      updateCircle
    }`
  }
  
  getVariables() {
    return  {
      circleId: this.props.idVar,
      circle: {
        name: this.props.nameVar,
        mode: this.props.modeVar,
        sport: this.props.sportVar,
        address: this.props.addressVar,
        isCircleUpdatableByMembers: this.props.isCircleUpdatableByMembersVar,
        isCircleUsableByMembers: this.props.isCircleUsableByMembersVar,
        isCircleAccessibleFromUrl: this.props.isCircleAccessibleFromUrlVar, 
        circlePreferences: this.props.circlePreferencesVar,
        description:  this.props.circleDescriptionVar
      },
    }
  }

  getFatQuery() {
      return Relay.QL`
        fragment on updateCirclePayload {
          clientMutationId,
          viewer {
            id,
            me
            circle {
              members,
              isCircleUpdatableByMembers,
              isCircleUsableByMembers,
              isCircleAccessibleFromUrl, 
              name,
              mode
              description
              address
              sport
              circlePreferences {
                isChildrenCircle
              }
            }
          }
        }
      `
   
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
        fieldIDs: {
          viewer: this.props.viewer.id,
        },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        me {
          id
        }
        
      }
    `,
  };

}
