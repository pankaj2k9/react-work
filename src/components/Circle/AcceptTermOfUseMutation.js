import Relay from 'react-relay';

export default class AcceptTermOfUseMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      acceptTermsOfUse
    }`
  }
  
  getVariables() {
    return  {
      termsOfUseId: this.props.termsOfUseId,
      userId: this.props.userId,
    }
  }

  getFatQuery() {
      return Relay.QL`
        fragment on acceptCircleTermsOfUsePayload {
          clientMutationId,
          viewer {
            id,
            me {
              termsOfUses
              circles
            }
          }
        }
      `
   
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
        fieldIDs: {
          viewer: this.props.viewer.id,
        },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        me {
          id
        }
        
      }
    `,
  };

}
