import React from 'react'
import Relay from 'react-relay'
import { colors, fonts } from '../../theme'
import localizations from '../Localizations'

let styles

class Subscribe extends React.Component {
  constructor(props) {
    super(props)
  }
  
  render() {
    return(
        <section style={styles.container} onClick={this.props.onSubscribe}>
            <div style={{...styles.icon, backgroundImage: this.props.viewer.me && this.props.viewer.me.avatar ? 'url('+ this.props.viewer.me.avatar +')' : 'url("https://sportunitydiag304.blob.core.windows.net/avatars/default-avatar.png")'}} />
            <div style={styles.button}>{localizations.circle_subscribe}</div>
        </section>)
  }
}


styles = {
  container: {
    display: 'flex',
    flexDirection: 'rox',
    alignItems: 'center',
    cursor: 'pointer',
    marginRight: 20
  },
  button: {
    fontFamily: 'Lato',
    fontSize: 18,
    color: colors.blue,
  },
  icon: {
    minWidth: 30,
    minHeight: 30,
    maxWidth: 30,
    maxHeight: 30,
    borderRadius: '50%',
    marginRight: 7,
    backgroundPosition: '50% 50%',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    flex: 1
  },
}

export default Relay.createContainer(Subscribe, {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
        me {
          id
          pseudo
          avatar
        }
      }
    `,
  },
});