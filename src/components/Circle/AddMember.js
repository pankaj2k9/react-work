import React from 'react'
import Relay from 'react-relay'
import { colors, fonts } from '../../theme'
import TutorialModal from '../common/Tutorial/index.js'
import localizations from '../Localizations'
import AddMemberModal from './AddMemberModal'
import AddChildModal from './AddChildModal'

let styles
let modalStyles

class AddMember extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      modalIsOpen: false,
      childModalIsOpen: false, 
      selectedCircle: null,
      selectedUserList: [],
      tutorial4IsVisible: false
    }
  }

  componentDidMount = () => {
    this.props.onRef && this.props.onRef(this)
  }

  componentWillUnmount = () => {
    this.props.onRef && this.props.onRef(undefined)
  }

  componentWillReceiveProps = (nextProps) => {     
    if (this.props.viewer && this.props.viewer.me && this.props.viewer.me.id && !this.props.superMe && nextProps.superMe && (nextProps.superMe.profileType === 'BUSINESS' || nextProps.superMe.profileType === 'ORGANIZATION')) {
      this.setState({
        tutorial4IsVisible: true
      })
    }
	}

  _openModal = () => {
    this.setState({ modalIsOpen: true, tutorial4IsVisible: false })
  }

  _openChildModal = () => {
    this.setState({ childModalIsOpen: true, tutorial4IsVisible: false })
  }

  _closeModal = () => {
    this.setState({ 
      modalIsOpen: false,
      childModalIsOpen: false,
      selectedCircle: null,
      selectedUserList: [],
      tutorial4IsVisible: false
     })
    this.props.onErrorChange(false)
    this.props.onCloseModal(); 
  }

  _handleAutocompleteClicked = (user) => {
    this.props.onChange(user)
  }

  _handleAutocompleteParentClicked = (user) => {
    this.props.onParentChange(user)
  }  

  _handleSelectCircle = circle => {
    this.setState({
      selectedCircle: circle,
      selectedUserList: circle.members.map(member => member.id)
    })
  }

  _handleUserClicked = user => {
    let userList = this.state.selectedUserList;
    let index = userList.indexOf(user.id); 
    if (index >= 0)
      userList.splice(index, 1);
    else  
      userList.push(user.id);

    this.setState({
      selectedUserList: userList
    })
  }

  _handleSelectAll = () => {
    if (this.state.selectedCircle.members.length === this.state.selectedUserList.length)
      this.setState({
        selectedUserList: []
      })
    else
      this.setState({
        selectedUserList: this.state.selectedCircle.members.map(member => member.id)
      })
  }
  
  render() {
    const { me, circle } = this.props;

    return(
      <section style={styles.container}>
        {circle.type !== 'CHILDREN' && 
          <div style={styles.button} onClick={this._openModal}>
            {localizations.circle_addMember}
            <TutorialModal
              isOpen={this.state.tutorial4IsVisible}
              tutorialNumber={5}
              tutorialName={"team_small_tutorial4"}
              message={localizations.team_small_tutorial4}
              confirmLabel={localizations.team_small_tutorial_ok}
              onPass={() => this.setState({tutorial4IsVisible: false})}
              position={{
                top: 30,
                left: 50
              }}
              arrowPosition= {{
                top: -8,
                left: 25
              }}
            />
          </div>
        }

        {circle.type === 'CHILDREN' && 
          <div style={styles.button} onClick={this._openChildModal}>
            {localizations.circle_addMemberChild}
          </div>
        }
        {this.props.selectedUserList && this.props.selectedUserList.length > 0 &&
          <div style={styles.button} onClick={this.props.handleTransfer}>
            {localizations.circle_transferMembers}
          </div>
        }

        {this.state.modalIsOpen && 
          <AddMemberModal
            viewer={this.props.viewer}
            modalIsOpen={this.state.modalIsOpen}
            selectedCircle={this.state.selectedCircle}
            selectedUserList={this.state.selectedUserList}
            handleUserClicked={this._handleUserClicked}
            handleSelectAll={this._handleSelectAll}
            handleSelectCircle={this._handleSelectCircle}
            handleAutocompleteClicked={this._handleAutocompleteClicked}
            closeModal={this._closeModal}
            onChange={this.props.onChange}
            isLoggedIn={this.props.isLoggedIn}
            isError={this.props.isError}
            user={this.props.user}
            onErrorChange={this.props.onErrorChange}
            me={this.props.me}
            circle={this.props.circle}
            circleId={this.props.circleId}
          />
        }

        {this.state.childModalIsOpen && 
          <AddChildModal
            viewer={this.props.viewer}
            modalIsOpen={this.state.childModalIsOpen}
            closeModal={this._closeModal}
            onChange={this.props.onChange}
            onParent1Change={this.props.onParent1Change}
            onParent2Change={this.props.onParent2Change}
            isLoggedIn={this.props.isLoggedIn}
            isError={this.props.isError}
            user={this.props.user}
            parent1={this.props.parent1}
            parent2={this.props.parent2}
            onErrorChange={this.props.onErrorChange}
            me={this.props.me}
            circle={this.props.circle}
            circleId={this.props.circleId}
          />
        }
      </section>)
  }
}

styles = {
  container: {
    display: 'flex'
  },
  button: {
    fontFamily: 'Lato',
    fontSize: 18,
    color: colors.blue,
    cursor: 'pointer',
    textAlign: 'left',
    padding: '0 15px',
    position: 'relative'
  },
  modalContent: {
		display: 'flex',
		flexDirection: 'column',
    justifyContent: 'flex-start',
    width: 400,
	},
	modalHeader: {
		display: 'flex',
		flexDirection: 'row',
    alignItems: 'flex-center',
		justifyContent: 'space-between',
	},
	modalTitle: {
		fontFamily: 'Lato',
		fontSize:24,
		fontWeight: fonts.weight.medium,
		color: colors.blue,
		marginBottom: 20,
		flex: '2 0 0',
	},
	modalClose: {
		justifyContent: 'flex-center',
		paddingTop: 10,
		color: colors.gray,
		cursor: 'pointer',
  },
  row: {
    display: 'flex',
		flexDirection: 'row',
    alignItems: 'flex-center',
    justifyContent: 'space-between',
    marginBottom: 10
  },
  label: {
    fontFamily: 'Lato',
    fontSize: 18,
    color: colors.blue,
  },
  checkBox: {
    cursor: 'pointer',
    height: 14,
    width: 14
  }
}

export default Relay.createContainer(AddMember, {
  initialVariables: {
    pseudo: '_',
    email: '_',
    pseudo_autocomplete: '_',
    autocompletion_required: false
  },
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        me {
          id
        }
        ${AddMemberModal.getFragment('viewer')}
        ${AddChildModal.getFragment('viewer')}
      }
    `,
  },
});