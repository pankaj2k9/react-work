import React from 'react'
import Relay from 'react-relay'
import Radium from 'radium';
import Modal from 'react-modal';
import Link from 'react-router/lib/Link'
import AlertContainer from 'react-alert';

import RelayStore from '../../../RelayStore.js'
import { colors, fonts, metrics } from '../../../theme'
import localizations from '../../Localizations'

import InformationFormModal from './InformationFormModal';
import UpdateFormMutation from './UpdateFormMutation';
import DeleteFormMutation from './DeleteFormMutation';

let styles
let modalStyles

class InformationForms extends React.Component {
    constructor(props) {
        super(props)

        this.alertOptions = {
            offset: 14,
            position: 'top right',
            theme: 'light',
            transition: 'fade',
            time: 0,
        };

        this.state = {
            language: localizations.getLanguage(),
            isNewFormVisible: false,
            deleteFormModalOpen: false,
            formToDelete: null,
            isEditFormVisible: false,
            formToEdit: null
        }
    }

    componentDidMount = () => {
        this.props.relay.forceFetch()
    }

    componentWillReceiveProps = (nextProps) => {
    }

    _showNewForm = () => {
        this.setState({
            isNewFormVisible: true
        })
    }

    _handleCloseNewForm = () => {
        this.setState({
            isNewFormVisible: false
        })
    }

    _handleSaveNewForm = (form) => {
        const viewer = this.props.viewer ;

        RelayStore.commitUpdate(
            new UpdateFormMutation({
                viewer,
                nameVar: form.name, 
                circleIdsVar: form.circles, 
                askedInformationVar : form.askedInformation
            }),
            {
            onFailure: error => {
                this.msg.show(localizations.popup_editCircle_update_failed, {
                    time: 2000,
                    type: 'error',
                });
                let errors = JSON.parse(error.getError().source);
                console.log(errors);
                setTimeout(() => {
                    this.msg.removeAll();
                }, 2000);
                
            },
            onSuccess: (response) => {
                this.msg.show(localizations.popup_editCircle_update_success, {
                    time: 2000,
                    type: 'success',
                });
                this.setState({
                    isNewFormVisible: false,
                })
                this.props.relay.forceFetch()
                setTimeout(() => {
                    this.msg.removeAll();
                }, 2000);
            },
            }
        )
    }

    _handleEditForm = form => {
        this.setState({
            isEditFormVisible: true,
            formToEdit: form
        })
    }

    _handleCloseEditForm = () => {
        this.setState({
            isEditFormVisible: false,
            formToEdit: null
        })
    }

    _handleSaveEditForm = (form) => {
        const viewer = this.props.viewer ;

        RelayStore.commitUpdate(
            new UpdateFormMutation({
                viewer,
                idVar: form.id, 
                nameVar: form.name, 
                circleIdsVar: form.circles, 
                askedInformationVar : form.askedInformation
            }),
            {
            onFailure: error => {
                this.msg.show(localizations.popup_editCircle_update_failed, {
                    time: 2000,
                    type: 'error',
                });
                let errors = JSON.parse(error.getError().source);
                console.log(errors);
                setTimeout(() => {
                    this.msg.removeAll();
                }, 2000);
                
            },
            onSuccess: (response) => {
                this.msg.show(localizations.popup_editCircle_update_success, {
                    time: 2000,
                    type: 'success',
                });
                this.setState({
                    isEditFormVisible: false,
                })
                setTimeout(() => {
                    this.msg.removeAll();
                }, 2000);
            },
            }
        )
    }

    _handleOnRemove = (form) => {
        this.setState({
            deleteFormModalOpen: true,
            formToDelete: form
        })
    }

    _closeModal = () => {
        this.setState({
            deleteFormModalOpen: false,
            formToDelete: null
        })
    }

    _confirmDelete = () => {
        const viewer = this.props.viewer ;
        
        RelayStore.commitUpdate(
            new DeleteFormMutation({
                viewer,
                idVar: this.state.formToDelete.id
            }),
            {
            onFailure: error => {
                this.msg.show(localizations.popup_editCircle_update_failed, {
                    time: 2000,
                    type: 'error',
                });
                let errors = JSON.parse(error.getError().source);
                console.log(errors);
                setTimeout(() => {
                    this.msg.removeAll();
                }, 2000);                
            },
            onSuccess: (response) => {
                this.msg.show(localizations.popup_editCircle_update_success, {
                    time: 2000,
                    type: 'success',
                });
                this.setState({
                    deleteFormModalOpen: false,
                    formToDelete: null
                })
                setTimeout(() => {
                    this.msg.removeAll();
                }, 2000);
            },
            }
        )
    }

    render() {
        const { viewer, user, language } = this.props
    
        return(
        <div style={{width: '100%'}}>
            <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
            <div style={styles.pageHeader}>
                {localizations.circles_information}
            </div>
            
            <div style={styles.wrapper}>
                <div style={styles.bodyContainer}>
                    <div style={styles.button} onClick={this._showNewForm}>
                        {localizations.circles_information_new_form}
                    </div>

                    {user.circleInformationForms && user.circleInformationForms.length > 0  
                    ?   <div style={styles.memberList} >
                            <div style={styles.tableRowHeader}>
                                <div style={styles.tableRowHeaderText}>
                                    {localizations.circles_information_form_name}
                                </div>
                                <div style={styles.tableRowHeaderCircleText}>
                                    {localizations.circles_information_form_circles_applied}
                                </div>
                                <div style={{flex: 2}}/>
                            </div>
                            { user.circleInformationForms.map((form, index) => (
                                <div style={styles.tableRow} key={form.id} >
                                    <div style={styles.tableRowText}>
                                        {form.name}
                                    </div>
                                    <div style={styles.tableRowCircleText}>
                                        {form.circles && form.circles.edges && form.circles.edges.length > 0
                                        ?   form.circles.edges.map(edge => (
                                                edge.node.owner.id === user.id ? edge.node.name : edge.node.name + ' (' + edge.node.owner.pseudo + ')'
                                            )).join(', ')
                                        :   '-'
                                        }
                                    </div>
                                    <div style={styles.buttonContainer}>
                                        <div style={styles.icon} onClick={() => this._handleEditForm(form)}>
                                            <i key={"edit"+index} className="fa fa-pencil" style={styles.iconEdit}></i>
                                        </div>
                                        <div style={styles.icon} onClick={() => this._handleOnRemove(form)}>
                                            <i key={"delete"+index} className="fa fa-times" style={styles.iconRemove}></i>
                                        </div>
                                    </div>
                                </div>
                            ))}
                        </div>
                    :   <div style={styles.memberList}>
                            <div style={styles.noFormContainer}>
                                <div style={styles.noFormText}>
                                    {localizations.circles_information_form_none}
                                </div>
                                {localizations.getLanguage().toUpperCase() === 'FR'
                                ?	<img src="/assets/images/circle_information_form-FR.png" style={styles.noFormImage}/>
                                :	<img src="/assets/images/circle_information_form-EN.png" style={styles.noFormImage}/>
                                }
                                <div style={styles.noFormText}>
                                    {localizations.circles_information_form_circles_applied_see_tutorial}
                                    <Link to={'/faq/tutorial/paiement-des-cotisations'} style={{color: colors.blue, textDecoration: 'none'}}>
                                        {localizations.circles_information_form_circles_applied_see_tutorial2}
                                    </Link>
                                </div>
                            </div>
                        </div>
                    }
                </div> 
            </div>


            {this.state.isNewFormVisible &&
                <InformationFormModal 
                    isModalVisible={this.state.isNewFormVisible}
                    viewer={viewer}
                    user={user}
                    onSave={this._handleSaveNewForm}
                    onClose={this._handleCloseNewForm}
                />
            }

            {this.state.isEditFormVisible &&
                <InformationFormModal 
                    isModalVisible={this.state.isEditFormVisible}
                    viewer={viewer}
                    user={user}
                    formToEdit={this.state.formToEdit}
                    onSave={this._handleSaveEditForm}
                    onClose={this._handleCloseEditForm}
                />
            }
            
            <Modal
                isOpen={this.state.deleteFormModalOpen}
                onRequestClose={this._closeModal}
                style={modalStyles}
                contentLabel={localizations.circles_information_delete}
            >
                <div style={styles.modalContent}>
                    <div style={styles.modalHeader}>
                        <div style={styles.modalContent}>
                            <div style={styles.modalText}>
                                {localizations.circles_information_delete} ?
                            </div>
                            <div style={styles.modalExplanation}>
                                {localizations.circles_information_delete_explanation}
                            </div>
                            <div style={styles.modalButtonContainer}>
                                <button style={styles.submitButton} onClick={this._confirmDelete}>{localizations.circle_yes}</button>
                                <button style={styles.cancelButton} onClick={this._closeModal}>{localizations.circle_no}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </Modal>
        </div>

        )
    }
}

styles = {
    pageHeader: {
		fontFamily: 'Lato',
		fontSize: 34,
		fontWeight: fonts.weight.large,
		color: colors.blue,
		display: 'flex',
        maxWidth: 1400,
		margin: '30px auto 0px auto',
        flexDirection: 'row',
		alignItems: 'left',
        justifyContent: 'left',
        '@media (max-width: 900px)': {
            flexDirection: 'column',
            marginBottom: 0
        },
        '@media (max-width: 768px)': {
            paddingLeft: 20
        }
    },
    bodyContainer: {
        display: 'flex',
        width: '100%',
        margin: '0px 0 50px 0', 
        flexDirection: 'column',
		justifyContent: 'flex-start',
        minHeight: 600,
        padding: '0 15px'
    },
    button: {
        fontFamily: 'Lato',
        fontSize: 18,
        color: colors.blue,
        cursor: 'pointer',
        textAlign: 'left',
        padding: '0 15px',
        position: 'relative'
    },	
    wrapper: {
        margin: '35px auto',
        display: 'flex',
        flexDirection: 'row',
        fontFamily: 'Lato',
        '@media (max-width: 960px)': {
            width: '100%',
        },
        '@media (max-width: 580px)': {
            display: 'block',
        }
    },
    memberList: {
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'flex-start',
		marginTop: 15,
		width: '100%',
		padding: 0,
        flexWrap: 'wrap',
        '@media (max-width: 1070px)': {
			justifyContent: 'center'
		}
    },
    memberListRow: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        marginTop: 15,
        width: '100%',
        padding: 0,
        flexWrap: 'wrap',
    },
    tableRowHeader: {
        width: '100%',
        height: 50,
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: colors.white,
        boxShadow: '0 0 4px 0 rgba(0,0,0,0.12)',
        border: '1px solid #E7E7E7',
        overflow: 'hidden',
        fontFamily: 'Lato',
        margin: '1px 0',
        padding: 15,
        textDecoration: 'none',
        justifyContent: 'space-between',
        alignItems: 'center',
        '@media (max-width: 768px)': {
          width: 'auto'
        }
    },
    tableRowHeaderText: {
        flex: 3,
        marginRight: 10,
        fontWeight: 'bold',
        fontSize: 16,
        color: 'rgba(0,0,0,0.65)'
    },
    tableRowHeaderCircleText: {
        flex: 8,
        marginRight: 10,
        fontWeight: 'bold',
        fontSize: 16,
        color: 'rgba(0,0,0,0.65)'
    },
    tableRow: {
        width: '100%',
        height: 50,
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: colors.white,
        boxShadow: '0 0 4px 0 rgba(0,0,0,0.12)',
        border: '1px solid #E7E7E7',
        overflow: 'hidden',
        fontFamily: 'Lato',
        margin: '1px 0',
        padding: 15,
        textDecoration: 'none',
        justifyContent: 'space-between',
        alignItems: 'center',
        color: '#A6A6A6',
        transition: 'all cubic-bezier(0.22,0.61,0.36,1) .15s',
        ':hover': {
            backgroundColor: '#F1F1F1',
            color: '#B6B6B6',
        },
        '@media (max-width: 768px)': {
          width: 'auto'
        }
    },
    tableRowText: {
        flex: 3,
        marginRight: 10,
        fontWeight: 'bold',
        fontSize: 16,
    },
    tableRowCircleText: {
        flex: 8,
        marginRight: 10,
        fontSize: 16,
    },
    buttonContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        flex: 2,
        justifyContent: 'flex-end'
    },
    icon: {
		//flex: 1,
		fontSize: 24,
		cursor: 'pointer',
        textAlign: 'end',
        marginLeft: 10
    },
    iconRemove: {
        color: '#A6A6A6',
        ':hover': {
            color: colors.redGoogle
        }
    },
    iconEdit: {
        color: colors.blueLight,
        ':hover': {
            color: colors.blue
        }
    },
    modalContent: {
		display: 'flex',
		flexDirection: 'column',
        justifyContent: 'flex-start',
        width: 300,
		padding: 10,'@media (max-width: 768px)': {
			width: 'auto'
		}
	},
	modalHeader: {
		display: 'flex',
		flexDirection: 'row',
        alignItems: 'flex-center',
		justifyContent: 'flex-center',
	},
	modalTitle: {
		fontFamily: 'Lato',
		fontSize:24,
		fontWeight: fonts.weight.medium,
		color: colors.blue,
		marginBottom: 20,
		flex: '2 0 0',
	},
	modalClose: {
		justifyContent: 'flex-center',
		paddingTop: 10,
		color: colors.gray,
		cursor: 'pointer',
	},
	modalText: {
		fontSize: 18,
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'center',
		width: '100%',
		fontFamily: 'Lato',
	},
	modalExplanation: {
		fontSize: 16,
		color: colors.gray,
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'center',
		width: '100%',
		fontFamily: 'Lato',
		fontStyle: 'italic',
		marginTop: 10,
		textAlign: 'justify'
	},
	modalButtonContainer: {
		fontSize: 18,
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'center',
		width: '100%',
		fontFamily: 'Lato',
		marginTop: 30,
	},
	submitButton: {
		width: 80,
		backgroundColor: colors.blue,
        color: colors.white,
        fontSize: fonts.size.small,
        borderRadius: metrics.radius.tiny,
        outline: 'none',
		border: 'none',
		padding: '10px',
		cursor: 'pointer',
		boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
		margin: 10,
	},
  	cancelButton: {
		width: 80,
		backgroundColor: colors.gray,
        color: colors.white,
        fontSize: fonts.size.small,
        borderRadius: metrics.radius.tiny,
        outline: 'none',
		border: 'none',
		padding: '10px',
		cursor: 'pointer',
		boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
		margin: 10,
    },
    noFormContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        padding: '15px 25px',
        border: '1px solid ' + colors.lightGray,
        borderRadius: 5,
        marginTop: 40
    },
    noFormText: {
        fontSize: 16,
		color: colors.gray,
		fontFamily: 'Lato',
		marginTop: 10,
		textAlign: 'justify'
    },
    noFormImage: {
        maxWidth: '90%',
        height: 'auto',
        maxHeight: 450,
        marginTop: 25
    }
}

modalStyles = {
    overlay : {
        position          : 'fixed',
        top               : 0,
        left              : 0,
        right             : 0,
        bottom            : 0,
        backgroundColor   : 'rgba(255, 255, 255, 0.75)',
    },
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)',
        border                     : '1px solid #ccc',
        background                 : '#fff',
        overflow                   : 'auto',
        WebkitOverflowScrolling    : 'touch',
        borderRadius               : '4px',
        outline                    : 'none',
        padding                    : '20px',
    },
}

export default Relay.createContainer(Radium(InformationForms), {
  fragments: {
    viewer: () => Relay.QL`
        fragment on Viewer {
            id
            ${UpdateFormMutation.getFragment('viewer')}
            ${DeleteFormMutation.getFragment('viewer')}
        }
    `,
    user: () => Relay.QL`
        fragment on User {
            id
            circles (last: 25) {
                edges {
                    node {
                        id,
                        type
                        name,
                        memberCount
                    }
                }
            }
            circlesSuperUser(last: 60) {
                edges {
                    node {
                        id,
                        name,
                        type
                        memberCount
                        owner {
                            id
                            pseudo
                            avatar
                        }
                    }
                }
            }
            circleInformationForms {
                id
                name
                circles (last: 20) {
                    edges {
                        node {
                            id
                            name
                            owner {
                                id
                                pseudo
                                avatar
                            }
                        }
                    }
                }
                askedInformation {
                    id
                    name
                    type
                    filledByOwner
                }
            }
        }
      `,
    },
});
