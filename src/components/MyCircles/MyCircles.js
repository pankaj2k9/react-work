
import React from 'react'
import Relay from 'react-relay'
import Radium from 'radium'
import {browserHistory, Link} from 'react-router';
import withRouter from 'react-router/lib/withRouter'
import ReactLoading from 'react-loading'

import Header from '../common/Header/Header'
import Footer from '../common/Footer/Footer'
import Loading from '../common/Loading/Loading'
import LeftSide from './LeftSide';
import { fonts, colors } from '../../theme'
import AlertContainer from 'react-alert'
import RelayStore from '../../RelayStore.js'
import CirclePage from '../Circle/Circle';
import TutorialModal from '../common/Tutorial/index.js'
import {confirmModal} from '../common/ConfirmationModal'
import AllCircleMembers from './AllCircleMembers';
import InformationForms from './InformationForms';
import PaymentModels from './PaymentModels';
import CircleItem from './CircleItem'
import NewCircle from './NewCircle'
import UnsubscribeFromCircleMutation from './mutation/UnsubscribeFromCircle';
import NewPaymentModelMutation from './PaymentModels/NewPaymentModelMutation';
import UpdatePaymentModelMutation from './PaymentModels/UpdatePaymentModelMutation';
import DeletePaymentModelMutation from './PaymentModels/DeletePaymentModelMutation';
import UpdateFormMutation from './InformationForms/UpdateFormMutation';
import DeleteFormMutation from './InformationForms/DeleteFormMutation';
import TermOfUse from './TermOfUse'
import { isEqual} from 'lodash';

import localizations from '../Localizations'
import FindPublicCircle from "./FindPublicCircle";
import SaveCircleFilterMutation from "./SaveCircleFilterMutation";
import SetDefaultCircleFilterMutation from "./SetDefaultCircleFilterMutation";
import Sidebar from "./Sidebar";
import * as types from "../../actions/actionTypes";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

let styles

class MyCircles extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      isQuerying: false,
      isError: false,
      isNewCircleModalOpen: false,
      name: '',
      newCircleType: 0,
      newCirclePublic: false,
      newCircleInvitationWithLink: true, 
      newCircleShared: true,
      subCircles: [],
      newCircleSport: null,
      newCircleAddress: null,
      language: localizations.getLanguage(),
      tutorial3aIsVisible: false,
      activeSection: 'myCircles',
      selectedCircle: null,
      shouldQuery: false
    };
    this.alertOptions = {
      offset: 60,
      position: 'top right',
      theme: 'light',
      transition: 'fade',
    };
  }

  _setLanguage = (language) => {
    this.setState({ language: language })
  };

  componentDidMount = () => {
    let superToken = localStorage.getItem('superToken');

    this.props.relay.setVariables({
      querySuperMe: superToken ? true : false,
      superToken,
    });

    if (this.props.viewer && this.props.viewer.me && !this.props.viewer.me.basicCircleSavedFiltersCreated) {
			this._createDefaultFilters();
		}
	  else if (this.props.viewer && this.props.viewer.me && this.props.viewer.me.defaultSavedCircleFilter) {
		  let defaultFilter = [];
		  defaultFilter.push(this.props.viewer.me.defaultSavedCircleFilter)
		  this.props._updateSelectedFilters(defaultFilter)
	  }
    if (this.props.circleId) {
      this.setState({
        selectedCircle: this.props.circleId
      });
      let activeSection = null; 
      if (this.props.viewer && this.props.viewer.me && this.props.viewer.me.circles && this.props.viewer.me.circles.edges && this.props.viewer.me.circles.edges.length > 0) {
        this.props.viewer.me.circles.edges.forEach(edge => {
          if (edge.node.id === this.props.circleId)
            activeSection = 'myCircles';
        })
      }
      if (!activeSection && this.props.viewer && this.props.viewer.me && this.props.viewer.me.circlesUserIsIn && this.props.viewer.me.circlesUserIsIn.edges && this.props.viewer.me.circlesUserIsIn.edges.length > 0) {
        this.props.viewer.me.circlesUserIsIn.edges.forEach(edge => {
          if (edge.node.id === this.props.circleId)
            activeSection = 'sportClubs';
        })
      }
      if (!activeSection && this.props.viewer && this.props.viewer.me && this.props.viewer.me.circlesSuperUser && this.props.viewer.me.circlesSuperUser.edges && this.props.viewer.me.circlesSuperUser.edges.length > 0) {
        this.props.viewer.me.circlesSuperUser.edges.forEach(edge => {
          if (edge.node.id === this.props.circleId)
            activeSection = 'subAccounts';
        })
      }
      
      this.setState({activeSection})
    }
    else if (this.props.location.pathname.indexOf('subaccounts-circle-creation') >= 0) {
      this.setState({
        activeSection: 'subAccounts',
        isNewCircleModalOpen: true
      })
    }
    else if (this.props.location.pathname.indexOf('/find-circles') >= 0)
      this.setState({
        activeSection: 'findCircles'
      });
    else if (this.props.location.pathname.indexOf('/all-members') >= 0)
      this.setState({
        activeSection: 'allMembers'
      });
    else if (this.props.location.pathname.indexOf('/members-info') >= 0)
      this.setState({
        activeSection: 'membersInfo'
      });
    else if (this.props.location.pathname.indexOf('/payment-models') >= 0)
      this.setState({
        activeSection: 'paymentModels'
      });
    else if (this.props.location.pathname.indexOf('/terms-of-uses') >= 0)
      this.setState({
        activeSection: 'termOfUse'
      });
    else if (this.props.viewer && !this.props.viewer.me) {
      this.setState({
        activeSection: 'myCircles'
      })
      this.props._updateTypeFilter(["PUBLIC_CIRCLES"]);
    }
    else {
      this.setState({shouldQuery: true})
      this.props._updateTypeFilter(["MY_CIRCLES"]);
      if (this.props.viewer.me.profileType === 'PERSON')
        this.props._updateFilter(["ADULTS", "CHILDREN"])
      else 
        this.props._updateFilter(["ADULTS", "CHILDREN", "TEAMS", "CLUBS", "COMPANIES"])
    }
    setTimeout(() => this.setState({ loading: false }), 500)
  };

  _createDefaultFilters = () => {
		let savedCircleFilters = [];
		if (this.props.viewer.me.savedCircleFilters && this.props.viewer.me.savedCircleFilters.length > 0) {
      savedCircleFilters = this.props.viewer.me.savedCircleFilters
		  .map(item => ({
			  userCircleFilterId: item.id,
        filterName: item.filterName,
			  memberTypes: item.memberTypes,
        sport: item.sport.map(sport => ({sportID: sport.sport.id})),
        owners: item.owners.map(user => user.id),
			  circleType: item.circleType,
			  location: {
				  lat: item.location.lat,
				  lng: item.location.lng,
				  radius: 30,
			  }
		  }))
		}

		if (this.props.viewer.me.profileType === 'PERSON') {
			savedCircleFilters.push({
        filterName: localizations.myCircles_defaultFilters_MyCircles,
        memberTypes: ["ADULTS", "CHILDREN"],
        circleType: ["MY_CIRCLES", "CIRCLES_I_AM_IN"],
      });
      if (this.props.userLocation && this.props.userCity) {
        savedCircleFilters.push({
          filterName: localizations.myCircles_defaultFilters_AroundMe + ' ' + this.props.userCity,
          memberTypes: ["ADULTS", "CHILDREN"],
          circleType: ["PUBLIC_CIRCLES"],
          location: {
            lat: this.props.userLocation.lat(),
            lng: this.props.userLocation.lng()
          }
        });
      }
      if (this.props.viewer.me.subAccounts && this.props.viewer.me.subAccounts.length > 0) {
        savedCircleFilters.push({
          filterName: localizations.myCircles_defaultFilters_ChildrenCircles,
          memberTypes: ["ADULTS", "CHILDREN"],
          circleType: ["CHILDREN_CIRCLES"],
        })
      }
		}
		else if (this.props.viewer.me.profileType === 'ORGANIZATION') {
			savedCircleFilters.push({
        filterName: localizations.myCircles_defaultFilters_MyCircles,
        memberTypes: ["ADULTS", "CHILDREN", "TEAMS", "CLUBS", "COMPANIES"],
        circleType: ["MY_CIRCLES"],
      });
      if (this.props.viewer.me.subAccounts && this.props.viewer.me.subAccounts.length > 0) {
        savedCircleFilters.push({
          filterName: localizations.myCircles_defaultFilters_TeamsCircles,
          memberTypes: ["ADULTS", "CHILDREN", "TEAMS", "CLUBS", "COMPANIES"],
          circleType: ["CHILDREN_CIRCLES"]
        })
      }
		}
		else {
			savedCircleFilters.push({
        filterName: localizations.myCircles_defaultFilters_MyCircles,
        memberTypes: ["ADULTS", "CHILDREN", "TEAMS", "CLUBS", "COMPANIES"],
        circleType: ["MY_CIRCLES"],
      });
      if (this.props.viewer.me.subAccounts && this.props.viewer.me.subAccounts.length > 0) {
        savedCircleFilters.push({
          filterName: localizations.myCircles_defaultFilters_SubAccountsCircles,
          memberTypes: ["ADULTS", "CHILDREN", "TEAMS", "CLUBS", "COMPANIES"],
          circleType: ["CHILDREN_CIRCLES"]
        })
      }
		}

		if (savedCircleFilters.length > 0) {
			this.updateSavedFilter(savedCircleFilters, true, (props) => {
				if (props.viewer.me.savedCircleFilters.length > 0) {
					let defaultFilter = props.viewer.me.savedCircleFilters.find(filter => filter.filterName === localizations.myCircles_defaultFilters_MyCircles)
					if (defaultFilter) {
						this.updateDefaultFilter(defaultFilter.id, () => {
							this.props._updateSelectedFilters([defaultFilter])
						})
					}
				}
			});
		}
	}

  _setFilter = (nextProps) => {
    if (this.state.activeSection === 'myCircles') {
      this.setState({isQuerying: true})
      this.props.relay.setVariables({
        filterCircle: {
          types: nextProps.filter,
          location: nextProps.locationFilter ? {
            lat: nextProps.locationFilter.location.lat,
            lng: nextProps.locationFilter.location.lng,
            radius: 30,
          } : null,
          sport: nextProps.sportFilter && nextProps.sportFilter.length > 0 ? nextProps.sportFilter.map(sport => ({sportID: sport})) : null,
          nameCompletion: nextProps.nameCompletion
        },
        filterCircleSubAccount: {
          types: nextProps.filter,
          location: nextProps.locationFilter ? {
            lat: nextProps.locationFilter.location.lat,
            lng: nextProps.locationFilter.location.lng,
            radius: 30,
          } : null,
          owners: nextProps.userFilter,
          sport: nextProps.sportFilter && nextProps.sportFilter.length > 0 ? nextProps.sportFilter.map(sport => ({sportID: sport})) : null,
          nameCompletion: nextProps.nameCompletion
        },
        queryMyCircle: nextProps.typeFilter.indexOf("MY_CIRCLES") >= 0 || nextProps.typeFilter.length === 0,
        queryCirclesImIn: nextProps.typeFilter.indexOf("CIRCLES_I_AM_IN") >= 0 || nextProps.typeFilter.length === 0,
        querySubAccount: nextProps.typeFilter.indexOf("CHILDREN_CIRCLES") >= 0 || nextProps.typeFilter.length === 0,
        queryPublicCircle: nextProps.typeFilter.indexOf("PUBLIC_CIRCLES") >= 0 || nextProps.typeFilter.length === 0
      }, readyState => {
        if (readyState.done) {
            setTimeout(() => {
                this.setState({
                  isQuerying: false,
                  isQueryingText: false
                })
                
            }, 50);
        }
      });
    }
  }

  componentWillReceiveProps = (nextProps) => {
		if (this.props.viewer && this.props.viewer.me && this.props.viewer.me.id && !this.props.viewer.superMe && nextProps.viewer.superMe && (nextProps.viewer.superMe.profileType === 'BUSINESS' || nextProps.viewer.superMe.profileType === 'ORGANIZATION')) {
			setTimeout(() => 
				this.setState({
					tutorial3aIsVisible: true
				}), 100
			);
    }
	  if (!isEqual(this.props.selectedFilters, nextProps.selectedFilters) && nextProps.selectedFilters.length > 0) {
      //this.setState({isQuerying: true})
			let memberfilter = [];
			let sportFilter = [];
		  let typeFilter = [];
		  let userFilter = [];
		  
		  nextProps.selectedFilters.forEach((filter) => {
		  	if (filter.memberTypes !== null && filter.memberTypes !== undefined)
		  		filter.memberTypes.forEach((memberType) => {
		  			if (memberfilter.indexOf(memberType) < 0)
		  				memberfilter.push(memberType)
				  })
			  if (filter.sport !== null && filter.sport !== undefined)
			  	filter.sport.forEach((sport) => {
			  		if (sportFilter.indexOf(sport.sport.id) < 0)
			  			sportFilter.push(sport.sport.id)
				  })
			  if (filter.circleType !== null && filter.circleType !== undefined)
			  	filter.circleType.forEach((type) => {
			  		if (typeFilter.indexOf(type) < 0)
			  			typeFilter.push(type)
				  })
			  if (filter.owners !== null && filter.owners !== undefined)
			  	filter.owners.forEach((user) => {
			  		if (userFilter.indexOf(user.id) < 0)
			  			userFilter.push(user.id)
          })
        if (filter.location && filter.location.lat) {
          let geocoder = new google.maps.Geocoder();
          geocoder.geocode({'latLng': new google.maps.LatLng(filter.location)},(results, status) => {
            if (status === 'OK') {
              let city
              for (var a=0 ; a<results.length; a++) {
                let resultIdFound = false;
                for (var n=0; n<results[a].types.length ; n++) {
                  if (results[a].types[n] === "locality") {
                    resultIdFound = true ;
                  }
                }
                if (resultIdFound) {
                  for (var i=0; i<results[a].address_components.length; i++) {
                    for (var b=0;b<results[a].address_components[i].types.length;b++) {
                        if (results[a].address_components[i].types[b] == "locality") {
                            city = results[a].address_components[i].long_name;
                            break;
                        }
                    }
                  }
                }
              }
              if (city) 
                this.props._updateLocationFilter({location: {lat: filter.location.lat, lng:filter.location.lng}, label: city})
            }
          })          
        }
		  })
		  this.props._updateFilter(memberfilter);
		  this.props._updateSportFilter(sportFilter);
		  this.props._updateTypeFilter(typeFilter);
		  this.props._updateUserFilter(userFilter);
	  }
    if (!isEqual(this.props.typeFilter, nextProps.typeFilter) || !isEqual(this.props.filter, nextProps.filter)
	    || !isEqual(this.props.userFilter, nextProps.userFilter) || !isEqual(this.props.sportFilter, nextProps.sportFilter)
      || !isEqual(this.props.locationFilter, nextProps.locationFilter))
      this._setFilter(nextProps);
    if (this.props.nameCompletion !== nextProps.nameCompletion) {
      let tempo = nextProps.nameCompletion; 
      this.setState({isQueryingText: true})
      setTimeout(() => {
        if (tempo === this.props.nameCompletion) {
          this._setFilter(nextProps)
        }
      }, 250)
    }
    if (this.state.shouldQuery 
      && isEqual(this.props.selectedFilters, nextProps.selectedFilters) 
      && isEqual(this.props.typeFilter, nextProps.typeFilter) 
      && isEqual(this.props.filter, nextProps.filter)
      && isEqual(this.props.userFilter, nextProps.userFilter) 
      && isEqual(this.props.sportFilter, nextProps.sportFilter)
      && isEqual(this.props.locationFilter, nextProps.locationFilter)) {
        this.setState({shouldQuery: false});
        this._setFilter(nextProps)
    }
    if (!this.props.circleId && nextProps.circleId) {
      this.setState({
        selectedCircle: nextProps.circleId
      })
    }
    if (this.props.circleId && !nextProps.circleId) {
      this.setState({
        selectedCircle: null,
        activeSection: 'myCircles'
      })
      this._setFilter(nextProps)
    }
    else if (this.props.location.pathname !== nextProps.location.pathname && this.props.location.pathname.indexOf('/circle/') < 0 && nextProps.location.pathname.indexOf('/circle/') < 0) {
      if (nextProps.location.pathname.indexOf('/find-circles') >= 0)
        this.setState({
          activeSection: 'findCircles'
        });
      else if (nextProps.location.pathname.indexOf('/all-members') >= 0)
        this.setState({
          activeSection: 'allMembers'
        });
      else if (nextProps.location.pathname.indexOf('/members-info') >= 0)
        this.setState({
          activeSection: 'membersInfo'
        });
      else if (nextProps.location.pathname.indexOf('/payment-models') >= 0)
        this.setState({
          activeSection: 'paymentModels'
        });
      else if (nextProps.location.pathname.indexOf('/terms-of-uses') >= 0)
        this.setState({
          activeSection: 'termOfUse'
        });
      else {
        this.setState({
          activeSection: 'myCircles'
        });
        if (this.props.viewer && !this.props.viewer.me)
          this.props._updateTypeFilter(["PUBLIC_CIRCLES"])
        else {
          this.props._updateTypeFilter(["MY_CIRCLES"]);
          if (this.props.viewer.me.profileType === 'PERSON')
            this.props._updateFilter(["ADULTS", "CHILDREN"])
          else 
            this.props._updateFilter(["ADULTS", "CHILDREN", "TEAMS", "CLUBS", "COMPANIES"])
        }
      }
    }    
	};

  _setIsError = (value) => {
    this.setState({
      isError: value,
    })
  }

  _handleChange = (value) => {
    if (value !== this.state.name) {
      this.setState({
        name: value,
      })
    }
  };

  _updateSubCircles = (subCircles) => {
    this.setState({
      subCircles
    })
  };
  _updateNewCircleType = e => {
    this.setState({
      newCircleType: e
    })
  };

  _updateNewCirclePrivacy = e => {
    this.setState({
      newCirclePublic: e,
      newCircleInvitationWithLink: e ? true : this.state.newCircleInvitationWithLink
    })
  };

  _updateNewCircleInvitationWithLink = e => {
    this.setState({
      newCircleInvitationWithLink: e
    })
  };

  _updateNewCircleShared = e => {
    this.setState({
      newCircleShared: e
    })
  };

  _updateNewCircleSport = e => {
    this.setState({
      newCircleSport: e
    })
  };

  _closeNewCircle = () => {
    this.setState({
      isNewCircleModalOpen: false,
      name: '',
      newCircleType: 0,
      newCirclePublic: false,
      newCircleInvitationWithLink: true, 
      newCircleShared: false,
      subCircles: [],
      newCircleSport: null,
      newCircleAddress: null,
    })
  };

  _handleAddressChange = ({label}) => {
    const splitted = label.split(', ');
    if (splitted.length < 2) {
      this.msg.show(localizations.circle_address_error, {
        time: 2000,
        type: 'error',
      });
      setTimeout(() => {
        this.msg.removeAll();
      }, 2000)
      
      return ;
    }
    const address = splitted.slice(0, splitted.length-2).join(', ') || '';
    const country = splitted[splitted.length - 1] || '';
    const city = splitted[splitted.length - 2] || '';

    this.setState({
      newCircleAddress: {
        address,
        country,
        city,
      }
    });
  };

  unSubscribe = (circle, callback) => {
    confirmModal({
      title: localizations.circles_unsubscribe,
      message: localizations.circles_unsubscribe_validation + ' ' + circle.name + ' ?',
      confirmLabel: localizations.circle_remove_all_data_yes,
      cancelLabel: localizations.circle_remove_all_data_no,
      canCloseModal: true,
      onConfirm: () => {
        let params = {
          circleIdVar: circle.id,
          userIdVar: this.props.viewer.me.id,
          viewer: this.props.viewer
        }

        RelayStore.commitUpdate(
          new UnsubscribeFromCircleMutation(params),
          {
            onFailure: error => {
              console.log(error.getError());
              this.msg.show(localizations.circles_unsubscribe_error, {
                time: 2000,
                type: 'error',
              });
              setTimeout(() => this.msg.removeAll(), 2000);
            },
            onSuccess: (response) => {
              this.msg.show(localizations.circles_unsubscribe_success + ' ' + circle.name, {
                time: 2000,
                type: 'success',
              });
              setTimeout(() => this.msg.removeAll(), 2000);
              if (typeof callback !== 'undefined')  
                callback()
            },
          }
        );
      },
      onCancel: () => {}        
    })
  }

  _showMoreCircles = () => {
    this.setState({
      isQuerying: true
    })
    this.props.relay.setVariables({
      circlesSuperUserNumber: this.props.relay.variables.circlesSuperUserNumber + 5,
      circlesUserIsInNumber: this.props.relay.variables.circlesUserIsInNumber + 5,
      ownerOfCirclesNumber: this.props.relay.variables.ownerOfCirclesNumber + 5,
      firstCircle: this.props.relay.variables.firstCircle + 10
    }, readyState => {
      if (readyState.done) {
        setTimeout(() => this.setState({isQuerying: false}), 50)
      }
    }) 
  }

  _handleChangeSection = section => {
    this.setState({
      activeSection: section !== this.state.activeSection ? section : 'myCircles',
      selectedCircle: null
    });
    browserHistory.push(`/my-circles`);
    if (section === 'subAccounts') {
      this.setState({isQuerying: true})
      this.props.relay.setVariables({
        circlesSuperUserNumber: 100,
      }, readyState => {
        if (readyState.done) {
            setTimeout(() => {
                this.setState({isQuerying: false})
            }, 50);
        }
      })
    }
    else if (section === 'sportClubs') {
      this.setState({isQuerying: true})
      this.props.relay.setVariables({
        circlesUserIsInNumber: 100
      }, readyState => {
        if (readyState.done) {
            setTimeout(() => {
                this.setState({isQuerying: false})
            }, 50);
        }
      })
    }
  };

	updateSavedFilter = (data, basicCircleSavedFiltersCreated = false, callback) => {
		this.props.relay.commitUpdate(
			new SaveCircleFilterMutation(
        basicCircleSavedFiltersCreated 
        ? {
            viewer: this.props.viewer,
            user: this.props.viewer.me,
            userIDVar: this.props.viewer.me.id,
            savedFiltersVar: data,
            basicCircleSavedFiltersCreatedVar: basicCircleSavedFiltersCreated
          }
        : {
          viewer: this.props.viewer,
          user: this.props.viewer.me,
          userIDVar: this.props.viewer.me.id,
          savedFiltersVar: data
        }),
			{
				onSuccess: () => {
          if (typeof callback !== "undefined")
            setTimeout(() => callback(this.props), 150);
          else 
            this.msg.show(localizations.popup_findSportunity_filter_success, {
              time: 3000,
              type: 'success',
            });
				},
				onFailure: (error) => {
					console.log(error.getError());
					this.msg.show(error.getError(), {
						time: 3000,
						type: 'error',
					});
				},
			}
		)

		setTimeout(() => {
			this.msg.removeAll();
		}, 3000);
	}

	updateDefaultFilter = (data, callback) => {
		this.props.relay.commitUpdate(
			new SetDefaultCircleFilterMutation({
				viewer: this.props.viewer,
				user: this.props.viewer.me,
				filterIDVar: data
			}),
			{
				onSuccess: () => {
          if (typeof callback !== 'undefined') {
						callback();
					}
					else {
            this.msg.show(localizations.popup_findSportunity_filter_success, {
              time: 3000,
              type: 'success',
            });
          }
				},
				onFailure: (error) => {
					console.log(error.getError());
					this.msg.show(error.getError(), {
						time: 3000,
						type: 'error',
					});
				},
			}
		)

		setTimeout(() => {
			this.msg.removeAll();
		}, 3000);
  }
  
  _handleSearchChange = e => {
    this.props._updateNameCompletion(e.target.value)
  }

	_handleSeeMore = (newStatus, callback) => {
		if (newStatus)
			this.props.relay.setVariables({
				sportNb: this.props.viewer.sports.count
			}, readyState => {
        if (readyState.done) {
            setTimeout(() => {
                callback()
            }, 100);
        }
    })
		else
			this.props.relay.setVariables({
				sportNb: 10
			}, readyState => {
        if (readyState.done) {
            setTimeout(() => {
                callback()
            }, 100);
        }
    })
	};

  render() {
    const { viewer } = this.props
	  let circleList = [].concat(
		  viewer.me && viewer.me.circles && viewer.me.circles.edges ? viewer.me.circles.edges : [],
		  viewer.me && viewer.me.circlesSuperUser && viewer.me.circlesSuperUser.edges ? viewer.me.circlesSuperUser.edges : [],
		  viewer.me && viewer.me.circlesUserIsIn && viewer.me.circlesUserIsIn.edges ? viewer.me.circlesUserIsIn.edges : [],
		  viewer.circles && viewer.circles.edges ? viewer.circles.edges : []
	  );

    circleList.sort((a, b) => b.node.memberCount - a.node.memberCount);

    let circleNb = 0 ;
    viewer.me && viewer.me.circles && (circleNb =+ viewer.me.circles.count); 
    viewer.me && viewer.me.circlesSuperUser && (circleNb =+ viewer.me.circlesSuperUser.count); 
    viewer.me && viewer.me.circlesUserIsIn && (circleNb =+ viewer.me.circlesUserIsIn.count); 
    viewer.circles && viewer.circles.edges && (circleNb =+ viewer.circles.count); 

    return (
      <div>
        {this.state.loading && <Loading/>}
        <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
        { viewer && viewer.me ? <Header user={viewer.me} viewer={viewer} onChangeSection={this._handleChangeSection} {...this.state}/> : <Header user={null} viewer={viewer} {...this.state}/> }
        <div style={styles.bodyContainer}>
          {/*{//viewer && viewer.me &&*/}
            {/*<LeftSide*/}
              {/*activeSection={this.state.activeSection}*/}
              {/*onChangeSection={this._handleChangeSection}*/}
              {/*user={viewer.me}*/}
            {/*/>*/}
          {/*}*/}

	        {!this.props.circleId && this.state.activeSection === 'myCircles' &&
		        <Sidebar 
              {...this.state} 
              viewer={viewer} 
              updateSavedFilter={this.updateSavedFilter}
              updateDefaultFilter={this.updateDefaultFilter} 
              onSeeMore={this._handleSeeMore}
              _handleSearchChange={this._handleSearchChange}
              nameCompletion={this.props.nameCompletion}
            />
	        }

          {this.props.circleId &&
            <CirclePage
              circleId={this.props.circleId}
              circle={this.props.viewer.circle}
              viewer={viewer}
              activeTab={this.props.activeTab}
              unSubscribe={this.unSubscribe}
            />
          }

          {this.state.activeSection === 'allMembers' && !this.state.selectedCircle && 
            <div style={{...styles.circlesContainer, width: '100%'}}>
              <AllCircleMembers
                user={viewer.me}
                viewer={viewer}
                onChangeSection={this._handleChangeSection}
              />
            </div>
          }
          {this.state.activeSection === 'findCircles' && !this.state.selectedCircle && 
            <FindPublicCircle
              viewer={viewer}
              onChangeSection={this._handleChangeSection}
            />
          }
          {this.state.activeSection === 'myCircles' && !this.state.selectedCircle &&
            <div style={styles.circlesContainer}>
              <div style={styles.pageHeader}>
                {viewer.me ? localizations.circles_title : localizations.header_find_circles}
              </div>
              <div style={styles.circleList}>
                <div style={styles.circleNumber}>
                  {circleNb > 1 
                  ? (circleNb) + ' ' + localizations.find_circles
                  : (circleNb) + ' ' + localizations.find_circle
                  }
                </div>

                {viewer.me && 
                  <NewCircle 
                    viewer={viewer} 
                    me={viewer.me}
                    isError={this.state.isError}
                    name={this.state.name}
                    subCircles={this.state.subCircles}
                    onChange={this._handleChange}
                    updateSubCircles={this._updateSubCircles}
                    onChangeNewCircleType={this._updateNewCircleType}
                    onChangeNewCirclePrivacy={this._updateNewCirclePrivacy}
                    onChangeNewCircleInvitationWithLink={this._updateNewCircleInvitationWithLink}
                    onChangeNewCircleShared={this._updateNewCircleShared}
                    onChangeNewCircleSport={this._updateNewCircleSport}
                    onChangeNewCircleAddress={this._handleAddressChange}
                    _closeNewCircle={this._closeNewCircle}
                    onErrorChange={this._setIsError}
                    openNewCircle={() => this.setState({tutorial3aIsVisible: false})}
                    label={this.props.typeFilter && this.props.typeFilter.indexOf("CHILDREN_CIRCLES") >= 0 && this.props.typeFilter.length === 1
                      ? viewer.me && viewer.me.profileType === 'PERSON'
                        ? localizations.circles_create_child
                        : localizations.circles_create_team
                      : localizations.circles_create}
                    {...this.state}
                    teamCreation={this.props.typeFilter && this.props.typeFilter.indexOf("CHILDREN_CIRCLES") >= 0 && this.props.typeFilter.length === 1}
                    superMe={viewer.superMe}
                  />
                }

                <TutorialModal
                  isOpen={this.state.tutorial3aIsVisible}
                  tutorialNumber={4}
                  tutorialName={"team_small_tutorial4"}
                  message={localizations.team_small_tutorial3a}
                  confirmLabel={localizations.team_small_tutorial_ok}
                  onPass={() => this.setState({tutorial3aIsVisible: false})}
                  position={{
                    bottom: -125,
                    left: 50
                  }}
                  arrowPosition= {{
                    top: -8,
                    left: 130
                  }}
                />

                { circleList.map(circle =>
                  <CircleItem key={circle.node.id} 
                    circle={circle.node} 
                    viewer = { viewer }
                    link={`/circle/${circle.node.id}`}
                    openCircle={() => this.setState({tutorial3aIsVisible: false})}
                    circleIsMine={true}
                    {...this.state}>{circle.node.owner ? circle.node.name + ' ' + localizations.find_my_sport_clubs_of + ' ' + circle.node.owner.pseudo : circle.node.name}</CircleItem>
                  )
                }
                {circleNb > 3 && circleList.length < circleNb && !this.state.isQuerying && 
                  <div style={styles.showContainer}>
                    <div onClick={() => this._showMoreCircles(circleNb)} style={styles.showButton}>
                      {localizations.find_public_circle_show_more}
                    </div>
                  </div>
                }   
                {this.state.isQuerying && 
                  <div style={styles.loadingContainer}>
                    <ReactLoading type='spinningBubbles' color={colors.blue}/>
                  </div>
                }           
              </div> 
            </div>
          }

          {this.state.activeSection === 'subAccounts' && !this.state.selectedCircle &&
            <div style={styles.circlesContainer}>
              <div style={styles.pageHeader}>
                {viewer.me && viewer.me.profileType === 'PERSON' ? localizations.circles_subAccounts_children : localizations.circles_subAccounts_teams}
              </div>
              <div style={styles.circleList}>
                <NewCircle viewer={viewer} 
                  me={viewer.me}
                  superMe={viewer.superMe}
                  isError={this.state.isError}
                  name={this.state.name}
                  subCircles={this.state.subCircles}
                  onChange={this._handleChange}
                  updateSubCircles={this._updateSubCircles}
                  _closeNewCircle={this._closeNewCircle}
                  onErrorChange={this._setIsError}
                  openNewCircle={() => this.setState({tutorial3aIsVisible: false})}
                  label={viewer.me && viewer.me.profileType === 'PERSON' ? localizations.circles_create_child : localizations.circles_create_team}
                  teamCreation={true}
                  {...this.state}
                />
                { viewer.me.circlesSuperUser.edges.map(circle => 
                  <CircleItem key={circle.node.id} 
                    circle={circle.node} 
                    viewer = { viewer }
                    link={`/circle/${circle.node.id}`}
                    circleIsMine={true}
                    openCircle={() => this.setState({tutorial3aIsVisible: false})}
                    {...this.state}
                  >
                    {circle.node.name + ' ' + localizations.find_my_sport_clubs_of + ' ' + circle.node.owner.pseudo}
                  </CircleItem>
                  )
                }
              </div>
            </div>
          }
          {this.state.activeSection === 'sportClubs' && !this.state.selectedCircle &&
            <div style={styles.circlesContainer}>
              <div style={styles.pageHeader}>
                {localizations.find_my_sport_clubs}
              </div>
              <div style={styles.circleList}>
                { viewer.me.circlesUserIsIn.edges.map(circle => 
                  <CircleItem key={circle.node.id} 
                    circle={circle.node} 
                    viewer = { viewer }
                    link={`/circle/${circle.node.id}`}
                    circleIsMine={false}
                    unSubscribe={this.unSubscribe}
                    openCircle={() => this.setState({tutorial3aIsVisible: false})}
                    {...this.state}>
                    {circle.node.name + ' ' + localizations.find_my_sport_clubs_of + ' ' + circle.node.owner.pseudo}
                  </CircleItem>
                  )
                }
              </div>
            </div>
          }
          {this.state.activeSection === 'membersInfo' &&
          <div style={styles.circlesContainer}>
            <InformationForms
              user={viewer.me}
              viewer={viewer}
              language={this.state.language}
            />
          </div>
          }
          {this.state.activeSection === 'paymentModels' &&
            <div style={styles.circlesContainer}>
              <PaymentModels  
                user={viewer.me}
                viewer={viewer}
                language={this.state.language}
              />
            </div>
          }
          {this.state.activeSection === 'termOfUse' &&
            <div style={styles.circlesContainer}>
              <TermOfUse
                user={viewer.me}
                viewer={viewer}
                language={this.state.language}
              />
            </div>
          }
        </div>
        {viewer.me ? <Footer onUpdateLanguage={this._setLanguage} viewer={viewer} user={viewer.me}/> : <Footer onUpdateLanguage={this._setLanguage} viewer={viewer} user={null}/> }
      </div>
    )
  }
}

styles = {
  bodyContainer: {
    width: '100%',
    minHeight: 600,
    paddingBottom: 50,
    paddingTop: 15,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'left',
    '@media (max-width: 677px)': {
      flexDirection: 'column',
    },
  },
  circlesContainer: {
    display: 'flex',
    width: '100%',
    margin: '0px auto', 
    flexDirection: 'column',
		paddingLeft: 70,
    justifyContent: 'flex-start',
    '@media (max-width: 1024px)': {
      paddingLeft: 10,
      paddingRight: 10
    },
    '@media (max-width: 850px) and (min-width: 761px)': {
      width: '70%',
    },
  },
  circleList: {
    //margin: 'auto',
    maxWidth: '100%',
    position: 'relative'
  },
  circleNumber: {
    fontFamily: 'Lato',
    fontSize: 20,
    color: '#898c8d',
    marginLeft: 10,
  },
  pageHeader: {
		height: 41,
		fontFamily: 'Lato',
		fontSize: 34,
		fontWeight: fonts.weight.large,
		color: colors.blue,
		display: 'flex',
    maxWidth: 1400,
		margin: '30px 0px', 
    flexDirection: 'row',
		//paddingLeft: 70,
		alignItems: 'left',
    justifyContent: 'left',
    '@media (max-width: 960px)': {
      paddingLeft: 0,
      textAlign: 'center',
      display: 'block',
    }
  },
  loadingContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
    width: 600, 
    '@media (min-width: 1024px)': {
      minWidth: 600,
    },
    '@media (max-width: 1024px)': {
      width: 'auto',
    },
  },
  showContainer: {
    display: 'flex',
    margin: '25px 0px 0px 25px',
  },
  showButton: {
    fontFamily: 'Lato',
    fontSize: 18,
    cursor: 'pointer',
    color: colors.blue
  },
}

const _updateFilter = (value) => {
	return {
		type: types.UPDATE_MY_CIRCLE_FILTER,
		value,
	}
}

const _updateSportFilter = (value) => {
	return {
		type: types.UPDATE_MY_CIRCLE_SPORT_FILTER,
		value
	}
}

const _updateLocationFilter = value => {
	return {
		type: types.UPDATE_MY_CIRCLE_LOCATION_FILTER,
		value
	}
}

const _updateTypeFilter = value => {
	return {
		type: types.UPDATE_MY_CIRCLE_TYPE_FILTER,
		value
	}
}

const _updateUserFilter = value => {
	return {
		type: types.UPDATE_MY_CIRCLE_USER_FILTER,
		value
	}
}

const _updateSelectedFilters = value => {
	return {
		type: types.UPDATE_MY_CIRCLE_SELECTED_FILTERS,
		value
	}
}

const _updateNameCompletion = value => {
  return {
    type: types.UPDATE_MY_CIRCLE_NAME_COMPLETION,
    value
  }
}

const _resetFilter = () => {
	return {
		type: types.UPDATE_MY_CIRCLE_RESET_FILTER,
	}
}


const dispatchToProps = (dispatch) => ({
	_updateFilter: bindActionCreators(_updateFilter, dispatch),
	_resetFilter: bindActionCreators(_resetFilter, dispatch),
	_updateSportFilter: bindActionCreators(_updateSportFilter, dispatch),
	_updateLocationFilter: bindActionCreators(_updateLocationFilter, dispatch),
	_updateTypeFilter: bindActionCreators(_updateTypeFilter, dispatch),
	_updateUserFilter: bindActionCreators(_updateUserFilter, dispatch),
  _updateSelectedFilters: bindActionCreators(_updateSelectedFilters, dispatch),
  _updateNameCompletion: bindActionCreators(_updateNameCompletion, dispatch)
});

const stateToProps = (state) => ({
	filter: state.myCircleFilterReducer.filter,
	sportFilter: state.myCircleFilterReducer.sportFilter,
	locationFilter: state.myCircleFilterReducer.locationFilter,
	typeFilter: state.myCircleFilterReducer.typeFilter,
	userFilter: state.myCircleFilterReducer.userFilter,
  selectedFilters: state.myCircleFilterReducer.selectedFilters,
  nameCompletion: state.myCircleFilterReducer.nameCompletion,
  userLocation: state.globalReducer.userLocation,
  userCity: state.globalReducer.userCity
});

const ReduxContainer = connect(
	stateToProps,
	dispatchToProps,
)(Radium(MyCircles));

export default Relay.createContainer(withRouter(Radium(ReduxContainer)), {
  initialVariables: {
    superToken: null,
    querySuperMe: false,
    circleId: null,
    circlesSuperUserNumber: 5,
    circlesUserIsInNumber: 5,
    ownerOfCirclesNumber: 5,
	  firstCircle: 10,
	  sportNb: 10,
	  filterSport: null,
	  filterCircle: {
    	types: [],
		  modes: [],
		  sport: null,
      owners: [],
      nameCompletion: null
	  },
	  filterCircleSubAccount: {
    	types: [],
		  modes: [],
		  sport: null,
      owners: [],
      nameCompletion: null
	  },
	  queryMyCircle: false,
	  queryCirclesImIn: false,
	  querySubAccount: false,
	  queryPublicCircle: false
  },
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
        ${Header.getFragment('viewer')},
        ${Footer.getFragment('viewer')}
        ${UnsubscribeFromCircleMutation.getFragment('viewer')}
        ${CirclePage.getFragment('viewer')}
        ${AllCircleMembers.getFragment('viewer')}
        ${InformationForms.getFragment('viewer')}
        ${PaymentModels.getFragment('viewer')}
        ${NewPaymentModelMutation.getFragment('viewer')}
        ${UpdatePaymentModelMutation.getFragment('viewer')}
        ${DeletePaymentModelMutation.getFragment('viewer')}
        ${UpdateFormMutation.getFragment('viewer')}
        ${DeleteFormMutation.getFragment('viewer')}
        ${FindPublicCircle.getFragment('viewer')}
        ${NewCircle.getFragment('viewer')}
        circle(id: $circleId) {
          ${CirclePage.getFragment('circle')}
        }
        circles (first: $firstCircle, filter: $filterCircle) @include(if: $queryPublicCircle) {
          edges {
            node {
              ${CircleItem.getFragment('circle')}
              id
              name
              mode
              isCircleUpdatableByMembers
              isCircleUsableByMembers
              memberCount
              owner {
                avatar
                pseudo
              }
              type
            }
          }
          count
        }
        sports(first: $sportNb, filter: $filterSport) {
          count
          edges {
            node {
              id
              name {
                EN
                FR
                DE
              }
              logo
              levels {
                id
                EN {
                  name
                  skillLevel
                  description
                }
                FR {
                  name
                  skillLevel
                  description
                }
                DE {
                  name
                  skillLevel
                  description
                }
              }
            }
          }
        }       
        me {
          id
          profileType
          basicCircleSavedFiltersCreated
          defaultSavedCircleFilter {
            id
            filterName
            location {
              lat
              lng
              radius
            }
            sport {
              sport {
                id
                name {
                  EN
                  FR
                }
              }
            }
            circleType
            memberTypes
            modes
            owners {
              id
              pseudo
            }
          }
          savedCircleFilters {
            id
            filterName
            location {
              lat
              lng
              radius
            }
            sport {
              sport {
                id
                name {
                  EN
                  FR
                }
              }
            }
            circleType
            memberTypes
            modes
            owners {
              id
              pseudo
            }
          }
          ${Header.getFragment('user')}
          ${Footer.getFragment('user')}
          ${AllCircleMembers.getFragment('user')}
          ${InformationForms.getFragment('user')}
          ${PaymentModels.getFragment('user')}
          ${TermOfUse.getFragment('user')}
          circles(last: 100, filter: $filterCircle) @include(if:$queryMyCircle) {
            edges {
              node {
                id
                name
                type
                memberCount
                ${CircleItem.getFragment('circle')}
              }
            }
            count
          }
          circlesUserIsIn(last: $circlesUserIsInNumber, filter: $filterCircle) @include(if:$queryCirclesImIn) {
            edges {
              node {
                id
                name,
                owner {
                  pseudo
                  avatar
                }
                type
                memberCount
                ${CircleItem.getFragment('circle')}
              }
            }
            count
          }
          subAccounts {
            id ,
            pseudo ,
            circles (last: 30) {
              edges {
                node {
                  id
                  name
                  memberCount
                }
              }
            }
          }
          circlesSuperUser(last: $circlesSuperUserNumber, filter: $filterCircleSubAccount) @include(if:$querySubAccount) {
            edges {
              node {
                id, 
                name 
                owner { 
                  id
                  pseudo
                  avatar
                }
                type
                memberCount
                ${CircleItem.getFragment('circle')}
              }
            }
            count
          }
        }
        ownersOfCirclesUserIsIn(last: $ownerOfCirclesNumber) {
          count
          edges {
            node {
              id
              pseudo
            }
          }
        }
        superMe (superToken: $superToken) @include(if:$querySuperMe) {
          id,
          profileType
          userPreferences {
            areSubAccountsActivated
          }
        }
      }
    `,
  },
});