import React from 'react'
import Relay from  'react-relay'
import Radium from 'radium'
import Loading from 'react-loading';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import CircleItem from "./FindCircleItem";
import localizations from '../Localizations'
import CircleSelect from "./CircleSelect";
import SportSelect from  './SportSelect'
import Geosuggest from 'react-geosuggest'
import {colors, fonts} from '../../theme'
import SportLevels from '../FindSportunity/SportLevels'
import CircleSuggestion from './CircleSuggestion';

let styles;

class FindPublicCircle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      type: null,
      sport: null,
      location: null,
      levelFrom: null,
      levelTo: null,
      levels: null,
      name: '',
      circleNb: 3,
      isLoading: true
    };
  }
  
  componentDidMount() {
    this.props.relay.setVariables({
      query: true
    },readyState => {
      if (readyState.done) {
        setTimeout(() => {
          this.setState({isLoading: false})
        }, 50);
      }
    })
  }

  componentDidMount() {
    fetch('https://ipapi.co/json')
    .then(res => res.json())
    .then(json => {
      if (json.lat && json.lon) {
        this._locationSelected({label: json.city + ', ' + json.country_name, location: {lat: json.latitude, lng: json.longitude}})
      }
    })  
  }

  _translatedName = (name) => {
    let translatedName = name.EN;
    switch(localizations.getLanguage().toLowerCase()) {
      case 'en':
        translatedName = name.EN;
        break;
      case 'fr':
        translatedName = name.FR || name.EN;
        break;
      case 'it':
        translatedName = name.IT || name.EN;
        break;
      case 'de':
        translatedName = name.DE || name.EN;
        break;
      default:
        translatedName = name.EN;
        break
    }
    return translatedName
  };

  _toggleShowMore = (count) => {
    this.setState({
      circleNb: this.state.circleNb + 10,
    }, this._updateCircleFilter)
  };


  _updateCircleFilter = (callBack) => {
    this.props.relay.setVariables({
      filterCircle: {
        location: (!this.state.location) ? null : {
          lat: this.state.location.location.lat,
          lng: this.state.location.location.lng,
          radius: 50,
        },
        sport: (this.state.sport) ? {
          sportID: this.state.sport.id,
          level: this.state.levels,
        } : null,
        type: (this.state.type) ? this.state.type.key : null,
        nameCompletion: this.state.name,
      },
      query: true,
      firstCircle: this.state.circleNb,
    },readyState => {
      if (readyState.done) {
        if (typeof callBack !== 'undefined')
          setTimeout(() => {
            callBack()
          }, 50);
      }
    });
  };

  _updateSportFilter = (value) => {
    this.props.relay.setVariables({
      filterSport: {
        name: value,
        language: localizations.getLanguage().toUpperCase(),
      },
    })
  };

  _updateType = (type) => {
    this.setState({
      type: type,
    }, this._updateCircleFilter)
  };

  _updateSport = (sport) => {
    this.setState({
      sport: sport,
    }, () => {
      if(this.state.sport && this.state.sport.levels)
        this._updateLevelRange(null, null);
      else
        this._updateCircleFilter();
    })
  };

  _handleLoadAllSports = () => {
    this.props.relay.setVariables({ sportNb: this.props.viewer.sports.count, filterSport: { name: '' , language: localizations.getLanguage().toUpperCase() }, });
    this.setState({
      allSportsLoaded: true,
    })
  };

  _locationSelected = (location) => {
    this.setState({
      location: location
    }, this._updateCircleFilter)
  };

  _setLevelRange = (range) =>
  {
    this.setState({
      levels: range
    }, this._updateCircleFilter)
  };

  _updateLevelRange = (levelFrom, levelTo) => {
    const levels = this.state.sport.levels;
    let selectedLevels = null;
    if (levelFrom && levelTo)
    {
      let fromIndex = levels.findIndex((e) => e.id === levelFrom.value);
      let toIndex = levels.findIndex((e) => e.id === levelTo.value);
      selectedLevels = levels.slice(fromIndex, toIndex+1)
    }
    else
      selectedLevels = levels;
    
    this._setLevelRange(selectedLevels.map(level => level.id));
  };

  _setLevelFrom = (value) => {
    this.setState({
      levelFrom: value,
    });
    this._updateLevelRange(value, this.state.levelTo)
  };

  _setLevelTo = (value) => {
    this.setState({
      levelTo: value,
    });
    this._updateLevelRange(this.state.levelFrom, value)
  };

  _onSearchName = (e, callBack) => {
    this.setState({
      name: e,
    }, this._updateCircleFilter(callBack));
  };

  render()
  {
    let sportsList =
      this.props.viewer.sports.edges.map(({node}) => ({...node, name: this._translatedName(node.name), value: node.id}));
    let typeList = [{key: 'ADULTS', name: localizations.circles_member_type_0},
      {key: 'CHILDREN', name: localizations.circles_member_type_1},
      {key: 'TEAMS', name: localizations.circles_member_type_2},
      {key: 'CLUBS', name: localizations.circles_member_type_3},
      {key: 'COMPANIES', name: localizations.circles_member_type_4},
    ];

    let levelList = (this.state.sport) ? this.state.sport.levels : [];
    let response = (this.props.viewer.circles) ? this.props.viewer.circles.edges : [];
    
    let circleNb = (this.props.viewer.circles) ? this.props.viewer.circles.count : 0;
    
    return (
      <section style={styles.container}>
        <div style={styles.header}>{localizations.find_public_circle_title}</div>
        <div style={styles.searchBar}>
          <div style={styles.wrapper}>
            <label style={styles.label}>{localizations.type_circle}</label>
            <div style={styles.inputContainer}>
              <CircleSelect
                list={typeList}
                onSelectItem={e => this._updateType(e)}
                isDisabled={false}
                placeholder={localizations.find_circleHolder}
                selectedItem={this.state.type}
              />
            </div>
          </div>
          <div style={styles.wrapper}>
            <label style={styles.label}>{localizations.find_city}</label>
            <div style={styles.inputContainer}>
              <Geosuggest
                style={inputStyles}
                placeholder={localizations.find_cityHolder}
                onSuggestSelect={this._locationSelected}
                initialValue={this.state.location ? this.state.location.label : null}
                location={this.props.userLocation}
                radius={50000}
              />
            </div>
          </div>
        </div>
        <div style={styles.searchBar}>
        <div style={styles.wrapper}>
            <label style={styles.label}>{localizations.find_sport}</label>
            <div style={styles.inputContainer}>
              <SportSelect
                label={localizations.find_sport}
                onChange={this._updateSport}
                onSearching={this._updateSportFilter}
                list={sportsList}
                placeholder={localizations.find_sportHolder}
                onLoadAllClick={this._handleLoadAllSports}
                allSportLoaded={this.state.allSportsLoaded}
                loadingAllSports={this.props.relay.pendingVariables}
                value={this.state.sport}
                //isError={isSportError}
              />
            </div>
          </div>
          <div style={styles.wrapper}>
            <labels style={styles.label}>{localizations.find_levels}</labels>
            <div style={styles.inputContainer}>
              <SportLevels
                label={localizations.find_levels}
                list={levelList}
                from={this.state.levelFrom}
                to={this.state.levelTo}
                placeholder={!this.props.sport ? localizations.newSportunity_levelHolderBefore : localizations.newSportunity_levelHolder}
                onFromChange={this._setLevelFrom}
                onToChange={this._setLevelTo}
                disabled={levelList.length === 0}
              />
            </div>
          </div>
        </div>
        <div style={styles.contentContainer}>
          { (circleNb > 0 || this.state.name !== '') &&
            <div style={styles.content}>
              <div style={styles.row}>
                <div style={styles.circleNumber}>{(circleNb) + ' ' + localizations.find_circle}</div>
                <div>
                  <CircleSuggestion
                    suggestion={response}
                    onChange={(e, callBack) => this._onSearchName(e, callBack)}
                    value={this.state.name}
                  />
                </div>
              </div>
              <div style={styles.circles}>
                {response && response.map((circleItem, index) =>
                  <CircleItem
                    key={index}
                    circle={circleItem.node}
                    link={`/circle/${circleItem.node.id}`}
                    viewer={this.props.viewer}
                  />
                )}
              </div>
              {circleNb > 3 && this.state.circleNb < circleNb && 
              <div style={styles.showContainer}>
                <div onClick={() => this._toggleShowMore(circleNb)}
                     style={styles.showButton}>{localizations.find_public_circle_show_more}</div>
              </div>
              }
            </div>
          }
          {this.state.isLoading && 
            <div style={styles.loadingContainer}><Loading type='spinningBubbles' color={colors.blue} /></div>
          }
          { !this.state.isLoading && circleNb === 0 && this.state.name === '' &&
            <div style={styles.emptyContainer}>
              <div style={{height: '5em'}}>
                <img src="/assets/images/icon_circle@3x.png"/>
                <div style={styles.timeContainer}>
                  <i
                    style={{color: colors.red}}
                    className='fa fa-times fa-5x'
                  />
                </div>
              </div>
              <div style={styles.errorMessages}>
                {localizations.find_public_circle_empty_1}
              </div>
              <div style={styles.errorMessages}>
                {localizations.find_public_circle_empty_2['0']}
                <span style={{color: colors.blue}} onClick={() => this.props.onChangeSection('myCircles')}>{localizations.find_public_circle_empty_2['1']}</span>
                {localizations.find_public_circle_empty_2['2']}
              </div>
            </div>
          }
        </div>
      </section>
    )
  }
}

let  inputStyles = {
  'input': {
    borderWidth: 0,
    borderBottomWidth: 2,
    borderStyle: 'solid',
    borderColor: colors.blue,
    height: '30px',
    lineHeight: '36px',
    fontFamily: 'Lato',
    display: 'block',
    background: 'transparent',
    fontSize: fonts.size.medium,
    outline: 'none',
    //marginLeft: 20,
    marginRight: 10,
    paddingRight: 20,
  },
  'suggests': {
    // width: '100%',
    width: 300,
    position: 'absolute',
    backgroundColor: colors.white,

    boxShadow: '0 2px 4px 0 rgba(0,0,0,0.24), 0 0 4px 0 rgba(0,0,0,0.12)',
    border: '2px solid rgba(94,159,223,0.83)',
    padding: 20,
    zIndex: 100,
  },
  'suggests--hidden': {
    width: '0',
    display: 'none',
  },
  'suggestItem': {
    paddingTop: 10,
    paddingBottom: 10,
    color: '#515151',
    fontSize: 18,
    fontWeight: 500,
    fontFamily: 'Helvetica Neue',
  },

};

styles = {
  emptyContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  timeContainer: {
    position: 'relative',
    top: '-45px',
    left: '8px',
    width: 24,
    textAlign: 'center'
  },
  errorMessages: {
    fontFamily: 'Lato',
    fontSize: 15,
    color: colors.gray,
    textAlign: 'center',
    marginTop: 10,
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'baseline'
  },
  container: {
    margin: 15,
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    '@media (min-width: 761px) and (max-width: 860px)': {
      maxWidth: 500
    },
  },
  circles: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  header: {
    fontFamily: 'Lato',
    fontSize: 34,
    color: 'rgb(94, 159, 223)',
    marginBottom: 15,
  },
  content: {
    width: '95%',
    padding: 10,
    border: '1px solid #E7E7E7',
    boxShadow: 'rgba(0, 0, 0, 0.12) 0px 0px 4px 0px',
  },
  contentContainer: {
    width: '100%',
    justifyContent: 'center',
  },
  searchBar: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'baseline',
    justifyContent: 'flex-start',
    '@media (max-width: 900px)': {
      flexDirection: 'column'
    }
  },
  circleNumber: {
    fontFamily: 'Lato',
    fontSize: 20,
    color: '#898c8d',
    marginLeft: 10,
  },
  showContainer: {
    display: 'flex',
    justifyContent: 'flex-end',
    margin: 5,
  },
  showButton: {
    fontFamily: 'Lato',
    fontSize: 16,
    cursor: 'pointer',
    color: colors.blue
  },
  label: {
    fontSize: 16,
    margin: 10,
    color: '#5e9fdf',
    fontFamily: 'Lato',
    width: 100
    /*flex: 1, 
    display: 'flex',
    justifyContent: 'flex-end'*/
  },
  inputContainer: {
    //flex: 1
  },
  wrapper: {
    display: 'flex',
    flexDirection: 'row',
    marginBottom: 10,
    flex: 1,
    justifyContent: 'flex-start',
  },
  loadingContainer: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: 50
  }
};

const dispatchToProps = (dispatch) => ({
})
  
const stateToProps = (state) => ({
    userLocation: state.globalReducer.userLocation,
})

let ReduxContainer = connect(
    stateToProps,
    dispatchToProps
)(Radium(FindPublicCircle));

export default Relay.createContainer(ReduxContainer,{
  initialVariables: {
    firstCircle: 3,
    sportNb: 10,
    filterSport: null,
    filterCircle: {
      location: null,
      sport: null,
      type: null,
      nameCompletion: null
    },
    query: false,
  },
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        circles (first: $firstCircle, filter: $filterCircle) @include(if: $query) {
          edges {
            node {
              ${CircleItem.getFragment('circle')}
              id
              name
              mode
              isCircleUpdatableByMembers
              isCircleUsableByMembers
              memberCount
              owner {
                avatar
                pseudo
              }
              type
            }
          }
          count
        }
        sports(first: $sportNb, filter: $filterSport) {
          count
          edges {
            node {
              id
              name {
                EN
                FR
                DE
              }
              logo
              levels {
                id
                EN {
                  name
                  skillLevel
                  description
                }
                FR {
                  name
                  skillLevel
                  description
                }
                DE {
                  name
                  skillLevel
                  description
                }
              }
            }
          }
        }
      }
    `,
  },
});