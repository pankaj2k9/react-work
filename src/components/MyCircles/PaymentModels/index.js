import React from 'react'
import Relay from 'react-relay'
import Radium from 'radium';
import Modal from 'react-modal';
import AlertContainer from 'react-alert';
import Link from 'react-router/lib/Link'

import RelayStore from '../../../RelayStore.js'
import { colors, fonts, metrics } from '../../../theme'
import localizations from '../../Localizations'

import PaymentModelModal from './PaymentModelModal';
import NewPaymentModelMutation from './NewPaymentModelMutation';
import UpdatePaymentModelMutation from './UpdatePaymentModelMutation';
import DeletePaymentModelMutation from './DeletePaymentModelMutation';

let styles
let modalStyles

class PaymentModels extends React.Component {
    constructor(props) {
        super(props)

        this.alertOptions = {
            offset: 14,
            position: 'top right',
            theme: 'light',
            transition: 'fade',
            time: 0,
        };

        this.state = {
            language: localizations.getLanguage(),
            isNewPaymentModelVisible: false,
            deleteModalOpen: false,
            paymentModelToDelete: null,
            isEditPaymentModelVisible: false,
            paymentModelToEdit: null,
            isSaving: false
        }
    }

    componentDidMount = () => {
        this.props.relay.setVariables({
            query: true
        },readyState => {
            if (readyState.done) {
              setTimeout(() => {
                this.props.relay.forceFetch();
              }
              , 50);
            }
          }
        )
    }

    componentWillReceiveProps = (nextProps) => {
    }

    _showNewPaymentModel = () => {
        this.setState({
            isNewPaymentModelVisible: true
        })
    }

    _handleCloseNewPaymentModel = () => {
        this.setState({
            isNewPaymentModelVisible: false
        })
    }

    _handleSaveNewPaymentModel = (paymentModel) => {
        const viewer = this.props.viewer ;
        this.setState({isSaving: true})

        RelayStore.commitUpdate(
            new NewPaymentModelMutation({
                viewer,
                paymentModelVar: {
                    name: paymentModel.name,
                    conditions: paymentModel.conditions, 
                    price: paymentModel.price,
                    circles: paymentModel.circles
                }
            }),
            {
            onFailure: error => {
                this.msg.show(localizations.popup_editCircle_update_failed, {
                    time: 2000,
                    type: 'error',
                });
                let errors = JSON.parse(error.getError().source);
                console.log(errors);
                this.setState({isSaving: false})
                let that = this ;
                setTimeout(function() {
                    that.msg.removeAll();
                }, 2000);
                
            },
            onSuccess: (response) => {
                this.msg.show(localizations.popup_editCircle_update_success, {
                    time: 2000,
                    type: 'success',
                });
                this.setState({
                    isSaving: false,
                    isNewPaymentModelVisible: false,
                })
                let that = this ;
                setTimeout(function() {
                    that.msg.removeAll();
                }, 2000);
            },
            }
        )
    }

    _handleEditPaymentModel = paymentModel => {
        this.setState({
            isEditPaymentModelVisible: true,
            paymentModelToEdit: paymentModel
        })
    }

    _handleCloseEditPaymentModel = () => {
        this.setState({
            isEditPaymentModelVisible: false,
            paymentModelToEdit: null
        })
    }

    _handleSaveEditPaymentModel = (paymentModel) => {
        const viewer = this.props.viewer ;
        this.setState({isSaving: true})

        RelayStore.commitUpdate(
            new UpdatePaymentModelMutation({
                viewer,
                paymentModelVar: {
                    id: paymentModel.id, 
                    name: paymentModel.name,
                    conditions: paymentModel.conditions, 
                    price: paymentModel.price,
                    circles: paymentModel.circles
                }
            }),
            {
            onFailure: error => {
                this.msg.show(localizations.popup_editCircle_update_failed, {
                    time: 2000,
                    type: 'error',
                });
                let errors = JSON.parse(error.getError().source);
                console.log(errors);
                let that = this ;
                this.setState({isSaving: false})
                setTimeout(function() {
                    that.msg.removeAll();
                }, 2000);
            },
            onSuccess: (response) => {
                this.msg.show(localizations.popup_editCircle_update_success, {
                    time: 2000,
                    type: 'success',
                });
                this.setState({
                    isEditPaymentModelVisible: false,
                    paymentModelToEdit: null,
                    isSaving: false
                })
                let that = this ;
                setTimeout(function() {
                    that.msg.removeAll();
                }, 2000);
            },
            }
        )
    }

    _handleOnRemove = (paymentModel) => {
        this.setState({
            deleteModalOpen: true,
            paymentModelToDelete: paymentModel
        })
    }

    _closeModal = () => {
        this.setState({
            deleteModalOpen: false,
            paymentModelToDelete: null
        })
    }

    _confirmDelete = () => {
        const viewer = this.props.viewer ;
        
        RelayStore.commitUpdate(
            new DeletePaymentModelMutation({
                viewer,
                paymentModelIdVar: this.state.paymentModelToDelete.id
            }),
            {
            onFailure: error => {
                this.msg.show(localizations.popup_editCircle_update_failed, {
                    time: 2000,
                    type: 'error',
                });
                let errors = JSON.parse(error.getError().source);
                console.log(errors);
                setTimeout(() => {
                    this.msg.removeAll();
                }, 2000);
                
            },
            onSuccess: (response) => {
                this.msg.show(localizations.popup_editCircle_update_success, {
                    time: 2000,
                    type: 'success',
                });
                this.setState({
                    deleteModalOpen: false,
                    paymentModelToDelete: null
                })
                setTimeout(() => {
                    this.msg.removeAll();
                }, 2000);
            },
            }
        )
    }

    render() {
        const { viewer, user } = this.props
    
        return(
        <div style={{width: '100%'}}>
            <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
            <div style={styles.pageHeader}>
                {localizations.circles_paymentModels}
            </div>
            
            <div style={styles.wrapper}>
                {user && user.bankAccount 
                ?   <div style={styles.bodyContainer}>
                        <div style={styles.button} onClick={this._showNewPaymentModel}>
                            {localizations.circles_paymentModel_new}
                        </div>

                        {user.circlePaymentModels && user.circlePaymentModels.length > 0 && 
                            <div style={styles.memberList} >
                                <div style={styles.tableRowHeader}>
                                    <div style={styles.tableRowHeaderText}>
                                        {localizations.circles_paymentModel_name}
                                    </div>
                                    <div style={styles.tableRowHeaderCircleText}>
                                        {localizations.circles_paymentModel_circles_applied}
                                    </div>
                                    <div style={{flex: 2}}/>
                                </div>
                                { user.circlePaymentModels.map((paymentModel, index) => (
                                    <div style={styles.tableRow} key={paymentModel.id} >
                                        <div style={styles.tableRowText}>
                                            {paymentModel.name}
                                        </div>
                                        <div style={styles.tableRowCircleText}>
                                            {paymentModel.circles && paymentModel.circles.edges && paymentModel.circles.edges.length > 0
                                            ?   paymentModel.circles.edges.map(edge => (
                                                    edge.node.owner.id === user.id ? edge.node.name : edge.node.name + ' (' + edge.node.owner.pseudo + ')'
                                                )).join(', ')
                                            :   '-'
                                            }
                                        </div>
                                        <div style={styles.buttonContainer}>
                                            <div style={styles.icon} onClick={() => this._handleEditPaymentModel(paymentModel)}>
                                                <i key={"edit"+index} className="fa fa-pencil" style={styles.iconEdit}></i>
                                            </div>
                                            <div style={styles.icon} onClick={() => this._handleOnRemove(paymentModel)}>
                                                <i key={"delete"+index} className="fa fa-times" style={styles.iconRemove}></i>
                                            </div>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        }
                    </div> 
                :   <div style={styles.completeAccountContainer}>
                        <div style={styles.explanation}>
                            {localizations.circles_paymentModel_new_missing_bankAccount}
                            <Link to={'/my-info'} style={{color: colors.blue, textDecoration: 'none'}}>
                                {localizations.circles_paymentModel_new_missing_bankAccount_linkLabel}
                            </Link>
                        </div>
                        <div style={styles.completeAccountDetailsContainer}>
                            <div style={styles.completeAccountDetailsCol}>
                                <div style={styles.completeAccountDetailsText}>
                                    {localizations.circles_paymentModel_new_missing_bankAccount_expl1}
                                </div>
                                {localizations.getLanguage().toUpperCase() === 'FR'
                                ?	<img src="/assets/images/new_payment_model-FR.png" style={styles.completeAccountImage}/>
                                :	<img src="/assets/images/new_payment_model-EN.png" style={styles.completeAccountImage}/>
                                }
                            </div>
                            <div style={styles.completeAccountDetailsCol}>
                                <div style={styles.completeAccountDetailsText}>
                                    {localizations.circles_paymentModel_new_missing_bankAccount_expl2}
                                </div>
                                {localizations.getLanguage().toUpperCase() === 'FR'
                                ?	<img src="/assets/images/payment_model-FR.png" style={styles.completeAccountImage}/>
                                :	<img src="/assets/images/payment_model-EN.png" style={styles.completeAccountImage}/>
                                }
                            </div>
                        </div>
                        <div style={styles.explanation}>
                            {localizations.circles_paymentModel_new_missing_bankAccount_see_tutorial}
                            <Link to={'/faq/tutorial/paiement-des-cotisations'} style={{color: colors.blue, textDecoration: 'none'}}>
                                {localizations.circles_paymentModel_new_missing_bankAccount_see_tutorial2}
                            </Link>
                        </div>
                    </div>
                }
            </div>


            {this.state.isNewPaymentModelVisible &&
                <PaymentModelModal 
                    isModalVisible={this.state.isNewPaymentModelVisible}
                    viewer={viewer}
                    user={user}
                    onSave={this._handleSaveNewPaymentModel}
                    onClose={this._handleCloseNewPaymentModel}
                    isSaving={this.state.isSaving}
                />
            }

            {this.state.isEditPaymentModelVisible &&
                <PaymentModelModal 
                    isModalVisible={this.state.isEditPaymentModelVisible}
                    viewer={viewer}
                    user={user}
                    paymentModelToEdit={this.state.paymentModelToEdit}
                    onSave={this._handleSaveEditPaymentModel}
                    onClose={this._handleCloseEditPaymentModel}
                    isSaving={this.state.isSaving}
                />
            }
            
            <Modal
                isOpen={this.state.deleteModalOpen}
                onRequestClose={this._closeModal}
                style={modalStyles}
                contentLabel={localizations.circles_paymentModel_delete}
            >
                <div style={styles.modalContent}>
                    <div style={styles.modalHeader}>
                        <div style={styles.modalContent}>
                            <div style={styles.modalText}>
                                {localizations.circles_paymentModel_delete}
                            </div>
                            <div style={styles.modalExplanation}>
                                {localizations.circles_paymentModel_delete_explanation}
                            </div>
                            <div style={styles.modalButtonContainer}>
                                <button style={styles.submitButton} onClick={this._confirmDelete}>{localizations.circle_yes}</button>
                                <button style={styles.cancelButton} onClick={this._closeModal}>{localizations.circle_no}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </Modal>
        </div>

        )
    }
}

styles = {
    pageHeader: {
		fontFamily: 'Lato',
		fontSize: 34,
		fontWeight: fonts.weight.large,
		color: colors.blue,
		display: 'flex',
        maxWidth: 1400,
		margin: '30px auto 0px auto',
        flexDirection: 'row',
		alignItems: 'left',
        justifyContent: 'left',
        '@media (max-width: 900px)': {
            flexDirection: 'column',
            marginBottom: 0
        },
        '@media (max-width: 768px)': {
            paddingLeft: 20
        }
    },
    bodyContainer: {
        display: 'flex',
        width: '100%',
        margin: '0px 0 50px 0', 
        flexDirection: 'column',
		justifyContent: 'flex-start',
        minHeight: 600,
        padding: '0 15px'
    },
    button: {
        fontFamily: 'Lato',
        fontSize: 18,
        color: colors.blue,
        cursor: 'pointer',
        textAlign: 'left',
        padding: '0 15px',
        position: 'relative'
    },	
    wrapper: {
        margin: '35px auto',
        display: 'flex',
        flexDirection: 'row',
        fontFamily: 'Lato',
        '@media (max-width: 960px)': {
            width: '100%',
        },
        '@media (max-width: 580px)': {
            display: 'block',
        }
    },
    memberList: {
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'flex-start',
		marginTop: 15,
		width: '100%',
		padding: 0,
        flexWrap: 'wrap',
        '@media (max-width: 1070px)': {
			justifyContent: 'center'
		}
    },
    memberListRow: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        marginTop: 15,
        width: '100%',
        padding: 0,
        flexWrap: 'wrap',
    },
    tableRowHeader: {
        width: '100%',
        height: 50,
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: colors.white,
        boxShadow: '0 0 4px 0 rgba(0,0,0,0.12)',
        border: '1px solid #E7E7E7',
        overflow: 'hidden',
        fontFamily: 'Lato',
        margin: '1px 0',
        padding: 15,
        textDecoration: 'none',
        justifyContent: 'space-between',
        alignItems: 'center',
        '@media (max-width: 768px)': {
          width: 'auto'
        }
    },
    tableRowHeaderText: {
        flex: 3,
        marginRight: 10,
        fontWeight: 'bold',
        fontSize: 16,
        color: 'rgba(0,0,0,0.65)'
    },
    tableRowHeaderCircleText: {
        flex: 8,
        marginRight: 10,
        fontWeight: 'bold',
        fontSize: 16,
        color: 'rgba(0,0,0,0.65)'
    },
    tableRow: {
        width: '100%',
        height: 50,
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: colors.white,
        boxShadow: '0 0 4px 0 rgba(0,0,0,0.12)',
        border: '1px solid #E7E7E7',
        overflow: 'hidden',
        fontFamily: 'Lato',
        margin: '1px 0',
        padding: 15,
        textDecoration: 'none',
        justifyContent: 'space-between',
        alignItems: 'center',
        color: '#A6A6A6',
        transition: 'all cubic-bezier(0.22,0.61,0.36,1) .15s',
        ':hover': {
            backgroundColor: '#F1F1F1',
            color: '#B6B6B6',
        },
        '@media (max-width: 768px)': {
          width: 'auto'
        }
    },
    tableRowText: {
        flex: 3,
        marginRight: 10,
        fontWeight: 'bold',
        fontSize: 16,
    },
    tableRowCircleText: {
        flex: 8,
        marginRight: 10,
        fontSize: 16,
    },
    buttonContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        flex: 2,
        justifyContent: 'flex-end'
    },
    icon: {
		//flex: 1,
		fontSize: 24,
		cursor: 'pointer',
        textAlign: 'end',
        marginLeft: 10
    },
    iconRemove: {
        color: '#A6A6A6',
        ':hover': {
            color: colors.redGoogle
        }
    },
    iconEdit: {
        color: colors.blueLight,
        ':hover': {
            color: colors.blue
        }
    },
    modalContent: {
		display: 'flex',
		flexDirection: 'column',
        justifyContent: 'flex-start',
        width: 300,
		padding: 10,'@media (max-width: 768px)': {
			width: 'auto'
		}
	},
	modalHeader: {
		display: 'flex',
		flexDirection: 'row',
        alignItems: 'flex-center',
		justifyContent: 'flex-center',
	},
	modalTitle: {
		fontFamily: 'Lato',
		fontSize:24,
		fontWeight: fonts.weight.medium,
		color: colors.blue,
		marginBottom: 20,
		flex: '2 0 0',
	},
	modalClose: {
		justifyContent: 'flex-center',
		paddingTop: 10,
		color: colors.gray,
		cursor: 'pointer',
	},
	modalText: {
		fontSize: 18,
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'center',
		width: '100%',
		fontFamily: 'Lato',
	},
	modalExplanation: {
		fontSize: 16,
		color: colors.gray,
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'center',
		width: '100%',
		fontFamily: 'Lato',
		fontStyle: 'italic',
		marginTop: 10,
		textAlign: 'justify'
	},
	modalButtonContainer: {
		fontSize: 18,
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'center',
		width: '100%',
		fontFamily: 'Lato',
		marginTop: 30,
	},
	submitButton: {
		width: 80,
		backgroundColor: colors.blue,
        color: colors.white,
        fontSize: fonts.size.small,
        borderRadius: metrics.radius.tiny,
        outline: 'none',
		border: 'none',
		padding: '10px',
		cursor: 'pointer',
		boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
		margin: 10,
	},
  	cancelButton: {
		width: 80,
		backgroundColor: colors.gray,
        color: colors.white,
        fontSize: fonts.size.small,
        borderRadius: metrics.radius.tiny,
        outline: 'none',
		border: 'none',
		padding: '10px',
		cursor: 'pointer',
		boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
		margin: 10,
    },
    explanation: {
		fontSize: 16,
		color: colors.gray,
		fontFamily: 'Lato',
		fontStyle: 'italic',
		marginTop: 10,
		textAlign: 'justify'
    },
    completeAccountContainer: {
        display: 'flex',
        width: '100%',
        margin: '0px 0 50px 0', 
        flexDirection: 'column',
		justifyContent: 'flex-start',
        minHeight: 600,
        padding: '0 15px',
        alignItems: 'center'
    },
    completeAccountImage: {
		maxWidth: '90%',
        height: 'auto',
        maxHeight: 450,
        marginTop: 25
    },
    completeAccountDetailsContainer: {
        display: 'flex',
        flexDirection: 'row',
        width: '100%',
        border: '1px solid ' + colors.lightGray,
        borderRadius: 5,
        marginTop: 40,
        padding: '10px 15px',
        '@media (max-width: 740px)': {
            flexDirection: 'column',
        }
    },
    completeAccountDetailsCol: {
        display: 'flex',
        flexDirection: 'column',
        flex: 1, 
        alignItems: 'center',
        margin: '10px 20px',
        '@media (max-width: 740px)': {
            margin: 20
        }
    },
    completeAccountDetailsText: {
        fontSize: 16,
		color: colors.gray,
		fontFamily: 'Lato',
		marginTop: 10,
		textAlign: 'justify'
    },
}

modalStyles = {
    overlay : {
        position          : 'fixed',
        top               : 0,
        left              : 0,
        right             : 0,
        bottom            : 0,
        backgroundColor   : 'rgba(255, 255, 255, 0.75)',
    },
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)',
        border                     : '1px solid #ccc',
        background                 : '#fff',
        overflow                   : 'auto',
        WebkitOverflowScrolling    : 'touch',
        borderRadius               : '4px',
        outline                    : 'none',
        padding                    : '20px',
    },
}

export default Relay.createContainer(Radium(PaymentModels), {
  initialVariables: {
    query: false,
  },
  fragments: {
    viewer: () => Relay.QL`
        fragment on Viewer {
            id
        }
    `,
    user: () => Relay.QL`
        fragment on User {
            id
            bankAccount {
                IBAN
            }
            circles (last: 25) @include (if: $query) {
                edges {
                    node {
                        id,
                        name,
                        type
                        askedInformation {
                            id, 
                            name,
                            type,
                            filledByOwner
                        }
                        memberCount
                    }
                }
            }
            circlesSuperUser(last: 60) @include (if: $query) {
                edges {
                    node {
                        id,
                        name,
                        type
                        askedInformation {
                            id, 
                            name,
                            type,
                            filledByOwner
                        }
                        memberCount
                        owner {
                            id
                            pseudo
                            avatar
                        }
                    }
                }
            }
            circlePaymentModels @include (if: $query) {
                id,
                name, 
                conditions {
                    id,
                    name,
                    price {
                        cents,
                        currency
                    },
                    conditions {
                        askedInformation {
                            id,
                            name,
                            type,
                            filledByOwner
                        }
                        askedInformationComparator
                        askedInformationComparatorValue
                        askedInformationComparatorDate
                    }
                }
                price {
                    cents,
                    currency
                },
                circles (last: 20) {
                    edges {
                        node {
                            id,
                            name
                            askedInformation {
                                id, 
                                name,
                                type,
                                filledByOwner
                            }
                            owner {
                                id
                                pseudo
                                avatar
                            }
                        }
                    }
                }
            }
        }
      `,
    },
});
