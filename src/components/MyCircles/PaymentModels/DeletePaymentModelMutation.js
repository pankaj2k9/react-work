import Relay from 'react-relay';

export default class deletePaymentModelMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      deleteCirclePaymentModel
    }`
  }
  
  getVariables() {
    return  {
        paymentModelId: this.props.paymentModelIdVar
    }
  }

  getFatQuery() {
      return Relay.QL`
        fragment on deleteCirclePaymentModelPayload {
          clientMutationId,
          viewer {
            id,
            me {
              circlePaymentModels
            }
          }
        }
      `
   
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
        fieldIDs: {
          viewer: this.props.viewer.id,
        },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        me {
          id
          circlePaymentModels  {
            id,
            name, 
            conditions {
                id,
                name,
                price {
                    cents,
                    currency
                },
                conditions {
                    askedInformation {
                        id,
                        name,
                        type,
                        filledByOwner
                    }
                    askedInformationComparator
                    askedInformationComparatorValue
                    askedInformationComparatorDate
                }
            }
            price {
                cents,
                currency
            },
            circles (last: 20) {
                edges {
                    node {
                        id,
                        name
                        askedInformation {
                            id, 
                            name,
                            type,
                            filledByOwner
                        }
                        owner {
                            id
                            pseudo
                            avatar
                        }
                    }
                }
            }
          }
        }
        
      }
    `,
  };

}
