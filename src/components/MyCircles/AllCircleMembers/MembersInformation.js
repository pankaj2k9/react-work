import React from 'react'
import Relay from 'react-relay'
import Radium from 'radium'
import Modal from 'react-modal'
import AlertContainer from 'react-alert'

import RelayStore from '../../../RelayStore.js'
import InputText from '../InputText'
import {styles, modalStyles} from './style'
import localizations from '../../Localizations'
//import UpdateAskedInformationMutation from './UpdateAskedInformationMutation.js'
import MembersDetailledInformation from './MembersDetailledInformation.js';
import {confirmModal} from '../../common/ConfirmationModal'

const backendTypeList = [null, "TEXT", "NUMBER", "BOOLEAN"]

class MembersInformation extends React.Component {
    constructor(props) {
        super(props)

        this.alertOptions = {
            offset: 14,
            position: 'top right',
            theme: 'light',
            transition: 'fade',
            time: 0,
        };
    }

    render() {
        
        const { viewer, rows, columns, user } = this.props;

        return (
            <div style={styles.container}>
                <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />

                <div style={styles.buttonRow}>
                    <div style={styles.buttonLabel} onClick={this.props.onLeave}>
                        {localizations.back}
                    </div>
                </div>

                <MembersDetailledInformation
                    viewer={viewer}
                    rows={rows}
                    user={user}
                    columns={columns}
                />
            </div>
        )
    }
}

export default Relay.createContainer(Radium(MembersInformation), {
    fragments: {
      viewer: () => Relay.QL`
        fragment on Viewer {
          id
          ${MembersDetailledInformation.getFragment('viewer')}
        }
      `
    }
  });