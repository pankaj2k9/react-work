import Relay from 'react-relay';

export default class SaveCircleFilterMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      upUser
    }`;
  }

  getVariables() {
    return {
      userID: this.props.userIDVar,
      user: {
        savedCircleFilters: this.props.savedFiltersVar,
        basicCircleSavedFiltersCreated: this.props.basicCircleSavedFiltersCreatedVar
      }
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on upUserPayload {
        clientMutationId,
        user {
          savedCircleFilters 
        }
      }
    `;
  }

  getConfigs() {
    return [{
        type: 'FIELDS_CHANGE',
        fieldIDs: {
            user: this.props.userIDVar,
        },
    }];
  }

  static fragments = {
    user: () => Relay.QL`
      fragment on User {
        id
        savedCircleFilters {
						id
            filterName
            location {
              lat
              lng
              radius
            }
            sport {
              sport {
                id
                name {
                  EN
                  FR
                }
              }
            }
            circleType
            memberTypes
            modes
            owners {
              id
              pseudo
            }
				}
      }
    `,
  };

}
