import Relay from 'react-relay';

export default class RemoveCircleMember extends Relay.Mutation {
  getMutation() {
    return Relay.QL`mutation{ removeCircleMember }`;
  }

  getVariables() {
    return {
        circleId: this.props.circleIdVar,
        userId: this.props.userIdVar
    };
  }

  getFatQuery() {
    return Relay.QL`
        fragment on removeCircleMemberPayload {
        clientMutationId,
        viewer {
            me {
                circlesUserIsIn
            }
        }
        }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        viewer: this.props.viewer.id,
      },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
      }
    `
  };
}