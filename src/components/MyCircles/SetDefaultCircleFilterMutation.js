import Relay from 'react-relay';

export default class SetDefaultCircleFilterMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      setDefaultCircleFilter
    }`;
  }

  getVariables() {
    return {
      filterID: this.props.filterIDVar,
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on setDefaultCircleFilterPayload {
        clientMutationId,
        user {
          defaultSavedCircleFilter {
            id
            filterName
            location {
              lat
              lng
              radius
            }
            sport {
              sport {
                id
                name {
                  EN
                  FR
                }
              }
            }
            circleType
            memberTypes
            modes
            owners {
              id
              pseudo
            }
          }
        }
      }
    `;
  }

  getConfigs() {
    return [{
        type: 'FIELDS_CHANGE',
        fieldIDs: {
            user: this.props.user.id,
        },
    }];
  }

  static fragments = {
    user: () => Relay.QL`
      fragment on User {
        id
        savedCircleFilters {
          id
          filterName
          location {
            lat
            lng
            radius
          }
          sport {
            sport {
              id
              name {
                EN
                FR
              }
            }
          }
          circleType
          memberTypes
          modes
          owners {
            id
            pseudo
          }
        }
      }
    `,
  };

}
