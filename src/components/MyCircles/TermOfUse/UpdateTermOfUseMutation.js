import Relay from 'react-relay';

export default class UpdateTermOfUseMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      updateTermsOfUse
    }`
  }
  
  getVariables() {
    return  {
      termsOfUse: this.props.termsOfUseVar,
    }
  }

  getFatQuery() {
      return Relay.QL`
        fragment on updateCircleTermsOfUsePayload {
          clientMutationId,
          viewer {
            id,
            me {
              termsOfUses
              circles
            }
          }
        }
      `
   
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
        fieldIDs: {
          viewer: this.props.viewer.id,
        },
    }];
  }

  static fragments = {
    user: () => Relay.QL`
      fragment on User {
          id
          termsOfUses {
            id
            name
            link
            content
            acceptedBy {
              user {
                id
              }
            }
            circles (first: 100) {
              edges {
                node {
                  id 
                  name
                  owner {
                    id
                    pseudo
                  }
                  members {
                    id
                  }
                  type
                  memberCount
                }
              }
            }
          }
      }
    `,
  };

}
