import Relay from 'react-relay';

export default class deleteTermOfUseMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      removeTermsOfUse
    }`
  }
  
  getVariables() {
    return  {
      termsOfUseId: this.props.idVar,
    }
  }

  getFatQuery() {
      return Relay.QL`
        fragment on removeCircleTermsOfUsePayload {
          clientMutationId,
          viewer {
            id,
            me {
              termsOfUses
              circles
            }
          }
        }
      `
   
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
        fieldIDs: {
          viewer: this.props.viewer.id,
        },
    }];
  }

  static fragments = {
    user: () => Relay.QL`
      fragment on User {
          id
          termsOfUses {
            id
            name
            link
            content
            acceptedBy {
              user {
                id
              }
            }
            circles (first: 100) {
              edges {
                node {
                  id 
                  name
                  owner {
                    id
                    pseudo
                  }
                  members {
                    id
                  }
                  type
                  memberCount
                }
              }
            }
          }
      }
    `,
  };

}
