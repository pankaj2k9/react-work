import Relay from 'react-relay';

export default class CircleMutation extends Relay.Mutation {

  getMutation() {
    if (this.props.idVar) {
      return Relay.QL`mutation Mutation{
        updateCircle
      }`
    } else {
      return Relay.QL`mutation Mutation{
        newCircle
      }`
    }
  }
  
  getVariables() {
    if (this.props.idVar) {
      return  {
        circleId: this.props.idVar,
        circle: {
          name: this.props.nameVar,
        },
      };
    } else {
      return  {
        circle: {
          name: this.props.nameVar,
          subCircles: this.props.subCirclesVar,
          owner: this.props.ownerVar,
          mode: this.props.modeVar,
          type: this.props.typeVar,
          sport: this.props.sportVar,
          address: this.props.addressVar,
          isCircleUsableByMembers: this.props.isCircleUsableByMembersVar,
          isCircleAccessibleFromUrl: this.props.isCircleAccessibleFromUrlVar,
        },
      };
    }

    
  }

  getFatQuery() {
    if (this.props.idVar) {
      return Relay.QL`
        fragment on updateCirclePayload {
          clientMutationId,
          viewer {
            id,
            me {
              circles
            }
          }
        }
      `
    } else {
      return Relay.QL`
        fragment on newCirclePayload {
          clientMutationId,
          viewer {
            id,
            me {
              circles
              circlesSuperUser
            }
          }
        }
      `
    }
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
        fieldIDs: {
          viewer: this.props.viewer.id,
        },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        me {
          id
        }
        
      }
    `,
  };

}
