import React from 'react'
import Relay from 'react-relay'
import { colors } from '../../theme'
import Sport from './Sport'
import Sportunity from '../common/Sportunity/Sportunity'
import Feedbacks from '../ProfileView/Feedbacks';
import localizations from '../Localizations'

import Radium from 'radium';

let styles

const Title = ({ children }) => <h2 style={styles.title}>{children}</h2>;

class Body extends React.Component{

  _getAge = (birthday) => {
    var today = new Date();
    var birthDate = new Date(birthday);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
  }

  render() {
    
    const { me, viewer, sportunities } = this.props
    // console.log(me.languages[0].name)
		// console.log(me.languages.lenght)
		let languages = ''
		if (me.languages.length === 1) {
			languages = me.languages[0].name
		} else if (me.languages.length > 1) {
			languages = me.languages.slice(0, me.languages.length - 1).map(language => language.name).join(', ') + ' '+localizations.profile_and+ ' ' + me.languages[me.languages.length - 1].name
		}

    const sexOptions = [
      { value: '', label: '' },
      { value: 'MALE', label: localizations.profile_sex_male },
      { value: 'FEMALE', label: localizations.profile_sex_female },
      { value: 'OTHER', label: localizations.profile_sex_other },
    ];

    return(
      <div style={styles.content}>
        <Title>{localizations.me_description}</Title>
        <div style={styles.description}>
          <p style={styles.descriptionLine}>
            {me.description}
          </p>        
          {!me.hideMyAge && me.birthday &&
            <p style={styles.descriptionLine}>
              {localizations.profile_age + ': ' + this._getAge(me.birthday) + ' ' + localizations.profile_yearsOld}
            </p>
          }
          {me.sex &&
            <p style={styles.descriptionLine}>
              {localizations.profile_gender + ': ' + sexOptions.find(sexOption => me.sex === sexOption.value ).label}
            </p>
          }
        </div>
        {
          languages.length > 0 && 
          <div>
            <Title>{localizations.profile_language}</Title>
            <p style={styles.description}>
              {me.pseudo} {localizations.profile_speaks} {languages}
            </p>
          </div>
        }
        <Title>{localizations.me_sports}</Title>
        {
          me.sports.length > 0 
          ? me.sports.map(sport => <Sport key={sport.sport.id} sport={sport} />)
          : <p style={styles.description}>{localizations.me_no_sport_selected}</p>
        }
        <Feedbacks user={me} {...this.props}/>
        {sportunities.edges.length > 0 &&
          <div style={styles.sportunitiesContainer}>
            <Title>{localizations.me_events}</Title>
            {sportunities.edges.map(edge => <Sportunity key={edge.node.id} sportunity={edge.node} {...this.props} /> )}
          </div>
        }
      </div>
    )
  }
}

styles = {
  content: {
    flexGrow: '1',
    padding: '40px',
    fontFamily: 'Lato',
    color: 'rgba(0,0,0,0.65)',
    '@media (max-width: 480px)': {
      padding: '15px',
    }

  },

  title: {
    fontSize: 32,
    fontWeight: 500,
    marginBottom: 30,
  },

  description: {
    fontSize: 18,
    lineHeight: 1.2,
    marginBottom: 40,
    wordBreak: 'break-word'
  },
  descriptionLine: {
    wordBreak: 'break-word',
    marginBottom: 15
  },
  sportunitiesContainer: {
    marginTop: 40
  }
  
};

export default Relay.createContainer(Radium(Body), {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
        ${Sportunity.getFragment('viewer')}
      }
    `,
    me: () => Relay.QL`
      fragment on User {
        ${Feedbacks.getFragment('user')}
        id
        description
        pseudo
        birthday,
        hideMyAge,
        sex,
        languages {
          id
          name
        }
        sports {
          sport {
            id
            logo
            name {
              EN
              FR
              DE
            }
            levels {
              EN {
                name
              }
              FR {
                name
              }
              DE {
                name
              }
            }
          }
          levels {
            id
            EN {
              name
            }
            FR {
              name
            }
            DE {
              name
            }
          }
          certificates {
            certificate {
              name {
                EN
                FR
                DE
              }
            }
          }
          positions {
            EN
            FR
            DE
          }
        }
      }
    `,
    
  },
});

