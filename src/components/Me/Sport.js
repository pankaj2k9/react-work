import React from 'react'
import IconTint from 'react-icon-tint'
import { colors, fonts, metrics } from '../../theme'
import localizations from '../Localizations'

let styles

const Sport = (sport) => {
  const levelFrom = sport.sport.levels[0]
  const levelTo = sport.sport.levels[sport.sport.levels.length - 1]
  return(
    <div
        style={styles.sportItem}
        key={sport.id}
      >

        <div style={styles.sportIcon}>
          <IconTint width='64' height='64' src={sport.sport.sport.logo} 
              color={colors.blue} 
              
              alt='sportIcon'/>
        </div>
        <div style={styles.info}>
          <div style={styles.nameLevel}>
            <h4 style={styles.name}>
              {sport.sport.sport.name[localizations.getLanguage().toUpperCase()]}
            </h4>
            <span style={styles.level}> - {levelFrom[localizations.getLanguage().toUpperCase()].name}
            { (levelTo && levelTo.id !== levelFrom.id)
              && ' '+localizations.find_to+' ' + levelTo[localizations.getLanguage().toUpperCase()].name
            }</span>
          </div>
          <span style={styles.certificate}>
            {!sport.sport.positions.length ? '' : localizations.event_positions + ': ' + sport.sport.positions.map(position => position[localizations.getLanguage().toUpperCase()]).join(', ')}
          </span>
          <span style={styles.certificate}>
            {!sport.sport.certificates.length ? '' : localizations.event_certificates + ': ' + sport.sport.certificates.map(certificate => certificate.certificate.name[localizations.getLanguage().toUpperCase()]).join(', ')}
          </span>
        </div>
      </div>
  )
} 

styles = {
  container: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    marginBottom: metrics.margin.large,
    color: colors.black,
  },
  sportIcon: {
    marginRight: metrics.margin.large,
    width: 64,
    height: 64,
  },
  h2: {
    fontSize: fonts.size.xl,
    color: colors.blue,
    fontWeight: fonts.size.xl,
    marginBottom: metrics.margin.medium,
  },
  sportList: {
    display: 'flex',
    flexDirection: 'column',
    borderWidth: 1,
    borderColor: 'red',
  },
  sportItem: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: metrics.margin.large,

  },
  info: {
    alignSelf: 'flex-start',
    display: 'flex',
    flexDirection: 'column',
  },
  nameLevel: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  name: {
    fontSize: fonts.size.xl,
    marginRight: metrics.margin.small,
    marginBottom: metrics.margin.small,
  },
  level: {
    fontSize: fonts.size.small,
    marginBottom: metrics.margin.small,
  },
  certificate: {
    fontSize: fonts.size.small,
    marginBottom:  metrics.margin.tiny,
  },
  edit: {
    fontSize: fonts.size.medium,
    color: colors.blue,
    marginLeft: 'auto',
  },
  addButton: {
    backgroundColor: colors.blue,
    color: colors.white,
    fontSize: fonts.size.small,
    padding: metrics.padding.tiny,
    borderRadius: metrics.radius.tiny,
    marginTop: metrics.margin.medium,
    alignSelf: 'flex-start',
    outline: 'none',
  },
}

export default Sport