import React, { Component } from 'react';
import Relay from 'react-relay';
import Header from '../common/Header/Header.js'
import Footer from '../common/Footer/Footer'
import Loading from '../common/Loading/Loading'
import Title from './Title'
import Body from './Body'
import Sportunity from '../common/Sportunity/Sportunity'
import localizations from '../Localizations'

import Radium from 'radium';
let styles

class Me extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      language: localizations.getLanguage(),
    }
  }

  _setLanguage = (language) => {
    this.setState({ language: language })
  }

  componentDidMount = () => {
		setTimeout(() => this.setState({ loading: false }), 1500);
  } 

  componentWillMount = () => {
		this.props.relay.forceFetch()
	}
  
  render() {
    const { viewer } = this.props
   // this.state.sports = viewer.me.sports
    if (this.state.loading) {
      return(<Loading />)
    }

    return (
      <div style={styles.container}>
        {
          viewer.me ? <Header user={viewer.me} viewer={viewer} {...this.state} /> : <Header user={null} {...this.state} />
        }
        {
          viewer.me && 
        
        <div style={styles.wrapper}>
            <Title me={viewer.me} viewer={viewer} {...this.state}/>
            <Body 
              me={viewer.me} 
              viewer={viewer} 
              sportunities={viewer.sportunities}
              {...this.state}
              />
        </div>
        }
        {viewer.me ? <Footer onUpdateLanguage={this._setLanguage} viewer={viewer} user={viewer.me}/> : <Footer onUpdateLanguage={this._setLanguage} viewer={viewer} user={null}/> }
      </div>
      
    );
  }
}

styles = {
  container: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    marginBottom: 20,
    
  },
  contentContainer: {
		width: '100%',
		display: 'flex',
		flexDirection: 'column',
	},
  content: {
    width: 1000,
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'left',
		height: '100%',
    borderRadius: 5,
    margin: '25px auto',
    '@media (max-width: 960px)': {
      width: '94%',
    },
	},
  wrapper: {
    width: 1000,
    borderRadius: 5,
    margin: '25px auto',
    display: 'flex',
    flexDirection: 'column',
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.4)',
    '@media (max-width: 960px)': {
      width: '94%',
    }
  },
}

// Container
// This is higher orther component connected to router, it will get viewer from router
// Viewer will be updated by the data we query here
// This is where you write queries
// ${Languages.getFragment('languages')} is taking fragment from Languages component
// Languages component will be updated after query ( we need to pass languages={viewer.languages}) line 45
export default Relay.createContainer(Radium(Me), {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        ${Header.getFragment('viewer')}
        ${Footer.getFragment('viewer')}
        ${Body.getFragment('viewer')}
        id
        sportunities(last:100, filter: { status: MySportunities }) {
          edges {
            node {
              ${Sportunity.getFragment('sportunity')},
              id 
              title
              description
              beginning_date
              ending_date
              address {
                address 
                city 
                country
                position {
                  lat
                  lng
                }
              }
              status
              venue {
                id
                name
              }
              participants {
                id
              }
              price {
                currency
                cents
              }
            }
          }
        }
        me {
          ${Title.getFragment('me')}
          ${Body.getFragment('me')}
          ${Header.getFragment('user')}
          ${Footer.getFragment('user')}
          id
          firstName
          lastName
          publicAddress {
            city
            country
          }
          venues(last:1) {
            edges {
              node {
                id
              }
            }
          }
          
        }
      }
    `,
  },
});


