import React from 'react'
import Relay from 'react-relay'
import { colors } from '../../theme'
import { Link } from 'react-router'

import localizations from '../Localizations'
import Radium from 'radium';
let styles

class Title extends React.Component {
  render() {
    const { me, viewer } = this.props
   

    return(
      <header style={styles.header}>
        <div style={styles.left}>

          <div style={{ ...styles.userpic }}>
            {/*<img src={me.avatar} width='120' style={styles.userpic} />*/}
            <div style={{...styles.userpic, backgroundImage: 'url('+me.avatar+')'}} />
          </div>
          <div style={styles.userInfo}>
            <h1 style={styles.title}>
              {me.pseudo}
            </h1>
            { me.publicAddress &&
              <div style={styles.address}>
                <i
                  style={styles.marker}
                  className="fa fa-map-marker"
                  aria-hidden="true"
                />
                  <span>{me.publicAddress.city}, {me.publicAddress.country}</span>
              </div>
            }
            <div style={styles.participants}>
              {localizations.formatString(
                viewer.sportunities.edges.length > 1
                ? localizations.me_upcomings : localizations.me_upcoming
                , viewer.sportunities.edges.length)}
            </div>
          </div>
        </div>
        <div style={styles.info}>
          <div style={styles.followers}>
            <span style={{fontWeight: 'bold'}}>{me.followers.length}</span>
            <i onClick={this.confirmFollowUser} style={styles.followerIcon} className="fa fa-heart-o fa-2x" />
          </div>
          <Link to='/profile'>
            <button
              style={styles.book}
            >
              {localizations.me_editProfile}
            </button>
          </Link>
        </div>
      </header>
    )
  }
}

styles = {
  header: {
    width: '100%',
    padding: '47px 42px 38px',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,

    backgroundColor: colors.blue,
    color: colors.white,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    '@media (max-width: 960px)': {
      display: 'block',
    },
  },

  left: {
    display: 'flex',
    alignItems: 'flex-start',
    '@media (max-width: 450px)': {
      display: 'block',
      textAlign: 'center',
    },
  },

  userpic: {
    borderRadius: '50%',
    width: 120,
    height: 120,
    backgroundSize: 'cover',
    backgroundPosition: '50% 50%',
    backgroundRepeat: 'no-repeat',
    marginRight: 54,
    '@media (max-width: 450px)': {
      margin: '0 auto',
    },
  },

  userInfo: {
    color: colors.white,
    fontFamily: 'Lato',
    fontSize: 22,
    fontWeight: 500,
  },

  title: {
    fontSize: 36,
    marginBottom: 13,
    maxWidth: 500,
  },

  address: {
    marginBottom: 17,
  },

  marker: {
    color: colors.white,
    marginRight: 18,
  },

  participants: {
    alignItems: 'center',
  },

  participantsMarker: {
    color: colors.white,
    fontSize: 16,
    marginRight: 14,
  },

  info: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    '@media (max-width: 600px)': {
      marginTop: 30,
      textAlign: 'center',
      display: 'block',
    },
  },

  datetime: {
    color: colors.white,
    fontSize: 16,
    fontWeight: 500,
    fontFamily: 'Lato',

    marginBottom: 26,
  },

  book: {
    backgroundColor: colors.green,
    color: colors.white,
    width: 180,
    height: 57,
    borderRadius: 100,
    borderStyle: 'none',
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
    fontSize: 22,
    cursor: 'pointer',

    ':disabled': {
      cursor: 'not-allowed',
    },
  },
	cancel: {
    backgroundColor: colors.red,
    color: colors.white,
    width: 180,
    height: 57,
    borderRadius: 100,
    borderStyle: 'none',
    boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
    fontSize: 22,
    cursor: 'pointer',

    ':disabled': {
      cursor: 'not-allowed',
    },
  },

  followers: {
    marginBottom: 20, 
    fontSize: 30,
    fontFamily: 'Lato',
    display: 'flex',
    justifyContent: 'center',
    '@media (max-width: 600px)': {
      marginTop: 10, 
    }
  },
  followerIcon: {
    fontSize: 30,
    marginLeft: 10,
  }
};

export default Relay.createContainer(Radium(Title), {
  fragments: {
    me: () => Relay.QL`
      fragment on User {
        id
        pseudo
        avatar,
        publicAddress{
          city
          country
        },
        followers {
          id
        }
      }
    `,

  },
});