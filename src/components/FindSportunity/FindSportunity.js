import React from 'react'
import PureComponent, { pure } from '../common/PureComponent'
import Radium from 'radium'
import Relay from 'react-relay'
import ReactLoading from 'react-loading'; 
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { isEqual} from 'lodash';
import withRouter from 'react-router/lib/withRouter'

import Header from '../common/Header/Header'
import Footer from '../common/Footer/Footer'
import Filter from './Filter'
import Events from './Events'
import GMap from './Map'
import Loading from '../common/Loading/Loading'
import localizations from '../Localizations'
import * as types from '../../actions/actionTypes.js'
import { colors } from '../../theme'

let styles

const Title = pure(({ children, style }) => <h2 style={{...styles.title, ...style}}>{children}</h2>)

class FindSportunity extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      levels: [],
      selectedLevels: [],
			locationLat: null,
			locationLng: null,
			mapStatus: 'updated',
      loading: true,
      queryIsLoading: false,
      updateQuery: false, 
      language: localizations.getLanguage(),
      pageSize: 20,
      itemCount: 10,
      distanceRange: null,
      isFirstQueryDone: false,
	    width: '0',
	    height: '0',
      showFilter: false,
    }

	  this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
  }

	updateWindowDimensions() {
		this.setState({ width: window.innerWidth, height: window.innerHeight });
	}

  _loadMore = () => {
    const nextCount = this.state.itemCount + this.state.pageSize
    this.setState({queryIsLoading: true})
    this.props.relay.setVariables({ first: nextCount }, 
			readyState => {
				if (readyState.done) {
						setTimeout(() => {// Needed to wait for Relay to re-fetch data in this.props.viewer
								this.setState({
									queryIsLoading: false,
								})
						}, 50);
				}
			})
    this.setState({
      itemCount: nextCount,
    })
  }

	_updateLocationGeo = (lat, lng) => {
		this.setState({
			locationLat: lat,
			locationLng: lng,
		})
	}

  _updateLocationDistanceRange = (radius) => {
    this.setState({
      distanceRange: radius
    })
  }

	_updateMapStatus = (status) => {
		this.setState({
			mapStatus: status,
		})
	}

  _setLevels = (levels) => {
    this.setState({
      levels: levels,
    })
  }

  _setSelectedLevels = (levels) => {
    this.setState({
      selectedLevels: levels,
    })
  }

  _setFilter = () => {
    let filter = {}
    if (this.props.locationLat && this.props.locationLng) {
        filter.location = {
          lat: this.props.locationLat,
          lng: this.props.locationLng,
          radius: this.props.distanceRange ? this.props.distanceRange : 50
        }
    }

    if(this.props.sportId) {
      filter.sport = { sportID: this.props.sportId }
    }

    if(this.props.isFreeOnly) {
      filter.price = { from: 0, to: 0 }
    }

    if(this.props.dateFrom && this.props.dateTo) {
      filter.dates = { from:this.props.dateFrom, to:this.props.dateTo }
    }

    if(this.props.hourFrom && this.props.hourTo) {
      filter.hours = { from:this.props.hourFrom.split(':')[0], to:this.props.hourTo.split(':')[0] }
    }

		filter.status = 'Available'
    if (this.props.sexRestriction !== '')
      filter.sexRestriction = this.props.sexRestriction;
    filter.ageRestriction = this.props.ageRestriction;

    let newValue = {
      filter: filter,
    }
    if (!this.props.relay.variables.query)
      newValue.query = true; 

    newValue.first = 10 ; 
    this.setState({itemCount: 10})

    this.setState({updateQuery: true})
    this.props.relay.setVariables(newValue, 
			readyState => {
				if (readyState.done) {
            setTimeout(() => {// Needed to wait for Relay to re-fetch data in this.props.viewer
								this.setState({
                  updateQuery: false,
                  isFirstQueryDone: true
								})
						}, 50);
				}
			}
		)
		if(Object.keys(filter).length === 0) {
			this.setState({ mapStatus: 'finalizing' })
		}
	}

  _onEventHoverHandler = (id) => {
		this.setState({
			highlightedId: id,
		})
	}

	_onEventLeaveHandler = () => {
		this.setState({
			highlightedId: null,
		})
	}

  componentDidMount = () => {
	  this.updateWindowDimensions();

	  if (typeof window !== 'undefined')
		  window.addEventListener('resize', this.updateWindowDimensions);
    if (this.props.urlLat || this.props.urlLng || this.props.urlRange || this.props.urlSportId || this.props.urlFromDate || this.props.urlToDate || this.props.urlFree) {
      this._setPropsFromUrl(() => this._setFilter());
    }
    else if (!this.props.locationLat && !this.props.locationLng) {
      fetch('https://ipapi.co/json')
      .then(res => res.json())
      .then(json => {
        if (json.latitude && json.longitude) {
          this.props._updateLocationAction(json.city, json.latitude, json.longitude)
          this._updateLocationGeo(json.latitude, json.longitude)
          this._updateLocationDistanceRange(50);
          this.props._updateDistanceRange(50);
          this._setFilter()
        }
      })  
    }
    else
		  this._setFilter()
		document.title = 'Find the right sport opportunities';
    setTimeout(() => this.setState({ loading: false }), 1000);
  }

  componentWillUnmount = () => {
    /*this._setSelectedLevels([])
    this._setLevels([])
    this.props._updateSportAction('','')
    this.props._updateLocationAction('', null, null)
    this.props._updateDistanceRange('');
    this.props._updateIsFreeOnly(false)
    this.props._updateDateFrom(null)
    this.props._updateDateTo(null)
    this.props._updateHourFrom(null)
    this.props._updateHourTo(null)
    this.props._updateSexRestriction('');
    this.props._updateAgeRestriction({from: 0, to: 100})
    */
	  window.removeEventListener('resize', this.updateWindowDimensions);
  }

  _setPropsFromUrl = (callback) => {
    const {urlLat,
      urlLng,
      urlRange,
      urlSportId,
      urlFromDate,
      urlToDate,
      urlFree} = this.props
    
    if (urlSportId) {
      this.props.relay.setVariables({
        sportId: urlSportId,
        querySport: true
      })
    }
    
    if (urlFromDate && urlToDate && new Date(parseInt(urlFromDate)) > 0 && new Date(parseInt(urlFromDate)) > 0) {
      this.props._updateDateFrom(new Date(parseInt(urlFromDate)))
      this.props._updateDateTo(new Date(parseInt(urlToDate)))
    }
    if (urlFree && typeof(urlFree) === 'boolean') {
      this.props._updateIsFreeOnly(urlFree)
    }

    if (urlLat && urlLng && !Number.isNaN(urlLat) && !Number.isNaN(urlLng)) {
      let geocoder = new google.maps.Geocoder();
      let city;
      
      geocoder.geocode({'latLng': new google.maps.LatLng({lat: parseFloat(urlLat), lng: parseFloat(urlLng)})},(results, status) => {
        if (status === 'OK') {
          for (var a=0 ; a<results.length; a++) {
            let resultIdFound = false;
            for (var n=0; n<results[a].types.length ; n++) {
              if (results[a].types[n] === "locality") {
                resultIdFound = true ;
              }
            }
            if (resultIdFound) {
              for (var i=0; i<results[a].address_components.length; i++) {
                for (var b=0;b<results[a].address_components[i].types.length;b++) {
                    if (results[a].address_components[i].types[b] == "locality") {
                        city = results[a].address_components[i].long_name;
                        break;
                    }
                }
              }
            }
          }
        }
      
        this.props._updateLocationAction(city, parseFloat(urlLat), parseFloat(urlLng))
        this._updateLocationGeo(parseFloat(urlLat), parseFloat(urlLng))

        if (urlRange && !Number.isNaN(urlRange)) {
          this._updateLocationDistanceRange(parseInt(urlRange));
          this.props._updateDistanceRange(parseInt(urlRange));
        }
        else {
          this._updateLocationDistanceRange(50);
          this.props._updateDistanceRange(50);
        }
        if (!urlSportId)
          callback();
      })
    }
    else if (!urlSportId) 
      callback()
  }


  componentWillReceiveProps = nextProps => {
    if (!this.props.viewer.sport && nextProps.viewer.sport) {
      this.props._updateSportAction(nextProps.viewer.sport.id, nextProps.viewer.sport.name[this.state.language.toUpperCase()])
      setTimeout(() => this._setFilter(), 150);
    }
  }

  componentDidUpdate = (prevProps) => {
    if (this.state.isFirstQueryDone && 
      ((prevProps.sportId !== this.props.sportId) ||
        (prevProps.locationLat !== this.props.locationLat) ||
        (prevProps.locationLng !== this.props.locationLng) ||
        (prevProps.distanceRange !== this.props.distanceRange) ||
        (prevProps.isFreeOnly !== this.props.isFreeOnly) ||
        (prevProps.dateFrom !== this.props.dateFrom) ||
        (prevProps.dateTo !== this.props.dateTo) ||
        (prevProps.hourFrom !== this.props.hourFrom) ||
        (prevProps.hourTo !== this.props.hourTo) ||
        (prevProps.sexRestriction !== this.props.sexRestriction) ||
        (prevProps.ageRestriction && this.props.ageRestriction && prevProps.ageRestriction.from !== this.props.ageRestriction.from) ||
        (prevProps.ageRestriction && this.props.ageRestriction && prevProps.ageRestriction.to !== this.props.ageRestriction.to)))  
    {
      this._setFilter()
    }

    const pendingVariables = this.props.relay.pendingVariables
		if(pendingVariables && this.state.mapStatus === 'updated') {
			this.setState({ mapStatus: 'updating' })
		} else if (!pendingVariables && this.state.mapStatus === 'updating') {
			this.setState({ mapStatus: 'finalizing' })
    }
    
    if (this.props.relay.variables.first === 10 
      && prevProps.relay.variables.first === 10 
      && this.props.viewer.sportunities
      && !this.state.queryIsLoading
      && this.state.isFirstQueryDone) {
        this._loadMore();
    }

  }

  _setLanguage = (language) => {
    this.setState({ language: language })
  }

	toogleFilter = () => {
    this.setState({
	    showFilter: !this.state.showFilter
    })
  }

  render(){
    const { viewer } = this.props

    return (
        <div>
          {this.state.loading && <Loading />}
					<div style={styles.container}>
						{
							viewer && viewer.me ?
                <Header user={viewer.me} viewer={viewer} {...this.state}/>
                : <Header viewer={viewer ? viewer : null} user={null} {...this.state}/>
						}
					</div>
					<div style={styles.bodyContainer}>
              <div style={styles.leftContainer}>
                <Title style={this.state.width < 1024 && {textAlign: 'center'}}>{localizations.find_title}</Title>
	              {this.state.width >= 1024 ?
		              <Filter {...this.props} {...this.state}
		                      setLevels={this._setLevels}
		                      setSelectedLevels={this._setSelectedLevels}
		                      viewer={viewer ? viewer : null}
		                      user={this.props.viewer ? this.props.viewer.me : null}
		                      onLocationChange={this._updateLocationGeo}
		                      onDistanceRangeChange={this._updateLocationDistanceRange}
		              />
                  : viewer.sportunities && !this.state.updateQuery &&
		              <Events sportunities={ viewer ? viewer.sportunities : null } {...this.props} {...this.state}
		                      onHover={this._onEventHoverHandler}
		                      onLeave={this._onEventLeaveHandler}
		                      onLoadMore={this._loadMore}
		                      queryIsLoading={this.state.queryIsLoading}
		              />
	              }
              </div>
              <div style={styles.middleContainer}>
                {this.state.updateQuery &&
                <div style={styles.loadingSpinner}>
                  <ReactLoading type='cylon' color={colors.blue} />
                </div>
                }
                {this.state.width < 1024 &&
	                <div style={{width: '100%', display: 'flex', justifyContent: 'center'}}>
		                <div style={styles.button} onClick={this.toogleFilter}>
			                {this.state.showFilter ? localizations.find_event_hideFilter : localizations.find_event_showFilter}
		                </div>
	                </div>
                }
                {this.state.width >= 1024
                  ? viewer.sportunities && !this.state.updateQuery &&
                  <Events sportunities={ viewer ? viewer.sportunities : null } {...this.props} {...this.state}
                          onHover={this._onEventHoverHandler}
                          onLeave={this._onEventLeaveHandler}
                          onLoadMore={this._loadMore}
                          queryIsLoading={this.state.queryIsLoading}
                  />
                  : this.state.showFilter &&
                    <Filter {...this.props} {...this.state}
                            setLevels={this._setLevels}
                            setSelectedLevels={this._setSelectedLevels}
                            viewer={viewer ? viewer : null}
                            user={this.props.viewer ? this.props.viewer.me : null}
                            onLocationChange={this._updateLocationGeo}
                            onDistanceRangeChange={this._updateLocationDistanceRange}
                    />
                }
              </div>
            <div style={styles.rightContainer}>
							
              <GMap sportunities={ viewer && viewer.sportunities ? viewer.sportunities : null}
                    {...this.props}
                    {...this.state}
                    onLocationChange={this._updateLocationGeo}
                    onUpdateMapStatus={this._updateMapStatus}
              />
              
            </div>
          </div>
          {viewer && viewer.me ?
            <Footer onUpdateLanguage={this._setLanguage} viewer={viewer} user={viewer.me}/>
            : <Footer onUpdateLanguage={this._setLanguage} viewer={viewer ? viewer : null} user={null}/> }
        </div>
    )

  }
}

const stateToProps = (state) => ({
  sportId: state.sportunitySearchReducer.sportId,
  sportName: state.sportunitySearchReducer.sportName,
  locationName: state.sportunitySearchReducer.locationName,
  locationLat: state.sportunitySearchReducer.locationLat,
  locationLng: state.sportunitySearchReducer.locationLng,
  distanceRange: state.sportunitySearchReducer.distanceRange,
  isFreeOnly: state.sportunitySearchReducer.isFreeOnly,
  dateFrom: state.sportunitySearchReducer.dateFrom,
  dateTo: state.sportunitySearchReducer.dateTo,
  hourFrom: state.sportunitySearchReducer.hourFrom,
  hourTo: state.sportunitySearchReducer.hourTo,
  ageRestriction: state.sportunitySearchReducer.ageRestriction,
  sexRestriction: state.sportunitySearchReducer.sexRestriction,
  userCountry: state.globalReducer.userCountry
});

const _updateSportAction = (id, name) => {
  return {
    type: types.UPDATE_SPORTUNITY_SEARCH_SPORT,
    sportId: id,
    sportName: name,
  };
}

const _updateLocationAction = (name, lat, lng) => {
  return {
    type: types.UPDATE_SPORTUNITY_SEARCH_LOCATION,
    locationName: name,
    locationLat: lat,
    locationLng: lng,
  };
}

const _updateDistanceRange = (distanceRange) => {
  return {
    type: types.UPDATE_SPORTUNITY_SEARCH_DISTANCE_RANGE,
    distanceRange: distanceRange
  }
}

const _updateIsFreeOnly = (isFreeOnly) => {
  return {
    type: types.UPDATE_SPORTUNITY_SEARCH_FREE_ONLY,
    isFreeOnly: isFreeOnly,
  }
}

const _updateDateFrom = (dateFrom) => {
  return {
    type: types.UPDATE_SPORTUNITY_SEARCH_DATE_FROM,
    dateFrom: dateFrom,
  }
}

const _updateDateTo = (dateTo) => {
  return {
    type: types.UPDATE_SPORTUNITY_SEARCH_DATE_TO,
    dateTo: dateTo,
  }
}

const _updateHourFrom = (hourFrom) => {
  return {
    type: types.UPDATE_SPORTUNITY_SEARCH_HOUR_FROM,
    hourFrom: hourFrom,
  }
}

const _updateHourTo = (hourTo) => {
  return {
    type: types.UPDATE_SPORTUNITY_SEARCH_HOUR_TO,
    hourTo: hourTo,
  }
}

const _updateSexRestriction = (sexRestriction) => {
  return {
    type: types.UPDATE_SPORTUNITY_SEARCH_SEX_RESTRICTION,
    sexRestriction: sexRestriction,
  }
}

const _updateAgeRestriction = (ageRestriction) => {
  return {
    type: types.UPDATE_SPORTUNITY_SEARCH_AGE_RESTRICTION,
    ageRestriction: ageRestriction,
  }
}

const dispatchToProps = (dispatch) => ({
  _updateSportAction: bindActionCreators(_updateSportAction, dispatch),
	_updateLocationAction: bindActionCreators(_updateLocationAction, dispatch),
  _updateIsFreeOnly: bindActionCreators(_updateIsFreeOnly, dispatch),
  _updateDistanceRange: bindActionCreators(_updateDistanceRange, dispatch),
	_updateDateFrom: bindActionCreators(_updateDateFrom, dispatch),
	_updateDateTo: bindActionCreators(_updateDateTo, dispatch),
	_updateHourFrom: bindActionCreators(_updateHourFrom, dispatch),
	_updateHourTo: bindActionCreators(_updateHourTo, dispatch),
  _updateSexRestriction: bindActionCreators(_updateSexRestriction, dispatch),
  _updateAgeRestriction: bindActionCreators(_updateAgeRestriction, dispatch),
});

const ReduxContainer = connect(
  stateToProps,
  dispatchToProps
)(Radium(FindSportunity));

styles = {
  button: {
	  height: '50px',
	  backgroundColor: colors.blue,
	  boxShadow: '0 0 4px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.24)',
	  borderRadius: '20px',
	  display: 'inline-block',
	  fontFamily: 'Lato',
	  fontSize: '22px',
	  textAlign: 'center',
	  color: colors.white,
	  borderWidth: 0,
	  margin: 10,
	  padding: 10,
	  cursor: 'pointer',
	  lineHeight: '27px',
  },
  bodyContainer: {
    display: 'flex',
    width: '100%',
		flexDirection: 'row',
		fontFamily : 'Lato',
		padding: 0,
    '@media (max-width: 1024px)': {
      display: 'inline-block',
    }
  },
  leftPart: {
    display: 'flex',
    flexDirection: 'row',
    '@media (max-width: 1024px)': {
      display: 'inline-block',
    },
    '@media (max-width: 750px)': {
      display: 'flex',
      flexDirection: 'column-reverse',
    }
  },
  container: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: 15,
    marginLeft: 25,
    color: colors.blue,
  },
	leftContainer: {
		// display: 'flex',
		flexDirection: 'column',
		width: 250,
    '@media (max-width: 1024px)': {
      display: 'inline-block',
      width: '100%',
    },
    '@media (max-width: 420px)': {
      overflow: 'hidden',
    }
	},
	middleContainer: {
		display: 'flex',
    width: 500,
    maxWidth: '100%',
    flex: 1,
    '@media (max-width: 1024px)': {
      display: 'inline-block',
    }
  },
	rightContainer: {
		display: 'flex',
    width: '100%',
    flex: 1,
    '@media (max-width: 1024px)': {
      display: 'inline-block',
    }
  },
  loadingSpinner:{
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    height: '100%'
  }
}


export default Relay.createContainer(withRouter(Radium(ReduxContainer)), {
  initialVariables: {
    filter: { status : 'Available' },
    first: 10,
    query: false,
    sportId: null,
    querySport: false
  },
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        me {
          ${Header.getFragment('user')}
          ${Footer.getFragment('user')}
          ${Filter.getFragment('user')}
        }
        ${Header.getFragment('viewer')}
        ${Events.getFragment('viewer')},
        sportunities(first: $first, filter: $filter ) @include(if: $query) {
          ${Events.getFragment('sportunities')},
          ${GMap.getFragment('sportunities')},
          pageInfo {
            hasNextPage
          }
        }
        ${Filter.getFragment('viewer')},
        ${Footer.getFragment('viewer')}
        sport(id: $sportId) @include(if: $querySport) {
          id
          name {
            EN
            FR
          }
        }
      }
    `,
  },
});
