import React, { Component } from 'react';
import TagItem from './TagItem';
import localizations from '../Localizations'

import Radium from 'radium';
let styles ;



class TagBox extends Component {
  render() {

    let items = [
      {
        icon: '/assets/images/loupe.png',
        title: localizations.homeCompanies_slide_event_title,
        desc: localizations.homeCompanies_slide_event_desc,
        image: '/assets/images/mes_activites.png',
        goTo: null,
      },
      {
        icon: '/assets/images/cercles.png',
        title: localizations.homeCompanies_slide_circle_title,
        desc: localizations.homeCompanies_slide_circle_desc,
        image: '/assets/images/communaute.png',
        goTo: null,
      },
      {
        icon: '/assets/images/organise.png',
        title: localizations.homeCompanies_slide_organise_title,
        desc: localizations.homeCompanies_slide_organise_desc,
        image: '/assets/images/organiser.png',
        goTo: null,
      },
    ];

    return (
      <div style={styles.container} >
        {items.map((item, index) =>
          <TagItem
            key={index}
            icon={item.icon}
            title={item.title}
            descr={item.desc}
            image={item.image}
            link={item.goTo}
            id={index}
            {...this.state}
          />
        )}
      </div>
    );
  }
}


styles = {
  container: {
    // height: '320px',
    height: 'auto',
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'wrap',
    '@media (max-width: 768px)': {
      margin: '2% 0px',
    },
    '@media (max-width: 480px)': {
      margin: '2% auto',
      display: 'block',
      height: 'auto'
    }
  },
};


export default Radium(TagBox);