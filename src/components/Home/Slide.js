import React, { Component } from 'react';

import Radium from 'radium'
import localizations from "../Localizations";
import {browserHistory} from "react-router";

import colors from "../../theme/colors";

let styles ;

class Slide extends Component {

  constructor(props) {
    super(props);
  }

  _handleClick = () => {
    if (this.props.link)
      browserHistory.push(this.props.link)
  };

  render() {
    const {icon, title, descr, image, id} = this.props;

    return (
      <div style={styles.container}>
        <div style={styles.textContainer}>
          <div style={styles.titleContainer}>
            <img src={icon} style={styles.icon}/>
            <p style={styles.title}>
              {title}
            </p>
          </div>
          <ul style={{textAlign: 'center'}}>
            {descr && descr.map((text, index) =>
                <li key={index} style={styles.text}>{text}</li>
            )}
          </ul>
        </div>
        <img src={image} style={styles.image}/>
      </div>
    )
  }
}

styles = {
  container: {
    display: 'flex',
    width: '100%',
    height: 650,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    paddingBottom: 40,
  },
  textContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: 10,
    margin: 10,
  },
  titleContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
    textAlign: 'center',
    '@media (max-width: 830px)': {
      flexDirection: 'column'
    },
  },
  icon: {
    width: 100,
    height: 100,
    margin: -10,
    '@media (max-width: 425px)': {
      width: 75,
      height: 75,
    }
  },
  title: {
    fontFamily: 'Lato',
    fontSize: '26px',
    fontWeight: 'bold',
    color: colors.darkGray,
    '@media (max-width: 600px)': {
      fontSize: '18px',
    },
  },
  text: {
    fontFamily: 'Lato',
    fontSize: '18px',
    marginBottom: 10,
    color: colors.darkGray,
    '@media (max-width: 600px)': {
      fontSize: '14px',
    },
  },
  image: {
    maxWidth: '50%',
    maxHeight: '95%',
    '@media (max-width: 440px)': {
      marginLeft: -30
    },
    '@media (min-width: 768px)': {
      //maxWidth: 384,
    }
  },
  seeMoreButton: {
    border: '1px solid #000',
    width: '50%',
    alignSelf: 'center',
    color: colors.blue,
    padding: 10,
    marginTop: 20,
    textAlign: 'center',
    fontFamily: 'Lato',
    fontSize: '18px',
    cursor: 'pointer',
    '@media (max-width: 600px)': {
      fontSize: '14px',
    },
  },
};

export default Radium(Slide);