import React, { Component } from 'react';
import Radium from 'radium'
import RSlider from 'react-slick'

import localizations from "../Localizations";
import Slide from './Slide';

let styles ;

class Slider extends Component {

  constructor(props) {
    super(props);
    this.state = {
      currentSlide: 0,
      displaySlider: false,
    }
  }

  componentDidMount() {
    this.setState({displaySlider: true})
  }

  render() {

    let items = [
      {
        icon: '/assets/images/cercles.png',
        title: localizations.home_slide_circle_title,
        desc: localizations.home_slide_circle_desc,
        image: '/assets/images/ta_communaute_smartphone.png',
        goTo: null,
      },
      {
        icon: '/assets/images/loupe.png',
        title: localizations.home_slide_explorer_title,
        desc: localizations.home_slide_explorer_desc,
        image: '/assets/images/explorer_smartphone.png',
        goTo: null,
      },
      {
        icon: '/assets/images/organise.png',
        title: localizations.home_slide_organise_title,
        desc: localizations.home_slide_organise_desc,
        image: '/assets/images/page_principale/creer_une_activite.png',
        goTo: null,
      },
    ];

    let settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 5000,
      pauseOnFocus: true,
      pauseOnHover: true,
      pauseOnDotsHover: true,
    };
    return  (
      <div style={styles.slideContainer}>
        {this.state.displaySlider && 
          <RSlider {...settings}>
            {items.map((item, index) =>
              <Slide
                icon={item.icon}
                title={item.title}
                descr={item.desc}
                image={item.image}
                link={item.goTo}
                id={index}
                key={index}
                {...this.state}
              />
            )}
          </RSlider>
        }
      </div>
    );
  }
}

styles = {
  slideContainer: {
    backgroundColor: '#f6f6fe',
    paddingBottom: 50
  },
};

export default Radium(Slider);