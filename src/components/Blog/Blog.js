import React, { Component } from 'react'
import Relay from 'react-relay'
import { Link } from 'react-router'

import Header from '../common/Header/Header.js'
import Footer from '../common/Footer/Footer'
import Loading from '../common/Loading/Loading'
import localizations from '../Localizations'
import { colors } from '../../theme'
import FaqVideo from '../FAQ/UserTutorial/FaqVideo'
import contents from './contents'

import Radium from 'radium'


class Blog extends Component {
  constructor(props) {
    super(props)
    this.state = {
      language: localizations.getLanguage(),
      videoStartTime: 0,
      videoAutoPlay: 0
    }
  }

  _setLanguage = (language) => {
    this.setState({ language: language })
  }

  render() {
    const videoLinksAndTimes = {
    }

    const { viewer } = this.props

    return (
      <div style={styles.container}>
        {
          viewer.me ? <Header user={viewer.me} viewer={viewer} {...this.state} /> : <Header user={null} {...this.state} />
        }
        <div style={styles.content}>
          <div style={styles.title}>
            <h1>{localizations.blog_title}</h1>
          </div>
          <div style={styles.content}>
            {contents.map(({title, content, video}, index) => (
              <div style={styles.body} key={`blog-content-${index}`}>
                <div style={styles.contentTitle}>
                  {title()}
                </div>
                <div style={styles.contentParagraph}>
                  {content()}
                </div>
                <div style={styles.videoContainer}>
                  <FaqVideo
                    videoId={video}
                    start={this.state.videoStartTime}
                    autoplay={this.state.videoAutoPlay}
                  />
                </div>
              </div>
            ))}
          </div>
        </div>


        {viewer.me
          ? <Footer onUpdateLanguage={this._setLanguage} viewer={viewer} user={viewer.me} />
          : <Footer onUpdateLanguage={this._setLanguage} viewer={viewer} user={null} />
        }
      </div>

    )
  }
}

const styles = {
  container: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      marginBottom: 20,
  },
  content: {
      width: 1000,
      margin: '35px auto',
      display: 'flex',
      flexDirection: 'column',
      '@media (max-width: 960px)': {
          width: '94%',
      }
  },
  title: {
      fontSize: 30,
      color: colors.blue,
      fontFamily: 'Lato',
      marginBottom: 30,
      fontWeight: '500'
  },
  body: {
      borderRadius: 5,
      display: 'flex',
      flexDirection: 'column',
      boxShadow: '0 0 4px 0 rgba(0,0,0,0.4)',
      fontSize: 16,
      fontFamily: 'Lato',
      color: colors.black,
      padding: '30px 40px',
  },
  contentTitle: {
    fontSize: 24, 
    fontWeight: '500',
    paddingBottom: 15,
    borderBottom: '1px solid ' + colors.blue
  },
  contentParagraph: {
      marginTop: 35,
      fontSize: 16,
      lineHeight: '20px'
  },
  videoContainer: {
    height: 500,
    '@media (max-width: 960px)': {
      height: 410,
    },
    '@media (max-width: 680px)': {
      height: 300,
    },
    '@media (max-width: 480px)': {
      height: 200,
    },
    '@media (max-width: 360px)': {
      height: 150,
    },
  }
}

export default Relay.createContainer(Radium(Blog), {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        ${Header.getFragment('viewer')}
        ${Footer.getFragment('viewer')}
        id
        me {
          id
          pseudo
        }
      }
    `,
  },
})
