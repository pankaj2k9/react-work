import React from 'react'
import Relay from 'react-relay'
import styles from './Styles'
import AlertContainer from 'react-alert'
import localizations from '../Localizations'

class CreditCard extends React.Component {
  constructor(props) {
    super(props)
    this.alertOptions = {
      offset: 60,
      position: 'top right',
      theme: 'light',
      transition: 'fade',
    };
    this.state = {
      editMode: false,
    }
  }

  _setCreditCardEditMode = () => {
    this.setState({
      editMode: true,
    })
  } 

  render() {
    const { editMode } = this.state
    const { user } = this.props
    return(
      <section>	
				<AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
				<div style={styles.rowHeader}>
					<div style={styles.pageHeader}>{localizations.payment_creditCard}</div>
					{ (!editMode) 
						&& <div style={styles.editButton} onClick={this._setCreditCardEditMode}>{localizations.payment_add}</div> }
				</div>
        {
					user.paymentMethods.length || editMode ? 
          <section>
          </section>
          :
          <div style={styles.noDataError}>No credit card data</div>
        }
      </section>
    )

  }
}


export default Relay.createContainer(CreditCard, {
  fragments: {
    user: () => Relay.QL`
      fragment on User {
				paymentMethods {
          id
          cardType
          cardMask
          expirationDate
        }
			}
    `,
		viewer: () => Relay.QL`
      fragment on Viewer {
				me {
					id
				}	
			}
    `,
  },
})