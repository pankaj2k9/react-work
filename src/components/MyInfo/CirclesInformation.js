import React from 'react'
import Relay from 'react-relay'
import AlertContainer from 'react-alert'
import ReactLoading from 'react-loading'
import moment from 'moment'

import RelayStore from '../../RelayStore.js'
import { colors, fonts, appStyles } from '../../theme'
import UpdateFilledInformationMutation from './UpdateFilledInformationMutation.js';
import InputText from '../common/Inputs/InputText'
import InputNumber from '../common/Inputs/InputNumber'
import InputCheckbox from '../common/Inputs/InputCheckbox'
import InputSelect from '../common/Inputs/InputSelect'
import InputDate from '../common/Inputs/InputDate'
import InputAddress from '../common/Inputs/InputAddress'
import InputPhone from '../common/Inputs/InputPhone'
import CirclePaymentReference from './CirclePaymentReference';

import localizations from '../Localizations'
import styles from './Styles'

class CirclesInformation extends React.Component {
	constructor(props) {
        super(props)		
        this.state = {
            circlesWithAskedInformation: [],
            circlesWithAskedFees: [],
            editInfo: false,
            isLoading: false
        }
        this.alertOptions = {
            offset: 60,
            position: 'top right',
            theme: 'light',
            transition: 'fade',
        };
    }

    componentDidMount = () => {
        if (this.props.user && this.props.user.circlesUserIsIn && this.props.user.circlesUserIsIn.edges && this.props.user.circlesUserIsIn.edges.length > 0)
            this._setCircleWithAskedInformation(this.props.user.circlesUserIsIn.edges)
    }

    componentWillReceiveProps = (nextProps) => {
        if (nextProps.user && nextProps.user.circlesUserIsIn && nextProps.user.circlesUserIsIn.edges && nextProps.user.circlesUserIsIn.edges.length > 0)
            this._setCircleWithAskedInformation(nextProps.user.circlesUserIsIn.edges)
    }
    
    _setCircleWithAskedInformation = (circles) => {
        let circleAskedInformationList = [];
        let paymentModelList = [];
        
        circles.forEach(circle => {
            if (circle.node.askedInformation && circle.node.askedInformation.length > 0) {
                circleAskedInformationList.push({
                    circle: circle.node,
                    forms: []
                });

                circle.node.askedInformation.forEach(askedInfo => {
                    let formIndex = circleAskedInformationList[circleAskedInformationList.length - 1].forms.findIndex(form => form.id === askedInfo.form.id);

                    if (formIndex >= 0) {
                        circleAskedInformationList[circleAskedInformationList.length - 1].forms[formIndex].info.push(askedInfo)
                    }
                    else {
                        circleAskedInformationList[circleAskedInformationList.length - 1].forms.push({
                            id: askedInfo.form.id,
                            name: askedInfo.form.name,  
                            info: [askedInfo]
                        });
                    }
                })
            }
            if (circle.node.paymentModels && circle.node.paymentModels.length > 0) {
                paymentModelList.push(circle.node)
            }
        });
        
        this.setState({
            circlesWithAskedInformation: circleAskedInformationList,
            circlesWithAskedFees: paymentModelList,
        })
    }

    _setEditMode = () => {
        this.setState({
            editInfo: !this.state.editInfo
        })
    }

    _handleCancel = () => {
        this.setState({
            editInfo: false
        })
    }

    _handleSave = (circle) => {
        const idVar = circle.id;
        const userId = this.props.user.id ;
        const viewer = this.props.viewer ;

        this.setState({
            isLoading: true
        })
        let answersVar = [{
            userId,
            filledInformation: circle.membersInformation.map(answer => (
                typeof answer.value !== 'undefined'
                ?   {
                        id: answer.information,
                        value: answer.value
                    }
                :   false
            )).filter(i => Boolean(i))
        }]

        RelayStore.commitUpdate(
            new UpdateFilledInformationMutation({
              viewer,
              idVar,
              answersVar
            }),
            {
              onFailure: error => {
                this.msg.show(localizations.popup_editCircle_update_failed, {
                  time: 2000,
                  type: 'error',
                });
                setTimeout(() => this.msg.removeAll(), 2000);
                let errors = JSON.parse(error.getError().source);
                console.log(errors);
                this.setState({
                    isLoading: false
                })
              },
              onSuccess: (response) => {
                this.msg.show(localizations.popup_editCircle_update_success, {
                  time: 2000,
                  type: 'success',
                });
                setTimeout(() => this.msg.removeAll(), 2000);
                this.setState({
                    editInfo: false,
                    isLoading: false
                })
              },
            }
        )
    }

   _handleUpdateValue = (circle, askedInfo, value) => {
        let newState = this.state.circlesWithAskedInformation ;
        let userId = this.props.user.id; 
        
        if (askedInfo.type === 'NUMBER' && value.length > 5)
            return ;
        if (askedInfo.type === 'PHONE_NUMBER' && value.length > 10)
            return ;

        newState.forEach((newStateCircle, i) => {
            if (newStateCircle.circle.id === circle.id) {
                let index = newStateCircle.circle.membersInformation.findIndex(memberInfo => {
                    return (memberInfo.user.id === userId && memberInfo.information === askedInfo.id) 
                })
                if (index >= 0) {
                    if (askedInfo.type === 'BOOLEAN')
                        newState[i].circle.membersInformation[index].value = value ? "true" : "false";
                    else 
                        newState[i].circle.membersInformation[index].value = value;
                }   
                else {
                    newState[i].circle.membersInformation.push({
                        information: askedInfo.id,
                        user: this.props.user,
                        value: askedInfo.type === 'BOOLEAN' ? value ? "true" : "false" : value
                    })
                }
            }
        });

        this.setState({
            circlesWithAskedInformation: newState
        })
    }

    _getRowValue = (userId, membersInfo, askedInfo) => {
        let result ; 
        membersInfo.forEach(info => {
            if (info.user.id === userId && info.information === askedInfo.id) {
                if (askedInfo.type === "BOOLEAN")
                    result = info.value === "true" ? true : false ; 
                else if (askedInfo.type === "NUMBER")
                    result = parseInt(info.value)
                else 
                    result = info.value;
            }                
        })
        
        if (typeof result === "undefined") {
            if (askedInfo.type === "BOOLEAN")
                result = false ; 
            else 
                result = ""
        }
        
        return result
    }

    _renderRowValue = (userId, membersInfo, askedInfo) => {
        let result ; 
        membersInfo.forEach(info => {
            if (info.user.id === userId && info.information === askedInfo.id && typeof info.value !== 'undefined') {
                if (askedInfo.type === "BOOLEAN")
                    result = info.value === "true" ? localizations.circle_yes : localizations.circle_no ; 
                else if (askedInfo.type === "NUMBER")
                    result = parseInt(info.value)
                else if (askedInfo.type === "DATE")
                    result = moment(new Date(info.value)).format('DD MMM YYYY')
                else 
                    result = info.value;
            }                
        })
        
        if (typeof result === "undefined") {
            if (askedInfo.type === "BOOLEAN")
                result = localizations.circle_no ; 
            else 
                result = ""
        }
        
        return result
    }

    _renderInputField = (askedInfo, circle) => {
        const { user } = this.props;

        if (this.state.editInfo) {
            switch(askedInfo.type) {
                case 'TEXT': return (
                    <InputText 
                        maxLenght={40} 
                        value={this._getRowValue(user.id, circle.membersInformation, askedInfo)}
                        onChange={(e) => this._handleUpdateValue(circle, askedInfo, e.target.value)}
                    />
                )
                case 'NUMBER': return (
                    <InputNumber 
                        max={99999} 
                        value={this._getRowValue(user.id, circle.membersInformation, askedInfo)}
                        onChange={(e) => this._handleUpdateValue(circle, askedInfo, e.target.value)}
                    />
                )
                case 'BOOLEAN': return (
                    <InputCheckbox 
                        checked={this._getRowValue(user.id, circle.membersInformation, askedInfo)}
                        onChange={(e) => this._handleUpdateValue(circle, askedInfo, e.target.checked)}
                    />
                )
                case 'ADDRESS': return (
                    <InputAddress 
                        value={this._getRowValue(user.id, circle.membersInformation, askedInfo)}
                        onChange={(e) => this._handleUpdateValue(circle, askedInfo, e.label)}
                    />
                )
                case 'DATE': return (
                    <div style={{width: 200}}>
                        <InputDate 
                            value={this._getRowValue(user.id, circle.membersInformation, askedInfo)}
                            onChange={(e) => this._handleUpdateValue(circle, askedInfo, e._d)}
                        />
                    </div>
                )
                case 'PHONE_NUMBER': return (
                    <InputPhone 
                        value={this._getRowValue(user.id, circle.membersInformation, askedInfo)}
                        onChange={(e) => this._handleUpdateValue(circle, askedInfo, e.target.value)}
                    />
                )
                default:
                    return (
                        <InputText 
                            maxLenght={40} 
                            value={this._getRowValue(user.id, circle.membersInformation, askedInfo)}
                            onChange={(e) => this._handleUpdateValue(circle, askedInfo, e.target.value)}
                        />
                    )
            }
        }
        else {
            return (
                <label style={styles.longLabel}>
                    {this._renderRowValue(user.id, circle.membersInformation, askedInfo)}
                </label>
            )
        }
    }

    _isNotMissingInfo = (circle) => {
	    let userId = this.props.user.id;

	    let isNotMissingCircle = false;
	    circle.paymentModels.forEach(paymentModel => {
		    let paymentModelAskedInformation = [];
        paymentModel.conditions.forEach(condition => {
          condition.conditions.forEach(cond => {
            paymentModelAskedInformation = paymentModelAskedInformation.concat(cond.askedInformation)
          })
        })
        let isNotMissing = true;
        paymentModelAskedInformation.forEach(askedInfo => {
          if (askedInfo.type !== 'BOOLEAN' && (!circle.membersInformation || circle.membersInformation.findIndex(memberInfo => userId === memberInfo.user.id && memberInfo.information === askedInfo.id) < 0))
            isNotMissing = false
        })
        isNotMissingCircle = isNotMissing || isNotMissingCircle;
      })
      return isNotMissingCircle
    }

    _getAmoutToPay = (circle, paymentModel) => {
        let userId = this.props.user.id ;

        let paymentModelAskedInformation = [];
        paymentModel.conditions.forEach(condition => {
            condition.conditions.forEach(cond => {
                paymentModelAskedInformation = paymentModelAskedInformation.concat(cond.askedInformation)
            })
        })       

        let didUserFillAll = true ;
        paymentModelAskedInformation.forEach(askedInfo => {
            if (askedInfo.type !== 'BOOLEAN' && (!circle.membersInformation || circle.membersInformation.findIndex(memberInfo => userId === memberInfo.user.id && memberInfo.information === askedInfo.id) < 0))
                didUserFillAll = false
        })

        if (!didUserFillAll)
            return <span>{localizations.circle_member_payment_missing_info}</span>
        else {
            let conditionListFilled = null;
            let numberOfValidAnswer = 0 ;  
            let userInformation = circle.membersInformation.filter(info => info.user.id === userId)
            
            paymentModel.conditions.forEach(condition => {
                let conditionAreValidated = true; 
                let currentNumberOfValidAnswer = 0 ; 

                condition.conditions.forEach(cond => {
                    let memberInfoIndex = userInformation.findIndex(userInfo => userInfo.information === cond.askedInformation.id);

                    if (cond.askedInformation.type === 'BOOLEAN' || this.isConditionFilled(cond, userInformation[memberInfoIndex])) {
                        currentNumberOfValidAnswer++ ;
                    }
                    else 
                        conditionAreValidated = false;
                })
                if (conditionAreValidated && currentNumberOfValidAnswer > numberOfValidAnswer) {
                    numberOfValidAnswer = currentNumberOfValidAnswer;
                    conditionListFilled = condition
                }
            })
            if  (conditionListFilled)
                return <span>{conditionListFilled.price.cents / 100 + ' ' + conditionListFilled.price.currency} </span>
            else    
                return <span>-</span>
        }
    }

    isConditionFilled = (condition, answer) => {
        
        switch(condition.askedInformation.type) {
            case 'NUMBER': {
                switch(condition.askedInformationComparator)  {
                    case '≤': {
                        return (parseInt(answer.value) <= condition.askedInformationComparatorValue)
                    }
                    case '<': {
                        return (parseInt(answer.value) < condition.askedInformationComparatorValue)
                    }
                    case '=': {
                        return (parseInt(answer.value) === condition.askedInformationComparatorValue)
                    }
                    case '>': {
                        return (parseInt(answer.value) > condition.askedInformationComparatorValue)
                    }
                    case '≥': {
                        return (parseInt(answer.value) >= condition.askedInformationComparatorValue)
                    }
                }
            }
            case 'BOOLEAN': {
                if ((condition.askedInformationComparatorValue === 1 && answer.value === 'true') || (condition.askedInformationComparatorValue === 0 && answer.value === 'false'))
                    return true ; 
                else 
                    return false
            }
            case 'DATE': {
                switch(condition.askedInformationComparator)  {
                    case '≤': {
                        if (moment(answer.value).isBefore(condition.askedInformationComparatorDate))
                            return true; 
                        else 
                            return false;
                    }
                    case '≥': {
                        if (moment(answer.value).isAfter(condition.askedInformationComparatorDate))
                            return true; 
                        else 
                            return false;
                    }
                }
            }
            default: return false;
        }
    }

    render() {
        const { user, viewer } = this.props;
        const { circlesWithAskedInformation, circlesWithAskedFees, editInfo, isLoading } = this.state ;
        
		return(
            user.circlesUserIsIn && user.circlesUserIsIn.edges && user.circlesUserIsIn.edges.length > 0 && circlesWithAskedInformation.length > 0 
            ?   <section style={styles.container}>	
                    <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
                    
                    <div style={styles.pageHeader}>
                        {localizations.info_sharedInfo}
                    </div>
                    <div style={styles.explaination}>
                        {localizations.info_sharedInfo_explanaition}
                    </div>
                    
                    <div style={styles.rowHeader}>
                        <div style={styles.header}>
                            {localizations.info_circleInfo}
                        </div>
                        <div style={styles.editButton} onClick={this._setEditMode}>
                            {localizations.info_edit}
                        </div> 
                    </div>
                    
                    {circlesWithAskedInformation.map((circleWithAskedInfo, index) => (
                        <div style={styles.col} key={index}>
                            {circleWithAskedInfo.forms.map(form => (
                                <div style={styles.bankAccountContainer} key={form.id}>
                                    <label style={styles.subHeader}>
                                        {localizations.formatString(localizations.info_formName, form.name, circleWithAskedInfo.circle.owner.pseudo)}
                                    </label>
                                    {form.info.map(askedInfo => (
                                        askedInfo.filledByOwner 
                                        ?   false
                                        :   <div style={styles.row} key={askedInfo.id}>
                                                <label style={styles.label}>
                                                    {askedInfo.name}
                                                </label>
                                                {this._renderInputField(askedInfo, circleWithAskedInfo.circle)}
                                            </div>
                                    )).filter(i => Boolean(i))}
                                </div>
                            ))}
                            
                            { editInfo && 
                                <div style={styles.row}>
                                    <label style={styles.label}></label>
                                    {	isLoading 
                                    ?   <ReactLoading type='cylon' color={colors.blue} /> 
                                    :    <section>
                                            <button style={appStyles.blueButton} onClick={() => this._handleSave(circleWithAskedInfo.circle)}>
                                                {localizations.info_update}
                                            </button> 
                                            <button style={appStyles.grayButton} onClick={this._handleCancel}>
                                                {localizations.info_cancel}
                                            </button>
                                        </section> 
                                    }
                                </div> 
                            }
                        </div>
                    ))}

                    <div style={styles.rowHeader}>
                        <div style={styles.header}>
                            {localizations.circle_member_asked_payment}
                        </div>
                    </div>

                    {circlesWithAskedFees.map(circle => (
                        <div style={styles.col} key={circle.id}>
                            <label style={styles.subHeader}>
                                {circle.name + ' ' + localizations.circle_owner + ' ' + circle.owner.pseudo}
                            </label>
                            <div style={{...styles.bankAccountExplanation, color: colors.red, marginTop: 5}}>
                                {localizations.circle_member_payment_bankAccount_explanation2}
                            </div>
                            {circle.paymentModels.map(paymentModel => (
                                <div key={paymentModel.id}>
                                    <div style={styles.row} >
                                        <label style={styles.longLabel}>
                                            {paymentModel.name} : {this._getAmoutToPay(circle, paymentModel)}
                                        </label>
                                    </div>
                                </div>
                            ))}
                            {this._isNotMissingInfo(circle) && circle.owner.bankAccount &&
                                <div style={styles.bankAccountContainer}>
                                    <div style={styles.bankAccountTitleContainer}>
                                        <div style={styles.bankAccountTitle}>
                                            {localizations.circle_member_payment_bankAccount}
                                        </div>
                                        <div style={styles.bankAccountExplanation}>
                                            {localizations.circle_member_payment_bankAccount_explanation}
                                        </div>
                                    </div>
	                                <CirclePaymentReference
		                                viewer={viewer}
		                                circleId={circle.id}
	                                />
                                    <div style={{...styles.bankAccountDetailRow, marginTop: 5}}>{localizations.bank_ownerName + ': ' + circle.owner.bankAccount.ownerName}</div>
                                    <div style={{...styles.bankAccountDetailRow, marginTop: 5}}>{localizations.newSportunity_add_bank_account_popup_address_line_1 + ': ' + circle.owner.bankAccount.addressLine1}</div>
                                    {circle.owner.bankAccount.addressLine2 && <div style={{...styles.bankAccountDetailRow, marginTop: 5}}>{localizations.bank_addressLine2 + ': ' + circle.owner.bankAccount.addressLine2}</div>}
                                    <div style={{...styles.bankAccountDetailRow, marginTop: 5}}>{localizations.bank_city + ': ' + circle.owner.bankAccount.city}</div>
                                    <div style={{...styles.bankAccountDetailRow, marginTop: 5}}>{localizations.bank_postalCode + ': ' + circle.owner.bankAccount.postalCode}</div>
                                    <div style={{...styles.bankAccountDetailRow, marginTop: 5}}>{localizations.bank_country + ': ' + circle.owner.bankAccount.country}</div>                                    
                                    <div style={{...styles.bankAccountDetailRow, marginTop: 5}}>{localizations.bank_IBAN + ': ' + circle.owner.bankAccount.IBAN}</div>
                                    <div style={{...styles.bankAccountDetailRow, marginTop: 5}}>{localizations.bank_BIC + ': ' + circle.owner.bankAccount.BIC}</div>
                                </div>
                            }
                        </div>
                    ))}
                </section>
            :   <section style={styles.container}>	
                    <div style={styles.pageHeader}>{localizations.info_sharedInfo}</div>
                    <div style={styles.explaination}>
                        {localizations.info_sharedInfo_nothing}
                    </div>
                </section>        
		)
	}
}

export default Relay.createContainer(CirclesInformation, {
    fragments: {
        user: () => Relay.QL`
            fragment on User {
                id,
                circlesUserIsIn (last: 100) {
                    edges {
                        node {
                            id,
                            name
                            owner {
                                pseudo
                                bankAccount {
                                    addressLine1,
                                    addressLine2,
                                    city,
                                    postalCode,
                                    country,
                                    ownerName,
                                    IBAN,
                                    BIC
                                }
                            }
                            askedInformation {
                                id, 
                                name,
                                type,
                                filledByOwner
                                form {
                                    id
                                    name
                                }
                            }
                            membersInformation {
                                id,
                                information,
                                user {
                                    id,
                                }
                                value
                            }
                            paymentModels {
                                id,
                                name,
                                conditions {
                                    id,
                                    name, 
                                    price {
                                        cents,
                                        currency
                                    }
                                    conditions {
                                        askedInformation {
                                            id
                                            type
                                        }
                                        askedInformationComparator
                                        askedInformationComparatorValue
                                        askedInformationComparatorDate
                                    }
                                }
                            }
                        }
                    }
                }
            }
        `,
        viewer: () => Relay.QL`
        fragment on Viewer {
            ${UpdateFilledInformationMutation.getFragment('viewer')}
            ${CirclePaymentReference.getFragment('viewer')}
        }`
    }
})
				