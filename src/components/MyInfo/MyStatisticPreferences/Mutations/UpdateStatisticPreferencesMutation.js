import Relay from 'react-relay';

export default class UpdateStatisticPreferencesMutation extends Relay.Mutation {

    getMutation() {
        return Relay.QL`mutation Mutation{
            updateStatisticPreferences
        }`;
    }

    getVariables() {
        return {
            userID: this.props.userIDVar,
            statisticPreferences: {
                private: this.props.privateVar,
                userStats: this.props.userStatsVar,
                sportunityStats: this.props.sportunityStatsVar,
                isManOfTheGameActivated: this.props.isManOfTheGameActivatedVar,
            },

        };
    }

    getFatQuery() {
        return Relay.QL`
        fragment on updateStatisticPreferencesPayload {
            clientMutationId,
            viewer {
                id
                statisticPreferences
            }
        }
        `;
    }

    getConfigs() {
        return [{
        type: 'FIELDS_CHANGE',
            fieldIDs: {
                viewer: this.props.viewer.id,
            },
        }];
    }

    static fragments = {
        viewer: () => Relay.QL`
        fragment on Viewer {
            id,
        }
        `,
    };

}
