import React from 'react'
import Relay from 'react-relay'
import Radium from 'radium'
import AlertContainer from 'react-alert'

import styles from '../Styles'
import ParticipantStatPrefs from './ParticipantStatPrefs';

import UpdateStatisticPreferencesMutation from './Mutations/UpdateStatisticPreferencesMutation';
import UpdateUserMutation from './Mutations/UpdateUserMutation';

import { appStyles, fonts, colors } from '../../../theme'

import localizations from '../../Localizations'

class MyStatisticPreferences extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            language: localizations.getLanguage(),
            publicChecked: false,
            areStatisticsActivated: false,
            shouldDisplayMainValidateButton: false,
            isManOfTheGameActivated: false,
        };

        this.alertOptions = {
            offset: 14,
            position: 'top right',
            theme: 'light',
            transition: 'fade',
            time: 0,
        };
    }

    componentDidMount = () => {
        if (this.props.user && this.props.user.id) {
            this.props.relay.setVariables({
                userID: this.props.user.id
            })
        }
        setTimeout(() => this.setState({ loading: false }), 1500)
    }

    componentWillReceiveProps = (nextProps) => {
        let publicChecked = nextProps.viewer.statisticPreferences.private;
        this.setState({
            publicChecked: !publicChecked,
            areStatisticsActivated: nextProps.user.areStatisticsActivated,
            isManOfTheGameActivated: nextProps.viewer.statisticPreferences.isManOfTheGameActivated,
        })
    }

    _setLanguage = (language) => {
        this.setState({ language: language })
    }

    _handleUpdateUserPreferences = (userStatPrefs) => {
        let params = {
            userIDVar: this.props.user.id,
            userStatsVar: userStatPrefs,
            viewer: this.props.viewer,
        } ;
        this.props.relay.commitUpdate(
            new UpdateStatisticPreferencesMutation(params),
            {
                onSuccess: () => {
                    this.msg.show(localizations.myStatPrefs_update_success, {
                        time: 2000,
                        type: 'success',
                    });
                    setTimeout(() => this.msg.removeAll(), 2000);
                },
                onFailure: (error) => {
                    this.msg.show(localizations.myStatPrefs_update_failed, {
                        time: 2000,
                        type: 'error',
                    });
                    setTimeout(() => this.msg.removeAll(), 2000);
                    console.log(error);
                },
            }
        );
    }

    _handleUpdatePrivatePreferences = (e) => {
        this.setState({
            publicChecked: e.target.checked,
            shouldDisplayMainValidateButton: true
        })
    }

    _handleUpdateStatActivation = (e) => {
        this.setState({
            areStatisticsActivated: e.target.checked,
            shouldDisplayMainValidateButton: true
        })
    }

    _handleUpdateManOfTheGameActivated = (e) => this.setState({
      isManOfTheGameActivated: e.target.checked,
      shouldDisplayMainValidateButton: true
    })

    _handleSavePrivatePreferences = () => {
        if (this.state.areStatisticsActivated !== this.props.user.areStatisticsActivated) {
            let params = {
                userIDVar: this.props.user.id,
                areStatisticsActivatedVar: this.state.areStatisticsActivated,
                viewer: this.props.viewer
            } ;
            this.props.relay.commitUpdate(
                new UpdateUserMutation(params),
                {
                    onSuccess: () => {
                        if (this.props.viewer.statisticPreferences && this.state.publicChecked !== !this.props.viewer.statisticPreferences.private) {
                            let params = {
                                userIDVar: this.props.user.id,
                                privateVar: !this.state.publicChecked,
                                isManOfTheGameActivatedVar: this.state.isManOfTheGameActivated,
                                viewer: this.props.viewer
                            } ;
                            this.props.relay.commitUpdate(
                                new UpdateStatisticPreferencesMutation(params),
                                {
                                    onSuccess: () => {
                                        this.msg.show(localizations.myStatPrefs_update_success, {
                                            time: 2000,
                                            type: 'success',
                                        });
                                        this.setState({
                                            shouldDisplayMainValidateButton: false
                                        })
                                        setTimeout(() => this.msg.removeAll(), 2000);
                                    },
                                    onFailure: (error) => {
                                        this.msg.show(localizations.myStatPrefs_update_failed, {
                                            time: 2000,
                                            type: 'success',
                                        });
                                        setTimeout(() => this.msg.removeAll(), 2000);
                                        console.log(error);
                                        this.setState({
                                            shouldDisplayMainValidateButton: false
                                        })
                                    },
                                }
                            );
                        }
                        else {
                            this.msg.show(localizations.myStatPrefs_update_success, {
                                time: 2000,
                                type: 'success',
                            });
                            setTimeout(() => this.msg.removeAll(), 2000);
                            this.setState({
                                shouldDisplayMainValidateButton: false
                            })
                        }
                    },
                    onFailure: (error) => {
                        this.msg.show(localizations.myStatPrefs_update_failed, {
                            time: 2000,
                            type: 'success',
                        });
                        setTimeout(() => this.msg.removeAll(), 2000);
                        console.log(error);
                    },
                }
            );
        }
        else if (this.props.viewer.statisticPreferences &&
          (this.state.publicChecked !== !this.props.viewer.statisticPreferences.private) ||
          (this.state.isManOfTheGameActivated !== this.props.viewer.statisticPreferences.isManOfTheGameActivated)) {
            let params = {
                userIDVar: this.props.user.id,
                privateVar: !this.state.publicChecked,
                isManOfTheGameActivatedVar: this.state.isManOfTheGameActivated,
                viewer: this.props.viewer
            } ;
            this.props.relay.commitUpdate(
                new UpdateStatisticPreferencesMutation(params),
                {
                    onSuccess: () => {
                        this.msg.show(localizations.myStatPrefs_update_success, {
                            time: 2000,
                            type: 'success',
                        });
                        setTimeout(() => this.msg.removeAll(), 2000);
                        this.setState({
                            shouldDisplayMainValidateButton: false
                        })
                    },
                    onFailure: (error) => {
                        this.msg.show(localizations.myStatPrefs_update_failed, {
                            time: 2000,
                            type: 'success',
                        });
                        setTimeout(() => this.msg.removeAll(), 2000);
                        console.log(error);
                        this.setState({
                            shouldDisplayMainValidateButton: false
                        })
                    },
                }
            );
        }
        else {
            this.msg.show(localizations.myStatPrefs_update_success, {
                time: 2000,
                type: 'success',
            });
            setTimeout(() => this.msg.removeAll(), 2000);
            this.setState({
                shouldDisplayMainValidateButton: false
            })
        }
    }


    render() {

        const { viewer, user } = this.props;

        return (
            <div>
                <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
                <div style={styles.container}>
                    <div style={styles.pageHeader}>{localizations.myStatPrefs_title}</div>
                    <div style={styles.wrapper}>
                        {viewer.statisticPreferences &&
                            <div>
                                <div>
                                    <div style={styles.row}>
                                        <div style={styles.subHeader}>
                                            {localizations.myStatPrefs_activation_label}
                                        </div>
                                        <input
                                            type="checkbox"
                                            style={styles.checkboxInput}
                                            checked={this.state.areStatisticsActivated}
                                            onChange={this._handleUpdateStatActivation}
                                            />
                                    </div>
                                    <div style={styles.explaination} >
                                        {localizations.myStatPrefs_activation_explanation}
                                    </div>
                                </div>
                                {user.areStatisticsActivated &&
                                    <div>
                                        <div style={styles.row}>
                                            <div style={styles.subHeader}>
                                                {localizations.myStatPrefs_privacy_label}
                                            </div>
                                            <input
                                                type="checkbox"
                                                style={styles.checkboxInput}
                                                checked={this.state.publicChecked}
                                                onChange={this._handleUpdatePrivatePreferences}
                                                />
                                        </div>
                                        <div style={styles.explaination} >
                                            {localizations.myStatPrefs_privacy_explanation}
                                        </div>

                                        <div style={styles.row}>
                                            <div style={styles.subHeader}>
                                                {localizations.myStatPrefs_manofthegame_label}
                                            </div>
                                            <input
                                              type="checkbox"
                                              style={styles.checkboxInput}
                                              checked={this.state.isManOfTheGameActivated}
                                              onChange={this._handleUpdateManOfTheGameActivated}
                                            />
                                        </div>
                                        <div style={styles.explaination} >
                                            {localizations.myStatPrefs_manofthegame_explanation}
                                        </div>

                                    </div>
                                }
                                {this.state.shouldDisplayMainValidateButton &&
                                    <button style={appStyles.blueButton} onClick={this._handleSavePrivatePreferences}>
                                        {localizations.info_update}
                                    </button>
                                }

                            </div>
                        }
                        <div style={styles.rightSide}>
                            {user.areStatisticsActivated && viewer.statisticPreferences &&
                                <ParticipantStatPrefs
                                    statisticPreferences={viewer.statisticPreferences}
                                    _handleUpdatePreferences={this._handleUpdateUserPreferences}
                                    {...this.state}
                                />
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Relay.createContainer(Radium(MyStatisticPreferences), {
    initialVariables: {
        userID: null
    },
    fragments: {
        viewer: () => Relay.QL`
            fragment on Viewer {
                id
                ${UpdateStatisticPreferencesMutation.getFragment('viewer')}
                statisticPreferences (userID: $userID) {
                    private,
                    isManOfTheGameActivated,
                    userStats {
                        stat0 {name}
                        stat1 {name}
                        stat2 {name}
                        stat3 {name}
                        stat4 {name}
                        stat5 {name}
                    }
                }
            }
        `,
        user: () => Relay.QL`
            fragment on User {
                id
                areStatisticsActivated
                ${UpdateUserMutation.getFragment('user')}
            }
        `
    },
});
