import Relay from 'react-relay';
/**
*  Add new bank account mutation
*/
export default class UpdateLegalAdvancedMutation extends Relay.Mutation {
  /**
  *  Mutation
  */
  getMutation() {
    return Relay.QL`mutation Mutation{
      upUser
    }`;
  }
  /**
  *  Variables
  */
  getVariables = () => (
    {
      userID: this.props.userIDVar,
      user: {
        address: this.props.addressVar,
        email: this.props.emailVar,
        business: this.props.businessVar,
        shouldDeclareVAT: this.props.shouldDeclareVATVar
      },
    }
  )
  /**
  *  Fat query
  */
  getFatQuery() {
    return Relay.QL`
      fragment on upUserPayload {
        viewer
        user {
          id
          profileType
          email
          firstName
          lastName
          birthday
          nationality
          occupation
          incomeRange
          shouldDeclareVAT
          address {
            address
            city
            country
            zip
          }
          business {
            businessName
            businessEmail
            VATNumber
            headquarterAddress {
              address
              city
              country
              zip
            }
          }
        }
        clientMutationId
      }
    `;
  }

  /**
  *  Config
  */
  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        user: this.props.userIDVar,
      },
    }];
  }

  static fragments = {
    user: () => Relay.QL`
      fragment on User {
        id
        profileType
        email
        firstName
        lastName
        birthday
        nationality
        occupation
        incomeRange
        shouldDeclareVAT
        address {
          address
          city
          country
          zip
        }
        business {
          businessName
          businessEmail
          VATNumber
          headquarterAddress {
            address
            city
            country
            zip
          }
        }
      }
    `,
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
        me {
          id
        }
      }
    `,
  };
}