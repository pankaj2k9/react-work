import Relay from 'react-relay';

export default class UpdateUserProfileMutation extends Relay.Mutation {

  getMutation() {
    return Relay.QL`mutation Mutation{
      upUser
    }`;
  }

  getVariables() {
    return {
      userID: this.props.userIDVar,
      user: {
        email: this.props.emailVar,
        lastName: this.props.lastNameVar, 
        firstName: this.props.firstNameVar,
        address: this.props.addressVar,
      },

    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on upUserPayload {
        clientMutationId,
        viewer {
          id
          me {
            firstName,
            lastName,
            address {
              address,
              country,
              city,
              zip,
              position
            }
          }
        }
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
        fieldIDs: {
          viewer: this.props.viewer.id,
        },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
        me {
          id
        }
      }
    `,
  };

}
