import React from 'react'
import Relay from 'react-relay'
import { browserHistory } from 'react-router'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Loading from '../common/Loading/Loading'
import Header from '../common/Header/Header'
import Footer from '../common/Footer/Footer'
import LeftSide from './LeftSide'
import Info from './Info'
import Payment from './Payment'
import BankAccount from './BankAccount'
import Password from './Password'
import Notification from './Notification'
import MyStatisticPreferences from './MyStatisticPreferences'
import MyUserPreferences from './MyUserPreferences';
import AccessRights from './AccessRights'
import Wallet from './Wallet/index'
import CirclesInformation from './CirclesInformation';
import UpdateNaturalBasicMutation from './UpdateNaturalBasicMutation'
import UpdateNaturalAdvancedMutation from './UpdateNaturalAdvancedMutation'
import UpdateLegalBasicMutation from './UpdateLegalBasicMutation'
import UpdateLegalAdvancedMutation from './UpdateLegalAdvancedMutation'
import localizations from '../Localizations'
import SynchronizeCalendar from './SynchronizeCalendar'

import * as types from '../../actions/actionTypes.js';

import Radium from 'radium';

let styles

class MyInfo extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
      loading: true,
			activeSection: 'my-info',
			language: localizations.getLanguage(),
			mobileVersion: false,
    }
	}

	_setLanguage = (language) => {
    this.setState({ language: language })
  }

	_changeSection = (name) => {
		this.setState({
			activeSection: name,
		})
	}

	componentDidMount = () => {
		
		if (this.props.location.pathname.indexOf('notification-preferences') >= 0) {
			let splitted = this.props.location.pathname.split('/') ;
			if (splitted[2]) {
				this.props.route.updateToken(splitted[2]);
				this.setState({mobileVersion: true})
				this._changeSection('notification')
			}
			else {
				this.props._loggedFromPage('/preferences')
				browserHistory.push(`/login`);
			}			
		}
		else if (!this.props.viewer.me) {
			this.props._loggedFromPage('/preferences')
			browserHistory.push(`/login`);
		}
		else if (this.props.location.pathname === '/preferences')
			this._changeSection('notification')
		else if (this.props.location.pathname === '/my-wallet')
			this._changeSection('wallet')
		else if (this.props.location.pathname === '/my-shared-info')
			this._changeSection('circle_fees')

		setTimeout(() => this.setState({ loading: false }), 1500);
	}

	_isProfileComplete = () => {
		if (this.props.viewer && this.props.viewer.me) {
			const user = this.props.viewer.me;
			return (user.firstName && user.lastName && user.address)
		}
	}

	_currentSection = () => {
		const { viewer } = this.props
		const defProps = { viewer, user: viewer.me }
		const isProfileComplete=this._isProfileComplete()
    
		switch(this.state.activeSection) {
			case 'payment': return <Payment {...this.state} {...defProps}  />
			case 'bank': return <BankAccount {...this.state} {...defProps} />
			case 'password': return <Password {...this.state} {...defProps} />
			case 'notification': return <Notification {...this.state} {...defProps} language={this.state.language}/>
			case 'statistics': return <MyStatisticPreferences {...this.state} {...defProps} language={this.state.language} />
			case 'user-preferences': return <MyUserPreferences {...this.state} {...defProps} language={this.state.language} />
			case 'share-access': return <AccessRights {...this.state} {...defProps} language={this.state.language}/>
			case 'wallet': return <Wallet {...this.state} {...defProps} />
			case 'circle_fees': return <CirclesInformation {...this.state} {...defProps} />
			case 'sync-calendar': return <SynchronizeCalendar {...this.state} {...defProps} />;
			case 'my-info':
			default:
				return <Info {...this.state} {...defProps} isProfileComplete={isProfileComplete}/>
		}
	}

	render() {
		const { viewer } = this.props
		const isProfileComplete = this._isProfileComplete();
		if (this.state.loading) {
      return(<Loading />)
    }
		return(
			<div>

				<div style={styles.container}>
					{!this.state.mobileVersion
					? viewer.me 
						? <Header user={viewer.me} viewer={viewer} {...this.state} />
						: <Header user={null} {...this.state} /> 
					: null
					}
				</div>
				<div style={styles.wrapper}>
					{!this.state.mobileVersion
						? <div style={styles.leftSide}>
								<LeftSide activeSection={this.state.activeSection}
													onChangeSection={this._changeSection}
													isProfileComplete={isProfileComplete}
													viewer={viewer}
													{...this.state}
													
								/>
							</div>
						: null
					}
					<div style={styles.rightSide} >
						{ this._currentSection() }
					</div>
				</div>
				
				{!this.state.mobileVersion
				? viewer.me 
						? <Footer 
								onUpdateLanguage={this._setLanguage} 
								viewer={viewer} 
								user={viewer.me}/>
						: <Footer 
								onUpdateLanguage={this._setLanguage} 
								viewer={viewer} 
								user={null}/>
					: null
				}
				
			</div>
		)
	}
}

styles = {
  container: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
	wrapper: {
    width: 1000,
    margin: '35px auto',
    display: 'flex',
    flexDirection: 'row',
		fontFamily: 'Lato',
		'@media (max-width: 960px)': {
	      width: '100%',
	    },
	    '@media (max-width: 480px)': {
	      display: 'block',
	    }
  },
	leftSide: {
		width: 200,
		display: 'flex',
		flexDirection: 'column',
		fontSize: 16,
	},
	rightSide: {
		paddingLeft: 60,
		paddingRight: 10,
		paddingTop: 10,
		flex: 1,
		//width: 800,
		'@media (max-width: 960px)': {
	      width: '100%',
	      paddingRight: 30,
	    },
	    '@media (max-width: 480px)': {
	    	paddingLeft: 10,
	    	paddingRight: 10,
	    }
	},
}

const _loggedFromPage = (value) => ({
  type: types.LOGGED_IN_FROM_PAGE,
  value,
})

const dispatchToProps = (dispatch) => ({
  _loggedFromPage: bindActionCreators(_loggedFromPage, dispatch),
})

const stateToProps = (state) => ({
})

let ReduxContainer = connect(
  stateToProps,
  dispatchToProps
)(Radium(MyInfo));

export default Relay.createContainer(Radium(ReduxContainer), {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
				${Header.getFragment('viewer')}
				${Footer.getFragment('viewer')}
				${Info.getFragment('viewer')}
				${Payment.getFragment('viewer')}
				${BankAccount.getFragment('viewer')}
				${MyStatisticPreferences.getFragment('viewer')}
				${AccessRights.getFragment('viewer')}
				${UpdateNaturalBasicMutation.getFragment('viewer')}
				${UpdateNaturalAdvancedMutation.getFragment('viewer')}
				${UpdateLegalBasicMutation.getFragment('viewer')}
				${UpdateLegalAdvancedMutation.getFragment('viewer')}
				${Wallet.getFragment('viewer')}
				${Notification.getFragment('viewer')}
				${Password.getFragment('viewer')}
				${CirclesInformation.getFragment('viewer')}
				${MyUserPreferences.getFragment('viewer')}
				${SynchronizeCalendar.getFragment('viewer')}
        me {
					firstName,
					lastName,
					isSubAccount
					address {
						address
						city
						country
					}
					${Header.getFragment('user')}
					${Footer.getFragment('user')}
					${Info.getFragment('user')}
					${BankAccount.getFragment('user')}
					${Payment.getFragment('user')}
					${Notification.getFragment('user')}
					${Password.getFragment('user')}
					${MyStatisticPreferences.getFragment('user')}
					${MyUserPreferences.getFragment('user')}
					${AccessRights.getFragment('user')}
					${CirclesInformation.getFragment('user')}
					profileType
				}

			}
    `,
  },
});
