import Relay from 'react-relay'

export default class UpdateSynchronizeSettings extends  Relay.Mutation {
  getMutation() {
    return Relay.QL`mutation Mutation{
      upUser
    }`;
  }

  getVariables() {
    return {
      userID: this.props.userIDVar,
      user: {
        calendar: {
          preferences: {
            own_synchronized_status: this.props.own_synchronized_statusVar,
          }
        }
      }
    }
  }

  getFatQuery() {
    return Relay.QL`
    fragment on upUserPayload {
      clientMutationId,
      viewer
      user {
        id,
        calendar {
          preferences {
            own_synchronized_status
          }
        }
      }
    }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
        fieldIDs: {
          user: this.props.userIDVar,
        },
    }];
  }

  static fragments = {
    user: () => Relay.QL`
      fragment on User{
        id,
        calendar {
          preferences {
            own_synchronized_status
          }
        }
      }
    `
  }
}