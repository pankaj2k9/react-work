import Relay from 'react-relay';
/**
*  Add new bank account mutation
*/
export default class RemoveCardMutation extends Relay.Mutation {
  /**
  *  Mutation
  */
  getMutation() {
    return Relay.QL`mutation Mutation{
      deletePaymentMethod 
    }`;
  }
  /**
  *  Variables
  */
  getVariables = () => (
    {
      paymentMethodId: this.props.paymentMethodIdVar,
    }
  )
  /**
  *  Fat query
  */
  getFatQuery() {
    return Relay.QL`
      fragment on deletePaymentMethodPayload {
        viewer {
          me {
            paymentMethods {
              id
              cardType
              cardMask
              expirationDate
            }
          }
        },
        clientMutationId
      }
    `;
  }

  /**
  *  Config
  */
  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        viewer: this.props.viewer.id,
      },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
        me {
          paymentMethods {
            id
            cardType
            cardMask
            expirationDate
          }
        }
      }
    `,
  };
}