import Relay from 'react-relay'

export default class UpdateCircleSynchronizeSettings extends  Relay.Mutation {
  getMutation() {
    return Relay.QL`mutation Mutation{
      upUser
    }`;
  }

  getVariables() {
    return {
      userID: this.props.userIDVar,
      user: {
        calendar: {
          users: this.props.usersVar,
        }
      }
    }
  }

  getFatQuery() {
    return Relay.QL`
    fragment on upUserPayload {
      clientMutationId,
      viewer
      user {
        id,
        calendar {
          users
        }
      }
    }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
        fieldIDs: {
          user: this.props.userIDVar,
        },
    }];
  }

  static fragments = {
    user: () => Relay.QL`
      fragment on User{
        id,
        calendar {
          users {
            id
          }
        }
      }
    `
  }
}