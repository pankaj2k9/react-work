import Relay from 'react-relay';
/**
*  Add new bank account mutation
*/
export default class UpdateNaturalBasicMutation extends Relay.Mutation {
  /**
  *  Mutation
  */
  getMutation() {
    return Relay.QL`mutation Mutation{
      upUser
    }`;
  }
  /**
  *  Variables
  */
  getVariables = () => (
    {
      userID: this.props.userIDVar,
      user: {
        occupation: this.props.occupationVar,
        incomeRange: this.props.incomeRangeVar,
        address: this.props.addressVar
      },
    }
  )
  /**
  *  Fat query
  */
  getFatQuery() {
    return Relay.QL`
      fragment on upUserPayload {
        viewer
        user {
          id
          profileType
          email
          firstName
          lastName
          birthday
          nationality
          occupation
          incomeRange
          address {
            address
            city
            country
            zip
          }
        }
        clientMutationId
      }
    `;
  }

  /**
  *  Config
  */
  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        user: this.props.userIDVar,
      },
    }];
  }

  static fragments = {
    user: () => Relay.QL`
      fragment on User {
        id
        profileType
        email
        firstName
        lastName
        birthday
        nationality
        occupation
        incomeRange
        address {
          address
          city
          country
          zip
        }
      }
    `,
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
        me {
          id
        }
      }
    `,
  };
}