import Relay from 'react-relay';
/**
*  Add new bank account mutation
*/
export default class UpdateNaturalBasicMutation extends Relay.Mutation {
  /**
  *  Mutation
  */
  getMutation() {
    return Relay.QL`mutation Mutation{
      upUser
    }`;
  }
  /**
  *  Variables
  */
  getVariables = () => (
    {
      userID: this.props.userIDVar,
      user: {
        email: this.props.emailVar,
        firstName: this.props.firstNameVar,
        lastName: this.props.lastNameVar,
        birthday: this.props.birthdayVar,
        nationality: this.props.nationalityVar,
      },
    }
  )
  /**
  *  Fat query
  */
  getFatQuery() {
    return Relay.QL`
      fragment on upUserPayload {
        user {
          id
          profileType
          email
          firstName
          lastName
          birthday
          nationality
          occupation
          incomeRange
          address {
            address
            city
            country
            zip
          }
        }
        clientMutationId
      }
    `;
  }

  /**
  *  Config
  */
  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        //viewer: this.props.viewer.id,
        user: this.props.userIDVar,
      },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
        me {
          id
        }
      }
    `,
    user:() => Relay.QL`
    fragment on User {
      id
      profileType
      email
      firstName
      lastName
      birthday
      nationality
      occupation
      incomeRange
      address {
        address
        city
        country
        zip
      }
    }
    `
  };
}