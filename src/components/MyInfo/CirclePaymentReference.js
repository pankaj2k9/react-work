import React from 'react'
import Relay from 'react-relay'

import localizations from '../Localizations'
import styles from './Styles'

class CirclePaymentReference extends React.Component {
	constructor(props) {
        super(props)		
    }

    componentDidMount = () => {
        if (this.props.circleId) {
            this.props.relay.setVariables({
                query: true,
                circleId: this.props.circleId
            })
        }
    }

    render() {
        const {viewer} = this.props;

        return (
            <span style={styles.refContainer}>
                {viewer.circlePersonalReference 
                ?   localizations.circle_member_reference_number + ': ' + viewer.circlePersonalReference
                :   null}
            </span>
        )
	}
}

export default Relay.createContainer(CirclePaymentReference, {
    initialVariables: {
        query: false,
        circleId: null
    },
    fragments: {
        viewer: () => Relay.QL`
            fragment on Viewer {
                id
                circlePersonalReference(circleId: $circleId) @include(if: $query)
            }`
    }
})
				