import Relay from 'react-relay';

export default class UpdateUserMutation extends Relay.Mutation {

    getMutation() {
        return Relay.QL`mutation Mutation{
            upUser
        }`;
    }

    getVariables() {
        return {
            userID: this.props.userIDVar,
            user: {
                authorized_managers: this.props.authorized_managersVar,
                userPreferences: this.props.userPreferencesVar,
                homePagePreference: this.props.homePagePreferenceVar
            }

        };
    }

    getFatQuery() {
        return Relay.QL`
        fragment on upUserPayload {
            clientMutationId,
            viewer
            user {
                id,
                authorized_managers
                userPreferences {
                    areSubAccountsActivated
                }
                homePagePreference
            }
        }
        `;
    }

    getConfigs() {
        return [{
        type: 'FIELDS_CHANGE',
            fieldIDs: {
                user: this.props.userIDVar,
            },
        }];
    }

    static fragments = {
        user: () => Relay.QL`
            fragment on User {
                id,
                authorized_managers {
                    user {
                        id
                        pseudo
                        avatar
                    }
                    authorization_level
                }
                userPreferences {
                    areSubAccountsActivated
                }
                homePagePreference
            }
        `,
    };

}
