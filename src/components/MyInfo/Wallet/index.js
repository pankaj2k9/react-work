import React from 'react'
import Relay from 'react-relay'
import AlertContainer from 'react-alert'
import ReactLoading from 'react-loading'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Radium from 'radium'

import styles from '../Styles.js'
import localizations from '../../Localizations'
import { appStyles, colors } from '../../../theme'

class Wallet extends React.Component {
	constructor(props) {
		super(props)
		this.alertOptions = {
            offset: 60,
            position: 'top right',
            theme: 'light',
            transition: 'fade',
        };
        this.state = {
            isLoading: false,
            amountToAddVisible: false,
            instructionsVisible: false,
            amount: {
                cents: 0,
                currency: 'CHF'
            }
        }
    }

    componentDidMount = () => {
        this.props.relay.setVariables({
            queryAmountOnWallet: true
        });
        if (this.props.userCurrency)
            this.setState({
                amount: {
                    cents: 0,
                    currency: this.props.userCurrency
                }
            })
    }

    componentWillReceiveProps = (nextProps) => {
        if (nextProps.viewer.bankwireToWallet && !this.state.instructionsVisible) {
            this.setState({
                isLoading: false,
                instructionsVisible: true
            })
        }
    }

    _handleShowAddAmount = () => {
        this.setState({
            amountToAddVisible: true,
        })
    }

    _updateAmount = (event) => {
        this.setState({
            amount: {
                cents: event.target.value,
                currency: this.props.userCurrency
            }
        })
    }

    _handleQueryNewBankWire = () => {
        if (this.state.amount.cents > 0) {
            this.props.relay.setVariables({
                queryBankWire: true,
                amount: this.state.amount
            })
            this.setState({
                isLoading: true
            })
        }
        else {
            this.msg.show(localizations.info_wallet_add_by_bankwire_wrong_amount, {
                time: 2000,
                type: 'info',
            });
            setTimeout(() => this.msg.removeAll(), 2000);
        }
    }
    
	render() {
        const { viewer, isProfileComplete } = this.props
        
		return(
			<section>	
				<AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
				<div style={styles.rowHeader}>
					<div style={styles.pageHeader}>{localizations.info_wallet_title}</div>
				</div>
                
                {!isProfileComplete 
                ?   <section>
						<div style={styles.completeInfoText}>
							{localizations.payment_wallet_complete_profile}
						</div>
						<div style={{...styles.completeInfoText, color: colors.red}}>
							{localizations.payment_wallet_complete_profile2}
						</div>
					</section>
                :   <section>
                        <div style={styles.row}>
                            <div style={styles.subHeader}>
                                {localizations.info_wallet_amount}
                            </div>
                            <div style={styles.label}>
                                {viewer.amountOnWallet && viewer.amountOnWallet.amountOnWallet 
                                ? viewer.amountOnWallet.amountOnWallet.cents / 100 + ' ' + viewer.amountOnWallet.amountOnWallet.currency
                                : localizations.info_wallet_nothing}
                            </div>
                        </div>
                        <div style={styles.row}>
                            <div style={styles.subHeader}>
                                {localizations.info_wallet_locked_amount}
                            </div>
                            <div style={styles.label}>
                                {viewer.amountOnWallet && viewer.amountOnWallet.lockedAmount 
                                ? viewer.amountOnWallet.lockedAmount.cents / 100 + ' ' + viewer.amountOnWallet.lockedAmount.currency
                                : 'O ' + this.props.userCurrency}
                            </div>
                        </div>
                        {viewer.amountOnWallet && viewer.amountOnWallet.amountOnWallet && !this.state.amountToAddVisible &&
                            <div style={styles.row}>
                                <button style={appStyles.blueButton} onClick={this._handleShowAddAmount}>
                                    {localizations.info_wallet_add_by_bankwire}
                                </button>
                            </div>
                        }
                        {this.state.amountToAddVisible && 
                            <section>
                                <div style={styles.row}>
                                    <div style={styles.subHeader}>
                                        {localizations.info_wallet_add_by_bankwire}
                                    </div>
                                </div>
                                <div style={styles.row}>
                                    <div style={styles.longLabel}>
                                        {localizations.info_wallet_add_by_bankwire_amount}
                                    </div>
                                    <input type='number' 
                                        style={styles.smallInput} 
                                        value={this.state.amount.cents} 
                                        onChange={this._updateAmount}
                                        /> 
                                    <div style={styles.smallLabel}>
                                        {this.state.amount.currency}
                                    </div>
                                </div>
                                {!this.state.instructionsVisible && 
                                    <div style={styles.row}>
                                        {this.state.isLoading 
                                        ? <ReactLoading type='cylon' color={colors.blue} />
                                        : <button style={appStyles.blueButton} onClick={this._handleQueryNewBankWire}>
                                            {localizations.info_wallet_add_by_bankwire_validate}
                                        </button>
                                        }
                                    </div>
                                }
                            </section>
                        }
                        {viewer.bankwireToWallet && !this.state.isLoading && this.state.instructionsVisible && 
                            <section>
                                <div style={styles.row}>
                                    <div style={styles.subHeader}>
                                        {localizations.info_wallet_bankwire_explaination}
                                    </div>
                                </div>
                                <div style={styles.row}>
                                    <div style={styles.importantNote}>
                                        {localizations.info_wallet_bankwire_delay_explaination}
                                    </div>
                                </div>
                                <div style={styles.exclamationRow}>
                                    <div style={styles.exclamationMark}>
                                        <i className="fa fa-exclamation-triangle fa-2x" />
                                    </div>
                                    <div style={styles.importantNote}>
                                        {localizations.info_wallet_bankwire_explaination3}
                                    </div>
                                </div>
                                <div style={styles.row}>
                                    <div style={styles.explaination}>
                                        {localizations.info_wallet_bankwire_explaination2}
                                    </div>
                                </div>
                                <div style={styles.row}>
                                    <div style={styles.longLabel}>
                                        {localizations.info_wallet_wire_reference+':'}
                                    </div>
                                    <div style={styles.smallLabel}>
                                        {viewer.bankwireToWallet.wireReference}
                                    </div>
                                </div>
                                <div style={styles.row}>
                                    <div style={styles.longLabel}>
                                        {localizations.info_wallet_bankAccount_type+':'}
                                    </div>
                                    <div style={styles.smallLabel}>
                                        {viewer.bankwireToWallet.bankAccountType}
                                    </div>
                                </div>
                                <div style={styles.row}>
                                    <div style={styles.longLabel}>
                                        {localizations.info_wallet_ownerName+':'}
                                    </div>
                                    <div style={styles.smallLabel}>
                                        {viewer.bankwireToWallet.ownerName}
                                    </div>
                                </div>
                                <div style={styles.row}>
                                    <div style={styles.longLabel}>
                                        {localizations.info_wallet_ownerAddress+':'}
                                    </div>
                                    <div style={styles.smallLabel}>
                                        {viewer.bankwireToWallet.ownerAddress}
                                    </div>
                                </div>
                                <div style={styles.row}>
                                    <div style={styles.longLabel}>
                                        {localizations.info_wallet_IBAN+':'}
                                    </div>
                                    <div style={styles.smallLabel}>
                                        {viewer.bankwireToWallet.IBAN}
                                    </div>
                                </div>
                                <div style={styles.row}>
                                    <div style={styles.longLabel}>
                                        {localizations.info_wallet_BIC+':'}
                                    </div>
                                    <div style={styles.smallLabel}>
                                        {viewer.bankwireToWallet.BIC.length === 8 
                                        ? viewer.bankwireToWallet.BIC + 'XXX'
                                        : viewer.bankwireToWallet.BIC}
                                    </div>
                                </div>
                            </section>
                        }
                    </section>
                }
            </section>
		)
	}
}

const dispatchToProps = (dispatch) => ({
})
  
const stateToProps = (state) => ({
    userCurrency: state.globalReducer.userCurrency,
})

let ReduxContainer = connect(
    stateToProps,
    dispatchToProps
)(Radium(Wallet));

export default Relay.createContainer(ReduxContainer, {
    initialVariables: {
        queryBankWire: false,
        queryAmountOnWallet: false,
        amount: null
    },
    fragments: {
		viewer: () => Relay.QL`
            fragment on Viewer {
				amountOnWallet @include(if: $queryAmountOnWallet) {
                    amountOnWallet {
                        cents,
                        currency
                    }
                    lockedAmount {
                        cents,
                        currency
                    }
                }
                bankwireToWallet (amount: $amount) @include(if: $queryBankWire) {
                    wireReference
                    bankAccountType
                    ownerName
                    ownerAddress
                    IBAN
                    BIC
                }
			}
        `
    },
})