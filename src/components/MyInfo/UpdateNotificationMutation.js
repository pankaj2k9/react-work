import Relay from 'react-relay';

export default class UpdateNotificationMutation extends Relay.Mutation {
  /**
  *  Mutation
  */
  getMutation() {
    return Relay.QL`mutation Mutation{
      upUser
    }`;
  }
  /**
  *  Variables
  */
  getVariables = () => (
    {
      userID: this.props.userIDVar,
      user: {
        notification_preferences: this.props.notification_preferencesVar,
        email_preferences: this.props.email_preferencesVar
      },
    }
  )
  /**
  *  Fat query
  */
  getFatQuery() {
    return Relay.QL`
      fragment on upUserPayload {
        viewer
        user {
          id
          notification_preferences {
            sportunityBooked
            sportunityBookerCancel
            sportunityNewInvited
            sportunityNewFollower
            sportunityModifiedParticipant					
            sportunityCancelParticipant
            paymentConfirmationOnDDay
            sportunityNewMainOrganizer
            sportunityBookedOrganizer
            sportunityBookerCancelOrganizer
            sportunityCancelMainOrganizer
            sportunityModifiedMainOrganizer					
            paymentReceivedMainOrganizer
            sportunityCompleteStatistics
            sportunityVoteForManOfTheGame
          }
          email_preferences {
            sportunityBooked
            sportunityBookerCancel
            sportunityNewInvited
            sportunityNewFollower
            sportunityModifiedParticipant					
            sportunityCancelParticipant
            chatUnReadMessage
            paymentConfirmationOnDDay
            sportunityNewMainOrganizer
            sportunityBookedOrganizer
            sportunityBookerCancelOrganizer
            sportunityCancelMainOrganizer
            sportunityModifiedMainOrganizer					
            paymentReceivedMainOrganizer
          }
        }
        clientMutationId
      }
    `;
  }

  /**
  *  Config
  */
  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        user: this.props.userIDVar,
      },
    }];
  }

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id
        me {
          id
        }
      }
    `,
  };
}