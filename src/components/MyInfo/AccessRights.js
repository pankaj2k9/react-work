import React from 'react'
import Relay from 'react-relay'
import AlertContainer from 'react-alert'
import ReactLoading from 'react-loading'
import { isEqual} from 'lodash';

import { appStyles, colors, fonts } from '../../theme'

import localizations from '../Localizations'
import Invited from '../NewSportunity/Invited'
import SelectUser from './SelectUser'
import Circle from '../common/Header/Circle'
import UpdateUserMutation from './UpdateUserMutation'

import Styles from './Styles'

const ADMIN_LEVEL = 'ADMIN'

const toAuthorizedManagersVar = (mngrs) =>
	mngrs.reduce((mem, mngr) => [...mem, {
		user: mngr.user.id,
		authorization_level: ADMIN_LEVEL,
	}], [])

class AccessRights extends React.Component {
	state = {
		authorized_managers: [],
		isSaving: false
	}

	alertOptions = {
		offset: 60,
		position: 'top right',
		theme: 'light',
		transition: 'fade',
	};

	componentDidMount() {
		this.componentWillReceiveProps(this.props)
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.user && nextProps.user.authorized_managers) {
			this.setState({
				authorized_managers: nextProps.user.authorized_managers,
				authorized_managersSrc: nextProps.user.authorized_managers,
			})
		}
	}

	_userAlreadyExist = (user) =>
		this.state.authorized_managers.find(
			(authrorized) => authrorized.user.id === user.id
		)

	_handleAddUser = (user) =>
    !this._userAlreadyExist(user) &&
    this.setState({
      authorized_managers: [
				...this.state.authorized_managers,
				{ user, }
			]
    })

	_removeManager = (index) =>
		this.setState({
			authorized_managers:
				this.state.authorized_managers.filter((item, i) => i !== index),
		})

	_handleSave = () => {
		this.setState({
			isSaving: true,
		})
		this.props.relay.commitUpdate(
      	new UpdateUserMutation({
				user: this.props.user,
				userIDVar: this.props.user.id,
				authorized_managersVar:
					toAuthorizedManagersVar(this.state.authorized_managers)
			}),	{
				onFailure: error => {
					this.msg.show(localizations.popup_editMyInfo_update_falied, {
						time: 2000,
						type: 'error',
					});
					setTimeout(() => this.msg.removeAll(), 2000);
					this.setState({
						isSaving: false,
					})
				},
				onSuccess: response => {
					this.msg.show(localizations.popup_editMyInfo_update_sucess, {
						time: 2000,
						type: 'success',
					});
					setTimeout(() => this.msg.removeAll(), 2000);
					this.setState({
						isSaving: false,
					})

				}
			}
		)
	}

	_handleCancel = () => {
		this.setState({
			authorized_managers: this.state.authorized_managersSrc,
		})
	}

	render() {
		const { user, viewer } = this.props
		const { authorized_managers } = this.state 

		return(
			<section style={styles.container}>
				<AlertContainer ref={a => this.msg = a} {...this.alertOptions} />

				<div style={styles.pageHeader}>{localizations.accessshare_title}</div>
				<div style={styles.row}>
					<SelectUser
						viewer={viewer}
						onSelectedUser={this._handleAddUser}
						language={this.props.language}
					/>
				</div>
				<div style={styles.row}>
					<ul style={styles.list}>
						{authorized_managers.length > 0 
						?	authorized_managers.map((authorized, index) => (
								<li
										key={index}
										style={styles.listItem}
								>
									<Circle image={authorized.user.avatar} style={styles.icon} />
									{authorized.user.pseudo}
									<span style={styles.removeCross} onClick={() => this._removeManager(index)}>
										<i className="fa fa-times" style={styles.cancelIcon} aria-hidden="true"></i>
									</span>
								</li>
							))
						:	<div style={styles.noManagerContainer}>
								<div style={styles.noManagerText}>
									{localizations.noManagerText}
								</div>
								{localizations.getLanguage().toUpperCase() === 'FR'
								?	<img src="/assets/images/noManager-FR.png" style={styles.noManagerImage}/>
								:	<img src="/assets/images/noManager-EN.png" style={styles.noManagerImage}/>
								}
							</div>
						}
					</ul>
				</div>
				<div style={styles.row}>
					{	this.state.isSaving 
					?	<ReactLoading type='cylon' color={colors.blue} /> 
					:	!isEqual(this.state.authorized_managers, user.authorized_managers) && 
							<section>
								<button style={appStyles.blueButton} onClick={this._handleSave}>{localizations.info_update}</button>
								<button style={appStyles.grayButton} onClick={this._handleCancel}>{localizations.info_cancel}</button>
							</section> 
					}
				</div>
			</section>
		)
	}
}

const alertOptions = {
	offset: 60,
	position: 'top right',
	theme: 'light',
	transition: 'fade',
}

const styles = {
	...Styles,
	list: {
		width: '100%',
	},
	listItem: {
		paddingBottom: 10,
		color: '#515151',
		fontSize: 20,
		fontWeight: 500,
		fontFamily: 'Lato',
		borderBottomWidth: 1,
		borderColor: colors.blue,
		borderStyle: 'solid',
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'flex-start',
		alignItems: 'center',
		marginBottom: 5,
		maxWidth: 500,
	},
	removeCross: {
		float: 'right',
		marginLeft: 'auto',
		width: 0,
		color: colors.gray,
		marginRight: '15px',
		cursor: 'pointer',
		fontSize: '16px',
	},
	cancelIcon: {
		marginRight: 15,
	},
	icon: {
		display: 'inline-block',
		width: 38,
		height: 38,
		marginRight: 10,
	  },
	noManagerContainer: {
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center'
	},
	noManagerText: {
		textAlign: 'center',
		fontSize: 16,
		fontFamily: 'Lato',
		color: colors.darkGray,
		margin: 25
	},
	noManagerImage: {
		maxWidth: '100%',
		height: 'auto'
	}
}

export default Relay.createContainer(AccessRights, {
  fragments: {
		viewer: () => Relay.QL`
      fragment on Viewer {
				id
				${SelectUser.getFragment('viewer')}
			}
    `,
		user: () => Relay.QL`
			fragment on User {
				id
				${UpdateUserMutation.getFragment('user')}
				authorized_managers {
					user {
						id
						avatar
						pseudo
					}
				}
			}`
  },
})
