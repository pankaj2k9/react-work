# Sportunity React Web Application

## Installation

Please use node@7.8.0 (use n module to change it if needed)

```
npm install
git pull
```
It is important to pull code after installing because some packages are modified in /node_modules/

You need to check some package versions : 
```
npm list react-select
```
If it is not 1.0.0-rc.3, then `npm install react-select@1.0.0-rc.3`

Those modules have to be installed globally : 
├── babel@6.23.0
├── babel-cli@6.24.1
├── babel-eslint@7.2.2
├── babel-plugin-transform-es2015-classes@6.24.1
├── eslint@4.3.0
├── eslint-plugin-import@2.2.0
├── eslint-plugin-react@6.10.3
├── eslint-plugin-react-native@2.3.2
├── webpack@2.4.1
├── webpack-dev-server@2.4.2

## Running

1. For developments :

Start a local server:
In 3 different terminals:
```
npm run webpack-dev
npm run build-dev
npm run start-dev
```

2. For production :
```
npm run start
```

## Modifying files 

To modify any file, please create a new branch from the master : 
```
git checkout master
git pull
git checkout -b features/FEATURE_NAME
```
Then after you finished your modifications, you can push and create a new pull request.

## Troubleshooting

1. If your changes are not seeable after you saved :

This is because we're serving a gzipped file, which is not compiled every time you save a file. 
To solve this, you can change this in src/server.js :

```
app.get('/sportunity.js', (req, res) => {
    res.setHeader('Content-Type', 'application/javascript');
    if (backendUrlGraphql.search('https') >= 0) {
        req.url = req.url + '.gz';
        res.set('Content-Encoding', 'gzip');
        res.sendFile('sportunity.js.gz', {root: __dirname});
    }
    else if (backendUrlGraphql.search('http') >= 0) {
        res.sendFile('sportunity.js', {root: __dirname});
    }
});
```
to :
```
app.get('/sportunity.js', (req, res) => {
    res.setHeader('Content-Type', 'application/javascript');
	res.sendFile('sportunity.js', {root: __dirname});
});
```

Those lines can also be commented in src/webpack.config.js :

```
new webpack.optimize.UglifyJsPlugin({
	compress: {warnings: false},
	comments: false,
	minimize: false,
	sourceMap: true
}),
new webpack.optimize.AggressiveMergingPlugin(),        
new CompressionPlugin({
	asset: "[path].gz[query]",
	algorithm: "gzip",
	test: /\.(js|html)$/,
	threshold: 10240,
	minRatio: 0.8
}),       
```

2. You have a "javascript heap out of memory" error : 

In package.json, replace build-dev script with this :

```
node --max_old_space_size=8192 node_modules/.bin/webpack --progress --colors --watch
```

3. Update SSL certificate
- Download certificate from goDaddy
- Copy the content to the concerned server in /etc/nginx/certs
- cat dpoaziepoudd.crt gb_bundle-g2-g1.crt >> bundle.crt (after making a copy)
- Restart nginx : /etc/init.d/nginx restart
- Check that there's no error : openssl s_client -connect domain_name:443

