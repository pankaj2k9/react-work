var path = require('path');
var webpack = require('webpack');
var combineLoaders = require('webpack-combine-loaders')
var CompressionPlugin = require("compression-webpack-plugin");
//var BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
    entry: [
        "babel-polyfill", path.resolve(__dirname, 'lib', 'client.js'),
    ],
    output: {
        filename: 'sportunity.js',
        path: path.resolve(__dirname, 'lib'),
    },
    devtool: 'cheap-module-source-map',
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: { warnings: false },
            comments: false,
            minimize: false,
            sourceMap: true
        }),
        new webpack.optimize.AggressiveMergingPlugin(),
        new CompressionPlugin({
            asset: "[path].gz[query]",
            algorithm: "gzip",
            test: /\.(js|html|css)$/,
            threshold: 10240,
            minRatio: 0.8
        }),
        new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /de|fr|en|es/),
        //new BundleAnalyzerPlugin({openAnalyzer: true})
    ],
    module: {
        loaders: [
            {
                test: /\.css$/,
                loader: combineLoaders([
                    {
                        loader: 'style-loader'
                    }, {
                        loader: 'css-loader',
                        query: {
                            modules: true,
                            localIdentName: '[name]__[local]___[hash:base64:5]'
                        }
                    }
                ])
            },
            {
                test: /\.js?$/,
                exclude: /node_modules/,
                loaders: ["react-hot-loader", "babel-loader"],
            }, { 
                test: /\.(jpe?g|png|gif|svg)$/i, 
                loaders: [ 
                  'file?hash=sha512&digest=hex&name=[hash].[ext]', 
                  'image-webpack?optimizationLevel=7&interlaced=false' 
                ] 
            }
        ]
    },
};
